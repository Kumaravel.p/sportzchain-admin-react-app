import { message } from 'antd';
import { uploadFile } from 'apis/upload';
import config from 'config';

export const handleMediaUpload = async (
  file: File,
  // getPresignedUrlQuery: any,
  onProgress?: any
) => {
  var progressPercent = 0;
  try {
    // setIsLoading(true);
    console.log(file);
    const reader = new FileReader();
    if (file) {
      // onFile(file);
      reader.readAsDataURL(file);
      progressPercent = 20;
      reader.onload = function () {
        if (reader.result) {
          progressPercent = 40;

          // setImageDataUrl(reader.result.toString());
        }
      };
      reader.onerror = function (error: any) {
        console.log('Error: ', error);
        message.error(
          error?.message ?? 'Something went wrong: ' + JSON.stringify(error)
        );
      };

      // Upload file to our S3 bucket.
      // const { data: presignedUrlData } = await getPresignedUrlQuery();

      // if (presignedUrlData?.get_signed_url?.url) {
      //   progressPercent = 70;

      //   await uploadFile(presignedUrlData?.get_signed_url?.url, file);
      //   console.log(
      //     `${config.s3BaseUrl}/${presignedUrlData?.get_signed_url?.key}`
      //   );
      //   progressPercent = 100;
      //   onProgress({ percent: progressPercent });

      //   return `${config.s3BaseUrl}/${presignedUrlData?.get_signed_url?.key}`;
      // }
    }
  } catch (error) {
    console.log(error);
  }
};
