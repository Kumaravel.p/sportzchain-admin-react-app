import { countryCode } from "./countryCodeList";
import { getExampleNumber } from "libphonenumber-js";
import examples from "libphonenumber-js/examples.mobile.json";
import { Auth } from "aws-amplify";
import moment from "moment";
import { categorytype } from "./enums";
import { useSnapshot } from "valtio";
import { state } from "state";
import { fetchUserMeta } from "apis/profile";

export const getUniqueData = (array, flitter) => {
    return [...new Set(array.reduce((a, c) => [...a, c.flitter], []))]
};

export const parseJwt = (token) => {
    if (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
        return JSON.parse(jsonPayload);
    } else {
        return {}
    }
};

// export const tokenDetails = () => { return parseJwt(localStorage.getItem('token')) };
// 
// export const teamDetail = () => {
//     let token = tokenDetails()?.sub
//     fetchUserMeta({remoteId: token}).then(res => {
//         const teamDetails = res?.data?.data?.teamDetail;
//         return teamDetails
//     })
// }

// export const isIamTeamUser = () => {
//     let team = teamDetail();
//     return team?.teams?.length > 0;
// }

// export const giveMeTeamDetails = () => {
//   let token = tokenDetails()?.sub;
//   let teamDetails;
//   fetchUserMeta({ remoteId: token }).then((res) => {
//     teamDetails = res?.data?.data?.teamDetail;
//   }).catch(ex => ex);
//   return teamDetails;
// };

// export const canIShow = (teamId, module, action) => {
//     return giveMeTeamDetails()?.permissions?.[teamId]?.[module]?.includes(action) ?? false
// };

export const statusAndEditFuntion = (allStatus, getData) => {

    // Initial status to display- Incomplete, Submitted
    // Status = Incomplete - can update anything (Status options - Submitted)
    // Status = Submitted - Only allow to capture notes (Status options - Incomplete, Approved)
    // Status = Approved - Only allow to capture notes (Status options - Discontinue)
    // Status = Discontinue - no updates allowed (no data can be updated including the notes) - hide Edit
    // Status = Open - Allow to update the end date (Status options - Suspend)
    // Status = Suspend - Only allow to capture notes (Status options - Open, Closed)
    // Status = Closed - no updates allowed (no data can be updated including the notes) - hide Edit 
    let status = [];
    let disableComponent = {
        // basic Info
        "basicinfo": false,
        // basic Info Component
        "teamName": false,
        "statusId": false,
        // Initial Offering
        "InitialOffering": false,
        // Initial Offering Component
        "targetAmount": false,
        "supplySize": false,
        "initialPrice": false,
        "startDateTime": false,
        "endDateTime": false,
        "displayStartDateTime": false,
        "displayEndDateTime": false,
        // Time Locking 
        "TimeLocking": false,
        // Time Locking Component
        "initialTokenRelease": false,
        "initialTokensToBeUnlocked": false,
        "remainingTokenRelease": false,
        // Discounts
        "Discounts": false,
        // Discounts Component
        "discount": false,
        "discountsPurchase": false,
        // Restriction Limits
        "RestrictionLimits": false,
        // Restriction Limits Component
        "maxTokenPurchaseLimitPerUser": false,
        "minTokenPurchaseLimitPerUser": false,
        "noofTransactionsLimitPerUser": false,
        // Notes 
        "notes": false,
        // Notes Component
        "NotedataSource": false
    };
    if (getData) {
        if (getData[0]?.stoStatusId === 2) {
            // Submitted 2
            status = allStatus?.filter((val) => val?.id === getData[0]?.stoStatusId || val?.id === 9 || val?.id === 3);
            disableComponent = {
                "basicinfo": true,
                // basic Info Component
                "teamName": true,
                "statusId": false,
                // Initial Offering
                "InitialOffering": true,
                // Initial Offering Component
                "targetAmount": true,
                "supplySize": true,
                "initialPrice": true,
                "startDateTime": true,
                "endDateTime": true,
                "displayStartDateTime": true,
                "displayEndDateTime": true,
                // Time Locking 
                "TimeLocking": true,
                // Time Locking Component
                "initialTokenRelease": true,
                "initialTokensToBeUnlocked": true,
                "remainingTokenRelease": true,
                // Discounts
                "Discounts": true,
                // Discounts Component
                "discount": true,
                "discountsPurchase": true,
                // Restriction Limits
                "RestrictionLimits": true,
                // Restriction Limits Component
                "maxTokenPurchaseLimitPerUser": true,
                "minTokenPurchaseLimitPerUser": true,
                "noofTransactionsLimitPerUser": true,
            }
        }
        else if (getData[0]?.stoStatusId === 3) {
            // Approved 3
            status = allStatus?.filter((val) => val?.id === getData[0]?.stoStatusId || val?.id === 9);
            disableComponent = {
                "basicinfo": true,
                // basic Info Component
                "teamName": true,
                "statusId": false,
                // Initial Offering
                "InitialOffering": true,
                // Initial Offering Component
                "targetAmount": true,
                "supplySize": true,
                "initialPrice": true,
                "startDateTime": true,
                "endDateTime": true,
                "displayStartDateTime": true,
                "displayEndDateTime": true,
                // Time Locking 
                "TimeLocking": true,
                // Time Locking Component
                "initialTokenRelease": true,
                "initialTokensToBeUnlocked": true,
                "remainingTokenRelease": true,
                // Discounts
                "Discounts": true,
                // Discounts Component
                "discount": true,
                "discountsPurchase": true,
                // Restriction Limits
                "RestrictionLimits": true,
                // Restriction Limits Component
                "maxTokenPurchaseLimitPerUser": true,
                "minTokenPurchaseLimitPerUser": true,
                "noofTransactionsLimitPerUser": true,
            }
        }
        else if (getData[0]?.stoStatusId === 4) {
            // Open 4
            status = allStatus?.filter((val) => val?.id === getData[0]?.stoStatusId || val?.id === 8);
            disableComponent = {
                "basicinfo": true,
                // basic Info Component
                "teamName": true,
                "statusId": false,
                // Initial Offering
                "InitialOffering": false,
                // Initial Offering Component
                "targetAmount": true,
                "supplySize": true,
                "initialPrice": true,
                "startDateTime": true,
                "endDateTime": false,
                "displayStartDateTime": true,
                "displayEndDateTime": false,
                // Time Locking 
                "TimeLocking": true,
                // Time Locking Component
                "initialTokenRelease": true,
                "initialTokensToBeUnlocked": true,
                "remainingTokenRelease": true,
                // Discounts
                "Discounts": true,
                // Discounts Component
                "discount": true,
                "discountsPurchase": true,
                // Restriction Limits
                "RestrictionLimits": true,
                // Restriction Limits Component
                "maxTokenPurchaseLimitPerUser": true,
                "minTokenPurchaseLimitPerUser": true,
                "noofTransactionsLimitPerUser": true,
            }
        }
        else if (getData[0]?.stoStatusId === 5) {
            // Closed
            // no edit
        }
        else if (getData[0]?.stoStatusId === 6) {
            // Distributed 6
            // no content
        }
        else if (getData[0]?.stoStatusId === 7) {
            // Discontinue
            // no edit
        }
        else if (getData[0]?.stoStatusId === 8) {
            // Suspend 8
            status = allStatus?.filter((val) => val?.id === getData[0]?.stoStatusId || val?.id === 4 || val?.id === 7);
            disableComponent = {
                "basicinfo": true,
                // basic Info Component
                "teamName": true,
                "statusId": true,
                // Initial Offering
                "InitialOffering": true,
                // Initial Offering Component
                "targetAmount": true,
                "supplySize": true,
                "initialPrice": true,
                "startDateTime": true,
                "endDateTime": true,
                "displayStartDateTime": true,
                "displayEndDateTime": true,
                // Time Locking 
                "TimeLocking": true,
                // Time Locking Component
                "initialTokenRelease": true,
                "initialTokensToBeUnlocked": true,
                "remainingTokenRelease": true,
                // Discounts
                "Discounts": true,
                // Discounts Component
                "discount": true,
                "discountsPurchase": true,
                // Restriction Limits
                "RestrictionLimits": true,
                // Restriction Limits Component
                "maxTokenPurchaseLimitPerUser": true,
                "minTokenPurchaseLimitPerUser": true,
                "noofTransactionsLimitPerUser": true,
            }
        }
        else if (getData[0]?.stoStatusId === 9) {
            //Saved 9
            status = allStatus?.filter((val) => val?.id === getData[0]?.stoStatusId || val?.id === 2);
        }
        else if (getData[0]?.stoStatusId === 10) {
            //Published 10
            status = allStatus?.filter((val) => val?.id === getData[0]?.stoStatusId || val?.id === 7);
        }
    } else {
        status = allStatus?.filter((val) => val?.id === 1 || val?.id === 2);
    }
    return [{
        status,
        disableComponent
    }]
}


export const range = (start, end) => {
    const result = [];
    for (let i = start; i < end; i++) {
        result.push(i);
    }
    return result;
};

export const ternaryOperator = (val, return_) => {
    return val ? val : return_;
}

export const filterStatus = (statusList, value, type) => {
    if (!statusList?.length) return []
    if (value === 'OPEN' && (type === "polls" || type === "contests")) {
        return statusList.filter(({ status }) => ['OPEN', 'SUSPENDED'].includes(status))
    }
    else if (value === "SUSPENDED" && (type === "polls" || type === "contests")) {
        return statusList.filter(({ status }) => ['OPEN', 'DISCONTINUED', 'SUSPENDED'].includes(status))
    }
    else {
        switch (value) {
            case 'SAVED':
                return statusList.filter(({ status }) => ['SAVED', 'SUBMITTED'].includes(status))
            case 'SUBMITTED':
                return statusList.filter(({ status }) => ['SAVED', 'APPROVED', 'SUBMITTED'].includes(status))
            case 'APPROVED':
                return statusList.filter(({ status }) => ['SAVED', 'APPROVED'].includes(status))
            case 'PUBLISHED':
                return statusList.filter(({ status }) => ['DISCONTINUED', 'PUBLISHED'].includes(status))
            case 'ACTIVE':
                return statusList.filter(({ status }) => ['ACTIVE', 'SUSPENDED'].includes(status))
            case 'SUSPENDED':
                return statusList.filter(({ status }) => ['ACTIVE', 'DISCONTINUED', 'SUSPENDED'].includes(status))
            default:
                return statusList
        }
    }
}

export const pollFilterStatus = (statusList, value, displayStartAt) => {
    let fields = []
    let except = false
    let status = [];
    switch (value) {
        case 'SAVED':
            status = ['SAVED', 'SUBMITTED'];
            fields = ['teamId', 'groupId', 'category'];
            except = true
            break;
        case 'SUBMITTED':
            status = ['SAVED', 'SUBMITTED', 'APPROVED'];
            fields = ['status']
            break;
        case 'APPROVED':
            status = ['SAVED', 'APPROVED'];
            fields = ['status']
            break;
        case 'READY':
            status = ['READY', 'DISCONTINUED'];
            fields = ['status', 'displayStartAt', 'displayEndAt']
            break;
        case 'PUBLISHED':
            status = ['PUBLISHED', 'DISCONTINUED',];
            if (moment(displayStartAt).isBefore(moment())) {
                fields = ['status', 'displayEndAt']
            }
            fields = ['status']
            break;
        case 'OPEN':
            status = ['OPEN', 'SUSPENDED'];
            fields = ['status', 'displayEndAt']
            break;
        case 'SUSPENDED':
            status = ['OPEN', 'SUSPENDED', 'DISCONTINUED',];
            fields = ['status']
            break;
        case 'FAILED':
            status = ['APPROVED', 'FAILED'];
            fields = ['status']
            break;
        default:
            status = []
            fields = []
    }
    return {
        options: statusList?.filter((list) => status.includes(list?.status)),
        disableComponents: fields,
        except
    }
}

export const stoFilterStatus = (statusList, value) => {
    let status = [];
    switch (value) {
        //Saved
        case 9:
            status = [9, 2];
            break;
        // SUBMITTED
        case 2:
            status = [9, 2, 3];
            break;
        // APPROVED
        case 3:
            status = [9, 3];
            break;
        // // READY
        // case 'READY':
        //     status = ['READY', 'DISCONTINUED'];
        //     fields = ['status', 'displayStartAt', 'displayEndAt']
        //     break;

        // PUBLISHED
        case 10:
            status = [10, 7,];
            break;
        // OPEN
        case 4:
            status = [4, 8];
            break;
        // SUSPENDED
        case 8:
            status = [4, 8, 7,];
            break;
        // FAILED
        // case 'FAILED':
        //     status = ['APPROVED', 'FAILED'];
        //     fields = ['status']
        //     break;
        default:
            status = []
        // fields = []
    }
    return {
        options: statusList?.filter((list) => status.includes(list?.id)),
        // disableComponents: fields,
        // except
    }
}

export const teamFilterStatus = (statusList, value) => {
    let fields = []
    let except = false
    let status = [];
    switch (value) {
        case 'SAVED':
            status = ['SAVED', 'SUBMITTED'];
            fields = [];
            except = true;
            break;
        case 'SUBMITTED':
            status = ['SAVED', 'SUBMITTED', 'APPROVED'];
            fields = ['status']
            break;
        case 'APPROVED':
            status = ['APPROVED'];
            fields = ['status']
            break;
        case 'PUBLISHED':
            status = ['PUBLISHED', 'DISCONTINUED'];
            fields = ['status', 'displayEndAt']
            break;
        case 'ACTIVE':
            status = ['ACTIVE', 'SUSPENDED'];
            fields = ['status', 'displayEndAt']
            break;
        case 'SUSPENDED':
            status = ['ACTIVE', 'SUSPENDED', 'DISCONTINUED'];
            fields = ['status']
            break;
        case 'INACTIVE':
            status = ['INACTIVE', 'SUBMITTED', 'DISCONTINUED'];
            fields = []
            break;
        case 'DISCONTINUED':
            status = ['DISCONTINUED'];
            fields = []
            break;
        default:
            status = []
            fields = []
    }
    return {
        options: statusList?.filter((list) => status.includes(list?.status)),
        disableComponents: fields,
        except
    }
}

export const groupFilterStatus = (statusList, value) => {
    let fields = []
    let except = false
    let status = [];
    switch (value) {
        // case 'SAVED':
        case 4:
            status = [4, 5];
            fields = [];
            except = true;
            break;
        // case 'Submitted':
        case 5:
            status = [4, 5, 8];
            fields = ['status']
            break;
        // case 'APPROVED':
        case 8:
            status = [8];
            fields = ['status']
            break;
        // case 'PUBLISHED':
        case 7:
            status = [7, 9];
            fields = ['status', 'displayEndAt']
            break;
        // case 'ACTIVE':
        case 1:
            status = [1, 6];
            fields = ['status', 'displayEndAt']
            break;
        // case 'SUSPENDED':
        case 6:
            status = [1, 6, 9];
            fields = ['status']
            break;
        // case 'INACTIVE':
        case 2:
            status = [2, 5, 9];
            fields = []
            break;
        // case 'DISCONTINUED':
        case 9:
            status = [9];
            fields = []
            break;
        default:
            status = []
            fields = []
    }
    return {
        options: statusList?.filter((list) => status.includes(list?.id)),
        disableComponents: fields,
        except
    }
}

export function getMobileLimitBasedOnCC(selectedCountryValue) {
    return changeMobileLimitBasedOnCC(selectedCountryValue)
}

export const changeMobileLimitBasedOnCC = (country_dial_code) => {
    const selectedCountry = countryCode?.filter(
        ({ dial_code }) => dial_code === country_dial_code
    );

    const ISO2Code = selectedCountry?.[0]?.code;
    const phoneNumber = getExampleNumber(ISO2Code, examples);
    return phoneNumber?.nationalNumber?.length ?? 10;
};
export const getUserCurrentSession = () => {
    return new Promise(async (resolve, reject) => {
        try {
            const session = await Auth.currentSession();
            const { profile } = useSnapshot(state);
            if (session) {
                resolve({ ...session, payload: { ...session.payload, user_id: profile?.user_id } });
            }
        } catch (error) {
            console.error(error);
            reject(null);
        }
    })
};


export const categoryOptions = [
    { label: "Team", value: categorytype.PLATFORM },
    { label: "Group", value: categorytype.GROUPS },
    { label: "Battleground", value: categorytype.BATTLEGROUND },
]