export enum StatusOptionsEnum {
  BLANK = 'BLANK',
  SAVED = 'SAVED',
  SUBMITTED = 'SUBMITTED',
  INACTIVE = 'INACTIVE',
  SUSPENDED = 'SUSPENDED',
}
