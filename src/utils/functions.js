export function isValidHttpUrl(string) {
  let url;

  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === 'http:' || url.protocol === 'https:';
}

export function matchYoutubeUrl(url) {
  var p = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  if(url.match(p)){
      return url.match(p)[1];
  }
  return false;
}

export const isValidEmail = (emailId) =>{
  // let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return (emailRegex.test(emailId))
}

export const parseJson = (val = "[]") => {
  try {
      return JSON.parse(val)
  }
  catch {
      return []
  }
}

export const middleEllipsis = (str) => {
  let maxLength = 15;
  if (!str) return str;
  if (maxLength < 1) return str;
  if (str.length <= maxLength) return str;
  if (maxLength === 1) return str.substring(0, 1) + '...';

  var midpoint = Math.ceil(str.length / 2);
  var toremove = str.length - maxLength;
  var lstrip = Math.ceil(toremove / 2);
  var rstrip = toremove - lstrip;
  return (
    str.substring(0, midpoint - lstrip) +
    '...' +
    str.substring(midpoint + rstrip)
  );
};