import { Auth } from 'aws-amplify';

const currentSession = async () => {
  let auth = await Auth.currentSession().then((res) => {
    localStorage.setItem('token', res.getIdToken().getJwtToken());
    return res.getIdToken();
  });
  return auth;
};

export default currentSession;
