import config from 'config';

const extractImageID = (imageURL: String) => {
    let urlarr = imageURL?.split("/");
    let id = `${urlarr[urlarr.length - 1]}`;
    return id
}
const imageUrl = (imageURL: String) => {
    if (imageURL?.includes("https:")) {
        return imageURL;
    } else {
        if (imageURL && typeof imageURL === "string") {
            let url = `${config.apiBaseUrl}files/${extractImageID(imageURL)}`
            return url
        }
    }
}
export default imageUrl;