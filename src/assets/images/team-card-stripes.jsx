import * as React from "react";

export const TeamCardStripe = (props) => {
	return (
		<svg width="272" height="336" viewBox="0 0 272 336" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M390.931 -111H453.292L-133.639 516H-196L390.931 -111Z" fill="url(#paint0_linear_1317_9179)"/>
        <path d="M482.639 -111H545L-41.9307 516H-104.292L482.639 -111Z" fill="url(#paint1_linear_1317_9179)"/>
        <defs>
        <linearGradient id="paint0_linear_1317_9179" x1="380.5" y1="-7" x2="376" y2="510" gradientUnits="userSpaceOnUse">
        <stop stop-color={props.stripesColor ?? "#53C0FF" } />
        <stop offset="0.425188" stop-color={props.stripesColor ?? "#53C0FF" }  stop-opacity="0"/>
        </linearGradient>
        <linearGradient id="paint1_linear_1317_9179" x1="380.5" y1="-7" x2="376" y2="510" gradientUnits="userSpaceOnUse">
        <stop stop-color={props.stripesColor ?? "#53C0FF" } />
        <stop offset="0.425188" stop-color={props.stripesColor ?? "#53C0FF" }  stop-opacity="0"/>
        </linearGradient>
        </defs>
        </svg>
	);
};

