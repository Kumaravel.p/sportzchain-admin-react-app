import * as React from 'react';
import { Switch, Route, useHistory, Redirect } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';

import { ToastContainer } from 'react-toastify';

import { GlobalStyles } from 'components/common/styles/GlobalStyles';
import Sidebar from 'components/sidebar/Sidebar';
import Navbar from 'components/navbar/Navbar';
import UserManagement from 'containers/UserManagement';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import Details from 'containers/UserDetails';
import Transaction from 'containers/Transaction';
import TransactionDetails from 'containers/TransactionDetails';
import MarketView from 'containers/MarketView';
import Rewards from 'containers/Rewards';
import Leaderboard from 'containers/Leaderboard';
import TeamManagement from 'containers/TeamManagement';
import RewardsDetails from 'containers/RewardDetails';
import TeamDetails from 'containers/TeamDetails';
import SignIn from 'containers/Signin';
import ReportAndAnalysis from 'containers/ReportAndAnalysis';
import { Account } from 'components/Account';
// import RequireAuth from 'components/RequireAuth';
import EditUserDetailsCard from 'components/user-details/EditUserDetailsCard';
import AddTeamDetails from 'containers/AddTeamDetails';
import EditTeamDetails from 'containers/EditTeamDetails';
import EditPasswordCard from 'components/user-details/EditPassword';
import RequireAuth from 'components/RequireAuth';
import Sto from 'containers/Sto';
import StoDetails from 'containers/StoDetails';
import AddPost from 'components/post/AddPost';
import PostManagement from 'containers/PostManagement';
import PostDetails from 'containers/PostDetails';
import EditPost from 'components/post/EditPost';
import PollsManagement from 'containers/PollsManagement';
import NewsManagement from 'containers/newsManagement';
import PollView from 'containers/PollView';
import AddPoll from 'components/poll/AddPoll';
import AddContest from 'components/contest/AddContest';
import ContestManagement from 'containers/ContestManagement';
import ContestDetails from 'containers/ContestDetails';
import GameCard from 'components/common/cards/GameCard';
import EditPoll from 'containers/EditPoll';
import CreateStoDetailPage from 'containers/createSto';
import ViewSTODetail from 'containers/ViewSTODetail';
import Addpost from 'components/news/Addpost';
import AddCms from 'components/contentManagement/addCms';
import EditNews from 'containers/EditNews';
import ListingSTO from 'containers/listingSTO';
import ListingBattleGround from 'containers/battleground/battlelist';
import currentSession from 'utils/getAuthToken';
import CreateRewards from 'containers/createRewards';
import ForgotPassword from 'containers/forgotPassword';
import ResetPassword from 'containers/resetPassword';
import ChangePassword from 'containers/ChangePassword';
import PrivateRoute from 'privateRouter';
import { SchedulerDashboard } from './containers/SchedulerDashboard';
import ActivityPoints from 'containers/activityPoints/ActivityPoints';
import CreateActivityPoint from 'containers/activityPoints/CreateActivityPoint';
import ViewActivityPoint from 'containers/activityPoints/ViewActivityPoint';
import LeaderboardLevels from 'containers/leaderboardLevels/LeaderboardLevels';
import PublicRoute from 'publicRouter';
import Groups from 'containers/groups/Groups';
import UpsertGroup from 'containers/groups/UpsertGroup';
import ViewGroup from 'containers/groups/ViewGroup';
import NewsView from 'containers/NewsView';
import StaticContentManagement from 'containers/staticContentManagement';
import GroupCriteria from 'containers/groups/GroupCriteria';
import UserListing from 'containers/users/UserListing';
import UserDetails from 'containers/users/UserDetails';
import ProfileMain from 'components/profile/profileMain';
import Reports from 'containers/Reports';
import { fetchUserMeta } from 'apis/profile';
import { state as ProxyState } from 'state';
import TeamImageBanner from 'components/contentManagement/teambanner';
import { useSnapshot } from 'valtio';
import UpsertBattle from 'containers/battleground/upsertBattle';
import ViewBattleground from 'containers/battleground/viewBattleground';

const queryClient = new QueryClient();

export const TeamSelectionContext = React.createContext({
  team: { id: 0, name: "" },
  setTeam: (team: any): any => false
})

const App = (): JSX.Element => {
  // check for token in localStorage

  const history = useHistory();
  const [loggedIn, setLoggedIn] = React.useState<any | null>(false)
  React.useEffect(() => {
    currentSession().then((res) => {
      let accessToken = res?.getJwtToken();
      if (!accessToken) {
        setLoggedIn(false)
      } else {
        setLoggedIn(true)
      }
    }).catch(ex => {
      setLoggedIn(false)
    });
  }, []);
  const userDetail = (sub: any) => {
    let data = fetchUserMeta({ remoteId: sub }).then((res: any) => {
      return res?.data?.data;
    })
    return data
  }

  React.useEffect(() => {
    currentSession().then(async res => {
      let remoteId = res.payload.sub;
      let response: any = await userDetail(remoteId);
      // console.log(remoteId, "________+++++++++++++++", response);
      if (remoteId) {
        ProxyState.profile = response;
      }
    });
  }, [])

  const { profile } = useSnapshot(ProxyState);

  let team: any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }
  const [state, setState] = React.useState({ team: team?.teams?.[0] ?? {} })
  return (
    <React.Fragment>
      <GlobalStyles />
      <TeamSelectionContext.Provider value={{ ...state, setTeam: (team: any) => setState({ ...state, team }) }} >
        <Account>
          <QueryClientProvider client={queryClient}>
            {loggedIn && <Navbar />}
            <FlexRowWrapper>
              {loggedIn && <Sidebar />}
              <Switch>

                <PublicRoute path="/signin/:mode?"><SignIn /></PublicRoute>
                <PublicRoute path="/forgot-password/:mode?"><ForgotPassword /></PublicRoute>
                <PublicRoute path="/reset-password"> <ResetPassword /></PublicRoute>
                <PublicRoute path="/change-password"> <ChangePassword /></PublicRoute>
                {/* <PublicRoute path="/forgot-password/:mode?" render={() => <ForgotPassword />} /> */}
                {/* <PublicRoute path="/reset-password" render={() => <ResetPassword />} /> */}
                <PrivateRoute path="/scheduler-dashboard" ><SchedulerDashboard /> </PrivateRoute>
                <PrivateRoute path="/reports" ><Reports /> </PrivateRoute>


                <PrivateRoute path="/users" ><UserListing /> </PrivateRoute>
                <PrivateRoute exact path="/user/:id" ><UserDetails /></PrivateRoute>

                <PrivateRoute path="/team/add" ><AddTeamDetails /></PrivateRoute>
                <PrivateRoute path="/team/:teamId/edit" ><EditTeamDetails /></PrivateRoute>
                <PrivateRoute exact path="/team/:id" ><TeamDetails /></PrivateRoute>
                <PrivateRoute exact path="/team-home" ><TeamManagement /></PrivateRoute>

                <PrivateRoute path="/contests/add" ><AddContest /></PrivateRoute>
                <PrivateRoute path="/contests/:contestId/edit" ><AddContest /></PrivateRoute>

                <PrivateRoute exact path="/report-and-analysis" ><ReportAndAnalysis /></PrivateRoute>

                <PrivateRoute exact path="/user/:id/edit" ><EditUserDetailsCard /></PrivateRoute>

                <PrivateRoute exact path="/transactions" ><Transaction /></PrivateRoute>

                <PrivateRoute exact path="/rewards" ><Rewards /></PrivateRoute>
                <PrivateRoute exact path="/create-reward" ><CreateRewards /></PrivateRoute>
                <PrivateRoute exact path="/edit-reward/:id" ><CreateRewards /></PrivateRoute>
                <PrivateRoute exact path="/view-rewards" ><RewardsDetails /></PrivateRoute>

                <PrivateRoute exact path="/sto" ><Sto /></PrivateRoute>
                <PrivateRoute exact path="/sto/:id" ><StoDetails /></PrivateRoute>
                <PrivateRoute exact path="/create-sto" ><CreateStoDetailPage /></PrivateRoute>
                <PrivateRoute exact path="/view-sto" ><ViewSTODetail /></PrivateRoute>
                <PrivateRoute exact path="/listing-sto" ><ListingSTO /></PrivateRoute>

                <PrivateRoute exact path="/leaderboard" ><Leaderboard /></PrivateRoute>

                <PrivateRoute exact path="/admin/:id/edit" ><EditPasswordCard /></PrivateRoute>

                <PrivateRoute exact path="/market-view" ><MarketView /></PrivateRoute>

                <PrivateRoute exact path="/contests" ><ContestManagement /></PrivateRoute>
                <PrivateRoute exact path="/contests/:teamId/:contestId" ><ContestDetails /></PrivateRoute>

                <PrivateRoute exact path="/cms" ><StaticContentManagement /></PrivateRoute>
                <PrivateRoute exact path="/cms/:type/:id"><AddCms /></PrivateRoute>
                <PrivateRoute exact path="/teamBannerCms/:teamid/:id/:type"><TeamImageBanner /></PrivateRoute>

                <PrivateRoute exact path="/polls" ><PollsManagement /></PrivateRoute>
                <PrivateRoute exact path="/polls/:teamId/:id" ><PollView /></PrivateRoute>
                <PrivateRoute exact path="/poll/add" ><AddPoll /></PrivateRoute>
                <PrivateRoute exact path="/poll/edit/:id" ><EditPoll /></PrivateRoute>

                <PrivateRoute exact path="/news" ><NewsManagement /></PrivateRoute>
                <PrivateRoute exact path="/news/add" ><Addpost /></PrivateRoute>
                <PrivateRoute exact path="/news/:teamId/:id" ><NewsView /></PrivateRoute>
                <PrivateRoute exact path="/newsfeed/edit/:id" ><EditNews /></PrivateRoute>

                <PrivateRoute exact path="/posts" ><PostManagement /></PrivateRoute>
                <PrivateRoute strict path="/posts/:id" ><PostDetails /></PrivateRoute>
                <PrivateRoute exact strict path="/post/add" ><AddPost /></PrivateRoute>
                <PrivateRoute exact strict path="/post/edit/:id" ><EditPost /></PrivateRoute>

                <PrivateRoute exact strict path="/profile"><ProfileMain /></PrivateRoute>

                {/* Activity Points */}
                <PrivateRoute exact path="/activity-points" ><ActivityPoints /></PrivateRoute>
                <PrivateRoute exact path="/create-activity-point" ><CreateActivityPoint /></PrivateRoute>
                <PrivateRoute exact path="/edit-activity-point/:id" ><CreateActivityPoint /></PrivateRoute>
                <PrivateRoute exact path="/view-activity-point/:id" ><ViewActivityPoint /></PrivateRoute>

                {/* Groups */}
                <PrivateRoute exact path="/groups" ><Groups /></PrivateRoute>
                <PrivateRoute exact path="/create-group" ><UpsertGroup /></PrivateRoute>
                <PrivateRoute exact path="/edit-group/:id" ><UpsertGroup /></PrivateRoute>
                <PrivateRoute exact path="/view-group/:id" ><ViewGroup /></PrivateRoute>
                <PrivateRoute exact path="/group-criteria" ><GroupCriteria /></PrivateRoute>

                {/* BattleGround */}
                <PrivateRoute exact path="/BattleGround" ><ListingBattleGround /></PrivateRoute>
                <PrivateRoute exact path="/create-battle" ><UpsertBattle /></PrivateRoute>
                <PrivateRoute exact path="/edit-battle/:id" ><UpsertBattle /></PrivateRoute>
                <PrivateRoute exact path="/view-battle/:id" ><ViewBattleground /></PrivateRoute>


                {/* Leaderboard Levels */}
                <PrivateRoute exact path="/leaderboard-levels" ><LeaderboardLevels /></PrivateRoute>

                <PrivateRoute exact path="/gamecard" ><GameCard /></PrivateRoute>

                <PrivateRoute exact path="/transaction/:id" ><TransactionDetails /></PrivateRoute>

                <Redirect from='/' to={isIamTeamUser() ? '/team-home' : '/users'} />
              </Switch>
            </FlexRowWrapper>
          </QueryClientProvider>
        </Account>
      </TeamSelectionContext.Provider>
      <ToastContainer />
    </React.Fragment>
  );
};

export default App;
