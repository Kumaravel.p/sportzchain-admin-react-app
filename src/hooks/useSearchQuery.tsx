import * as React from 'react';
import { useLocation } from 'react-router-dom';

const useSearchQuery = () => {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
};

export default useSearchQuery;
