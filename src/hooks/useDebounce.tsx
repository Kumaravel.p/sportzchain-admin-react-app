import React from 'react';

export const useDebounce = () => {

    const [timeoutState, setTimeoutState] = React.useState<any>("");

    const debounce = (callback: Function, delay: number = 1000) => {
        clearTimeout(timeoutState);
        const timer = setTimeout(() => callback(), delay)
        setTimeoutState(timer)
    }

    return debounce
}