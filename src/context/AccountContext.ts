import { createContext } from 'react';

interface AccountContextProps {
  getSession: any;
  authenticate: any;
  logout: any;
}

export const AccountContext = createContext({} as AccountContextProps);
