import { Button, Card, Col, Divider, Input, Row, Upload, message } from 'antd';
import Text from 'antd/lib/typography/Text';
import * as React from 'react';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import styled from 'styled-components/macro';
import {
  useEditUserByIdMutation,
  useGetUserByIdQuery,
} from 'generated/graphql';
import { AutoComplete } from 'antd';
import { DatePicker, Space } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import moment from 'moment';
import UserPool from 'components/UserPool';

import { useHistory, useParams } from 'react-router-dom';
import TextArea from 'antd/lib/input/TextArea';
import { uploadFile } from 'apis/upload';
import { RcFile } from 'antd/lib/upload';
import config from 'config';
import { toast } from 'react-toastify';
import EditUserDetailsCard from './EditUserDetailsCard';

const EditPasswordCardWrapper = styled.div`
  margin: 40px 0;
  padding: 0 40px;
  .ant-card {
    width: 994px;
  }

  .basic-edit-wrapper {
  }
  .user-card-avatar {
    width: 114px;
    height: 114px;
    margin-right: 10px;
    border-radius: 121px;
  }
  .profile-status-wrapper {
    margin: 10px 0;
    align-items: center;
  }
  .t-title {
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
    color: #051f24;
    margin-bottom: 20px;
  }
  .t-user-key-field {
    font-size: 15px;
    line-height: 18px;
    color: rgba(5, 31, 36, 0.6);
    margin-bottom: 6px;
  }
  .t-user-value {
    font-size: 16px;
    font-weight: 600;
    line-height: 19px;
    color: #051f24;
  }
  .t-edit {
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    color: #151516;
  }
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.discard {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface EditPasswordCardProps { }

const EditPasswordCard = ({ }: EditPasswordCardProps): JSX.Element => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const [passwordDetails, setPasswordDetails] = React.useState<{
    oldPassword?: string;
    newPassword?: string;
    confirmNewPassword?: string;
  }>({});

  const onPasswordUpdate = () => {
    const cognitoUser = UserPool.getCurrentUser();
    console.log(cognitoUser, 'cu');
    if (!cognitoUser) {
      toast.error('No user found');
      return;
    }
    if (passwordDetails?.newPassword !== passwordDetails?.confirmNewPassword) {
      toast.error("Password's do not match");
      return;
    }

    if (passwordDetails.newPassword && passwordDetails.newPassword.length < 9) {
      toast.error('Length of password should be more than 8');
      return;
    }

    cognitoUser.getSession(function (err: any, session: any) {
      console.log(session, 'session');
      if (err) {
        alert(err.message || JSON.stringify(err));
        return;
      }

      if (!session) {
        toast.error('No user session found');
        return;
      }
      cognitoUser?.changePassword(
        passwordDetails.oldPassword ?? '',
        passwordDetails.newPassword ?? '',
        function (err, result) {
          if (err) {
            alert(err.message || JSON.stringify(err));
            return;
          }
          toast.success('Password updated successfully!');
          history.push('/');
        }
      );
    });
  };

  return (
    <EditPasswordCardWrapper>
      <EditUserDetailsCard />

      <Divider />
      <Card>
        <Row justify="space-between">
          <Col className="t-title">Login</Col>
          <Col>
            <Button
              className="cta-button save"
              onClick={() => {
                onPasswordUpdate();
              }}
            >
              Update Password
            </Button>
          </Col>
        </Row>
        <Row justify="space-between">
          <Col span={6}>
            <FlexColumnWrapper>
              <Text className="t-user-key-field">Old Password</Text>
              <Input
                placeholder="Old Password"
                style={{ width: 200 }}
                onChange={(e) =>
                  setPasswordDetails({
                    ...passwordDetails,
                    oldPassword: e.target.value,
                  })
                }
              />
            </FlexColumnWrapper>
          </Col>
          <Col span={6}>
            <FlexColumnWrapper>
              <Text className="t-user-key-field">New Password</Text>
              <Input
                placeholder="Password"
                style={{ width: 200 }}
                onChange={(e) =>
                  setPasswordDetails({
                    ...passwordDetails,
                    newPassword: e.target.value,
                  })
                }
              />
            </FlexColumnWrapper>
          </Col>
          <Col span={6}>
            <FlexColumnWrapper>
              <Text className="t-user-key-field">Confirm New Password</Text>
              <Input
                placeholder="Confirm new password"
                onChange={(e) =>
                  setPasswordDetails({
                    ...passwordDetails,
                    confirmNewPassword: e.target.value,
                  })
                }
              />
            </FlexColumnWrapper>
          </Col>
        </Row>
      </Card>
    </EditPasswordCardWrapper>
  );
};

export default EditPasswordCard;
