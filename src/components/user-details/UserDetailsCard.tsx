import styled from 'styled-components/macro';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import { Card, Col, Divider, Row } from 'antd';
import Text from 'antd/lib/typography/Text';

import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import LabelValue from 'components/common/LabelValue';

import defaultImage from 'assets/images/default_image.png';

const UserDetailsCardWrapper = styled.div`
  margin: 40px 0;
  .ant-card {
    width: 994px;
    height: 610px;
  }

  .user-card-avatar {
    width: 114px;
    height: 114px;
    border-radius: 121px;
  }
  .profile-status-wrapper {
    margin: 10px 0;
    align-items: center;
  }
  .t-basic-info {
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
    color: #051f24;
  }
  .t-user-key-field {
    font-size: 15px;
    line-height: 18px;
    color: rgba(5, 31, 36, 0.6);
    margin-bottom: 6px;
  }
  .t-user-value {
    font-size: 16px;
    font-weight: 600;
    line-height: 19px;
    color: #051f24;
  }
  .t-edit {
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    color: #151516;
  }
`;

interface UserDetailsCardProps {
  name?: string;
  bio?: string;
  dob?: Date | string;
  gender?: string;
  email?: string;
  imageURL?: string;
  status?: string;
  leaderboardRank?: string;
  location?: string;
  joinedAt?: string;
  lifetimeSPNValue?: number;
  lastActive?: string;
  mobile?: string;
  id: string;
}

const UserDetailsCard = ({
  name,
  email,
  id,
  gender,
  imageURL = defaultImage,
  leaderboardRank,
  joinedAt,
  bio,
  status,
  lastActive = '',
  lifetimeSPNValue,
  location,
  dob,
  mobile,
}: UserDetailsCardProps): JSX.Element => {
  return (
    <UserDetailsCardWrapper>
      <Card>
        <Row
          className="basic-edit-wrapper"
          gutter={[2, 1]}
          justify="space-between"
        >
          <Col className="t-basic-info">Basic Info</Col>

          <Col>
            <FlexRowWrapper>
              <Text className="t-edit">
                <Link to={`${id}/edit`}>Edit</Link>
              </Text>
            </FlexRowWrapper>
          </Col>
        </Row>

        <Row className="profile-status-wrapper" justify="space-between">
          <Col span={12}>
            <img
              className="user-card-avatar"
              src={!!imageURL ? imageURL : defaultImage}
            />
          </Col>

          <Col>
            <LabelValue field="Status" value={!!status ? status : '-'} />
          </Col>
        </Row>

        <Row>
          <Col span={9}>
            <LabelValue field="Name" value={!!name ? name : '-'} />
          </Col>

          <Col span={6}>
            <LabelValue field="Bio" value={!!bio ? bio : '-'} />
          </Col>
        </Row>

        <Divider />

        <Row justify="space-between">
          <Col span={6}>
            <LabelValue field="Gender" value={!!gender ? gender : '-'} />
          </Col>

          <Col span={6}>
            <LabelValue
              field="Date of Birth"
              value={dob ? format(new Date(dob), 'dd/MM/yyyy') : '-'}
            />
          </Col>

          <Col span={6}>
            <LabelValue field="Location" value={!!location ? location : '-'} />
          </Col>
        </Row>

        <Divider />

        <Row justify="space-between">
          <Col span={6}>
            <LabelValue field="Email Address" value={!!email ? email : '-'} />
          </Col>

          <Col span={6}>
            <LabelValue field="Phone number" value={!!mobile ? mobile : '-'} />
          </Col>

          <Col span={6}>
            <LabelValue
              field="Leaderboard Rank"
              value={!!leaderboardRank ? leaderboardRank : '-'}
            />
          </Col>
        </Row>

        <Divider />

        <Row justify="space-between">
          <Col span={6}>
            <LabelValue
              field="Joining Date"
              value={joinedAt ? format(new Date(joinedAt), 'dd/MM/yyyy') : '-'}
            />
          </Col>

          <Col span={6}>
            <LabelValue
              field="Last Activity"
              value={
                lastActive ? format(new Date(lastActive), 'dd/MM/yyyy') : '-'
              }
            />
          </Col>

          <Col span={6}>
            <LabelValue
              field="Lifetime value"
              value={lifetimeSPNValue ?? '-'}
            />
          </Col>
        </Row>
      </Card>
    </UserDetailsCardWrapper>
  );
};

export default UserDetailsCard;
