import { Button, Card, Col, Divider, Input, Row, Form, Upload } from 'antd';
import Text from 'antd/lib/typography/Text';
import * as React from 'react';
import { toast } from 'react-toastify';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import styled from 'styled-components/macro';
import {
  useEditUserByIdMutation,
  useGetUserByIdQuery,
  GetUserByIdQuery,
} from 'generated/graphql';
import { AutoComplete } from 'antd';
import { DatePicker, Space } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import moment from 'moment';

import { useHistory, useParams } from 'react-router-dom';
import { RcFile } from 'antd/lib/upload';
import defaultImage from 'assets/images/default_image.png';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import TextArea from 'antd/lib/input/TextArea';
import SelectElement from 'components/common/SelectElement';
import LabelValue from 'components/common/LabelValue';
import LabelInput from 'components/common/LabelInput';
import TitleCta from 'components/common/TitleCta';

const EditUserDetailsCardWrapper = styled.div`
  margin: 40px 0;
  .ant-card {
    width: 994px;
    height: 610px;
  }

  .user-card-avatar {
    width: 114px;
    height: 114px;
    margin-right: 10px;
    border-radius: 121px;
  }
  .profile-status-wrapper {
    margin: 10px 0;
    align-items: center;
  }
  .t-basic-info {
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
    color: #051f24;
  }
  .t-user-key-field {
    font-size: 15px;
    line-height: 18px;
    color: rgba(5, 31, 36, 0.6);
    margin-bottom: 6px;
  }
  .t-user-value {
    font-size: 16px;
    font-weight: 600;
    line-height: 19px;
    color: #051f24;
  }
  .t-edit {
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    color: #151516;
  }
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.discard {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface EditUserDetailsCardProps { }

// dropdown contraints, using enums to limit inputs

enum gender {
  MALE = 'Male',
  FEMALE = 'Female',
  NON_BINARY = 'Non-binary',
}

enum status {
  ACTIVE = 'Active',
  INACTIVE = 'Inactive',
  SUSPENDED = 'Suspended',
}

const EditUserDetailsCard = ({ }: EditUserDetailsCardProps): JSX.Element => {
  const [form] = Form.useForm();
  const { id } = useParams<{ id: string }>();
  const history = useHistory();
  const { data: userDetails } = useGetUserByIdQuery({
    variables: { userId: +id },
  });

  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [edittedUser, setEdittedUser] = React.useState<
    GetUserByIdQuery['user_by_pk'] | any
  >(userDetails?.user_by_pk);

  const [editUserDetails] = useEditUserByIdMutation();

  const editUser = async () => {
    const { bio, gender, dob, location, name, status, imageURL } =
      edittedUser ?? {};
    setConfirmLoading(true);

    await editUserDetails({
      variables: {
        bio,
        userId: +id,
        gender,
        dob,
        location,
        name,
        status,
        imageURL,
      },
    });

    setConfirmLoading(false);
    history.push(`/user/${id}`);
    toast.success(`Update user: ${name}`);
  };

  return (
    <EditUserDetailsCardWrapper>
      <Card>
        <TitleCta
          title="Basic Info"
          cta={
            <>
              <Button
                className="cta-button save"
                disabled={confirmLoading}
                onClick={() => editUser()}
              >
                {confirmLoading ? 'Updating...' : 'Save Details'}
              </Button>

              <Button
                onClick={() => history.push(`/user/${id}`)}
                className="cta-button discard"
                disabled={confirmLoading}
              >
                Discard
              </Button>
            </>
          }
        />

        <Form onFinish={editUser} form={form} preserve={false}>
          <Row className="profile-status-wrapper" justify="space-between">
            <Col span={12}>
              <img
                className="user-card-avatar"
                src={
                  edittedUser?.imageURL ? edittedUser?.imageURL : defaultImage
                }
                alt="user"
              />
              <Upload
                iconRender={() => <></>}
                customRequest={({ file, onProgress }) =>
                  handleMediaUpload(
                    file as RcFile,
                    onProgress
                  )
                }
              >
                <Button icon={<UploadOutlined />}>Click to Upload</Button>
              </Upload>
            </Col>
          </Row>

          <Row justify="space-between">
            <Col span={5}>
              <LabelInput
                label="Name"
                inputElement={
                  <Input
                    placeholder="Name"
                    style={{ width: 200 }}
                    defaultValue={edittedUser?.name ?? ''}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      setEdittedUser({ ...edittedUser, name: e.target.value })
                    }
                  />
                }
              />
            </Col>
            <Col span={6}>
              <LabelInput
                label="Bio"
                name="bio"
                initialValue="bio"
                inputElement={
                  <TextArea
                    placeholder="Bio"
                    defaultValue={edittedUser?.bio}
                    onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
                      setEdittedUser({ ...edittedUser, bio: e.target.value })
                    }
                  />
                }
              />
            </Col>
            <Col>
              <LabelInput
                label="Status"
                name="status"
                initialValue={edittedUser.status}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      setEdittedUser({ ...edittedUser, status: e })
                    }
                    placeholder="Status"
                    defaultValue={edittedUser.status}
                    toFilter="value"
                    options={[
                      { value: status.ACTIVE },
                      { value: status.INACTIVE },
                      { value: status.SUSPENDED },
                    ]}
                  />
                }
              />
            </Col>
          </Row>

          <Divider />
          <Row justify="space-between">
            <Col span={6}>
              <LabelInput
                label="Gender"
                name="gender"
                initialValue={edittedUser.gender}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      setEdittedUser({ ...edittedUser, gender: e })
                    }
                    placeholder="Gender"
                    defaultValue={edittedUser.gender}
                    toFilter="value"
                    options={[
                      { value: gender.MALE },
                      { value: gender.FEMALE },
                      { value: gender.NON_BINARY },
                    ]}
                  />
                }
              />
            </Col>

            <Col span={6}>
              <LabelInput
                label="Date of Birth"
                // initialValue={moment(edittedUser?.dob)}
                inputElement={
                  <DatePicker
                    format="DD-MM-YYYY"
                    defaultValue={moment(edittedUser?.dob)}
                    onChange={(e: any) => {
                      setEdittedUser({ ...edittedUser, dob: e?._d });
                    }}
                  />
                }
              />
            </Col>
            <Col span={6}>
              <LabelInput
                label="Location"
                inputElement={
                  <Input
                    placeholder="Location"
                    defaultValue={edittedUser?.location ?? ''}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                      setEdittedUser({
                        ...edittedUser,
                        location: e.target.value,
                      })
                    }
                  />
                }
              />
            </Col>
          </Row>
        </Form>
      </Card>
    </EditUserDetailsCardWrapper >
  );
};

export default EditUserDetailsCard;
