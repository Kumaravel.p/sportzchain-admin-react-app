import { Card } from 'antd';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';

const UserStatsWrapper = styled.div`
  .cards-wrapper {
    justify-content: space-between;
    .count-monthly-wrapper {
      column-gap: 10px;
      align-items: flex-end;
      .t-stat-price {
        font-size: 24px;
        line-height: 28px;
        color: #4da1ff;
        &.red {
          color: #ff6d4a;
        }
      }
    }
  }
`;

interface UserStatsProps {
  totalHoldingValue?: number;
  totalLoyaltyPoints?: number;
  totalSpnTokens?: number;
}

const UserStats = ({
  totalHoldingValue = 0,
  totalLoyaltyPoints = 0,
  totalSpnTokens = 0,
}: UserStatsProps): JSX.Element => {
  return (
    <UserStatsWrapper>
      <FlexRowWrapper className="cards-wrapper">
        <Card
          title="Total Holding Value"
          // extra={<MoreIcon />}

          style={{ width: 300 }}
        >
          <FlexRowWrapper className="count-monthly-wrapper">
            {/* <FlexRowWrapper> */}
            <Text className="t-stat-price">${totalHoldingValue}</Text>

            <Text className="t-month">Gas Fees: $90 </Text>
          </FlexRowWrapper>
        </Card>
        <Card
          title="Loyalty Points"
          // extra={<MoreIcon />}

          style={{ width: 300 }}
        >
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-stat-price red">
              {totalLoyaltyPoints || 0} RWRD
            </Text>
          </FlexRowWrapper>
        </Card>{' '}
        <Card
          title="SPN tokens"
          // extra={<MoreIcon />}
          style={{ width: 300 }}
        >
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-stat-price">{totalSpnTokens} SPN</Text>
          </FlexRowWrapper>
        </Card>
      </FlexRowWrapper>
    </UserStatsWrapper>
  );
};

export default UserStats;
