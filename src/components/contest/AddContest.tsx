import * as React from 'react';
import styled from 'styled-components/macro';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import {
  Form,
  Row,
  Card,
  Button,
  Typography,
  Col,
  Input,
  DatePicker,
  InputNumber,
} from 'antd';
import config from 'config';
import {
  WinningForm,
  RewardsForm,
  QuestionsWrapperModal,
  QuestionsFormCommon,
  MatchCard,
  ManOfMatchForm,
} from 'components/contests';
import {
  filterQuestionData,
  questionTableManagement,
  rankBasedRewardDataSource,
} from 'components/contests/data';
import { useGetTeamsByStatusIdLazyQuery } from 'generated/graphql';
import { toast } from 'react-toastify';
import SelectElement from 'components/common/SelectElement';
import LabelInput from 'components/common/LabelInput';
import { TemplateWrapper, optionsInitialState } from '../contests';

import BreadcrumbComp from 'components/common/breadcrumb';

import { createContestApi, updateContestApi, updateOptionAnswer } from 'apis/contest';

import moment, { MomentInput } from 'moment';
import { useGetAllGroupsByStatusIdLazyQuery, useGetAllRequiredContestOptionsQuery } from 'generated/pgraphql';
import { useGetContestDetailsQuery } from 'generated/pgraphql';
import { categoryOptions, pollFilterStatus } from 'utils';
import { TeamSelectionContext } from 'App';
import { getAllContests } from 'apis/contest';
import currentSession from 'utils/getAuthToken';
import { PlusOutlined } from '@ant-design/icons';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';
import { battlegroundList } from '../../apis/contest';

const AddContestWrapper = styled.div`
  padding: 20px 20px;
  width: 100%;
  background: rgba(245, 245, 247, 1);
  .breadcumb-btn-mg {
    margin-right: 15px;
  }
  .ant-card {
    margin-top: 40px;
    width: 100%;
  }
  .ant-form {
    width: 100%;
  }
  .datepicker {
    width: 100%;
  }
  & .header {
    color: #051f24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
  }
  & .sub-header {
    color: #051f24;
    font-weight: 600;
    font-size: 16px;
    margin-bottom: 10px;
  }
  & .body1 {
    color: #051f24;
    font-size: 14px;
    font-weight: 500;
    margin-bottom: 10px;
  }
`;

interface AddContestProps { }

const CommonRadioOptions = {
  ALL: 'ALL',
  SELECTED: 'SELECTED',
  NONE: 'NONE',
};

const authDetails = {
  userId: null,
  name: ""
}

const AddContest = ({ }: AddContestProps): JSX.Element => {
  const [teamValue, setTeamValue] = React.useState<any>('');
  const [coinSymbol, setcoinSymbol] = React.useState<any>('');
  const [pollType, setPollType] = React.useState<any>(1);
  const [contestType, setContestType] = React.useState<any>('Quiz');
  const [matchData, setMatchData] = React.useState<any>();
  const [categoryType, setCategoryType] = React.useState<any>("");
  const [predictQuestions, setPredictQuestions] = React.useState<any>({
    option: optionsInitialState(''),
  });
  const { state } = useLocation<any>();
  const [manOfMatchQuestion, setManOfMatchQuestion] = React.useState<any>({
    question: "Predict Man of the Match",
    team1: {
      options: optionsInitialState('')
    },
    team2: {
      options: optionsInitialState('')
    }
  });

  const { profile } = useSnapshot(ProxyState);

  let team: any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  const questionsDataRef: any = React.useRef();
  const questionsDataOrgRef: any = React.useRef(); /// Original questions data
  const totalRewardsRef: any = React.useRef();
  const answerListRef: any = React.useRef([]);
  const params = useParams<{ contestId: string }>();
  const isTeamUser = isIamTeamUser();
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  const [winnerSelection, setWinnerSelection] = React.useState("ALL");
  const [rewardType, setRewardType] = React.useState('EQUAL_REWARD');
  const [noOfWinners, setNoOfWinners] = React.useState<number>(0);
  const [formData, setFormData] = React.useState<object>({});
  const [isVisible, setIsVisible] = React.useState<boolean>(false);
  const [notesList, setNotesList] = React.useState<any[]>([]);
  const [editIndex, setEditIndex] = React.useState<number>(-1);
  const [checkStatus, setCheckStatus] = React.useState<boolean>(false);
  const [battleground, setBattleground] = React.useState<any>();

  const [result, setResult] = React.useState<any>();
  const {
    data: contestDetailsGql,
    loading: contestDataLoading,
  } = useGetContestDetailsQuery({
    variables: { id: params?.contestId },
    skip: !!!params?.contestId,
    fetchPolicy: 'no-cache',
  });

  const [form] = Form.useForm();

  const history = useHistory();

  // const [getAllTeams, { data: teams }] = useGetTeamsLazyQuery();

  //team listing
  const [getAllTeams, { data: teams, loading: teamOptionLoading }] = useGetTeamsByStatusIdLazyQuery({
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    variables: {
      statusId: ["APPROVED", "PUBLISHED", "ACTIVE"]
    }
  });
  console.log(teams)

  //group listing
  const [getAllGroups, { data: groups, loading: groupOptionLoading }] = useGetAllGroupsByStatusIdLazyQuery({
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    variables: {
      statusID: [1]
    }
  });
  // const [getAllBattleground, { data: battlegrounds, loading: battleOptionLoading }] = useGetAllGroupsByStatusIdLazyQuery({
  //   fetchPolicy: "network-only",
  //   notifyOnNetworkStatusChange: true,
  //   variables: {
  //     statusID: [1]
  //   }
  // });


  const [totalContest, setTotalContest] = React.useState();
  const {
    data: allRequiredOptions,
    loading: requiredOptionsLoading,
  } = useGetAllRequiredContestOptionsQuery();

  React.useEffect(() => {
    (async () => {
      // get position count
      let contestdata = await getAllContests({ length: 0, start: 0 });
      if (contestdata?.data?.statusCode === '200') {
        setTotalContest(contestdata?.data?.data?.list?.allContests?.totalCount);
        form.setFieldsValue({
          orderBy: contestdata?.data?.data?.list?.allContests?.totalCount,
        });
      }
      ///
    })();
  }, []);


  React.useEffect(() => {
    battlegroundList()
      .then((res: any) => {
        const data_ = res?.data?.data?.list
        if (res?.data?.statusCode === '200') {
          const datas = data_?.map((v: any) => {
            return {
              key: v?.id,
              value: v?.id,
              label: v?.title,
              id: v?.title,
            }
          })
          setBattleground(datas);
        } else {
          toast.error(res?.data?.message)
        }
      })
      .catch((err: any) => {
        toast.error(err);
      });
  }, [])

  React.useEffect(() => {
    if (categoryType === "GROUPS" && !groups?.allGroups?.nodes) {
      getAllGroups();
    }
  }, [categoryType])

  // React.useEffect(() => {
  //   if (categoryType === "BATTLEGROUND" && !battleground) {
  //     getAllBattleground();
  //   }
  // }, [categoryType])

  React.useEffect(() => {
    if (state?.teamId) {
      setCategoryType("PLATFORM")
      if (teams?.team?.length) {
        let teamsData = filterSelectedTeam(teams, state?.teamId);
        form.setFieldsValue({
          teamId: {
            key: state?.teamId?.toString(),
            value: state?.teamId,
            label: teamsData?.name
          },
          category: "PLATFORM"
        })
        var coinSymbolCurrent = teamsData?.wallets[0]?.coinSymbol ?? "";
        setTeamValue({
          label: teamsData?.name,
          key: state?.teamId?.toString(),
          value: state?.teamId,
          coinSymbol: coinSymbolCurrent,
        });
      }
    }
  }, [state, teams])

  // React.useEffect(() => {
  //   if (battleground?.length > 0) {
  //     var seletedbattle = battleground?.filter((item: any) => item?.value === form.getFieldsValue()?.battlegroundId)?.[0] ?? []
  //     form.setFieldsValue({
  //       battlegroundId: seletedbattle,
  //       category: "BATTLEGROUND",
  //     })
  //   }


  // }, [form.getFieldsValue()?.battlegroundId])


  const onSave = async (formValues: any) => {

    const isPredict = contestType === 'Predict' ? true : false;
    if (isForceAnswer && isPredict) {
      updatePredictFormAnswerApi(formValues?.teamId?.value || result?.teamId);
      return
    }
    if (!questionsDataRef?.current?.length && !isPredict) {
      toast.error(`No Questions Found`);
      return;
    }

    //construct notes payload
    let contructNotes: any[] = [];

    if (notesList?.length) {
      contructNotes = notesList?.map(note => ({
        "addedDate": note?.addedDate,
        "name": note?.name,
        "notes": note?.notes,
        "addedBy": note?.addedby
      }))
    }

    const formObject: any = {
      // contest details
      type: formValues?.category,
      ...(formValues?.category === "GROUPS" ? {
        groupId: (formValues?.groupId?.value || result?.groupId)
      } : formValues?.category === "BATTLEGROUND" ? {
        battlegroundId: formValues?.battlegroundId?.value || result?.battlegroundId
      } : {
        teamId: (formValues?.teamId?.value || result?.teamId)
      }),
      title: formValues?.title,
      contestTypeId: formValues?.contestTypeId,
      platformTypeId: formValues?.platformTypeId,
      startAt: formValues?.startAt.seconds('0o0').toISOString(),
      endAt: formValues?.endAt.seconds('0o0').toISOString(),
      displayStartAt: formValues?.displayStartAt.seconds('0o0').toISOString(),
      displayEndAt: formValues?.displayEndAt.seconds('0o0').toISOString(),
      description: formValues?.description,
      orderBy: formValues?.orderBy,
      // participation criteria
      minJoinBalance: formValues?.minJoinBalance,
      noParticipantAllowed: formValues?.noParticipantAllowed,
      // winnner definition
      winnerEligibleCriteria: formValues?.winnerEligibleCriteria,
      winnerSelectionCriteria: formValues?.winnerSelectionCriteria,
      noOfWinners: formValues?.noOfWinners,
      noOfQuestionAnswerCorrectly: formValues?.noOfQuestionAnswerCorrectly,
      winnerBy: formValues?.winnerBy,
      coinSymbol: formValues?.coinSymbol,
      templateId: pollType,
      /// reward form data

      reward: {
        rewardApplicable: formValues?.rewardApplicable,
        rewardType: formValues?.rewardType,
        rewardPerWinner: formValues?.rewardPerWinner,
        perRewardValue: formValues?.perRewardValue,
        bonusReward: formValues?.bonusReward,
        winnerSplitup:
          formValues?.rewardType === 'RANKING'
            ? totalRewardsRef?.current?.data ?? []
            : [],
      },

      ///questions wrapper
      // question: isPredict ? [predictQuestions] : questionsDataRef.current,
      notes: contructNotes
    };


    /*
      * poll type 3 use seperate state than all other predict type template questions
      * predict and question handle seperate state management
      */
    if (pollType === 3) {
      formObject['question'] = [{
        ...manOfMatchQuestion,
        team1: {
          name: matchData?.tournamentGroupMemberByParticipant1?.teamName,
          ...manOfMatchQuestion.team1,
        },
        team2: {
          name: matchData?.tournamentGroupMemberByParticipant2?.teamName,
          ...manOfMatchQuestion.team2,
        }
      }];
    } else {
      formObject['question'] = isPredict ? JSON.parse(JSON.stringify([predictQuestions])) : questionsDataRef.current;
    }

    //answer type and question type for what happen next
    if (pollType === 4 && formObject?.question?.[0]) {
      formObject.question[0].questionType = predictQuestions?.uploadQuestionType
      formObject.question[0].answerType = predictQuestions?.uploadAnswerType
      delete formObject?.question?.[0]?.uploadQuestionType
      delete formObject?.question?.[0]?.uploadAnswerType
    }

    if (formValues?.rewardType === 'RANKING') {
      formObject.reward['rewardValue'] = formValues?.rewardValue;
      formObject.reward['rewardGivenFor'] = formValues?.rewardGivenFor;
    }

    let response: any = null;
    if (totalRewardsRef?.current?.value) {
      formObject.reward['totalWinnerRewards'] = totalRewardsRef?.current?.value ?? '';
    }
    if (totalRewardsRef?.current?.quantity) {
      formObject.reward['totalQuantityReward'] =
        totalRewardsRef?.current?.quantity ?? '';
    }
    if (isPredict) {
      formObject['matchDetail'] = {
        matchId: formValues?.matchId?.value || formValues?.matchId,
        leagueId: formValues?.leagueId,
        team1Color: matchData?.team1Color,
        team2Color: matchData?.team2Color,
      };
    }
    if (params?.contestId) {
      ///set UUID on Edit
      formObject['statusId'] = formValues?.statusId;
      formObject['contestId'] = params?.contestId;
      response = await updateContestApi(formObject);
    } else {
      response = await createContestApi(formObject);
    }
    if (response?.data?.statusCode === '200') {
      toast.success(response?.data?.message ?? `contest created successfully`);
      if (params?.contestId) {
        history.push(`/contests/${formObject?.teamId}/${params?.contestId}`)
      } else {
        history.push(`/contests/${formObject?.teamId}/${response?.data?.data?.createdContest?.createContest?.contest?.id}`)
      }
    } else {
      toast.error(response?.data?.message ?? `Some Error Occurred`);
    }
  };

  const onPreview = (formValues: any) => {
    if (formValues?.contestTypeId === 'Predict') {
      //  updatePredictFormAnswerApi();
      const formObject: any = {
        // contest details
        type: formValues?.category,
        ...(formValues?.category === "GROUPS" ? {
          groupId: (formValues?.groupId?.value || result?.groupId)
        } : formValues?.category === "BATTLEGROUND" ? {
          battlegroundId: formValues?.battlegroundId?.value || result?.battlegroundId
        } : {
          teamId: (formValues?.teamId?.value || result?.teamId)
        }),
        // groupId:formValues?.category === "GROUPS" ? (formValues?.groupId?.value || result?.groupId) : null,
        // teamId: (formValues?.teamId?.value || result?.teamId) : null,
        title: formValues?.title,
        contestTypeId: formValues?.contestTypeId,
        platformTypeId: formValues?.platformTypeId,
        startAt: moment(formValues?.startAt).toISOString(),
        endAt: moment(formValues?.endAt).toISOString(),
        displayStartAt: moment(formValues?.displayStartAt).toISOString(),
        displayEndAt: moment(formValues?.displayEndAt).toISOString(),
        description: formValues?.description,
        orderBy: formValues?.orderBy,
        // participation criteria
        minJoinBalance: formValues?.minJoinBalance,
        noParticipantAllowed: formValues?.noParticipantAllowed,
        // winnner definition
        winnerEligibleCriteria: formValues?.winnerEligibleCriteria,
        winnerSelectionCriteria: formValues?.winnerSelectionCriteria,
        noOfWinners: formValues?.noOfWinners,
        noOfQuestionAnswerCorrectly: formValues?.noOfQuestionAnswerCorrectly,
        winnerBy: formValues?.winnerBy,
        coinSymbol: formValues?.coinSymbol,
        templateId: pollType,
        /// reward form data
        contestRewardsByContestId: {
          "nodes": [
            {
              "rewardApplicable": formValues?.rewardApplicable,
              "rewardType": formValues?.rewardType,
              "rewardPerWinner": formValues?.rewardPerWinner,
              "perRewardValue": formValues?.perRewardValue,
              "rewardValue": formValues?.rewardValue,
              "totalWinnerRewards": totalRewardsRef?.current?.value,
              "totalQuantityReward": totalRewardsRef?.current?.quantity,
              "bonusReward": formValues?.bonusReward,
              "rewardGivenFor": formValues?.rewardGivenFor,
              "contestRewardWinnerSplitsByContestRewardId": {
                "nodes": formValues?.rewardType === 'RANKING'
                  ? totalRewardsRef?.current?.data ?? []
                  : [],
              },

            }
          ],
        },
        ///questions wrapper
        totalQuantityReward: totalRewardsRef?.current?.quantity,
        contestMatchDetailsByContestId: {
          "nodes": [
            {
              "contestId": params?.contestId,
              "id": matchData?.id,
              matchId: formValues?.matchId?.value || formValues?.matchId,
              leagueId: formValues?.leagueId,
              team1Color: matchData?.team1Color,
              team2Color: matchData?.team2Color,
              "matchByMatchId": {
                "location": matchData?.location,
                "name": matchData?.name,
                "participant1": matchData?.participant1,
                "participant2": matchData?.participant2,
                "startDate": matchData?.startDate,
                "endDate": matchData?.startDate,
                "tournamentGroupMemberByParticipant1": matchData?.tournamentGroupMemberByParticipant1,
                "tournamentGroupMemberByParticipant2": matchData?.tournamentGroupMemberByParticipant2
              }
            }
          ],
        },
        templateName: {
          "name": formValues['question-master']
        },
      };
      formObject.teamByTeamId = {
        "id": formValues?.teamId?.value,
        "name": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.name,
        "profileImageUrl": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.profileImageUrl,
        "currencyTypeId": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.currencyTypeId,
        "categoryId": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.categoryId
      }
      if (predictQuestions?.imageUrl) {
        formObject.question = [
          {
            "imageUrl": predictQuestions?.imageUrl,
            "question": formValues['question-master'],
            "orderBy": formValues['orderBy'],
            "canMultiSelect": false,
            "option": predictQuestions?.option
          }
        ]
      }
      else if (predictQuestions?.teamImageUrl1) {
        formObject.question = [
          {
            "teamImageUrl1": predictQuestions?.teamImageUrl1,
            "teamImageUrl2": predictQuestions?.teamImageUrl2,
            "question": formValues['question-master'],
            "orderBy": formValues['orderBy'],
            "canMultiSelect": false,
            "option": predictQuestions?.option
          }
        ]
      }
      else {
        formObject.question = [
          {
            "question": formValues['question-master'],
            "orderBy": formValues['orderBy'],
            "canMultiSelect": false,
            "option": {
              team1Name: manOfMatchQuestion.team1.options,
              team2Name: manOfMatchQuestion.team2.options
            }
          }
        ]
      }
      if (formObject) {
        window.open(`${config.sportzchainFanPageURL}/contest-preview?formValues=${btoa(JSON.stringify({ ...formObject }))}`)
      }
    }
    else {
      const formObject: any = {
        // contest details
        type: formValues?.category,
        ...(formValues?.category === "GROUPS" ? {
          groupId: (formValues?.groupId?.value || result?.groupId)
        } : formValues?.category === "BATTLEGROUND" ? {
          battlegroundId: formValues?.battlegroundId?.value || result?.battlegroundId
        } : {
          teamId: (formValues?.teamId?.value || result?.teamId)
        }),
        // groupId:formValues?.category === "GROUPS" ? (formValues?.groupId?.value || result?.groupId) : null,
        // teamId: (formValues?.teamId?.value || result?.teamId) : null,
        title: formValues?.title,
        contestTypeId: formValues?.contestTypeId,
        platformTypeId: formValues?.platformTypeId,
        startAt: moment(formValues?.startAt).toISOString(),
        endAt: moment(formValues?.endAt).toISOString(),
        displayStartAt: moment(formValues?.displayStartAt).toISOString(),
        displayEndAt: moment(formValues?.displayEndAt).toISOString(),
        description: formValues?.description,
        orderBy: formValues?.orderBy,
        // participation criteria
        minJoinBalance: formValues?.minJoinBalance,
        noParticipantAllowed: formValues?.noParticipantAllowed,
        // winnner definition
        winnerEligibleCriteria: formValues?.winnerEligibleCriteria,
        winnerSelectionCriteria: formValues?.winnerSelectionCriteria,
        noOfWinners: formValues?.noOfWinners,
        noOfQuestionAnswerCorrectly: formValues?.noOfQuestionAnswerCorrectly,
        winnerBy: formValues?.winnerBy,
        coinSymbol: formValues?.coinSymbol,
        templateId: pollType,
        /// reward form data
        contestRewardsByContestId: {
          "nodes": [
            {
              "rewardApplicable": formValues?.rewardApplicable,
              "rewardType": formValues?.rewardType,
              "rewardPerWinner": formValues?.rewardPerWinner,
              "perRewardValue": formValues?.perRewardValue,
              "rewardValue": formValues?.rewardValue,
              "totalWinnerRewards": totalRewardsRef?.current?.value,
              "totalQuantityReward": totalRewardsRef?.current?.quantity,
              "bonusReward": formValues?.bonusReward,
              "rewardGivenFor": formValues?.rewardGivenFor,
              "contestRewardWinnerSplitsByContestRewardId": {
                "nodes": formValues?.rewardType === 'RANKING'
                  ? totalRewardsRef?.current?.data ?? []
                  : [],
              },

            }
          ],
        },
        ///questions wrapper
        totalQuantityReward: totalRewardsRef?.current?.quantity,
      };

      if (formValues?.rewardType === 'RANKING') {
        formObject.reward['rewardValue'] = formValues?.rewardValue;
        formObject.reward['rewardGivenFor'] = formValues?.rewardGivenFor;
      }
      formObject['contestQuestionsByContestId'] = {
        "nodes": [
          {
            "canMultiSelect": questionsDataRef?.current[0]?.canMultiSelect,
            "imageUrl": questionsDataRef?.current[0]?.imageUrl,
            "question": questionsDataRef?.current[0]?.question,
            "isActive": true,
            "contestQuestionOptionsByContestQuestionId": {
              "nodes": questionsDataRef?.current[0]?.option
            },
          }
        ],
      }
      formObject['statusId'] = formValues?.statusId;
      formObject['contestId'] = params?.contestId;
      formObject.teamByTeamId = {
        "id": formValues?.teamId?.value,
        "name": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.name,
        "profileImageUrl": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.profileImageUrl,
        "currencyTypeId": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.currencyTypeId,
        "categoryId": filterSelectedTeam(teams, formValues?.teamId?.value || result?.teamId)?.categoryId
      }


      if (formObject) {
        window.open(`${config.sportzchainFanPageURL}/contest-preview?formValues=${btoa(JSON.stringify({ ...formObject }))}`)
      }
    }
    // const isPredict = contestType === 'Predict' ? true : false;
    // if (isForceAnswer && isPredict) {
    //   updatePredictFormAnswerApi();
    //   history.push(`/contests/${formValues?.teamId?.value || result?.teamId}/${params?.contestId}`)
    //   return
    // }
    // if (!questionsDataRef?.current?.length && !isPredict) {
    //   toast.error(`No Questions Found`);
    //   return;
    // }

  }

  React.useEffect(() => {
    totalRewardsRef.current = {
      data: rankBasedRewardDataSource,
    };

    if (!isTeamUser) {
      getAllTeams();
    } else {
      form.setFieldsValue({ teamId: teamSelectionContext.team.id });
    }
    setPredictQuestions({
      option: optionsInitialState(''),
    })
  }, []);

  React.useEffect(() => {
    if (contestDetailsGql?.contestById) {
      let data: any = contestDetailsGql?.contestById;
      let questions: any[] = [];
      if (data?.contestTypeId === 'Quiz') {
        questions = questionTableManagement(
          data?.contestQuestionsByContestId?.nodes,
          0
        );
        questionsDataOrgRef.current = filterQuestionData(
          data?.contestQuestionsByContestId?.nodes,
          0
        );
      } else if (data?.contestTypeId === 'Predict') {
        questions = questionTableManagement(data, data?.templateId);
        questionsDataOrgRef.current = filterQuestionData(data, data?.templateId);
        setPollType(data?.templateId);

        let descriptionData: any = {};
        // man of match special addon
        if (data?.templateId !== 3) {
          setPredictQuestions({
            ...questions[0],
          });
          questions?.[0]?.option?.forEach((item: any, index: any) => {
            let key = `description${index}`;
            let value = item?.description;
            descriptionData[key] = value;
          });
        } else {
          setManOfMatchQuestion({
            ...questions[0],
          });
          questions?.[0]?.team1?.options?.forEach((item: any, index: any) => {
            let key = `description_team1_${index}`;
            let value = item?.description;
            descriptionData[key] = value;
          });
          questions?.[0]?.team2?.options?.forEach((item: any, index: any) => {
            let key = `description_team2_${index}`;
            let value = item?.description;
            descriptionData[key] = value;
          });
        }
        let matchData = data?.contestMatchDetailsByContestId?.nodes[0];
        setMatchData({
          ...matchData?.matchByMatchId,
          team1Color: matchData?.team1Color,
          team2Color: matchData?.team2Color,
        });
        form.setFieldsValue({
          leagueId: matchData?.leagueId,
          matchId: matchData?.matchId,
          team1Color: matchData?.team1Color,
          team2Color: matchData?.team2Color,
          ...descriptionData,
        });
      }
      let result = {
        ...data,
        ...data?.contestRewardsByContestId?.nodes[0],
        question: questions,
      };
      setNoOfWinners(result?.noOfWinners);
      setWinnerSelection(result?.winnerSelectionCriteria ?? "ALL");
      setRewardType(result?.rewardType ?? 'EQUAL_REWARD');
      setCategoryType(data.type ?? "PLATFORM")
      /// handle selected symbol logic
      setcoinSymbol(data?.coinSymbol ?? null);
      setContestType(data?.contestTypeId);
      // set require existing field data
      form.setFieldsValue({
        ...data,
        ...(data?.groupId && {
          groupId: {
            key: data?.groupId?.toString(),
            value: data?.groupId,
            label: data?.groupByGroupId?.groupName
          }
        }),
        category: data.type ?? "PLATFORM",
        startAt: data?.startAt ? moment(data?.startAt.concat('z')) : undefined,
        endAt: data?.endAt ? moment(data?.endAt.concat('z')) : undefined,
        displayStartAt: data?.displayStartAt
          ? moment(data?.displayStartAt.concat('z'))
          : undefined,
        displayEndAt: data?.displayEndAt
          ? moment(data?.displayEndAt.concat('z'))
          : undefined,
        statusId: data?.statusId,
        description: data?.description,
        ...data?.contestRewardsByContestId?.nodes[0],
        orderBy: data?.orderBy ? data?.orderBy : totalContest,
        ...(data?.teamId && {
          teamId: {
            key: data?.teamId?.toString(),
            value: data?.teamId,
            label: data?.teamByTeamId?.name
          }
        })
      });
      questionsDataRef.current = questions;

      setResult(result);
      if (data?.contestNotesByContestId?.nodes?.length) {
        let contructNotesList = data?.contestNotesByContestId?.nodes?.map((note: any) => ({
          name: note?.name,
          notes: note?.notes,
          addedDate: moment(note?.createdDate.concat('z')),
          addedby: note?.addedBy,
          id: note?.id,
        }))
        setNotesList(contructNotesList)
      }
    }
  }, [contestDataLoading]);

  const onQuestionsFormSubmit = (questionsData: any) => {
    questionsDataRef.current = questionsData;
  };

  const handleTotalRewardsMaster = (record: any) => {
    totalRewardsRef.current = record;
  };

  const handlePredictQuestionsChange = (keyName: any, value: any) => {
    if (!value && typeof keyName !== "string") {
      setPredictQuestions({
        ...predictQuestions,
        ...keyName
      });
    }
    else {
      setPredictQuestions({
        ...predictQuestions,
        [keyName]: value,
      });
    }
  };

  const handleManOfMatchForm = (keyName: any, value: any) => {
    let data = manOfMatchQuestion;
    if (keyName === "answer") {
      setManOfMatchQuestion({ ...data, ...value })
    }
    else {
      data[keyName] = value;
      setManOfMatchQuestion({ ...data })
    }
  }


  React.useEffect(() => {
    (async () => {
      if (profile) {
        let userSession = await currentSession();

        authDetails.userId = profile?.user_id;
        authDetails.name = userSession?.payload?.name;
      }
    })()
  }, [profile]);
  console.log(profile, authDetails);

  React.useEffect(() => {
    if (checkStatus) {
      onSave(form.getFieldsValue())
    }
  }, [notesList])

  const handleMatchChange = (matchRecord: any) => {
    setMatchData({
      ...matchData,
      ...matchRecord,
    });
  };

  const filterSelectedTeam = (teams: any, teamId: number) => {
    return (
      teams?.team?.filter((item: any) => item?.id === teamId)?.[0] ?? []
    );
  };
  const filterSelectedBattle = (battle: any) => {
    return (
      battleground?.filter((item: any) => item?.id === battle?.value)?.[0] ?? []
    );
  };

  // multi option update fallback
  const updatePredictFormAnswer = async (value: any, index: number, teamType?: string) => {
    let questions = questionsDataOrgRef?.current?.[0];
    let optionId = questions?.option?.[index]?.id;
    //man of the match
    if (pollType === 3) {
      if (teamType) {
        optionId = manOfMatchQuestion[teamType]?.options[index]?.id
      }
      else {
        toast.error('teamType is missing!');
        return false
      }
    }
    if (value) {
      if (pollType === 6) {
        answerListRef.current = [{
          isAnswer: true,
          description: value,
          optionId: optionId
        }]
      }
      else {
        answerListRef.current = [{
          isAnswer: value,
          optionId: optionId
        }]
      }
    }
  }

  const updatePredictFormAnswerApi = async (teamId: number) => {
    if (!params?.contestId) return
    let questions = questionsDataOrgRef?.current[0];
    if (questions?.id && answerListRef?.current?.length) {
      let payload = {
        "contestId": params?.contestId,
        "question": [
          {
            "questionId": questions?.id,
            "options": predictQuestions?.answerType === "FREE_TEXT" ? predictQuestions?.option : answerListRef?.current
          }
        ],
        "userId": parseInt(authDetails.userId ?? "0")
      }
      await updateOptionAnswer(payload)
        .then((response: any) => {
          if (response?.data?.statusCode === "200") {
            toast.success(response?.data?.message ?? `Answer Updated successfully`);
            history.push(`/contests/${teamId}/${params?.contestId}`)
          }
          else {
            toast.error(response?.data?.statusCode)
          }
        })
        .catch((e: any) => {
          toast.error(e?.message)
        })
    }
    else {
      toast.error(`${questions?.id ? "question id" : "answer"} is missing!`)
    }
  }

  //get props based on category
  const getPropsByType = () => {
    let type = form.getFieldValue('category');
    const returnValues = (teamValue: any, groupValue: any, battleground: any) => type === "PLATFORM" ? teamValue : type === "BATTLEGROUND" ? battleground : groupValue;
    return {
      label: `${returnValues("Team", "Group", "Battleground")} Name`,
      name: returnValues("teamId", "groupId", "battlegroundId"),
      initialValue: returnValues(result?.teamId, result?.groupId, result?.battlegroundId),
      rules: {
        message: `${returnValues("Team", "Group", "Battleground")} Name is required!`
      },
      toFilter: returnValues("name", "groupName", "battleground"),
      placeholder: `Select ${returnValues("Team", "Group", "Battleground")}`,
      disabled: () => returnValues(((disabledComponents('teamId') || state?.teamId) ? true : isTeamUser), disabledComponents('groupId'), disabledComponents('battlegroundId')),
      loading: returnValues(teamOptionLoading, groupOptionLoading, false),
      options: returnValues(teams?.team, groups?.allGroups?.nodes, battleground)
    }
  }
  console.log("fun-name", getPropsByType())


  ///Allow Predict Answer Editable only on following status
  const isForceAnswer =
    result?.statusId === 'CLOSED' || result?.statusId === 'COMPLETED';

  const isAnswerImmediately = React.useMemo(() => (!(!!params?.contestId) || result?.statusId === 'SAVED'), [params, result])

  // const isViewOnly = !(result?.statusId === 'SAVED' || !params?.contestId) ? true : false;

  const onValuesChange = (field: any) => {

    if (field?.hasOwnProperty('winnerSelectionCriteria')) {
      setWinnerSelection(field?.winnerSelectionCriteria)
      if (field?.winnerSelectionCriteria === "ALL") {
        setRewardType("EQUAL_REWARD");
        form.setFieldsValue({
          rewardType: "EQUAL_REWARD"
        })
      }
    }
    else if (field?.hasOwnProperty('rewardType')) {
      setRewardType(field?.rewardType)
    }
    else if (field?.hasOwnProperty('contestTypeId')) {
      setContestType(field?.contestTypeId);
    }
    else if (field?.hasOwnProperty('noOfWinners')) {
      setNoOfWinners(field?.noOfWinners);
    }
    else if (field?.hasOwnProperty('team1Color') || field?.hasOwnProperty('team2Color')) {
      handleMatchChange({ ...field })
    }
    else if (field?.hasOwnProperty('leagueId')) {
      form.setFieldsValue({
        matchId: null
      })
      setMatchData({})
    }
    setFormData({ ...form.getFieldsValue(true) })
  }

  const onBeforeSave = () => {
    form.submit();
    form.validateFields()
      .then(async () => {
        try {
          //check number of users are winners and Rank should be same
          const { rewardType, rewardGivenFor, statusId } = form.getFieldsValue();
          if (rewardType === "RANKING" && rewardGivenFor === "EACH_WINNER") {
            if (totalRewardsRef?.current?.data?.length != noOfWinners) {
              toast.info('Number of users are winners and Rank should be same!')
              return false
            }
          }
          // check status of contest if status is in ['SUBMITTED', 'APPROVED', 'DISCONTINUED', 'SUSPENDED'] then we ask to add notes befoew save 
          if (params?.contestId && ['SUBMITTED', 'APPROVED', 'DISCONTINUED', 'SUSPENDED'].includes(statusId)) {
            setIsVisible(true);
            setCheckStatus(true)
          }
          else {
            if (checkStatus) {
              setCheckStatus(false)
            }
            onSave(form.getFieldsValue());
          }
        } catch (error: any) {
          toast.error(error?.message || JSON.stringify(error));
        }
      })
      .catch((e) => {
        console.log(e, 'e');
        toast.info('Please fill all the mandatory fields!')
      });
  }

  //on delete Notes
  const deleteNotes = () => {
    let filteredNotes = notesList?.filter((list, index) => index !== editIndex)
    setNotesList(filteredNotes)
    handleCancelNote()
  }

  //on cancel notes
  const handleCancelNote = () => {
    setIsVisible(false)
    if (editIndex > -1) {
      setEditIndex(-1)
    }
  }

  //on upserting notes
  const onUpsertNotes = (val: any, editdata: any) => {

    if (Object.keys(editdata).length > 0 && editIndex > -1) {
      notesList[editIndex] = {
        ...notesList[editIndex],
        name: val?.name,
        notes: val?.note,
      }
      setNotesList(notesList)
    }
    else {
      setNotesList([
        ...notesList, {
          name: val?.name,
          notes: val?.note,
          addedDate: moment(),
          addedby: authDetails.name,
        }
      ])
    }
    handleCancelNote()
  }

  //on open individual note
  const openEditNote = (obj: any, key: number) => {
    setEditIndex(key - 1);
    setIsVisible(true)
  }

  // note data
  const editNoteData = React.useMemo(() => {
    if (editIndex > -1) {
      return {
        name: notesList[editIndex]?.name,
        note: notesList[editIndex]?.notes,
        id: notesList[editIndex]?.id,
      }
    }
    return {}
  }, [editIndex])

  //disable start date
  const isDisableStartDate = React.useCallback((date) => {
    return date && moment(date).isBefore(moment(), 'day')
  }, [form])

  //disable end date
  const isDisableEndDate = React.useCallback((date, key) => {
    if (!form.getFieldValue(key)) {
      return true
    }
    let checkDate: MomentInput = moment(form.getFieldValue(key)).isBefore(moment(), 'day') ? moment() : moment(form.getFieldValue(key))
    return date && moment(date).isBefore(checkDate, 'day')
  }, [form])

  //status validation for Start Date Time
  const startDateTimeValidation = (getFieldValue: any, value: any) => {
    if (!value) {
      return Promise.reject(new Error(`Start Date Time is required!`));
    }
    if (getFieldValue('endAt')) {
      if (!value.isBefore(getFieldValue('endAt'))) {
        return Promise.reject(new Error(`Start Date Time should be lesser than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('displayStartAt')) {
      if (!value.isAfter(getFieldValue('displayStartAt'))) {
        return Promise.reject(new Error(`Start Date Time should be greater than Display Start Date Time(${getFieldValue('displayStartAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')?.value) {
      let team = filterSelectedTeam(teams, getFieldValue('teamId')?.value);
      if (!value.isAfter(moment(team?.activeStartAt?.concat('z')))) {
        return Promise.reject(new Error(`Start Date Time should be greater than Team Start Date Time(${moment(team?.activeStartAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for End Date Time
  const endDateTimeValidation = (getFieldValue: any, value: any) => {
    if (!value) {
      return Promise.reject(new Error(`End Date Time is required!`));
    }
    if (getFieldValue('startAt')) {
      if (!value.isAfter(getFieldValue('startAt'))) {
        return Promise.reject(new Error(`End Date Time should be greater than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('displayEndAt')) {
      if (!value.isBefore(getFieldValue('displayEndAt'))) {
        return Promise.reject(new Error(`End Date Time should be lesser than Display End Date Time(${getFieldValue('displayEndAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')?.value) {
      let team = filterSelectedTeam(teams, getFieldValue('teamId')?.value);
      if (!value.isBefore(moment(team?.activeEndAt?.concat('z')))) {
        return Promise.reject(new Error(`End Date Time should be lesser than Team End Date Time(${moment(team?.activeEndAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for Display Start Date Time
  const displayStartDateTimeValidation = (getFieldValue: any, value: any) => {
    if (!value) {
      return Promise.reject(new Error(`Display Start Date Time is required!`));
    }
    if (getFieldValue('startAt')) {
      if (!value.isBefore(getFieldValue('startAt'))) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')?.value) {
      let team = filterSelectedTeam(teams, getFieldValue('teamId')?.value);
      if (!value.isAfter(moment(team?.displayStartAt?.concat('z')))) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Team Start Date Time(${moment(team?.displayStartAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for Display End Date Time
  const displayEndDateTimeValidation = (getFieldValue: any, value: any) => {
    if (!value) {
      return Promise.reject(new Error(`Display End Date Time is required!`));
    }
    if (getFieldValue('endAt')) {
      if (!value.isAfter(getFieldValue('endAt'))) {
        return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')?.value) {
      let team = filterSelectedTeam(teams, getFieldValue('teamId')?.value);
      if (!value.isBefore(moment(team?.displayEndAt?.concat('z')))) {
        return Promise.reject(new Error(`Display End Date Time should be lesser than Team End Date Time(${moment(team?.displayEndAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //contest status validation
  const statusValidations = React.useMemo(() => {
    return pollFilterStatus(allRequiredOptions?.allTeamStatuses?.nodes, result?.statusId, result?.displayStartAt?.concat('z'))
  }, [result?.statusId, allRequiredOptions])

  //find disable components
  const disabledComponents = React.useCallback((key: string) => {
    if (!result) return false
    let validate = statusValidations?.disableComponents?.includes(key)
    return statusValidations?.except ? validate : !validate
  }, [statusValidations])

  // team currency symbol show hide based on team's coin symbol
  const currencySymbolToggle = () => {
    return (form.getFieldValue('teamId') ? teamValue?.coinSymbol ? true : false : true)
  }

  return (
    <AddContestWrapper>
      <Row>
        <BreadcrumbComp
          backPath="/contests"
          cta={
            <>
              <Button
                type="primary"
                className="breadcumb-btn-mg"
                onClick={() => onBeforeSave()}
              >
                {params?.contestId && 'Save Changes'}
                {!params?.contestId && 'Create Contest'}
              </Button>
              <Button
                className="breadcumb-btn-mg "
                onClick={() => history.goBack()}
              >
                Discard
              </Button>
              <Button onClick={() => onPreview({ ...form.getFieldsValue() })}>Live preview</Button>
            </>
          }
          breadCrumbs={[
            { name: 'Contest', url: '/contests' },
            { name: `${params?.contestId ? "Edit" : "New"} Contest`, url: '/contests/add' },
          ]}
        />

        <Form onFinish={onSave} form={form} preserve={false} onValuesChange={onValuesChange}>
          <Row gutter={[12, 0]}>
            {/* contest details card  START*/}
            <Card>
              <Typography className="header">
                {state?.teamId ? `Team > ${filterSelectedTeam(teams, state?.teamId)?.name} > Contest Details` : "Contest Details"}
              </Typography>
              <Row gutter={[12, 12]} justify="space-between">
                {/* contest title input START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Contest Title"
                    name="title"
                    initialValue={result?.title}
                    className="label-input"
                    required
                    rules={[
                      {
                        required: true,
                        message: 'Contest title is required!',
                      },
                    ]}
                    inputElement={<Input
                      placeholder="Type Contest Title"
                      disabled={disabledComponents('title')}
                    />}
                  />
                </Col>
                {/* contest title input END */}
                {/* contest category START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Category"
                    name="category"
                    className="label-input"
                    initialValue={result?.category}
                    rules={[
                      {
                        required: true,
                        message: 'Category is required!',
                      },
                    ]}
                    inputElement={
                      <SelectElement
                        onChange={(e: any) => {
                          form.setFieldsValue({
                            category: e,
                            groupId: null,
                            teamId: null,
                          }
                          );
                          setCategoryType(e)
                        }}
                        toStore="value"
                        options={categoryOptions}
                        toFilter="label"
                        placeholder="Category"
                        disabled={state?.teamId || disabledComponents('category')}
                      />
                    }
                  />
                </Col>
                {/* contest category END */}
                {/* Team name select START */}
                {
                  categoryType &&
                  <Col xs={24} sm={12} md={7}>
                    {/* {JSON.stringify(getPropsByType().options)} */}
                    <LabelInput
                      label={getPropsByType().label}
                      name={getPropsByType().name}
                      className="label-input"
                      required
                      rules={[
                        {
                          required: true,
                          message: getPropsByType().rules.message,
                        },
                      ]}
                      initialValue={getPropsByType().initialValue}
                      inputElement={
                        <SelectElement
                          defaultValue={getPropsByType().initialValue}
                          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                            let data: any = e;
                            let key = getPropsByType().name;
                            if (key === "groupId") {
                              form.setFieldsValue({ [key]: e });
                              return false;
                            }
                            if (key === "battlegroundId") {
                              var seletedbattle: any = filterSelectedBattle(
                                e
                              )
                              form.setFieldsValue({ [key]: seletedbattle });
                              // setBattleground()
                              return false;
                            }
                            if (data?.value !== teamValue?.value) {
                              form.setFieldsValue({ teamId: e });
                              var selectedTeam: any = filterSelectedTeam(
                                teams,
                                Number(data?.value)
                              );
                              var coinSymbolCurrent =
                                selectedTeam?.wallets[0]?.coinSymbol;
                              setTeamValue({
                                ...e,
                                coinSymbol: coinSymbolCurrent,
                              });
                              if (
                                coinSymbol !== 'SPN' &&
                                coinSymbol !== 'RWRD' &&
                                coinSymbol
                              ) {
                                setcoinSymbol(coinSymbolCurrent);
                                form.setFieldsValue({
                                  coinSymbol: coinSymbolCurrent,
                                });
                              }
                            }
                          }}
                          options={getPropsByType().options ?? []}
                          placeholder={getPropsByType().placeholder}
                          searchable={true}
                          toFilter={getPropsByType().toFilter}
                          toStore="id"
                          labelInValue={true}
                          disabled={getPropsByType().disabled()}
                          loading={getPropsByType().loading}
                        />
                      }
                    />
                  </Col>
                }
                {/* Team name select END */}
                {/* contest status select START */}
                {params?.contestId && (
                  <Col xs={24} sm={12} md={7}>
                    <LabelInput
                      label="Contest Status"
                      name="statusId"
                      className="label-input"
                      required
                      rules={[
                        {
                          required: true,
                          message: 'Contest Status is required!',
                        },
                      ]}
                      initialValue={result?.statusId}
                      inputElement={
                        <SelectElement
                          defaultValue={result?.statusId}
                          onChange={(
                            e: React.ChangeEvent<HTMLInputElement>
                          ) => {
                            form.setFieldsValue({ statusId: e });
                          }}
                          options={requiredOptionsLoading ? [] : statusValidations.options}
                          placeholder="Select Contest Status"
                          searchable={true}
                          toFilter="description"
                          toStore="status"
                          disabled={disabledComponents('status')}
                        />
                      }
                    />
                  </Col>
                )}
                {/* contest status select END */}
                {/* contest type select START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Contest Type"
                    name="contestTypeId"
                    className="label-input"
                    required
                    rules={[
                      {
                        required: true,
                        message: 'Contest Type is required!',
                      },
                    ]}
                    // initialValue={result?.contestTypeId}
                    inputElement={
                      <SelectElement
                        // defaultValue={result?.contestTypeId}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                        loading={requiredOptionsLoading}
                        options={
                          requiredOptionsLoading
                            ? []
                            : ((allRequiredOptions?.allTeamContestTypes
                              ?.nodes as unknown) as [])
                        }
                        placeholder="Select Contest Type"
                        searchable={true}
                        disabled={disabledComponents('contestTypeId')}
                        toFilter="description"
                        toStore="type"
                      />
                    }
                  />
                </Col>
                {/* contest type select END */}
                {/* Contest Platform select START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Contest Platform"
                    name="platformTypeId"
                    className="label-input"
                    required
                    rules={[
                      {
                        required: true,
                        message: 'Contest Platform is required!',
                      },
                    ]}
                    initialValue={result?.platformTypeId}
                    inputElement={
                      <SelectElement
                        defaultValue={result?.platformTypeId}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                          form.setFieldsValue({ platformTypeId: e });
                        }}
                        loading={requiredOptionsLoading}
                        options={
                          requiredOptionsLoading
                            ? []
                            : ((allRequiredOptions?.allTeamPollPlatforms
                              ?.nodes as unknown) as [])
                        }
                        placeholder="Select Team Name"
                        searchable={true}
                        toFilter="description"
                        toStore="type"
                        disabled={disabledComponents('platformTypeId')}
                      />
                    }
                  />
                </Col>
                {/* Contest Platform select END */}
                {/* START Date & Time START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Start Date & time"
                    name={'startAt'}
                    required
                    initialValue={
                      result?.startAt ? moment(result?.startAt + "z") : undefined
                    }
                    rules={[
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          return startDateTimeValidation(getFieldValue, value)
                        },
                      }),
                    ]}
                    inputElement={
                      <DatePicker
                        format={"DD-MM-YYYY hh:mm"}
                        showTime
                        disabledDate={(value) => isDisableStartDate(value)}
                        showNow={false}
                        showSecond={false}
                        className="datepicker"
                        disabled={disabledComponents('startAt')}
                      />
                    }
                  />
                </Col>
                {/* START Date & Time End */}
                {/* END Date & Time START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="End Date & time"
                    name={'endAt'}
                    required
                    initialValue={
                      result?.endAt ? moment(result?.endAt + "z") : undefined
                    }
                    rules={[
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          return endDateTimeValidation(getFieldValue, value)
                        },
                      }),
                    ]}
                    inputElement={
                      <DatePicker
                        format={"DD-MM-YYYY hh:mm"}
                        showTime={Boolean(form.getFieldValue('startAt'))}
                        disabledDate={(value) => isDisableEndDate(value, 'startAt')}
                        showNow={false}
                        showSecond={false}
                        className="datepicker"
                        disabled={disabledComponents('endAt')}
                      />
                    }
                  />
                </Col>
                {/* END Date & Time End */}
                {/* Display START Date & Time START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Display Start Date & time"
                    name={'displayStartAt'}
                    required
                    initialValue={
                      result?.displayStartAt
                        ? moment(result?.displayStartAt + "z")
                        : undefined
                    }
                    rules={[
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          return displayStartDateTimeValidation(getFieldValue, value)
                        },
                      }),
                    ]}
                    inputElement={
                      <DatePicker
                        format={"DD-MM-YYYY hh:mm"}
                        disabledDate={(value) => isDisableStartDate(value)}
                        showTime
                        showNow={false}
                        showSecond={false}
                        className="datepicker"
                        disabled={disabledComponents('displayStartAt')}
                      />
                    }
                  />
                </Col>
                {/* Display START Date & Time End */}
                {/* Display END Date & Time START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Display End Date & time"
                    name={'displayEndAt'}
                    required
                    initialValue={
                      result?.displayEndAt
                        ? moment(result?.displayEndAt + "z")
                        : undefined
                    }
                    rules={[
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          return displayEndDateTimeValidation(getFieldValue, value)
                        },
                      })
                    ]}
                    inputElement={
                      <DatePicker
                        format={"DD-MM-YYYY hh:mm"}
                        disabledDate={(value) => isDisableEndDate(value, 'displayStartAt')}
                        showTime={Boolean(form.getFieldValue('displayStartAt'))}
                        showNow={false}
                        showSecond={false}
                        className="datepicker"
                        disabled={disabledComponents('displayEndAt')}
                      />
                    }
                  />
                </Col>
                {/* Display END Date & Time End */}
                {/* Currency Type START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Currency Symbol"
                    name="coinSymbol"
                    // rules={[
                    //   {
                    //     required: true,
                    //     message: 'Currency Symbol is required!',
                    //   },
                    // ]}
                    inputElement={
                      <SelectElement
                        defaultValue={result?.coinSymbol}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                          setcoinSymbol(e);
                          form.setFieldsValue({ coinSymbol: e });
                        }}
                        options={[
                          {
                            currencyType: 'SPN',
                          },
                          {
                            currencyType: 'RWRD',
                          },
                          ...((categoryType === "PLATFORM") && currencySymbolToggle() ? [{
                            currencyType: form.getFieldValue('teamId') ? teamValue?.coinSymbol : 'Choose your team',
                          }] : []),
                        ]}
                        toFilter="currencyType"
                        disabledOptions={["Choose your team"]}
                        placeholder="Currency Type"
                        disabled={disabledComponents('currency')}
                      />
                    }
                  />
                </Col>
                {/* Currency Type End */}
                <Col xs={24} sm={12} md={6}></Col>
                {/* contest Description input START */}
                <Col xs={20}>
                  <LabelInput
                    label="Contest Description"
                    name="description"
                    className="label-input"
                    required
                    initialValue={result?.description}
                    rules={[
                      {
                        required: true,
                        message: 'Contest Description is required!',
                      },
                    ]}
                    inputElement={<Input placeholder="Description" disabled={disabledComponents('description')} />}
                  />
                </Col>
                {/* constest position position START */}
                <Col xs={4}>
                  <LabelInput
                    label="Position"
                    name="orderBy"
                    rules={[
                      {
                        required: true,
                        message: 'Poll position is required!',
                      },
                    ]}
                    inputElement={
                      <InputNumber
                        type={'number'}
                        className="fields"
                        min={1}
                        max={totalContest && totalContest + 1}
                        disabled={disabledComponents('orderBy')}
                      />
                    }
                  />
                </Col>
              </Row>
            </Card>
            {/* contest details card  START*/}
            {/* Participation Criteria details Card  START*/}
            <Card>
              <Typography className="header">Participation Criteria</Typography>
              <Row gutter={[12, 12]} justify="space-between">
                {/* Joining Balance Field START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="Minimum Joining Balance"
                    name="minJoinBalance"
                    initialValue={result?.minJoinBalance}
                    // required
                    // rules={[
                    //   {
                    //     required: true,
                    //     message: 'Amount is required!',
                    //   },
                    // ]}
                    inputElement={
                      <Input
                        suffix={
                          <span
                            style={{
                              fontSize: 14,
                              color: '#4DA1FF',
                              fontWeight: '500',
                            }}
                          >
                            {coinSymbol}
                          </span>
                        }
                        placeholder="Enter Amount"
                        disabled={disabledComponents('minJoinBalance')}
                      />
                    }
                  />
                </Col>
                {/* Joining Balance Field END */}
                {/* Participation Restriction Field START */}
                <Col xs={24} sm={12} md={7}>
                  <LabelInput
                    label="No Of Participation Allow"
                    name="noParticipantAllowed"
                    initialValue={result?.noParticipantAllowed}
                    // required
                    // rules={[
                    //   {
                    //     required: true,
                    //     message: 'Participation Count is required!',
                    //   },
                    // ]}
                    inputElement={
                      <Input type={'number'} placeholder="Enter Numbers" disabled={disabledComponents('noParticipantAllowed')} />
                    }
                  />
                </Col>
                {/* Participation Restriction Field END */}
                <Col xs={24} sm={12} md={7}></Col>
              </Row>
            </Card>
            {/* Participation Criteria details Card  START*/}

            <Card>
              <Typography className="header">Winner Definition</Typography>
              <WinningForm
                form={form}
                radioOptions={CommonRadioOptions}
                result={{
                  ...result,
                  contestTypeId: contestType,
                }}
                isView={disabledComponents('winningForm')}
                winnerSelection={winnerSelection}
              />
            </Card>
            <Card>
              <Typography className="header">Rewards</Typography>
              <RewardsForm
                form={form}
                radioOptions={CommonRadioOptions}
                result={result}
                handleTotalRewardsMaster={handleTotalRewardsMaster}
                isView={disabledComponents('rewardsForm')}
                rewardType={rewardType}
                winnerSelection={winnerSelection}
                noOfWinners={noOfWinners}
              />
            </Card>
            {contestType === 'Quiz' && (
              <Card>
                <QuestionsWrapperModal
                  data={result?.question}
                  onSubmit={onQuestionsFormSubmit}
                  isView={disabledComponents('quizForm')}
                />
              </Card>
            )}
            {/* Predict Only UI START */}
            {contestType === 'Predict' && (
              <Card>
                <TemplateWrapper
                  form={form}
                  title={'Contest Template'}
                  onChange={(v: any) => {
                    setPredictQuestions({ option: optionsInitialState(v) });
                    setPollType(v);
                  }}
                  value={pollType}
                  keyName="templateId"
                  renderJson={
                    requiredOptionsLoading
                      ? []
                      : ((allRequiredOptions?.allMasterContestTemplates
                        ?.nodes as unknown) as [])
                  }
                  isView={disabledComponents('contestTemplate')}
                />
              </Card>
            )}
            {contestType === 'Predict' && (
              <Card>
                <Typography className="header">Match Details</Typography>
                <MatchCard
                  form={form}
                  data={matchData}
                  handleChange={handleMatchChange}
                  isView={disabledComponents('matchDetails')}
                />
              </Card>
            )}
            {contestType === 'Predict' && pollType === 1 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={predictQuestions}
                  isImageUploadEnabled={false}
                  isSelectAnswer={isForceAnswer}
                  isAllowMultiSelect={false}
                  questionMasterTitle={'Predict the Match Winner'}
                  optionType={'text'}
                  handleChange={handlePredictQuestionsChange}
                  isView={disabledComponents('predictTheMatchWinner')}
                  isDescriptionDisabled={disabledComponents('predictTheMatchWinner')}
                  handleAnswerUpdate={updatePredictFormAnswer}
                  templateId={pollType}
                  disabledAddOption={disabledComponents('predictTheMatchWinner')}
                // minSelection={2}
                />
              </Card>
            )}
            {contestType === 'Predict' && pollType === 2 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={predictQuestions}
                  isImageUploadEnabled={predictQuestions?.answerType !== 'FREE_TEXT' ? true : false}
                  isAllowMultiSelect={false}
                  isSelectAnswer={isForceAnswer}
                  questionMasterTitle={'Predict the Score'}
                  // optionType={
                  //   predictQuestions?.answerType === 'FREE_TEXT' ? 'image' : 'text'
                  // }
                  optionType={"text"}
                  handleChange={handlePredictQuestionsChange}
                  isView={params?.contestId ? true : false}
                  isDescriptionDisabled={params?.contestId ? true : false}
                  handleAnswerUpdate={updatePredictFormAnswer}
                  templateId={pollType}
                  isAnswerEnabled={predictQuestions?.answerType === 'FREE_TEXT' ? false : true}
                  disabledAddOption={disabledComponents('predictTheScore') || predictQuestions?.answerType === "FREE_TEXT"}
                />
              </Card>
            )}
            {contestType === 'Predict' && pollType === 3 && (
              <Card>
                <ManOfMatchForm
                  form={form}
                  data={manOfMatchQuestion}
                  handleChange={handleManOfMatchForm}
                  // handleAnswerUpdate={updateManOfTheMatchAnswer}
                  handleAnswerUpdate={updatePredictFormAnswer}
                  questionMasterTitle={'Predict Man of the Match'}
                  isSelectAnswer={isForceAnswer}
                  isView={disabledComponents('predictManOfTheMatch')}
                  isDescriptionDisabled={disabledComponents('predictManOfTheMatch')}
                />
              </Card>
            )}
            {contestType === 'Predict' && pollType === 4 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={predictQuestions}
                  isImageUploadEnabled={true}
                  isAllowMultiSelect={false}
                  isSelectAnswer={isAnswerImmediately}
                  questionMasterTitle={'What Happens Next'}
                  optionType={'text'}
                  handleChange={handlePredictQuestionsChange}
                  isView={disabledComponents('whatHappensNext')}
                  isDescriptionDisabled={disabledComponents('whatHappensNext')}
                  handleAnswerUpdate={updatePredictFormAnswer}
                  templateId={pollType}
                  disabledAddOption={disabledComponents('whatHappensNext')}
                />
              </Card>
            )}
            {contestType === 'Predict' && pollType === 5 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={predictQuestions}
                  isAllowMultiSelect={true}
                  isImageUploadEnabled={true}
                  isSelectAnswer={isAnswerImmediately}
                  optionType={'puzzle'}
                  handleChange={handlePredictQuestionsChange}
                  isView={disabledComponents('spotTheBall')}
                  isDescriptionDisabled={disabledComponents('spotTheBall')}
                  handleAnswerUpdate={updatePredictFormAnswer}
                  templateId={pollType}
                  disabledAddOption={true}
                />
              </Card>
            )}
            {contestType === 'Predict' && pollType === 6 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={predictQuestions}
                  isImageUploadEnabled={true}
                  isAllowMultiSelect={false}
                  isSelectAnswer={false}
                  questionMasterTitle={'Predict the Player Position'}
                  optionType={'text'}
                  handleChange={handlePredictQuestionsChange}
                  isView={disabledComponents('predictThePlayerPosition')}
                  handleAnswerUpdate={updatePredictFormAnswer}
                  templateId={pollType}
                  disabledAddOption={true}
                  isDescriptionDisabled={!isForceAnswer}
                  isHideDeleteIcon={pollType === 6}
                />
              </Card>
            )}
            {/* Predict Only UI END */}
            {/* Notes UI START */}
            <Card>
              <Row style={{ margin: 0 }} gutter={[24, 24]}>
                <Typography className="header" style={{ flex: 1 }}>Notes</Typography>
                <Button type="link" icon={<PlusOutlined />} onClick={() => setIsVisible(true)}>
                  Add New Note
                </Button>
              </Row>
              <NoteModal
                handleDelete={deleteNotes}
                EditData={editNoteData}
                ModalVisible={isVisible}
                handleOk={(val: any, editdata: any) => onUpsertNotes(val, editdata)}
                handleCancel={handleCancelNote}
                readOnly={editNoteData?.id}
              />
              <Note
                JsonData={notesList}
                openEdit={(obj: any, key: number) => openEditNote(obj, key)}
              />
            </Card>
            {/* Notes UI END */}
          </Row>
        </Form>
      </Row>
    </AddContestWrapper>
  );
};

export default AddContest;
