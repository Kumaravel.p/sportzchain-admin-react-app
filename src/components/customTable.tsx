import React, { useCallback } from 'react';
import { Table, Typography, Avatar, Input, Popconfirm, Space, DatePicker } from 'antd';
import LabelInput from './common/LabelInput';
import styled from 'styled-components/macro';
import { TablePaginationConfig } from 'antd/lib/table';
import { SorterResult, FilterValue } from 'antd/lib/table/interface';
import { PaginationProps } from 'antd';
import { EyeOutlined, LeftOutlined, RightOutlined } from '@ant-design/icons';
import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import { DeleteOutlined } from '@ant-design/icons';
import moment from 'moment';
interface TableContainerProps {
  border: string;
}

const TableContainer = styled.div<TableContainerProps>`
  .ant-table-container {
    & .ant-table-body{
      overflow-y: auto !important;
      overflow-x: auto !important;
    }
    & table {
      tbody {
        tr {
          .ant-table-cell {
            border-bottom: ${(props) =>
              props.border === 'none' ? 0 : '1px solid #f0f0f0'};
          }
          .input-field{
            border-radius: 2px;
            border-color: #d9d9d9;
          }
          input{
            text-overflow: ellipsis;
          }
        }
      }
    }
  }
  .ant-pagination {
    & .ant-pagination-item {
      border-radius: 5px;
      background: #fff;
      border: 0;
    }
    & .ant-pagination-item-active {
      background: rgba(7, 105, 125, 0.05);
      a {
        color: #07697d;
      }
    }
    [aria-disabled='true'] a {
      color: #051f2466;
    }
    [aria-disabled='false'] a {
      color: #051f24;
      font-weight: 500;
    }
    .ant-pagination-prev {
      margin-right: 16px;
    }
    .ant-pagination-next {
      margin-left: 8px;
    }
    .ant-pagination-prev,
    .ant-pagination-next {
      & a {
        display: flex;
        align-items: center;
        gap: 6px;
        & span {
          font-size: 12px;
        }
      }
    }
  }
`;

interface tableProps {
  columns: Array<any>;
  dataSource: Array<any>;
  loading?: boolean;
  pagination?: TablePaginationConfig;
  onChangeTable?: (
    newPagination: TablePaginationConfig,
    filters: Record<string, FilterValue | null>,
    sorter: SorterResult<any> | SorterResult<any>[]
  ) => void;
  onRowClick?: (record: any, rowIndex: any, keyname?: string) => void;
  isEditIcon?: (record: any, rowIndex: any) => void;
  isDeleteIcon?: (record: any, rowIndex: any) => void;
  returnEditComponents?: (record: any, rowIndex: any) => void;
  onRowSave?: (newRecord: any, rowIndex: any, type: "edit" | "delete") => void;
  onCancel?: (rowIndex: any) => void;
  border?: 'none' | undefined;
  sorter?: boolean;
  align?: 'left' | 'right' | 'center';
  tableLayout?: 'fixed' | 'auto';
  hidePagination?: boolean;
  showSizeChanger?: boolean;
  editIndex?:number|null;
  editValue?:any;
  scrollY?:number | string | undefined;
  scrollX?:number | string | undefined;
  columnWidth?:number | string;
}

const CustomTable = (props: tableProps): JSX.Element => {
  const {
    dataSource = [],
    columns = [],
    loading = false,
    sorter = false,
    pagination,
    onChangeTable,
    onRowClick,
    onRowSave,
    isEditIcon,
    isDeleteIcon,
    returnEditComponents,
    onCancel,
    border = 'none',
    align = 'left',
    tableLayout = 'auto',
    hidePagination = false,
    showSizeChanger = true,
    editIndex=null,
    editValue,
    scrollY,
    scrollX,
    columnWidth
  } = props;

  const [isEdit, setisEdit] = React.useState<number | null>(null);
  const [newRecordState, setNewRecordState] = React.useState({});

  React.useEffect(()=>{
    setisEdit(editIndex);
  },[editIndex])

  const handleFieldChange = (value: any, keyname: string) => {
    setNewRecordState({
      ...newRecordState,
      [keyname]: value,
    });
  };

  const handleRowSave = () => {
    if (onRowSave) {
      onRowSave(newRecordState, isEdit, "edit");
      setNewRecordState({});
      setisEdit(null);
    }
  };

  const handleTableChange = (
    newPagination: TablePaginationConfig,
    filters: Record<string, FilterValue | null>,
    sorter: SorterResult<any> | SorterResult<any>[]
  ) => {
    if (onChangeTable) {
      onChangeTable(newPagination, filters, sorter);
    }
  };

  const itemRender: PaginationProps['itemRender'] = useCallback(
    (_, type, originalElement) => {
      if (type === 'prev') {
        return (
          <a>
            {' '}
            <LeftOutlined /> Prev
          </a>
        );
      }
      if (type === 'next') {
        return (
          <a>
            Next <RightOutlined />
          </a>
        );
      }
      return originalElement;
    },
    [pagination]
  );  

  const findEditIndex = (rowRecord:any,rowIndex:number) => editValue ? rowRecord?.[editValue] : rowIndex;

  const switchComponents = (
    value: any,
    column: any,
    rowRecord: any,
    rowIndex: number,
  ) => {
    let getEditIndex = findEditIndex(rowRecord,rowIndex);
    switch (column?.component) {
      case 'object':
        return (
          <Typography.Text>
            {value?.[column?.toShowLabel ?? 'label'] ?? '-'}
          </Typography.Text>
        );
      case 'avatar':
        return <Avatar src={value?.src}>{value?.firstChar ?? ''}</Avatar>;
      case 'icons':
        if (typeof value === 'function') {
          return (
            //you must use  e.stopPropagation() in your onClick function or else onRowClick also trigger
            value(rowRecord, rowIndex)
          );
        } else {
          return value;
        }
      case 'status':
        let val = value ? value?.toLowerCase() : '';
        let color = '#07697D';
        if (val?.includes(['open', 'closed', 'saved'])) {
          color = '#07697D';
        }
        return (
          <Typography.Text style={{ color }}>{value ?? '-'}</Typography.Text>
        );
      case 'EDITABLE':
        if (isEdit === getEditIndex && (returnEditComponents ? returnEditComponents(rowRecord,getEditIndex)?.[column?.dataIndex] : true)) {
            if (column?.component_name === "date-picker") {
                return (
                    <LabelInput
                        name={`${column?.dataIndex}${getEditIndex}`}
                        rules={[
                            {
                                required: true,
                                message: `${column?.title} is required!`,
                            },
                        ]}
                        inputElement={
                            <DatePicker
                            format={"DD-MM-YYYY hh:mm"}
                            showTime
                            showNow={false}
                            showSecond={false}
                                    style={{
                                        width: '100%'
                                    }}
                                    defaultValue={value ? (value instanceof moment) ? value : moment(value) : null}
                                    onChange={(e) => {
                                        handleFieldChange(e?.toISOString(), column?.dataIndex);
                                      }}
                                      disabledDate={d => column.disabledDate ? column.disabledDate(d,rowRecord,newRecordState) : false}
                                      {...(column?.options ?? {})}
                                />
                        }
                    />
                )
            }
          return (
            <LabelInput
              name={`${column?.dataIndex}${getEditIndex}`}
              rules={[
                {
                  required: true,
                  message: `${column?.title} is required!`,
                },
              ]}
              inputElement={
                <Input
                  placeholder={'Enter Value'}
                  style={{ width: '100%' }}
                  defaultValue={value}
                  onChange={(e) => {
                    handleFieldChange(e.target.value, column?.dataIndex);
                  }}
                  className="input-field"
                  {...(column?.options ?? {})}
                />
              }
            />
          );
        }
        return (
          <Typography.Text>
            {column?.valueFormat ? column?.valueFormat(value) : value ?? '-'} {column?.suffix}
          </Typography.Text>
      );
      case 'VIEW':
        return <EyeOutlined
          style={{color:"#4DA1FF"}}
          onClick={(e) =>{
            if(onRowClick){
              onRowClick(rowRecord, rowIndex, 'view')
            }
          }}
        />
      case 'ACTION':
        return (
          <Space size={12}>
            <EditIcon
              className="edit-icon"
              onClick={(e) =>{
                if(onRowClick){
                  onRowClick(rowRecord, rowIndex, 'edit')
                }
              }}
            />
            <DeleteOutlined
              className="delete-icon"
              onClick={(e) =>{
                if(onRowClick){
                  onRowClick(rowRecord, rowIndex, 'delete')
                }
              }}
            />
          </Space>
        );
      case 'INSERT':
        const editable = getEditIndex === isEdit;
        return editable ? (
          <span>
            <Typography.Link
              onClick={handleRowSave}
              style={{
                marginRight: 8,
              }}
            >
              Save
            </Typography.Link>
            <Popconfirm
              title="Sure to cancel?"
              onConfirm={() => {
                onCancel && onCancel(getEditIndex)
                setisEdit(null)
              }}
            >
              <a>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
            <Space>
              {
                (isEditIcon ? isEditIcon(rowRecord,getEditIndex) : true) &&
                <Typography.Link
                disabled={isEdit !== null}
                onClick={() => setisEdit(getEditIndex)}
              >
                Edit
              </Typography.Link>
              }
              {
                (isDeleteIcon ? isDeleteIcon(rowRecord,getEditIndex) : true) &&
                <Typography.Link
                disabled={isEdit !== null}
                onClick={() => onRowSave ? onRowSave({}, getEditIndex, "delete") : null}
                >
                Delete
              </Typography.Link>
              }
            </Space>
        );
      case "RANK":
        return <Typography.Text>Rank {column?.valueFormat ? column?.valueFormat(value) : value ?? '-'}</Typography.Text>;
      default:
        return <Typography.Text>{column?.valueFormat ? column?.valueFormat(value) : value ?? '-'}</Typography.Text>;
    }
  };

  return (
    <div style={{ width: '100%' }}>
      <TableContainer border={border}>
        <Table
          dataSource={dataSource}
          loading={loading}
          pagination={hidePagination  ? false : {
            ...pagination,
            showSizeChanger: showSizeChanger,
            pageSizeOptions: ['5', '10', '20', '30', '40'],
            itemRender: (_, type, originalElement) =>
              itemRender(_, type, originalElement),
          }}
          onChange={handleTableChange}
          tableLayout={tableLayout}
          scroll={{ y: scrollY, x: scrollX }}
          onRow={(record: any, rowIndex: any) => {
            return {
              onClick: (event) => {
                onRowClick && onRowClick(record, rowIndex);
              }, // click row
            };
          }}
        >
          {columns?.map((column: any, index: number) => (
            <Table.Column
              title={column?.title}
              dataIndex={column?.dataIndex}
              width={column?.width ? column?.width : columnWidth}
              key={column?.key ?? `column-${index}`}
              render={
                column.render
                  ? column.render
                  : (value, rowRecord, rowIndex) =>
                      switchComponents(value, column, rowRecord, rowIndex)
              }
              sorter={sorter || column?.sorter}
              align={column?.align ? column?.align : align}
            />
          ))}
        </Table>
      </TableContainer>
    </div>
  );
};

export default CustomTable;
