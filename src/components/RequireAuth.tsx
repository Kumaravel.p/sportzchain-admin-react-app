import * as React from 'react';
import { Redirect } from 'react-router-dom';

import { AccountContext } from 'context/AccountContext';

interface RequireAuthProps {
  children: React.ReactNode;
  redirectTo: string;
}

const RequireAuth = ({ children, redirectTo }: RequireAuthProps) => {
  const [mounted, setMounted] = React.useState(false);
  const [loggedIn, setLoggedIn] = React.useState(false);

  const { getSession } = React.useContext(AccountContext);
  React.useEffect(() => {
    getSession()
      .then((session: any) => {
        console.log(session, typeof session, 'get session');
        if (session) {
          setLoggedIn(true);
          setMounted(true);
        } else {
          setLoggedIn(false);
          setMounted(true);
        }
      })
      .catch(console.error);
  }, [getSession]);

  if (mounted && !loggedIn) return <Redirect to={redirectTo} />;

  return <>{children}</>;
};

export default RequireAuth;
