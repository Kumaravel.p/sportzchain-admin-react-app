import React from "react";
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import {
  Table,
  Divider,
  Row,
  Input,
  Button,
  Form
} from 'antd';
import { format } from 'date-fns';
import Text from 'antd/lib/typography/Text';
import { useHistory } from 'react-router-dom';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import {
  useGetTeamCategoriesQuery,
  useGetTeamCurrencyQuery,
  useGetTeamStatusesQuery,
} from 'generated/graphql';
import moment from "moment";

const teamsColumns = [
  {
    title: 'STATUS',
    dataIndex: ['teamStatus', 'status'],
    render: (status: string) => <Text className="t-status">{status}</Text>,
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    sorter: (a: any, b: any) => a.name.localeCompare(b.name)
  },
  {
    title: 'SPORT',
    dataIndex: ['teamCategory', 'category'],
  },
  {
    title: 'TYPE',
    dataIndex: 'currencyTypeId',
  },
  {
    title: 'MEMBERS',
    dataIndex: ['teamContacts_aggregate', 'aggregate', 'count'],
  },
  {
    title: 'START',
    dataIndex: 'activeStartAt',
    render: (date: Date) => date && moment(date).format('LL'),
  },
  {
    title: 'END',
    dataIndex: 'activeEndAt',
    render: (date: Date) => date && moment(date).format('LL'),
  },
];

const TeamInfoWrapper = styled.div`
  .filters-wrapper {
    padding: 40px 0;
    column-gap: 25px;
    align-items: center;
    input {
      padding: 9px;
      background: rgba(5, 31, 36, 0.05);
      border: 1px solid rgba(5, 31, 36, 0.05);
      box-sizing: border-box;
      border-radius: 5px;
    }
  }

  .ant-table-thead > tr > th {
    background: transparent;
  }

  .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
    background: transparent;
  }

  .table-user-avatar {
    width: 44px;
    height: 44px;
    border-radius: 10px;
  }
  .t-table-status {
    font-weight: normal;
    font-size: 16px;
    line-height: 19px;
    color: #07697d;
  }
  .ant-dropdown-trigger {
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.4px;
    color: #9c9c9c;
    font-family: 'Montserrat', sans-serif;
    padding: 20px;
    display: flex;
    align-items: center;
  }
  .ant-table-content {
    .ant-table-thead {
      .ant-table-cell {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #9ab1b6;
      }
    }
    .ant-table-row {
      font-size: 16px;
      cursor: pointer;
      line-height: 19px;
      font-weight: 500;
      color: #051f24;
    }
  }
  .t-status {
    color: #07697d;
  }
`;

interface TeamInfoProps {
  teamsData: any;
  handleChange: any;
  clearFlitter: any;
}

const TeamInfo = ({ teamsData, handleChange, clearFlitter }: TeamInfoProps): JSX.Element => {
  const history = useHistory();
  const [selected, setSelected] = React.useState<any>();
  const [form] = Form.useForm();

  const { data: teamStatuses } = useGetTeamStatusesQuery();
  const { data: teamCategories } = useGetTeamCategoriesQuery();
  const { data: teamCurrencyTypes } = useGetTeamCurrencyQuery();

  const clearFlitterIn = () => {
    form.resetFields();
    clearFlitter();
  }
  return (
    <TeamInfoWrapper>
      <Form form={form}>
        <Row style={{ width: "100%", display: "flex", alignItems: "center" }}>
          <div style={{ width: "20%", marginRight: "1%" }}>
            <LabelInput
              label="Search and filter"
              name="name"
              inputElement={
              <Input
                type="text"
                onChange={(e: any) => handleChange(e?.target?.value, "name")}
                placeholder={"Search and filter"}
              />
              }
            />
          </div>
          <div style={{ width: "20%", marginRight: "1%" }}>
            <LabelInput
              label="Currency type"
              rules={[
                {
                  required: true,
                  message: 'Currency type is required!',
                },
              ]}
              name={"currency"}
              inputElement={
                <SelectElement
                  onChange={(val: any) => handleChange(val, "currency")}
                  options={teamCurrencyTypes?.teamCurrencyType ?? []}
                  toFilter="currency"
                  placeholder="Currency Type"
                />
              }
            />
          </div>
          <div style={{ width: "20%", marginRight: "1%" }}>
            <LabelInput
              label="Sports Category"
              rules={[
                { required: true, message: 'Sports Category is required!' },
              ]}
              name={"SportsType"}
              inputElement={
                <SelectElement
                  onChange={(val: any) => handleChange(val, "SportsType")}
                  options={teamCategories?.teamCategory ?? []}
                  toFilter="category"
                  placeholder="Sport Category"
                  searchable={true}
                />
              }
            />
          </div>
          <div style={{ width: "20%" }}>
          <LabelInput
            label="Status"
            rules={[
              {
                required: true,
                message: 'Status is required!',
              },
            ]}
            name={"Status"}
            inputElement={
              <SelectElement
                onChange={(val: any) => handleChange(val, "Status")}
                options={teamStatuses?.teamStatus ?? []}
                toFilter="status"
                placeholder="Status"
              />
            }
            />
          </div>
          <Button type="link" style={{ marginTop: "4%" }} onClick={() => clearFlitterIn()}>Clear Filter</Button>
        </Row>
      </Form>
      <div>
        <Divider />
        <Table
          onRow={(record, rowIndex) => {
            return {
              onClick: () => {
                console.log(history.push(`/team/${record.id}`));
              },
            };
          }}
          columns={teamsColumns}
          dataSource={teamsData}
        />
      </div>
    </TeamInfoWrapper>
  );
};

export default TeamInfo;
