import React from 'react';
import { Modal, Button, Row, Col, Divider } from 'antd';
import ViewCardGenerator from 'components/ViewCardGenerator/viewCardGenerator';
import moment from "moment";
interface NotesInfoProps {
  Data: any;
  ModalVisible: boolean;
  handleCancel: any;
}
const ViewRewardModal = ({Data, ModalVisible, handleCancel}: NotesInfoProps): JSX.Element => {
  
  let RewardsDetail = [
    {
    title:"Rules",
    valuesArr:[
      {
        lable: "Rule Name",
        value: Data?.extra?.val?.ruleName,
        col: 16,
        divider: true
      },
      {
        lable: "Rule Status",
        value: Data?.extra?.val?.masterRwrdRuleStatusByRuleStatusId?.status,
        col: 8,
        divider: true
      },
      {
        lable: "Rewards Per User",
        value: `${parseInt(Data?.extra?.val?.rewardPerUser)} USD` ,
        col: 8,
        divider: true
      },
      {
        lable: "Reward Value",
        value: `1 RWRD = ${parseInt(Data?.extra?.val?.rewardValue)} USD`,
        col: 8,
        divider: true
      },
      {
        lable: "Total Quantity of Rewards Per User",
        value: `${parseInt(Data?.extra?.val?.totalQuantity)} RWRD`,
        col: 8,
        divider: true
      },
      {
        lable: "Start Date & Time",
        value: moment(Data?.extra?.val?.startDate?.concat('z'))?.format('MMMM D YYYY, HH:mm'),
        col: 8,
        divider: true
      },
      {
        lable: "End Date & Time",
        value: moment(Data?.extra?.val?.endDate?.concat('z')).format('MMMM D YYYY, HH:mm'),
        col: 8,
        divider: true
      },
      {
        col: 8,
        divider: true
      },
    ]
    },
    {
      title:"Restriction Limit",
      valuesArr:[
        {
          lable: "Max Limit (No. of times)",
          value: Data?.extra?.val?.maxLimit || '-',
          col: 8,
        },
        {
          lable: "Duration",
          value: Data?.extra?.val?.duration ? `${Data?.extra?.val?.duration} Days` : '-',
          col: 8,
        },
        {
          col: 8,
        }
      ]
    }  
  ];
  return (
    <>
      <Modal 
        // title={'View Reward'}
        okText="View"
        visible={ModalVisible}
        closable={true}
        onCancel={()=>{
          handleCancel();
        }}
        destroyOnClose={true}
        footer={
          <Button key="submit" type="primary" onClick={()=> handleCancel()}>
            Close
          </Button>
        }
        width={1000}
        className={"noPadding"}
      >
          <ViewCardGenerator DataJSON={RewardsDetail} margin={0.1}/>
      </Modal>
    </>
  );
};

export default ViewRewardModal;
