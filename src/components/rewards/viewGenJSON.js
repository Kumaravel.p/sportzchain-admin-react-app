import moment from "moment";
export const viewGenJSON = (data)=>{
    
    let RewardsDetail = [{
        title:"Reward Details",
        valuesArr:[
          {
            lable: "Category",
            value: data?.masterRwrdCategoryByCategoryId?.category ? data?.masterRwrdCategoryByCategoryId?.category : "",
            col: 6,
            divider: true
          },
          {
            lable: "Activity",
            value: data?.masterRwrdActivityByActivityId?.description ? data?.masterRwrdActivityByActivityId?.description : "",
            col: 6,
            divider: true
          },
          {
            lable: "Start Date & Time",
            value: data?.startDate ? moment(data?.startDate?.concat('z')).format('MMMM D YYYY, HH:mm') : "",
            col: 6,
            divider: true
          },
    
          {
            lable: "End Date & Time",
            value: data?.endDate ? moment(data?.endDate?.concat('z')).format('MMMM D YYYY, HH:mm') : "",
            col: 6,
            divider: true
          },
          {
            lable: "Reward Description",
            value: data?.description ? data?.description : "",
            col: 24,
          },
        ]
    }]
    let result = {
        RewardsDetail,
    }
    return result;
}

export default viewGenJSON;