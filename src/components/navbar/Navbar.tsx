import * as React from 'react';
import { Dropdown } from 'antd';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';
import defaultImage from 'assets/images/default_image.png';
import currentSession from 'utils/getAuthToken';
import { useEffect } from "react";
import { Auth } from 'aws-amplify';
import { TeamSelectionContext } from 'App';
import { CaretDownOutlined } from '@ant-design/icons';
// import { ProfileIcon } from 'components/svg/profile';
import { LogoutIcon } from 'components/svg/logout';
import { ProfileIcon } from 'components/svg/profile';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';


const NavbarWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: fit-content;
  align-items: center;
  padding: 10px 20px;
  border-bottom: 1px solid #eee;
  .logo-sportzchain {
    font-weight: bold;
    font-size: 25px;
    line-height: 30px;
    color: #07697d;
  }
  // .dropdown-option{
  //   background: #FFFFF !important;
  // }
  .search-profile-wrapper {
    justify-content: flex-end;
    column-gap: 25px;
    align-items: center;

    .search-wrapper {
      .search-input {
        background: rgba(5, 31, 36, 0.05);
        border: 1px solid rgba(5, 31, 36, 0.05);
        box-sizing: border-box;
        border-radius: 5px;
        padding: 15px;
        width: 390px;
      }
    }
    .profile-image {
      img {
        /* width: 44px; */
        height: 44px;
        border-radius: 10px;
      }
    }
  }
`;

interface NavbarProps { }

const Navbar = ({ }: NavbarProps): JSX.Element => {
  const onLogout = () => {
    Auth.signOut().then(() => {
      localStorage.removeItem('token');
      window.location.replace('/signin/email');
    })
  };

  const teamSelectionContext = React.useContext(TeamSelectionContext)
  // useEffect(() => {
  //   currentSession().then(res => {
  //     console.log(res?.decodePayload(), "Decoded")
  //     console.log(res?.payload)
  //   })
  // }, [])
  // const user_id = +JSON.parse(Buffer.from((localStorage.getItem('token') || '').split('.')[1], 'base64').toString('binary')).user_id;

  const onTeamSelected = (team: any) => {
    teamSelectionContext.setTeam(team);
  }
  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  return (
    <NavbarWrapper>
      <div>
        <Text className="logo-sportzchain">Sportzchain</Text>
      </div>
      <FlexRowWrapper className="search-profile-wrapper">
        {/* <div className="search-wrapper">
          <input type="text" className="search-input" placeholder="Search" />
        </div> */}
        {isIamTeamUser() && <Dropdown
          overlay={<>
            {team?.teams?.map((team: any, key: any) => {
              return <div
                key={key}
                onClick={() => onTeamSelected(team)}
                style={{
                  textAlign: 'center',
                  cursor: 'pointer',
                  padding: '8px 16px',
                  background: '#FFFFF',
                }}
                className="dropdown-option"
              >
                {team.name}
              </div>
            })}
          </>}
        >
          <div>{teamSelectionContext?.team?.name ?? ""}
            <CaretDownOutlined style={{ marginLeft: 2 }} />
          </div>
        </Dropdown>}
        <Dropdown
           placement="bottomCenter"
           arrow
          overlay={
            < div style={{backgroundColor: '#FFFFFF',borderRadius: '4px', boxShadow: '0px 0px 12px 8px rgba(0, 0, 0, 0.04)',}}>
            <Link  to={"/profile"}>
              <div
                style={{
                  textAlign: 'center',
                  cursor: 'pointer',
                  display:'flex',
                  justifyContent:'center',
                  alignItems:'center',
                  padding: '8px', 
                }}
              >
                <ProfileIcon />
                <span style={{marginLeft:'4px',color:'#0075FF',fontSize:'15px'}}>
               View Profile
                </span>
              </div>
              </Link>
              <div
                onClick={() => onLogout()}
                style={{
                  textAlign: 'center',
                  cursor: 'pointer',
                  display:'flex',
                  justifyContent:'center',
                  alignItems:'center',
                  padding: '10px',
                }}
              >
                <LogoutIcon />
                <span  style={{marginRight:'24px',marginLeft:'4px',color:'#EF4444',fontSize:'16px'}}>
                Logout
                </span>
              </div>
              
              {/* <Link
                to={`/admin/${user_id}/edit`}
                style={{
                  textAlign: 'center',
                  cursor: 'pointer',
                  padding: '8px 16px',
                  background: '#eee',
                }}
                className="dropdown-option"
              >
                Edit Profile
              </Link> */}
            </div>
          }
        >
          <div className="profile-image">
            <img alt="profile" src={defaultImage} />
          </div>
        </Dropdown>
      </FlexRowWrapper>
    </NavbarWrapper>
  );
};

export default Navbar;
