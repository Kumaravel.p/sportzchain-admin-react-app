import styled from 'styled-components/macro';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Card } from 'antd';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import Text from 'antd/lib/typography/Text';
import { ReactComponent as MoreIcon } from 'assets/icons/more.svg';
import { ReactComponent as CalenderIcon } from 'assets/icons/calendar.svg';

const AdoptionGraphsWrapper = styled(FlexRowWrapper)`
  column-gap: 32px;
  .title-cta-wrapper {
    justify-content: space-between;
    margin-bottom: 20px;
    .t-title {
      font-family: Montserrat;
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 132%;
      color: #333333;
    }
  }
`;

interface AdoptionGraphsProps {}

const AdoptionGraphs = ({}: AdoptionGraphsProps): JSX.Element => {
  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
  );

  const options = {
    responsive: true,
    scaleShowLabels: false,
    scale: {
      display: false,
      ticks: {
        display: false,
      },
    },
    plugins: {
      legend: {
        position: 'bottom' as const,
      },
      title: {
        // display: true,
        // text: 'Chart.js Line Chart',
      },
    },
  };

  const labels = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
  ];

  const data = {
    labels,
    datasets: [
      {
        label: 'Dataset 1',
        data: [
          1, 2, 3, 4, 52, 6, 7, 8, 9, 6, 4, 3, 7, 8, 9, 12, 5, 3, 2, 3, 4, 5, 6,
          8, 9, 0, 6, 5, 4, 5, 6, 7, 3, 9, 9,
        ],
        borderColor: 'rgb(255, 99, 132)',
        backgroundColor: 'rgba(255, 99, 132, 0.5)',
      },
      {
        label: 'Dataset 2',
        data: [
          9, 9, 3, 7, 6, 5, 4, 5, 6, 0, 9, 8, 6, 5, 4, 3, 2, 3, 5, 12, 9, 8, 7,
          3, 4, 6, 9, 8, 7, 6, 5, 4, 3, 2, 1,
        ],
        borderColor: 'rgb(53, 162, 235)',
        backgroundColor: 'rgba(53, 162, 235, 0.5)',
      },
    ],
  };

  return (
    <AdoptionGraphsWrapper>
      <Card>
        <FlexRowWrapper className="title-cta-wrapper">
          <Text className="t-title">User Adoption</Text>
          <MoreIcon />
        </FlexRowWrapper>
        <Line options={options} data={data} />
      </Card>
      <Card>
        <FlexRowWrapper className="title-cta-wrapper">
          <Text className="t-title">Reward vs adoption</Text>
          <CalenderIcon />
        </FlexRowWrapper>
        <Line options={options} data={data} />
      </Card>
    </AdoptionGraphsWrapper>
  );
};

export default AdoptionGraphs;
