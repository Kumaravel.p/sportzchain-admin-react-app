import styled from 'styled-components/macro';

const Spinner = styled.div`
  display: inline-flex;
  flex-shrink: 0;
  width: 50px;
  height: 50px;
  border-radius: 50%;
  border: 3px solid #fb923c;
  border-top-color: #737373;
  animation: spin 1s ease-in-out infinite;

  @keyframes spin {
    to {
      transform: rotate(360deg);
    }
  }
`;

export default Spinner;
