import * as React from 'react';
import { Avatar, Button, Card, Row, Col, Typography, Input, Upload, Spin, Tooltip, Space } from 'antd';
import { DeleteIcon } from 'components/svg/delete';
import styled from 'styled-components/macro';
import currentSession from 'utils/getAuthToken';
import { useGetUserByIdLazyQuery, useUpdateUserByIdMutation } from 'generated/pgraphql';
import { toast } from "react-toastify";
import config from 'config';
import { s3FileUpload } from 'apis/storage';
import { Auth } from 'aws-amplify';
import { useHistory } from 'react-router-dom';
import { AccountContext } from 'context/AccountContext';
import { InfoCircleOutlined } from '@ant-design/icons';
import { passwordStrength, Result } from 'check-password-strength';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const ProfileMainWrapper = styled.div`
padding: 0 40px;
width: 100%;
background: #E5E5E5;
.row{
    display: flex;
    justify-content: space-between;
    align-items:center;
    margin-top:20px
  }
  .text-profile{
    font-weight: 600;
    font-size: 25px;
    color: #051F24;
    flex:1;
  }
.save-button{
    text-align: center;
    background: #369AFE;
    color: #FFFFFF;
    border-radius: 5px;
    font-weight: 500;
    font-size: 16px;
    text-transform: uppercase; 
    width: 175px;
    height: 52px;
  }
  .discard-button{
    border: 1px solid #369AFE;
    border-radius: 5px;
    color: #369AFE;
    text-align: center;
    text-transform: uppercase;
    font-weight: 500;
    font-size: 16px;
    width: 131px;
    height: 52px;
    margin-left:24px;
  }
  .Upload-Photo-button{ 
    background: #369AFE;
    color: #FFFFFF;
    border-radius: 5px;
    font-weight: 500;
    font-size: 16px; 
    width: 150px;
    height: 40px;
    margin-top:16px;
    margin-left:14px;
    position: relative;
    & input{
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      z-index: 1;
      opacity:0;
      position: absolute;
      cursor: pointer;
    }
  }
  .vertical {
    border-left: 1.3px solid #EBEBEB;
    height: 400px;
  }
  .horizontal{
    border-bottom: 1.3px solid #EBEBEB;
    margin-left:14px;
    margin-top:30px;
  }
  .Remove-Photo-button{
    border: 1px solid #EF4444;
    border-radius: 5px;
    background: #FFFFFF;
    color: #EF4444;
    font-weight: 500;
    font-size: 16px;
    width: 150px;
    height: 40px;
    margin-top:16px;
    margin-left:14px;
  }
  .ant-card {
    margin-block: 40px;
    width: 100%;
    border-radius: 10px;
    padding:24px;
    .info-edit-wrapper {
      margin-bottom: 40px;
    }}
.ant-input{
  width: 302px;
}
.ant-input-password{
  width: 302px;
}
.text-label{
  font-weight: 400;
  font-size: 14px;
  color: #737373;
  margin-left:16px;
  margin-bottom:4px;
}
.text-email{
  font-weight: 400;
  font-size: 14px;
  color: #737373;
  margin-bottom:4px;
}
.text-password{
  font-weight: 400;
  font-size: 14px;
  color: #737373;
  margin-bottom:4px;
  margin-top:12px;
}
.text-username-email{
  background: #F5F5F5;
  border: 1px solid #D9D9D9;
  border-radius: 2px;
}
.text-reset{
  font-weight: 600;
  font-size: 16px;
  color: #171717;
  margin-left:16px;
  margin-top:16px;
}
.password-strength-wrapper {
    width:100%;
      .password-strength {
        width: 100%;
        /* display: flex; */
        column-gap: 2px;
        text-align: left;
        .ant-space-item{
            flex: 1;
            display: flex;
            justify-content: center;
        }
        .bottom-border{
            width: 60%;
            height: 2.5px;
            border-radius: 8px;
        }
        .level {
          background: #10b981;
        }
        .empty {
            background: #a3a3a3;
          }
      }
      .t-password-strength {
        display: flex;
        margin-left: 0;
        color: #10b981;
        margin-right: auto;
      }
}
.error-text{
  margin-top: 4px;
  font-size: 12px;
  color: red;
}
.error-input{
  border-color: red;
}
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    position: fixed;
    top:0;
    bottom:0;
    right:0;
    left: 0;
    z-index: 1;
    backdrop-filter: brightness(0.8);
`;

interface StateProps {
  fullname?: string;
  currentPassword?: string;
  newPassword?: string;
  confirmPassword?: string;
  imageId?: string;
}


interface ProfileMainProps { }

const ProfileMain = ({ }: ProfileMainProps): JSX.Element => {

  const { authenticate } = React.useContext(AccountContext);
  const history = useHistory();
  const [userData, setUserData] = React.useState<any>({});
  const [state, setState] = React.useState<StateProps>({});
  const [error, setError] = React.useState<StateProps>({});
  const [loading, setLoading] = React.useState<boolean>(false);
  const [passwordStrengthResult, setPasswordStrengthResult] = React.useState<Result<string> | null>(null);

  const [getUserDetails, { data, loading: userDataLoading }] = useGetUserByIdLazyQuery({
    notifyOnNetworkStatusChange: true,
    nextFetchPolicy: "network-only"
  })

  const [updateUser, { loading: updateUserLoading }] = useUpdateUserByIdMutation({
    notifyOnNetworkStatusChange: true,
  });
  const { profile } = useSnapshot(ProxyState);
  const user_id = profile?.user_id;

  React.useEffect(() => {
    currentSession().then(res => {
      setUserData(profile)
      if (!isNaN(user_id)) {
        getUserDetails({
          variables: {
            id: Number(user_id)
          }
        })
      }
      else {
        catchError('User id is missing')
      }
    })
  }, [])

  const catchError = (msg: string) => {
    console.log(msg)
    setLoading(false)
    toast.error(msg ? msg : "Something went wrong!")
    return false
  }

  const handleChange = (e: any) => {
    let file = e.target.files?.[0];
    if (file) {
      setLoading(true);
      s3FileUpload(file)
        .then((res: any) => {
          if (res?.data?.statusCode === "500") {
            catchError(res?.data?.message)
          }
          toast.success(res?.data?.message)
          setLoading(false);
          let fileId = res?.data?.fileId;
          onChangeState('imageId', fileId)
          setLoading(false);
        })
        .catch(e => catchError(e))
    }
    // choose same file for the next time
    e.target.value = ''
  }

  const onDelete = () => {
    onChangeState('imageId', null)
  }

  const onChangeState = (key: string, value: any) => {
    setState({
      ...state, [key]: value
    })
  }

  const onChangeNewPassword = (password: string) => {
    onChangeState("newPassword", password);
    const result = passwordStrength(password);
    setError({
      ...error,
      newPassword: result?.id < 3 ? `Password is ${result?.value}` : ""
    })
    setPasswordStrengthResult(result);
  }

  const onSave = async () => {

    const { currentPassword, newPassword, fullname, imageId } = state ?? {};

    let errorData = validate();
    setError(errorData)
    if (!Object.keys(errorData)?.length) {

      await updateUser({
        variables: {
          userData: {
            name: fullname,
            imageUrl: imageId
          },
          id: Number(userData?.user_id)
        }
      })
        .then((res: any) => {
          if (res.data.updateUserById?.user?.id) {
            toast.success("Profile Updated Successfully!");
          }
        })
        .catch(e => {
          toast.error(e?.message);
        })

      if (newPassword && currentPassword) {
        setLoading(true)
        toast.info('Changing password....')
        await Auth.currentAuthenticatedUser()
          .then(user => {
            return Auth.changePassword(user, currentPassword, newPassword)
              .then(res => {
                toast.success('Password changed successfully and You will be redirecting to the login', {
                  onClose: () => {
                    setLoading(false)
                    return Auth.signOut().then(() => {
                      window.location.replace(window.location.origin);
                    });
                  }
                });
              })
              .catch(e => catchError(e?.message))
          })
          .catch(e => catchError(e?.message))
      }
      else {
        setLoading(false)
        getUserDetails({
          variables: {
            id: Number(userData?.user_id)
          }
        })
      }

    }
  }

  const userDetails = React.useMemo(() => data?.userById, [data]);

  let checkFields = () => ['currentPassword', 'newPassword', 'confirmPassword'].some((key: string) => state?.[key as keyof StateProps]);

  const validate = () => {

    let errorState: StateProps = {};

    if (checkFields()) {
      ['currentPassword', 'newPassword', 'confirmPassword', 'fullname'].map((key: string) => {
        let value = state?.[key as keyof StateProps]
        if (!value) {
          errorState[key as keyof StateProps] = "This field is required"
        }
        return key
      })
      if (state?.newPassword && (!passwordStrengthResult?.id || (passwordStrengthResult?.id < 3))) {
        errorState["newPassword"] = `Password is ${passwordStrengthResult?.value}`
      }
      if (state?.newPassword !== state?.confirmPassword) {
        errorState["confirmPassword"] = "Password doesn't match"
      }
    }
    else {
      if (!(!!state?.fullname)) {
        errorState["fullname"] = "This field is required"
      }
    }
    return errorState
  }

  React.useEffect(() => {
    if (userDetails) {
      setState({
        ...state,
        fullname: userDetails?.name ?? "",
        imageId: userDetails?.imageUrl ?? ""
      })
    }
  }, [userDetails])

  return (
    <ProfileMainWrapper>
      {
        (userDataLoading || loading || updateUserLoading) &&
        <SpinWrapper>
          <Spin />
        </SpinWrapper>
      }
      <div className='row'>
        <Typography className='text-profile'>Profile</Typography>
        <Space size={16}>
          <Button className="save-button" onClick={onSave}>Save changes</Button>
          <Button className="discard-button" onClick={() => history.goBack()}>Discard</Button>
        </Space>
      </div>
      <Card>
        <Row>
          <Col span={18} push={6}>
            <div className='vertical'>
              <Typography className='text-label'>Full Name</Typography>
              <div style={{ marginLeft: '14px' }}>
                <Input
                  placeholder="Enter Your Name"
                  size="large"
                  value={state?.fullname}
                  onChange={e => onChangeState('fullname', e.target.value)}
                  className={`${error?.fullname ? "error-input" : ""}`}
                />
                {error?.fullname && <Typography.Title level={4} className="error-text">{error?.fullname}</Typography.Title>}
              </div>
              <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '20px' }}>
                <div>
                  <Typography className='text-label'>Username</Typography>
                  <div style={{ marginLeft: '14px' }}>
                    <Input placeholder="Enter User Name" disabled size="large" className='text-username-email' value={userDetails?.username ?? ""} />
                  </div>
                </div>
                <div>
                  <Typography className='text-email'>Email</Typography>
                  <Input placeholder="Enter Your Email" disabled size="large" className='text-username-email' value={userDetails?.email ?? ""} />
                </div>
              </div>
              <div className='horizontal'></div>
              <Typography className='text-reset'>Reset Password</Typography>
              <div style={{ marginLeft: '14px' }}>
                <Typography className='text-password'>Current Password</Typography>
                <Input.Password
                  placeholder="Current Password"
                  autoComplete="new-password"
                  value={state?.currentPassword}
                  onChange={e => onChangeState('currentPassword', e.target.value)}
                  className={`ant-input-password ${error?.currentPassword ? "error-input" : ""}`}
                />
                {error?.currentPassword && <Typography.Title level={4} className="error-text">{error?.currentPassword}</Typography.Title>}
              </div>
              <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: '20px' }}>
                <div style={{ marginLeft: '14px' }}>
                  <Typography className='text-password'>New Password</Typography>
                  <Input.Password
                    placeholder="New Password"
                    value={state?.newPassword}
                    className={`ant-input-password ${error?.newPassword ? "error-input" : ""}`}
                    onChange={e => onChangeNewPassword(e.target.value)}
                    prefix={(
                      <Tooltip placement="right" title={(
                        <>
                          Password must contain at least 8 characters
                          <ul>
                            <li>lower-case letters (a, b, c)</li>
                            <li>upper-case letters (A, B, C)</li>
                            <li>digits (1, 2 3)</li>
                            <li>
                              special characters,” which include punctuation <br /> (. ; !) and
                              other characters (# * &)
                            </li>
                          </ul>
                        </>
                      )}
                        className={`${error?.newPassword ? "error-input" : ""}`}
                      >
                        <InfoCircleOutlined style={{ color: "rgba(0, 0, 0, 0.45)" }} />
                      </Tooltip>
                    )}
                  />
                  {error?.newPassword && checkFields() && <Typography.Title level={4} className="error-text">{error?.newPassword}</Typography.Title>}
                  <div className="password-strength-wrapper">
                    <Space align='center' className="password-strength">
                      {
                        new Array(4)
                          .fill(0)
                          .map((_, i) => (
                            <div className={
                              `bottom-border ${(state?.newPassword && passwordStrengthResult && (passwordStrengthResult.id >= i)) ? "level" : "empty"}`
                            }
                              key={i}
                            />
                          ))}
                    </Space>
                  </div>
                </div>
                <div>
                  <Typography className='text-password'>Confirm New Password</Typography>
                  <Input.Password
                    placeholder="Confirm New Password"
                    value={state?.confirmPassword}
                    className={`'ant-input-password ${error?.confirmPassword ? "error-input" : ""}`}
                    onChange={e => onChangeState('confirmPassword', e.target.value)}
                  />
                  {error?.confirmPassword && <Typography.Title level={4} className="error-text">{error?.confirmPassword}</Typography.Title>}
                </div>
              </div>
            </div>
          </Col>
          <Col span={6} pull={18}>
            <div style={{ marginTop: '4rem', marginLeft: '12px' }}>
              <Avatar size={150} src={`${config.apiBaseUrl}files/${state?.imageId}`}
                style={{ border: '4px solid #F2F2F2' }}>
                {userDetails?.name?.charAt(0)}
              </Avatar>
            </div>
            <div>
              <Button className="Upload-Photo-button">
                Upload Photo
                <input
                  type="file"
                  onChange={handleChange}
                  accept="image/*"
                />
              </Button>
            </div>
            <div>
              <Button className="Remove-Photo-button" onClick={onDelete}><DeleteIcon /> <span style={{ marginLeft: '4px' }}>Remove Photo</span></Button>
            </div>
          </Col>
        </Row>
      </Card>
    </ProfileMainWrapper>
  );
};

export default ProfileMain;
