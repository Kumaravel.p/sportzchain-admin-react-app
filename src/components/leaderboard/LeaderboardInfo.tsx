import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import { Menu, Dropdown, Button, Table, Divider } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import { leaderboardColumns } from 'data/leaderboard';

import { GetLeaderboardInfoQuery } from 'generated/graphql';

const LeaderboardInfoWrapper = styled.div`
  .filters-wrapper {
    padding: 40px 0;
    column-gap: 25px;
    align-items: center;
    input {
      padding: 9px;
      background: rgba(5, 31, 36, 0.05);
      border: 1px solid rgba(5, 31, 36, 0.05);
      box-sizing: border-box;
      border-radius: 5px;
    }
  }

  .ant-table-thead > tr > th {
    background: transparent;
  }

  .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
    background: transparent;
  }

  .table-user-avatar {
    width: 44px;
    height: 44px;
    border-radius: 10px;
  }
  .t-table-status {
    font-weight: normal;
    font-size: 16px;
    line-height: 19px;
    color: #07697d;
  }
  .ant-dropdown-trigger {
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.4px;
    color: #9c9c9c;
    font-family: 'Montserrat', sans-serif;
    padding: 20px;
    display: flex;
    align-items: center;
  }
  .ant-table-content {
    .ant-table-thead {
      .ant-table-cell {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #9ab1b6;
      }
    }
    .ant-table-row {
      font-size: 16px;
      line-height: 19px;
      font-weight: 500;
      color: #051f24;
    }
  }
  .add-new-button {
    border: 1px solid rgba(7, 105, 125, 0.25);
    color: rgba(7, 105, 125, 1);
    box-sizing: border-box;
    border-radius: 5px;
    height: fit-content;
    padding: 15px;
    display: flex;
    align-items: center;
    column-gap: 10px;
    margin-left: auto;
    margin-right: 0;
  }
  .table-text-blue {
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    color: #07697d;
  }
`;

interface LeaderboardInfoProps {
  leaderboardInfo: GetLeaderboardInfoQuery['platformLeaderboard'];
}

const menu = (
  <Menu>
    <Menu.Item key="1" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="2" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="3" icon={<UserOutlined />}>
      Option
    </Menu.Item>
  </Menu>
);

const LeaderboardInfo = ({
  leaderboardInfo,
}: LeaderboardInfoProps): JSX.Element => {
  return (
    <LeaderboardInfoWrapper>
      <FlexRowWrapper className="filters-wrapper">
        <input type="text" placeholder="Search and filter..." />
        <Dropdown overlay={menu}>
          <Button>
            Leaderboard
            <DownOutlined />
          </Button>
        </Dropdown>
        <Dropdown overlay={menu}>
          <Button>
            RWRD
            <DownOutlined />
          </Button>
        </Dropdown>{' '}
        <Dropdown overlay={menu}>
          <Button>
            Last Rewarded
            <DownOutlined />
          </Button>
        </Dropdown>{' '}
      </FlexRowWrapper>

      <div>
        <Divider />
        <Table
          rowSelection={{
            type: 'checkbox',
            // ...rowSelection,
          }}
          columns={leaderboardColumns}
          dataSource={leaderboardInfo}
        />
      </div>
    </LeaderboardInfoWrapper>
  );
};

export default LeaderboardInfo;
