import styled from 'styled-components/macro';
import * as React from 'react';
import {
    Button,
    Card,
    Row,
    Form,
    Upload,
    Typography,
} from 'antd';
import BreadcrumbComp from 'components/common/breadcrumb';
import { useHistory, useParams, } from 'react-router-dom';
import TitleCta from 'components/common/TitleCta';
import { toast } from 'react-toastify';
import { s3FileUpload } from 'apis/storage';
import { DeleteOutlined } from '@ant-design/icons';
import config from 'config';
import { cmsCreate } from 'apis/cms/createList';
import { GetTeamImageList } from 'graphql_v2';
import graphqlReq from 'apis/graphqlRequest';

const TeamBannerWrapper = styled.div`
padding: 0 40px;
width: 100%;
height: 100%;
background: #E5E5E5;

.row{
  display: flex;
  justify-content: space-between;
  align-items:center;
  margin-top:20px
}
.save-button{
  text-align: center;
  background: #369AFE;
  color: #FFFFFF;
  border-radius: 5px;
  font-weight: 500;
  font-size: 16px;
  text-transform: uppercase; 
  width: 175px;
  height: 52px;
}
.discard-button{
  border: 1px solid #369AFE;
  border-radius: 5px;
  color: #369AFE;
  text-align: center;
  text-transform: uppercase;
  font-weight: 500;
  font-size: 16px;
  width: 131px;
  height: 52px;
  margin-left:24px;
}
.ant-card {
  margin-top: 40px;
  width: 100%;
  height: 100%;
  border-radius: 10px;
  // height: 563px;
  margin-bottom: 100px;
  .info-edit-wrapper {
    margin-bottom: 40px;
  }
.uploader{
display:flex;
justify-content:center;
}
.upload-button{
  height:172px;
  width: 395px;
  background: #FAFAFA;
  margin-top:40px;
  border: 1px solid #D9D9D9;
}


.upload-announcement-button{
  height:160px;
  width:160px;
  border: 1px dashed #D9D9D9;
}
.drag-upload{
  font-weight: 400;
  font-size: 16px;
  text-align: center;
  color: rgba(0, 0, 0, 0.85);
}
.support{
  text-align: center;
  color: rgba(0, 0, 0, 0.45);
  font-size: 14px;
  font-weight: 400;
  word-break: break-all;
  white-space: break-spaces;
}
// .ant-card-body {
//     padding: 12px;
//     height: 100%;
//     width: 100%;
// }
}
.remove-mb{
  margin-bottom: 0;
}

`

const HoverDiv = styled('div')`
position:relative;
cursor:pointer;
& img{
  width: 159px;
  height: 159px;
  object-fit:cover;
}
& > div{
  display:none;
  position:absolute;
  bottom:0;
  left:0;
  right:0;
}
& button{
  color:#fff;
  font-weight:bold;
  width:100%;
  &:hover{
    color:#fff;
  }
}
&:hover{
  & img{
    filter:brightness(0.6);
  }
  & > div{
    display:block;
  }
}
`

interface TeamImageBannerProps {
    cmsFormData?: any;
    contentId?: any;
    fileName?: string;
    TeamId?: string;
    
}


const TeamImageBanner = ({  }: TeamImageBannerProps): JSX.Element => {

    const [form] = Form.useForm();
    const history = useHistory();
    const { id, teamid, type } = useParams<{ id: string, teamid: string, type: string }>();
    const [Document, setDocument] = React.useState<any>([]);
    const [mobileImage, setMobileImage] = React.useState<any>([]);
    const [loading, setLoading] = React.useState<any>();

    const removeBannerLogo = (index: number, type: 'mobile' | 'web' = "web") => {
        if(type === "web"){
            let data = Document;
            data.splice(index, 1);
            setDocument([...data]);
        }
        else{
            let data = mobileImage;
            data.splice(index, 1);
            setMobileImage([...data]);
          }
    }

    const uploadImageType = async (file: any, type: 'mobile' | 'web' = "web") => {
        let fileupload = await s3FileUpload(file);
        if (fileupload.status === 200) {
            if(type === "web"){
                let data: any = Document;
                data.push({ fileName: fileupload.data.fileId });
                setDocument([...data]);
            }
            else{
                let data:any = mobileImage;
                data.push({ fileName: fileupload.data.fileId });
                setMobileImage([...data]);
              }
            toast.success("File uploaded successfully");
        }
        else {
            toast.error("File uploaded failed");
        }
    }

    const beforeUpload = (file: any) => {
        const allowedTypes = file.type === 'image/png' || file.type === 'image/jpeg';
        if (!allowedTypes) {
            toast.error(`${file.name} is not a png or jpeg file`);
        }
        return allowedTypes || Upload.LIST_IGNORE;
    }
    const Save = () => {
        if (Document.length > 0) {
            const main: any[] = [];
            Document.map((val: any) => main.push(val.fileName))
            cmsCreate({
                contentId: id,
                fileName: main,
                teamId: teamid,
                mobileViewFileName: mobileImage?.length ? mobileImage?.map((file: any) => file?.fileName) : []
            })
                .then((res) => {
                    toast.success(res.data.message)
                })
                .catch((ex) => {
                    toast.error('Something went wrong!')
                });
        }
    }

    React.useEffect(() => {
        setLoading(true)
        graphqlReq({ document: GetTeamImageList(id, parseInt(teamid)) })
            .then((res: any) => {
                if (res?.allContents?.nodes[0]?.masterContentTypeByTypeId?.name === 'IMAGE') {
                    let nodes = res?.allContents?.nodes[0]?.contentItemsByContentId?.nodes;
                    setDocument(nodes?.filter((_: any) => !_?.isMobileView))
                    setMobileImage(nodes?.filter((_: any) => _?.isMobileView))
                }
            })
            .catch(e => console.log(e))
    }, [])

    return (
        <TeamBannerWrapper>
            <Row className='row'>
                <BreadcrumbComp
                    backPath="/cms"
                    cta={
                        <>
                            <Button className="save-button" onClick={Save}>
                                Save changes
                            </Button>
                            {console.log(type,"?????")}
                            <Button
                                className="discard-button"
                                onClick={() => {
                                    debugger
                                    history.push(`/cms/${type}/${id}`)
                                }}
                            >
                                Discard
                            </Button>
                        </>
                    }
                    breadCrumbs={[
                        { name: 'CMS', url: "/cms" },
                        { name: "TeamName", url: "/cms/" }
                    ]}
                />
            </Row>
            <div>
                <>
                    <Card  className='remove-mb'>
                        <TitleCta title="Upload Team Banner - Web" />
                        <div className='uploader-announcement'>
                            <Typography>Banner Image (Image resolution should be 1136x200 pixels)</Typography>
                            <div style={{ display: "flex", alignItems: "center", gap: '16px', flexWrap: "wrap" }}>
                                {
                                    loading && Document?.map((item: any, index: number) => {
                                        console.log(loading, "loading")
                                        return <HoverDiv>
                                            <img src={`${config.apiBaseUrl}files/${item?.fileName}`} />
                                            <div>
                                                <Button type='text' onClick={() => removeBannerLogo(index)}>Remove<DeleteOutlined /></Button>
                                            </div>
                                        </HoverDiv>
                                    })

                                }
                                <Upload
                                    iconRender={() => <></>}
                                    maxCount={1}
                                    accept="image/png, image/jpeg"
                                    customRequest={async ({ file, fileList, onProgress }: any) => {
                                        await uploadImageType(file)
                                    }}
                                    beforeUpload={(file: any) => beforeUpload(file)}
                                    showUploadList={false}
                                >
                                    <Button className="upload-announcement-button">+</Button>
                                </Upload>
                            </div>
                        </div>
                    </Card>
                    <Card>
                        <TitleCta title="Upload Team Banner - Mobile" />
                        <div className='uploader-announcement'>
                            <Typography>Banner Image (Image resolution should be 1136x200 pixels)</Typography>
                            <div style={{ display: "flex", alignItems: "center", gap: '16px', flexWrap: "wrap" }}>
                                {
                                    loading && mobileImage?.map((item: any, index: number) => {
                                        return <HoverDiv>
                                            <img src={`${config.apiBaseUrl}files/${item?.fileName}`} />
                                            <div>
                                                <Button type='text' onClick={() => removeBannerLogo(index,"mobile")}>Remove<DeleteOutlined /></Button>
                                            </div>
                                        </HoverDiv>
                                    })

                                }
                                <Upload
                                    iconRender={() => <></>}
                                    maxCount={1}
                                    accept="image/png, image/jpeg"
                                    customRequest={async ({ file, fileList, onProgress }: any) => {
                                        await uploadImageType(file,"mobile")
                                    }}
                                    beforeUpload={(file: any) => beforeUpload(file)}
                                    showUploadList={false}
                                >
                                    <Button className="upload-announcement-button">+</Button>
                                </Upload>
                            </div>
                        </div>
                    </Card>
                </>
            </div>
        </TeamBannerWrapper>
    )
};
export default TeamImageBanner;