import React from 'react';
import { Row, Col, Card, Divider } from 'antd';
interface contactInfoProps {
    DataJSON:any;
    margin?: number;
    justify?: 'start'|'end'|'center'|'space-around'|'space-between';
}

const ViewCardGenerator = ({DataJSON, margin,justify="space-between"}: contactInfoProps): JSX.Element => {
    return(
        <Card style={{width: "100%", margin: `${margin ? margin : 30}px`, marginBottom:"0px"}}>
        {DataJSON?.map((val:any,i:any)=>{
            return(
                <div key={val?.title+""+i}>
                    <h2 className="bold">
                        {val?.title} &nbsp;&nbsp;
                        {val?.noOfRound ?
                            <span className="normal"  style={{backgroundColor:"#F4F4F4", color:"#07697D", padding: 6, borderRadius:"6px", fontSize: 16}}> 
                                Round 
                                <span className='bold'>
                                    &nbsp;{val?.noOfRound}
                                </span>
                            </span>
                        : null
                        }
                    </h2>
                    <br />
                    <Row justify={justify} align="bottom">
                        {val?.valuesArr?.map((val:any,j:any)=>{
                            return(
                                <>
                                    <Col lg={val?.col} key={val?.col+""+j+i}>
                                        <div style={{marginBottom:"2%", color:"gray", fontSize:14}}>{val?.lable}</div>
                                        <div  style={{marginBottom:"6%"}}><span style={{fontSize: 16, fontWeight:"500"}}>{val?.value}</span></div>
                                        {val?.divider ? <Divider /> : null}
                                    </Col>
                                </>
                            )
                        })}
                    </Row>
                </div>
            )
        })}
        </Card>
    )
};

export default ViewCardGenerator;
