import {
  CognitoUserPool,
  ICognitoUserPoolData,
} from 'amazon-cognito-identity-js';
import config from 'config';

const poolData: ICognitoUserPoolData = {
  UserPoolId: config.userPoolId,
  ClientId: config.clientId
};

export default new CognitoUserPool(poolData);
