import styled from 'styled-components/macro';
import { Card } from 'antd';
import TitleCtaTable from 'components/common/TitleCtaTable';
import { teamColumns } from 'data/team';
import { actionHistoryColumns } from 'data/actionHistory';
import { notesColumns } from 'data/notes';
import { teamDocumentsColumns } from 'data/teamDocuments';
import * as React from 'react';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import {
  useCreateTeamNoteMutation,
  useGetTeamAddressByIdQuery,
  useGetTeamAddressTypesQuery,
  useGetTeamAdminActionHistoriesByIdQuery,
  useGetTeamContactsByIdQuery,
  // useGetTeamContractsByIdQuery,
  useGetTeamDocumentsByIdQuery,
  useGetTeamNotesByIdQuery,
  useUpdateTeamNoteByIdMutation,
} from 'generated/graphql';
import TeamContactsModal from 'components/modals/team/ContactsModal';
import NoteModal from 'components/modals/team/NoteModal';
import DocumentModal from 'components/modals/team/DocumentModal';
import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import AddressModal from 'components/modals/team/AddressModal';
import TitleCta from 'components/common/TitleCta';
import { EyeOutlined } from '@ant-design/icons';
import { ternaryOperator } from 'utils';
import { useSnapshot } from 'valtio';
import { state } from 'state';

const BasicInfoWrapper = styled.div`
  .address-wrapper {
    .title-cta-wrapper {
      margin: 20px 0;
      justify-content: space-between;
      align-items: center;
      .t-title {
        font-weight: 600;
        font-size: 20px;
        line-height: 24px;
        color: #051f24;
      }
      .cta {
        font-weight: 600;
        font-size: 16px;
        line-height: 19px;
        color: #07697d;
      }
    }
    .address-details-wrapper {
      column-gap: 20px;

      .address-details {
        padding: 25px;
        row-gap: 20px;
        background: #f5f5f7;
        border: 0.7px solid rgba(5, 31, 36, 0.1);
        border-radius: 15px;
        .t-address-type {
          font-size: 18px;
          line-height: 22px;
          color: rgba(5, 31, 36, 0.6);
          display: flex;
          align-items: center;
          justify-content: space-between;
          .edit-icon {
            cursor: pointer;
          }
        }
        .t-address {
          font-size: 18px;
          line-height: 151.52%;
          color: #051f24;
        }
      }
    }
  }
`;

interface BasicInfoProps {
  id: string;
  permanentAddress?: {
    city?: string;
    country?: any;
    line1?: string;
    line2?: string;
    state?: string;
  };
  presentAddress?: {
    city?: string;
    country?: any;
    line1?: string;
    line2?: string;
    state?: string;
  };
}

const BasicInfo = ({
  id = '0',
}: // presentAddress,
// permanentAddress,
BasicInfoProps): JSX.Element => {
  const { profile } = useSnapshot(state);

  const user_id = profile?.user_id;

  const { data: teamAddress, refetch: refetchTeamAddress } =
    useGetTeamAddressByIdQuery({
      variables: { teamId: +id },
    });

  const { data: teamContacts, refetch: refetchTeamContacts } =
    useGetTeamContactsByIdQuery({
      variables: { teamId: +id },
    });
  const { data: teamNotes, refetch: refetchTeamNotes } =
    useGetTeamNotesByIdQuery({
      variables: { teamId: +id },
    });
  const { data: teamDocuments, refetch: refetchTeamDocuments } =
    useGetTeamDocumentsByIdQuery({
      variables: { teamId: +id },
    });

  const { data: teamAdminHistories } = useGetTeamAdminActionHistoriesByIdQuery({
    variables: { teamId: +id },
  });

  const { data: teamAddressTypes } = useGetTeamAddressTypesQuery();

  const [isContactModalVisible, setIsContactModalVisible] =
    React.useState(false);
  const [isNoteModalVisible, setIsNoteModalVisible] = React.useState(false);
  const [isAddressModalVisible, setIsAddressModalVisible] =
    React.useState(false);

  const [isDocumentModalVisible, setIsDocumentModalVisible] =
    React.useState(false);

  const [noteData, setNoteData] = React.useState<any>({});
  const [contactData, setContactData] = React.useState<any>({});
  const [documentData, setDocumentData] = React.useState<any>({});
  const [addressData, setAddressData] = React.useState<any>({});

  const presentAddress = teamAddress?.team_by_pk?.teamAddresses.filter(
    ({ teamAddressTypeId }: any) => teamAddressTypeId === 'PRESENT'
  )[0];
  const permanentAddress = teamAddress?.team_by_pk?.teamAddresses.filter(
    ({ teamAddressTypeId }: any) => teamAddressTypeId === 'PERMANENT'
  )[0];

  const [createTeamNote] = useCreateTeamNoteMutation();
  const [updateTeamNote] = useUpdateTeamNoteByIdMutation();


    const formatJson = React.useCallback(
      (val:string) => {
      try{
        return JSON.parse(val)?.label
      }
      catch{
        return ''
      }
    },[teamAddress]);

  const formatAddressData = (val: any) => {
    let modifyAddress = {
      ...val,
      countryTitle: val?.countryTitle ? JSON.parse(val?.countryTitle) : '',
      state: val?.state ? JSON.parse(val?.state) : ''
    }
    setAddressData(modifyAddress);
    setIsAddressModalVisible(true);
  }

  const formatContactData = (val: any) => {
    let modifyContactData = {
      ...val,
      primaryPhoneNumber:{
        countryCode:val?.primaryCountryCode,
        mobileNumber:val?.primaryPhoneNumber
      },
      secondaryPhoneNumber:{
        countryCode:val?.secondaryCountryCode,
        mobileNumber:val?.secondaryPhoneNumber
      }
    }
    setContactData(modifyContactData);
    setIsContactModalVisible(true);
  }

  return (
    <BasicInfoWrapper>
      <Card className="address-wrapper">
        <TitleCta
          title="Address"
          cta={
            <Text
              className="cta"
              onClick={() => setIsAddressModalVisible(true)}
            >
              + Add New Address
            </Text>
          }
        />
        <FlexRowWrapper className="address-details-wrapper">
          {presentAddress && (
            <FlexColumnWrapper className="address-details">
              <Text className="t-address-type">
                {presentAddress?.teamAddressTypeId} ADDRESS
                <EditIcon
                  onClick={() => {
                    formatAddressData(presentAddress);
                  }}
                  className="edit-icon"
                />
              </Text>
              <Text className="t-address">
                {presentAddress?.line1 ? ternaryOperator(`${presentAddress?.line1},`,null) : ""}
                {presentAddress?.line2 ? ternaryOperator(`${presentAddress?.line2},`,null) : ""}
                {presentAddress?.city ? ternaryOperator(`${presentAddress?.city},`,null) : ""}
                {presentAddress?.state ? ternaryOperator(`${formatJson(presentAddress?.state)},`,"") : ''}
                {presentAddress?.countryTitle ? formatJson(presentAddress?.countryTitle) : ''}{' - '}
                {presentAddress?.pincode ?? ''}
              </Text>
            </FlexColumnWrapper>
          )}

          {permanentAddress && (
            <FlexColumnWrapper className="address-details">
              <Text className="t-address-type">
                {permanentAddress?.teamAddressTypeId} ADDRESS
                <EditIcon
                  className="edit-icon"
                  onClick={() => {
                    formatAddressData(permanentAddress);
                  }}
                />
              </Text>
              <Text className="t-address">
                {permanentAddress?.line1 ? ternaryOperator(`${permanentAddress?.line1},`,null) : ""}
                {permanentAddress?.line2 ? ternaryOperator(`${permanentAddress?.line2},`,null) : ""}
                {permanentAddress?.city ? ternaryOperator(`${permanentAddress?.city},`,null) : ""}
                {permanentAddress?.state ? ternaryOperator(`${formatJson(permanentAddress?.state)},`,"") : ''}
                {permanentAddress?.countryTitle ? formatJson(permanentAddress?.countryTitle) : ''}{' - '}
                {permanentAddress?.pincode ?? ''}
              </Text>
            </FlexColumnWrapper>
          )}
        </FlexRowWrapper>
      </Card>

      <AddressModal
        isModalVisible={isAddressModalVisible}
        creatorId={user_id}
        setIsModalVisible={setIsAddressModalVisible}
        teamId={+id}
        addressData={addressData}
        setAddressData={setAddressData}
        refetchTeamAddress={refetchTeamAddress}
      />

      {/* <TitleCtaTable
        title="Contracts"
        cta={
          <div onClick={() => setIsContractModalVisible(true)}>
            + Add New Contract
          </div>
        }
        columns={[
          ...contractsColumns,
          {
            title: '',
            render: (values: any, index: any) => (
              <EditIcon
                onClick={() => {
                  setContractData(values);
                  setIsContractModalVisible(true);
                }}
              />
            ),
          },
        ]}
        data={teamContracts?.team_by_pk?.teamContracts ?? []}
      />
      <ContractModal
        isModalVisible={isContractModalVisible}
        creatorId={user_id}
        setIsModalVisible={setIsContractModalVisible}
        teamId={+id}
        teamContacts={teamContacts?.team_by_pk?.teamContacts}
        contractData={contractData}
        setContractData={setContractData}
        refetchTeamContracts={refetchTeamContracts}
      /> */}

      <TitleCtaTable
        title="Team"
        cta={
          <div onClick={() =>{
            setContactData({})
             setIsContactModalVisible(true)
            }}>+ Add Member</div>
        }
        columns={[
          ...teamColumns,
          {
            title: '',
            render: (values: any, index: any) => (
              <EditIcon
                onClick={() => {
                  formatContactData(values)
                }}
              />
            ),
          },
        ]}
        data={teamContacts?.team_by_pk?.teamContacts ?? []}
      />
      <TeamContactsModal
        creatorId={user_id}
        isModalVisible={isContactModalVisible}
        setIsModalVisible={setIsContactModalVisible}
        teamId={+id}
        contactData={contactData}
        setContactData={setContactData}
        refetchTeamContacts={refetchTeamContacts}
      />
      <TitleCtaTable
        title="Notes"
        cta={<div onClick={() => setIsNoteModalVisible(true)}>+ Add Note</div>}
        columns={[
          ...notesColumns,
          {
            title: '',
            render: (values: any, index: any) => (
              <EyeOutlined
                onClick={() => {
                  setNoteData(values);
                  setIsNoteModalVisible(true);
                }}
              />
            ),
          },
        ]}
        data={teamNotes?.team_by_pk?.teamNotes ?? []}
      />

      <NoteModal
        creatorId={user_id}
        isModalVisible={isNoteModalVisible}
        teamId={+id}
        noteData={noteData}
        setNoteData={setNoteData}
        refetchTeamNotes={refetchTeamNotes}
        setIsModalVisible={setIsNoteModalVisible}
        disabled={true}
      />
      <TitleCtaTable
        title="Document"
        cta={
          <div onClick={() => setIsDocumentModalVisible(true)}>
            + Add Document
          </div>
        }
        columns={[
          ...teamDocumentsColumns,
          {
            title: '',
            render: (values: any, index: any) => (
              <EditIcon
                onClick={() => {
                  setDocumentData(values);
                  setIsDocumentModalVisible(true);
                }}
              />
            ),
          },
        ]}
        data={teamDocuments?.team_by_pk?.teamDocuments ?? []}
      />
      <DocumentModal
        user_id={user_id}
        isModalVisible={isDocumentModalVisible}
        setIsModalVisible={setIsDocumentModalVisible}
        teamId={+id}
        documentData={documentData}
        setDocumentData={setDocumentData}
        refetchTeamDocuments={refetchTeamDocuments}
      />

      <TitleCtaTable
        title="Action History"
        columns={actionHistoryColumns}
        data={teamAdminHistories?.team_by_pk?.teamAdminActionHistories ?? []}
      />
    </BasicInfoWrapper>
  );
};

export default BasicInfo;
