import React, { useMemo, useCallback, useState } from 'react';
import { Button, Card, Col, Row, Typography } from 'antd';
import LabelValue from 'components/common/LabelValue';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import TitleCtaTable from 'components/common/TitleCtaTable';
import { transactionColumns } from 'data/transaction';
import styled from 'styled-components/macro';
import QRCode from 'react-qr-code';
import { useGetTeamWalletQuery, useGetStoByTeamIdQuery, GetStoByTeamIdQuery } from 'generated/graphql';
import { useClipboard } from 'use-clipboard-copy';

import { ReactComponent as CopyIcon } from 'assets/icons/copy.svg';
import { ReactComponent as CheckIcon } from 'assets/icons/check-icon.svg';
import SelectElement from 'components/common/SelectElement';
import moment from 'moment'
import { useHistory } from 'react-router-dom';
import { stoSpnValueByToken } from 'apis/sto';
import { toast } from 'react-toastify';

const TokenWrapper = styled.div`
  .title {
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
    color: #051f24;
  }
  .barcode-img {
    & > * {
      transform: scale(0.85);
    }
  }
  .t-card-title {
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
    color: #051f24;
    margin-bottom: 40px;
  }
  .spn-bal-wrapper,
  .barcode-actions {
    align-items: center;
  }
  .barcode-actions {
    align-items: center;
    cursor: pointer;
    column-gap: 8px;
  }
  .view-details {
    margin-top: 20px;
  }
  .token-section1{
    margin-bottom: 40px;
  }
  .token-section2{
    &.ant-row .ant-col{
      & > div{
        gap:16px;
      }  
    }
  }
  .label-style{
      color: #4DA1FF;
      font-size: 18px;
    }
`;

interface TokenProps {
  id: string | number;
}

const Token = ({ id }: TokenProps): JSX.Element => {
  const clipboard = useClipboard({ copiedTimeout: 1000 });
  const [round, setRound] = useState<number>(1);
  const [achievedValue, setAchievedValue] = useState<string>('0');
  const history = useHistory();

  const { data: wallet } = useGetTeamWalletQuery({
    variables: { teamId: +id },
  });

  const { data: token } = useGetStoByTeamIdQuery({
    variables: { teamId: +id },
  });

  const tokens = token?.stos as NonNullable<GetStoByTeamIdQuery["stos"]>;

  React.useEffect(() => {
    if (tokens?.length) {
      setRound(Math.max(...tokens?.map(_ => _?.round)) ?? 1)
    }
  }, [tokens])

  const roundOptions = useMemo(() => tokens?.length ? [...tokens]?.sort((a, b) => a?.round - b?.round)?.map(_ => ({ round: _?.round })) : [], [token]);

  const tokenData: any = useMemo(() => {
    if (!tokens?.length) return []
    return tokens?.find(_ => _?.round === round) ?? {}
  }, [round])

  const format = useCallback((date: Date) => moment(date).isValid() ? moment(date).format('DD MMM') : '', [tokenData]);

  const totalSoldInPercentage = useMemo(() => {
    let supplySizeInNumber = tokenData?.supplySize ? Number(tokenData?.supplySize) : 0;
    return tokenData?.totalSold / (supplySizeInNumber * 100);
  }, [tokenData])

  React.useEffect(() => {
    getAchievedValue()
  }, [tokenData])

  const getAchievedValue = async () => {
    let tokenSymbol = tokenData?.team?.wallets?.[0]?.coinSymbol;
    if (tokenSymbol) {
      await stoSpnValueByToken({ tokenSymbol })
        .then((res: any) => {
          if (res?.data?.statusCode === "500") {
            setAchievedValue("0");
            return false
          }
          let dollarValue = res?.data?.data?.dollarValue;
          setAchievedValue(dollarValue ? (tokenData?.totalSold * dollarValue).toString() : "0")
        })
        .catch(e => {
          console.log(e)
          toast.error('Something went wrong!')
          setAchievedValue("0")
        })
    }
  }

  const returnStatus = useMemo(() => {
    if (moment(tokenData?.displayStartDate).isAfter()) {
      return 'Upcoming'
    }
    else if (moment(tokenData?.displayEndAt).isBefore()) {
      return 'Closed'
    }
    return 'Ongoing'
  }, [tokenData])

  const viewDetails = () => {
    history.push('/view-sto', { id: tokenData?.id })
  }

  const walletData = wallet?.wallet;

  const isCopied = clipboard.copied ? <CheckIcon /> : <CopyIcon />;

  return (
    <TokenWrapper>
      <div className="title">Token</div>
      <Card>
        {
          tokenData?.id ? (
            <>
              <Row gutter={[24, 24]} className="token-section1">
                <Col xs={6} md={18}><Typography.Text className="title">Sto</Typography.Text></Col>
                <Col xs={18} md={6}>
                  <SelectElement
                    onChange={(e: any) => {
                      setRound(e);
                    }}
                    value={round}
                    options={roundOptions}
                    toFilter="round"
                    placeholder="Round"
                  />
                </Col>
              </Row>
              <Row gutter={[24, 24]} className="token-section2">
                <Col xs={24} sm={12} md={4}>
                  <LabelValue
                    field={`Token Sold`}
                    value={<Typography.Text className='label-style'>{`${totalSoldInPercentage ?? 0}%`}</Typography.Text>}
                  />
                </Col>
                <Col xs={24} sm={12} md={4}>
                  <LabelValue
                    field={'Status'}
                    value={<Typography.Text className='label-style'>{returnStatus ?? '-'}</Typography.Text>}
                  />
                </Col>
                <Col xs={24} sm={12} md={4}>
                  <LabelValue
                    field={'Initial Price'}
                    value={`${tokenData?.initialPrice ?? 0} USD`}
                  />
                </Col>
                <Col xs={24} sm={12} md={4}>
                  <LabelValue
                    field={'Time Period'}
                    value={`${format(tokenData?.displayStartDate)} - ${' '} ${format(tokenData.displayEndAt)}`}
                  // value={'23 Feb - 7 March'}
                  />
                </Col>
                <Col xs={24} sm={12} md={4}>
                  <LabelValue
                    field={'Target'}
                    value={`$ ${tokenData?.targetAmount ?? 0}`}
                  />
                </Col>
                <Col xs={24} sm={12} md={4}>
                  <LabelValue
                    field={'Acheived'}
                    value={`$ ${achievedValue}` ?? '-'}
                  />
                </Col>
                <Col xs={24}>
                  <Button
                    onClick={viewDetails}
                    type="primary"
                    style={{ borderRadius: 4 }}
                  >
                    View Details
                  </Button>
                </Col>
              </Row>
            </>
          ) : (
            <Typography.Title level={4}>No Token Found</Typography.Title>
          )
        }
      </Card>
      <Card>
        <div className="t-card-title">Wallet</div>
        <FlexRowWrapper>
          {walletData?.map((wallet) => (
            <FlexRowWrapper className="spn-bal-wrapper">
              <LabelValue
                field={`${wallet.coinSymbol} Balance`}
                value={wallet.balance}
              />
            </FlexRowWrapper>
          ))}

          {
            (walletData && walletData[0]?.walletAddress) ?
              <>
                <FlexColumnWrapper>
                  <div className="barcode-img">
                    <QRCode size={64} value={walletData[0]?.walletAddress ?? ''} />
                  </div>
                  <FlexRowWrapper
                    className="barcode-actions"
                    onClick={() => {
                      clipboard.copy(walletData[0]?.walletAddress);
                    }}
                  >
                    <>
                      {walletData[0]?.walletAddress ?? '-'}
                      {isCopied}
                    </>
                  </FlexRowWrapper>
                </FlexColumnWrapper>
              </> : (
                <Typography.Title level={4}>No Wallet Found</Typography.Title>
              )
          }
        </FlexRowWrapper>
      </Card>

      <TitleCtaTable
        title="Transaction history"
        cta="+ Sort And Filter"
        data={[]}
        columns={transactionColumns}
      />
    </TokenWrapper>
  );
};

export default Token;
