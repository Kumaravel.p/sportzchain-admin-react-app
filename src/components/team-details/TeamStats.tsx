import { Card } from 'antd';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';

const TeamStatsWrapper = styled.div`
  .cards-wrapper {
    justify-content: space-between;
    flex-wrap: wrap;
    .count-monthly-wrapper {
      column-gap: 10px;
      align-items: flex-end;
      justify-content: space-between;

      .t-month {
        font-size: 14px;
        line-height: 19px;
        letter-spacing: -0.1px;
        color: rgba(50, 60, 71, 0.4);
      }
      .t-stat-price {
        font-size: 24px;
        line-height: 28px;
        color: #4da1ff;
        span {
          /* Red / 1 */
          align-self: flex-end;
          box-sizing: border-box;
          border-radius: 100px;
          font-size: 12px;
          line-height: 16px;
          letter-spacing: 0.4px;
          color: #ffffff;
          padding: 1px 9px;
          &.red {
            border: 2px solid #ff5756;

            background: #ff5756;
          }
          &.green {
            border: 2px solid #4aaf05;
            background: #4aaf05;
          }
        }
        &.red {
          color: #ff6d4a;
        }
      }
    }
  }
`;

interface TeamStatsProps {}

const TeamStats = ({}: TeamStatsProps): JSX.Element => {
  return (
    <TeamStatsWrapper>
      <FlexRowWrapper className="cards-wrapper">
        <Card
          title="MI Market Price"
          extra={<MoreIcon />}
          style={{ width: 300 }}
        >
          <FlexRowWrapper className="count-monthly-wrapper">
            {/* <FlexRowWrapper> */}
            <Text className="t-stat-price">
              $30/MI
              <span className="green">7.00%</span>
            </Text>
            <Text className="t-month">This Month</Text>

            {/* <Text className="t-month">Gas Fees: $90 </Text> */}
          </FlexRowWrapper>
        </Card>
        <Card title="Total Members" extra={<MoreIcon />} style={{ width: 300 }}>
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-stat-price red">
              80000
              <span className="red">7.00%</span>
            </Text>
            <Text className="t-month">This Month</Text>
          </FlexRowWrapper>
        </Card>{' '}
        <Card
          title="Total Marketcap"
          extra={<MoreIcon />}
          style={{ width: 300 }}
        >
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-stat-price">
              400M
              <span className="red">7.00%</span>
            </Text>
            <Text className="t-month">This Month</Text>
          </FlexRowWrapper>
        </Card>
        <Card
          title="Total Managing Cost"
          extra={<MoreIcon />}
          style={{ width: 300 }}
        >
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-stat-price">400M</Text>
            <Text className="t-month">This Month</Text>
          </FlexRowWrapper>
        </Card>{' '}
        <Card title="Gas Fees" extra={<MoreIcon />} style={{ width: 300 }}>
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-stat-price">400M</Text>
            <Text className="t-month">This Month</Text>
          </FlexRowWrapper>
        </Card>{' '}
        <Card title="Hosting Games" extra={<MoreIcon />} style={{ width: 300 }}>
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-stat-price">400M</Text>
            <Text className="t-month">This Month</Text>
          </FlexRowWrapper>
        </Card>
      </FlexRowWrapper>
    </TeamStatsWrapper>
  );
};

export default TeamStats;
