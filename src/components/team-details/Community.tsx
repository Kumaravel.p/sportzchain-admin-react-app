import styled from 'styled-components/macro';
import React, { useState } from 'react';
import { getCommunityByTeamID } from 'apis/teamsApi';
import { toast } from 'react-toastify';
import { TeamCommunityPayload } from 'ts/interfaces/teams.interface';
import { useDebounce } from 'hooks/useDebounce';
import { Col, Input, Row, TablePaginationConfig, Typography } from 'antd';
import CustomTable from 'components/customTable';
import { ReactComponent as SearchIcon } from 'assets/icons/searchIcon.svg';
import config from 'config';

const CommunityWrapper = styled.div`
  & .search-input{
    border-radius: 5px;
    /* border-color:#051F24; */
  }
`;

interface CommunityProps {
  id: string;
  coinSymbol?: string;
}

interface TableDataType {
  avatar?: { src?: string, firstChar?: string };
  name: string;
  tokens: string;
  rank: string;
  leaderboard: string;
}

const tableColumns = [
  { title: 'Avatar', dataIndex: "avatar", component: 'avatar' },
  { title: 'Name', dataIndex: "name", sorter: (a: any, b: any) => a.name.localeCompare(b.name) },
  { title: 'Tokens', dataIndex: "tokens",sorter: (a: any, b: any) => a.tokens.localeCompare(b.tokens) },
  { title: 'Rank', dataIndex:'rank', sorter: (a: any, b: any) => a.rank.localeCompare(b.rank)},
  { title: 'Leaderboard', dataIndex:'leaderboard',sorter: (a: any, b: any) => a.leaderboard.localeCompare(b.leaderboard) }
]

let pageSize = 5;

const Community = ({ id }: CommunityProps): JSX.Element => {

  //debounce hook
  const debounce = useDebounce();

  const [pagination, setPagination] = useState<TablePaginationConfig>({
    current: 1,
    pageSize,
    // total: 6,
  });

  const [searchValue, setSearchValue] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [dataSource, setDataSource] = useState<TableDataType[]>([]);


  React.useEffect(() => {
    getCommunityList()
  }, [])

  const getCommunityList = async (offset: number = 1, limit: number = pageSize, searchText: string = "", from: string = "initial") => {
    setLoading(true);

    let payload: TeamCommunityPayload = {
      "start": ((offset * limit) - limit),
      "length": limit,
      "member": searchText,
      "teamId": +id,
    }

    getCommunityByTeamID(payload)
      .then((res: any) => {
        let data = res?.data?.data?.userArray ?? [];
        if (data?.length) {
          let modifyData = data.map((_: any) => ({
            avatar: {
              src: `${config.apiBaseUrl}files/${_?.userByUserId?.imageUrl}`,
              firstChar: _?.userByUserId?.name?.charAt(0)
            },
            name: _?.userByUserId?.name ?? '-',
            tokens: `${_?.userByUserId?.userTransactionsByUserId?.aggregates?.sum?.fromCoinAmount ?? 0} ${_?.teamByTeamId?.walletsByTeamId?.nodes?.[0]?.coinSymbol ?? ""}`,
            rank:_?.rank ?? '_',
            leaderboard:_?.type ?? '_',
          }))
          setDataSource(modifyData);
          setPagination({
            ...pagination,
            current: from === "initial" ? 1 : offset,
            pageSize: limit,
            total: res?.data?.data?.count ?? 0
          })
        }
        setLoading(false)
      })
      .catch((e: any) => {
        console.log(e);
        setLoading(false)
        toast.error('Something went wrong!')
      })
  }

  const onSearch = (value: string) => {
    setSearchValue(value)
    debounce(() => getCommunityList(1, pageSize, value), 800);
  }

  const onChangeTable = (newPagination: TablePaginationConfig) => {

    const { current, pageSize: _pageSize } = newPagination;
    let offset = current;
    let from = "pagination"
    if (pagination?.pageSize !== newPagination?.pageSize) {
      offset = 1;
      from = ""
    }
    getCommunityList(offset, _pageSize, searchValue, from)
  }

  return (
    <CommunityWrapper>
      <Row gutter={[24, 24]} style={{ marginBottom: 24 }}>
        <Col xs={6} md={18}>
          <Typography.Title level={5}>Users ({pagination?.total ?? 0})</Typography.Title>
        </Col>
        <Col xs={18} md={6}>
          <Input
            value={searchValue}
            onChange={(e) => onSearch(e.target.value)}
            className="search-input"
            placeholder='Search Name'
            prefix={<SearchIcon />}
          />
        </Col>
      </Row>
      <CustomTable
        columns={tableColumns}
        dataSource={dataSource}
        loading={loading}
        pagination={pagination}
        onChangeTable={onChangeTable}
      />
    </CommunityWrapper>
  );
};

export default Community;
