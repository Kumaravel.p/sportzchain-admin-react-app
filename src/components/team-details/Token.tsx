import { Card } from 'antd';
import { Form, Input, Row, Col, Divider, Button } from 'antd';
import { useClipboard } from 'use-clipboard-copy';
import QRCodeImg from "../../assets/images/qrcode.png"
import { CopyOutlined, ShareAltOutlined } from '@ant-design/icons';
import TokenTable from "./TokenTable";

interface TokenProps {
  id: string | number;
}

const Token = ({ id }: TokenProps): JSX.Element => {
  const clipboard = useClipboard({ copiedTimeout: 1000 });
  return (
    <div>
      <h1 style={{fontWeight:"bold"}}>Wallet</h1>
      <Card style={{width: "100%", height: "210px"}}>
        <h3 style={{fontWeight:"bold", margin:0}}>Wallet</h3>
        <Row justify="space-between" align="bottom">
          <Col lg={9}>
            <p style={{fontWeight:"bold", color:"#051F2499", fontSize:"14"}}>SPN Balance</p>
            <p style={{fontSize:"16px", margin:0, fontWeight:"600"}}>100,000 SPN <span style={{color:"#4da1ff", fontSize: "14px"}}>($400,000)</span></p>
          </Col>
          <Col lg={7} style={{textAlign:"center"}}>
            <div style={{textAlign:"left"}}>
              <p style={{fontWeight:"bold", color:"#051F2499", fontSize:"14"}}>CSK Balance</p>
              <p style={{fontSize:"16px", margin:0, fontWeight:"600"}}>100,000 CSK <span style={{color:"#4da1ff", fontSize: "14px"}}>($400,000)</span></p>
            </div>
          </Col>
          <Col lg={8} style={{textAlign:"right"}}>
            <div>
              <img src={QRCodeImg} style={{width:"26%"}}/>
            </div>
            <div style={{padding: "2%"}}>
              <div data-middle-ellipsis="99">
                <span style={{marginRight: 8}}>b405839ee0...809e14f09db</span>
                <CopyOutlined 
                  onClick={() => {
                    clipboard.copy("b40583ec239ee09c2b64aa4517eb5809e14f09db");
                  }}
                  style={{marginRight: 8}}
                />
                <ShareAltOutlined />
              </div>
            </div>
          </Col>
        </Row>
      </Card>

      <Card>
        <h3 style={{fontWeight:"bold", marginBottom: "4%"}}>Transaction History</h3>
        <TokenTable />
      </Card>
    </div>
  );
};

export default Token;
