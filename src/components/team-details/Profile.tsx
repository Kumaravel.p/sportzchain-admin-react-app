import {
  Button,
  Card,
  Carousel,
  Col,
  Divider,
  Input,
  Row,
  Form,
  Upload,
  Tooltip,
  Spin,
} from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import LabelValue from 'components/common/LabelValue';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import LabelInput from 'components/common/LabelInput';
import styled from 'styled-components/macro';
import * as React from 'react';
import {
  useGetTeamDisplayInfoQuery,
  useUpdateTeamDisplayInfoMutation,
} from 'generated/graphql';
import SocialMediaModal from 'components/modals/team/SocialMediaModal';
import LogoBannerModal from 'components/modals/team/LogoBannerModal';
import BannerModal from 'components/modals/team/BannerModal';
import { teamData } from 'data/team';
import TitleCta from 'components/common/TitleCta';
import VideoModal from 'components/modals/team/VideoModal';
import { RcFile } from 'antd/lib/upload';
import { ternaryOperator } from '../../utils';
import { toast } from 'react-toastify';
import { Editor, EditorState } from 'draft-js';
import 'draft-js/dist/Draft.css';

import { ReactComponent as Stripes } from 'assets/images/team-card-stripes.svg';
import TextEditorInput from 'components/common/TextEditorInput';
import config from 'config';
import { s3FileUpload } from '../../apis/storage';
import { isValidHttpUrl, matchYoutubeUrl } from 'utils/functions';
import { LiveTeamCard } from 'components/NewTeams/liveTeamCard';

const ProfileWrapper = styled.div`
  .banner-image {
    /* text-align: center; */
    margin: 20px 0;
  }
  .t-banner-info {
    font-size: 15px;
    line-height: 18px;
    color: rgba(5, 31, 36, 0.6);
  }
  .title {
    font-weight: 600;
    font-size: 20px;
    line-height: 24px;
    color: #051f24;
  }
  .colours-card {
    width: 100%;
  }
  .token-card-preview {
    padding: 24px;
    width: 272px;
    background: #262626;
    border-radius: 8px;
    row-gap: 8px;
    margin-left: 12px;
    /* & { */
    .stripes {
      position: absolute;
      top: -10px;
      left: 0px;
      visibility: visible;
      opacity: 1;
    }
    .avatar-token-name-wrapper {
      column-gap: 8px;
      align-items: center;
      .avatar {
        img {
          width: 80px;
          height: 80px;
          background: #e5e5e5;
          border-radius: 50%;
        }
      }
      .t-token-name {
        font-weight: bold;
        font-size: 18px;
        line-height: 28px;
        letter-spacing: -0.26px;
        color: #fafafa;
      }
    }
    .t-price {
      font-weight: bold;
      font-size: 24px;
      line-height: 36px;
      letter-spacing: -0.47px;
      color: #fafafa;
    }
    .t-daily-change {
      font-size: 14px;
      line-height: 20px;
      letter-spacing: -0.09px;
      color: #a3a3a3;
    }
    .change-wrapper {
      background: #171717;
      border-radius: 4px;
      padding: 8px;
      margin: 8px 0 12px;
      .change-duration-wrapper {
        align-items: center;
      }
      .vertical-divider {
        background: #404040;
        width: 2px;
        height: 100%;
      }
      .change {
        font-weight: bold;
        font-size: 18px;
        line-height: 28px;
        display: flex;
        align-items: center;
        letter-spacing: -0.26px;
        &.percentage {
          color: #10b981;
        }
        &.fiat {
          color: #fafafa;
        }
      }
      .t-duration {
        font-size: 16px;
        font-weight: 600;
        line-height: 24px;
        letter-spacing: -0.18px;
        color: #a3a3a3;
      }
    }
    .view-details {
      background: #fb923c;
      border-radius: 2px;
      font-weight: bold;
      font-size: 16px;
      line-height: 16px;
      border: none;
      padding: 10px;
      text-align: center;
      letter-spacing: -0.18px;
      color: #171717;
    }
  }
  .banner-image {
    &.banner-urls {
      display: flex;
      column-gap: 20px;
      width: 70vw;
      overflow-x: scroll;
      img {
        height: 500px;
        width: 800px;
      }
    }
    img.logo {
      max-width: 300px;
      border-radius: 4px;
      max-height: 300px;
    }
  }
  .card-column-design {
    position: absolute;
    background: #fff;
    width: 100px;
    height: 100px;
    transform: scale(0.1) rotate(136deg) translate(-50px, -50px);
  }
`;

const ResolutionHint = styled.p`
    color: #bfbfbf;
    /* font-weight: 600; */
    margin-top: 0.5rem;
    margin-bottom: 0;
`;


interface ProfileProps {
  id: string;
}
interface BannerProps {
  uid?: string;
  url?: string;
}

const Profile = ({ id }: ProfileProps): JSX.Element => {
  const [updateTeamDetails] = useUpdateTeamDisplayInfoMutation();

  const {
    data: teamDisplay,
    refetch: refetchTeamDisplay,
    loading: loadingTeamDisplay,
  } = useGetTeamDisplayInfoQuery({
    variables: { teamId: +id },
  });

  const [socialMediaData, setSocialMediaData] = React.useState<any>({});
  const [bannerData, setBannerData] = React.useState<any>({});
  const [logoBannerData, setLogoBannerData] = React.useState<any>({});

  const [description, setDescription] = React.useState('');

  // modals visibility toggle
  const [isSocialMediaModalVisible, setIsSocialMediaModalVisible] =
    React.useState(false);

  const [isLogoBannerModalVisible, setIsLogoBannerModalVisible] =
    React.useState(false);
  const [isVideoModalVisible, setIsVideoModalVisible] = React.useState(false);

  const [isBannerModalVisible, setIsBannerModalVisible] = React.useState(false);
  const [videoUrl, setVideoUrl] = React.useState('');
  const existingTeamData = teamDisplay?.team_by_pk;
  const [colors, setColors] = React.useState({
    primaryColor: '',
    secondaryColor: '',
  });
  const [banner, setBanner] = React.useState<BannerProps[]>(
    teamDisplay?.team_by_pk?.bannerURLS?.map(
      (bannerUrl: string, i: number) => ({
        uid: i + '',
        url: bannerUrl,
      })
    )
  );

  const [loading, setLoading] = React.useState<any>(null);

  React.useEffect(() => {
    setBanner(
      teamDisplay?.team_by_pk?.bannerURLS?.map((bannerUrl: any, i: number) => ({
        uid: i + '',
        url: bannerUrl,
      }))
    );
    setDescription(teamDisplay?.team_by_pk?.description ?? '');
  }, [loadingTeamDisplay]);

  React.useEffect(() => {
    if (teamDisplay?.team_by_pk?.videoURL) {
      let video: HTMLVideoElement | any = document.querySelector('#team-video');
      video?.load();
    }
  }, [teamDisplay?.team_by_pk?.videoURL]);

  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

  const onUpdateDescription = async () => {
    const { facebookUrl, instagramUrl, twitterUrl, telegramUrl, websiteUrl } =
      existingTeamData?.socialMedia[0] ?? {};

    const { profileImageURL, bannerURLS, videoURL } = existingTeamData ?? {};
    await updateTeamDetails({
      variables: {
        teamId: +id,
        description,
        facebookUrl,
        instagramUrl,
        telegramUrl,
        websiteUrl,
        twitterUrl,
        profileImageURL,
        bannerURLS,
        videoURL,
      },
    });
    refetchTeamDisplay();
    toast.success('Updated Description');
  };
  console.log(description, 'descriotion');
  const onUpdateColors = async () => {
    const { facebookUrl, instagramUrl, twitterUrl, telegramUrl, websiteUrl } =
      existingTeamData?.socialMedia[0] ?? {};

    const { profileImageURL, bannerURLS, videoURL, description } =
      existingTeamData ?? {};
    const { primaryColor, secondaryColor } = colors;

    await updateTeamDetails({
      variables: {
        teamId: +id,
        description,
        facebookUrl,
        instagramUrl,
        telegramUrl,
        websiteUrl,
        twitterUrl,
        profileImageURL,
        bannerURLS,
        videoURL,
        primaryColor,
        secondaryColor,
      },
    });
    refetchTeamDisplay();
    toast.success('Updated Colours');
  };

  const handleBannerUpload = async () => {
    setLoading('uploading');
    const { facebookUrl, twitterUrl, instagramUrl, telegramUrl, websiteUrl } =
      existingTeamData?.socialMedia[0] ?? {};
    const { profileImageURL, description, videoURL } = existingTeamData ?? {};
    await updateTeamDetails({
      variables: {
        teamId: +id,
        bannerURLS: banner?.map((bannerUrls: BannerProps) => bannerUrls?.url),
        facebookUrl,
        twitterUrl,
        instagramUrl,
        telegramUrl,
        videoURL,
        websiteUrl,
        profileImageURL,
        description,
      },
    });
    refetchTeamDisplay();
    setLoading(null);
    setBannerData({});
    toast.success('Updated banners!');
  };

  const beforeUpload = (file: any) => {
    const allowedTypes =
      file.type === 'image/png' || file.type === 'image/jpeg';
    if (!allowedTypes) {
      toast.error(`${file.name} is not a png or jpeg file`);
    }
    return allowedTypes || Upload.LIST_IGNORE;
  };

  const returnBanner = React.useMemo((): any => {
    return banner?.map((_: BannerProps) => ({
      ..._,
      url: `${config.apiBaseUrl}files/${_?.url}`,
    }));
  }, [banner]);

  return (
    <ProfileWrapper>
      <Card>
        <TitleCta
          title="Team Banner"
          cta={
            <Col
              className="cta"
              onClick={() => {
                handleBannerUpload();
              }}
            >
              Update Banners
            </Col>
          }
        />

        {!loadingTeamDisplay && (
          <Upload
            iconRender={() => <></>}
            listType="picture-card"
            accept="image/png, image/jpeg"
            fileList={returnBanner}
            defaultFileList={returnBanner}
            onRemove={(e: any) => {
              setBanner(banner.filter(({ url }: any) => url !== e.url));
            }}
            customRequest={async ({ file, fileList, onProgress }: any) => {
              setLoading('teamBanner');
              let fileupload = await s3FileUpload(file);
              if (fileupload.status === 200) {
                setBanner([...banner, { url: fileupload.data.fileId }]);
                toast.success('File uploaded successfully');
              } else {
                toast.error('File uploaded failed');
              }
              setLoading(null);
            }}
            beforeUpload={(file: any) => beforeUpload(file)}
          >
            {loading === 'teamBanner' ? <Spin indicator={antIcon} /> : '+'}
          </Upload>
        )}

        <BannerModal
          bannerData={bannerData}
          setBannerData={setLogoBannerData}
          teamId={+id}
          existingTeamData={existingTeamData}
          refetchBannerData={refetchTeamDisplay}
          isModalVisible={isBannerModalVisible}
          setIsModalVisible={setIsBannerModalVisible}
        />

        <Row className="banner-specs-wrapper" justify="space-between">
          <ResolutionHint>Recommended resolution :- 1136 x 200 px, Supported formats :- PNG, JPEG</ResolutionHint>
        </Row>
      </Card>
      <Row gutter={[4, 2]} className="logo-media-wrapper">
        <Col>
          <Card>
            <TitleCta
              title="Team Logo"
              cta={
                <Col
                  className="cta"
                  onClick={() => {
                    setLogoBannerData({
                      url: teamDisplay?.team_by_pk?.profileImageURL,
                    });
                    setIsLogoBannerModalVisible(true);
                  }}
                >
                  + Update Logo
                </Col>
              }
            />

            {teamDisplay?.team_by_pk?.profileImageURL && (
              <>
                <div className="banner-image">
                  <img
                    className="banner-image-element logo"
                    src={
                      `${config.apiBaseUrl}files/${teamDisplay?.team_by_pk?.profileImageURL}` ??
                      ''
                    }
                    alt="banner"
                  />
                </div>
                {/* <Col className="t-banner-info">
                  Last updated: {'20 Feb 2022'}
                </Col> */}
              </>
            )}
          </Card>
        </Col>

        <LogoBannerModal
          logoBannerData={logoBannerData}
          setLogoBannerData={setLogoBannerData}
          teamId={+id}
          existingTeamData={existingTeamData}
          refetchTeamLogoBanner={refetchTeamDisplay}
          isModalVisible={isLogoBannerModalVisible}
          setIsModalVisible={setIsLogoBannerModalVisible}
        />

        <Col span={16}>
          <Card>
            <TitleCta
              title="Social Media Links"
              cta={
                <Col
                  className="cta"
                  onClick={() => {
                    setSocialMediaData(teamDisplay?.team_by_pk?.socialMedia[0]);
                    setIsSocialMediaModalVisible(true);
                  }}
                >
                  Edit
                </Col>
              }
            />
            <SocialMediaModal
              socialMediaData={socialMediaData}
              setSocialMediaData={setSocialMediaData}
              teamId={+id}
              existingTeamData={existingTeamData}
              refetchTeamSocialMedia={refetchTeamDisplay}
              isModalVisible={isSocialMediaModalVisible}
              setIsModalVisible={setIsSocialMediaModalVisible}
            />
            <Col>
              <FlexRowWrapper>
                <LabelValue
                  field="Website"
                  value={
                    teamDisplay?.team_by_pk?.socialMedia[0]?.websiteUrl ? (
                      <Tooltip
                        title={
                          teamDisplay?.team_by_pk?.socialMedia[0]?.websiteUrl
                        }
                      >
                        <div
                          style={{
                            width: 220,
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                          }}
                        >
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.websiteUrl || '/'
                            }
                          >
                            {
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.websiteUrl
                            }
                          </a>
                        </div>
                      </Tooltip>
                    ) : (
                      '-'
                    )
                  }
                />
                <LabelValue
                  field="Facebook"
                  value={
                    teamDisplay?.team_by_pk?.socialMedia[0]?.facebookUrl ? (
                      <Tooltip
                        title={
                          teamDisplay?.team_by_pk?.socialMedia[0]?.facebookUrl
                        }
                      >
                        <div
                          style={{
                            width: 220,
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                          }}
                        >
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.facebookUrl || '/'
                            }
                          >
                            {
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.facebookUrl
                            }
                          </a>
                        </div>
                      </Tooltip>
                    ) : (
                      '-'
                    )
                  }
                />
              </FlexRowWrapper>
              <Divider />
              <FlexRowWrapper>
                <LabelValue
                  field="Twitter"
                  value={
                    teamDisplay?.team_by_pk?.socialMedia[0]?.twitterUrl ? (
                      <Tooltip
                        title={
                          teamDisplay?.team_by_pk?.socialMedia[0]?.twitterUrl
                        }
                      >
                        <div
                          style={{
                            width: 220,
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                          }}
                        >
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.twitterUrl || '/'
                            }
                          >
                            {
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.twitterUrl
                            }
                          </a>
                        </div>
                      </Tooltip>
                    ) : (
                      '-'
                    )
                  }
                />
                <LabelValue
                  field="Instagram"
                  value={
                    teamDisplay?.team_by_pk?.socialMedia[0]?.instagramUrl ? (
                      <Tooltip
                        title={
                          teamDisplay?.team_by_pk?.socialMedia[0]?.instagramUrl
                        }
                      >
                        <div
                          style={{
                            width: 220,
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                          }}
                        >
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.instagramUrl || '/'
                            }
                          >
                            {
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.instagramUrl
                            }
                          </a>
                        </div>
                      </Tooltip>
                    ) : (
                      '-'
                    )
                  }
                />
              </FlexRowWrapper>
              <Divider />

              <FlexRowWrapper>
                <LabelValue
                  field="Telegram"
                  value={
                    teamDisplay?.team_by_pk?.socialMedia[0]?.telegramUrl ? (
                      <Tooltip
                        title={
                          teamDisplay?.team_by_pk?.socialMedia[0]?.telegramUrl
                        }
                      >
                        <div
                          style={{
                            width: 400,
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                          }}
                        >
                          <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.telegramUrl || ''
                            }
                          >
                            {
                              teamDisplay?.team_by_pk?.socialMedia[0]
                                ?.telegramUrl
                            }
                          </a>
                        </div>
                      </Tooltip>
                    ) : (
                      '-'
                    )
                  }
                />
              </FlexRowWrapper>
            </Col>
          </Card>
        </Col>
      </Row>

      <Card>
        <TitleCta
          title="About"
          cta={
            <Button className="cta" onClick={() => onUpdateDescription()}>
              Update
            </Button>
          }
        />

        <TextEditorInput
          editData={teamDisplay?.team_by_pk?.description}
          value={description}
          setValue={setDescription}
        />
      </Card>
      <Col span={8}>
        <Card>
          <TitleCta
            title="Team Video"
            cta={
              <Col
                className="cta"
                onClick={() => {
                  setVideoUrl(teamDisplay?.team_by_pk?.videoURL ?? '');
                  setIsVideoModalVisible(true);
                }}
              >
                + Update Video
              </Col>
            }
          />

          {teamDisplay?.team_by_pk?.videoURL ? (
            isValidHttpUrl(teamDisplay?.team_by_pk?.videoURL) ? (
              matchYoutubeUrl(teamDisplay?.team_by_pk?.videoURL) ? (
                <iframe
                  width="100%"
                  height="100%"
                  src={`https://www.youtube.com/embed/${matchYoutubeUrl(
                    teamDisplay?.team_by_pk?.videoURL
                  )}`}
                  title="YouTube video player"
                  frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen
                />
              ) : (
                <>
                  <iframe
                    width="100%"
                    height="100%"
                    src={teamDisplay?.team_by_pk?.videoURL}
                    title="Video player"
                    frameBorder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                  />
                </>
              )
            ) : (
              <>
                <video id="team-video" width="320" height="240" controls>
                  <source
                    src={`${config.apiBaseUrl}files/${teamDisplay?.team_by_pk?.videoURL}`}
                    type="video/mp4"
                  />
                  <source
                    src={`${config.apiBaseUrl}files/${teamDisplay?.team_by_pk?.videoURL}`}
                    type="video/ogg"
                  />
                  Your browser does not support the video tag.
                </video>
              </>
            )
          ) : null}
        </Card>
      </Col>
      <VideoModal
        videoUrlData={videoUrl}
        setVideoUrlData={setVideoUrl}
        teamId={+id}
        existingTeamData={existingTeamData}
        refetchTeamVideoUrl={refetchTeamDisplay}
        isModalVisible={isVideoModalVisible}
        setIsModalVisible={setIsVideoModalVisible}
      />

      <Card className="colours-card">
        <Row gutter={[24,24]} style={{width:"100%"}}>
          <Col xs={24} sm={6}>
            <TitleCta title="Colours" />
            <Col className="t-colors title">Colours</Col>
            <LabelInput
              label="Primary colour"
              name="primaryColor"
              initialValue={teamDisplay?.team_by_pk?.primaryColor ?? ''}
              inputElement={
                <Input
                  defaultValue={teamDisplay?.team_by_pk?.primaryColor ?? ''}
                  onChange={(e) =>
                    setColors({ ...colors, primaryColor: e.target.value })
                  }
                  type="color"
                />
              }
            />
            <LabelInput
              label="Secondary colour"
              name="secondaryColor"
              initialValue={teamDisplay?.team_by_pk?.secondaryColor ?? ''}
              inputElement={
                <Input
                  defaultValue={teamDisplay?.team_by_pk?.secondaryColor ?? ''}
                  onChange={(e) =>
                    setColors({ ...colors, secondaryColor: e.target.value })
                  }
                  type="color"
                />
              }
            />

            <Button onClick={onUpdateColors}>Update Colors</Button>
          </Col>
          <Col xs={24} sm={8}>
            <LiveTeamCard
              profileImgUrl={`${config.apiBaseUrl}files/${teamDisplay?.team_by_pk?.profileImageURL}`}
              teamName={teamDisplay?.team_by_pk?.name}
              description={description? description : teamDisplay?.team_by_pk?.description}
              catogory={teamDisplay?.team_by_pk?.categoryId}
              countryName={teamDisplay?.team_by_pk?.countryTitle ?? null}
              primaryColor={colors?.primaryColor ? colors?.primaryColor : teamDisplay?.team_by_pk?.primaryColor}
            />
          </Col>
        </Row>
      </Card>
    </ProfileWrapper>
  );
};

export default Profile;
