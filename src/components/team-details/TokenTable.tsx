import { Table } from 'antd';
import { EditOutlined } from '@ant-design/icons';
interface contactInfoProps {
//   JsonData: any;
//   openEdit: any;
}

function onChange(pagination: any, filters: any, sorter: any, extra: any) {
  console.log('params', pagination, filters, sorter, extra);
}
const TokenTable = ({}: contactInfoProps): JSX.Element => {
const Sourcedata = [
    {
        key: '898775',
        date: "Feb 28, 2021",
        type:"Token Exchange",
        from: "2MI",
        to: "100 SPN",
        gas:"2 USD",
        rate: "10MI/SPN",
        status:"Processing",
        address: 'New York No. 1 Lake Park',
    },
    {
        key: '744748',
        date: "May 22, 2021",
        type:"Token Exchange",
        from: "100 SPN",
        to: "100 SPN",
        gas:"2 USD",
        rate: "10MI/SPN",
        status:"Processing",
        address: 'London No. 1 Lake Park',
    },
    {
        key: '574474',
        date: "Dec 2, 2021",
        type:"Token Exchange",
        from: "Rs.10000",
        to: "100 SPN",
        gas:"2 USD",
        rate: "10MI/SPN",
        status:"Processing",
        address: 'Sidney No. 1 Lake Park',
    },
    {
        key: '848748',
        date: 32,
        type:"Token Exchange",
        from: "200 RWRD",
        to: "2SPN",
        gas:"2 USD",
        rate: "10MI/SPN",
        status:"Processing",
        address: 'London No. 2 Lake Park',
    },
    ];
  const columns = [
    {
      title: 'Pay ID',
      dataIndex: 'key',
    },
    {
      title: 'DATE',
      dataIndex: 'date',
    },
    {
      title: 'TRASACTION TYPE',
      dataIndex: 'type',
    },
    {
      title: 'FROM',
      dataIndex: 'from',
    },
    {
      title: 'TO',
      dataIndex: 'to',
    },
    {
      title: 'GAS FEES',
      dataIndex: 'gas',
    },
    {
      title: 'EXCHANGE RATE',
      dataIndex: 'rate',
    },
    {
      title: 'STATUS',
      dataIndex: 'status',
    },
  ];
  

  const data = Sourcedata?.map((val: any,i: number)=>{
    return({
        key: val.key,
        date: val?.date,
        type: val?.type,
        from: val?.from,
        to: val?.to,
        gas: val?.gas,
        rate: val?.rate,
        status: val?.status
      })
  });
  return(
    <Table columns={columns} dataSource={data} onChange={onChange} size="middle" scroll={{ x: 500 }}/>
  )
}
export default TokenTable;