import styled from 'styled-components/macro';
import { Button, Card, TablePaginationConfig, Tabs } from 'antd';
import React, { useMemo, useState } from 'react';
import { useHistory } from 'react-router-dom';
import CustomTable from 'components/customTable';
import { getPollsApi } from 'apis/polls/getpolls';
import { getAllContests } from 'apis/contest';
import { getNewsApi } from 'apis/news/getnews';
import { toast } from 'react-toastify';
import { pollsColumns } from 'data/polls';
import { contestColumns } from 'data/contest';
import { newsColumns } from 'data/news';

const EngagementWrapper = styled.div`
  .t-contest-active {
    color: #07697d;
  }
  .contest-cta-wrapper {
    align-items: center;
    column-gap: 8px;
  }
  .t-contest-participation {
    font-size: 16px;
    line-height: 19px;
    color: #07697d;
    span {
      font-size: 11px;
    }
  }
`;

interface EngagementProps {
  id : string;
}

type TabKey = '1' | '2' | '3';

let initialPagination: TablePaginationConfig = {
  current: 1,
  pageSize: 5,
  total: 0
}

const Engagement = ({ id }: EngagementProps): JSX.Element => {

  const { TabPane } = Tabs;
  const history = useHistory();

  const [activeKey, setActivekey] = useState<'1' | '2' | '3'>('1');
  const [loading, setLoading] = useState<boolean>(false);
  const [dataSource, setDataSource] = useState<any[]>([]);
  const [pagination, setPagination] = React.useState<TablePaginationConfig>(initialPagination);

  const getPropsByActiveKey = (data: any = {}, recordId: string | null = null) => {
    return {
      "1": {
        columns: pollsColumns,
        api: getPollsApi,
        dataSourceKey: data?.allTeamPolls?.nodes,
        totalCount: data?.allTeamPolls?.totalCount,
        buttonName: "Poll",
        addRoute: `/poll/add`,
        editRoute: `/polls`,
        teamIdKey:"team"
      },
      "2": {
        columns: contestColumns,
        api: getAllContests,
        dataSourceKey: data?.list?.allContests?.nodes,
        totalCount: data?.list?.allContests?.totalCount,
        buttonName: "Contest",
        addRoute: `/contests/add`,
        editRoute: `/contests`,
        teamIdKey:"teamId"
      },
      "3": {
        columns: newsColumns,
        api: getNewsApi,
        dataSourceKey: data?.list?.allPosts?.nodes,
        totalCount: data?.list?.allPosts?.totalCount,
        buttonName: "Post",
        addRoute: `/news/add`,
        editRoute: `/news`,
        teamIdKey:"teamId"
      },
    }
  }

  React.useEffect(() => {
    getDataSource()
  }, [activeKey])

  const onChangeTable = (newPagination: TablePaginationConfig) => {
    const { current = 1, pageSize } = newPagination;
    let offset = current;
    let from = "pagination"
    if (pagination?.pageSize !== newPagination?.pageSize) {
      offset = 1;
      from = ""
    }
    getDataSource(offset, pageSize, from)
  }

  const getDataSource = (offset: number = 1, limit: number = 5, from: string | null = null) => {
    setLoading(true);
    let payload = {
      "start": ((offset * limit) - limit),
      "length": limit,
      [getPropsByActiveKey()?.[activeKey].teamIdKey]: Number(id)
    }

    getPropsByActiveKey()?.[activeKey]?.api(payload)
      .then((res: any) => {
        if (res?.data?.statusCode === "500") {
          catchError(res?.data?.message)
        }

        let data = res?.data?.data;

        setDataSource(getPropsByActiveKey(data)?.[activeKey]?.dataSourceKey ?? [])

        setPagination({
          ...pagination,
          current: from === "initial" ? 1 : offset,
          pageSize: limit,
          total: getPropsByActiveKey(data)?.[activeKey]?.totalCount ?? 0
        })
        setLoading(false)

      })
      .catch(e => catchError(e?.message))
  }

  const catchError = (msg: string) => {
    console.log(msg);
    setLoading(false)
    toast.error(msg ? msg : 'Something went wrong!');
    return false
  }

  const onRowClick = (rowRecord: any) => {
    let pathName = getPropsByActiveKey()?.[activeKey]?.editRoute;
    history.push(`${pathName}/${id}/${rowRecord.id}`)
  }

  const returnTable = () => {
    return (
      <CustomTable
        columns={getPropsByActiveKey()?.[activeKey]?.columns}
        dataSource={dataSource ?? []}
        loading={loading}
        pagination={pagination}
        onChangeTable={onChangeTable}
        onRowClick={onRowClick}
      />
    )
  }

  const returnButton = () => {
    return (
      <Button onClick={() => onClickButton(activeKey)}>Create New {getPropsByActiveKey()?.[activeKey]?.buttonName}</Button>
    )
  }

  const onClickButton = (key: TabKey) => {
    let pathname = getPropsByActiveKey()?.[key]?.addRoute
    history.push({
      pathname,
      state:{
        teamId:Number(id)
      }
    })
  }

  const getTotalCount = (key: TabKey) => {
    return activeKey === key ? `(${loading ? 0 : pagination.total})` : ""
  }

  return (
    <EngagementWrapper>
      <Card>
        <Tabs
          tabBarExtraContent={returnButton()}
          activeKey={activeKey}
          onChange={(val: string) => setActivekey(val as TabKey)}
        >
          <TabPane tab={`Polls ${getTotalCount("1")}`} key="1">
            {returnTable()}
          </TabPane>
          <TabPane tab={`Contests ${getTotalCount("2")}`} key="2">
            {returnTable()}
          </TabPane>
          <TabPane tab={`News ${getTotalCount("3")}`} key="3">
            {returnTable()}
          </TabPane>
        </Tabs>

      </Card>
    </EngagementWrapper>
  );
};

export default Engagement;
