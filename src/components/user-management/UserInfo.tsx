import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import debounce from 'lodash/debounce';
import * as React from 'react';
import {
  Menu,
  Dropdown,
  Button,
  message,
  Space,
  Table,
  Tooltip,
  Radio,
  Divider,
} from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import { usersColumns } from 'data/usersData';
import { useHistory } from 'react-router-dom';
import DropdownElement from 'components/DropdownElement';
import SelectElement from 'components/common/SelectElement';
import { GetAllUsersQuery, useGetAllCountriesQuery } from 'generated/graphql';

const UserInfoWrapper = styled.div`
  .filters-wrapper {
    padding: 40px 0;
    column-gap: 25px;
    align-items: center;
    input {
      padding: 9px;
      background: rgba(5, 31, 36, 0.05);
      border: 1px solid rgba(5, 31, 36, 0.05);
      box-sizing: border-box;
      border-radius: 5px;
    }
  }
`;

interface UserInfoProps {
  usersData: GetAllUsersQuery['user'];
  filters: {
    team: string;
    status: string;
    leaderboard: string;
    age: string;
    location: string;
  };
  refetchUsersData: () => void;
  setFilters: React.Dispatch<
    React.SetStateAction<{
      team: string;
      status: string;
      leaderboard: string;
      age: string;
      location: string;
    }>
  >;
}

const menu = (
  <Menu selectable onClick={(e) => console.log(e.key)}>
    <Menu.Item key="value" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="2" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="3" icon={<UserOutlined />}>
      Option
    </Menu.Item>
  </Menu>
);

const UserInfo = ({
  usersData,
  refetchUsersData,
  filters,
  setFilters,
}: UserInfoProps): JSX.Element => {
  const history = useHistory();
  const [searchVal, setSearchVal] = React.useState('');
  const { data: countries } = useGetAllCountriesQuery();
  // const [searchQuery, { data: dataToQuery }] = useSearchQueryLazyQuery();

  // React.useEffect(() => {
  //   const searchDebounce = debounce(
  //     () =>
  //       searchQuery({
  //         variables: { val: searchVal },
  //       }),
  //     200
  //   );
  //   if (searchVal) {
  //     searchDebounce();
  //   }
  //   return () => searchDebounce.cancel();
  // }, [searchVal]);

  const filteredUsers = usersData.filter(({ name, bio }: any) =>
    name?.toLowerCase().includes(searchVal.toLowerCase())
  );

  React.useEffect(() => {
    refetchUsersData();
  }, [filters]);

  console.log(filters);

  return (
    <UserInfoWrapper>
      <FlexRowWrapper className="filters-wrapper">
        <input
          type="text"
          onChange={(e) => setSearchVal(e.target.value)}
          placeholder="Search and filter..."
        />
        <SelectElement
          options={[{ team: 'india' }, { team: 'eur' }]}
          toFilter="team"
          onChange={(e: any) => setFilters({ ...filters, team: e })}
          placeholder="Team"
        />{' '}
        <SelectElement
          options={[{ status: 'verified' }, { status: 'eur' }]}
          toFilter="status"
          onChange={(e: any) => setFilters({ ...filters, status: e })}
          placeholder="Status"
        />
        <SelectElement
          options={[{ team: 'india' }, { team: 'eur' }]}
          toFilter="team"
          onChange={(e: any) => setFilters({ ...filters, team: e })}
          placeholder="Team"
        />{' '}
        <SelectElement
          options={[{ team: 'india' }, { team: 'eur' }]}
          toFilter="team"
          onChange={(e: any) => setFilters({ ...filters, team: e })}
          placeholder="Team"
        />{' '}
        <SelectElement
          options={countries?.country}
          toFilter="title"
          onChange={(e: any) => setFilters({ ...filters, location: e })}
          placeholder="Location"
        />
      </FlexRowWrapper>

      <div>
        <Divider />
        <Table
          rowSelection={{
            type: 'checkbox',
          }}
          onRow={(record) => {
            return {
              onClick: () => {
                console.log(history.push(`/user/${record.key}`));
              },
            };
          }}
          columns={usersColumns}
          dataSource={filteredUsers}
        />
      </div>
    </UserInfoWrapper>
  );
};

export default UserInfo;
