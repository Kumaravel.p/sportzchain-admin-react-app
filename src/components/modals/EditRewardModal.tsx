import styled from 'styled-components/macro';
import * as React from 'react';
import { Modal, Form, Input, Upload } from 'antd';
// import LabelInput from '../../common/LabelInput';
import TextArea from 'antd/lib/input/TextArea';
import {
  useCreateTeamNoteMutation,
  useUpdateRewardByIdMutation,
  useUpdateTeamNoteByIdMutation,
} from 'generated/graphql';
import { toast } from 'react-toastify';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import { Stats } from 'fs';

const EditRewardModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
      height: fit-content;
      margin: 0;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface EditRewardModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  refetchRewardsData: () => void;
  setRewardData: React.Dispatch<React.SetStateAction<any>>;
  rewardData: any;
}

enum status {
  ACTIVE = 'Active',
  INACTIVE = 'Inactive',
}

const EditRewardModal = ({
  setIsModalVisible,
  isModalVisible,
  rewardData,
  setRewardData,
  refetchRewardsData,
}: EditRewardModalProps): JSX.Element => {
  const [form] = Form.useForm();
  console.log(rewardData, 'rewarddataa');
  const [updateReward] = useUpdateRewardByIdMutation();

  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const handleOk = async (formValues: any) => {
    console.log(formValues, 'formvalues');
    setConfirmLoading(true);
    await updateReward({
      variables: {
        rewardId: +rewardData?.id,
        description: formValues.description,
        enabled: formValues.status === status.ACTIVE ? true : false,
      },
    });

    setConfirmLoading(false);
    setIsModalVisible(false);
    setRewardData({});
    refetchRewardsData();
    toast.success(`Updated Reward`);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setRewardData({});
  };

  return (
    <EditRewardModalWrapper>
      <Modal
        title={`Update Reward`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Status"
            name="status"
            initialValue={rewardData?.enabled ? status.ACTIVE : status.INACTIVE}
            inputElement={
              <SelectElement
                searchable
                defaultValue={
                  rewardData?.enabled ? status.ACTIVE : status.INACTIVE
                }
                onChange={(e) => form.setFieldsValue({ status: e })}
                placeholder="Status"
                toFilter="value"
                options={[{ value: status.ACTIVE }, { value: status.INACTIVE }]}
              />
            }
          />

          <LabelInput
            label="Description"
            initialValue={rewardData?.loyaltyActivity?.description}
            name="description"
            inputElement={
              <Input
                onChange={(e) =>
                  form.setFieldsValue({ description: e.target.value })
                }
                defaultValue={rewardData?.loyaltyActivity?.description}
                type="text"
                placeholder="Description"
              />
            }
          />
        </Form>
      </Modal>
    </EditRewardModalWrapper>
  );
};

export default EditRewardModal;
