import {
  GetRewardByIdQuery,
  GetRewardByIdQueryResult,
  useAddNewRewardRuleMutation,
  useEditRewardRuleByIdMutation,
  useUpdateRwrdPriceMutation,
} from 'generated/graphql';

import styled from 'styled-components/macro';
import * as React from 'react';
import {
  AutoComplete,
  Breadcrumb,
  Button,
  Card,
  Col,
  DatePicker,
  Divider,
  Dropdown,
  Input,
  Form,
  Row,
} from 'antd';

import Modal from 'antd/lib/modal/Modal';
import { toast } from 'react-toastify';
import Text from 'antd/lib/typography/Text';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import { useHistory } from 'react-router-dom';
import moment, { Moment } from 'moment';

const AddRewardRuleModalWrapper = styled.div``;

enum duration {
  DAY = 'Day',
  WEEK = 'Week',
  MONTH = 'Month',
}

interface AddRewardRuleModalProps {
  isModalVisible: boolean;
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  rewardRulesData: any;
  rewardData: any;
  refetchRewardData: () => void;
  setRewardRulesData: React.Dispatch<React.SetStateAction<any>>;
}

const AddRewardRuleModal = ({
  isModalVisible,
  setIsModalVisible,
  rewardRulesData,
  rewardData,
  setRewardRulesData,
  refetchRewardData,
}: AddRewardRuleModalProps): JSX.Element => {
  const [form] = Form.useForm();

  const [addNewReward] = useAddNewRewardRuleMutation();
  const [editReward] = useEditRewardRuleByIdMutation();

  const edit = !!rewardRulesData?.id;

  const [rewardsData, setRewardsData] = React.useState<any>(rewardRulesData);
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  const handleOk = async () => {
    setConfirmLoading(true);
    {
      edit
        ? await editReward({
            variables: {
              rewardId: rewardRulesData?.id,
              amount: +rewardsData?.amount,
              activeFrom: rewardsData?.activeFrom,
              activeTill: rewardsData?.activeTill,
              maxApplyCount: +rewardsData?.maxApplyCount,
              duration: rewardsData?.duration,
              ruleName: rewardsData?.ruleName,
            },
          })
        : await addNewReward({
            variables: {
              amount: +rewardsData?.amount,
              activeFrom: rewardsData?.activeFrom,
              activityId: rewardData?.rewardRule[0]?.activityId,
              activeFromTimestamp:
                rewardsData?.activeFrom &&
                Math.floor(new Date(rewardsData?.activeFrom).getTime() / 1000),
              activeTillTimestamp:
                rewardsData?.activeTill &&
                Math.floor(new Date(rewardsData?.activeTill).getTime() / 1000),
              activeTill: rewardsData?.activeTill,
              maxApplyCount: +rewardsData?.maxApplyCount,
              duration: rewardsData?.duration,
              ruleName: rewardsData?.ruleName,
              period: 10,
              category: rewardData?.rewardRule[0]?.category,
            },
          });
    }

    setConfirmLoading(false);
    setIsModalVisible(false);
    refetchRewardData();
    setRewardRulesData({});
    toast.success(
      `${edit ? 'Updated' : 'Added'} Reward Rule: ${rewardsData?.ruleName}`
    );
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setRewardRulesData({});
  };

  console.log(rewardRulesData, 'reward d b ata');
  return (
    <AddRewardRuleModalWrapper>
      <Modal
        title={`${edit ? 'Edit' : 'Add New'} Reward Rule`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <div className="modal-container-reward">
          <Text className="t-reward-activity">Reward activity</Text>
          <div style={{ margin: '4px 0' }} className="t-reward-category">
            {rewardData?.rewardRule?.[0]?.category}
          </div>

          <Form onFinish={handleOk} form={form} preserve={false}>
            <LabelInput
              label="Reward Amount"
              name="amount"
              inputElement={
                <Input
                  style={{ width: 'calc(100% - 200px)' }}
                  placeholder="Reward Amount"
                  defaultValue={rewardRulesData?.amount}
                  type="number"
                  onChange={(e) =>
                    setRewardsData({ ...rewardsData, amount: e.target.value })
                  }
                />
              }
            />

            <LabelInput
              label="Start and End Date"
              inputElement={
                <DatePicker.RangePicker
                  defaultValue={[
                    moment(rewardRulesData?.activeFrom),
                    moment(rewardRulesData?.activeTill),
                  ]}
                  showSecond={false}
                  format="YYYY-MM-DD HH:mm"
                  style={{ margin: '4px 0' }}
                  onChange={(e: any) =>
                    setRewardsData({
                      ...rewardsData,
                      activeFrom: e[0]?._d,
                      activeTill: e[1]?._d,
                    })
                  }
                  dateRender={(current) => {
                    const style = {};
                    if (current.date() === 1) {
                      // style.border = '1px solid #1890ff';
                      // style.borderRadius = '50%';
                    }
                    return (
                      <div className="ant-picker-cell-inner" style={style}>
                        {current.date()}
                      </div>
                    );
                  }}
                />
              }
            />

            <LabelInput
              label="Rule Name"
              name="ruleName"
              rules={[{ required: true, message: 'Rule name is required!' }]}
              initialValue={rewardRulesData?.ruleName}
              inputElement={
                <Input
                  style={{ margin: '4px 0' }}
                  defaultValue={rewardRulesData?.ruleName}
                  onChange={(e) =>
                    setRewardsData({ ...rewardsData, ruleName: e.target.value })
                  }
                  placeholder="Rule name"
                />
              }
            />
            <LabelInput
              label="Max. Apply count"
              name="maxApplyCount"
              rules={[
                { required: true, message: 'Max. Apply count is required!' },
              ]}
              initialValue={rewardRulesData?.maxApplyCount}
              inputElement={
                <Input
                  style={{ margin: '4px 0' }}
                  type="number"
                  defaultValue={rewardRulesData?.maxApplyCount}
                  onChange={(e) =>
                    setRewardsData({
                      ...rewardsData,
                      maxApplyCount: e.target.value,
                    })
                  }
                  placeholder="Number of times"
                  min="0"
                />
              }
            />

            <LabelInput
              label="Period (Range)"
              // rules={[{ required: true, message: 'Duration is required!' }]}
              name="duration"
              initialValue={rewardRulesData?.duration}
              inputElement={
                <SelectElement
                  onChange={(e) =>
                    setRewardsData({ ...rewardsData, duration: e })
                  }
                  placeholder="Duration"
                  defaultValue={rewardData?.duration}
                  toFilter="value"
                  options={[
                    { value: duration.DAY },
                    { value: duration.WEEK },
                    { value: duration.MONTH },
                  ]}
                />
              }
            />
          </Form>
        </div>
      </Modal>
    </AddRewardRuleModalWrapper>
  );
};

export default AddRewardRuleModal;
