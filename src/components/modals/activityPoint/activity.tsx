import React, { useEffect } from 'react';
import { Row, Col, Typography, DatePicker, Input, Button, Form, Divider, FormInstance } from 'antd';
import SelectElement from 'components/common/SelectElement';
import styled from 'styled-components/macro';
import LabelInput from 'components/common/LabelInput';
import moment from 'moment';
import { range, ternaryOperator } from 'utils';

const Wrapper = styled.div`
    & .label{
    font-weight: 400;
    font-size: 14px;
    color:#051F2499;
    margin-bottom: 16px;
    & span{
        color:red;
    }
}
& .title{
    font-weight: 600;
    font-size: 20px;
    color: #051F24;
    margin-bottom: 24px;
}
& .total-quantity-label{
    font-weight: 500;
    font-size: 24px;
    color: #07697D;
}
& .token-style{
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    color: #4DA1FF;
}
& .prefix-style{
    font-weight: 500;
    font-size: 16px;
    color: #151516;
}
& .ant-select-selector{
    border: 1px solid #d9d9d9 !important;
    border-radius: 2px !important;
}
`;

interface ActivityModalInteface {
    form: FormInstance<any>
};

export const ActivityModal = (props: ActivityModalInteface): JSX.Element => {

    const {
        form
    } = props;

    const onSave = () => { }  

    return (
        <Wrapper>
            <Form
                onFinish={onSave}
                form={form}
                preserve={false}
                // onValuesChange={onValuesChange}
                initialValues={{
                    status: { label: 'Active', value: 6 }
                }}
            >
                <Row gutter={[24, 24]}>
                    <Col xs={24} sm={18}>
                        <LabelInput
                            label="Rule Name"
                            name="ruleName"
                            className="label-input"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Rule Name is required!',
                                },
                            ]}
                            inputElement={
                                <Input
                                    placeholder="Rule Name"
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} sm={6}>
                        <LabelInput
                            label="Rule Status"
                            name="status"
                            className="label-input"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Status is required!',
                                },
                            ]}
                            inputElement={
                                <SelectElement
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                    options={[
                                        { label: 'Active', value: 6 }
                                    ]}
                                    placeholder="Select Status"
                                    searchable={true}
                                    toFilter="label"
                                    toStore='value'
                                    labelInValue
                                    // mode="multiple"
                                    loading={false}
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} sm={12} md={8}>
                        <LabelInput
                            label="Activity Points Per User"
                            name="activityPointsPerUser"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Activity Points Per User is required!',
                                },
                            ]}
                            inputElement={
                                <Input
                                    placeholder="Activity Points Per User"
                                    type="number"
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} sm={12} md={8}>
                        <LabelInput
                            label="Start Date & Time"
                            name="startDateTime"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Start Date & Time is required!',
                                },
                            ]}
                            inputElement={
                                <DatePicker
                                    format={"DD-MM-YYYY hh:mm"}
                                    showNow={false}
                                    showSecond={false}
                                    showTime
                                    disabledDate={d => d && d < moment().subtract(1, 'days')}
                                    style={{
                                        width: '100%'
                                    }}
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} sm={12} md={8}>
                        <LabelInput
                            label="End Date & Time"
                            name="endDateTime"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'End Date & Time is required!',
                                },
                            ]}
                            inputElement={
                                <DatePicker
                                    format={"DD-MM-YYYY hh:mm"}
                                    showNow={false}
                                    showSecond={false}
                                    showTime
                                    disabledDate={d => (form.getFieldValue('startDateTime') ? d && d < moment(form.getFieldValue('startDateTime')) : true)}
                                    disabledHours={() => form.getFieldValue('startDateTime') ? [] : range(0, 24)}
                                    disabledMinutes={() => form.getFieldValue('startDateTime') ? [] : range(0, 59)}
                                    disabledSeconds={() => form.getFieldValue('startDateTime') ? [] : range(0, 59)}
                                    style={{
                                        width: '100%'
                                    }}
                                />
                            }
                        />
                    </Col>
                </Row>
                <Divider />
                <Typography className="title">Restriction Limit</Typography>
                <Row gutter={[24, 24]}>
                    <Col xs={24} sm={12} md={8}>
                        <LabelInput
                            label="Max Limit (No. of times)"
                            name="maxLimit"
                            className="label-input"
                            inputElement={
                                <Input
                                    placeholder="Enter Value"
                                    type="number"
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} sm={12} md={8}>
                        <LabelInput
                            label="Duration"
                            name="duration"
                            className="label-input"
                            inputElement={
                                <Input
                                    placeholder="Enter Numbers"
                                    type="number"
                                    suffix={
                                        <Typography className='token-style'>Days</Typography>
                                    }
                                />
                            }
                        />
                    </Col>
                    {/* <Col xs={24} sm={12} md={8}>
                        <LabelInput
                            label="Duration unit"
                            name="durationUnit"
                            className="label-input"
                            inputElement={
                                <SelectElement
                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                    options={[]}
                                    placeholder="Duration unit"
                                    searchable={true}
                                    toFilter="type"
                                    toStore='id'
                                    // mode="multiple"
                                    loading={false}
                                />
                            }
                        />
                    </Col> */}
                </Row>
            </Form>
        </Wrapper>
    )
}