import styled from 'styled-components/macro';
import * as React from 'react';
import { Modal, Form, Input, Upload } from 'antd';

import TextArea from 'antd/lib/input/TextArea';
import {
  useCreateContestNoteMutation,
  useCreateTeamNoteMutation,
  useUpdateContestNoteByIdMutation,
  useUpdateTeamNoteByIdMutation,
} from 'generated/graphql';
import { toast } from 'react-toastify';
import LabelInput from 'components/common/LabelInput';

const ContestNoteModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
      height: fit-content;
      margin: 0;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface ContestNoteModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  creatorId: number;
  contestId: number;
  refetchContestNotes: () => void;
  setNoteData: ({}) => void;
  noteData: any;
}

const ContestNoteModal = ({
  setIsModalVisible,
  isModalVisible,
  creatorId,
  contestId,
  noteData,
  setNoteData,
  refetchContestNotes,
}: ContestNoteModalProps): JSX.Element => {
  const [form] = Form.useForm();
  const [createContestNote] = useCreateContestNoteMutation();
  const [updateContestNote] = useUpdateContestNoteByIdMutation();
  const edit = !!noteData.key;

  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const handleOk = async (formValues: any) => {
    setConfirmLoading(true);
    {
      edit
        ? await updateContestNote({
            variables: {
              id: noteData.key,
              content: formValues.content,
            },
          })
        : await createContestNote({
            variables: {
              content: formValues.content,
              creatorId,
              contestId,
            },
          });
    }
    setConfirmLoading(false);
    setIsModalVisible(false);
    setNoteData({});
    refetchContestNotes();
    toast.success(`${edit ? 'Updated' : 'Added'} Note : ${formValues.content}`);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setNoteData({});
  };

  return (
    <ContestNoteModalWrapper>
      <Modal
        title={`${edit ? 'Edit' : 'Add'} Note`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Note"
            initialValue={noteData?.content}
            rules={[{ required: true, message: 'Note Content is required!' }]}
            name="content"
            inputElement={
              <TextArea defaultValue={noteData?.content} placeholder="Note" />
            }
          />
        </Form>
      </Modal>
    </ContestNoteModalWrapper>
  );
};

export default ContestNoteModal;
