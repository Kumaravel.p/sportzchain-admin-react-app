import {
  GetExchangeRateQuery,
  useUpdateRwrdPriceMutation,
} from 'generated/graphql';
import styled from 'styled-components/macro';
import * as React from 'react';
import Modal from 'antd/lib/modal/Modal';
import { Input } from 'antd';
import { toast } from 'react-toastify';

const UpdateRwrdPriceModalWrapper = styled.div``;

interface UpdateRwrdPriceModalProps {
  isModalVisible: boolean;
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  exchangeRate: GetExchangeRateQuery['exchangeRate'];
  refetchExchangeRate: (pair: { base: string; quote: string }) => void;
}

const UpdateRwrdPriceModal = ({
  isModalVisible,
  setIsModalVisible,
  exchangeRate,
  refetchExchangeRate,
}: UpdateRwrdPriceModalProps): JSX.Element => {
  const [updateRwrdPrice] = useUpdateRwrdPriceMutation();

  const [updatedRwrdPrice, setUpdatedRwrdPrice] = React.useState(
    exchangeRate[0]?.rate
  );
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const handleOk = async () => {
    setConfirmLoading(true);
    await updateRwrdPrice({
      variables: {
        id: exchangeRate[0]?.id ? +exchangeRate[0]?.id : 0,
        rate: updatedRwrdPrice,
      },
    });
    refetchExchangeRate({
      base: 'RWRD',
      quote: 'USD',
    });
    setConfirmLoading(false);
    setIsModalVisible(false);
    toast.success(`Updated RWRD Price to ${updatedRwrdPrice}`);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <UpdateRwrdPriceModalWrapper>
      <Modal
        title="Update RWRD Price"
        visible={isModalVisible}
        onOk={handleOk}
        okText="Update"
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <Input
          placeholder="0"
          type="number"
          min={0}
          onChange={(e) => setUpdatedRwrdPrice(e.target.value)}
        />
      </Modal>
    </UpdateRwrdPriceModalWrapper>
  );
};

export default UpdateRwrdPriceModal;
