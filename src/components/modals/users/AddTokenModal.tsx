import React from 'react';
import styled from 'styled-components/macro';
import { toast } from 'react-toastify';
import { Modal, Input } from 'antd';

import LabelInput from '../../common/LabelInput';

const AddTokenModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface AddTokenModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  userAddress: string;
}

const AddTokenModal = ({
  setIsModalVisible,
  isModalVisible,
  userAddress,
}: AddTokenModalProps): JSX.Element => {
  const [tokensToAdd, setTokensToAdd] = React.useState(0);
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  // const [transferSpn] = useTransferSpnMutation();

  const handleOk = async () => {
    setConfirmLoading(true);
    // await transferSpn({
    //   variables: {
    //     spnAmount: +tokensToAdd,
    //     address: userAddress,
    //   },
    // });
    setConfirmLoading(false);
    setIsModalVisible(false);
    toast.success(`Added ${tokensToAdd} to address ${userAddress}`);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <AddTokenModalWrapper>
      {/* to add ctas texts for edit also */}
      <Modal
        title="Add Tokens"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="Save"
        destroyOnClose={true}
        confirmLoading={confirmLoading}
      >
        <LabelInput
          label="NO. OF TOKENS"
          inputElement={
            <Input
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setTokensToAdd(+e.target.value)
              }
              placeholder="0"
            />
          }
        />
      </Modal>
    </AddTokenModalWrapper>
  );
};

export default AddTokenModal;
