import styled from 'styled-components/macro';
import * as React from 'react';
import { Modal, Button, Input, Form, Upload } from 'antd';
import LabelInput from '../../common/LabelInput';
import TextArea from 'antd/lib/input/TextArea';
import {
  useCreateTeamDocumentMutation,
  useUpdateTeamDocumentByIdMutation,
} from 'generated/graphql';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';
import { toast } from 'react-toastify';
import { s3FileUpload } from 'apis/storage';
import config from 'config';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';

const DocumentModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface DocumentModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  user_id: number;
  teamId: number;
  refetchTeamDocuments: () => void;
  setDocumentData: ({ }) => void;
  documentData: any;
}

const DocumentModal = ({
  setIsModalVisible,
  isModalVisible,
  user_id,
  teamId,
  documentData,
  setDocumentData,
  refetchTeamDocuments,
}: DocumentModalProps): JSX.Element => {
  const [form] = Form.useForm();
  const [createTeamDocument] = useCreateTeamDocumentMutation();
  const [updateTeamDocument] = useUpdateTeamDocumentByIdMutation();
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  const [document, setDocument] = React.useState<any>({
    url: '',
  });

  React.useEffect(()=>{
    setDocument({
      url:documentData?.url
    })
  },[documentData])

  console.log(document.url, 'url');
  const edit = !!documentData.key;
  const handleOk = async (formValues: any) => {
    setConfirmLoading(true);
    {
      edit
        ? await updateTeamDocument({
          variables: {
            name: formValues.name,
            description: formValues.description,
            url: document?.url,
            id: documentData.key,
          },
        })
        : await createTeamDocument({
          variables: {
            name: formValues.name,
            description: formValues.description,
            url: document?.url,
            uploaderId: user_id,
            teamId,
          },
        });
    }
    setConfirmLoading(false);
    setIsModalVisible(false);
    setDocumentData({});
    refetchTeamDocuments();
    toast.success(
      `${edit ? 'Updated' : 'Added'} Document : ${formValues.name}`
    );
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setDocumentData({});
  };

  return (
    <DocumentModalWrapper>
      {/* to add ctas texts for edit also */}
      <Modal
        title={`${edit ? 'Edit' : 'Add'} Document`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Document Name"
            name="name"
            initialValue={documentData.name}
            rules={[{ required: true, message: 'Name is required!' }]}
            inputElement={
              <Input
                defaultValue={documentData.name}
                placeholder="Document Name"
              />
            }
          />
          <LabelInput
            label="Description"
            name="description"
            rules={[{ required: true, message: 'Description is required!' }]}
            initialValue={documentData.description}
            inputElement={
              <TextArea
                defaultValue={documentData.description}
                placeholder="Description"
              />
            }
          />
          {/* <Form.Item label="ATTACHMENT" name={"url"}>
            
          </Form.Item> */}
          <LabelInput
            label="Attachment"
            name="url"
            rules={[{ required: true, message: 'Document is required!' }]}
            initialValue={documentData.url ? `${config.apiBaseUrl}files/${documentData.url}` : ''}
            inputElement={
             <>
             {document?.url ? (
              <div>
                <a title={document?.url} href={`${config.apiBaseUrl}files/${document?.url}`}>{document?.url}</a> <DeleteOutlined onClick={()=>setDocument({})} style={{ color: "red" }} />
              </div>
            ) :
              <Upload
                // defaultFileList={fileData}
                maxCount={1}
                // disabled={fileData?.url?.length > 0 ? true : false}
                customRequest={async ({
                  file,
                  fileData,
                  onProgress,
                  onSuccess,
                }: any) => {
                  let fileupload = await s3FileUpload(file);
                  if (fileupload?.status === 200) {
                    setDocument({
                      ...document,
                      url: fileupload?.data?.fileId,
                    });
                    onProgress({ percent: 100 });
                    onSuccess(() => console.log("onSuccess"));
                  }
                }}
                accept="image/png, image/jpeg, .doc, .docx, application/pdf"
                beforeUpload={(file: any) => {
                  let type = file.type;
                  let types = ['image/png', 'image/jpeg', 'doc', 'docx', 'application/pdf'];
                  if (!type) {
                    let splitName = file.name.split('.');
                    type = splitName?.[splitName?.length - 1];
                  }
                  const allowedTypes = types.includes(type)
                  if (!allowedTypes) {
                    toast.error(`${file.name} is not a ${types?.join(',')} type`);
                  }
                  return allowedTypes || Upload.LIST_IGNORE;
                }}
              >
                <Button icon={<PlusOutlined />}></Button>
              </Upload>}
             </>
            }
          />
        </Form>
      </Modal>
    </DocumentModalWrapper>
  );
};

export default DocumentModal;
