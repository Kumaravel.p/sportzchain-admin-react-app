import styled from 'styled-components/macro';
import React from 'react';
import { Modal, Form, Input, AutoComplete } from 'antd';
import { toast } from 'react-toastify';

import {
  useGetTeamContactRolesQuery,
} from 'generated/graphql';
import LabelInput from '../../common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import { countryCode } from 'utils/countryCodeList';
import SelectWithInput from 'components/common/selectWithInput';
import { upsertTeamContact } from 'apis/teamsApi';
import { TeamContactPayload } from 'ts/interfaces/teams.interface';

const ContactsModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

const FormWrapper = styled('div')`
& form{
  & > div{
  padding:0;
  .ant-form-item{
    margin-bottom:10px
  }
}
}
`;

interface ContactsModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  creatorId: number;
  teamId: number;
  refetchTeamContacts: () => void;
  setContactData: ({ }) => void;
  contactData: any;
}

const divisionOptions = [
  { label: 'Polls' },
  { label: 'Contests' },
  { label: 'Posts' }
]

const ContactsModal = ({
  setIsModalVisible,
  isModalVisible,
  creatorId,
  teamId,
  contactData,
  setContactData,
  refetchTeamContacts,
}: ContactsModalProps): JSX.Element => {
  const [form] = Form.useForm();

  const { data: teamContactRoles } = useGetTeamContactRolesQuery();
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  // const [contactRole, setContactRole] = React.useState('');
  const edit = !!contactData.key;
  const handleOk = async (formValues: any) => {
    const { name, division, primaryPhoneNumber, secondaryPhoneNumber, email, roleId } = formValues;
    setConfirmLoading(true);

    let payload:TeamContactPayload={
      ...((contactData?.key) && { id: contactData?.key }),
      division,
      email,
      name,
      primaryPhoneNumber:primaryPhoneNumber?.mobileNumber,
      primaryCountryCode:primaryPhoneNumber?.countryCode,
      roleId,
      secondaryPhoneNumber:secondaryPhoneNumber?.mobileNumber,
      secondaryCountryCode:secondaryPhoneNumber?.countryCode,
      teamId,
      userId:creatorId
    }

    upsertTeamContact(payload)
    .then(res=>{
      if(res?.data?.statusCode === "500"){
        setConfirmLoading(false);
        toast.error(res?.data?.message);
        return false
      }
      setConfirmLoading(false);
      setIsModalVisible(false);
      setContactData({});
      refetchTeamContacts();
      toast.success(res?.data?.message);
    })
    .catch(e=>{
      setConfirmLoading(false);
      toast.error(e?.message);
    })
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <ContactsModalWrapper>
      {/* to add ctas texts for edit also */}
      <Modal
        title={`${edit ? 'Edit' : 'Add'} Contact`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText={edit ? "Update" : "Save"}
      >
        <FormWrapper>
          <Form onFinish={handleOk} form={form} preserve={false}>
            <LabelInput
              label="Name"
              name="name"
              rules={[{ required: true, message: 'Name is required!' }]}
              initialValue={contactData?.name}
              inputElement={
                <Input
                  defaultValue={contactData?.name}
                  type="text"
                  placeholder="Name"
                />
              }
            />
            <LabelInput
              label="Division"
              name="division"
              rules={[{ required: false, message: 'Division is required!' }]}
              initialValue={contactData?.division}
              inputElement={
                // <Input
                //   defaultValue={contactData?.division}
                //   type="text"
                //   placeholder="Division"
                // />
                <SelectElement
                  onChange={(e: any) => {
                    form.setFieldsValue({ division: e });
                  }}
                  placeholder="Division"
                  toFilter="label"
                  defaultValue={contactData?.division ?? []}
                  options={divisionOptions}
                  mode={"multiple"}
                />
              }
            />
            <LabelInput
              label="Role"
              name="roleId"
              initialValue={contactData?.roleId}
              rules={[
                {
                  required: true,
                  message: 'Role is required!',
                },
              ]}
              inputElement={
                <SelectElement
                  onChange={(e: any) => form.setFieldsValue({ roleId: e })}
                  placeholder="Role"
                  toFilter="role"
                  defaultValue={contactData?.roleId}
                  options={teamContactRoles?.teamContactRole}
                />
              }
            />
            <LabelInput
              name="primaryPhoneNumber"
              label="Primary Contact Number"
              initialValue={contactData?.primaryPhoneNumber}
              rules={[
                () => ({
                  validator(_, value) {
                    if (!(!!value?.countryCode)) {
                      return Promise.reject(new Error('Country code is required!'));
                    }
                    else if (!(!!value?.mobileNumber)) {
                      return Promise.reject(new Error('Mobile Number is required!'));
                    }
                    return Promise.resolve();
                  },
                }),
              ]}
              inputElement={
                // <Input
                //   type="tel"
                //   defaultValue={contactData?.primaryPhoneNumber}
                //   placeholder="Primary Contact Number"
                // />
                <SelectWithInput
                  options={countryCode}
                  optionLabel="dial_code"
                  optionValue="dial_code"
                  value={contactData?.primaryPhoneNumber}
                  placeholder="Primary Contact Number"
                  onChange={(e: any) => {
                    form.setFieldsValue({ primaryPhoneNumber: e });
                  }}
                />
              }
            />
            <LabelInput
              name="secondaryPhoneNumber"
              label="Secondary Contact Number"
              initialValue={contactData?.secondaryPhoneNumber}
              rules={[
                {
                  required: false,
                  message: 'Secondary Contact Number is required!',
                },
              ]}
              inputElement={
                // <Input
                //   type="tel"
                //   defaultValue={contactData?.secondaryPhoneNumber}
                //   placeholder="Secondary Contact Number"
                // />
                <SelectWithInput
                  options={countryCode}
                  optionLabel="dial_code"
                  optionValue="dial_code"
                  value={contactData?.secondaryPhoneNumber}
                  placeholder="Enter Primary Contact"
                  onChange={(e: any) => {
                    form.setFieldsValue({ secondaryPhoneNumber: e });
                  }}
                />
              }
            />
            <LabelInput
              label="EMAIL"
              name="email"
              initialValue={contactData?.email}
              rules={[
                {
                  required: true,
                  type: "email",
                  message: 'Email is required!',
                },
              ]}
              inputElement={
                <Input
                  type="email"
                  defaultValue={contactData?.email}
                  placeholder="xyz@mail.com"
                />
              }
            />
          </Form>
        </FormWrapper>
      </Modal>
    </ContactsModalWrapper>
  );
};

export default ContactsModal;
