import styled from 'styled-components/macro';
import * as React from 'react';
import { Modal, Form, Input, Upload } from 'antd';
import LabelInput from '../../common/LabelInput';
import TextArea from 'antd/lib/input/TextArea';
import {
  useCreateTeamNoteMutation,
  useUpdateTeamNoteByIdMutation,
} from 'generated/graphql';
import { toast } from 'react-toastify';

const NoteModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
      height: fit-content;
      margin: 0;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface NoteModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  creatorId: number;
  teamId: number;
  refetchTeamNotes: () => void;
  setNoteData: ({ }) => void;
  noteData: any;
  disabled?: boolean
}

const NoteModal = ({
  setIsModalVisible,
  isModalVisible,
  creatorId,
  teamId,
  noteData,
  setNoteData,
  refetchTeamNotes,
  disabled = false
}: NoteModalProps): JSX.Element => {
  const [form] = Form.useForm();
  const [createTeamNote] = useCreateTeamNoteMutation();
  const [updateTeamNote] = useUpdateTeamNoteByIdMutation();
  const edit = !!noteData.key;

  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const handleOk = async (formValues: any) => {
    setConfirmLoading(true);
    if (!disabled) {
      edit
        ? await updateTeamNote({
          variables: {
            id: noteData.key,
            content: formValues.content,
            name: formValues.name,
          },
        })
        : await createTeamNote({
          variables: {
            content: formValues.content,
            creatorId,
            teamId,
            name: formValues.name,
          },
        });
    }
    setConfirmLoading(false);
    setIsModalVisible(false);
    setNoteData({});
    refetchTeamNotes();
    toast.success(`${edit ? 'Updated' : 'Added'} Note : ${formValues.name}`);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setNoteData({});
  };

  const giveMeTitle = () => {
    let title = "";
    if (disabled) {
      title = "View";
    } else if (edit) {
      title = "Edit";
    } else if (!edit) {
      title = "Add"
    }
    return title + " Note";
  }

  return (
    <NoteModalWrapper>
      <Modal
        title={giveMeTitle()}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
        okButtonProps={{ disabled: disabled }}
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Note Name"
            name="name"
            rules={[{ required: true, message: 'Note Name is required!' }]}
            initialValue={noteData?.name}
            inputElement={
              <Input
                defaultValue={noteData?.name}
                type="text"
                placeholder="Note Name"
                readOnly={disabled}
              />
            }
          />
          <LabelInput
            label="Note"
            initialValue={noteData?.content}
            rules={[
              { required: true, message: 'Note Description is required!' },
            ]}
            name="content"
            inputElement={
              <TextArea defaultValue={noteData?.content} placeholder="Note" readOnly={disabled} />
            }
          />
        </Form>
      </Modal>
    </NoteModalWrapper>
  );
};

export default NoteModal;
