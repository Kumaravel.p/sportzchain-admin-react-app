import styled from 'styled-components/macro';
import * as React from 'react';
import { Modal, Button, Form, Upload, Input,Spin } from 'antd';
import LabelInput from '../../common/LabelInput';
import {
  useUpdateTeamDisplayInfoMutation,
} from 'generated/graphql';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';
import { toast } from 'react-toastify';
import config from 'config';
import { s3FileUpload } from 'apis/storage';
import { LoadingOutlined  } from '@ant-design/icons';

const ResolutionHint = styled.p`
    color: #bfbfbf;
    /* font-weight: 600; */
    margin-top: 0.5rem;
    margin-bottom: 0;
`;

const VideoModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface VideoModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  teamId: number;
  refetchTeamVideoUrl: () => void;
  existingTeamData: any;
  setVideoUrlData: React.Dispatch<React.SetStateAction<string>>;
  videoUrlData: string;
}

const VideoModal = ({
  setIsModalVisible,
  isModalVisible,
  teamId,
  videoUrlData,
  setVideoUrlData,
  refetchTeamVideoUrl,
  existingTeamData,
}: VideoModalProps): JSX.Element => {
  const [form] = Form.useForm();
  const [updateTeamVideo] = useUpdateTeamDisplayInfoMutation();
  const [loading, setLoading] = React.useState(false);

  const [video, setVideo] = React.useState<string>('');

  React.useEffect(()=>{
    setVideo(videoUrlData)
  },[videoUrlData])

  const handleOk = async () => {
    setLoading(true);

    const { facebookUrl, twitterUrl, instagramUrl, telegramUrl, websiteUrl } =
      existingTeamData?.socialMedia[0];

    const { bannerURLS, description, profileImageURL } = existingTeamData;

    await updateTeamVideo({
      variables: {
        teamId,
        videoURL: video,
        profileImageURL,
        facebookUrl,
        twitterUrl,
        instagramUrl,
        telegramUrl,
        websiteUrl,
        bannerURLS,
        description,
      },
    });
    refetchTeamVideoUrl();
    setLoading(false);
    setIsModalVisible(false);
    setVideoUrlData(video);
    toast.success('Updated Video!');
  };
  const handleCancel = () => {
    setIsModalVisible(false);
    setVideoUrlData(videoUrlData);
  };

  const beforeUpload = (file:any) =>{
    let type = file.type === "video/mp4";
    if (!type) {
      toast.error(`${file.name} is not a Mp4 file`);
    }
    return type || Upload.LIST_IGNORE;
  }

  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

  return (
    <VideoModalWrapper>
      <Modal
        title={`Update Video`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={loading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Video URL"
            // name="url"
            // rules={[{ required: true, message: 'Video URL is required!' }]}
            // initialValue={video}
            inputElement={
              <Input
                placeholder="Video URL"
                value={video}
                onChange={(e) => {
                  setVideo(e.target.value)
                  form.setFieldsValue({url:e.target.value})
                }}
              />
            }
          />
          <div>OR</div>
          <LabelInput
            label="Attachment"
            name="url"
            // rules={[{ required: true, message: 'Video URL is required!' }]}
            initialValue={video}
            inputElement={
              <>
              <Upload
                iconRender={() => <></>}
                accept="video/*"
                beforeUpload={(file: any) => beforeUpload(file)}
                customRequest={async ({ file, fileList, onProgress }: any) =>{
                  setLoading(true)
                  let fileupload = await s3FileUpload(file);
                  if(fileupload?.status === 200){
                    setVideo(fileupload?.data?.fileId)
                    form.setFieldsValue({url:fileupload?.data?.fileId})
                    toast.success("File uploaded successfully");
                  }
                  else{
                    toast.error("Video upload failed");
                  }
                  setLoading(false)
                }}
              >
                <Button className="upload-button">{loading ? <Spin indicator={antIcon} /> : '+'}</Button>
              </Upload>
              <ResolutionHint>Recommended resolution :- 750 x 406 px, Supported formats :- MP4</ResolutionHint>
              </>
            }
          />
        </Form>
      </Modal>
    </VideoModalWrapper>
  );
};

export default VideoModal;
