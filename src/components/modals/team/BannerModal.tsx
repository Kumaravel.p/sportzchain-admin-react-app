import styled from 'styled-components/macro';
import * as React from 'react';
import { Modal, Button, Form, Upload } from 'antd';
import LabelInput from '../../common/LabelInput';
import {
  useUpdateTeamDisplayInfoMutation,
} from 'generated/graphql';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';

const BannerModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface BannerModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  teamId: number;
  refetchBannerData: () => void;
  bannerData: any;
  setBannerData: ({ }) => void;
  existingTeamData: any;
}

const BannerModal = ({
  setIsModalVisible,
  isModalVisible,
  teamId,
  bannerData,
  setBannerData,
  existingTeamData,
  refetchBannerData,
}: BannerModalProps): JSX.Element => {
  const [form] = Form.useForm();
  // const [Banner] = BannerMutation();
  const [updateBanner] = useUpdateTeamDisplayInfoMutation();
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  const [banner, setBanner] = React.useState<any>([]);
  const [uploadedBanners, setUploadedBanners] = React.useState<any>([]);


  const handleOk = async (formValues: any) => {
    setConfirmLoading(true);
    const { facebookUrl, twitterUrl, instagramUrl, telegramUrl, websiteUrl } =
      existingTeamData?.socialMedia[0];
    const { profileImageURL, description } = existingTeamData;
    await updateBanner({
      variables: {
        teamId,
        bannerURLS: banner,
        facebookUrl,
        twitterUrl,
        instagramUrl,
        telegramUrl,
        websiteUrl,
        profileImageURL,
        description,
      },
    });

    setConfirmLoading(false);
    setIsModalVisible(false);
    setBannerData({});
    refetchBannerData();
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setBannerData({});
  };

  return (
    <BannerModalWrapper>
      {/* to add ctas texts for edit also */}
      <Modal
        title={`Update Logo Banner`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Attachment"
            name="url"
            rules={[{ required: true, message: 'Banner is required!' }]}
            initialValue={bannerData?.url}
            inputElement={
              <Upload
                iconRender={() => <></>}
                onChange={({ file, fileList }) => {
                  console.log(file, fileList, 'filelist');
                  setUploadedBanners(fileList);
                }}
                customRequest={async ({ file, fileList, onProgress }: any) => {
                  console.log(typeof file, fileList, 'filelist 2 ');
                  uploadedBanners &&
                    uploadedBanners.map(async (files: any) => {
                      console.log(typeof files, 'files');
                      setBanner([
                        ...banner,
                        await handleMediaUpload(
                          files as RcFile,
                          onProgress
                        ),
                      ]);
                    });
                }}
              >
                <Button className="upload-button">+</Button>
              </Upload>
            }
          />
        </Form>
      </Modal>
    </BannerModalWrapper>
  );
};

export default BannerModal;
