import styled from 'styled-components/macro';
import * as React from 'react';
import { Modal, Button, Form, Upload, Spin } from 'antd';
import LabelInput from '../../common/LabelInput';
import {
  useUpdateTeamDisplayInfoMutation,
} from 'generated/graphql';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';
import { toast } from 'react-toastify';
import config from 'config';
import { s3FileUpload } from "../../../apis/storage";
import { LoadingOutlined  } from '@ant-design/icons';

const ResolutionHint = styled.p`
    color: #bfbfbf;
    /* font-weight: 600; */
    margin-top: 0.5rem;
    margin-bottom: 0;
`;

const LogoBannerModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface LogoBannerModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  teamId: number;
  refetchTeamLogoBanner: () => void;
  existingTeamData: any;
  setLogoBannerData: ({ }) => void;
  logoBannerData: any;
}

const LogoBannerModal = ({
  setIsModalVisible,
  isModalVisible,
  teamId,
  logoBannerData,
  setLogoBannerData,
  refetchTeamLogoBanner,
  existingTeamData,
}: LogoBannerModalProps): JSX.Element => {
  const [form] = Form.useForm();
  // const [createTeamLogoBanner] = useCreateTeamLogoBannerMutation();
  const [updateTeamLogoBanner] = useUpdateTeamDisplayInfoMutation();
  const [loading,setLoading] = React.useState<any>(null);

  const [logoBanner, setLogoBanner] = React.useState<any>({
    url: '',
  });

  React.useEffect(()=>{
    setLogoBanner({
      url:logoBannerData?.url
    })
  },[logoBannerData])

  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;


  const handleOk = async () => {
    setLoading('uploading');

    const { facebookUrl, twitterUrl, instagramUrl, telegramUrl, websiteUrl } =
      existingTeamData?.socialMedia[0];

    const { bannerURLS, description } = existingTeamData;

    await updateTeamLogoBanner({
      variables: {
        teamId,
        profileImageURL: logoBanner.url,
        facebookUrl,
        twitterUrl,
        instagramUrl,
        telegramUrl,
        websiteUrl,
        bannerURLS,
        description,
      },
    });
    setLoading(null);
    setIsModalVisible(false);
    refetchTeamLogoBanner();
    setLogoBannerData({});
    toast.success('Banner Updated!');
  };
  const handleCancel = () => {
    setIsModalVisible(false);
    setLogoBannerData({});
  };

  const beforeUpload = (file:any) =>{
    const allowedTypes = file.type === 'image/png' || file.type === 'image/jpeg';
    if (!allowedTypes) {
      toast.error(`${file.name} is not a png or jpeg file`);
    }
    return allowedTypes || Upload.LIST_IGNORE;
  }

  return (
    <LogoBannerModalWrapper>
      <Modal
        title={`Update Logo Banner`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={loading === "uploading"}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Attachment"
            name="url"
            rules={[{ required: true, message: 'Logo Banner is required!' }]}
            initialValue={logoBannerData.url}
            inputElement={
              <>
              <Upload
                accept="image/png, image/jpeg"
                customRequest={async ({ file, fileList, onProgress }: any) => {
                  setLoading('teamLogo');
                  let fileupload = await s3FileUpload(file);
                  if (fileupload.status === 200) {
                    setLogoBanner({
                      url: fileupload.data.fileId
                    })
                    toast.success("File uploaded successfully");
                  }
                  else {
                    toast.error("File uploaded failed");
                  }
                  setLoading(null)
                }}
                showUploadList={false}
                beforeUpload={(file: any) => beforeUpload(file)}
              >
                <Button className="upload-button">
                {loading === "teamLogo" ? <Spin indicator={antIcon} /> : '+'}
                </Button>
              </Upload>
              <ResolutionHint>Recommended resolution :- 500x500 px, Supported formats :- PNG, JPEG</ResolutionHint>
              </>
            }
          />
          { 
            logoBanner?.url &&
            <img style={{width:'100px',height:'200px',objectFit:'cover'}} src={`${config.apiBaseUrl}files/${logoBanner?.url}`} />
          }
        </Form>
      </Modal>
    </LogoBannerModalWrapper>
  );
};

export default LogoBannerModal;
