import styled from 'styled-components/macro';
import * as React from 'react';
import {
  Modal,
  Button,
  Input,
  Form,
  DatePicker,
  Upload,
  AutoComplete,
} from 'antd';
import LabelInput from '../../common/LabelInput';
import {
  useCreateTeamContractMutation,
  useUpdateTeamContractByIdMutation,
} from 'generated/graphql';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';
import getUnixTime from 'date-fns/getUnixTime';
import moment, { Moment } from 'moment';
import { toast } from 'react-toastify';
import SelectElement from 'components/common/SelectElement';
import config from 'config';
import { DeleteOutlined } from '@ant-design/icons';
import { s3FileUpload } from 'apis/storage';

const ContractModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface DocumentModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  creatorId: number;
  teamContacts: any;
  teamId: number;
  refetchTeamContracts: () => void;
  setContractData: ({ }) => void;
  contractData: any;
}

enum status {
  ACTIVE = 'ACTIVE',
  INACTIVE = 'INACTIVE',
}

const DocumentModal = ({
  setIsModalVisible,
  isModalVisible,
  creatorId,
  teamContacts,
  teamId,
  contractData,
  setContractData,
  refetchTeamContracts,
}: DocumentModalProps): JSX.Element => {
  const [form] = Form.useForm();
  const [createTeamContract] = useCreateTeamContractMutation();
  const [updateTeamContract] = useUpdateTeamContractByIdMutation();
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  const [contract, setContract] = React.useState<any>({
    url: '',
  });

  const [uploadProgress, setUploadProgress] = React.useState(0);



  const edit = !!contractData.key;
  const handleOk = async (formValues: any) => {
    setConfirmLoading(true);

    let api = edit ? updateTeamContract : createTeamContract;

    await api({
      variables: {
        ...(contractData?.key && {id:contractData.key}),
        teamId,
        contractULR: contract?.url,
        startAt: contract?.contractstartAtendAt?.[0]?.toISOString(),
        endAt: contract?.contractstartAtendAt?.[1]?.toISOString(),
        fileName: contract.fileName,
        statusId: contract.statusId,
        superAdminSignerId: creatorId,
        startAtTimestamp: contract?.contractstartAtendAt?.[0]?.unix(),
        endAtTimestamp: contract?.contractstartAtendAt?.[1]?.unix(),
      }
    })

    // 
    // {
    //   edit
    //     ? await updateTeamContract({
    //       variables: {
    //         // url: contract?.url,
    //         teamId,
    //       },
    //     })
    //     : await createTeamContract({
    //       variables: {
    //         teamId,
    //         contractULR: contract?.url,
    //         startAt: contract?.contractstartAtendAt?.[0]?.toISOString(),
    //         endAt: contract?.contractstartAtendAt?.[1]?.toISOString(),
    //         fileName: contract.fileName,
    //         statusId: contract.statusId,
    //         superAdminSignerId: creatorId,
    //         startAtTimestamp: contract?.contractstartAtendAt?.[0]?.unix(),
    //         endAtTimestamp: contract?.contractstartAtendAt?.[1]?.unix(),
    //       },
    //     });
    // }
    setConfirmLoading(false);
    setIsModalVisible(false);
    setContractData({});
    refetchTeamContracts();
    toast.success(
      `${edit ? 'Updated' : 'Added'} Contract : ${contract.fileName}`
    );
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setContractData({});
  };
  React.useEffect(() => {
    let modifyData = {
      ...contractData,
      url: contractData?.contractULR ?? '',
      contractstartAtendAt: [moment(contractData?.startAt), moment(contractData?.endAt)]
    }
    setContract(modifyData)
    form.setFieldsValue(modifyData)
  }, [contractData])

  const removeFile = () =>{
    form.setFieldsValue({
      url: '',
      fileName: ''
    });
    setContract({ ...contract, url: '', fileName: '' })
  }

  return (
    <ContractModalWrapper>
      {/* to add ctas texts for edit also */}
      <Modal
        title={`${edit ? 'Edit' : 'Add'} Contract Document`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Document Name"
            name="name"
            initialValue={contractData.fileName}
            rules={[{ required: true, message: 'Name is required!' }]}
            inputElement={
              <Input
                onChange={(e) => {
                  setContract({ ...contract, fileName: e.target.value })
                  form.setFieldsValue({ name: e.target.value })
                }
                }
                defaultValue={contractData.fileName}
                placeholder="Document Name"
              />
            }
          />
          <LabelInput
            label="Contract Duration Range"
            name="contractstartAtendAt"
            // initialValue={contractData.name}
            rules={[{ required: true, message: 'Start - End date' }]}
            inputElement={
              <DatePicker.RangePicker
                format={'DD/MM/YYYY'}
                showSecond={false}
                // defaultValue={defaultValue}
                // defaultValue={contract?.contractstartAtendAt}
                onChange={(e: any) => {
                  setContract({
                    ...contract,
                    contractstartAtendAt: e
                  })
                  form.setFieldsValue({
                    contractstartAtendAt: e
                  })
                }}
              />
            }
          />
          <LabelInput
            label="Status"
            name="statusId"
            initialValue={contractData.statusId}
            rules={[{ required: true, message: 'Status is required!' }]}
            inputElement={
              <SelectElement
                onChange={(e) => {
                  setContract({ ...contract, statusId: e })
                  form.setFieldsValue({ statusId: e })
                }}
                placeholder="Status"
                defaultValue={contractData.statusId}
                toFilter="value"
                options={[{ value: status.ACTIVE }, { value: status.INACTIVE }]}
              />
            }
          />
          <LabelInput
            label="Attachment"
            name="url"
            rules={[{ required: true, message: 'Document is required!' }]}
            initialValue={contractData.url}
            inputElement={
              <>
                {contract?.url ? (
                  <div>
                    <a title={contract?.fileName} href={`${config.apiBaseUrl}files/${contract?.url}`}>{contract?.fileName}</a> <DeleteOutlined onClick={removeFile} style={{ color: "red" }} />
                  </div>) :
                  <Upload
                    // iconRender={() => <></>}
                    defaultFileList={[]}
                    customRequest={async ({ file, fileList, onProgress, onSuccess }: any) => {
                      let fileupload = await s3FileUpload(file);
                      if (fileupload?.status === 200) {
                        setContract({
                          ...contract,
                          url: fileupload?.data?.fileId,
                          fileName: file?.name
                        });
                        form.setFieldsValue({
                          url: fileupload?.data?.fileId,
                          fileName: file?.name
                        });
                        onProgress({ percent: 100 });
                        onSuccess(() => console.log("onSuccess"));
                      }
                    }}
                    accept="image/png, image/jpeg, .doc, .docx, application/pdf"
                    beforeUpload={(file: any) => {
                      let type = file.type;
                      let types = ['image/png', 'image/jpeg', 'doc', 'docx', 'application/pdf'];
                      if (!type) {
                        let splitName = file.name.split('.');
                        type = splitName?.[splitName?.length - 1];
                      }
                      const allowedTypes = types.includes(type)
                      if (!allowedTypes) {
                        toast.error(`${file.name} is not a ${types?.join(',')} type`);
                      }
                      return allowedTypes || Upload.LIST_IGNORE;
                    }}
                  >
                    <Button className="upload-button">+</Button>
                  </Upload>
                }
              </>
            }
          />
        </Form>
      </Modal>
    </ContractModalWrapper>
  );
};

export default DocumentModal;
