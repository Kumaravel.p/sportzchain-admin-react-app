import styled from 'styled-components/macro';
import React from 'react';
import { Modal, Form, Input } from 'antd';
import LabelInput from '../../common/LabelInput';
import { useUpdateTeamDisplayInfoMutation } from 'generated/graphql';
import { toast } from 'react-toastify';

const SocialMediaModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface SocialMediaModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  creatorId?: number;
  teamId: number;
  refetchTeamSocialMedia: () => void;
  setSocialMediaData: ({}) => void;
  socialMediaData: any;
  existingTeamData: any;
}

const SocialMediaModal = ({
  setIsModalVisible,
  isModalVisible,
  creatorId,
  teamId,
  socialMediaData,
  setSocialMediaData,
  refetchTeamSocialMedia,
  existingTeamData,
}: SocialMediaModalProps): JSX.Element => {
  const [form] = Form.useForm();

  const [updateTeamSocialMedia] = useUpdateTeamDisplayInfoMutation();
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  console.log(socialMediaData);
  const handleOk = async (formValues: any) => {
    const { websiteUrl, twitterUrl, telegramUrl, instagramUrl, facebookUrl } =
      formValues;
    setConfirmLoading(true);

    await updateTeamSocialMedia({
      variables: {
        teamId,
        websiteUrl,
        twitterUrl,
        telegramUrl,
        instagramUrl,
        facebookUrl,
        ...existingTeamData,
      },
    });

    setConfirmLoading(false);
    setIsModalVisible(false);
    setSocialMediaData({});
    refetchTeamSocialMedia();
    toast.success('Updated Social Media!');
  };

  const handleCancel = () => {
    setSocialMediaData({});
    setIsModalVisible(false);
  };

  return (
    <SocialMediaModalWrapper>
      <Modal
        title={`Update Social Media`}
        visible={isModalVisible}
        onOk={form.submit}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Facebook"
            name="facebookUrl"
            rules={[{ type: 'url', message: 'Please enter a valid URL' }]}
            initialValue={socialMediaData?.facebookUrl}
            inputElement={
              <Input
                defaultValue={socialMediaData?.facebookUrl}
                placeholder="Facebook"
              />
            }
          />
          <LabelInput
            label="Website"
            name="websiteUrl"
            initialValue={socialMediaData?.websiteUrl}
            rules={[{ type: 'url', message: 'Please enter a valid URL' }]}
            inputElement={
              <Input
                defaultValue={socialMediaData?.websiteUrl}
                placeholder="Website"
              />
            }
          />
          <LabelInput
            label="Twitter"
            name="twitterUrl"
            initialValue={socialMediaData?.twitterUrl}
            rules={[{ type: 'url', message: 'Please enter a valid URL' }]}
            inputElement={
              <Input
                defaultValue={socialMediaData?.twitterUrl}
                placeholder="Twitter"
              />
            }
          />
          <LabelInput
            label="Telegram"
            name="telegramUrl"
            initialValue={socialMediaData?.telegramUrl}
            rules={[{ type: 'url', message: 'Please enter a valid URL' }]}
            inputElement={
              <Input
                defaultValue={socialMediaData?.telegramUrl}
                placeholder="Telegram"
              />
            }
          />
          <LabelInput
            label="Instagram"
            name="instagramUrl"
            rules={[{ type: 'url', message: 'Please enter a valid URL' }]}
            initialValue={socialMediaData?.instagramUrl}
            inputElement={
              <Input
                defaultValue={socialMediaData?.instagramUrl}
                placeholder="Instagram"
              />
            }
          />
        </Form>
      </Modal>
    </SocialMediaModalWrapper>
  );
};

export default SocialMediaModal;
