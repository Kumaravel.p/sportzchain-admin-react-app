import styled from 'styled-components/macro';
import React from 'react';
import { Modal, Form, Input, Spin } from 'antd';
import LabelInput from '../../common/LabelInput';

import {
  useCreateTeamAddressMutation,
  useUpdateTeamAddressByIdMutation,
  useGetTeamAddressTypesQuery,
  //   useUpdateTeamAddressByIdMutation,
} from 'generated/graphql';
import SelectElement from 'components/common/SelectElement';
import { toast } from 'react-toastify';
import config from 'config';
import { LoadingOutlined  } from '@ant-design/icons';

const AddressModalWrapper = styled.div`
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.cancel {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
  `;

  const FormWrapper = styled('div')`
  & .spin-wrapper{
    position: absolute;
    inset: 0;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    z-index: 1;
    justify-content: center;
    background: #00000047;
  }
    & form{
      & > div{
      padding:0;
      .ant-form-item{
        margin-bottom:10px
      }
    }
  }
  `;

interface AddressModalProps {
  setIsModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  isModalVisible: boolean;
  creatorId: number;
  teamId: number;
  refetchTeamAddress: () => void;
  setAddressData: ({}) => void;
  addressData: any;
}

const AddressModal = ({
  setIsModalVisible,
  isModalVisible,
  creatorId,
  teamId,
  addressData,
  setAddressData,
  refetchTeamAddress,
}: AddressModalProps): JSX.Element => {
  const [form] = Form.useForm();

  const [createTeamAddress] = useCreateTeamAddressMutation();
  const [updateTeamAddress] = useUpdateTeamAddressByIdMutation();
  const { data: teamAddressTypes } = useGetTeamAddressTypesQuery();
  const [loading, setLoading] = React.useState<boolean>(false);

  const [countriesOptions,setCountriesOptions] = React.useState([]);
  const [stateOptions,setStateOptions] = React.useState([]);
  const [citiesOptions,setCitiesOptions] = React.useState([]);

  // console.log(addressData);
  const edit = !!addressData.id;
  const addressId = addressData.id

  const getRequestOptions = () =>{
    var headers = new Headers();
    headers.append("X-CSCAPI-KEY", config.countriesStateCitiesApiKey);
    let requestOptions:RequestInit = {
      method: 'GET',
      headers,
      redirect: "follow"
    };
    return requestOptions
  }

  React.useEffect(()=>{
    if(!edit){
      getAllCountriesOrStateOrCity('',null,'countries',setCountriesOptions)
    }
  },[]);

  React.useEffect(()=>{
    if(edit){
      getAllCountriesStateCity()
    }
  },[edit]);


  const getAllCountriesOrStateOrCity = async (key?:string,value?:any,url?:string,callback?:any,) =>{
    
    if (key && value) {
      form.setFieldsValue({
        ...form.getFieldsValue(),
        [key]: value,
        state:key === "state" ? value : key === "countryTitle" ? null : form.getFieldsValue().state,
        city:key === "city" ? value : (key === "state" || key === "countryTitle") ? null : form.getFieldsValue().city,
      })
    }

    if(url){
      setLoading(true);
      if(url === "countries"){
        url=""
      }
      await fetch(`https://api.countrystatecity.in/v1/countries${url}`, getRequestOptions())
      .then(response => response.json())
      .then(result => {
        if(callback){
          callback(result?.length ? result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})) : [])
          setLoading(false);
        }
      })
      .catch(error => {
        console.log('error', error)
        setLoading(false);
      });
    }
  }

  const getAllCountriesStateCity = async () =>{
    setLoading(true);
    //get all the countries
    await fetch(`https://api.countrystatecity.in/v1/countries`, getRequestOptions())
      .then(response => response.json())
      .then(result => {
        if(result?.length){
          setCountriesOptions(result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})))
        }
      })
      .catch(error => console.log('error', error));
      
      //get all the states by country
    if(addressData?.countryTitle?.value){
      await fetch(`https://api.countrystatecity.in/v1/countries/${addressData?.countryTitle.value}/states`, getRequestOptions())
      .then(response => response.json())
      .then(result => {
        if(result?.length){
          setStateOptions(result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})))
        }
      })
      .catch(error => console.log('error', error));

      //get all the cities by country and state
      if(addressData?.state?.value){
        await fetch(`https://api.countrystatecity.in/v1/countries/${addressData?.countryTitle?.value}/states/${addressData?.state?.value}/cities`, getRequestOptions())
        .then(response => response.json())
        .then(result => {
          if(result?.length){
            setCitiesOptions(result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})))
          }
        })
        .catch(error => console.log('error', error));
      }
      setLoading(false);
    }
  }

  const handleOk = async (formValues: any) => {
    const { state, line2="", line1, countryTitle, city, teamAddressTypeId,pincode } = formValues;
    setLoading(true);

    let modifyState = stateOptions?.length ? JSON.stringify({label:state?.label,value:state?.value}) : "";
    let modifyCountry = JSON.stringify({label:countryTitle?.label,value:countryTitle?.value});
    
    {
      edit
        ? await updateTeamAddress({
            variables: {
              teamId,
              state:modifyState,
              line2,
              line1,
              countryTitle:modifyCountry,
              city:(stateOptions?.length && citiesOptions?.length) ? city : "",
              pincode,
              teamAddressTypeId,
              addressId
            },
          })
        : await createTeamAddress({
            variables: {
              teamId,
              state:modifyState,
              line2,
              line1,
              countryTitle:modifyCountry,
              city:(stateOptions?.length && citiesOptions?.length) ? city : "",
              teamAddressTypeId,
              pincode
            },
          });
    }

    setLoading(false);
    setIsModalVisible(false);
    setAddressData({});
    refetchTeamAddress();
    toast.success(`${edit ? 'Updated' : 'Added'} ${teamAddressTypeId} Address`);
  };

  const handleCancel = () => {
    setAddressData({});
    setIsModalVisible(false);
  };

  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

  return (
    <AddressModalWrapper>
      {/* to add ctas texts for edit also */}
      <Modal
        title={`${edit ? 'Edit' : 'Add'} Address`}
        visible={isModalVisible}
        onOk={form.submit}
        // confirmLoading={loading}
        onCancel={handleCancel}
        destroyOnClose={true}
        okText="Save"
      >
        <FormWrapper>
        { loading &&
          <div className="spin-wrapper">
          <Spin indicator={antIcon} />
        </div>
        }
        <Form onFinish={handleOk} form={form} preserve={false}>
          <LabelInput
            label="Address Type"
            name="teamAddressTypeId"
            rules={[{ required: true, message: 'Address Type is required!' }]}
            initialValue={addressData?.teamAddressTypeId}
            inputElement={
              <SelectElement
                onChange={(e) => form.setFieldsValue({ teamAddressTypeId: e })}
                placeholder="Type"
                defaultValue={addressData?.teamAddressTypeId}
                toFilter="type"
                options={teamAddressTypes?.teamAddressType}
              />
            }
          />

          <LabelInput
            label="Country"
            name="countryTitle"
            rules={[{ required: true, message: 'Country is required!' }]}
            initialValue={addressData?.countryTitle ?? addressData?.country}
            inputElement={
              <SelectElement
                searchable
                labelInValue
                defaultValue={addressData?.countryTitle}
                onChange={(e:any) => getAllCountriesOrStateOrCity('countryTitle',e,`/${e?.value}/states`,setStateOptions)}
                options={countriesOptions ?? []}
                toStore="value"
                toFilter="name"
                placeholder="Country"
              />
            }
          />

          <LabelInput
            label="State"
            name="state"
              rules={[
                {
                  required: Boolean(stateOptions?.length),
                  message: 'State is required!'
                }]}
            initialValue={addressData?.state}
            inputElement={
              <SelectElement
                labelInValue
                onChange={(e: any) => getAllCountriesOrStateOrCity('state', e, `/${form.getFieldsValue().countryTitle?.value}/states/${e?.value}/cities`, setCitiesOptions)}
                defaultValue={addressData?.state}
                options={stateOptions ?? []}
                toFilter="name"
                disabled={!(!!form.getFieldsValue()?.countryTitle?.value)}
                toStore="value"
                placeholder="State"
                searchable={true}
              />
            }
          />

          <LabelInput
            label="City"
            name="city"
              rules={[
                {
                  required: Boolean(stateOptions?.length) && Boolean(citiesOptions?.length),
                  message: 'City is required!'
                }
              ]}
            initialValue={addressData?.city}
            inputElement={
              <SelectElement
                onChange={(e: React.ChangeEvent<HTMLInputElement>) => getAllCountriesOrStateOrCity('city', e,)}
                defaultValue={addressData?.city}
                options={citiesOptions ?? []}
                toFilter="name"
                placeholder="City"
                disabled={(!(!!form.getFieldsValue()?.countryTitle?.value) || !(!!form.getFieldsValue()?.state?.value))}
                searchable={true}
              />
            }
          />
          <LabelInput
            label="Pincode"
            name="pincode"
            rules={[{ required: true, message: 'Pincode is required!' }]}
            initialValue={addressData?.pincode}
            inputElement={
              <Input
                defaultValue={addressData?.pincode}
                type="text"
                placeholder="pincode"
              />
            }
          />
          <LabelInput
            label="Line 1"
            name="line1"
            rules={[{ required: true, message: 'Line 1 is required!' }]}
            initialValue={addressData?.line1}
            inputElement={
              <Input
                defaultValue={addressData?.line1}
                type="text"
                placeholder="Line 1"
              />
            }
          />
          <LabelInput
            label="Line 2"
            name="line2"
            rules={[{ required: false, message: 'Line 2 is required!' }]}
            initialValue={addressData?.line2}
            inputElement={
              <Input
                defaultValue={addressData?.line2}
                type="text"
                placeholder="Line 2"
              />
            }
          />
        </Form>
        </FormWrapper>
      </Modal>
    </AddressModalWrapper>
  );
};

export default AddressModal;
