interface DeleteIconProps {
    color?: string;
}

export const DeleteIcon = (props: DeleteIconProps) => {

    const {
    } = props;

    return (
        <svg width="12" height="14" viewBox="0 0 12 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0.75 3.39844H1.91667H11.25" stroke="#EF4444" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M3.6665 3.4V2.2C3.6665 1.88174 3.78942 1.57652 4.00821 1.35147C4.22701 1.12643 4.52375 1 4.83317 1H7.1665C7.47592 1 7.77267 1.12643 7.99146 1.35147C8.21025 1.57652 8.33317 1.88174 8.33317 2.2V3.4M10.0832 3.4V11.8C10.0832 12.1183 9.96025 12.4235 9.74146 12.6485C9.52267 12.8736 9.22592 13 8.9165 13H3.08317C2.77375 13 2.47701 12.8736 2.25821 12.6485C2.03942 12.4235 1.9165 12.1183 1.9165 11.8V3.4H10.0832Z" stroke="#EF4444" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M4.83301 6.40039V10.0004" stroke="#EF4444" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M7.16724 6.40039V10.0004" stroke="#EF4444" stroke-width="1.2" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>

    )
}