interface DeleteIconProps {
    color?: string;
}

export const DeleteIcon = (props: DeleteIconProps) => {

    const {
        color = "#10B981"
    } = props;

    return (
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M2 4H3.33333H14" stroke="#EF4444" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M5.33334 3.9987V2.66536C5.33334 2.31174 5.47382 1.9726 5.72387 1.72256C5.97392 1.47251 6.31305 1.33203 6.66668 1.33203H9.33334C9.68697 1.33203 10.0261 1.47251 10.2762 1.72256C10.5262 1.9726 10.6667 2.31174 10.6667 2.66536V3.9987M12.6667 3.9987V13.332C12.6667 13.6857 12.5262 14.0248 12.2762 14.2748C12.0261 14.5249 11.687 14.6654 11.3333 14.6654H4.66668C4.31305 14.6654 3.97392 14.5249 3.72387 14.2748C3.47382 14.0248 3.33334 13.6857 3.33334 13.332V3.9987H12.6667Z" stroke="#EF4444" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M6.66666 7.33203V11.332" stroke="#EF4444" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M9.33334 7.33203V11.332" stroke="#EF4444" stroke-width="1.6" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>

    )
}