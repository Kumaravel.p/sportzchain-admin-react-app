import { Progress } from 'antd';
import styled from 'styled-components/macro';

const ProgressBarLabelWrapper = styled.div`
  .label {
    font-size: 14px;
    line-height: 20px;
    color: #000000;
  }
`;

interface ProgressBarLabelProps {
  className?: string;
  label?: string;
  strokeColor?: string;
  trailColor?: string;
  percent?: number;
}

const ProgressBarLabel = ({
  className = '',
  label = '',
  trailColor = '',
  strokeColor = '',
  percent = 0,
}: ProgressBarLabelProps): JSX.Element => {
  return (
    <ProgressBarLabelWrapper>
      <div className="label">{label}</div>
      <Progress
        className={className}
        trailColor={trailColor}
        percent={40}
        strokeColor={strokeColor}
      />
    </ProgressBarLabelWrapper>
  );
};

export default ProgressBarLabel;
