import React from 'react';
import { Modal, Form, Button, Row, Col, Input, Upload, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import LabelInput from 'components/common/LabelInput';
import { s3FileUpload } from 'apis/storage';
import { DeleteOutlined } from '@ant-design/icons';
import config from 'config';
import { toast } from 'react-toastify';

const { TextArea } = Input;

interface AddressInfoProps {
  ModalVisible: any;
  onclickModel: any;
  handleOk: any;
  handleCancel: any;
  EditData: any;
  handleDelete: any;
}

const DocModal = ({ModalVisible,handleOk, handleCancel, EditData, handleDelete}: AddressInfoProps): JSX.Element => {
    const [form] = Form.useForm();

    const [Document, setDocument] = React.useState<any>({});
    const [fileData, ] = React.useState<any>([]);

    const removeFile = () => {
      Document.url= "";
      Document.fileName = "";
      setDocument({
        ...Document,
      });
    }

    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
            message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
            message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    const Subumit = () => {
      form.submit();
      if(Document.name){
        handleOk(Document, EditData);
        setDocument({});
      }
    }
    React.useEffect(()=>{
      setDocument(EditData);
    },[EditData])
  return (
    <>
      <Modal title={'Add New Documents'}
        okText="Save"
        visible={ModalVisible}
        onOk={handleOk}
        closable={true}
        onCancel={()=>{
          setDocument({});
          handleCancel();
        }}
        destroyOnClose={true}
        footer={Object.keys(EditData).length> 0 ?[
          <Button key="submit" type="primary" onClick={()=> Subumit()}>
            Add Document
          </Button>,
          <Button key="back" onClick={()=>{
            setDocument({});
            handleDelete("documents");
          }}>
            DELETE
          </Button>
        ]:
        <Button key="submit" type="primary" onClick={()=> Subumit()}>
            Add Document
        </Button>
      }
      >
        <Form form={form} layout={"vertical"} preserve={false}>
          <Row justify="space-between">
            <Col lg={24}>
                <LabelInput
                  label="DOCUMENT NAME"
                  name="name"
                  rules={[
                    {
                      required: true,
                      message: 'Document Name is required!',
                    },
                  ]}
                  inputElement={
                    <Input
                      onChange={(e) => {
                        setDocument({ ...Document, name: e.target.value });
                        form.setFieldsValue({ name: e.target.value });
                      }}
                      defaultValue={EditData?.name}
                    />
                  }
                />
            </Col>
          </Row>
          <Row justify="space-between">
            <Col lg={24}>
                <LabelInput
                  label="Description"
                  name="description"
                  rules={[
                    {
                      required: true,
                      message: 'Description is required!',
                    },
                  ]}
                  inputElement={
                    <TextArea
                      onChange={(e) => {
                        setDocument({ ...Document, description: e.target.value });
                        form.setFieldsValue({ description: e.target.value });
                      }}
                      defaultValue={EditData?.description}
                    />
                  }
                />
            </Col>
          </Row>
          <Row justify="space-between" style={{marginTop: "3%"}}>
            <Col lg={24}>
                <Form.Item label="ATTACHMENT">
                {Document?.url ? (
                  <div>
                    <a title={Document?.fileName} href={`${config.apiBaseUrl}files/${Document?.url}`}>{Document?.fileName}</a> <DeleteOutlined onClick={()=> removeFile()} style={{color:"red"}}/>
                  </div>
                ) : 
                <Upload 
                  defaultFileList={fileData}
                  maxCount={1}
                  disabled={fileData?.url?.length > 0 ? true : false}
                  customRequest={async ({
                    file,
                    fileData,
                    onProgress,
                    onSuccess,
                  }: any) => {
                    let fileupload = await s3FileUpload(file);
                    if(fileupload?.status === 200){
                      setDocument({
                        ...Document,
                        url: fileupload?.data?.fileId,
                        fileName: file?.name
                      });
                      onProgress({ percent: 100 });
                      onSuccess(()=> console.log("onSuccess"));
                    }
                  }}
                  accept="image/png, image/jpeg, .doc, .docx, application/pdf"
                  beforeUpload={(file:any)=>{
                    let type = file.type;
                    let types = ['image/png', 'image/jpeg', 'doc', 'docx', 'application/pdf'];
                    if(!type){
                      let splitName = file.name.split('.');
                      type=splitName?.[splitName?.length-1];
                    }
                    const allowedTypes = types.includes(type)
                      if (!allowedTypes) {
                        toast.error(`${file.name} is not a ${types?.join(',')} type`);
                      }
                      return allowedTypes || Upload.LIST_IGNORE;
                  }}
                >
                    <Button icon={<PlusOutlined />}></Button>
                </Upload>}
                    {/* <Upload {...props}>
                        <Button icon={<PlusOutlined />}></Button>
                    </Upload> */}
                </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default DocModal;
