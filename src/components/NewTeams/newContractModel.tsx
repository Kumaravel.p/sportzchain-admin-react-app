import React from 'react';
import { Modal, Form, Button, Row, Col, Input, Upload, DatePicker } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import moment from 'moment';
import { s3FileUpload } from 'apis/storage';
import { DeleteOutlined } from '@ant-design/icons';
import config from 'config';
import { toast } from 'react-toastify';

interface contactInfoProps {
  ModalVisible: any;
  onclickModel: any;
  handleOk: any;
  handleCancel: any;
  teamStatuses: any;
  EditData: any;
  handleDelete: any;
}

const NewContactModal = ({
  ModalVisible,
  handleOk,
  handleCancel,
  teamStatuses,
  EditData,
  handleDelete,
}: contactInfoProps): JSX.Element => {
  const [form] = Form.useForm();
  const [Contract, setContract] = React.useState<any>({});
  const [fileData, setfileData] = React.useState<any>([]);

  React.useEffect(() => {
    setContract(EditData);
  }, [EditData]);

  const saveContact = () => {
    form.submit();
    if (Contract.status && Contract.documentName) {
      handleOk(Contract, EditData);
      setContract({});
    }
  };
  const removeFile = () => {
    Contract.contractUrl = '';
    Contract.fileName = '';
    setContract({
      ...Contract,
    });
  };
  console.log(EditData, '_________');
  return (
    <>
      <Modal
        title={'New contract'}
        okText="Save"
        visible={ModalVisible}
        onOk={handleOk}
        closable={true}
        onCancel={() => {
          setContract({});
          handleCancel();
        }}
        destroyOnClose={true}
        footer={
          Object.keys(EditData).length > 0 ? (
            [
              <Button key="submit" type="primary" onClick={() => saveContact()}>
                ADD CONTRACT
              </Button>,
              <Button
                key="back"
                onClick={() => {
                  setContract({});
                  handleDelete('contracts');
                }}
              >
                DELETE
              </Button>,
            ]
          ) : (
            <Button key="submit" type="primary" onClick={() => saveContact()}>
              ADD CONTRACT
            </Button>
          )
        }
      >
        <Form form={form} layout={'vertical'} preserve={false}>
          <Row justify="space-between">
            <Col lg={24}>
              <LabelInput
                label="DOCUMENT NAME"
                name="documentName"
                rules={[
                  {
                    required: true,
                    message: 'Document Name is required!',
                  },
                ]}
                inputElement={
                  <Input
                    placeholder={'Document Name…'}
                    onChange={(e: any) => {
                      
                      setContract({
                        ...Contract,
                        documentName: e.target.value,
                      });
                      form.setFieldsValue({
                        documentName: e.target.value,
                      });
                    }}
                    defaultValue={EditData?.documentName}
                  />
                }
              />
            </Col>
            <Col lg={11} style={{ marginTop: '2%', padding: 0 }}>
              <LabelInput
                label="STATUS"
                name="status"
                rules={[{ required: true, message: 'status is required!' }]}
                // initialValue={teamDetails?.categoryId}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      setContract({ ...Contract, status: e });
                      form.setFieldsValue({ status: e });
                    }}
                    defaultValue={EditData?.status}
                    options={teamStatuses?.teamStatus ?? []}
                    toFilter="status"
                    placeholder="Status"
                    searchable={true}
                  />
                }
              />
            </Col>
            <Col lg={11} style={{ marginTop: '2%' }}>
                <LabelInput
                  label="Contract Duration Range"
                  name="contractstartAtendAt"
                  // initialValue={contractData.name}
                  rules={[{ required: true, message: 'Start - End date' }]}
                  inputElement={
                    <DatePicker.RangePicker
                      format={'DD/MM/YYYY'}
                      defaultValue={EditData?.contractstartAtendAt}
                      onChange={(e: any) => {
                        setContract({ ...Contract, contractstartAtendAt: e });
                        form.setFieldsValue({ contractstartAtendAt: e });
                      }}
                    />
                  }
                />
            </Col>

            {/* <Col lg={11} style={{marginTop: "2%"}}>
                <LabelInput
                  label="SPORTZCHAIN TEAM"
                  name="categoryId"
                  rules={[
                    { required: true, message: 'status is required!' },
                  ]}
                  // initialValue={teamDetails?.categoryId}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        // setTeamDetails({ ...teamDetails, categoryId: e });
                        // form.setFieldsValue({ categoryId: e });
                      }}
                      // defaultValue={teamDetails?.categoryId}
                      options={teamStatuses?.teamStatus ?? []}
                      toFilter="status"
                      placeholder="status"
                    />
                  }
                />
            </Col> */}
            {/* <Col lg={11} style={{marginTop: "2%"}}>
                <LabelInput
                  label="TEAM"
                  name="categoryId"
                  rules={[
                    { required: true, message: 'status is required!' },
                  ]}
                  // initialValue={teamDetails?.categoryId}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        // setTeamDetails({ ...teamDetails, categoryId: e });
                        // form.setFieldsValue({ categoryId: e });
                      }}
                      // defaultValue={teamDetails?.categoryId}
                      options={teamStatuses?.teamStatus ?? []}
                      toFilter="status"
                      placeholder="status"
                    />
                  }
                />
            </Col> */}
            <Col lg={11} style={{ marginTop: '2%' }}>
              <Form.Item label="ATTACHMENT">
                {Contract?.contractUrl ? (
                  <div>
                    <a title={Contract?.fileName} href={`${config.apiBaseUrl}files/${Contract?.contractUrl}`}>
                      {Contract?.fileName}
                    </a>{' '}
                    <DeleteOutlined
                      onClick={() => removeFile()}
                      style={{ color: 'red' }}
                    />
                  </div>
                ) : (
                  <Upload
                    defaultFileList={fileData}
                    maxCount={1}
                    accept="image/png, image/jpeg, .doc, .docx, application/pdf"
                    disabled={fileData?.contractUrl?.length > 0 ? true : false}
                    customRequest={async ({
                      file,
                      fileData,
                      onProgress,
                      onSuccess,
                    }: any) => {
                      let fileupload = await s3FileUpload(file);
                      if (fileupload?.status === 200) {
                        setContract({
                          ...Contract,
                          contractUrl: fileupload?.data?.fileId,
                          fileName: file?.name,
                        });
                        onProgress({ percent: 100 });
                        onSuccess(() => console.log('onSuccess'));
                      }
                    }}
                    beforeUpload={(file:any)=>{
                      let type = file.type;
                      let types = ['image/png', 'image/jpeg', 'doc', 'docx', 'application/pdf'];
                      if(!type){
                        let splitName = file.name.split('.');
                        type=splitName?.[splitName?.length-1];
                      }
                      const allowedTypes = types.includes(type)
                        if (!allowedTypes) {
                          toast.error(`${file.name} is not a ${types?.join(',')} type`);
                        }
                        return allowedTypes || Upload.LIST_IGNORE;
                    }}
                  >
                    <Button icon={<UploadOutlined />}>Click to Upload</Button>
                  </Upload>
                )}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default NewContactModal;
