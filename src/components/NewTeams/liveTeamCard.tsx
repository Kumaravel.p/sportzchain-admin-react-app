import React, { useMemo } from 'react';
import { Avatar, Button, Card, Divider, Space, Typography } from 'antd';
import styled from 'styled-components/macro';
import { ReactComponent as SportIcon } from 'assets/icons/Sport.svg';
import { Sample } from 'utils/countryCodeList';
import { TeamCardStripe } from 'assets/images/team-card-stripes';

interface LiverTeamCardProps {
    profileImgUrl?: string;
    teamName?: string;
    description?: string;
    catogory?: string;
    countryName?: string | null;
    primaryColor?: string | null;
}

const Wrapper = styled.div`
position:relative;
overflow: hidden;
.stripes{
    position:absolute;
    right:0;
    top:0;
    opacity:0;
}
&:hover{
    & .stripes{
        opacity:1;
    }
}
& .team-card{
    background-color: #262626;
    border-radius: 8px;
    margin-top: 0 !important;
    & .nameImg{
        margin-bottom: 24px;
    }
    & .teamName{
        color:#fafafa;
        z-index:1;
        flex:1;
        margin-bottom: 0;
    }
    & .description{
        color: #fafafa;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        overflow: hidden;
        margin-bottom: 16px;
        font-size: 16px;
        z-index:1;
    }
    & .country-category{
        z-index:1;
        width:100%;
        background: #171717;
        border-radius: 4px;
        padding: 16px;
        /* margin-top: 8px; */
        & .ant-space-item{
            flex:1;
        }
        & .ant-divider{
            border-left:1px solid #fff;
            height: 2.5em;
        }
        & .flex-column{
            display: flex;
            flex-direction: column;
            align-items: center;
            & h5{
                color:#fafafa;
                margin-bottom: 0;
            }
        }
    }
    & .view-detail-btn{
        z-index:1;
        background: #fb923c;
        border-color: #fb923c;
        border-radius: 2px;
        color: #171717;
        padding:8px;
        height:100%;
        width:100%;
        font-weight: bold;
        margin-top: 24px;
    }
}
`;

export const LiveTeamCard = (props: LiverTeamCardProps): JSX.Element => {

    const {
        profileImgUrl = "",
        teamName = "-",
        description = "-",
        catogory = "-",
        countryName = "",
        primaryColor = "whitesmoke",
    } = props;

    const countryFlag = useMemo(() => {
        if (!countryName) return null
        return Sample?.find(country => country?.name?.toLowerCase() === countryName?.toLowerCase())
    }, [countryName])

    const modifyDesc = useMemo(() => {
        if (description === "<p></p>") return '-'
        return description
    }, [description])


    return (
        <Wrapper>
            <Card className='team-card'>
                <Space size={16} align="center" className='nameImg'>
                    <Avatar src={profileImgUrl} size={80}>{teamName?.charAt(0)}</Avatar>
                    <Typography.Title level={4} className="teamName">{teamName}</Typography.Title>
                </Space>
                <div className='description' dangerouslySetInnerHTML={{ __html: modifyDesc }} />
                <Space
                    split={<Divider type="vertical" />}
                    className="country-category"
                >
                    <div className='flex-column' style={{ gap: 8 }}>
                        <SportIcon />
                        <Typography.Title level={5}>{catogory}</Typography.Title>
                    </div>
                    <div className='flex-column'>
                        <Avatar src={countryFlag?.image}>{countryFlag?.emoji}</Avatar>
                        <Typography.Title level={5}>{countryName ? countryName : '-'}</Typography.Title>
                    </div>
                </Space>
                <div>
                    <Button className='view-detail-btn'>View Detail ↗</Button>
                </div>
            </Card>
            <div className="stripes">
                <TeamCardStripe stripesColor={primaryColor} />
            </div>
        </Wrapper>
    )
}