import { Table } from 'antd';
import { EditOutlined } from '@ant-design/icons';
interface contactInfoProps {
  JsonData: any;
  openEdit: any;
}

function onChange(pagination: any, filters: any, sorter: any, extra: any) {
  console.log('params', pagination, filters, sorter, extra);
}
const NewNotes = ({JsonData, openEdit}: contactInfoProps): JSX.Element => {
  const columns = [
    {
      title: '#',
      dataIndex: 'key',
    },
    {
      title: 'NOTE',
      dataIndex: 'note',
    },
    {
      title: 'Added by',
      dataIndex: 'addedby',
    },
    {
      title: 'Added date',
      dataIndex: 'date',
    },
    {
      title: '',
      dataIndex: '',
      key: 'x',
      render: (_:any, record: { key: React.Key }) =>
          data?.length >= 1 ? (
            <EditOutlined onClick={()=> editClick(_,record.key)} style={{color:'#4DA1FF'}}/>
        ) : null,
    },
  ];
  
  const editClick = (obj: any, record: any) => {
    openEdit(obj, record, "documents");
  }

  const data = JsonData?.map((val: any,i: number)=>{
    return({
        key: i + 1,
        note: val?.name,
        addedby: val?.addedby,
        date: val?.addedDate,
      })
  });
  return(
    <Table columns={columns} dataSource={data} onChange={onChange} size="middle" scroll={{ x: 500 }}/>
  )
}
export default NewNotes