import { Table } from 'antd';
import { EditOutlined } from '@ant-design/icons';


interface contactInfoProps {
  JsonData: any;
  openEdit: any;
}

function onChange(pagination: any, filters: any, sorter: any, extra: any) {
  console.log('params', pagination, filters, sorter, extra);
}
const NewContact = ({JsonData, openEdit}: contactInfoProps): JSX.Element => {
  const data = JsonData?.map((val: any,i: number)=>{
    return({
        key: i + 1,
        name: val?.documentName,
        status: val?.status,
        teamsign: val?.contractsName,
        start: val?.startDate,
        expiry: val?.endDate,
      })
  });
  const editClick = (obj: any, record: any) => {
    openEdit(obj, record, "contracts")
  }
  const columns = [
    {
      title: 'Document Name',
      dataIndex: 'name',
    },
    {
      title: 'Status',
      dataIndex: 'status',
    },
    {
      title: 'Team sign',
      dataIndex: 'teamsign',
    },
    {
      title: 'start date',
      dataIndex: 'start',
    },
    {
      title: 'expiry daten',
      dataIndex: 'expiry',
    },
    {
      title: '',
      dataIndex: '',
      key: 'x',
      render: (_:any, record: { key: React.Key }) =>
        data?.length >= 1 ? (
          <EditOutlined onClick={()=> editClick(_,record.key)} style={{color:'#4DA1FF'}}/>
      ) : null,
    },
  ];
  return(
    <Table columns={columns} dataSource={data} onChange={onChange} size="middle" scroll={{ x: 500 }}/>
  )
}
export default NewContact;
// export default () => <Table columns={columns} dataSource={data} onChange={onChange} size="middle" scroll={{ x: 500 }}/>;