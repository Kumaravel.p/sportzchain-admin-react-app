import React from 'react';
import { Modal, Form, Button, Row, Col, Input } from 'antd';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import SelectWithInput from 'components/common/selectWithInput';
import {
  useGetTeamContactRolesQuery,
} from 'generated/graphql';
import { countryCode } from 'utils/countryCodeList'

interface AddressInfoProps {
  ModalVisible: any;
  onclickModel: any;
  handleOk: any;
  handleCancel: any;
  EditData: any;
  handleDelete: any;
}

const divisionOptions = [
  { label: 'Polls' },
  { label: 'contests' },
  { label: 'newsfeed' },
  { label: 'Operations' },
  { label: 'Sales' }
]


const TeamsModal = ({ModalVisible,handleOk, handleCancel, EditData, handleDelete}: AddressInfoProps): JSX.Element => {
  const [form] = Form.useForm();
  const [teams, setTeams] = React.useState<any>({});
  const { data: teamContactRoles } = useGetTeamContactRolesQuery();

  const submitData = () => {
    form.submit();
    
    if(teams?.teamsName && teams?.teamsRole && teams?.email && teams?.teamsPrimaryContact && teams?.teamsDivision?.length > 0){
      handleOk(teams, EditData);
      setTeams({});
    }
  }


  React.useEffect(()=>{
    setTeams(EditData);
    form.setFieldsValue(EditData)
  },[EditData]);
  return (
    <>
      <Modal title={'Add Team Member'}
        okText="Save"
        visible={ModalVisible}
        onOk={handleOk}
        closable={true}
        onCancel={()=>{
          setTeams({});
          handleCancel();
        }}
        destroyOnClose={true}
        footer={Object.keys(EditData).length > 0 ? [
          <Button key="submit" type="primary" onClick={()=> submitData()}>
            Add and Invite Person
          </Button>,
          <Button key="back" onClick={()=>{
            setTeams({});
            handleDelete("teamMembers");
          }}>
            DELETE
          </Button>
          
        ]: 
        <Button key="submit" type="primary" onClick={()=> submitData()}>
          Add and Invite Person
        </Button>
        }
      >
        <Form form={form} layout={"vertical"} preserve={false}>
          <Row justify="space-between">
            <Col lg={24}>
                <LabelInput
                  label="Name"
                  name="teamsName"
                  rules={[
                    {
                      required: true,
                      message: 'Name is required!',
                    },
                  ]}
                  inputElement={
                    <Input  
                      onChange={(e) => {
                        setTeams({ ...teams, teamsName: e.target.value });
                        form.setFieldsValue({ teamsName: e.target.value });
                      }}
                      placeholder="NAME"
                      defaultValue={teams?.teamsName}
                    />
                  }
                />
            </Col>
            <Col lg={24}>
                <LabelInput
                  label="Division"
                  name="teamsDivision"
                  rules={[
                    {
                      required: true,
                      message: 'Division is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                        onChange={(e: any) => {
                        setTeams({ ...teams, teamsDivision: e });
                        form.setFieldsValue({ teamsDivision: e });
                      }}
                      placeholder="Division"
                      toFilter="label"
                      defaultValue={teams?.teamsDivision ?? []}
                      options={divisionOptions}
                      mode={"multiple"}
                    />
                  }
                />
            </Col>
            <Col lg={24}>
                <LabelInput
                  label="Role"
                  name="teamsRole"
                  rules={[
                    {
                      required: true,
                      message: 'Role is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                        onChange={(e: any) => {
                        setTeams({ ...teams, teamsRole: e });
                        form.setFieldsValue({ teamsRole: e });
                      }}
                      placeholder="Role"
                      toFilter="role"
                      defaultValue={teams?.teamsRole}
                      options={teamContactRoles?.teamContactRole}
                    />
                  }
                />
            </Col>
            {/* <Col lg={24}>
                <LabelInput
                  label="Primary Contact Number"
                  name="teamsPrimaryContact"
                  rules={[
                    {
                      required: true,
                      message: 'Primary Contact is required!',
                    },
                  ]}
                  inputElement={
                    <Input 
                      onChange={(e) => {
                        setTeams({ ...teams, teamsPrimaryContact: e.target.value });
                        form.setFieldsValue({ teamsPrimaryContact: e.target.value });
                      }}
                      placeholder="Enter Primary Contact"
                      defaultValue={teams?.teamsPrimaryContact}
                    />
                  }
                />
            </Col> */}
            {/* <Col lg={24}>
                <LabelInput
                  label="Secondary Contact Number"
                  name="teamsSecondaryContact"
                  rules={[
                    {
                      required: false,
                      message: 'Secondary Contact is required!',
                    },
                  ]}
                  inputElement={
                    <Input 
                      onChange={(e) => {
                        setTeams({ ...teams, teamsSecondaryContact: e.target.value });
                        form.setFieldsValue({ teamsSecondaryContact: e.target.value });
                      }}
                      placeholder="Secondary Contact"
                      defaultValue={teams?.teamsSecondaryContact}
                    />
                  }
                />
            </Col> */}
            <Col lg={24}>
              <LabelInput
                label="Primary Contact Number"
                name="teamsPrimaryContact"
                rules={[
                  () => ({
                    validator(_, value) {
                      if (!(!!value?.countryCode)) {
                        return Promise.reject(new Error('Country code is required!'));
                      }
                      else if (!(!!value?.mobileNumber)) {
                        return Promise.reject(new Error('Mobile Number is required!'));
                      }
                      return Promise.resolve();
                    },
                  }),
                ]}
                inputElement={
                  <SelectWithInput
                    options={countryCode}
                    optionLabel="dial_code"
                    optionValue="dial_code"
                    value={teams?.teamsPrimaryContact}
                    placeholder="Enter Primary Contact"
                    onChange={(e:any) => {
                      setTeams({ ...teams, teamsPrimaryContact: e });
                      form.setFieldsValue({ teamsPrimaryContact: e });
                    }}
                  />
                }
              />
          </Col>
            <Col lg={24}>
              <LabelInput
                label="Secondary Contact Number"
                name="teamsSecondaryContact"
                rules={[
                  {
                    required: false,
                    message: 'Secondary Contact is required!',
                  },
                ]}
                inputElement={
                  <SelectWithInput
                    options={countryCode}
                    optionLabel="dial_code"
                    optionValue="dial_code"
                    value={teams?.teamsSecondaryContact}
                    placeholder="Enter Primary Contact"
                    onChange={(e:any) => {
                      setTeams({ ...teams, teamsSecondaryContact: e });
                      form.setFieldsValue({ teamsSecondaryContact: e });
                    }}
                  />
                }
              />
          </Col>
            <Col lg={24}>
                <LabelInput
                  label="Email"
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: 'Email Is Required!',
                      type: "email"
                    },
                  ]}
                  inputElement={
                    <Input 
                      onChange={(e) => {
                        setTeams({ ...teams, email: e.target.value });
                        form.setFieldsValue({ email: e.target.value });
                      }}
                      placeholder="EMAIL Id"
                      defaultValue={teams?.email}
                    />
                  }
                />
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default TeamsModal;
