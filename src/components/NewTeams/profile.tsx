import {
  Button,
  Card,
  Col,
  Input,
  Row,
  Upload,
  Form,
  Spin
} from 'antd';
import { LoadingOutlined  } from '@ant-design/icons';
import * as React from 'react';
import LabelInput from 'components/common/LabelInput';
import TitleCta from 'components/common/TitleCta';
import TextEditorInput from 'components/common/TextEditorInput';
import {TeamCardStripe}  from 'assets/images/team-card-stripes.jsx';
  import { toast } from 'react-toastify';
import { s3FileUpload } from "../../apis/storage";
import { DeleteOutlined } from '@ant-design/icons';
import config from 'config';
import styled from 'styled-components/macro';
import { LiveTeamCard } from './liveTeamCard';

const HoverDiv = styled('div')`
position:relative;
cursor:pointer;
& img{
  width: 102px;
  height: 102px;
  object-fit:cover;
}
& > div{
  display:none;
  position:absolute;
  bottom:0;
  left:0;
  right:0;
}
& button{
  color:#fff;
  font-weight:bold;
  width:100%;
  &:hover{
    color:#fff;
  }
}
&:hover{
  & img{
    filter:brightness(0.6);
  }
  & > div{
    display:block;
  }
}
`
const ResolutionHint = styled.p`
    color: #bfbfbf;
    /* font-weight: 600; */
    margin-top: 0.5rem;
    margin-bottom: 0;
`;

const Header = styled.div`
    color: #051f24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
`;

interface ProfileProps {
  id: string;
  profileDataFun: any;
  teamDetailsState: any;
  setTeamDetails: any;
  teamDetails: any;
}

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const Profile = ({ profileDataFun, teamDetailsState, setTeamDetails, teamDetails }: ProfileProps): JSX.Element => {
  const [form] = Form.useForm();

  const [loading,setLoading] = React.useState<any>(null);

  const imageUploadRemove = (fileInput:any) => {
    profileDataFun("", fileInput)
  }

  const removeBannerLogo = (index:number) =>{
    profileDataFun(teamDetails?.teamBanner?.filter((_:string,i:number)=>i !== index),"teamBanner")
  }

  const uploadImageType = async (key:string,file:any) => {
    setLoading(key)
    let fileupload = await s3FileUpload(file);
    if (fileupload.status === 200) {
      toast.success("File uploaded successfully");
      if (key === "teamBanner") {
        profileDataFun([...teamDetails?.teamBanner ?? [], fileupload.data.fileId], key)
      }
      else {
        profileDataFun(fileupload.data.fileId, key);
      }
    }
    else {
      toast.error("File uploaded failed");
    }
    setLoading(null)
  }

  const beforeUpload = (file:any) =>{
    const allowedTypes = file.type === 'image/png' || file.type === 'image/jpeg';
    if (!allowedTypes) {
      toast.error(`${file.name} is not a png or jpeg file`);
    }
    return allowedTypes || Upload.LIST_IGNORE;
  }

  return (
    <Row style={{margin: "0px 30px", width: "100%"}}>
      <Card style={{width: "100%"}}>
        <div style={{width: "100%", display:"flex", justifyContent:"space-between"}}>
          <Header>Team banner</Header>
          {/* <p style={{fontWeight:"bold"}}>Team banner</p> */}
          {/* <Button type="link" onClick={()=> {}}>+ Add New banner</Button> */}
        </div>
        <div style={{width:"100%", marginBottom: "6%"}}>
          <>
            <div style={{ display: "flex", alignItems: "center", gap: '16px' }}>
              {
                teamDetails?.teamBanner?.length > 0 && teamDetails.teamBanner?.map((_: string, index: number) => (
                  <HoverDiv>
                    <img src={`${config.apiBaseUrl}files/${_}`} />
                    <div>
                      <Button type='text' onClick={() => removeBannerLogo(index)}>Remove <DeleteOutlined /></Button>
                    </div>
                  </HoverDiv>
                ))
              }
              <Upload
                listType="picture-card"
                showUploadList={false}
                accept="image/png, image/jpeg"
                style={{ margin: 0 }}
                customRequest={async ({ file }: any) => uploadImageType('teamBanner', file)}
                beforeUpload={(file: any) => beforeUpload(file)}
              >
                {loading === "teamBanner" ? <Spin indicator={antIcon} /> : '+'}
              </Upload>
            </div>
            <ResolutionHint>Recommended resolution :- 1136 x 200 px, Supported formats :- PNG, JPEG</ResolutionHint>
          </>
        </div>
        <Form
          form={form}
          initialValues={{
            layout: "horizontal",
          }}
          layout={"vertical"}
        >
          <div style={{width:"100%"}}>
            <Row gutter={[0, 32]}>
              <Col span={24}>
                <div style={{display:"flex", justifyContent:"space-between"}}>
                  <Header>Team Logo</Header>
                  {/* <h3 style={{fontWeight:"bold"}}>Team Logo</h3> */}
                  {/* <Button type="link" onClick={()=> {}}>+ Add New</Button> */}
                </div>
                {teamDetails?.logo ?
                  <div>
                    <img src={`${config.apiBaseUrl}files/${teamDetails.logo}`} style={{ width: "auto", height: "9rem" }} />
                    <div>
                      <Button type="text" style={{ marginTop: "1%", padding: 0 }} onClick={() => imageUploadRemove("logo")}>Remove <DeleteOutlined /></Button>
                    </div>
                  </div>
                  :
                  <>
                    <Upload
                      listType="picture-card"
                      showUploadList={false}
                      customRequest={async ({ file }: any) => uploadImageType('logo', file)}
                      beforeUpload={(file: any) => beforeUpload(file)}
                      // className={"addBannerUpload"}
                      accept="image/png, image/jpeg"
                    >
                      {loading === "logo" ? <Spin indicator={antIcon} /> : '+'}
                    </Upload>
                  </>
                }
                <ResolutionHint>Recommended resolution :- 500x500 px, Supported formats :- PNG, JPEG</ResolutionHint>
              </Col>
              <Col span={24}>
                <div>
                  <Header>Social media links</Header>
                </div>
                <Row gutter={[16,16]}>
                  <Col span={12}>
                    <LabelInput
                      label="Web Site"
                      name="Website"
                      rules={[
                        {
                          // required: true,
                          message: 'Web Site URL Format',
                          type:"url"
                        },
                      ]}
                      inputElement={
                        <Input  
                          // onChange={(e) => {
                          //   setProfileData({ ...profileData, webSite: e.target.value });
                          // }}
                          autoComplete={"off"}
                          onChange={(e) => profileDataFun(e.target.value, "websiteUrl")}
                        />
                      }
                    />
                  </Col>
                  <Col span={12}>
                    <LabelInput
                      label="Facebook"
                      name="facebook"
                      rules={[
                        {
                          // required: true,
                          message: 'Facebook  URL Format',
                          type:"url"
                        },
                      ]}
                      inputElement={
                        <Input  
                          // onChange={(e) => {
                          //   setProfileData({ ...profileData, webSite: e.target.value });
                          // }}
                          autoComplete={"off"}
                          onChange={(e) => profileDataFun(e.target.value, "facebookUrl")}
                        />
                      }
                    />
                  </Col>
                </Row>
                <Row gutter={[16,16]}>
                  <Col span={12}>
                    <LabelInput
                      label="Twitter"
                      name="twitter"
                      rules={[
                        {
                          // required: true,
                          message: 'Twitter  URL Format',
                          type:"url"
                        },
                      ]}
                      inputElement={
                        <Input  
                          // onChange={(e) => {
                          //   setProfileData({ ...profileData, webSite: e.target.value });
                          // }}
                          autoComplete={"off"}
                          onChange={(e) => profileDataFun(e.target.value, "twitterUrl")}
                        />
                      }
                    />
                  </Col>
                  <Col span={12}>
                    <LabelInput
                      label="Instagram"
                      name="instagram"
                      rules={[
                        {
                          // required: true,
                          message: 'Instagram URL Format',
                          type:"url"
                        },
                      ]}
                      inputElement={
                        <Input  
                          // onChange={(e) => {
                          //   setProfileData({ ...profileData, webSite: e.target.value });
                          // }}
                          autoComplete={"off"}
                          onChange={(e) => profileDataFun(e.target.value, "instagramUrl")}
                        />
                      }
                    />
                  </Col>
                </Row>
                <Row gutter={[16,16]}>
                  <Col span={12}>
                    <LabelInput
                      label="Telegram"
                      name="telegram"
                      rules={[
                        {
                          // required: true,
                          message: 'Telegram URL Format',
                          type:"url"
                        },
                      ]}
                      inputElement={
                        <Input  
                          // onChange={(e) => {
                          //   setProfileData({ ...profileData, webSite: e.target.value });
                          // }}
                          autoComplete={"off"}
                          onChange={(e) => profileDataFun(e.target.value, "telegramUrl")}
                        />
                      }
                    />
                  </Col>
                  <Col span={12}>
          
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              {/* <TitleCta title="About"/> */}
              <Header>About</Header>
              {/* <TextEditorInput value={""} setValue={(val:any)=> console.log(val)} /> */}
              <TextEditorInput value={""} setValue={(val:any) => profileDataFun(val, "description")} />
            </Row>
            <Row style={{marginTop: "6%"}}>
              <Row gutter={[24,24]} style={{width:'100%'}}>
                <Col xs={24} sm={6}>
                  <LabelInput
                    label="Primary colour"
                    name="primaryColor"
                    initialValue={""}
                    inputElement={
                      <Input
                        defaultValue={""}
                        onChange={(e) => profileDataFun(e.target.value, "primaryColor")}
                        type="color"
                      />
                    }
                  />
                  <LabelInput
                    label="Secondary colour"
                    name="secondaryColor"
                    initialValue={""}
                    inputElement={
                      <Input
                        defaultValue={""}
                        onChange={(e) =>profileDataFun(e.target.value, "secondaryColor")}
                        type="color"
                      />
                    }
                  />
                </Col>
                <Col xs={24} sm={8}>
                  <LiveTeamCard
                    profileImgUrl={`${config.apiBaseUrl}files/${teamDetails?.logo}`}
                    teamName={teamDetails?.teamName}
                    description={teamDetails?.description}
                    catogory={teamDetails?.categoryId}
                    countryName={teamDetails?.countryTitle}
                    primaryColor={teamDetails?.primaryColor}
                  />
                </Col>
              </Row>
            </Row>
          </div>
        </Form>
      </Card>
    </Row>
  )
}
export default Profile;