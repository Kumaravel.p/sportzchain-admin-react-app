import { Table } from 'antd';

import { EditOutlined } from '@ant-design/icons';

interface contactInfoProps {
  JsonData: any
  openEdit: any
}
function onChange(pagination: any, filters: any, sorter: any, extra: any) {
  console.log('params', pagination, filters, sorter, extra);
}
const NewTeams = ({JsonData, openEdit}: contactInfoProps): JSX.Element => {
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Role',
      dataIndex: 'role',
    },
    {
      title: 'Mobile number',
      dataIndex: 'mobile',
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: '',
      dataIndex: '',
      key: 'x',
      render: (_:any, record: { key: React.Key }) =>
          data?.length >= 1 ? (
            <EditOutlined onClick={()=> editClick(_,record.key)} style={{color:'#4DA1FF'}}/>
        ) : null,
    },
  ];

  const data = JsonData?.map((val: any,i: number)=>{
    return({
        key: i + 1,
        name: val?.teamsName,
        role: val?.teamsRole,
        mobile: `${val?.teamsPrimaryContact?.countryCode}${val?.teamsPrimaryContact?.mobileNumber}`,
        email: val?.email
      })
  });

  const editClick = (obj: any, record: any) => {
    openEdit(obj, record, "teamMembers");
  }

  return (
    <Table columns={columns} dataSource={data} onChange={onChange} size="middle" scroll={{ x: 500 }}/>
  )
}


export default NewTeams;