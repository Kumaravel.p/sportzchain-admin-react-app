import { Table } from 'antd';

import { EditOutlined,EyeOutlined } from '@ant-design/icons';
import moment from 'moment';

interface contactInfoProps {
  JsonData: any;
  openEdit: any;
  readOnly?:boolean;
}

function onChange(pagination: any, filters: any, sorter: any, extra: any) {
  console.log('params', pagination, filters, sorter, extra);
}
const NewNotes = ({JsonData, openEdit, readOnly = false}: contactInfoProps): JSX.Element => {


  const columns = [
    {
      title: '#',
      dataIndex: 'key',
    },
    {
      title: 'NOTE',
      dataIndex: 'name',
    },
    {
      title: 'Added by',
      dataIndex: 'addedby',
    },
    {
      title: 'Added date',
      dataIndex: 'addedDate',
    },
    {
      title: '',
      dataIndex: '',
      key: 'x',
      render: (_:any, record: { key: React.Key }) =>
        data?.length >= 1 ? (
          <>
          {
            (readOnly || _?.id) ? (
              <EyeOutlined onClick={()=> onIconClick(_, record.key)} style={{color:'#4DA1FF'}}/>
            ) : (
              <EditOutlined onClick={()=> onIconClick(_, record.key)} style={{color:'#4DA1FF'}}/>
            )
          }
          </>
      ) : null,
    },
  ];

  const onIconClick = (obj: any, record: any) => {
    
    openEdit(obj, record, "notes")
  }

  const data = JsonData?.map((val: any,i: number)=>{
    return({
        key: i + 1,
        name: val.name,
        addedby: val.addedby,
        addedDate: (val.addedDate instanceof moment) ? moment(val.addedDate).format('DD/MM/YYYY, hh:mm') : val.addedDate,
        id:val?.id
      })
  });
  return(
    <Table columns={columns} dataSource={data} onChange={onChange} size="middle" scroll={{ x: 500 }}/>
  )
}
export default NewNotes