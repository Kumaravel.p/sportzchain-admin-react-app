import React, {useEffect} from 'react';
import { Modal, Form, Button, Row, Col, Input, Spin } from 'antd';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
// import { useGetAllCountriesQuery } from 'generated/graphql';
import type { FormInstance } from 'antd/es/form';
import config from 'config';
import { toast } from 'react-toastify';
import styled from 'styled-components/macro';
import { LoadingOutlined  } from '@ant-design/icons';

const FormWrapper = styled('div')`
& .spin-wrapper{
  position: absolute;
  inset: 0;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  z-index: 1;
  justify-content: center;
  background: #00000047;
}
`;


interface AddressInfoProps {
  ModalVisible: any;
  onclickModel: any;
  handleOk: any;
  handleCancel: any;
  teamAddressTypes: any;
  data: any;
}

const AddressModal = ({ModalVisible,handleOk, handleCancel, teamAddressTypes,data={}}: AddressInfoProps): JSX.Element => {
  const [form] = Form.useForm();
  const [address, setaddress] = React.useState<any>({});
  const formRef = React.createRef<FormInstance>();
  const [loading, setLoading] = React.useState<boolean>(false);

  //
  const [countriesOptions,setCountriesOptions] = React.useState([]);
  const [stateOptions,setStateOptions] = React.useState([]);
  const [citiesOptions,setCitiesOptions] = React.useState([]);

  // const { data: countries } = useGetAllCountriesQuery();

  const SaveAddress = () => {
    form.submit();
    if(
      address?.addressType && 
      address?.country?.value &&
      (stateOptions?.length ? address?.addressState?.value : true) &&
      (citiesOptions?.length ? address?.city : true) &&
      address?.pincode &&
      address?.line1
      ){
      handleOk(address);
      setaddress({});
    }
    else{
      toast.error('please fill all the fields')
    }
  }

  React.useEffect(()=>{
    getAllCountriesOrStateOrCity('',null,'countries',setCountriesOptions)
  },[]);

  React.useEffect(()=>{
    if(data && Object.keys(data)?.length){
      setaddress(data)
      form.setFieldsValue({...data});
      getAllCountriesStateCity(data)
    }
  },[data])

  const getAllCountriesStateCity = async (_data:any) =>{
    setLoading(true);
    //get all the countries
    await fetch(`https://api.countrystatecity.in/v1/countries`, getRequestOptions())
      .then(response => response.json())
      .then(result => {
        if(result?.length){
          setCountriesOptions(result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})))
        }
      })
      .catch(error => console.log('error', error));
      
      //get all the states by country
    if(_data?.country?.value){
      await fetch(`https://api.countrystatecity.in/v1/countries/${_data?.country.value}/states`, getRequestOptions())
      .then(response => response.json())
      .then(result => {
        if(result?.length){
          setStateOptions(result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})))
        }
      })
      .catch(error => console.log('error', error));

      //get all the cities by country and state
      if(_data?.addressState?.value){
        await fetch(`https://api.countrystatecity.in/v1/countries/${_data?.country?.value}/states/${_data?.addressState?.value}/cities`, getRequestOptions())
        .then(response => response.json())
        .then(result => {
          if(result?.length){
            setCitiesOptions(result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})))
          }
        })
        .catch(error => console.log('error', error));
      }
      setLoading(false);
    }
  }


  const getRequestOptions = () =>{

    var headers = new Headers();
    headers.append("X-CSCAPI-KEY", config.countriesStateCitiesApiKey);
    
    let requestOptions:RequestInit = {
      method: 'GET',
      headers,
      redirect: "follow"
    };

    return requestOptions
  }

  const onChangeState = (key:string,value:any) =>{
    setaddress({ ...address, [key]: value });
    form.setFieldsValue({ [key]: value });
  }

  const getAllCountriesOrStateOrCity = async (key?:string,value?:any,url?:string,callback?:any,) =>{
    
    const setState = (prevData:any,key:string,value:any) =>{
      return{
        ...prevData,
        [key]: value,
        addressState:key === "addressState" ? value : key === "country" ? null : address.addressState,
        city:key === "city" ? value : (key === "addressState" || key === "country") ? null : address.city,
      }
    }

    if (key && value) {
      setaddress(setState(address,key,value));
      form.setFieldsValue(setState(form.getFieldsValue(),key,value))
    }

    if(url){
      setLoading(true);
      if(url === "countries"){
        url=""
      }
      await fetch(`https://api.countrystatecity.in/v1/countries${url}`, getRequestOptions())
      .then(response => response.json())
      .then(result => {
        if(callback){
          callback(result?.length ? result?.map(({name,iso2,id}:any)=>({name,value:iso2 ?? id})) : [])
        }
        setLoading(false);
      })
      .catch(error => {
        console.log('error', error)
        setLoading(false);
      });
    }
  }

  const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;


  return (
    <>
      <Modal 
        title={Object.keys(data)?.length ? 'Edit' : 'Add New Address'}
        okText={Object.keys(data)?.length ? "Update" : "Save"}
        visible={ModalVisible}
        onOk={handleOk}
        closable={true}
        onCancel={()=>{
          setaddress({});
          handleCancel();
        }}
        destroyOnClose={true}
        footer={[
          <Button key="submit" type="primary" onClick={()=> SaveAddress()}>
            {Object.keys(data)?.length ? 'UPDATE ADDRESS' : 'ADD ADDRESS'}
          </Button>,
          // <Button key="back" onClick={()=> {
          //   setaddress({});
          //   handleCancel();
          // }}>
          //   DELETE
          // </Button>
        ]}
      >
        <FormWrapper>
        { loading &&
          <div className="spin-wrapper">
          <Spin indicator={antIcon} />
        </div>
        }
        <Form form={form} layout={"vertical"} preserve={false}>
          <Row justify="space-between">
            <Col lg={24}>
              <LabelInput
                  label="Address Type"
                  name="addressType"
                  rules={[
                    {
                      required: true,
                      message: 'Address Type is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeState('addressType',e)}
                      options={teamAddressTypes}
                      toFilter="type"
                      placeholder="Address Type"
                      defaultValue={address?.addressType}
                      searchable={true}
                    />
                  }
              />
            </Col>
            <Col lg={24}>
              <LabelInput
                  label="Country"
                  name="country"
                  rules={[
                    {
                      required: true,
                      message: 'Country is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      labelInValue
                      onChange={(e: any) => getAllCountriesOrStateOrCity('country',e,`/${e?.value}/states`,setStateOptions)}
                      defaultValue={address.country}
                      options={countriesOptions ?? []}
                      toStore="value"
                      toFilter="name"
                      placeholder="Country"
                      searchable={true}
                    />
                  }
              />
            </Col>
            <Col lg={24}>
              <LabelInput
                  label="State"
                  name="addressState"
                  rules={[
                    {
                      required: Boolean(stateOptions?.length),
                      message: 'State is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      labelInValue
                      onChange={(e: any) => getAllCountriesOrStateOrCity('addressState',e,`/${address.country?.value}/states/${e?.value}/cities`,setCitiesOptions)}
                      defaultValue={address?.addressState}
                      options={stateOptions ?? []}
                      toFilter="name"
                      disabled={!(!!address?.country?.value)}
                      toStore="value"
                      placeholder="State"
                      searchable={true}
                    />
                  }
              />
            </Col>
            <Col lg={24}>
              <LabelInput
                  label="City"
                  name="city"
                  rules={[
                    {
                      required: Boolean(stateOptions?.length) && Boolean(citiesOptions?.length),
                      message: 'City is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => getAllCountriesOrStateOrCity('city',e,)}
                      defaultValue={address?.city}
                      options={citiesOptions ?? []}
                      toFilter="name"
                      placeholder="City"
                      disabled={(!(!!address?.country?.value) || !(!!address?.addressState?.value))}
                      searchable={true}
                    />
                  }
              />
            </Col>  
            <Col lg={24}>
              <LabelInput
                  label="Pincode"
                  name="pincode"
                  rules={[
                    {
                      required: true,
                      message: 'Pincode is required!',
                    },
                  ]}
                  inputElement={
                    <Input 
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeState('pincode',e.target.value)}
                      placeholder="Pincode"
                      defaultValue={address?.pincode}
                    />
                  }
              />
            </Col>   
            <Col lg={24}>
              <LabelInput
                  label="Line 1"
                  name="line1"
                  rules={[
                    {
                      required: true,
                      message: 'Line 1 is required!',
                    },
                  ]}
                  inputElement={
                    <Input 
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeState('line1',e.target.value)}
                      placeholder="Address Line 1"
                      defaultValue={address?.line1}
                    />
                  }
              />
            </Col>
            <Col lg={24}>
              <LabelInput
                  label="Line 2"
                  name="line2"
                  rules={[
                    {
                      required: false,
                      message: 'Line 2 is required!',
                    },
                  ]}
                  inputElement={
                    <Input 
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeState('line2',e.target.value)}
                      placeholder="Address Line 2"
                      defaultValue={address?.line2}
                    />
                  }
              />
            </Col> 
          </Row>
        </Form>
        </FormWrapper>
      </Modal>
    </>
  );
};

export default AddressModal;
