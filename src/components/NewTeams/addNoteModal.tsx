import React from 'react';
import { Modal, Form, Button, Row, Col, Input,} from 'antd';
import LabelInput from 'components/common/LabelInput';
import { parseJwt } from "../../utils/index";

const { TextArea } = Input;
interface AddressInfoProps {
  ModalVisible: any;
  onclickModel?: any;
  handleOk: any;
  handleCancel: any;
  EditData?: any;
  handleDelete?: any;
  readOnly?: boolean;
}

const NoteModal = ({ModalVisible,handleOk, handleCancel, EditData, handleDelete, readOnly=false}: AddressInfoProps): JSX.Element => {
  const [form] = Form.useForm();
  const [Note, setNote] = React.useState<any>({});
  
  const submitData = () => {
    form.submit();
    if(Note.name && Note.note){
      handleOk(Note, EditData);
      setNote({});
    }
  }

  React.useEffect(()=>{
    setNote(EditData)
  },[EditData])

  const isEdit = Object.keys(EditData).length > 0;

  return (
    <>
      <Modal title={`${readOnly ? "View" : isEdit ? "Edit" : "Add"} Note`}
        okText="Save"
        visible={ModalVisible}
        onOk={handleOk}
        closable={true}
        onCancel={()=>{
          setNote({});
          handleCancel();
        }}
        destroyOnClose={true}
        footer={readOnly ? null : isEdit ? [
          <Button key="submit" type="primary" onClick={()=> submitData()}>
            UPDATE NOTE
          </Button>,
          <Button key="back" 
          onClick={()=>{
            setNote({});
            handleDelete("notes");
          }}>
            DELETE
          </Button>
        ]:
        <Button key="submit" type="primary" onClick={()=> submitData()}>
          ADD NOTE
        </Button>
      }
      >
        <Form form={form} layout={"vertical"} preserve={false}>
          <Row justify="space-between">
            <Col lg={24}>
                <LabelInput
                  label="Name"
                  name="name"
                  rules={[
                    {
                      required: true,
                      message: 'Document Name is required!',
                    },
                  ]}
                  inputElement={
                    <Input
                      onChange={(e) => {
                        setNote({ ...Note, name: e.target.value });
                        form.setFieldsValue({ name: e.target.value });
                      }}
                      defaultValue={Note?.name}
                      // disabled={readOnly}
                      readOnly={readOnly}
                    />
                  }
                />
            </Col>
          </Row>
          <Row justify="space-between">
            <Col lg={24}>
                <LabelInput
                  label="Document Note"
                  name="note"
                  rules={[
                    {
                      required: true,
                      message: 'Document Note is required!',
                    },
                  ]}
                  inputElement={
                    <TextArea
                      rows={5}   
                      onChange={(e) => {
                        setNote({ ...Note, note: e.target.value });
                        form.setFieldsValue({ note: e.target.value });
                      }}
                      defaultValue={Note?.note}
                      // disabled={readOnly}
                      readOnly={readOnly}
                    />
                  }
                />
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default NoteModal;
