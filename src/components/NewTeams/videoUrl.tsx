import React from 'react';
import { Modal, Form, Button, Row, Col, Input, Upload,} from 'antd';
import LabelInput from 'components/common/LabelInput';
import { parseJwt } from "../../utils/index";
import { s3FileUpload } from 'apis/storage';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import config from 'config';
import { toast } from 'react-toastify';

import styled from 'styled-components/macro';

const ResolutionHint = styled.p`
    color: #bfbfbf;
    /* font-weight: 600; */
    margin-top: 0.5rem;
    margin-bottom: 0;
`;

const { TextArea } = Input;
interface AddressInfoProps {
  ModalVisible: any;
  onclickModel: any;
  handleOk: any;
  handleCancel: any;
  EditData: any;
  handleDelete: any
}

const VideoModal = ({ModalVisible,handleOk, handleCancel, EditData, handleDelete}: AddressInfoProps): JSX.Element => {
  const [form] = Form.useForm();
  const [Video, setVideo] = React.useState<any>({});
  const [fileData, ] = React.useState<any>([]);

  const submitData = () => {
    form.submit();
    if(Video.url || Video.uploadFile){
      handleOk(Video, EditData);
      setVideo({});
    }
  }

  const removeFile = () => {
    Video.url= "";
    Video.uploadFile= "";
    Video.name = "";
    setVideo({
      ...Video,
    });
  }

  React.useEffect(()=>{
    setVideo(EditData)
  },[EditData])

  const beforeUpload = (file:any) =>{
    let type = file.type === "video/mp4";
    if (!type) {
      toast.error(`${file.name} is not a Mp4 file`);
    }
    return type || Upload.LIST_IGNORE;
  }

  return (
    <>
      <Modal title={'Add Videos'}
        okText="Save"
        visible={ModalVisible}
        onOk={handleOk}
        closable={true}
        onCancel={()=>{
          setVideo({});
          handleCancel();
        }}
        destroyOnClose={true}
        footer={Object.keys(EditData).length > 0 ? [
          <Button key="submit" type="primary" onClick={()=> submitData()}>
            ADD Video
          </Button>,
          <Button key="back" 
          onClick={()=>{
            setVideo({});
            handleDelete("Videos");
          }}>
            DELETE
          </Button>
        ]:
        <Button key="submit" type="primary" onClick={()=> submitData()}>
          ADD Video
        </Button>
      }
      >
        <Form form={form} layout={"vertical"} preserve={false}>
          <Row justify="space-between">
            <Col lg={24}>
                <LabelInput
                  label="Enter Url"
                  name="url"
                  rules={[
                    {
                      type: "url",
                      message: 'Please Enter valid URL !',
                    },
                  ]}
                  inputElement={
                    <Input
                      onChange={(e) => {
                        setVideo({ ...Video, url: e.target.value });
                        form.setFieldsValue({ url: e.target.value });
                      }}
                      defaultValue={Video?.url}
                    />
                  }
                />
            </Col>
          </Row>
          <Row justify="space-between">
          <Col lg={24}>
                <Form.Item label="ATTACHMENT">
                {Video?.uploadFile ? (
                  <div>
                    <a title={Video?.name} href={`${config.apiBaseUrl}files/${Video?.uploadFile}`}>{Video?.name}</a> <DeleteOutlined onClick={()=> removeFile()} style={{color:"red"}}/>
                  </div>
                ) : 
                <Upload 
                  defaultFileList={fileData}
                  maxCount={1}
                  disabled={fileData?.url?.length > 0 ? true : false}
                  accept="video/*"
                  beforeUpload={(file: any) => beforeUpload(file)}
                  customRequest={async ({
                    file,
                    fileData,
                    onProgress,
                    onSuccess,
                  }: any) => {
                    let fileupload = await s3FileUpload(file);
                    if(fileupload?.status === 200){
                      setVideo({
                        ...Video,
                        uploadFile: fileupload?.data?.fileId,
                        name: file?.name
                      });
                      onProgress({ percent: 100 });
                      onSuccess(()=> console.log("onSuccess"));
                    }
                  }}
                >
                    <Button icon={<PlusOutlined />}></Button>
                </Upload>}
                    {/* <Upload {...props}>
                        <Button icon={<PlusOutlined />}></Button>
                    </Upload> */}
                </Form.Item>
                <ResolutionHint>Recommended resolution :- 750 x 406 px, Supported formats :- MP4</ResolutionHint>
            </Col>
          </Row>
        </Form>
      </Modal>
    </>
  );
};

export default VideoModal;
