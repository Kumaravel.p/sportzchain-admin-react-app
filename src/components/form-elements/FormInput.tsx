import styled from 'styled-components/macro';
import { useFormContext } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';

// import Text from '../common/typography/Text';
import FlexColumnWrapper from '../common/wrappers/FlexColumnWrapper';
import FlexRowWrapper from '../common/wrappers/FlexRowWrapper';

const FormInputWrapper = styled(FlexColumnWrapper) <{
  leftIcon?: string;
  rightIcon?: () => JSX.Element | string;
  height?: string;
}>`
  margin-top: 20px;

  .input-container {
    position: relative;
    display: inline-flex;
  }
  input {
    background: #171717;
    border-radius: 8px;
    padding: 12px;
    color: #fafafa;
    margin: auto 0;
    font-family: Inter;
    font-style: normal;
    font-size: 16px;
    letter-spacing: -0.18px;
    font-weight: 600;
    ::placeholder {
      color: #525252;
    }
    &:focus {
      outline: none;
      border: 2px solid #fb923c;
    }
    padding-left: ${(props) => props.leftIcon && '50px'};
    width: 100%;
    border: 2px solid transparent;
  }

  .icon {
    position: absolute;
    margin: auto 0;
    top: 50%;
    transform: translateY(-50%);
    display: flex;
    align-content: center;
    padding: 0 8px;
    &.left {
      left: 1%;
    }

    &.right {
      right: 1%;
    }
  }

  .label-container {
    justify-content: space-between;
    margin-bottom: 4px;

    .label {
      font-size: 14px;
      line-height: 20px;
      letter-spacing: -0.09px;
      font-weight: 600;
      color: #737373;

      span {
        font-size: 12px;
        line-height: 18px;
      }
    }
  }
  .supporting-label {
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    text-align: right;
    letter-spacing: -0.09px;
    color: #525252;
    margin-top: 5px;
  }
  p {
    margin-top: 5px;
    color: red;
  }
`;

interface FormInputProps {
  rightIcon?: () => JSX.Element | string;
  type?: string;
  placeholder?: string;
  leftIcon?: string;
  value?: string;
  height?: string;
  label?: string;
  supportingLabel?: string;
  name: string;
  required?: boolean;
}

const FormInput = ({
  rightIcon,
  leftIcon,
  type,
  placeholder,
  height,
  label,
  supportingLabel,
  name,
  required = false,
}: FormInputProps & React.HTMLProps<HTMLInputElement>): JSX.Element => {
  const {
    register,
    formState: { errors },
  } = useFormContext(); // retrieve all hook methods

  return (
    <FormInputWrapper leftIcon={leftIcon} rightIcon={rightIcon} height={height}>
      <FlexRowWrapper className="label-container">
        <div className="label">
          {label}&nbsp;
          {required ? <span>required</span> : null}
        </div>
      </FlexRowWrapper>
      <div className="input-container">
        {leftIcon && (
          <img src={leftIcon} alt="input-left-icon" className="icon left" />
        )}
        <input type={type} placeholder={placeholder} {...register(`${name}`)} />
        {typeof rightIcon === 'string' ? (
          <img src={rightIcon} alt="input-right-icon" className="icon right" />
        ) : (
          <div className="icon right">{rightIcon ?? rightIcon}</div>
        )}
      </div>
      <div className="supporting-label">{supportingLabel}</div>

      <ErrorMessage
        errors={errors}
        name={name}
        render={(error) => <p className="error-message">{error.message}</p>}
      />
    </FormInputWrapper>
  );
};

export default FormInput;
