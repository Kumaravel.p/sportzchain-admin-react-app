import styled from 'styled-components/macro';
import React, { useCallback, useMemo } from 'react';
import {
  Button,
  Card,
  Col,
  Input,
  Row,
  DatePicker,
  Form,
  Upload,
  Typography,
  Grid,
  InputNumber,
} from 'antd';
import { Label, TextContent } from 'components/common/atoms';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import Text from 'antd/lib/typography/Text';
import { DeleteOutlined } from '@ant-design/icons';
import { GetPollsCount } from 'graphql_v2';
import graphqlReq from 'apis/graphqlRequest';
import {
  useCreatePostMutation,
  // useCreateTeamMutation,
  useGetTeamStatusesQuery,
  useGetTeamsByStatusIdLazyQuery,
} from 'generated/graphql';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import LabelInput from 'components/common/LabelInput';
import { toast } from 'react-toastify';
import SelectElement from 'components/common/SelectElement';
import TitleCta from 'components/common/TitleCta';
import TextEditorInput from 'components/common/TextEditorInput';
import moment, { MomentInput } from 'moment';
import BreadcrumbComp from 'components/common/breadcrumb';
import { categoryOptions, pollFilterStatus } from 'utils';
import { TeamSelectionContext } from 'App';
import { createNewsApi } from 'apis/news/create';
import { updateNewsApi } from 'apis/news/update';
import { s3FileUpload } from 'apis/storage';
import config from 'config';
import { useGetAllGroupsByStatusIdLazyQuery } from 'generated/pgraphql';
import { PlusOutlined } from '@ant-design/icons';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import currentSession from 'utils/getAuthToken';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const AddPostWrapper = styled.div`
  padding: 0 40px;
  width: 100%;

  .t-transactions {
    font-size: 25px;
    line-height: 30px;
    color: rgba(5, 31, 36, 0.3);
  }

  .row{
    display: flex;
    justify-content: space-between;
    align-items:center;
    margin-top:20px
  }

  .breadcrumb{
    font-size:22px;
    font-weight: 600;
    line-height: 30px;
    opacity:0.7;
    .active{
        opacity:1
    }
  }

  .field{
    border-radius:5px;
    width:100%;
   
  }

  input[type=number]::-webkit-inner-spin-button {
    visibility:hidden
  }

  .upload-button{
    height:160px;
    width:160px
  }
  .t-transaction-id {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .ant-card {
    margin-top: 40px;
    width: 100%;
    .info-edit-wrapper {
      margin-bottom: 40px;
    }
    .t-info {
      font-weight: 600;
      font-size: 20px;
      line-height: 24px;
      color: #051f24;
    }
    .t-transaction-key-field {
      font-size: 15px;
      line-height: 18px;
      color: rgba(5, 31, 36, 0.6);
    }
    .t-transaction-value {
      font-size: 16px;
      line-height: 19px;
      font-weight: 500;
      color: #051f24;
      margin: 10px;
      &.blue {
        color: #4da1ff;
      }
    }
    .key-value-wrapper {
      row-gap: 10px;
    }
    .edit-col {
      display: flex;
      align-items: center;
      column-gap: 20px;
    }
  }

  .ant-btn.add-new-button {
    border: 1px solid rgba(7, 105, 125, 0.25);
    color: rgba(7, 105, 125, 1);
    box-sizing: border-box;
    border-radius: 5px;
    height: fit-content;
    padding: 15px;
    display: flex;
    align-items: center;
    column-gap: 10px;
  }

  .sr-no-wrapper {
    position: relative;
    .srno-default-pop {
      position: absolute;
      background: #4aaf05;
      border: 2px solid #4aaf05;
      font-size: 12px;
      padding: 0 6px;
      top: -10px;
      left: 12px;
      color: #fff;
      border-radius: 100px;
    }
  }

  .banner-image {
    &.banner-urls {
      display: flex;
      column-gap: 20px;
      width: 70vw;
      overflow-x: scroll;
      img {
        height: 500px;
        width: 800px;
      }
    }
    img.logo {
      max-width: 300px;
      border-radius: 4px;
      max-height: 300px;
    }
  }

  .sub-routes-wrapper {
    column-gap: 8px;
    margin: 60px auto;
    text-align: center;
    .sub-route {
      padding: 8px;
      width: 100%;
      background: rgba(198, 198, 198, 0.6);
      color: rgba(5, 31, 36, 0.6);
      border-radius: 4px;
      cursor: pointer;
      font-weight: 600;
      &.active {
        background: #fff;
        border: 1px solid #eee;
        color: #4da1ff;
      }
    }
  }
  a {
    text-decoration: none;
    color: inherit;
  }
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.discard {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }`;
const HoverDiv = styled('div')`
position:relative;
cursor:pointer;
& img{
  width: 159px;
  height: 159px;
  object-fit:cover;
}
& > div{
  display:none;
  position:absolute;
  bottom:0;
  left:0;
  right:0;
}
& button{
  color:#fff;
  font-weight:bold;
  width:100%;
  &:hover{
    color:#fff;
  }
}
&:hover{
  & img{
    filter:brightness(0.6);
  }
  & > div{
    display:block;
  }
}
`
interface AddPostProps {
  newsFormData?: any;
}

const Addpost = ({ newsFormData }: AddPostProps): JSX.Element => {
  const [form] = Form.useForm();
  const { id } = useParams<{ id: string }>();
  const { state } = useLocation<any>();
  const [createPost] = useCreatePostMutation();

  //team listing
  const [getAllTeams, { data: teams, loading: teamOptionLoading }] = useGetTeamsByStatusIdLazyQuery({
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    variables: {
      statusId: ["APPROVED", "PUBLISHED", "ACTIVE"]
    }
  });

  //post status list
  const { data: teamStatuses, loading: teamStatusLoading } = useGetTeamStatusesQuery();

  //group listing
  const [getAllGroups, { data: groups, loading: groupOptionLoading }] = useGetAllGroupsByStatusIdLazyQuery({
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    variables: {
      statusID: [1]
    }
  });

  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  const [formData, setFormData] = React.useState<object>({});
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [updateImage, setImageupdate] = React.useState(false);
  const [pollCount, setPollcount] = React.useState(0);
  const [message, setMessage] = React.useState('');
  const history = useHistory();
  const isTeamUser = isIamTeamUser();
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  const [setImage,ImageUpload] = React.useState<any>()
  const [categoryType,setCategoryType] = React.useState<any>("");
  const [tokenSymbol,setTokenSymbol] = React.useState<string>("");
  const [teamId,setTeamId] = React.useState<number | null>();
  const [isVisible,setIsVisible] = React.useState<boolean>(false);
  const [notesList,setNotesList] = React.useState<any[]>([]);
  const [editIndex,setEditIndex] = React.useState<number>(-1);
  const [checkStatus,setCheckStatus] = React.useState<boolean>(false);
  const [userData, setUserData] = React.useState<any>({});

  //get team Details by id
  const getTeamById = (teamId: number) => teams?.team.find(team => team?.id === teamId)

  React.useEffect(() => {
    if (categoryType === "GROUPS" && !groups?.allGroups?.nodes) {
      getAllGroups();
    }
  }, [categoryType]);

  React.useEffect(() => {
    if (state?.teamId) {
      form.setFieldsValue({
        teamId: state?.teamId,
        category: "PLATFORM"
      })
      setCategoryType("PLATFORM");
      setTeamId(state?.teamId);
    }
  }, [state])

  React.useEffect(() => {
    if (newsFormData) {
      setCategoryType(newsFormData?.type ?? "PLATFORM");
      setTokenSymbol(newsFormData?.coinSymbol ?? "")
      setTeamId(newsFormData?.teamByTeamId?.id);
      setMessage(newsFormData?.description);
      if(newsFormData?.postNotesByPostId?.nodes?.length){
        let contructNotesList = newsFormData?.postNotesByPostId?.nodes?.map((note:any)=>({
            name: note?.name,
            notes: note?.notes,
            addedDate: moment(note?.createdDate.concat('z')),
            addedby: note?.addedBy,
            id: note?.id,
        }))
        setNotesList(contructNotesList)
      }
    }
  }, [newsFormData])

  React.useEffect(() => {
    graphqlReq({ document: GetPollsCount })
      .then((data: any) => {
        setPollcount(data?.allPosts?.totalCount)
        form.setFieldsValue({
          displayPriority: data?.allPosts?.totalCount + 1
        })
      })
      .catch(e => console.log(e))
  }, [])

  React.useEffect(() => {
    if (checkStatus) {
      onSave(form.getFieldsValue())
    }
  }, [notesList])

  const onSave = async (formValues: any) => {
    setConfirmLoading(true);
    //construct notes payload
    let contructNotes:any[] = [];

    if(notesList?.length){
      contructNotes=notesList?.map(note => ({
        "addedDate": note?.addedDate,
        "name": note?.name,
        "notes": note?.notes,
        "addedBy": note?.addedby
      }))
    }
    if(newsFormData){
      let oldData;
      if (typeof (formValues?.teamId) === 'string' || formValues?.teamId === null) {
        oldData = {
          postId: newsFormData?.id,
          title: formValues?.title,
          startAt: formValues?.startAt.seconds('0o0').toISOString(),
          endAt: formValues?.endAt.seconds('0o0').toISOString(),
          displayStartAt: formValues?.displayStartAt.seconds('0o0').toISOString(),
          displayEndAt: formValues?.displayEndAt.seconds('0o0').toISOString(),
          minJoinBalance: parseInt(formValues?.minJoinBalance as unknown as string),
          coinSymbol: formValues?.coinSymbol,
          displayPriority: parseInt(formValues?.displayPriority as unknown as string),
          description: message,
          postUrl: formValues?.postUrl,
          statusId: formValues?.statusId,
          type: formValues?.category,
          groupId: formValues?.groupId,
          notes:contructNotes,
        }
      }
      else {
        oldData = {
          ...formValues,
          postId: newsFormData?.id,
          title: formValues?.title,
          startAt: formValues?.startAt.seconds('0o0').toISOString(),
          endAt: formValues?.endAt.seconds('0o0').toISOString(),
          displayStartAt: formValues?.displayStartAt.seconds('0o0').toISOString(),
          displayEndAt: formValues?.displayEndAt.seconds('0o0').toISOString(),
          statusId: formValues?.statusId,
          minJoinBalance: parseInt(formValues.minJoinBalance as unknown as string),
          coinSymbol: formValues?.coinSymbol,
          displayPriority: parseInt(formValues.displayPriority as unknown as string),
          description: message,
          postUrl: formValues?.postUrl,
          type: formValues?.category,
          ...(formValues?.category === "GROUPS" ? {
            groupId: formValues?.groupId,
          } : {
            teamId: parseInt(formValues?.teamId as unknown as string),
          }),
          notes:contructNotes,
        }
      }
      delete oldData?.["category"]
      await updateNewsApi(oldData)
        .then(() => {
          setConfirmLoading(false);
          toast.success(`Updated Post`);
          let key = formValues?.category === "GROUPS" ? "groupId" : "teamId"
          history.push(`/news/${formValues?.[key]}/${id}`)
        })
        .catch((err) => {
          console.log(err)
          setConfirmLoading(false)
          toast.error(`Post Not Updated`);
        })
    }
    else {
      let newData = {
        ...formValues,
        title: formValues?.title,
        statusId: formValues?.statusId,
        startAt: formValues?.startAt.seconds('0o0').toISOString(),
        endAt: formValues?.endAt.seconds('0o0').toISOString(),
        displayStartAt: formValues?.displayStartAt.seconds('0o0').toISOString(),
        displayEndAt: formValues?.displayEndAt.seconds('0o0').toISOString(),
        minJoinBalance: parseInt(formValues.minJoinBalance as unknown as string),
        coinSymbol: formValues?.coinSymbol,
        displayPriority: parseInt(formValues.displayPriority as unknown as string),
        description: message,
        postUrl: formValues?.postUrl,
        type: formValues?.category,
        ...(formValues?.category === "GROUPS" ? {
          groupId: formValues?.groupId,
        } : {
          teamId: parseInt(formValues.teamId as unknown as string),
        }),
        notes:contructNotes,
      }
      delete newData?.["category"]
      await createNewsApi(newData)
        .then((res) => {
          setConfirmLoading(false);
          if (res?.data?.statusCode !== "200") {
            toast.error(res?.data?.message);
            return false
          }
          toast.success(res?.data?.message);
          let key = formValues?.category === "GROUPS" ? "groupId" : "teamId"
          let postId = id ? id : res?.data?.data?.createdPost?.createPost?.post?.id
          history.push(`/news/${formValues?.[key]}/${postId}`)
        })
        .catch((err) => {
          console.log(err)
          setConfirmLoading(false)
          toast.error(`Post Not Created`);
        })

    }
  };

  const removeBannerLogo = () => {
    ImageUpload('')
    form.setFieldsValue({ postUrl: "" })
  }

  const filterSelectedTeam = (teams: any, teamId: number) => {
    return (
      teams?.team?.filter((item: any) => item?.id === teamId)?.[0] ?? []
    );
  };

  const uploadImageType = async (file: any) => {

    let fileupload = await s3FileUpload(file);
    if (fileupload.status === 200) {
      toast.success("File uploaded successfully");
      ImageUpload(fileupload.data.fileId);
      form.setFieldsValue({ postUrl: fileupload?.data?.fileId })
    }
    else {
      toast.error("File uploaded failed");
    }
  }

  const beforeUpload = (file: any) => {
    const allowedTypes = file.type === 'image/png' || file.type === 'image/jpeg';
    if (!allowedTypes) {
      toast.error(`${file.name} is not a png or jpeg file`);
    }
    return allowedTypes || Upload.LIST_IGNORE;
  }

  const onMessageStore = (body: any) => {
    setMessage(body);
    form.setFieldsValue({
      description: body
    });
  };

  React.useEffect(() => {
    if (!isTeamUser) {
      getAllTeams();
    } else {
      form.setFieldsValue({ teamId: teamSelectionContext.team.id })
    }
    currentSession().then(res => {
      setUserData(res.payload)
    })
    setMessage(newsFormData?.description)
  }, [])

  //disable start date
  const isDisableStartDate = useCallback((date) => {
    return date && moment(date).isBefore(moment(), 'day')
  }, [form])

  //disable end date
  const isDisableEndDate = useCallback((date, key) => {
    if (!form.getFieldValue(key)) {
      return true
    }
    let checkDate: MomentInput = moment(form.getFieldValue(key)).isBefore(moment(), 'day') ? moment() : moment(form.getFieldValue(key))
    return date && moment(date).isBefore(checkDate, 'day')
  }, [form])  
  
  
  //status validation for Start Date Time
  const startDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`Start Date Time is required!`));
    }
    if (getFieldValue('endAt')) {
      if (!value.isBefore(getFieldValue('endAt'))) {
        return Promise.reject(new Error(`Start Date Time should be lesser than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('displayStartAt')) {
      if (!value.isAfter(getFieldValue('displayStartAt'))) {
        return Promise.reject(new Error(`Start Date Time should be greater than Display Start Date Time(${getFieldValue('displayStartAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isAfter(moment(team?.activeStartAt?.concat('z')))) {
        return Promise.reject(new Error(`Start Date Time should be greater than Team Start Date Time(${moment(team?.activeStartAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for End Date Time
  const endDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`End Date Time is required!`));
    }
    if (getFieldValue('startAt')) {
      if (!value.isAfter(getFieldValue('startAt'))) {
        return Promise.reject(new Error(`End Date Time should be greater than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('displayEndAt')) {
      if (!value.isBefore(getFieldValue('displayEndAt'))) {
        return Promise.reject(new Error(`End Date Time should be lesser than Display End Date Time(${getFieldValue('displayEndAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isBefore(moment(team?.activeEndAt?.concat('z')))) {
        return Promise.reject(new Error(`End Date Time should be lesser than Team End Date Time(${moment(team?.activeEndAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for Display Start Date Time
  const displayStartDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`Display Start Date Time is required!`));
    }
    if (getFieldValue('startAt')) {
      if (!value.isBefore(getFieldValue('startAt'))) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isAfter(moment(team?.displayStartAt?.concat('z')))) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Team Start Date Time(${moment(team?.displayStartAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for Display End Date Time
  const displayEndDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`Display End Date Time is required!`));
    }
    if (getFieldValue('endAt')) {
      if (!value.isAfter(getFieldValue('endAt'))) {
        return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isBefore(moment(team?.displayEndAt?.concat('z')))) {
        return Promise.reject(new Error(`Display End Date Time should be lesser than Team End Date Time(${moment(team?.displayEndAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  const getPropsByType = () => {
    let type = form.getFieldValue('category');
    const returnValues = (teamValue: any, groupValue: any) => type === "PLATFORM" ? teamValue : groupValue;
    return {
      label: `${returnValues("Team", "Group")} Name`,
      name: returnValues("teamId", "groupId"),
      rules: {
        message: `${returnValues("Team", "Group")} Name is required!`
      },
      toFilter: returnValues("name", "groupName"),
      placeholder: `Select ${returnValues("Team", "Group")}`,
      loading: returnValues(teamOptionLoading, groupOptionLoading),
      options: returnValues(teams?.team, groups?.allGroups?.nodes),
      disabled: () => returnValues(((disabledComponents('teamId') || state?.teamId) ? true : isTeamUser), disabledComponents('groupId')),
    }
  }

  //on delete Notes
  const deleteNotes = () => {
    let filteredNotes = notesList?.filter((list, index) => index !== editIndex)
    setNotesList(filteredNotes)
    handleCancelNote()
  }

  //on cancel notes
  const handleCancelNote = () => {
    setIsVisible(false)
    if (editIndex > -1) {
      setEditIndex(-1)
    }
  }

  //on upserting notes
  const onUpsertNotes = (val: any, editdata: any) => {

    if (Object.keys(editdata).length > 0 && editIndex > -1) {
      notesList[editIndex] = {
        ...notesList[editIndex],
        name: val?.name,
        notes: val?.note,
      }
      setNotesList(notesList)
    }
    else {
      setNotesList([
        ...notesList, {
          name: val?.name,
          notes: val?.note,
          addedDate: moment(),
          addedby: userData?.name,
        }
      ])
    }
    handleCancelNote()
  }

  //on open individual note
  const openEditNote = (obj: any, key: number) => {
    setEditIndex(key - 1);
    setIsVisible(true)
  }

  // note data
  const editNoteData = React.useMemo(() => {
    if (editIndex > -1) {
      return {
        name: notesList[editIndex]?.name,
        note: notesList[editIndex]?.notes,
        id: notesList[editIndex]?.id,
      }
    }
    return {}
  }, [editIndex])

  const getTeamWalletById = (teamId:number) =>{
    return getTeamById(teamId)?.wallets?.[0]?.coinSymbol;
  }
  
  const checkCurrencyType = (val:string) =>{
    return ['SPN','RWRD'].includes(val)
  }

  const onValuesChange = () =>{
    setFormData({ ...form.getFieldsValue(true) })
  }

  //post status validation
  const statusValidations = useMemo(() => {
    return pollFilterStatus(teamStatuses?.teamStatus, newsFormData?.statusId, newsFormData?.displayStartAt?.concat('z'))
  }, [newsFormData, teamStatuses])

  //find disable components
  const disabledComponents = React.useCallback((key: string) => {
    if (!newsFormData) return false
    let validate = statusValidations?.disableComponents?.includes(key)
    return statusValidations?.except ? validate : !validate
  }, [statusValidations])

  const onBeforeSave = () =>{
    form.submit();
    form.validateFields()
    .then(async () => {
      try {
        // check status of post if status is in ['SUBMITTED', 'APPROVED', 'DISCONTINUED', 'SUSPENDED'] then we ask to add notes befoew save 
        if ( id && ['SUBMITTED', 'APPROVED', 'DISCONTINUED', 'SUSPENDED'].includes(form.getFieldValue('statusId'))) {
          setIsVisible(true);
          setCheckStatus(true)
        }
        else{
          if(checkStatus){
            setCheckStatus(false)
          }
          onSave(form.getFieldsValue());
        }
      } catch (error: any) {
        toast.error(error?.message || JSON.stringify(error));
      }
    })
    .catch((e) => {
      console.log(e, 'e');
      toast.info('Please fill all the mandatory fields!')
    });
  }

  // team currency symbol show hide based on team's coin symbol
  const currencySymbolToggle = () =>{
    let teamId = form.getFieldValue('teamId');
    return (teamId ? getTeamWalletById(teamId) ? true : false : true)
  } 

 
  const onPerview =(formValues:any)=>{
      let contructNotes:any[] = [];

      if(notesList?.length){
        contructNotes=notesList?.map(note => ({
          "addedDate": note?.addedDate,
          "name": note?.name,
          "notes": note?.notes,
          "addedBy": note?.addedby
        }))
      }

      let newData = {
        title: formValues?.title,
        statusId: formValues?.statusId,
        startAt:formValues?.startAt.seconds('0o0').toISOString().slice(0,-1),
        endAt: formValues?.endAt.seconds('0o0').toISOString().slice(0,-1),
        displayStartAt: formValues?.displayStartAt.seconds('0o0').toISOString().slice(0,-1),
        displayEndAt: formValues?.displayEndAt.seconds('0o0').toISOString().slice(0,-1),
        minJoinBalance: parseInt(formValues?.minJoinBalance as unknown as string),
        coinSymbol: formValues?.coinSymbol,
        displayPriority: parseInt(formValues?.displayPriority as unknown as string),
        description: message,
        postUrl: formValues?.postUrl,
        type: formValues?.category,
        ...(formValues?.category === "GROUPS" ? {
          groupId: formValues?.groupId,
        } : {
          teamId: parseInt(formValues?.teamId as unknown as string),
        }),
        notes:contructNotes,
        teamByTeamId : {
          "id":formValues?.teamId,
          "name": filterSelectedTeam(teams,formValues?.teamId)?.name,
          "profileImageUrl": filterSelectedTeam(teams,formValues?.teamId)?.profileImageURL,
          "currencyTypeId": filterSelectedTeam(teams,formValues?.teamId)?.currencyTypeId,
          "categoryId": filterSelectedTeam(teams,formValues?.teamId)?.categoryId
          }
      }
      if(newData){
        window.open(`${config.sportzchainFanPageURL}/post-preview?formValues=${btoa(JSON.stringify({...newData}))}`)
      } 
  }

  return (
    <AddPostWrapper>
        <Row className='row'>
            <BreadcrumbComp
              backPath="/news"
              cta={
                <>
                  <Button className="save cta-button" onClick={()=>onBeforeSave()}>
                  {confirmLoading ? 'Creating...' : 'Save'}
                  </Button>
                   <Button
                  className="cta-button discard"
                  onClick={() => history.push('/news')}
                 >
                  Discard
                </Button>
                <Button
                   className="cta-button discard"
                   onClick={()=>onPerview({...form.getFieldsValue()})}
                >
                  Live Preview
                </Button>
                </>
              }
              breadCrumbs={[
                { name: 'Newsfeed', url: '/news' },            
                { name: newsFormData ? 'Edit Post' : 'Add New Post', url: newsFormData ? `/newsfeed/edit/${id}` : '/news/add'},
              ]}
            />
          </Row>
       
      <Form onFinish={onSave} form={form} preserve={false}  
        name="basic-form"
        initialValues={
          newsFormData ? {
            ...newsFormData,
            teamId: newsFormData?.teamByTeamId?.id,
            startAt: moment(newsFormData?.startAt + "z"),
            endAt: moment(newsFormData?.endAt + "z"),
            displayStartAt: moment(newsFormData?.displayStartAt + "z"),
            displayEndAt: moment(newsFormData?.displayEndAt + "z"),
            coinSymbol: newsFormData?.coinSymbol,
            minJoinBalance: newsFormData?.minJoinBalance,
            displayPriority: newsFormData?.displayPriority,
            category: newsFormData?.type ?? "PLATFORM",
            groupId: newsFormData?.groupId,
          } : {
            teamId: state?.teamId ? state?.teamId : null,
            category: state?.teamId ? "PLATFORM" : null,
          }
        }
        onValuesChange={onValuesChange}
      >
        <Card>
          <TitleCta
            title={
              state?.teamId ? `Team > ${getTeamById(state?.teamId)?.name} > Basic Info` : "Basic Info"
            }
          />

          <Row gutter={[24, 24]}>
            <Col span={8}>
              <LabelInput
                label="Title"
                name="title"
                rules={[
                  {
                    required: true,
                    message: 'Title is required!',
                  },
                ]}
                inputElement={
                  <Input
                    type="text"
                    disabled={['PUBLISHED', 'OPEN', 'CLOSED', 'APPROVED'].includes(
                      newsFormData?.statusId ?? ''
                    )}
                    onChange={(e) =>
                      form.setFieldsValue({ name: e.target.value })
                    }
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                label="Category"
                name="category"
                rules={[
                  {
                    required: true,
                    message: 'Category is required!',
                  },
                ]}
                inputElement={
                  <SelectElement
                    onChange={(e: any) => {
                      form.setFieldsValue({
                        category: e,
                        groupId: null,
                        teamId: null,
                        ...(!checkCurrencyType(form.getFieldValue('coinSymbol')) && { coinSymbol: null })
                      });
                      setCategoryType(e)
                      setTeamId(null);
                      if (!checkCurrencyType(form.getFieldValue('coinSymbol'))) {
                        setTokenSymbol("")
                      }
                    }}
                    disabled={['PUBLISHED', 'OPEN', 'CLOSED', 'APPROVED'].includes(newsFormData?.statusId) || state?.teamId || disabledComponents('category')}
                    toStore="value"
                    options={categoryOptions}
                    toFilter="label"
                    placeholder="Category"
                  />
                }
              />
            </Col>
            {
              categoryType &&
              <Col span={8}>
                <LabelInput
                  label={getPropsByType().label}
                  name={getPropsByType().name}
                  rules={[
                    {
                      required: true,
                      message: getPropsByType().rules.message,
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        let key = getPropsByType().name
                        form.setFieldsValue({
                          [key]: e,
                          ...(!checkCurrencyType(form.getFieldValue('coinSymbol')) && { coinSymbol: null })
                        });
                        if (key === "teamId") {
                          setTeamId(Number(e));
                          if (
                            tokenSymbol !== 'SPN' &&
                            tokenSymbol !== 'RWRD' &&
                            tokenSymbol
                          ) {
                            let coinSymbol = getTeamById(e as unknown as number)?.wallets?.[0]?.coinSymbol ?? "";
                            setTokenSymbol(coinSymbol)
                            form.setFieldsValue({ coinSymbol })
                          }
                        }
                      }}
                      toStore="id"
                      options={getPropsByType().options ?? []}
                      toFilter={getPropsByType().toFilter}
                      placeholder={getPropsByType().placeholder}
                      loading={getPropsByType().loading}
                      disabled={getPropsByType().disabled() || ['PUBLISHED', 'OPEN', 'CLOSED', 'APPROVED'].includes(
                        newsFormData?.statusId ?? ''
                      )}
                      searchable
                    />
                  }
                />
              </Col>
            }
            {newsFormData && 
             <Col span={8}>
             <LabelInput
               label="Status"
               name="statusId"
               inputElement={
                 <SelectElement
                   onChange={(e: any) => {
                     form.setFieldsValue({ statusId: e });
                   }}
                   options={teamStatusLoading ? [] : statusValidations.options}
                   toFilter="status"
                   placeholder="Status"
                 />
               }
             />
             </Col>
            } 
            <Col span={8}>
              <LabelInput
                label="Start Date & Time"
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      return startDateTimeValidation(getFieldValue, value)
                    },
                  }),
                ]}
                name="startAt"
                inputElement={
                  <DatePicker
                    format={"DD-MM-YYYY hh:mm"}
                    showNow={false}
                    showSecond={false}
                    showTime
                    disabledDate={(value) => isDisableStartDate(value)}
                    disabled={['OPEN','CLOSED'].includes(
                      newsFormData?.statusId ?? ''
                    )}
                    style={{ width: "100%" }}
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                label="End Date & Time"
                name="endAt"
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      return endDateTimeValidation(getFieldValue, value)
                    },
                  }),
                ]}
                inputElement={
                  <DatePicker
                    disabled={['CLOSED'].includes(
                      newsFormData?.statusId ?? ''
                    )}
                    format={"DD-MM-YYYY hh:mm"}
                    showNow={false}
                    showSecond={false}
                   disabledDate={(value) => isDisableEndDate(value, 'startAt')}
                    showTime={Boolean(form.getFieldValue('startAt'))}
                    style={{ width: "100%" }}
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                 label="Display Start Date & Time"
                 name="displayStartAt"
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      return displayStartDateTimeValidation(getFieldValue, value)
                    },
                  }),
                ]}
                 inputElement={
                  <DatePicker
                    showTime
                    disabled={['OPEN', 'PUBLISHED', 'CLOSED'].includes(
                      newsFormData?.statusId ?? ''
                    )}

                    disabledDate={(value) => isDisableStartDate(value)}
                    format={"DD-MM-YYYY hh:mm"}
                    showNow={false}
                    showSecond={false}
                    style={{ width: "100%" }}
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                 label="Display End Date & Time"
                 name="displayEndAt"
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      return displayEndDateTimeValidation(getFieldValue, value)
                    },
                  })
                ]}
                 inputElement={
                  <DatePicker
                   showTime={Boolean(form.getFieldValue('displayStartAt'))}
                    disabledDate={(value) => isDisableEndDate(value, 'displayStartAt')}
                    style={{ width: "100%" }}
                    format={"DD-MM-YYYY hh:mm"}
                    showNow={false}
                    showSecond={false}
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                label="Currency Type"
                name="coinSymbol"
                inputElement={
                  <SelectElement
                    onChange={(e: any) => {
                      form.setFieldsValue({ coinSymbol: e });
                      setTokenSymbol(e)
                    }}
                    disabled={['PUBLISHED', 'OPEN', 'CLOSED', 'APPROVED'].includes(
                      newsFormData?.statusId ?? ''
                    )}
                    options={[
                      {
                        currencyType: 'SPN',
                      },
                      {
                        currencyType: 'RWRD',
                      },
                      ...((categoryType === "PLATFORM" && currencySymbolToggle()) ? [{
                        currencyType: teamId ? getTeamWalletById(teamId) : 'Choose your team',
                      }] : []),
                    ]}
                    disabledOptions={["Choose your team"]}
                    toFilter="currencyType"
                    placeholder="Currency Type"
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                label="Minimum Token needed"
                name="minJoinBalance"
                inputElement={
                  <Input
                    className='field'
                    type="number"
                    disabled={['PUBLISHED', 'OPEN', 'CLOSED', 'APPROVED'].includes(
                      newsFormData?.statusId ?? ''
                    )}
                    min={0}
                    suffix={
                      <Typography style={{ color: "#0075FF" }}>{tokenSymbol}</Typography>
                    }
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                label="Post Display Priority"
                name="displayPriority"
                inputElement={
                  <InputNumber
                    className='field'
                    type="number"
                    disabled={['PUBLISHED', 'OPEN', 'CLOSED', 'APPROVED'].includes(
                      newsFormData?.statusId ?? ''
                    )}
                    min={1}
                    max={pollCount && pollCount + 1}
                  />
                }
              />
            </Col>
          </Row>
        </Card>

        <Card>

          <TitleCta
            title="Post Details"
          />



          <LabelInput
            label="Body Text"
            //  name="description"
            inputElement={
              <TextEditorInput value={message} editData={newsFormData?.description} setValue={onMessageStore} />
            }
          />

          {newsFormData?.postUrl && !updateImage ?
            <div className="banner-image">
              <Label>Post image <Button style={{ marginLeft: "10px" }} onClick={() => setImageupdate(true)}>+ Update Image</Button></Label>
              <img
                className="banner-image-element logo"
                src={
                  `${config.apiBaseUrl}files/${newsFormData.postUrl}` ??
                  ''
                }
                alt="banner"
              />
            </div>
            :
            <LabelInput
              label={
                <div>
                  {updateImage ? <Label>Post image <Button style={{ marginLeft: "10px" }} onClick={() => setImageupdate(false)}>Back</Button></Label> : "Post Image"}
                </div>
              }
              name="postUrl"
              //  rules={[{ required: true, message: 'Image is required!' }]}
              // initialValue={bannerData?.url}
              inputElement={
                <div style={{ display: "flex", alignItems: "center", gap: '16px' }}>
                  {
                    setImage &&
                    <HoverDiv>
                      <img src={`${config.apiBaseUrl}files/${setImage}`} />
                      <div>
                        <Button type='text' onClick={removeBannerLogo}>Remove<DeleteOutlined /></Button>
                      </div>
                    </HoverDiv>

                  }
                  <Upload
                    iconRender={() => <></>}
                    maxCount={1}
                    accept="image/png, image/jpeg"
                    customRequest={async ({ file, fileList, onProgress }: any) => {
                      await uploadImageType(file)
                    }}
                    beforeUpload={(file: any) => beforeUpload(file)}
                    showUploadList={false}
                  >
                    <Button className="upload-button">+</Button>
                  </Upload>
                </div>

              }
            />
          }


          {/* src={
                      `${config.apiBaseUrl}files/${teamDisplay?.team_by_pk?.profileImageURL}` ??
                      ''
                    } */}

        </Card>
        {/* <Card>
          <LabelValue
            field="Preview"
            value={
              <PostCard
                postCreationDate={form.getFieldValue('publishedAt')}
                body={form.getFieldValue('body') ?? ''}
                postBannerImageUrl={form.getFieldValue('postUrl') ?? ''}
              />
            }
          />
        </Card> */}
        {/* Notes UI START */}
        <Card>
          <TitleCta
            title="Notes"
            cta={(
              <Button type="link" icon={<PlusOutlined />} onClick={() => setIsVisible(true)}>
                Add New Note
              </Button>
            )}
          />
          <NoteModal
            handleDelete={deleteNotes}
            EditData={editNoteData}
            ModalVisible={isVisible}
            handleOk={(val: any, editdata: any) => onUpsertNotes(val, editdata)}
            handleCancel={handleCancelNote}
            readOnly={editNoteData?.id}
          />
          <Note
            JsonData={notesList}
            openEdit={(obj: any, key: number) => openEditNote(obj, key)}
          />
        </Card>
        {/* Notes UI END */}
      </Form>
    </AddPostWrapper>
  );
};

export default Addpost;
function typeOf(teamId: any) {
  throw new Error('Function not implemented.');
}
