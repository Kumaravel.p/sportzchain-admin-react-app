
import styled from 'styled-components/macro';
import React, { useCallback, useMemo } from 'react';
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Divider,
  Input,
  Row,
  DatePicker,
  Form,
  InputNumber,
  Checkbox,
  Switch
} from 'antd';
import { LeftOutlined, PlusOutlined } from '@ant-design/icons';
import {
  GetPollQuery,
  useGetTeamPollTypesQuery,
  useGetTeamPollPlatformsQuery,
  useGetPollsCountQuery,
  useGetTeamStatusesQuery,
  useGetTeamsByStatusIdLazyQuery,
} from 'generated/graphql';
import { Link, useHistory, useLocation } from 'react-router-dom';
import LabelInput from 'components/common/LabelInput';
import { toast } from 'react-toastify';
import SelectElement from 'components/common/SelectElement';
import TitleCta from 'components/common/TitleCta';
import moment, { MomentInput } from 'moment';
import PollQuestionCard from 'components/common/cards/PollQuestionCard';
import PollTemplate from './PollTemplate';
import { isValidHttpUrl } from 'utils/functions'
import { v4 as uuidv4 } from 'uuid';
import { createPollApi } from 'apis/polls/create'
import { updatePollApi } from 'apis/polls/update';
import BreadcrumbComp from 'components/common/breadcrumb'

import type { PayloadProps } from 'apis/polls/create'
import type { TeamPollOption, Poll } from 'ts/interfaces/polls.inteface';
import { categoryOptions, pollFilterStatus } from 'utils';
import config from 'config';

import { TeamSelectionContext } from 'App';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import currentSession from 'utils/getAuthToken';
import { Label, TextContent } from 'components/common/atoms';
import OptionsView from './optionsview';
import Text from 'antd/lib/typography/Text';
import { useGetAllGroupsByStatusIdLazyQuery } from 'generated/pgraphql';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';


const AddPollWrapper = styled.div`
  padding: 40px 40px;
  background:rgba(245, 245, 247, 1);
  width: 100%;
  .ant-btn{
    border-radius:5px;
  }
  .field{
    border-radius:5px;
    width:100%
  }
  .ant-input-number-handler-wrap {
    display: none 
  }
  .fields{
    border-radius:5px;
  }
  .t-transactions {
    font-size: 25px;
    line-height: 30px;
    color: rgba(5, 31, 36, 0.3);
  }
  .t-transaction-id {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .ant-upload-list{
    display:none;
  }
  .ant-card {
    border-radius:10px;
    width: 100%;
    .info-edit-wrapper {
      margin-bottom: 40px;
    }
    .t-info {
      font-weight: 600;
      font-size: 20px;
      line-height: 24px;
      color: #051f24;
    }
    .t-transaction-key-field {
      font-size: 15px;
      line-height: 18px;
      color: rgba(5, 31, 36, 0.6);
    }
    .t-transaction-value {
      font-size: 16px;
      line-height: 19px;
      font-weight: 500;
      color: #051f24;
      margin: 10px;
      &.blue {
        color: #4da1ff;
      }
    }
    .key-value-wrapper {
      row-gap: 10px;
    }
    .edit-col {
      display: flex;
      align-items: center;
      column-gap: 20px;
    }
  }
  .sr-no-wrapper {
    position: relative;
    .srno-default-pop {
      position: absolute;
      background: #4aaf05;
      border: 2px solid #4aaf05;
      font-size: 12px;
      padding: 0 6px;
      top: -10px;
      left: 12px;
      color: #fff;
      border-radius: 100px;
    }
  }

  .sub-routes-wrapper {
    column-gap: 8px;
    margin: 60px auto;
    text-align: center;
    .sub-route {
      padding: 8px;
      width: 100%;
      background: rgba(198, 198, 198, 0.6);
      color: rgba(5, 31, 36, 0.6);
      border-radius: 4px;
      cursor: pointer;
      font-weight: 600;
      &.active {
        background: #fff;
        border: 1px solid #eee;
        color: #4da1ff;
      }
    }
  }
  a {
    text-decoration: none;
    color: inherit;
  }
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;
    &.back{
      color: #9ab1b6;
    }
    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.discard {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
  & .live-preview-poll{
      height:100%;
      float: right;
      background: #1890ff;
      color: #fff;
      font-weight: 500;
      border-radius: 5px;
      padding: 5px 10px;
      &:disabled{
        color:rgba(0, 0, 0, 0.25);
        background:#f5f5f5;
      }
  }
`;

type PollProps = {
  pollFormData?: GetPollQuery["teamPoll_by_pk"];
}

const AddPoll = ({ pollFormData }: PollProps): JSX.Element => {

  const [form] = Form.useForm();
  const history = useHistory();
  const { state } = useLocation<any>();
  
  //team listing
  const [getAllTeams, { data: teams, loading: teamOptionLoading }] = useGetTeamsByStatusIdLazyQuery({
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    variables: {
      statusId: ["APPROVED", "PUBLISHED", "ACTIVE"]
    }
  });
  
  //group listing
  const [getAllGroups, { data: groups, loading: groupOptionLoading }] = useGetAllGroupsByStatusIdLazyQuery({
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    variables: {
      statusID:[1]
    }
  });

  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  const { data: pollTypes } = useGetTeamPollTypesQuery();
  const { data: pollPlatforms } = useGetTeamPollPlatformsQuery();
  const { data: pollsCount, loading:pollsCountLoading } = useGetPollsCountQuery({
    fetchPolicy:"network-only",
    notifyOnNetworkStatusChange:true
  })
  const { data: teamStatuses, loading: teamStatusLoading } = useGetTeamStatusesQuery();
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  const isTeamUser = isIamTeamUser();


  const [formData, setFormData] = React.useState<Poll | object>({});
  const [isVisible, setIsVisible] = React.useState<boolean>(false);
  const [notesList, setNotesList] = React.useState<any[]>([]);
  const [userData, setUserData] = React.useState<any>({});
  const [editIndex, setEditIndex] = React.useState<number>(-1);
  const [checkStatus, setCheckStatus] = React.useState<boolean>(false);


  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [pollOptions, setPollOptions] = React.useState<TeamPollOption[]>([
    {
      description: '',
      uniqueKey: uuidv4(),
      orderBy: 1
    },
    {
      description: '',
      uniqueKey: uuidv4(),
      orderBy: 2
    },
  ]);

  const updateState = (field:any) => {
    if(field?.category === "GROUPS" && !groups?.allGroups?.nodes){
      getAllGroups();
    }
    setFormData({ ...form.getFieldsValue(true) })
  }

  function pollOptionsValidator(polls: TeamPollOption[], type: string): boolean {
    let result = polls.every((option) => {
      if (type === "image") {
        if (option?.description.length > 0 && isValidHttpUrl(option?.imageUrl) && typeof option?.orderBy === "number") {
          return true
        } else {
          return false
        }
      } else {
        if (option?.description.length > 0 && typeof option?.orderBy === "number") {
          return true
        } else {
          return false
        }
      }
    })
    return result;
  }

  const onBeforeSave = async (formValues: Poll) => {
    form.validateFields()
      .then(async () => {
        if (['SUBMITTED', 'APPROVED', 'DISCONTINUED', 'SUSPENDED'].includes(formValues.status)) {
          setIsVisible(true);
          setCheckStatus(true)
        }
        else {
          if (checkStatus) {
            setCheckStatus(false)
          }
          onSave(formValues)
        }
      })
      .catch(e => {
        toast.info('Please fill all the mandatory fields!')
        console.log(e, 'e')
      })
  };

  const onSave = async (formValues: Poll) => {
    setConfirmLoading(true);

    //Validating Poll options
    const result = pollOptionsValidator(pollOptions, formValues?.teamPollContentType);
    pollOptions.forEach((pollOption, index: number, array) => {
      const { uniqueKey, ...remainingOptions } = pollOption;
      array[index] = {
        ...remainingOptions
      }
    });

    if (!result) {
      setConfirmLoading(false);
      return toast.warning("Please fill all the Poll option fields");
    }

    let contructNotes: any[] = [];

    if (notesList?.length) {
      contructNotes = notesList?.map(note => ({
        "createdDate": note?.addedDate,
        "name": note?.name,
        "notes": note?.notes,
        "addedBy": note?.addedby
      }))
    }

    if (pollFormData) {
      let updatePayloadProps = {
        ...formValues,
        id: pollFormData?.id,
        minJoinBalance: parseInt(formValues.minJoinBalance as unknown as string),
        allowedParticipants: parseInt(formValues.allowedParticipants as unknown as string),
        teamId: isNaN(parseInt(formValues.teamId as unknown as string)) ? pollFormData?.teamId : parseInt(formValues.teamId as string),
        startAt: formValues?.startAt.seconds('0o0').toISOString(),
        endAt: formValues?.endAt.seconds('0o0').toISOString(),
        displayStartAt: formValues?.displayStartAt.seconds('0o0').toISOString(),
        displayEndAt: formValues?.displayEndAt.seconds('0o0').toISOString(), 
        pollPlatform: formValues.pollPlatform,
        teamPollOptions: pollOptions,
        notes: contructNotes,
        ...(formValues?.category && {type: formValues?.category}),
        ...(formValues?.category === "GROUPS" ? {
          groupId: formValues?.groupId,
        } : {
          teamId: isNaN(parseInt(formValues.teamId as unknown as string)) ? pollFormData?.teamId : parseInt(formValues.teamId as string),
        }),
      } as PayloadProps;

      await updatePollApi(updatePayloadProps)
        .then((apiResult) => {
          setConfirmLoading(false)
          if (apiResult?.data?.statusCode !== "200") {
            toast.error("Unable to save the poll");
          } else {
            toast.success(`Poll saved successfully`);
            setTimeout(() => {
              history.push(`/polls/${formValues.category === "PLATFORM" ? formValues.teamId : formValues.groupId}/${pollFormData.id}`)
            }, 1500);
          }
        }).catch((err) => {
          console.log(err)
          toast.error("Request failed. Something Went wrong!");
          setConfirmLoading(false)
        });

    } else {
      let createPayloadProps = {
        ...formValues,
        status: "SUBMITTED",
        teamPollOptions: pollOptions,
        minJoinBalance: parseInt(formValues.minJoinBalance as unknown as string),
        allowedParticipants: parseInt(formValues.allowedParticipants as unknown as string),
        startAt: formValues?.startAt.seconds('0o0').toISOString(),
        endAt: formValues?.endAt.seconds('0o0').toISOString(),
        displayStartAt: formValues?.displayStartAt.seconds('0o0').toISOString(),
        displayEndAt: formValues?.displayEndAt.seconds('0o0').toISOString(),
        pollPlatform: formValues.pollPlatform,
        notes: contructNotes,
        ...(formValues?.category && {type: formValues?.category,}),
        ...(formValues?.category === "GROUPS" ? {
          groupId: formValues?.groupId,
        } : {
          teamId: parseInt(formValues.teamId as unknown as string),
        }),
      } as PayloadProps;

      await createPollApi(createPayloadProps)
        .then((apiResult) => {
          setConfirmLoading(false)
          if (apiResult?.data?.statusCode !== "200") {
            toast.error("Unable to save the poll");
          } else {
            toast.success(`Poll saved successfully`);
            let pollId = apiResult?.data?.data?.[0]?.createTeamPoll?.teamPoll?.id
            if (pollId) {
              setTimeout(() => {
                history.push(`/polls/${formValues.category === "PLATFORM" ? formValues.teamId : formValues.groupId}/${pollId}`)
              }, 1500);
            } else {
              setTimeout(() => {
                history.push("/polls");
              }, 1500);
            }
          }
        }).catch((err) => {
          console.log(err)
          toast.error("Request failed. Something Went wrong!");
          setConfirmLoading(false)
        });
    }
  };

  React.useEffect(() => {
    if (pollFormData?.teamPollOptions) {
      let pollOptionsCopy: TeamPollOption[] = JSON.parse(JSON.stringify(pollFormData?.teamPollOptions));
      pollOptionsCopy.forEach((pollOption, index: number, array) => {
        array[index] = {
          ...pollOption,
          uniqueKey: uuidv4()
        }
      });
      setPollOptions([...pollOptionsCopy])
    }
    if (pollFormData?.teamPollNotes?.length) {
      let contructNotesList = pollFormData?.teamPollNotes?.map(note => ({
        name: note?.name,
        notes: note?.notes,
        addedDate: moment(note?.createdDate.concat('z')),
        addedby: note?.addedBy,
        id: note?.id
      }))
      setNotesList(contructNotesList)
    }
    if(pollFormData?.type === "GROUPS"){
      getAllGroups()
    }
  }, [pollFormData]);

  React.useEffect(() => {
    if (!pollFormData) {
      form.setFieldsValue({
        status: "BLANK", orderBy: (pollsCount?.teamPoll_aggregate?.aggregate?.count ?? 0) + 1, canMultiselect: false, minJoinBalance: 0
      })
    }
  }, [pollsCount]);

  React.useEffect(() => {
    if (!isTeamUser) {
      getAllTeams();      
    } else {
      form.setFieldsValue({ teamId: teamSelectionContext.team.id })
    }
    currentSession().then(res => {
      setUserData(res.payload)
    })
  }, [])

  React.useEffect(() => {
    if (checkStatus) {
      onSave(form.getFieldsValue())
    }
  }, [notesList])

  const deleteNotes = () => {
    let filteredNotes = notesList?.filter((list, index) => index !== editIndex)
    setNotesList(filteredNotes)
    handleCancelNote()
  }

  const handleCancelNote = () => {
    setIsVisible(false)
    if (editIndex > -1) {
      setEditIndex(-1)
    }
  }

  const onNotesOk = (val: any, editdata: any) => {

    if (Object.keys(editdata).length > 0 && editIndex > -1) {
      notesList[editIndex] = {
        ...notesList[editIndex],
        name: val?.name,
        notes: val?.note,
      }
      setNotesList(notesList)
    }
    else {
      setNotesList([
        ...notesList, {
          name: val?.name,
          notes: val?.note,
          addedDate: moment(),
          addedby: userData?.name,
        }
      ])
    }
    handleCancelNote()
  }

  const openEditNote = (obj: any, key: number) => {
    setEditIndex(key - 1);
    setIsVisible(true);
  }

  const editNoteData = useMemo(() => {
    if (editIndex > -1) {
      return {
        name: notesList[editIndex]?.name,
        note: notesList[editIndex]?.notes,
        id: notesList[editIndex]?.id,
      }
    }
    return {}
  }, [editIndex])


  //poll status validation
  const statusValidations = useMemo(() => {
    return pollFilterStatus(teamStatuses?.teamStatus, pollFormData?.status, pollFormData?.displayStartAt?.concat('z'))
  }, [pollFormData, teamStatuses])


  //find disable components
  const disabledComponents = useCallback((key: string) => {
    if (!pollFormData) return false
    let validate = statusValidations?.disableComponents?.includes(key)
    return statusValidations?.except ? validate : !validate
  }, [statusValidations])


  //disable start date
  const isDisableStartDate = useCallback((date) => {
    return date && moment(date).isBefore(moment(), 'day')
  }, [form])


  //disable end date
  const isDisableEndDate = useCallback((date, key) => {
    if (!form.getFieldValue(key)) {
      return true
    }
    let checkDate: MomentInput = moment(form.getFieldValue(key)).isBefore(moment(), 'day') ? moment() : moment(form.getFieldValue(key))
    return date && moment(date).isBefore(checkDate, 'day')
  }, [form])


  //get team Details by id
  const getTeamById = (teamId: number) => teams?.team.find(team => team?.id === teamId)

  //status validation for Start Date Time
  const startDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`Start Date Time is required!`));
    }
    if (getFieldValue('endAt')) {
      if (!value.isBefore(getFieldValue('endAt'))) {
        return Promise.reject(new Error(`Start Date Time should be lesser than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('displayStartAt')) {
      if (!value.isAfter(getFieldValue('displayStartAt'))) {
        return Promise.reject(new Error(`Start Date Time should be greater than Display Start Date Time(${getFieldValue('displayStartAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isAfter(moment(team?.activeStartAt?.concat('z')))) {
        return Promise.reject(new Error(`Start Date Time should be greater than Team Start Date Time(${moment(team?.activeStartAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for End Date Time
  const endDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`End Date Time is required!`));
    }
    if (getFieldValue('startAt')) {
      if (!value.isAfter(getFieldValue('startAt'))) {
        return Promise.reject(new Error(`End Date Time should be greater than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('displayEndAt')) {
      if (!value.isBefore(getFieldValue('displayEndAt'))) {
        return Promise.reject(new Error(`End Date Time should be lesser than Display End Date Time(${getFieldValue('displayEndAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isBefore(moment(team?.activeEndAt?.concat('z')))) {
        return Promise.reject(new Error(`End Date Time should be lesser than Team End Date Time(${moment(team?.activeEndAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for Display Start Date Time
  const displayStartDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`Display Start Date Time is required!`));
    }
    if (getFieldValue('startAt')) {
      if (!value.isBefore(getFieldValue('startAt'))) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isAfter(moment(team?.displayStartAt?.concat('z')))) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Team Start Date Time(${moment(team?.displayStartAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  //status validation for Display End Date Time
  const displayEndDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`Display End Date Time is required!`));
    }
    if (getFieldValue('endAt')) {
      if (!value.isAfter(getFieldValue('endAt'))) {
        return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    if (getFieldValue('teamId')) {
      let team = getTeamById(getFieldValue('teamId'));
      if (!value.isBefore(moment(team?.displayEndAt?.concat('z')))) {
        return Promise.reject(new Error(`Display End Date Time should be lesser than Team End Date Time(${moment(team?.displayEndAt?.concat('z')).format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  // decide props by category(GROUPS OR PLATFORM)
  const getPropsByType = () =>{
    let type = form.getFieldValue('category');
    const returnValues = (teamValue:any,groupValue:any) => type === "PLATFORM" ? teamValue : groupValue;
    return{
      label:`${returnValues("Team","Group")} Name`,
      name:returnValues("teamId","groupId"),
      initialValue:returnValues(pollFormData?.teamId,pollFormData?.groupId),
      rules:{
        message:`${returnValues("Team","Group")} Name is required!`
      },
      toFilter:returnValues("name","groupName"),
      placeholder:`Select ${returnValues("Team","Group")}`,
      disabled: () => returnValues(((disabledComponents('teamId') || state?.teamId) ? true : isTeamUser), disabledComponents('groupId')),
      loading:returnValues(teamOptionLoading,groupOptionLoading),
      options: returnValues(teams?.team, groups?.allGroups?.nodes)
    }
  }

  const getTeamWalletById = (teamId:number) =>{
    return getTeamById(teamId)?.wallets?.[0]?.coinSymbol;
  }

  const checkCurrencyType = (val:string) =>{
    return ['SPN','RWRD'].includes(val)
  }

  return (
    <AddPollWrapper>
      <Row gutter={[0, 32]}>
        <BreadcrumbComp
          backPath='/polls'
          cta={<>
            <Button type="primary" style={{ marginRight: "16px" }} onClick={form.submit}>
              {pollFormData ? `Updat${confirmLoading ? 'ing' : 'e'}` : `Sav${confirmLoading ? 'ing' : 'e'}`}
            </Button>
            <Button
              onClick={() => {
                if (pollFormData) {
                  history.push(`/polls/${pollFormData.id}`)
                } else if (state?.teamId) {
                  history.push(`/team/${state.teamId}`)
                } else {
                  history.push('/polls')
                }
              }}
            >
              Discard
            </Button>
          </>}
          breadCrumbs={[{ name: "Polls", url: "/polls" }, { name: pollFormData ? "Edit Poll" : "Create New Polls", url: `/poll/edit/${pollFormData?.id}` }]}
        />
        <Form onFinish={onBeforeSave} form={form} preserve={false} onValuesChange={updateState}
          initialValues={
            pollFormData ? {
              ...pollFormData,
              teamId: pollFormData?.teamId,
              startAt: moment(pollFormData?.startAt + "z"),
              endAt: moment(pollFormData?.endAt + "z"),
              displayStartAt: moment(pollFormData?.displayStartAt + "z"),
              displayEndAt: moment(pollFormData?.displayEndAt + "z"),
              category:pollFormData?.type ?? "PLATFORM",
              groupId:pollFormData?.groupId,
            } : {
              teamId: state?.teamId ? state?.teamId : null,
              category: state?.teamId ? "PLATFORM" : null,
            }
          }>
          <Card>
            <TitleCta
              title={`${pollFormData ? 'Edit' : state?.teamId ? "Team > " + getTeamById(state?.teamId)?.name + " > Create Poll " : "Poll"} Details`}
            />

            <Row gutter={[24, 12]}>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Poll Title"
                  name="title"
                  rules={[
                    {
                      required: true,
                      message: 'Poll Title is required!',
                    },
                  ]}
                  inputElement={
                    <Input
                      type="text"
                      disabled={disabledComponents('title')}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Category"
                  name="category"
                  // initialValue={pollFormData?.team?.name ? pollFormData?.team?.name : state?.teamName ?? ""}
                  // state?.teamName
                  rules={[
                    {
                      required: true,
                      message: 'Category is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        form.setFieldsValue({ 
                          category : e,
                          groupId : null,
                          teamId : null,
                          ...(!checkCurrencyType(form.getFieldValue('currency')) && {currency:null})
                        });
                      }}
                      toStore="value"
                      options={categoryOptions}
                      toFilter="label"
                      placeholder="Category"
                      disabled={disabledComponents('category') || state?.teamId}
                    />
                  }
                />
              </Col>
             { 
             form.getFieldValue('category') &&
             <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label={getPropsByType().label}
                  name={getPropsByType().name}
                  initialValue={getPropsByType().initialValue}
                  // state?.teamName
                  rules={[
                    {
                      required: true,
                      message: getPropsByType().rules.message,
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        let key = getPropsByType().name
                        form.setFieldsValue({ 
                          [key]: e,
                          ...(!checkCurrencyType(form.getFieldValue('currency')) && {currency:null})
                         });
                      }}
                      toStore="id"
                      options={getPropsByType().options ?? []}
                      loading={getPropsByType().loading ?? false}
                      toFilter={getPropsByType().toFilter}
                      placeholder={getPropsByType().placeholder}
                      disabled={getPropsByType().disabled()}
                      searchable
                    />
                  }
                />
              </Col>
              }
              {
                pollFormData &&
                <Col xs={24} sm={12} md={8}>
                  <LabelInput
                    label="Poll Status"
                    name="status"
                    inputElement={
                      <SelectElement
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                          form.setFieldsValue({ status: e })
                        }
                        loading={teamStatusLoading}
                        options={teamStatusLoading ? [] : statusValidations.options}
                        toFilter="status"
                        placeholder="Status"
                        disabled={disabledComponents('status')}
                      />
                    }
                  />
                </Col>
              }
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Poll Type"
                  name="typeId"
                  rules={[
                    {
                      required: true,
                      message: 'Poll Type is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        form.setFieldsValue({ typeId: e });
                      }}
                      options={pollTypes?.teamPollType ?? []}
                      toFilter="type"
                      placeholder="Poll Type"
                      disabled={disabledComponents('typeId')}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Poll Platform"
                  name="pollPlatform"
                  rules={[
                    {
                      required: true,
                      message: 'Poll Platform is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        form.setFieldsValue({ pollPlatform: e });
                      }}
                      options={pollPlatforms?.teamPollPlatform ?? []}
                      toFilter="type"
                      placeholder="Poll Platform"
                      disabled={disabledComponents('typeId')}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Start Date Time"
                  name={'startAt'}
                  rules={[
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        return startDateTimeValidation(getFieldValue, value)
                      },
                    }),
                  ]}
                  inputElement={
                    <DatePicker
                      showTime
                      className='datepicker'
                      // defaultValue={[
                      // moment(teamDetails?.activeStartAt),
                      // moment(teamDetails?.activeEndAt),
                      //]}
                      style={{ width: "100%" }}
                      disabledDate={(value) => isDisableStartDate(value)}
                      disabled={disabledComponents('startAt')}
                      format={"DD-MM-YYYY hh:mm"}
                      showNow={false}
                      showSecond={false}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="End Date Time"
                  name={'endAt'}
                  rules={[
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        return endDateTimeValidation(getFieldValue, value)
                      },
                    }),
                  ]}
                  inputElement={
                    <DatePicker
                      showTime={Boolean(form.getFieldValue('startAt'))}
                      // defaultValue={[

                      // moment(teamDetails?.activeStartAt),
                      // moment(teamDetails?.activeEndAt),
                      //]}
                      className='datepicker'
                      style={{ width: "100%" }}
                      disabled={disabledComponents('endAt')}
                      disabledDate={(value) => isDisableEndDate(value, 'startAt')}
                      format={"DD-MM-YYYY hh:mm"}
                      
                      showNow={false}
                      showSecond={false}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Display Start Date Time"
                  name={'displayStartAt'}
                  rules={[
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        return displayStartDateTimeValidation(getFieldValue, value)
                      },
                    }),
                  ]}
                  inputElement={
                    <DatePicker
                      showTime
                      className='datepicker'
                      // defaultValue={[

                      // moment(teamDetails?.activeStartAt),
                      // moment(teamDetails?.activeEndAt),
                      //]}
                      style={{ width: "100%" }}
                      disabled={disabledComponents('displayStartAt')}
                      disabledDate={(value) => isDisableStartDate(value)}
                      format={"DD-MM-YYYY hh:mm"}
                    
                      showNow={false}
                      showSecond={false}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Display End Date Time"
                  name={'displayEndAt'}
                  rules={[
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        return displayEndDateTimeValidation(getFieldValue, value)
                      },
                    }),
                  ]}
                  inputElement={
                    <DatePicker
                      showTime={Boolean(form.getFieldValue('displayStartAt'))}
                      // defaultValue={[

                      // moment(teamDetails?.activeStartAt),
                      // moment(teamDetails?.activeEndAt),
                      //]}
                      className='datepicker'
                      style={{ width: "100%" }}
                      disabled={disabledComponents('displayEndAt')}
                      disabledDate={(value) => isDisableEndDate(value, 'displayStartAt')}
                      format={"DD-MM-YYYY hh:mm"}
                      showNow={false}
                      showSecond={false}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Currency Symbol"
                  name="currency"
                  // rules={[
                  //   {
                  //     required: true,
                  //     message: 'Currency Symbol is required!',
                  //   },
                  // ]}
                  inputElement={
                    <SelectElement
                      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                        form.setFieldsValue({ currency: e });
                      }}
                      options={[
                        {
                          currencyType: 'SPN',
                        },
                        {
                          currencyType: 'RWRD',
                        },
                        ...(form.getFieldValue('category') === "PLATFORM" ? [{
                          currencyType: form.getFieldValue("teamId") ? getTeamWalletById(form.getFieldValue("teamId")) : 'Choose your team',
                        }] : [])
                      ]}
                      toFilter="currencyType"
                      placeholder="Currency Type"
                      disabled={disabledComponents('currency')}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="Minimum Join Balance"
                  name="minJoinBalance"
                  className='field-input'
                  // rules={[
                  //   {
                  //     required: true,
                  //     message: 'Min. Joining Balance is required!',
                  //   },
                  // ]}
                  inputElement={
                    <InputNumber
                      className='field'
                      type="number"
                      min={0}
                      disabled={disabledComponents('minJoinBalance')}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8}>
                <LabelInput
                  label="No of Participants Allowed"
                  className='field-input'
                  name="allowedParticipants"
                  // rules={[
                  //   {
                  //     required: true,
                  //     message: 'No of Allowed Participants is required!',
                  //   },
                  // ]}
                  inputElement={
                    <InputNumber
                      className='field'
                      type="number"
                      min={0}
                      disabled={disabledComponents('allowedParticipants')}
                    />
                  }
                />
              </Col>
              <Col xs={24} sm={12} md={8} style={{ display: 'flex', justifyContent: 'start', marginTop: '40px' }}>
                <Form.Item
                  name="displayResult"
                  labelAlign="left"
                  colon={false}
                  valuePropName="checked"
                >
                  <Checkbox defaultChecked={false} disabled={disabledComponents('displayResult')}>
                    Display results at the end of poll
                  </Checkbox>
                </Form.Item>
              </Col>
              <Col xs={24} sm={12} style={{ display: 'flex' }}>
                <LabelInput
                  label="Poll Display Priority"
                  name="orderBy"
                  rules={[
                    {
                      required: true,
                      message: 'Poll Display Priority is required!',
                    },
                  ]}
                  inputElement={
                    <InputNumber
                      type={'number'}
                      className='fields'
                      disabled={pollsCountLoading || disabledComponents('orderBy')}
                      min={1}
                      max={(pollsCount?.teamPoll_aggregate?.aggregate?.count ?? 0) + 1}
                    />
                  }
                />
              </Col>
            </Row>
            <Divider />
            <PollTemplate form={form} disabled={disabledComponents('pollTemplate')} />
            <Divider />
            <TitleCta
              title="Poll Question"
            // cta={
            //   <Button icon={<PlusOutlined />} onClick={() => onAddNewPollQuestion()} className="add-new-button" >Add Question</Button>
            // }
            />
            <Col>
              <>
                <Card>
                  <Button
                    disabled={
                      pollOptions?.filter(_ => _?.description)?.length < 2 ||
                      !form.getFieldValue('question')
                    }
                    className='live-preview-poll'
                  >
                    <Link
                      to={{
                        pathname: `${config.sportzchainFanPageURL}/poll-preview?formValues=${JSON.stringify({ ...form.getFieldsValue(), teamPollOptions: pollOptions, teamDetails:getTeamById(form.getFieldValue('teamId')) })}`,
                      }}
                      target="_blank"
                    >
                      Poll Preview
                    </Link>
                  </Button>
                  {
                    disabledComponents('pollQuestions') ? (
                      <Row>
                        <Col span={24}>
                          <Label>Poll Question</Label>
                          <TextContent>{form.getFieldValue('question')}</TextContent>
                        </Col>
                        <Divider />
                        <Col span={24}>
                          <Label>Poll Description</Label>
                          <TextContent>{form.getFieldValue('description')}</TextContent>
                        </Col>
                        <Divider />
                        {pollOptions && pollOptions.length > 0 && (
                          <Col span={24} style={{ marginTop: '16px' }}>
                            <OptionsView options={pollOptions ?? []} />
                          </Col>
                        )}
                        <Col style={{ marginTop: '32px' }}>
                          <Row gutter={[24, 0]}>
                            <Col>
                              <Switch
                                disabled={true}
                                className="toggle-if-multiple"
                                defaultChecked={form.getFieldValue('canMultiselect')}
                              />
                            </Col>
                            <Text className="t-allow-multiple" strong>
                              Allow Multiple
                            </Text>
                          </Row>
                        </Col>
                      </Row>
                    ) : (
                      <PollQuestionCard
                        form={form}
                        pollOptions={pollOptions}
                        setPollOptions={setPollOptions}
                      />
                    )
                  }
                </Card>
              </>
            </Col>
            <Col>
              <Card style={{ width: "100%", marginTop: 32 }}>
                <Row justify="space-between" gutter={[24, 24]}>
                  <h3 className="bold">Notes</h3>
                  <Button
                    type="link"
                    icon={<PlusOutlined />}
                    onClick={() => {
                      setIsVisible(true)
                    }}
                  >
                    Add New Note
                  </Button>
                </Row>
                <NoteModal
                  handleDelete={deleteNotes}
                  EditData={editNoteData}
                  ModalVisible={isVisible}
                  handleOk={(val: any, editdata: any) => onNotesOk(val, editdata)}
                  handleCancel={handleCancelNote}
                  readOnly={editNoteData?.id}
                />
                <Note
                  JsonData={notesList}
                  openEdit={(obj: any, key: number) => openEditNote(obj, key)}
                />

              </Card>
            </Col>
          </Card>
        </Form>
      </Row>
    </AddPollWrapper >
  );
};




export default AddPoll;
