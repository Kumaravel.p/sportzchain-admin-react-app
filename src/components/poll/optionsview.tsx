import { Button, Card, Switch, Row, Col } from 'antd';
import styled from 'styled-components/macro';



const PollOptionWrapper = styled.div`
padding:16px;
border-radius:5px;
 background: #eee;
  .optionImage{
    width:80%;
    max-height: 240px;
    object-fit: contain;
    min-height: 240px;
  }
 `

const Text = styled.p`
    font-size:16px;
    margin-bottom:0px;
    font-weight:500;
    color:gray;
`;

interface optionsType {
    options?: {
        __typename?: "teamPollOption" | undefined;
        id?: number;
        description?: string;
        imageUrl?: string | null | undefined;
        orderBy?: number | null | undefined;
    }[]
}

const OptionsView = ({ options }: optionsType): JSX.Element => (
    <Row gutter={[24, 24]} >
        {
            options && options.map((option, index) => {
                return (
                    <Col span={12} key={index} >
                        <PollOptionWrapper>
                            <Row justify='space-between'>
                                <Col>
                                    <Text>{option?.description}</Text>
                                </Col>
                                <Col>
                                    <Row>
                                        <Text style={{ marginRight: "12px" }}>Position</Text>
                                        <Text>{option?.orderBy ?? index + 1}</Text>
                                    </Row>
                                </Col>
                            </Row>
                            {option?.imageUrl && option?.imageUrl !== "null" &&
                                <Row style={{ marginTop: "16px", width: "100%" }} align="middle" justify="center">
                                    <img className='optionImage' src={option?.imageUrl} alt="Option" />
                                </Row>
                            }
                        </PollOptionWrapper>
                    </Col>
                )
            })
        }
    </Row>
)


export default OptionsView