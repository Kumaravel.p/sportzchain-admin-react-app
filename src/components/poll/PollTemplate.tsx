import styled from 'styled-components/macro';
import { ReactComponent as PollTemplateImage } from 'assets/poll-template/poll-template-image.svg';
import { ReactComponent as PollTemplateText } from 'assets/poll-template/poll-template-text.svg';
import Title from 'antd/lib/typography/Title';
import { Col, FormInstance, Radio, Row } from 'antd';
import Text from 'antd/lib/typography/Text';
import FormInput from 'components/form-elements/FormInput';
import { Label } from 'components/common/atoms';
import { Form } from 'antd';


const PollTemplateWrapper = styled.div`
 .ant-radio-wrapper{
    align-items:start;
  }
`;

interface PollTemplateProps {
  form?: FormInstance;
  value?: string;
  disabled?:boolean;
}

const PollTemplate = ({ form, value, disabled=false }: PollTemplateProps): JSX.Element => {
  return (
    <PollTemplateWrapper>
      <Title level={3}>Poll Template</Title>
      <Form.Item name={"teamPollContentType"} rules={[
        {
          required: true,
          message: 'Poll Type is required!',
        },
      ]}>
        <Radio.Group defaultValue={value ?? null} disabled={disabled || (value ? true : false)}>
          <Row gutter={[124, 12]}>
            <Col span={12} >
              <Label style={{ margin: "12px 0px 20px 24px" }}>Image</Label>
              <Radio value="image" className="pollTemplateSelect">
                <PollTemplateImage />
              </Radio>
            </Col>
            <Col span={12}>
              <Label style={{ margin: "12px 0px 20px 24px" }}>Text</Label>
              <Radio value="text">
                <PollTemplateText />
              </Radio>
            </Col>
          </Row>
        </Radio.Group>
      </Form.Item>
    </PollTemplateWrapper >
  );
};

export default PollTemplate;
