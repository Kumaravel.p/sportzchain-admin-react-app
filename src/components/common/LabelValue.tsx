import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';

import FlexColumnWrapper from './wrappers/FlexColumnWrapper';

const LabelValueWrapper = styled(FlexColumnWrapper)`
  .t-user-key-field {
    font-size: 15px;
    line-height: 18px;
    color: rgba(5, 31, 36, 0.6);
    margin-bottom: 6px;
  }
  .t-user-value {
    font-size: 16px;
    font-weight: 600;
    line-height: 19px;
    color: #051f24;
  }
`;

interface LabelValueProps {
  field: string;
  value: string | number | React.ReactNode;
}

const LabelValue = ({ field, value }: LabelValueProps): JSX.Element => {
  return (
    <LabelValueWrapper>
      <Text className="t-user-key-field">{field}</Text>
      <Text className="t-user-value">{value}</Text>
    </LabelValueWrapper>
  );
};

export default LabelValue;
