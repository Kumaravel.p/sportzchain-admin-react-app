import React from 'react';
import styled from 'styled-components/macro';
import {
  ContentState,
  convertToRaw,
  EditorState,
  convertFromHTML,
} from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { convertToHTML } from 'draft-convert';

const TextEditorInputWrapper = styled.div`
  .wrapper-class {
    /* padding: 1rem; */
    /* border: 1px solid #ccc; */
  }
  .editor-class {
    background-color: #efefef;
    padding: 1rem;
    border: 1px solid #ccc;
  }
  .toolbar-class {
    background-color: lightgray;
  }
`;

interface TextEditorInputProps {
  value?: string;
  editData?: string;
  view?:boolean
  setValue: React.Dispatch<React.SetStateAction<string>>;
}

const TextEditorInput = ({
  value,
  setValue,
  editData,
  view = false
}: TextEditorInputProps): JSX.Element => {
  const [editorState, setEditorState] = React.useState(() =>
    EditorState.createEmpty()
  );

  const handleEditorChange = (state: EditorState) => {
    setEditorState(state);
    convertContentToHTML();
  };

  const convertContentToHTML = () => {
    let currentContentAsHTML = convertToHTML(editorState.getCurrentContent());
    setValue(currentContentAsHTML);
  };

  React.useEffect(() => {
    let newEditorData = editData ? EditorState.createWithContent(
      ContentState.createFromBlockArray(convertFromHTML(editData)?.contentBlocks, convertFromHTML(editData)?.entityMap))
      : editorState;
    setEditorState(newEditorData);
    convertContentToHTML();
  }, [editData])

  return (
    <TextEditorInputWrapper>
      <Editor
        defaultEditorState={value ? EditorState.createWithContent(
          ContentState.createFromBlockArray(convertFromHTML(value)?.contentBlocks, convertFromHTML(value)?.entityMap))
          : editorState
        }
        editorState={editorState}
        onEditorStateChange={view ? ()=>false : handleEditorChange }
        wrapperClassName="wrapper-class"
        editorClassName="editor-class"
        toolbarClassName="toolbar-class"
      />
    </TextEditorInputWrapper>
  );
};

export default TextEditorInput;
