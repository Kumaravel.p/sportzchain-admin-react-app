import { css, createGlobalStyle } from 'styled-components/macro';

import { cssVariables } from './cssVariables';

// antd global styles
import 'antd/dist/antd.css';

// react-toastify global styles
import 'react-toastify/dist/ReactToastify.css';

const globalCss = css`
  *,
  *::before,
  *::after {
    margin: 0;
    padding: 0;
    box-sizing: inherit;

    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    user-select: none;
    -webkit-user-select: none; /* disable text select */
    -webkit-touch-callout: none; /* disable callout, image save panel (popup) */
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    -webkit-tap-highlight-color: transparent; /* "turn off" link highlight */
  }

  body {
    box-sizing: border-box;
    font-family: 'Inter', -apple-system, BlinkMacSystemFont, 'Segoe UI',
      'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans',
      'Helvetica Neue', sans-serif;
    background: var(--main-background-color);
    color: var(--main-text-color);
  }

  h1,
  h2,
  h3 {
    text-rendering: optimizeLegibility;
  }
  iframe {
    display: none;
  }

  a:focus {
    outline: 0; // Firefox (remove border on link click)
  }

  // antd table styles

  .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
    background: transparent;
  }

  .ant-table-content {
    .ant-table-thead {
      .ant-table-cell {
        font-weight: 500;
        font-size: 14px;
        line-height: 17px;
        color: #9ab1b6;
      }
    }
    .ant-table-row {
      font-size: 16px;
      cursor: pointer;
      line-height: 19px;
      font-weight: 500;
      color: #051f24;
    }
  }
  .table-user-avatar {
    width: 44px;
    height: 44px;
    border-radius: 10px;
  }
  .t-table-status {
    font-weight: normal;
    font-size: 16px;
    line-height: 19px;
    color: #07697d;
  }

  // ant dropdown
  .ant-dropdown-trigger {
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.4px;
    color: #9c9c9c;
    font-family: 'Montserrat', sans-serif;
    padding: 20px;
    display: flex;
    align-items: center;
  }
`;

const GlobalStyles = createGlobalStyle`
  ${cssVariables}
  ${globalCss};
`;

export { GlobalStyles };
