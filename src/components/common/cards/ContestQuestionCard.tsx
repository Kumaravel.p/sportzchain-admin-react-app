import { Button, Col, FormInstance, Input, Row, Switch } from 'antd';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import LabelInput from '../LabelInput';
import LabelValue from '../LabelValue';
import ContestOptionCard from './ContestOptionCard';
import PollOptionCard from './PollOptionCard';
import * as React from 'react';

const ContestQuestionCardWrapper = styled.div`
  .add-option-button {
    padding: 20px 0;
    height: 100%;
  }
  .toggle-if-multiple {
    margin: 20px 0;
  }
  .t-allow-multiple {
    font-size: 16px;
    line-height: 150%;
    color: #0c2146;
  }
`;

interface ContestQuestionCardProps {
  contestQuestions: any;
  setContestQuestions: React.Dispatch<React.SetStateAction<any>>;
  form: FormInstance;
  index: number;
  view?: boolean;
}

const ContestQuestionCard = ({
  contestQuestions,
  setContestQuestions,
  form,
  view,
  index,
}: ContestQuestionCardProps): JSX.Element => {
  const onQuestionChange = (key: any, value: any) => {
    contestQuestions[index][key] = value;
    setContestQuestions(contestQuestions);
    console.log(contestQuestions, 'curr question');
  };

  const onAddNewContestOption = () => {
    const currentIndex = contestQuestions[index];
    console.log('add options click');
    currentIndex.teamContestAnswers?.data.push({
      text: '',
      imageUrl: '',
      isAnswer: false,
    });

    // creating new array to fix rerendering problems

    let updatedContests = [...contestQuestions];
    setContestQuestions(updatedContests);
  };

  console.log(contestQuestions, 'contest questions add');

  return (
    <ContestQuestionCardWrapper>
      {view ? (
        <LabelValue field="Question" value={contestQuestions[index]?.title} />
      ) : (
        <LabelInput
          label="Question"
          inputElement={
            <Input
              type="text"
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                onQuestionChange('title', e.target.value);
                form.setFieldsValue({ question: e.target.value });
              }}
              placeholder="Contest question"
            />
          }
        />
      )}

      <Row gutter={[16, 16]}>
        {(view
          ? contestQuestions[index].teamContestAnswers
          : contestQuestions[index].teamContestAnswers.data
        )?.map((_: any, i: number) => (
          <Col span={12} key={i}>
            <ContestOptionCard
              key={i}
              view={view}
              contestOptions={contestQuestions}
              setContestOptions={setContestQuestions}
              form={form}
              index={i}
              questionIndex={index}
            />
          </Col>
        ))}

        {!view && (
          <Col span={12}>
            <Button
              onClick={() => onAddNewContestOption()}
              className="add-option-button"
              block
              type="dashed"
            >
              Add Option
            </Button>
          </Col>
        )}
      </Row>
    </ContestQuestionCardWrapper>
  );
};

export default ContestQuestionCard;
