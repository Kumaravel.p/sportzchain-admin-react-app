import { Card, Col, Input, Row, Upload, Form, FormInstance } from 'antd';
import Checkbox from 'antd/lib/checkbox/Checkbox';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import * as React from 'react';
import { ReactComponent as UploadIcon } from 'assets/icons/upload.svg';
import { ReactComponent as DeleteIcon } from 'assets/icons/delete.svg';
import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';
// import { useGetPresignedUrlLazyQuery } from 'generated/graphql';
import FlexRowWrapper from '../wrappers/FlexRowWrapper';
import { UploadFile } from 'antd/lib/upload/interface';
import { setOptions } from 'react-chartjs-2/dist/utils';
import { ReactComponent as CheckIcon } from 'assets/icons/check-icon.svg';
const ContestOptionCardWrapper = styled(Card)`
  background: #eee;
  border-radius: 8px;
  margin-top: 0 !important;
  .upload-icon,
  .delete-icon {
    cursor: pointer;
  }
  .option-wrapper {
    align-items: center;
  }
`;

interface ContestOptionCardProps {
  form: FormInstance;
  index: number;
  questionIndex: number;
  contestOptions: any;
  view?: boolean;
  edit?: boolean;

  setContestOptions: React.Dispatch<React.SetStateAction<any>>;
}

const ContestOptionCard = ({
  form,
  index,
  contestOptions,
  setContestOptions,
  questionIndex,
  view,
  edit,
}: ContestOptionCardProps): JSX.Element => {

  const onDeleteOption = () => {
    console.log('delete');
    console.log(contestOptions, 'contestOptions');
    const currentQuestion = contestOptions[questionIndex];
    var answerOptions = currentQuestion?.teamContestAnswers?.data;
    answerOptions.splice(index, 1);
    const newContestOptions = [...contestOptions];
    setContestOptions(newContestOptions);
  };

  const onOptionChange = (key: any, value: any) => {
    if (!edit) {
      contestOptions[questionIndex]['teamContestAnswers']['data'][index][key] =
        value;
      setContestOptions(contestOptions);
    }
    if (edit) {
      console.log(contestOptions, 'contest options');
    }
  };

  return (
    <ContestOptionCardWrapper key={index}>
      <Row justify="space-between">
        <Col>
          <FlexRowWrapper className="option-wrapper">
            &nbsp;
            {view ? (
              <>
                {contestOptions[questionIndex]?.teamContestAnswers[index]
                  ?.isAnswer && <CheckIcon />}
                {contestOptions[questionIndex]?.teamContestAnswers[index]?.text
                  ? contestOptions[questionIndex]?.teamContestAnswers[index]
                    ?.text
                  : '-'}
              </>
            ) : (
              <>
                <Checkbox
                  onChange={(e) => onOptionChange('isAnswer', e.target.checked)}
                />{' '}
                <Input
                  type="text"
                  onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                    onOptionChange('text', e.target.value)
                  }
                  defaultValue={
                    contestOptions[questionIndex]?.teamContestAnswers[index]
                      ?.text
                  }
                  placeholder="Option"
                />
              </>
            )}
          </FlexRowWrapper>
        </Col>
        <Col>
          <Row gutter={[12, 12]}>
            <Col>
              <EditIcon />
            </Col>
            <Col>
              {!view && (
                <Form.Item name="imageUrl">
                  <Upload
                    customRequest={async ({ file, onProgress }) => {
                      onOptionChange(
                        'imageUrl',
                        await handleMediaUpload(
                          file as RcFile,
                          onProgress
                        )
                      );
                      form.setFieldsValue({
                        imageUrl: await handleMediaUpload(
                          file as RcFile,
                          onProgress
                        ),
                      });
                    }}
                  >
                    <UploadIcon className="upload-icon" />
                  </Upload>
                </Form.Item>
              )}
            </Col>
            <Col>
              {!view && (
                <DeleteIcon
                  onClick={() => onDeleteOption()}
                  className="delete-icon"
                />
              )}
            </Col>
          </Row>
        </Col>
      </Row>
    </ContestOptionCardWrapper>
  );
};

export default ContestOptionCard;
