import {
  Button,
  Card,
  Col,
  FormInstance,
  Input,
  Row,
  Upload,
  Form,
  InputNumber,
} from 'antd';
import { useRef } from 'react'
import Checkbox from 'antd/lib/checkbox/Checkbox';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import * as React from 'react';
import { ReactComponent as UploadIcon } from 'assets/icons/upload.svg';
import { ReactComponent as DeleteIcon } from 'assets/icons/delete.svg';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';
import { s3FileUpload } from "apis/storage";
import { toast } from 'react-toastify';
import config from 'config';

import type { TeamPollOption } from 'ts/interfaces/polls.inteface';



const PollOptionCardWrapper = styled(Card)`
  background: #eee;
  border-radius: 8px;
  margin-top: 0 !important;
  .optionImage{
    width: 80%;
    max-height: 240px;
    object-fit: contain;
    min-height: 240px;
  }
  .upload-icon,
  .delete-icon {
    cursor: pointer;
  }
  .optionDescription{
    font-weight:500;
  }
  .ant-upload.ant-upload-select-picture-card{
    width:32px;
    height:32px;
  }
  .ant-upload-list-picture-card-container{
    width:72px;
    height:72px;
  }
`;

interface PollOptionCardProps {
  form: FormInstance;
  index: number;
  pollOptions: TeamPollOption[];
  pollOption?: TeamPollOption;
  setPollOptions: React.Dispatch<React.SetStateAction<any>>;
}

const PollOptionCard = ({
  pollOptions,
  pollOption,
  setPollOptions,
  index,
  form,
}: PollOptionCardProps): JSX.Element => {


  const [optionImage, setOptionImage] = React.useState(pollOption?.imageUrl !== "null" ? pollOption?.imageUrl : null);
  const uploadRef = useRef(null)


  const onDeleteOption = (optionIndex: number) => {

    let pollOptionsCopy: TeamPollOption[] = JSON.parse(JSON.stringify(pollOptions));

    const optionsAfterDelete = pollOptionsCopy.filter(
      (_: TeamPollOption, i: number) => i !== optionIndex
    );
    setPollOptions([...optionsAfterDelete]);

  };

  let teamPollContentType = form.getFieldValue("teamPollContentType");

  const handleImageUpload = async (file: File, index: number) => {
    toast.info(`Uploading the image....`);
    const obj = await s3FileUpload(file);
    if (obj?.data.statusCode !== 200) {
      //throw error
      toast.error(`Unable to upload the image`);
    } else {
      let url = `${config.apiBaseUrl}files/${obj?.data.fileId}`
      handleOption(index, "imageUrl", url)
      setOptionImage(url);
      toast.success(`Image uploaded successfully`);
    }

  }

  const handleOption = (index: number, key: string, value: string | number) => {
    let pollOptionsCopy: TeamPollOption[] = JSON.parse(JSON.stringify(pollOptions));
    pollOptionsCopy.forEach((option: TeamPollOption, optionIndex: number, array: Array<object>) => {
      if (optionIndex === index) {
        array[index] = {
          ...option,
          [key]: value
        }
      }
    });
    setPollOptions([...pollOptionsCopy])
  }

  return (
    <PollOptionCardWrapper>
      <Row gutter={[12, 12]} justify="space-between" align='middle'>
        <Col span={6}>
          {/* <Checkbox /> &nbsp; */}
          <Text type="secondary" >Option {index + 1}</Text>
        </Col>
        <Col span={18}>
          <Row gutter={[12, 12]} justify="end" align='top'>
            <Col style={{ display: "flex", alignItems: "center" }}>
              <Text type="secondary" style={{ marginRight: "12px" }}>Position</Text>
              <InputNumber size='small' min={1} style={{ maxWidth: "50px" }} max={pollOptions.length} value={pollOption?.orderBy}
                onChange={(value) => {
                  handleOption(index, "orderBy", value)
                }} />
            </Col>
            {teamPollContentType === "image" &&
              <Col>
                <Upload
                  ref={uploadRef}
                  customRequest={async ({
                    file,
                  }) => {
                    handleImageUpload(
                      file as File,
                      index
                    )
                  }}
                  maxCount={1}
                  name={`option${index + 1}`}
                >
                  <UploadIcon className="upload-icon" />
                </Upload>
              </Col>}
            <Col>
              {pollOptions.length > 2 && <DeleteIcon
                onClick={() => onDeleteOption(index)}
                className="delete-icon"
              />}
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Input placeholder="Option" className='optionDescription' type="text"
            defaultValue={pollOption?.description}
            onChange={(e) => {
              handleOption(index, "description", e.target.value)
            }}
          />
        </Col>
        {
          optionImage && (
            <Row style={{ marginTop: "16px", width: "100%" }} align="middle" justify="center"> <img className='optionImage' src={optionImage} alt="Option" /></Row>
          )
        }
      </Row >
    </PollOptionCardWrapper >
  );
};

export default PollOptionCard;
