import { Button, Col, FormInstance, Input, Row, Switch } from 'antd';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import LabelInput from '../LabelInput';
import LabelValue from '../LabelValue';
import PollOptionCard from './PollOptionCard';
import * as React from 'react';
import { ReactComponent as DeleteIcon } from 'assets/icons/delete.svg';
import { v4 as uuidv4 } from 'uuid';
import { TeamPollOption } from 'ts/interfaces/polls.inteface';

const PollQuestionCardWrapper = styled.div`
    /* display:grid; */
    /* grid-gap:16px; */
  .add-option-button {
    padding: 20px 0;
    height: 100%;
  }
  .toggle-if-multiple {
    margin: 20px 0;
  }
  .t-allow-multiple {
    font-size: 16px;
    line-height: 150%;
    color: #0c2146;
  }
`;

interface PollQuestionCardProps {
  form: FormInstance;
  pollOptions: TeamPollOption[];
  setPollOptions: React.Dispatch<any>;
}

const PollQuestionCard = ({ form, pollOptions, setPollOptions }: PollQuestionCardProps): JSX.Element => {
  const onAddNewPollOption = () => {
    setPollOptions([...pollOptions, { description: '', uniqueKey: uuidv4(), orderBy: pollOptions.length + 1 }]);
  };
  return (
    <PollQuestionCardWrapper>
      <LabelInput
        label="Poll Question"
        name='question'
        rules={[
          {
            required: true,
            message: 'Poll Title is required!',
          },
        ]}
        inputElement={
          <Input
            type="text"
            onChange={(e) => form.setFieldsValue({ question: e.target.value })}
            placeholder="Poll question"
          />
        }
      />
      <LabelInput
        label="Description"
        name="description"
        rules={[
          {
            required: true,
            message: 'Poll Title is required!',
          },
        ]}
        inputElement={
          <Input
            type="text"
            onChange={(e) =>
              form.setFieldsValue({ description: e.target.value })
            }
            placeholder="Description"
          />
        }
      />

      <Row gutter={[16, 16]} style={{ marginTop: "16px" }}>
        {pollOptions.map((option, index) => (
          <Col span={12} key={option.uniqueKey}>
            <PollOptionCard
              pollOptions={pollOptions}
              pollOption={option}
              setPollOptions={setPollOptions}
              form={form}
              index={index}
            />
          </Col>
        ))}

        {
          pollOptions.length < 6 && <Col span={12}>
            <Button
              className="add-option-button"
              onClick={() => onAddNewPollOption()}
              block
              type="dashed"
            >
              + Add Option
            </Button>
          </Col>
        }
      </Row>
      <LabelInput
        name="canMultiselect"
        inputElement={
          <Row gutter={[12, 4]} align="middle">
            <Col>
              <Switch
                defaultChecked={form.getFieldValue("canMultiselect")}
                onChange={(checked) =>
                  form.setFieldsValue({ canMultiselect: checked })
                }
                className="toggle-if-multiple"
              />
            </Col>
            <Text className="t-allow-multiple" strong>
              Allow Multiple
            </Text>
          </Row>
        }
      />
    </PollQuestionCardWrapper >
  );
}
export default PollQuestionCard;
