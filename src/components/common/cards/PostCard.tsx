import styled from 'styled-components/macro';
import { format } from 'date-fns';
import ReactMarkdown from 'react-markdown';

import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';

const PostCardWrapper = styled.div`
  flex: 0 0 1;
  .post-wrapper {
    padding: 32px;
    margin-top: 40px;
    row-gap: 16px;
    background: #262626;
    border-radius: 4px;
    max-width: 580px;
    width: 100%;
    .team-action-wrapper {
      column-gap: 8px;
      align-items: center;
      justify-content: space-between;
      .post-image {
        img {
          width: 56px;
          height: 56px;
          border-radius: 50%;
        }
      }

      .post-supporting-image {
        width: 100%;
        height: 100%;
        border-radius: 4px;
      }
      .t-team-name {
        font-weight: bold;
        font-size: 18px;
        line-height: 28px;
        letter-spacing: -0.26px;
        color: #ffffff;
      }
      .t-posted-on {
        font-size: 14px;
        line-height: 20px;
        letter-spacing: -0.09px;
        color: #737373;
      }
    }
    .recent-user-image {
      width: 40px;
      height: 40px;
      border-radius: 50%;
      &:not(:first-child) {
        margin-left: -20px;
      }
    }
    .comment-button {
      border: none;
      padding: 8px 16px;
      font-size: 16px;
      line-height: 24px;
      height: fit-content;
      letter-spacing: -0.18px;
      color: #242423;
      background: #fb923c;
      border-radius: 4px;
    }
    .actions-wrapper {
      align-items: center;
      column-gap: 18px;
      justify-content: flex-end;
    }
  }
  .post-text {
    font-size: 18px;
    line-height: 30px;
    letter-spacing: -0.26px;
    font-family: 'Sora', sans-serif;
    color: #f5f5f5;
  }
`;

interface PostCardProps {
  teamProfileImage?: string;
  teamName?: string;
  postCreationDate: string | number;
  postBannerImageUrl: string;
  body: string;
}

const PostCard = ({
  teamProfileImage,
  teamName,
  postCreationDate,
  postBannerImageUrl,
  body,
}: PostCardProps): JSX.Element => {
  return (
    <PostCardWrapper className="all-posts-wrapper">
      <FlexColumnWrapper className="post-wrapper">
        <FlexRowWrapper className="team-action-wrapper">
          <div className="post-image">
            <img
              src={
                teamProfileImage ||
                'https://m.media-amazon.com/images/I/61peIdz-C2L._SX355_.jpg'
              }
              alt=""
            />
          </div>
          <FlexColumnWrapper>
            <div className="t-team-name">{teamName ?? 'Team Name'}</div>
            <div className="t-posted-on">
              {postCreationDate && format(new Date(postCreationDate), 'PPp')}
            </div>
          </FlexColumnWrapper>
          {/* <FlexRowWrapper className="actions-wrapper">
            <FullscreenIcon />
            <MoreIcon />
          </FlexRowWrapper> */}
        </FlexRowWrapper>
        <img
          src={
            postBannerImageUrl ||
            'https://m.media-amazon.com/images/I/61peIdz-C2L._SX355_.jpg'
          }
          className="post-supporting-image"
          alt=""
        />
        <div className="post-text">
          {body}
          {/* <ReactMarkdown 
          children={body} /> */}
        </div>
      </FlexColumnWrapper>
    </PostCardWrapper>
  );
};

export default PostCard;
