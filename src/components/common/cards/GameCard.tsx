import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
// import { ReactComponent as CoinIcon } from 'assets/icons/coin.svg';

const GameCardWrapper = styled(FlexColumnWrapper)`
  background: #f5f5f5;
  max-width: 368px;
  width: 100%;
  border-radius: 4px;
  .game-info-wrapper {
    padding: 16px 24px;
  }
  .game-team-info-wrapper {
    column-gap: 16px;
    padding: 16px 24px;
    background: #e5e5e5;
    .t-game-sport {
      font-weight: 600;
      font-size: 14px;
      line-height: 20px;
      letter-spacing: -0.09px;
      color: #f59e0b;
    }
    .t-game-team {
      font-weight: bold;
      font-size: 18px;
      line-height: 28px;
      letter-spacing: -0.26px;
      color: #000000;
    }
    .t-game-team-symbol {
      font-weight: bold;
      font-size: 18px;
      line-height: 28px;
      letter-spacing: -0.26px;
      color: #737373;
    }
    .game-image-wrapper {
      img {
        width: 80px;
        height: 80px;
        background: #525252;
        border-radius: 50%;
      }
    }
  }
  .t-game-question {
    font-weight: bold;
    font-size: 18px;
    line-height: 28px;
    letter-spacing: -0.26px;
    color: #171717;
  }
  .participate-coin-wrapper {
    align-items: center;
    .participate-button {
      background: #0075ff;
      border-radius: 40px;
      border: none;
      padding: 8px 24px;
      font-weight: bold;
      font-size: 16px;
      line-height: 24px;
      text-align: right;
      letter-spacing: -0.18px;
      color: #fafafa;
    }
    .coin-rewards {
      align-items: center;
      justify-content: flex-end;
      font-weight: bold;
      column-gap: 8px;
      font-size: 18px;
      line-height: 28px;
      letter-spacing: -0.26px;
      color: #171717;
    }
  }
  .divider-game {
    background: #e5e5e5;
    height: 2px;
    margin: 10px 0;
  }
`;

interface GameCardProps {}

const GameCard = ({}: GameCardProps): JSX.Element => {
  return (
    <GameCardWrapper>
      <FlexRowWrapper className="game-team-info-wrapper">
        <div className="game-image-wrapper">
          <img
            src="https://m.media-amazon.com/images/I/61peIdz-C2L._SX355_.jpg"
            alt="game"
          />
        </div>
        <FlexColumnWrapper className="">
          <div className="t-game-sport">Kabaddi</div>
          <div className="t-game-team">Dabang Delhi</div>
          <div className="t-game-team-symbol">$DBD</div>
        </FlexColumnWrapper>
      </FlexRowWrapper>
      <div className="game-info-wrapper">
        <div className="t-game-question">
          Who Will become the man of the Dabang vs Thalaivas match?
        </div>
        <div className="divider-game" />
        <FlexRowWrapper className="participate-coin-wrapper">
          <button className="participate-button">Participate</button>
          <FlexRowWrapper className="coin-rewards">10 $SPN</FlexRowWrapper>
        </FlexRowWrapper>
      </div>
    </GameCardWrapper>
  );
};

export default GameCard;
