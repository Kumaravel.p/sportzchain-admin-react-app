import { Card } from 'antd';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import FlexRowWrapper from '../wrappers/FlexRowWrapper';

const StatCardWrapper = styled.div`
  .t-value {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */

    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    .green {
      color: #4aaf05;
    }

    span {
      /* Red / 1 */
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }
  .t-supporting-text-value {
    font-size: 14px;
    line-height: 19px;
    display: flex;
    align-items: flex-end;
    letter-spacing: -0.1px;
    color: rgba(50, 60, 71, 0.4);
  }
  .stat-card-value-text-wrapper {
    justify-content: space-between;
    align-items: center;
  }
`;

interface StatCardProps {
  title: string;
  cta: JSX.Element | string;
  value: JSX.Element | string;
  supportingValueText: string;
}

const StatCard = ({
  title,
  cta,
  value,
  supportingValueText,
}: StatCardProps): JSX.Element => {
  return (
    <StatCardWrapper>
      <Card title={title} extra={cta} style={{ width: 300 }}>
        <FlexRowWrapper className="stat-card-value-text-wrapper">
          <Text className="t-value ">{value} &nbsp;</Text>
          <Text className="t-supporting-text-value">{supportingValueText}</Text>
        </FlexRowWrapper>
      </Card>
    </StatCardWrapper>
  );
};

export default StatCard;
