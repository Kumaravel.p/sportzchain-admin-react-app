import styled from 'styled-components/macro';
import { Select, Input } from 'antd';

const Wrapper = styled('div')`
&&& {
.ant-input-group-addon{
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
}
.ant-input{
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
}
.ant-select-selection-item{
    color:#000;
}
.ant-select-selection-search input{
    color:#000;
}
}
`;


interface MobileElementProps {
    onChange?: any;
    value?: ValueProps;
    optionValue?: string;
    optionLabel?: string;
    options?: Array<Object | string>,
    labelInValue?: boolean,
    placeholder?: string,
}

interface ValueProps {
    countryCode?: Object,
    mobileNumber?: string
}

const prefixSelector = (
    value: object,
    options: any,
    optionLabel: string,
    optionValue: string,
    callback: any,
    labelInValue: boolean,
) => {
    const { Option } = Select;
    return (
        <Select
            style={{ width: 80 }}
            value={Object.keys(value).length ? value : undefined}
            onChange={(val) => callback('countryCode', val)}
            labelInValue={labelInValue}
            showSearch
            placeholder="Select"
        >
            {
                options?.map((_: any) => (
                    <Option value={_?.[`${optionValue}`]}>{_?.[`${optionLabel}`]}</Option>
                ))
            }
        </Select>
    )
};

const SelectWithInput = ({
    value = {},
    options,
    optionValue = "value",
    optionLabel = "label",
    onChange,
    labelInValue = false,
    placeholder=""
}: MobileElementProps): JSX.Element => {


    const onChangeValues = (key: string, val: any) => {
        if(key === "mobileNumber"){
            const reg = /^-?\d*(\.\d*)?$/;
            if (!reg.test(val)) {
              return false
            }
        }
        let updateValue = {
            ...value,
            [key]: val
        }
        if (onChange) {
            onChange(updateValue)
        }
    }

    return (
        <Wrapper>
            <Input
                addonBefore={
                    prefixSelector(
                        value?.countryCode ?? {},
                        options,
                        optionLabel,
                        optionValue,
                        onChangeValues,
                        labelInValue
                    )}
                value={value?.mobileNumber}
                placeholder={placeholder}
                style={{ width: '100%' }}
                onChange={(e: any) => onChangeValues('mobileNumber', e.target.value)}
            />
        </Wrapper>
    );
};

export default SelectWithInput;
