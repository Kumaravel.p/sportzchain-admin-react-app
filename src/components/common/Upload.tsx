import React from 'react';
import { Spin, Upload as AntUpload } from 'antd'
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { RcFile } from 'rc-upload/lib/interface';
import { s3FileUpload } from 'apis/storage';
import { toast } from 'react-toastify';
import config from 'config';
import styled from 'styled-components/macro';

const StyledUpload = styled(AntUpload) <{ fullWidth?: boolean }>`
    & .ant-upload,.ant-upload-list-picture-card-container{
        width:${props => props?.fullWidth ? "100%" : "104px"}
    }
`;

interface UploadProps {
    fileList?: any[];
    doApiCall?: boolean;
    onChange?: (data: any) => void;
    beforeUpload?: (file: RcFile) => void;
    fullWidth?: boolean;
    maxCount?: number | undefined;
    accept?: string | undefined;
    disabled?: boolean;
}

export const Upload = (props: UploadProps): JSX.Element => {

    const [loading, setLoading] = React.useState<boolean>(false);

    const {
        fileList = [],
        doApiCall = true,
        onChange,
        beforeUpload,
        fullWidth = false,
        maxCount,
        accept,
        disabled=false
    } = props;

    const handleChange = async (file: any, from: 'remove' | 'upload' = 'upload') => {
        if (from === "remove") {
            const filteredFiles = fileList?.filter(item => item?.fileId !== file?.fileId);
            if (onChange) {
                onChange(filteredFiles)
            }
            return false
        }
        else if (doApiCall) {
            setLoading(true);
            await s3FileUpload(file)
                .then(res => {
                    if (res?.data?.statusCode === "500") {
                        catchError(res?.data?.message)
                    }
                    toast.success(res?.data?.message)
                    let fileId = res?.data?.fileId;
                    if (onChange) {
                        onChange([
                            ...fileList, {
                                uid: fileId,
                                fileId: fileId,
                                url: `${config.apiBaseUrl}files/${fileId}`
                            }])
                    }
                    setLoading(false);
                })
                .catch(e => catchError(e))
        }
        else {
            onChange && onChange(file)
        }
    }

    const catchError = (msg: string) => {
        setLoading(false);
        toast.error(msg);
        console.log(msg);
        return false
    }

    const uploadButton = (
        <div>
            <PlusOutlined />
        </div>
    );

    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

    const getButtonChildren = () => {
        if (loading) {
            return <Spin indicator={antIcon} />
        }
        else if (maxCount) {
            if (fileList.length < maxCount) {
                return uploadButton
            }
            return null
        }
        return uploadButton
    }

    return (
        <StyledUpload
            fullWidth={fullWidth}
            accept={accept}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            listType="picture-card"
            fileList={fileList}
            customRequest={({ file }: any) => handleChange(file)}
            beforeUpload={(file: RcFile) => beforeUpload && beforeUpload(file)}
            maxCount={maxCount}
            showUploadList={{
                showPreviewIcon: false
            }}
            onRemove={(file: any) => handleChange(file, "remove")}
            disabled={disabled}
        >
            {getButtonChildren()}
        </StyledUpload>
    )
}