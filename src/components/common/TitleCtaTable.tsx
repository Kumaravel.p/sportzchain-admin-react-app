import { Card, Table } from 'antd';
import styled from 'styled-components/macro';
import TitleCta from './TitleCta';

const TitleCtaTableWrapper = styled.div`
  margin: 40px 0px 20px 0px;
  .ant-card {
    padding: 5px;
    .ant-table-thead {
      .ant-table-cell {
        background: transparent;
        font-weight: 500;
        font-size: 14px;
        line-height: 135%;
        text-transform: uppercase;
        color: #9ab1b6;
      }
    }
   &.ant-card{
      border-radius:10px;
    }
    .ant-table-row {
      .ant-table-cell {
        font-weight: 500;
        font-size: 16px;
        line-height: 19px;
        color: #051f24;
      }
    }

    .ant-table-thead
      > tr
      > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
      background-color: transparent;
    }
    .title-cta-wrapper {
      margin-bottom: 30px;
      .title {
        font-weight: 600;
        font-size: 20px;
        line-height: 24px;
        color: #051f24;
      }
      .cta {
        font-weight: 600;
        cursor: pointer;
        font-size: 16px;
        line-height: 19px;
        color: #07697d;
      }
    }
  }
`;

interface TitleCtaTableProps {
  data?: Object[];
  cta?: JSX.Element | string;
  title: string;
  columns?: Object[];
  onRow?: any; //GetComponentProps<Object> | undefined;
}

const TitleCtaTable = ({
  data,
  title,
  cta,
  columns,
  onRow,
}: TitleCtaTableProps): JSX.Element => {
  return (
    <TitleCtaTableWrapper>
      <Card>
        <TitleCta title={title} cta={cta} />
        <Table
          onRow={onRow}
          columns={columns}
          dataSource={data}
          size="middle"
        />
      </Card>
    </TitleCtaTableWrapper>
  );
};

export default TitleCtaTable;
