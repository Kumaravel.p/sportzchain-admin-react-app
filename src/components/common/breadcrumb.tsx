import React from "react";
import { Breadcrumb, Row } from 'antd';
import { Link } from 'react-router-dom';
import { ArrowLeftOutlined } from '@ant-design/icons';
import styled from 'styled-components/macro';


const Back = styled.div`
    width:32px;
    height:32px;
    border-radius:100%;
    background:#EEEEEE;
    display:grid;
    place-items:center;
    &:hover{
        background:#e5e7eb;
        transition:0.5s;
    }
`;

const BreadcrumbWrapper = styled(Row)`
    width:100%;
    justify-content:space-between;
    align-items:center;
    grid-gap:16px;
    .link{
        font-size:16px;
        font-weight:500;
    }
    `


type BreadCrumb = {
    url: string;
    name: string;
}

interface BreadcrumbProps {
    backPath?: string;
    breadCrumbs: BreadCrumb[];
    cta?: JSX.Element
}


const BreadcrumbComp = ({ backPath, breadCrumbs, cta }: BreadcrumbProps): JSX.Element => {
    return (
        <BreadcrumbWrapper>
            <Row align="middle">
                {
                    backPath &&
                    <Back>
                        <Link to={backPath}>
                            <ArrowLeftOutlined />
                        </Link>
                    </Back>
                }
                <Breadcrumb separator=">" style={{ marginLeft: backPath ? '16px' : '0px' }}>
                    {
                        breadCrumbs.map((breadCrumb) => {
                            return (

                                <Breadcrumb.Item key={breadCrumb.url}>
                                    <Link className="link" to={breadCrumb.url}>{breadCrumb.name}</Link>
                                </Breadcrumb.Item>

                            )
                        })
                    }
                </Breadcrumb>
            </Row>
            {cta && <Row>{cta}</Row>}
        </BreadcrumbWrapper>
    )
}

export default BreadcrumbComp
