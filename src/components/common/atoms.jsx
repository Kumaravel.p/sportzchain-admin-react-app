import styled from 'styled-components/macro';


export const Label=styled.p`
    color:rgba(5, 31, 36, 0.6);
    font-size:14px;
    font-weight:400;
    margin-bottom:4px;
`;

export const TextContent=styled.p`
    color:black;
    font-size:16px;
    font-weight:500;
`;