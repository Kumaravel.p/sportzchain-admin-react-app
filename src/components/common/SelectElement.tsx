import styled from 'styled-components/macro';
import { Select } from 'antd';

const SelectElementWrapper = styled.div`
.ant-select{
  width:100%;
}
.ant-select .ant-select-selector{
  border-radius:5px;
  border:1px solid #97a0c3;
}
`;

interface SelectElementProps {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  toFilter: string;
  toStore?: string;
  mode?: "multiple" | "tags" | undefined,
  searchable?: boolean;
  placeholder: string;
  defaultValue?: any;
  options?: Array<Object | string>;
  allowClear?: any
  value?: any
  disabled?: boolean
  labelInValue?: boolean;
  loading?:boolean;
  disabledOptions?:string[];
}

const SelectElement = ({
  onChange,
  toFilter,
  toStore,
  searchable,
  placeholder,
  options = [],
  defaultValue,
  allowClear,
  value,
  disabled = false,
  labelInValue = false,
  mode,
  loading=false,
  disabledOptions=[]
}: SelectElementProps): JSX.Element => {
  // console.log(typeof defaultValue, 'options');
  const onSearch = (val: string) => {
    console.log('search:', val);
  };

  return (
    <SelectElementWrapper>
      <Select
        defaultValue={defaultValue}
        showSearch={searchable ? true : false}
        placeholder={placeholder}
        mode={mode}
        labelInValue={labelInValue}
        optionFilterProp={toFilter}
        onChange={onChange}
        onSearch={onSearch}
        // options: func ovject
        filterOption={(input, option: any) =>
          option?.children?.toLowerCase().indexOf(input?.toLowerCase()) >= 0
        }
        allowClear={allowClear}
        value={value}
        disabled={disabled}
        loading={loading}
      >
        {options.map((item: any, i: number) => (
          <Select.Option 
            key={item[`${toStore ? toStore : toFilter}`]} 
            value={item[`${toStore ? toStore : toFilter}`]}
            disabled={disabledOptions?.length ? disabledOptions?.some(_=>_ === item?.[`${toStore ? toStore : toFilter}`]) : false}
            >
            {item[`${toFilter}`]}
          </Select.Option>
        ))}
      </Select>
    </SelectElementWrapper>
  );
};

export default SelectElement;
