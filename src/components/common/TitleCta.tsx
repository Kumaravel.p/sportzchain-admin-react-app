import { Col, Row } from 'antd';
import styled from 'styled-components/macro';

const TitleCtaWrapper = styled(Row)`
  justify-content: space-between;
  align-items: center;
  margin-bottom: 40px;
  .title {
    font-weight: 600;
    font-size: 22px;
    line-height: 24px;
    color: #051f24;
  }
`;

interface TitleCtaProps {
  title: string;
  cta?: JSX.Element | string;
}

const TitleCta = ({ title, cta }: TitleCtaProps): JSX.Element => {
  return (
    <TitleCtaWrapper>
      <Col className="title">{title}</Col>
      <Col className="cta">{cta}</Col>
    </TitleCtaWrapper>
  );
};

export default TitleCta;
