import { Form } from 'antd';
import { Rule } from 'antd/lib/form';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import styled from 'styled-components/macro';
import { Label, TextContent } from 'components/common/atoms'
import { ReactNode } from 'react-markdown/lib/react-markdown';


const LabelInputWrapper = styled(FlexColumnWrapper)`
  row-gap: 8px;
  padding: 10px 0;
  & .required{
    color:red;
    margin-left:8px;
  }
  .modal-input-label {
    color:gray;
    font-size:16px;
    margin-bottom:8px;
    line-height: 15px;
    opacity: 0.9;
  }
  .input-container {
    input[type='text'],
    input[type='tel'],
    input[type='number'],
    input[type='email'] {
      background: #fff;
      border-radius: 5px;
      border-color:#97a0c3;
    }
   
    .ant-input-group-addon{
      border-color: #97a0c3;
    }
    .ant-input{
      font-weight:500
    }
    .datepicker{
    border-radius:5px;
    border:1px solid #97a0c3;
    padding:4.5px 4px;
  }
  }
  .ant-form-item-has-error{
    .ant-input-group-addon{
      border-color:#ff4d4f;
    }
  }
`;

interface LabelInputProps {
  label?: string | React.ReactNode;
  inputElement?: JSX.Element;
  name?: string;
  rules?: Rule[];
  initialValues?: Object[];
  initialValue?: string | number | object;
  className?: string;
  required?: boolean;
  valuePropName?:string | undefined;
}

const LabelInput = ({
  label,
  inputElement,
  name,
  rules,
  initialValue,
  initialValues,
  className="",
  required=false,
  valuePropName=undefined
}: LabelInputProps): JSX.Element => {
  return (
    <LabelInputWrapper>
      <Label>
        {label && (
          <Label>
            {label}
            {required && <span className="required">*</span>}
          </Label>
        )}
      </Label>
      <Form.Item className={className ? className : "input-container"} name={name} rules={rules} initialValue={initialValue} valuePropName={valuePropName}>
        {inputElement}
      </Form.Item>
    </LabelInputWrapper>
  );
};

export default LabelInput;
