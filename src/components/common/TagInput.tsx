import React, { useMemo, useState } from 'react';
import { Input, Button, Typography } from 'antd';
import styled from 'styled-components/macro';
import { CloseOutlined } from '@ant-design/icons';

const Wrapper = styled.div`
    display:flex;
    gap:8px;
`;

const Flex = styled.div`
    display:flex;
    padding:8px;
    flex:1;
    gap:8px;
    border: 1px solid #D9D9D9;
    flex-wrap:wrap;
    & input{
        border:0;
        min-width:200px;
        padding-inline:8px;
        flex:1;
        &:focus{
            border-color:transparent;
            box-shadow:none;
        }
    }
    & .flexWrap{
        display:flex;
        align-items:center;
        gap:8px;
    }
    & .chip{
        border: 1px solid #D9D9D9;
        border-radius: 20px;
        padding: 4px 8px;
    }
    & .close-icon{
        cursor:pointer;
        font-size:12px;
    }
    & .email-name{
        color: #171717;
        font-weight: 400;
        font-size: 12px;
    }
`;

const ErrorText = styled.div`
    color: #FF4D4A;
    margin-top: 4px;
    font-weight: 500;
    text-align:end;
    font-size: 12px;
`;

interface TagInputProps {
    list?: string[];
    onDelete?: (val: string) => void;
    onChange?: (val: string) => void;
    onPressEnter?: (e: any) => boolean;
    onSubmit?: (val: string[]) => void;
    errorMessage?: string;
}

export const TagInput = (props: TagInputProps): JSX.Element => {

    const {
        list = [],
        onPressEnter = (e: any) => null,
        onDelete = (e: string) => null,
        onChange = (e: string) => null,
        onSubmit = (val: string[]) => null,
        errorMessage = "This field is required"
    } = props;

    const [value, setValue] = useState("");
    const [error, setError] = useState(false);

    const onChangeValue = (e: any) => {
        setValue(e.target.value)
    }

    const onPressEnterInput = (e: any) => {
        let val = e.target.value;
        if (onPressEnter && onPressEnter(val)) {
            setError(false)
            if (onChange) {
                if (!list?.some(_ => _ === val)) {
                    onChange(val)
                    setValue("")
                }
                else {
                    setValue("")
                }
            }
            return false
        }
        setError(true)
    }

    const addonBefore = useMemo(() => {
        return (
            <div className='flexWrap'>
                {
                    list?.map((_, index) => (
                        <div className='flexWrap chip' key={`email-${index}`}>
                            <Typography.Text className='email-name'>{_}</Typography.Text>
                            <CloseOutlined className='close-icon' onClick={() => onDelete(_)} />
                        </div>
                    ))
                }
            </div>
        )
    }, [list])

    const onSubmitBtn = () =>{
        if(onSubmit){
            onSubmit(list)
        }
        setError(false)
        setValue("")
    }

    return (
        <div>
            <Wrapper>
                <div style={{ flex: 1 }}>
                    <Flex>
                        {list?.length > 0 && addonBefore}
                        <Input
                            onPressEnter={onPressEnterInput}
                            value={value}
                            onChange={onChangeValue}
                        />
                    </Flex>
                    {error && <ErrorText>{errorMessage}</ErrorText>}
                </div>
                <Button onClick={onSubmitBtn} type="primary">Submit</Button>
            </Wrapper>
        </div>
    )

}