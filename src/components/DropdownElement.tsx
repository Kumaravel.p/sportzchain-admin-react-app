import styled from 'styled-components/macro';
import * as React from 'react';
import { Button, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';

const DropdownElementWrapper = styled.div``;

interface DropdownElementProps {
  options: React.ReactElement<any, string | React.JSXElementConstructor<any>>;
  title: string;
}

const DropdownElement = ({
  options,
  title,
}: DropdownElementProps): JSX.Element => {
  return (
    <DropdownElementWrapper>
      <Dropdown overlay={options}>
        <Button>
          {title}
          <DownOutlined />
        </Button>
      </Dropdown>
    </DropdownElementWrapper>
  );
};

export default DropdownElement;
