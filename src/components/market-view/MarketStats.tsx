import styled from 'styled-components/macro';
import { marketColumns, marketData } from 'data/marketData';
import { Table } from 'antd';

const MarketStatsWrapper = styled.div`
  .market-search-input {
    padding: 9px;
    width: 300px;
    background: rgba(5, 31, 36, 0.05);
    border: 1px solid rgba(5, 31, 36, 0.05);
    box-sizing: border-box;
    border-radius: 5px;
    margin-bottom: 30px;
  }
`;

interface MarketStatsProps {}

const MarketStats = ({}: MarketStatsProps): JSX.Element => {
  return (
    <MarketStatsWrapper>
      <input
        type="text"
        className="market-search-input"
        placeholder="Search and filter..."
      />
      <Table columns={marketColumns} dataSource={marketData} size="middle" />
    </MarketStatsWrapper>
  );
};

export default MarketStats;
