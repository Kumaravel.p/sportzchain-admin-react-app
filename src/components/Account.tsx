import { CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import { Auth } from 'aws-amplify';

import { AccountContext } from 'context/AccountContext';

import Pool from './UserPool';

export const Account = ({ children }: { children: React.ReactElement }) => {
  const getSession = async () => {
    return await new Promise((resolve, reject) => {
      const user = Pool.getCurrentUser();
      if (user) {
        user.getSession((err: Error, session: CognitoUser | null) => {
          if (err) {
            reject(err);
          } else {
            resolve(session);
          }
        });
      } else {
        reject();
      }
    });
  };

  const authenticate = async (Username: string, Password: string) => {
    // const user = new CognitoUser({ Username, Pool });

    // const authDetails = new AuthenticationDetails({ Username, Password });

    try {
      let auth = await Auth.signIn(Username, Password);
      return auth;
    } catch (error) {
      console.log('error signing in', error);
    }
  };

  const logout = () => {
    const user = Pool.getCurrentUser();
    if (user) {
      user.signOut();
    }
  };

  return (
    <AccountContext.Provider value={{ authenticate, getSession, logout }}>
      {children}
    </AccountContext.Provider>
  );
};
