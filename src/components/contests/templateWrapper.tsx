import styled from 'styled-components/macro';
import Title from 'antd/lib/typography/Title';
import { Col, FormInstance, Radio, Row, Typography } from 'antd';
import Text from 'antd/lib/typography/Text';
import FormInput from 'components/form-elements/FormInput';
import { Label } from 'components/common/atoms';
import { Form } from 'antd';
import config from 'config';

const TemplateWrapperDiv = styled.div`
  .ant-radio-wrapper {
    align-items: start;
  }
  .header {
    color: #051f24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 8px;
  }
`;

const RadioLabel = styled.div`
    display: flex;
    flex-direction: row-reverse;
    align-items: flex-start;
    justify-content: flex-end;
    margin-bottom: 15px;
`

interface templateProps {
  form: FormInstance;
  value?: number;
  title?: string;
  onChange?: any;
  renderJson?: { id: number, name?: string, grid?: number; }[],
  keyName: string,
  isView?: boolean
}

export const TemplateWrapper = ({
  form,
  renderJson,
  onChange,
  value,
  title,
  keyName,
  isView
}: templateProps) => {
  return (
    <TemplateWrapperDiv>
      <Typography className="header">{title}</Typography>
      <Form.Item
        name={keyName}
        initialValue={1}
        rules={[
          {
            required: true,
            message: `${title} Type is required!`,
          },
        ]}
      >
        <Radio.Group onChange={(e) => onChange(e.target.value)} style={{ width: '100%' }} disabled={isView}>
          <Row gutter={[12, 12]}>
            {renderJson &&
              renderJson?.length &&
              renderJson?.map((item) => {
                return (
                  <Col span={item?.grid ?? 8} style={{ marginBottom: 15 }}>
                    <RadioLabel>
                      <Label>
                        {item?.name}
                      </Label>
                      <Radio value={item?.id}>
                      </Radio>
                    </RadioLabel>
                    <div>
                      <div
                        onClick={() => {
                          if (!isView) {
                            form.setFieldsValue({ [keyName]: item?.id })
                            onChange(item?.id)
                          }
                        }}
                        style={{ width: 245, height: "100%", border: value === item?.id ? 'orange 2px solid' : '' }}>
                        <img width={"100%"} src={`${config.apiBaseUrl}contest/template/${item?.id}.svg`} alt="template-image" />
                      </div>
                    </div>
                  </Col>
                );
              })}
          </Row>
        </Radio.Group>
      </Form.Item>
    </TemplateWrapperDiv>
  );
};
