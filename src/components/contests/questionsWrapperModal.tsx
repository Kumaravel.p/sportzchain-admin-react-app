/*
*  constest question form with modal and table only
*  refer component questionsFormCommon for additional prop implementation used in their question form components
*/
import React from 'react';
import {
  Row,
  Col,
  Typography,
  Button,
  Modal,
  Form,
  Input,
  Switch,
  Upload,
} from 'antd';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import { PlusOutlined } from '@ant-design/icons';
import { DeleteOutlined as DeleteIcon } from '@ant-design/icons';
import CustomTable from 'components/customTable';
import LabelInput from 'components/common/LabelInput';
import { UploadOptionsCard } from './uploadOptionsCard';
import { toast } from 'react-toastify';
import { s3FileUpload } from 'apis/storage';
import config from 'config';

const QuestionsWrapperComp = styled.div`
  .ant-radio-group {
    width: 100%;
  }
  .mb-20 {
    margin-bottom: 20px;
  }
  .column-wrapper {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .radio-center-align {
    flex: 1;
    display: flex;
    align-items: center;
  }
`;

const ButtonGroupWrapper = styled.div`
  margin-top: 15px;
  margin-bottom: 15px;
  .breadcumb-btn-mg {
    width: 160px;
  }
`;

const DivWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 120px;
  height: 140px;
  border: dashed 1px #07697d;
  .highlight-text {
    font-size: 14px;
    color: #4da1ff;
    font-weight: 500;
  }
`;



const IconButton = styled.div`
  cursor: pointer;
  font-size: 17px;
  .delete-icon {
    color: red;
  }
`;

interface QuestionsWrapperProps {
  data: any;
  onSubmit: (record: any) => void;
  isView?: boolean;
  isSelectAnswer?: boolean;
}

interface uploadRecordState {
  description: string;
  isAnswer?: boolean;
  imageUrl?: string;
}

const initialUploadRecordState = [
  {
    description: '',
    isAnswer: true,
  },
  {
    description: '',
  },
];

// form
export const QuestionsWrapperModal = (props: QuestionsWrapperProps) => {
  const {
    data,
    onSubmit,
    isView = false,
    isSelectAnswer = true
  } = props;
  const [questions, setQuestionsData] = React.useState<any[]>([]);
  const [uploadRecord, setUploadRecord] = React.useState<uploadRecordState[]>([
    ...initialUploadRecordState,
  ]);
  const [editIndex, setEditIndex] = React.useState<number>();
  const [questionsModal, setQuestionsModal] = React.useState(false);
  const [questionImageUrl, setQuestionImageUrl] = React.useState<
    string | undefined
  >('');
  const [allowMutipleSelect, setAllowMultipleSelect] = React.useState<boolean>(
    false
  );
  const [form] = Form.useForm();

  const questionTableColumns = [
    {
      title: 'QUESTION',
      dataIndex: 'question',
      sorter: (a: any, b: any) => a.question.localeCompare(b.question),
    },
    {
      title: 'NO OF OPTIONS',
      dataIndex: ['option', 'length'],
      sorter: (a: any, b: any) => a.option.length - b.option.length,
    },
    {
      title: '',
      dataIndex: 'actions',
      component: isView ? 'VIEW' : 'ACTION',
    },
  ];

  const handleSaveForm = () => {
    setQuestionsModal(false);
    handleQuestionForm({
      question: form.getFieldValue('question-master'),
      option: uploadRecord,
    });
  };

  React.useEffect(() => {
    setQuestionsData(data);
  }, [data]);

  ////handle question table action EDIT | DELETE
  const handleRowClick = (
    record: any,
    rowIndex: any,
    keyName: string | undefined
  ) => {
    let data = questions;
    if (keyName === 'delete') {
      data.splice(rowIndex, 1);
      setQuestionsData([...data]);
    } else if (keyName === 'edit') {
      setEditIndex(rowIndex);
      let editData = data[rowIndex];
      form.setFieldsValue({ 'question-master': editData?.question });
      // auto filling form
      editData?.option?.forEach((item: any, index: any) => {
        let key = `description${index}`;
        let value = item?.description;
        form.setFieldsValue({ [key]: value });
      });
      setAllowMultipleSelect(editData?.canMultiSelect);
      setQuestionImageUrl(editData?.imageUrl);
      setQuestionsModal(true);
      setUploadRecord([...editData.option]);
    } else if (keyName === 'view') {
      setEditIndex(rowIndex);
      let editData = data[rowIndex];
      form.setFieldsValue({ 'question-master': editData?.question });
      // auto filling form
      editData?.option?.forEach((item: any, index: any) => {
        let key = `description${index}`;
        let value = item?.description;
        form.setFieldsValue({ [key]: value });
      });
      setAllowMultipleSelect(editData?.canMultiSelect);
      setQuestionImageUrl(editData?.imageUrl);
      setQuestionsModal(true);
      setUploadRecord([...editData.option]);
    }
  };

  /// close modal and resetting state
  const handleModalClose = () => {
    setQuestionsModal(false);
    setUploadRecord([...initialUploadRecordState]);
    setAllowMultipleSelect(false);
    setQuestionImageUrl(undefined);
  };

  const handleQuestionForm = (record: any) => {
    let construct = {
      question: record?.question,
      option: record?.option,
      canMultiSelect: allowMutipleSelect,
      imageUrl: questionImageUrl,
    };
    let data = questions ?? [];
    if (editIndex !== undefined) {
      data[editIndex] = {
        ...data[editIndex],
        ...construct,
      };
      // undo all internal state
      setEditIndex(undefined);
    } else {
      data.push({
        ...construct,
      });
    }
    onSubmit(data);
    setQuestionsData([...data]);
    handleModalClose();
  };

  const uploadRef = React.useRef(null);

  const handleOptionChange = (value: any, keyName: string, index: number) => {
    // handling multiple selection and isAnswer vaidation for multiple answers
    const isStateEditable = (keyName: string) => {
      if (keyName === 'isAnswer') {
        let checkDuplicate =
          uploadRecord?.filter((item) => item.isAnswer) ?? [];
        // check whether single options moved to false
        if (checkDuplicate?.length === 1 && value === false) {
          toast.info(`Minimum one answer options required`);
          return false;
        } else if (checkDuplicate?.length && !allowMutipleSelect) {
          if (checkDuplicate?.length === 1 && value === true) {
            ///allow swapping liimting one
            return 1;
          } else {
            // prevent multiple answer prevention without multiple select enable
            toast.info(
              `Allow multiple selection for selecting multiple answer options`
            );
          }
          return false;
        }
      }
      return 2;
    };

    let data = uploadRecord;

    let status = isStateEditable(keyName);
    if (status) {
      if (status === 1) {
        data?.map((item) => {
          item.isAnswer = false;
        });
      }
      data[index] = {
        ...data[index],
        [keyName]: value,
      };
      setUploadRecord([...data]);
    }
  };

  const onAddNewOption = () => {
    setUploadRecord([
      ...uploadRecord,
      {
        description: '',
      },
    ]);
  };

  const onDeleteOption = (recordIndex: number) => {
    if (uploadRecord?.length > 2) {
       // on delete form state not updated
       let modifyOption = uploadRecord.reduce((total: any, item: any, index: number) => (index >= recordIndex) ? ({ ...total, ['description' + index]: uploadRecord[index + 1]?.description ?? "" }) : total, {})
       form.setFieldsValue({ ...modifyOption })
       //
      uploadRecord.splice(recordIndex, 1);
      setUploadRecord([...uploadRecord]);
    } else {
      toast.warning(`Minimum of 2 option required`);
    }
  };

  const handleImageUpload = async (file: File, index: number | null) => {
    toast.info(`Uploading the image....`);
    const obj = await s3FileUpload(file);
    if (obj?.data.statusCode !== 200) {
      toast.error(`Unable to upload the image`);
    } else {
      let url = `${config.apiBaseUrl}files/${obj?.data.fileId}`;
      setQuestionImageUrl(url);
      toast.success(`Image uploaded successfully`);
    }
  };

  const handleSelectionOption = () => {
    if (allowMutipleSelect) {
      let activeRecords = uploadRecord?.filter((item) => item?.isAnswer) ?? [];
      if (activeRecords?.length > 1) {
        let data = uploadRecord;
        data?.map((item) => {
          item.isAnswer = false;
          return true;
        });
        setUploadRecord([
          {
            ...uploadRecord[0],
            isAnswer: true,
          },
          ...data.splice(1, data?.length),
        ]);
      }
    } else {
      setAllowMultipleSelect(!allowMutipleSelect);
    }
  };

  const uploadBasedRender = () => {
    if (questionImageUrl) {
      return (
        <DivWrapper>
          <img style={{width:'120px',height:'120px'}} className="img" src={questionImageUrl} alt="options-image" />
          <Text style={{ display: 'flex', alignItems: 'center' }}>
            <a>image.jpg</a>
            <IconButton onClick={() => setQuestionImageUrl('')}>
              <DeleteIcon className="delete-icon" />
            </IconButton>
          </Text>
        </DivWrapper>
      );
    } else {
      return (
        <Upload
          ref={uploadRef}
          customRequest={async ({ file }) => {
            handleImageUpload(file as File, null);
          }}
          maxCount={1}
          accept={"video/*,image/*"}
        >
          <DivWrapper>
            <PlusOutlined />
          </DivWrapper>
        </Upload>
      );
    }
  };

  return (
    <QuestionsWrapperComp>
      {/* Modal dialog form appending question START */}
        <QuestionFormModal 
          questionsModal={questionsModal}
          handleModalClose={handleModalClose}
          isView={isView}
          form={form}
          questions={questions}
          setQuestionsModal={setQuestionsModal}
          questionTableColumns={questionTableColumns}
          handleRowClick={handleRowClick}
        >
          <QuestionsForm
            handleSaveForm={handleSaveForm}
            form={form}
            isView={isView}
            uploadRecord={uploadRecord}
            onDeleteOption={onDeleteOption}
            handleOptionChange={handleOptionChange}
            onAddNewOption={onAddNewOption}
            allowMutipleSelect={allowMutipleSelect}
            handleSelectionOption={handleSelectionOption}
            uploadBasedRender={uploadBasedRender}
          />
        </QuestionFormModal>
      {/* Modal dialog form appending question END */}
    </QuestionsWrapperComp>
  );
};

interface QuestionFormProps {
  handleSaveForm: any;
  form: any;
  isView: any;
  uploadRecord: any;
  onDeleteOption: any;
  handleOptionChange: any;
  onAddNewOption: any;
  allowMutipleSelect: any;
  handleSelectionOption: any;
  uploadBasedRender: any;
  questions: any;
  setQuestionsModal: any;
  questionTableColumns: any;
  handleRowClick: any;
}

export const QuestionFormModal = (props: any) => {
  const {
    questionsModal,
    handleModalClose,
    isView,
    form,
    questions,
    setQuestionsModal,
    questionTableColumns,
    handleRowClick,
  } = props;
  return (
    <>
      <Modal
        title={'Question'}
        visible={questionsModal}
        closable={true}
        onCancel={handleModalClose}
        destroyOnClose={true}
        width={800}
        footer={
          <ButtonGroupWrapper>
            <Button className="breadcumb-btn-mg" onClick={handleModalClose}>
              Cancel
            </Button>
            {!isView && (
              <Button
                type="primary"
                className="breadcumb-btn-mg"
                onClick={form.submit}
              >
                Save
              </Button>
            )}
          </ButtonGroupWrapper>
        }
      >
        {props?.children}
      </Modal>
      <Row justify="space-between" style={{ width: '100%', marginTop: 20 }}>
        <Col lg={12}>
          <Typography className="sub-header">Questions</Typography>
        </Col>
        {!isView && (
          <Button
            type="link"
            icon={<PlusOutlined />}
            onClick={() => setQuestionsModal(true)}
          >
            Add New Question
          </Button>
        )}
      </Row>
      <CustomTable
        dataSource={questions}
        columns={questionTableColumns}
        onRowClick={handleRowClick}
      />
    </>
  );
};

export const QuestionsForm = (props: any) => {
  const {
    handleSaveForm,
    form,
    isView,
    uploadRecord,
    onDeleteOption,
    handleOptionChange,
    onAddNewOption,
    allowMutipleSelect,
    handleSelectionOption,
    uploadBasedRender,
    isSelectAnswer,
  } = props;

  return (
    <Form onFinish={handleSaveForm} form={form} preserve={false}>
      <LabelInput
        label="Question"
        name="question-master"
        className="label-input"
        rules={[
          {
            required: true,
            message: 'Question field is required!',
          },
        ]}
        inputElement={
          <Input
            placeholder="Type Question"
            disabled={isView}
          />
        }
      />
      {/* <Radio.Group value={'option1'} style={{ width: '100%' }}> */}
      <Row gutter={[16, 16]} style={{ marginTop: '16px' }}>
        {uploadRecord?.map((item: any, index: number) => {
          return (
            <Col span={12}>
              <UploadOptionsCard
                isAnswer={item?.isAnswer}
                index={index}
                description={item?.description}
                onDeleteOption={onDeleteOption}
                handleOptionChange={handleOptionChange}
                imageUrl={item?.imageUrl}
                isView={isView}
                isSelectAnswer={isSelectAnswer}
              />
            </Col>
          );
        })}
        {!isView && (
          <>
            <Col span={24}>
              <Button
                // type="primary"
                style={{ borderStyle: 'dashed !important' }}
                icon={<PlusOutlined />}
                onClick={() => onAddNewOption()}
              >
                <Text>Add option</Text>
              </Button>
            </Col>
              <Col span={24}>
                <Switch
                  defaultChecked={allowMutipleSelect}
                  onChange={handleSelectionOption}
                />
                &nbsp;Allow mutiple selection
              </Col>
            <Col span={24}>
                  <Text type="secondary">Question Image</Text>
                </Col>
                <Col span={24}>{uploadBasedRender()}</Col>
          </>
        )}
      </Row>
      {/* </Radio.Group> */}
    </Form>
  );
};
