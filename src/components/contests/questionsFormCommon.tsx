/*
 * WARNING
 * The component has reached its limit of reusablity.
 * requires code revamping if any outside the box functionality needs to be addded/
 * templateId conditional rendering need to be moved as parent component.
 */

import React from 'react';
import {
  Row,
  Col,
  Button,
  Form,
  Input,
  Switch,
  Upload,
  Radio,
  Typography,
  Modal
} from 'antd';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import { PlusOutlined } from '@ant-design/icons';
import { DeleteOutlined as DeleteIcon } from '@ant-design/icons';
import LabelInput from 'components/common/LabelInput';
import { UploadOptionsCard } from './uploadOptionsCard';
import { toast } from 'react-toastify';
import { s3FileUpload } from 'apis/storage';
import { PuzzleCard } from './puzzleCard';
import { ReactComponent as UploadIcon } from 'assets/icons/upload.svg';
import config from 'config';
import imageUrl from 'utils/imageURL';
import { isValidHttpUrl, matchYoutubeUrl } from 'utils/functions';
import { optionsInitialState } from './data';

const QuestionsWrapperComp = styled.div`
  .ant-radio-group {
    width: 100%;
  }
  .mb-20 {
    margin-bottom: 20px;
  }
  .column-wrapper {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .radio-center-align {
    flex: 1;
    display: flex;
    align-items: center;
  }
`;

const DivWrapper = styled.div<{ isHide?: boolean }>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: ${(props) => (props?.isHide ? '120px' : '')};
  height: ${(props) => (props?.isHide ? '120px' : '')};
  border: ${(props) => (props?.isHide ? 'dashed 1px #07697d' : '')};
  .highlight-text {
    font-size: 14px;
    color: #4da1ff;
    font-weight: 500;
  }
`;

const ImageUploader = styled.div`
  width: 100%;
  height: 120px;
  border: dashed 1px #07697d;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const IconButton = styled.div`
  cursor: pointer;
  font-size: 17px;
  .delete-icon {
    color: red;
  }
`;

const ImageWrapper = styled.div`
  width: 100%;
  height: 100%;
  .img {
    width: 100%;
    height: 100%;
    object-fit: fill;
  }
`;

interface QuestionsWrapperProps {
  data: any;
  form: any;
  isView?: boolean;
  isModal?: boolean;
  isAllowMultiSelect?: boolean;
  isImageUploadEnabled?: boolean;
  questionMasterTitle?: string;
  isSelectAnswer?: boolean;
  optionType?: string;
  minSelection?: number;
  maxSelection?: number;
  handleChange: (keyName: any, value: any) => void;
  handleAnswerUpdate?: (value: any, index: number) => void;
  templateId: number;
  isAnswerEnabled?: boolean;
  disabledAddOption?: boolean;
  isDescriptionDisabled?: boolean;
  isHideDeleteIcon?: boolean;
}

// form
export const QuestionsFormCommon = (props: QuestionsWrapperProps) => {
  const {
    form,
    data,
    isView = false,
    isAllowMultiSelect = true,
    isImageUploadEnabled = true,
    questionMasterTitle = '',
    isSelectAnswer = true,
    optionType = '',
    handleChange,
    minSelection = 1,
    handleAnswerUpdate,
    templateId,
    maxSelection,
    isAnswerEnabled = true,
    disabledAddOption=true,
    isDescriptionDisabled=false,
    isHideDeleteIcon=false
  } = props;

  let option = data?.option;

  let question = data?.question;

  React.useEffect(() => {
    if (questionMasterTitle && !data?.question) {
      handleChange('question', questionMasterTitle);
      form.setFieldsValue({ 'question-master': questionMasterTitle });
    } else {
      form.setFieldsValue({ 'question-master': data?.question });
    }
    let answerType = data?.answerType ?? 'RANGE';
    form.setFieldsValue({ answerType: answerType });
    handleChange('answerType', answerType);
  }, [questionMasterTitle]);


  const uploadRef = React.useRef(null);

  const handleOptionChange = (value: any, keyName: string, index: number) => {
    // handling multiple selection and isAnswer vaidation for multiple answers
    const isStateEditable = (keyName: string) => {
      if (keyName === 'isAnswer') {
        let checkDuplicate = option?.filter((item: any) => item.isAnswer) ?? [];
        // check whether single options moved to false
        if (checkDuplicate?.length === minSelection && value === false) {
          toast.info(`Minimum ${minSelection} answer options required`);
          return false;
        } else if (checkDuplicate?.length && !data?.canMultiSelect) {
          if (checkDuplicate?.length === 1 && value === true) {
            ///allow swapping liimting one
            return 1;
          } else {
            if (isAllowMultiSelect) {
              toast.info(
                `Allow multiple selection for selecting multiple answer options`
              );
            }else{
              toast.info(
                `Only one option allowed`
              );
            }
          }
          return false;
        }
      }
      return 2;
    };

    let optionData = option;

    let status = isStateEditable(keyName);
    if (status) {
      if (status === 1) {
        optionData?.map((item: any,index:any) => {
          if(item.isAnswer === true){
            if (handleAnswerUpdate) handleAnswerUpdate(false, index);
            item.isAnswer = false;
          }
        });
      }
      optionData[index] = {
        ...optionData[index],
        [keyName]: value,
      };
      if (handleAnswerUpdate) handleAnswerUpdate(value, index);
      handleChange('option', [...optionData]);
    }
  };

  const onAddNewOption = () => {
    let data = [
      ...option,
      {
        description: '',
      },
    ];
    handleChange('option', [...data]);
  };

  const onDeleteOption = (recordIndex: number) => {
    if (option?.length > 2) {
      // on delete form state not updated
      let modifyOption = option.reduce((total: any, item: any, index: number) => (index >= recordIndex) ? ({ ...total, ['description' + index]: option[index + 1]?.description ?? "" }) : total, {})
      form.setFieldsValue({ ...modifyOption })
      //
      option.splice(recordIndex, 1);
      handleChange('option', [...option]);
    } else {
      toast.warning(`Minimum of 2 option required`);
    }
  };

  const handleImageUpload = async (
    keyName: string,
    file: File,
    index: number | null
  ) => {
    toast.info(`Uploading the image....`);
    const obj = await s3FileUpload(file);
    if (obj?.data.statusCode !== 200) {
      toast.error(`Unable to upload the image`);
    } else {
      let url = `${config.apiBaseUrl}files/${obj?.data.fileId}`;
      handleChange(keyName, url)
      toast.success(`Image uploaded successfully`);
    }
  };

  const handleNextUpload = async(
    file: File, onProgress: Function, onSuccess: Function, callback: Function) => {
      toast.info(`Uploading the image....`);
      if(file.type === 'video/mp4'){
         const obj = await s3FileUpload(file);
         onProgress({ percent: 100 });
         if (obj?.data.statusCode !== 200) {
           toast.error(`Unable to upload the video`);
          } else {
            let url = `${config.apiBaseUrl}files/${obj?.data.fileId}`;
            callback(url)
            onSuccess(()=> console.log("onSuccess"));
        toast.success(`Video uploaded successfully`);
      }
      }else{
        const obj = await s3FileUpload(file);
        onProgress({ percent: 100 });
          if (obj?.data.statusCode !== 200) {
        toast.error(`Unable to upload the image`);
      } else {
        let url = `${config.apiBaseUrl}files/${obj?.data.fileId}`;
        callback(url)
        onSuccess(()=> console.log("onSuccess"));
        toast.success(`Image uploaded successfully`);
      }
      }
  }

  const handleSelectionOption = () => {
    if (data?.canMultiSelect) {
      let activeRecords = option?.filter((item: any) => item?.isAnswer) ?? [];
      if (activeRecords?.length > 1) {
        let data = option;
        data?.map((item: any) => {
          item.isAnswer = false;
          return true;
        });
        let record = [
          {
            ...option[0],
            isAnswer: true,
          },
          ...data.splice(1, data?.length),
        ];
        handleChange('option', [...record]);
      }
    } else {
      handleChange('canMultiSelect', !data?.canMultiSelect);
    }
  };


  const handleKeyDelete = async(keyName:any)=>{
      handleChange(keyName,'')
      if(keyName === 'answerImageUrl'){
         handleChange(keyName,'')
         changeOptions()
      }
  }

  const changeOptions =()=>{
    if(data?.answerImageUrl === ""){
      handleChange('option',optionsInitialState(5))
    }
  }



  const uploadBasedRender = (
    imageUrl: string,
    keyName: string,
    isHide?: boolean
  ) => {
    if (imageUrl && isHide) {
      return (
        <DivWrapper>
          <img style={{width:'120px',height:'120px'}} className="img" src={imageUrl} alt="options-image" />
          <Text style={{ display: 'flex', alignItems: 'center' }}>
            <a>image.jpg</a>
            <IconButton onClick={() => handleKeyDelete(keyName)}>
              <DeleteIcon className="delete-icon" />
            </IconButton>
          </Text>
        </DivWrapper>
      );
    } else if (imageUrl && !isHide) {
      return (
        <ImageWrapper>
          <img className="img" src={imageUrl} alt="options-image" />
        </ImageWrapper>
      );
    } else {
      return (
        <Upload
          ref={uploadRef}
          customRequest={async ({ file }) => {
            handleImageUpload(keyName, file as File, null);
          }}
          maxCount={1}
          accept={"video/*,image/*"}
        >
          <DivWrapper isHide={isHide}>
            {isHide ? (
              <PlusOutlined />
            ) : (
              <>
                <UploadIcon className="upload-icon" />
                <span className="highlight-text">Upload Image</span>
              </>
            )}
          </DivWrapper>
        </Upload>
      );
    }
  };



  const uploadBasedRenderNext = (
    imageUrl?: string,
    isHide?: boolean,
    uploadType?: string,
    callback:Function = () => null
  ) => {
    

      if (imageUrl && uploadType === "VIDEO" && isHide) {
        return (
          <DivWrapper>
            <video style={{width:'120px',height:'120px'}} className="img" src={imageUrl} />
            <Text style={{ display: 'flex', alignItems: 'center' }}>
              <a>video.mp4</a>
              <IconButton onClick={() => callback('')}>
                <DeleteIcon className="delete-icon" />
              </IconButton>
            </Text>
          </DivWrapper>
        );
      } else if (imageUrl && uploadType === "VIDEO" && !isHide) {
        return (
          <ImageWrapper>
            <video className="img" src={imageUrl}/>
          </ImageWrapper>
        );
      }else if (imageUrl && uploadType === "IMAGE" && isHide) {
        return(
          <DivWrapper>
          <img style={{width:'120px',height:'120px'}} className="img" src={imageUrl} alt="options-image"/>
          <Text style={{ display: 'flex', alignItems: 'center' }}>
            <a>image.jpg</a>
            <IconButton onClick={() => callback('')}>
              <DeleteIcon className="delete-icon" />
            </IconButton>
          </Text>
        </DivWrapper>
        )
      } else if(imageUrl && uploadType === "IMAGE" && !isHide){
        return (
          <ImageWrapper>
            <img className="img" src={imageUrl} alt="options-image"/>
          </ImageWrapper>
        );
      }
      else{
        return(
          <Upload
          ref={uploadRef}
          customRequest={async ({ file,onProgress,onSuccess }) => {
            handleNextUpload(file as File, onProgress as Function, onSuccess as Function, callback);
          }}
          maxCount={1}
          accept={uploadType === "VIDEO" ? "video/*" : "image/*"}
        >
          <DivWrapper isHide={isHide}>
            {isHide ? (
              <PlusOutlined />
            ) : (
              <>
                <UploadIcon className="upload-icon" />
                <span className="highlight-text">Upload Image</span>
              </>
            )}
          </DivWrapper>
        </Upload>
        )
      }
    
  
      
    // if (imageUrl && isHide) {
    //   return (
    //     <DivWrapper>
    //       <img style={{width:'120px',height:'120px'}} className="img" src={imageUrl} alt="options-image" />
    //       <Text style={{ display: 'flex', alignItems: 'center' }}>
    //         <a>image.jpg</a>
    //         <IconButton onClick={() => handleChange(keyName, '')}>
    //           <DeleteIcon className="delete-icon" />
    //         </IconButton>
    //       </Text>
    //     </DivWrapper>
    //   );
    // } else if (imageUrl && !isHide) {
    //   return (
    //     <ImageWrapper>
    //       <img className="img" src={imageUrl} alt="options-image" />
    //     </ImageWrapper>
    //   );
    // } else {
    //   return (
    //     <Upload
    //       ref={uploadRef}
    //       customRequest={async ({ file }) => {
    //         handleImageUpload(keyName, file as File, null);
    //       }}
    //       maxCount={1}
    //       accept={"video/*,image/*"}
    //     >
    //       <DivWrapper isHide={isHide}>
    //         {isHide ? (
    //           <PlusOutlined />
    //         ) : (
    //           <>
    //             <UploadIcon className="upload-icon" />
    //             <span className="highlight-text">Upload Image</span>
    //           </>
    //         )}
    //       </DivWrapper>
    //     </Upload>
    //   );
    // }
  };

  const viewBasedRenderNext = (
    imageUrl?: string,
    isHide?: boolean,
    type?: string,
    uploadType?: string,
  ) => {
    
      if (imageUrl && uploadType === "VIDEO" && isHide) {
        return (
          <DivWrapper>
            <video style={{width:'120px',height:'120px'}} className="img" src={imageUrl} />
            <Text style={{ display: 'flex', alignItems: 'center' }}>
              <a>video.mp4</a>
              <IconButton onClick={() => handleChange(type, '')}>
                <DeleteIcon className="delete-icon" />
              </IconButton>
            </Text>
          </DivWrapper>
        );
      } else if (imageUrl && uploadType === "VIDEO" && !isHide) {
        return (
          <ImageWrapper>
            <video className="img" src={imageUrl}/>
          </ImageWrapper>
        );
      }else if (imageUrl && uploadType === "IMAGE" && isHide) {
        return(
          <DivWrapper>
          <img style={{width:'120px',height:'120px'}} className="img" src={imageUrl} alt="options-image"/>
          <Text style={{ display: 'flex', alignItems: 'center' }}>
            <a>image.jpg</a>
            <IconButton onClick={() => handleChange(type,'')}>
              <DeleteIcon className="delete-icon" />
            </IconButton>
          </Text>
        </DivWrapper>
        )
      } else if(imageUrl && uploadType === "IMAGE" && !isHide){
        return (
          <ImageWrapper>
            <img className="img" src={imageUrl} alt="options-image"/>
          </ImageWrapper>
        );
      } else if(imageUrl && isValidHttpUrl(imageUrl) && isHide){
        return (
          <DivWrapper>
            <p style={{width:'120px'}}>{imageUrl}</p>
            <Text style={{ display: 'flex', alignItems: 'center',width:'120px' }}>
              <a>video link</a>
              <IconButton onClick={() => handleChange(type, '')}>
                <DeleteIcon className="delete-icon" />
              </IconButton>
            </Text>
          </DivWrapper>
          
        ) 
      }else if(imageUrl && isValidHttpUrl(imageUrl) && !isHide){
        return (
          <DivWrapper>
            <p style={{width:'120px'}}>{imageUrl}</p>
          </DivWrapper>
          
        ) 
      }
  };


  return (
    <QuestionsWrapperComp>
      <Typography className="header">Contest Questions & Options</Typography>
      <QuestionsForm
        form={form}
        isView={isView}
        option={option}
        onDeleteOption={onDeleteOption}
        handleOptionChange={handleOptionChange}
        onAddNewOption={onAddNewOption}
        allowMutipleSelect={data?.canMultiSelect}
        handleSelectionOption={handleSelectionOption}
        uploadBasedRender={uploadBasedRender}
        uploadBasedRenderNext={uploadBasedRenderNext}
        viewBasedRenderNext={viewBasedRenderNext}
        optionType={optionType}
        /// flags to be used on combination for view/hide respective elements
        isAllowMultiSelect={isAllowMultiSelect}
        isImageUploadEnabled={isImageUploadEnabled}
        questionMasterTitle={questionMasterTitle}
        isSelectAnswer={isSelectAnswer}
        handleChange={handleChange}
        question={question}
        questionImageUrl={data?.imageUrl}
        answerImageUrl={data?.answerImageUrl}
        questionUrl={data?.questionUrl}
        answerUrl={data?.answerUrl}
        teamImageUrl1={data?.teamImageUrl1}
        teamImageUrl2={data?.teamImageUrl2}
        answerType={data?.answerType}
        templateId={templateId}
        isAnswerEnabled={isAnswerEnabled}
        disabledAddOption={disabledAddOption}
        isDescriptionDisabled={isDescriptionDisabled}
        isHideDeleteIcon={isHideDeleteIcon}
        uploadQuestionType={data?.uploadQuestionType ?? "IMAGE"}
        uploadAnswerType={data?.uploadAnswerType ?? "IMAGE"}
      />
    </QuestionsWrapperComp>
  );
};

const QuestionsForm = (props: any) => {
  const {
    form,
    isView,
    option,
    onDeleteOption,
    handleOptionChange,
    onAddNewOption,
    allowMutipleSelect,
    handleSelectionOption,
    uploadBasedRender,
    uploadBasedRenderNext,
    viewBasedRenderNext,
    isAllowMultiSelect,
    isImageUploadEnabled,
    questionMasterTitle,
    isSelectAnswer,
    optionType,
    handleChange,
    question,
    questionImageUrl,
    answerImageUrl,
    questionUrl,
    answerUrl,
    teamImageUrl1,
    teamImageUrl2,
    templateId,
    answerType,
    isAnswerEnabled,
    disabledAddOption,
    isDescriptionDisabled=false,
    isHideDeleteIcon=false,
    uploadQuestionType,
    uploadAnswerType
  } = props;

  const teamImage1Ref = React.useRef();
  const teamImage2Ref = React.useRef();

const [questionsModal,setquestionModal] = React.useState(false);
const [openType,setOpenType] = React.useState('');
const [video,setVideo] = React.useState('')

const [uploadModalValue,setUploadModalValue] = React.useState<any>({
  type:"IMAGE",
  url:"",
  upload:""
})

const onChangeUploadModalValue = (keyName:string,value:string) =>{
  setUploadModalValue({
    ...uploadModalValue,
    [keyName]: value,
    ...(keyName === "type" && { upload: "" })
  })
}

const onToggleUploadModal = (type:"save"|"cancel") =>{
  if(type === "save"){
    if(openType === 'question'){
      handleChange({
        uploadQuestionType:uploadModalValue?.type,
        questionUrl:uploadModalValue?.upload || uploadModalValue?.url
      })
    }
    else{
      handleChange({
        uploadAnswerType:uploadModalValue?.type,
        answerUrl:uploadModalValue?.upload || uploadModalValue?.url
      })
    }
  }
  setquestionModal(false)
}

const openModal=(type?:any)=>{
  setOpenType(type)
  if(type === "question"){
    setUploadModalValue({
      type: uploadQuestionType,
      ...(questionUrl?.includes(config.apiBaseUrl) ? {
        upload: questionUrl,
        url: ""
      } : {
        upload: "",
        url: questionUrl,
      })
    })
  }
  else{
    setUploadModalValue({
      type:uploadAnswerType,
      ...(answerUrl?.includes(config.apiBaseUrl) ? {
        upload: answerUrl,
        url: ""
      } : {
        upload: "",
        url: answerUrl,
      })
    })
  }
  setquestionModal(true)
}



  return (
    <>
      <LabelInput
        label="Question"
        name="question-master"
        className="label-input"
        rules={[
          {
            required: true,
            message: 'Question fields is required!',
          },
        ]}
        inputElement={
          <Input
            placeholder="Type Question"
            disabled={isView}
            onChange={(e) => {
              form.setFieldsValue({ 'question-master': e.target.value });
              handleChange('question', e.target.value);
            }}
          />
        }
      />
      {(templateId === 1 || answerType === "FREE_TEXT") && (
        <Row gutter={[16, 16]} style={{ marginTop: '16px' }}>
          <Col span={12}>
            <div>Team Image 1</div>
            <ImageUploader>
              {uploadBasedRender(teamImageUrl1, 'teamImageUrl1')}
            </ImageUploader>
          </Col>
          <Col span={12}>
            <div>Team Image 2</div>
            <ImageUploader>
              {uploadBasedRender(teamImageUrl2, 'teamImageUrl2')}
            </ImageUploader>
          </Col>
        </Row>
      )}
      {templateId === 2 && (
        <>
          <div style={{ fontSize: 14, fontWeight: 500, marginTop: 10 }}>
            Type of Answer
          </div>
          <Form.Item className="mb-20" name={'answerType'}>
            <Radio.Group
              onChange={(e) => {
                handleChange('answerType', e.target.value);
              }}
              disabled={isView}
            >
              <Radio value={'RANGE'}>Range</Radio>

              <Radio value={'FREE_TEXT'}>Free Text</Radio>
            </Radio.Group>
          </Form.Item>
        </>
      )}
      <Row gutter={[16, 16]} style={{ marginTop: '16px' }}>
        {/* block uploadoption card for puzzle and free text */}
        {optionType !== 'puzzle' && (answerType !== "FREE_TEXT" || isSelectAnswer) &&
          option?.map((item: any, index: number) => {
            return (
              <Col span={12}>
                <UploadOptionsCard
                  isAnswer={item?.isAnswer}
                  index={index}
                  description={item?.description}
                  onDeleteOption={onDeleteOption}
                  handleOptionChange={handleOptionChange}
                  imageUrl={item?.imageUrl}
                  isView={answerType === "FREE_TEXT" ? false : isView}
                  isSelectAnswer={isSelectAnswer}
                  optionType={optionType}
                  isAnswerEnabled={isAnswerEnabled}
                  isDescriptionDisabled={isDescriptionDisabled}
                  isHideDeleteIcon={isHideDeleteIcon}
                />
              </Col>
            );
          })}
        {optionType === 'puzzle' && (
          <>
          <div>
          <Text type="secondary" style={{padding:10}}>Question Image</Text>
          <PuzzleCard
            url={questionImageUrl}
            handleChange={()=>false}           
            data={option}
            isView={isView}
            isQues={true}
          />
          </div>
           <div>
          <Text type="secondary" style={{padding:10}}>Answer Image</Text>
          <PuzzleCard
            url={answerImageUrl}
            handleChange={handleOptionChange}
            data={option}
            isView={isView}
          />
          </div>
  
          </>
        )}
        {(!disabledAddOption) && (
          <>
            <Col span={24}>
              <Button
                // type="primary"
                style={{ borderStyle: 'dashed !important' }}
                icon={<PlusOutlined />}
                disabled={isView}
                onClick={() => onAddNewOption()}
              >
                <Text>Add option</Text>
              </Button>
            </Col>
          </>
        )}
        {isAllowMultiSelect && (
          <Col span={24}>
            <Switch
              defaultChecked={allowMutipleSelect}
              onChange={handleSelectionOption}
              disabled={isView}
            />
            &nbsp;Allow mutiple selection
          </Col>
        )}
        {isImageUploadEnabled && (
          <>
          { templateId ===  4 ? 
          <div style={{display:"flex",paddingBottom: '10px'}}>
            <div>
            <Col span={24}>
              <Text type="secondary">Question Image or Video</Text>
            </Col>
            <Col span={12} style={{padding:10}}>
              {!isView && <Button onClick={()=>openModal('question')}><Text>upload</Text></Button>}
              
              <div style={{padding:10,marginLeft:25}}>
                {viewBasedRenderNext(questionUrl, !isView, 'questionUrl', uploadQuestionType)}
              </div>  
            </Col>
            </div>
            
            <div>
            <Col span={24}>
              <Text type="secondary">Answer Image or Video</Text>
            </Col>
            <Col span={12} style={{padding:10}}>
              {!isView && <Button onClick={()=>openModal('answer')}><Text>upload</Text></Button>}
              <div style={{padding:10,marginLeft:25}}>
                {viewBasedRenderNext(answerUrl, !isView, 'answerUrl', uploadAnswerType)}
              </div>
               
            </Col>

            </div>
          </div> : <>
            <Col span={24}>
              <Text type="secondary">Question Image</Text>
            </Col>
            <Col span={24}>
              <div style={{ width: '120px', height: '120px' }}>
                {uploadBasedRender(questionImageUrl, 'imageUrl', !isView)}
              </div>
            </Col>
            {templateId === 5 && <> <Col span={24} style={{marginTop:15}}>
              <Text type="secondary">Answer Image</Text>
            </Col>
            <Col span={24}>
              <div style={{ width: '120px', height: '120px' }}>
                {uploadBasedRender(answerImageUrl, 'answerImageUrl', !isView)}
              </div>
            </Col> </>}
          </> }     
          </>
        )}
      </Row>
      {/* </Radio.Group> */}
      { <Modal
        title={openType.toUpperCase()}
        visible={questionsModal}
        closable={true}
        onCancel={()=>onToggleUploadModal("cancel")}
        onOk={()=>onToggleUploadModal("save")}
        destroyOnClose={true}
      >
        <div style={{marginBottom:16}}>
         <div>
            <Text style={{marginBottom:8}} type="secondary">Type</Text>
          </div>
        <Radio.Group
          onChange={(e) => {
            onChangeUploadModalValue('type',e.target.value)
          }}
          value={uploadModalValue?.type}
          disabled={isView}
        >
          <Radio value={'IMAGE'}>Image</Radio>

          <Radio value={'VIDEO'}>Video</Radio>
        </Radio.Group>  
        </div>      
        
        <div>
          {uploadBasedRenderNext(uploadModalValue?.upload, !isView, uploadModalValue?.type, (val: string) => onChangeUploadModalValue('upload', val))}
            <div style={{marginTop:"20px"}}>
                <Text type="secondary" style={{paddingTop:10}}>Url</Text>
                <Input.Group compact>
                <Input
                   style={{
                     width: 'calc(100% - 200px)',
                     }}
                    disabled={Boolean(uploadModalValue?.upload)}
                    value={(uploadModalValue?.url)}
                   onChange={(e)=>onChangeUploadModalValue('url',e.target.value)}
                 />
                </Input.Group>         
            </div>
            <div style={{marginTop:"20px"}}>
              <a href={uploadModalValue?.upload || uploadModalValue?.url}>{uploadModalValue?.upload || uploadModalValue?.url}</a>
            </div>    
           </div>
         
      </Modal>}
    </>
  );
};
