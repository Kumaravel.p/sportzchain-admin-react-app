import React from 'react';
import styled from 'styled-components/macro';
import { Row, Col, Radio, Input, Upload } from 'antd';
import { ReactComponent as UploadIcon } from 'assets/icons/upload.svg';
import { useDebounce } from 'hooks/useDebounce';
import LabelInput from 'components/common/LabelInput';
import { DeleteOutlined } from '@ant-design/icons';
import { toast } from 'react-toastify';
import { s3FileUpload } from 'apis/storage';
import config from 'config';
import { Checkbox } from 'antd';

const UploadOptionsCardWrapper = styled.div`
  background: #eee;
  border-radius: 8px;
  margin-top: 0 !important;
  .optionImage {
    width: 80%;
    max-height: 240px;
    object-fit: contain;
    min-height: 240px;
  }
  .upload-icon,
  .delete-icon {
    cursor: pointer;
  }
  .optionDescription {
    font-weight: 500;
  }
  .ant-upload.ant-upload-select-picture-card {
    width: 32px;
    height: 32px;
  }
  .ant-upload-list-picture-card-container {
    width: 72px;
    height: 72px;
  }
`;
const FlexWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  input[type=number]::-webkit-inner-spin-button {
    visibility:hidden
  }

`;
const IconButton = styled.div`
  cursor: pointer;
  font-size: 17px;
  .delete-icon {
    color: red;
  }
`;

const ImageUploader = styled.div`
  width: 100%;
  height: 120px;
  border: dashed 1px #07697d;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const DivWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  .highlight-text {
    font-size: 14px;
    color: #4da1ff;
    font-weight: 500;
  }
`;

const ImageWrapper = styled.div`
  width: 100%;
  height: 100%;
  .img {
    width: 100%;
    height: 100%;
    object-fit: fill;
  }
`;

interface UploadOptionsCardProps {
  index: number;
  isAnswer: boolean | undefined;
  description: string;
  onDeleteOption: (recordIndex: number) => void;
  handleOptionChange: (value: any, keyName: string, index: number) => void;
  imageUrl?: string;
  isView?: boolean;
  isSelectAnswer?: boolean;
  optionType?: string;
  prefix?: string;
  isAnswerEnabled?: boolean;
  isDescriptionDisabled?: boolean;
  isHideDeleteIcon?: boolean;
}

export const UploadOptionsCard = (props: UploadOptionsCardProps) => {
  const {
    index,
    isAnswer = false,
    description,
    onDeleteOption,
    handleOptionChange,
    imageUrl,
    isView = false,
    isSelectAnswer = true,
    optionType = 'image',
    prefix = '',
    isAnswerEnabled = true,
    isDescriptionDisabled = false,
    isHideDeleteIcon = false,
  } = props;
  const [optionImageUrl, setOptionImageUrl] = React.useState('');
  const uploadRef = React.useRef(null);
  const debounce = useDebounce();

  React.useEffect(() => {
    setOptionImageUrl(imageUrl ? imageUrl : '');
  }, [imageUrl]);

  const secondMethod=(e:any)=>{
    const re = /[0-9A-F]+/g;
    if (!re.test(e.key)) {
      e.preventDefault();
    }
  }

  const handleImageUpload = async (file: File, index: number) => {
    toast.info(`Uploading the image....`);
    const obj = await s3FileUpload(file);
    if (obj?.data.statusCode !== 200) {
      toast.error(`Unable to upload the image`);
    } else {
      let url = `${config.apiBaseUrl}files/${obj?.data.fileId}`;
      handleOptionChange(url, 'imageUrl', index);
      setOptionImageUrl(url);
      toast.success(`Image uploaded successfully`);
    }
  };

  const uploadBasedRender = () => {
    if (optionImageUrl) {
      return (
        <ImageWrapper>
          <img className="img" src={optionImageUrl} alt="options-image" />
        </ImageWrapper>
      );
    } else if (!isView) {
      return (
        <Upload
          ref={uploadRef}
          customRequest={async ({ file }) => {
            handleImageUpload(file as File, index);
          }}
          maxCount={1}
          name={`option${index + 1}`}
        >
          <DivWrapper>
            <UploadIcon className="upload-icon" />
            <span className="highlight-text">Upload Image</span>
          </DivWrapper>
        </Upload>
      );
    }
  };


  return (
    <Row
      justify="space-between"
      align="middle"
      style={{ backgroundColor: '#F5F5F7', padding: 15 }}
    >
      <Col span={8}>
        <Radio checked={false}>Option {index + 1}</Radio>
      </Col>
      <Col span={16}>
        <FlexWrapper>
          {
            isAnswerEnabled && <Checkbox
              checked={isAnswer}
              disabled={!isSelectAnswer}
              onClick={(e) => handleOptionChange(!isAnswer, 'isAnswer', index)}
              style={{
                display: 'flex',
                flexDirection: 'row-reverse',
                marginRight: 10,
              }}
            >
              Answer
            </Checkbox>
          }
          {(isHideDeleteIcon ? !isHideDeleteIcon : !isView) && (
            <IconButton onClick={() => onDeleteOption(index)}>
              <DeleteOutlined className="delete-icon" />
            </IconButton>
          )}
        </FlexWrapper>
      </Col>
      <Col span={24}>
      <FlexWrapper>
        <LabelInput
          name={`description${prefix}${index}`}
          className="label-input"
          initialValue={description}
          rules={[
            {
              required: !isDescriptionDisabled,
              message: 'description field is required!',
            },
          ]}
          inputElement={

            !isAnswerEnabled ?
            <Input  
              onChange={(e) =>
                debounce(() => handleOptionChange(e.target.value, 'description', index), 800)
              }
              type='number'
              onKeyPress={(e)=>secondMethod(e)}
              placeholder="Type description"
            /> : <Input  
            onChange={(e) =>
              debounce(() => handleOptionChange(e.target.value, 'description', index), 800)
            }
            // type='number'
            // onKeyPress={(e)=>secondMethod(e)}
            disabled={isDescriptionDisabled}
            placeholder="Type description"
          />
          }
        />
        </FlexWrapper>
      </Col>
      {optionType === 'image' && (
        <Col span={24}>
          <ImageUploader>{uploadBasedRender()}</ImageUploader>
        </Col>
      )}
    </Row>
  );
};
