import React from 'react';
import styled from 'styled-components/macro';
import {
  Row,
  Typography,
  Input,
  Radio,
  Col,
  Divider,
  Form,
  FormInstance,
} from 'antd';
import LabelInput from 'components/common/LabelInput';

const TransparentBg = styled.div`
  background: rgba(245, 245, 247, 1);
  width: 100%;
  height: 100%;
  display: flex;
  padding: 15px;
  padding-left: 20px;
  border-radius: 12px;
`;

const WinningFormWrapper = styled.div`
  .ant-radio-group {
    width: 100%;
  }
  .mb-20 {
    margin-bottom: 20px;
  }
  .column-wrapper {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .radio-center-align {
    flex: 1;
    display: flex;
    align-items: center;
  }
`;

interface WinningFormProps {
  radioOptions: any;
  form: FormInstance;
  result: any;
  isView?: boolean;
  winnerSelection:string;
}
export const WinningForm = (props: WinningFormProps) => {
  const {
    radioOptions,
    form,
    result,
    isView = false,
    winnerSelection="ALL",
  } = props;

  const [winnerEligible, setWinnerEligible] = React.useState();


  React.useEffect(() => {
    setWinnerEligible(result?.winnerEligibleCriteria ?? "ALL");
  }, [result?.winnerEligibleCriteria]);

  return (
    <WinningFormWrapper>
      <Typography className="sub-header">
        Winner Eligibility Criteria
      </Typography>
      <Form.Item
        className="mb-20"
        name={'winnerEligibleCriteria'}
        initialValue={result?.winnerEligibleCriteria ?? radioOptions?.ALL}
      >
        <Radio.Group
          onChange={(e) => {
            setWinnerEligible(e.target.value);
          }}
          disabled={isView}
        >
          <Row gutter={[12, 12]} justify="space-between">
            <Col xs={24} sm={12} md={12}>
              <TransparentBg>
                <Radio value={radioOptions?.ALL}>
                  Answer all the questions correctly{' '}
                </Radio>
              </TransparentBg>
            </Col>
            {result?.contestTypeId === 'Quiz' && (
              <Col xs={24} sm={12} md={12}>
                <TransparentBg>
                  <div>
                    <Radio value={radioOptions?.SELECTED}>
                      Answer Part of the questions correctly{' '}
                    </Radio>
                    {winnerEligible === radioOptions?.SELECTED && (
                      <LabelInput
                        name="noOfQuestionAnswerCorrectly"
                        className="label-input"
                        initialValue={result?.noOfQuestionAnswerCorrectly}
                        rules={[
                          {
                            required: true,
                            message: 'Questions Count is required',
                          },
                        ]}
                        inputElement={
                          <Input
                            style={{ marginTop: 10 }}
                            placeholder="Number of questions to be answered correctly"
                          />
                        }
                      />
                    )}
                  </div>
                </TransparentBg>
              </Col>
            )}
          </Row>
        </Radio.Group>
      </Form.Item>
      <Typography className="sub-header">Winner Selection Criteria</Typography>
      <Form.Item
        className="mb-20"
        name={'winnerSelectionCriteria'}
        initialValue={result?.winnerSelectionCriteria ?? radioOptions?.ALL}
      >
        <Radio.Group
          // onChange={(e) => {
          //   setWinnerSelection(e.target.value);
          // }}
          disabled={isView}
        >
          <Row gutter={[12, 12]} justify="space-between">
            <TransparentBg>
              <Radio value={radioOptions?.ALL}>
                <Typography className="body1">
                  All the eligible users are winners
                </Typography>
              </Radio>
            </TransparentBg>
          </Row>
          {winnerEligible === radioOptions?.ALL && (
            <Row gutter={[12, 12]} justify="space-between">
              <TransparentBg style={{ marginTop: '20px' }}>
                <Row
                  // gutter={[12, 12]}
                  justify="space-between"
                  style={{ width: '100%' }}
                >
                  <Col xs={24} sm={10} md={10}>
                    <Radio value={radioOptions?.SELECTED}>
                      <Typography className="body1">
                        Particular number of users are winners
                      </Typography>
                    </Radio>
                    {winnerSelection === radioOptions?.SELECTED && (
                      <LabelInput
                        name="noOfWinners"
                        className="label-input"
                        initialValue={result?.noOfWinners}
                        rules={[
                          {
                            required: true,
                            message: 'Winners Count is required',
                          },
                        ]}
                        inputElement={
                          <Input
                            style={{ marginTop: 10 }}
                            placeholder="Enter number of winners for the contest"
                            disabled={isView}
                            type="number"
                          />
                        }
                      />
                    )}
                  </Col>
                  <Col xs={24} sm={2} md={2}>
                    <Divider type="vertical" style={{ height: '100%' }} />
                  </Col>
                  {
                    winnerSelection === radioOptions?.SELECTED && <Col xs={24} sm={10} md={10}>
                      <div className="column-wrapper">
                        <Typography className="body1">Pick Winners By</Typography>
                        <Form.Item
                          name={'winnerBy'}
                          initialValue={result?.winnerBy ?? 'TIME_BASED'}
                        >
                          <Radio.Group
                            className="radio-center-align"
                            value={'TIME_BASED'}
                            disabled={isView}
                          >
                            <Radio value="LOTTERY">Lottery</Radio>
                            <Radio value="TIME_BASED">Time-based</Radio>
                          </Radio.Group>
                        </Form.Item>
                      </div>
                    </Col>
                  }
                </Row>
              </TransparentBg>
            </Row>
          )}
        </Radio.Group>
      </Form.Item>
    </WinningFormWrapper>
  );
};
