import moment from "moment";



export const viewGenJSON = (data)=>{
  
  const getValueByType = (team, group) =>{
    return data?.type === "GROUPS" ? group : team
  }

    let contestDetails = [{
        title:"Contest Title",
        valuesArr:[
          {
            lable: "Title",
            value: data?.title ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: "ID",
            value: getValueByType(data?.teamId,'-') ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: "Status of Contest",
            value: data?.statusId ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: `${getValueByType("Team","Group")} Name`,
            value: getValueByType(data?.teamName,data?.groupByGroupId?.groupName) ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: "Contest Type",
            value: data?.contestTypeId ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: "Contest Platform",
            value: data?.platformTypeId ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: "Start Date & Time",
            value: data?.startAt ? moment(data?.startAt + "z").format('MMMM D YYYY, HH:mm') : '-',
            col: 8,
            divider: true
          },
          {
            lable: "End Date & Time",
            value: data?.endAt ? moment(data?.endAt + "z").format('MMMM D YYYY, HH:mm') : '-',
            col: 8,
            divider: true
          },
          {
            lable: "Display Start Date & Time",
            value: data?.displayStartAt ? moment(data?.displayStartAt + "z").format('MMMM D YYYY, HH:mm') : '-',
            col: 8,
            divider: true
          },
          {
            lable: "Display End Date & Time",
            value: data?.displayEndAt ? moment(data?.displayEndAt + "z").format('MMMM D YYYY, HH:mm') : '-',
            col: 8,
            divider: true
          },
          {
            lable: "Currency Symbol",
            value: data?.coinSymbol ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: "Position",
            value: data?.orderBy ?? '-',
            col: 8,
            divider: true
          },
          {
            lable: "Contest Description",
            value: data?.description ?? '-',
            col: 16,
            divider: true
          },
        ]
    }]
    let participationCriteria = [
      {
        title:"Participation Requirement",
        valuesArr:[
          {
            lable: "Minimum Joining Balance",
            value: `${data?.minJoinBalance ?? '-'} ${data?.coinSymbol ?? ''}`,
            col: 12,
            divider: true
          },
          {
            lable: "Number of Participants Allowed",
            value: data?.noParticipantAllowed ?? '-',
            col: 12,
            divider: true
          }
        ]
      }
    ]
    let result = {
      contestDetails,
      participationCriteria
    }
    return result;
}

export default viewGenJSON;