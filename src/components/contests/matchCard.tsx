import React from 'react';
import styled from 'styled-components/macro';
import { Row, Col, Typography, Avatar, Input } from 'antd';
import SelectElement from 'components/common/SelectElement';
import LabelInput from 'components/common/LabelInput';
import { ReactComponent as StadiumIcon } from 'assets/icons/stadium.svg';
import { ReactComponent as CalenderIcon } from 'assets/icons/calendar.svg';
import {
  useGetAllTeamMatchesLazyQuery,
  useGetAllTeamLeaguesQuery,
} from 'generated/pgraphql';
import imageUrl from 'utils/imageURL';
import moment from 'moment';
const MatchCardWrapper = styled('div')`
  width: 100%;
  .border-highlight {
    width: 100%;
    height: 20px;
    margin-top: 10px;
    border: solid 2px black;
  }
`;

const IconWrapper = styled('div')`
  display: flex;
  align-items: center;
  svg {
    margin-right: 10px;
  }
`;

const MatchSlashWrapper = styled('div')`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  .ant-avatar {
    margin-right: 10px;
  }
  .logoWrapper {
    display: flex;
    align-items: center;
  }
`;

interface matchCardProps {
  form: any;
  data: any;
  isView?: boolean;
  handleChange?: (matchRecord: any) => void;
}

export const MatchCard = (props: matchCardProps) => {
  const { form, data, isView = false, handleChange } = props;
  
  const {
    data: leagueDetails,
    loading: leagueDetailsLoading,
  } = useGetAllTeamLeaguesQuery();

  const [
    getMatchDetails,
    { data: matchDetails, loading: matchDetailsLoading },
  ] = useGetAllTeamMatchesLazyQuery();

  React.useEffect(()=>{
    let leagueId = form.getFieldValue('leagueId');
    if(leagueId && !(!!matchDetails?.allMatches?.nodes)){
      getMatchDetails({
        variables: { equalTo: leagueId },
      });
    }
  },[form])

  const filterSelectedMatch = (matchDetails: any, matchId: any) => {
    let data = matchDetails?.allMatches?.nodes?.find(
      (item: any) => item?.id === matchId?.key
    );
    let team1Color: string = data?.tournamentGroupMemberByParticipant1?.teamByTeamId?.primaryColor || 'black';
    let team2Color = data?.tournamentGroupMemberByParticipant2?.teamByTeamId?.primaryColor || 'black';
    form.setFieldsValue({
      team1Color,
      team2Color
    })
    if (handleChange) {
      handleChange({ ...data, team1Color, team2Color });
    }
  };

  return (
    <MatchCardWrapper>
      <Row gutter={[12, 12]} justify="space-between">
        <Col xs={24} sm={12} md={12}>
          <LabelInput
            label="League"
            name="leagueId"
            className="label-input"
            required
            rules={[
              {
                required: true,
                message: 'League Field is required!',
              },
            ]}
            // initialValue={data?.leagueId}
            inputElement={
              <SelectElement
                // defaultValue={data?.leagueId}
                onChange={(e: any) => {
                  getMatchDetails({
                    variables: { equalTo: e },
                  });
                  // form.setFieldsValue({
                  //   leagueId: e,
                  // });
                }}
                loading={leagueDetailsLoading}
                options={
                  leagueDetailsLoading
                    ? []
                    : ((leagueDetails?.allTeamLeagues?.nodes as unknown) as [])
                }
                placeholder="Select Team Name"
                searchable={true}
                toFilter="description"
                toStore="leagueName"
                // labelInValue={true}
                disabled={isView}
              />
            }
          />
        </Col>
        <Col xs={24} sm={12} md={12}>
          {
            data?.id && isView ?
              <LabelInput
                label="Match"
                className="label-input"
                required
                rules={[
                  {
                    required: true,
                    message: 'Match Field is required!',
                  },
                ]}
                
                initialValue={data?.name}
                inputElement={
                  <SelectElement
                    allowClear={true}
                    defaultValue={data?.name}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      filterSelectedMatch(matchDetails, e);
                      form.setFieldsValue({ matchId: e });
                    }}
                    loading={matchDetailsLoading}
                    options={(matchDetails?.allMatches?.nodes as unknown) as []}
                    placeholder="Select Team Name"
                    searchable={true}
                    toFilter="name"
                    toStore="id"
                    labelInValue={true}
                    disabled={true}
                  />
                }
              />
              :
              <LabelInput
                label="Match"
                name="matchId"
                className="label-input"
                required
                rules={[
                  {
                    required: true,
                    message: 'Match Field is required!',
                  },
                ]}
                initialValue={data?.matchName}
                inputElement={
                  <SelectElement
                    defaultValue={data?.name}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      filterSelectedMatch(matchDetails, e);
                      form.setFieldsValue({ matchId: e });
                    }}
                    loading={matchDetailsLoading}
                    options={(matchDetails?.allMatches?.nodes as unknown) as []}
                    placeholder="Select Team Name"
                    searchable={true}
                    toFilter="name"
                    toStore="id"
                    labelInValue={true}
                    disabled={isView}
                  />
                }
              />
          }
        </Col>
        <Col xs={24}>
          {data?.id && (
            <Row gutter={[12, 12]} justify="space-between">
              <Col
                xs={24}
                sm={4}
                md={10}
                style={{
                  backgroundColor: '#EBEBEB',
                  paddingLeft: 24,
                  paddingTop: 18,
                  paddingBottom: 12,
                }}
              >
                <IconWrapper style={{ marginBottom: 10 }}>
                  <StadiumIcon />
                  <Typography>
                    {data?.startDate
                      ? moment(data?.startDate).format(
                        'DD MMMM YYYY, HH:mm A Z'
                      )
                      : '-'}
                  </Typography>
                </IconWrapper>
                <IconWrapper>
                  <CalenderIcon />
                  <Typography>{data?.location}</Typography>
                </IconWrapper>
              </Col>
              <Col
                xs={24}
                sm={8}
                md={14}
                style={{ backgroundColor: '#F5F5F5' }}
              >
                <MatchSlashWrapper>
                  <div>
                    <div className="logoWrapper">
                      <Avatar size={45} src={imageUrl(data?.tournamentGroupMemberByParticipant1?.teamLogo)}>
                        {data?.tournamentGroupMemberByParticipant1?.teamName?.[0]}
                      </Avatar>
                      <Typography>{data?.tournamentGroupMemberByParticipant1?.teamName}</Typography>
                    </div>
                  </div>
                  <Typography
                    style={{
                      fontSize: 18,
                      color: '#737373',
                      marginLeft: 8,
                      marginRight: 8,
                    }}
                  >
                    VS
                  </Typography>
                  <div>
                  <div className="logoWrapper">
                      <Avatar size={45} src={imageUrl(data?.tournamentGroupMemberByParticipant2?.teamLogo)}>
                        {data?.tournamentGroupMemberByParticipant2?.teamName?.[0]}
                      </Avatar>
                      <Typography>{data?.tournamentGroupMemberByParticipant2?.teamName}</Typography>
                    </div>
                  </div>
                </MatchSlashWrapper>
              </Col>
              <Col xs={23}>
                <Typography>Team Colors</Typography>
                <Row gutter={[10, 12]}>
                  <Col xs={10} sm={8} md={8}>
                    <LabelInput
                      name="team1Color"
                      // initialValue={data?.team1Color}
                      inputElement={
                        <Input
                          // value={data?.team1Color}
                          disabled={isView}
                          // onChange={(e) => {
                          //   if (handleChange)
                          //     handleChange({ team1Color: e.target.value });
                          // }}
                          type="color"
                        />
                      }
                    />
                  </Col>
                  <Col xs={10} sm={8} md={8}>
                    <LabelInput
                      name="team2Color"
                      // initialValue={data?.team2Color}
                      inputElement={
                        <Input
                          // value={data?.team2Color}
                          disabled={isView}
                          // onChange={(e) => {
                          //   if (handleChange)
                          //     handleChange({ team2Color: e.target.value });
                          // }}
                          type="color"
                        />
                      }
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          )}
        </Col>
      </Row>
    </MatchCardWrapper>
  );
};
