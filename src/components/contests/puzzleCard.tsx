import { Checkbox, Typography } from 'antd';
import React from 'react';
import styled from 'styled-components/macro';

const PuzzleWrapper = styled.div<{ src: string }>`
  background: url(${(p) => p.src}) no-repeat;
  background-size: 100%;
  width: 399px;
  height: 399px;
  background-size: cover;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  .check-box {
    color: #fff;
    z-index: 14;
  }
  div {
    opacity: 1;
    padding: 10px;
    background-color: ${(p) => (p.src ? 'transparent' : '#E8E8E8')};
    border: solid 1px white;
    text-align:right;
    ${(p) =>
      p.src &&
      `
    &:hover {
        opacity: 0.4;
        background: white;
      }
    `}
    }
`;

interface PuzzleCardProps {
  handleChange: (value: any, keyName: string, index: number) => void;
  data?: any[];
  url: string;
  isView?: boolean;
  isQues?:boolean
}

export const PuzzleCard = (props: PuzzleCardProps) => {
  const { handleChange, data, url, isView = false,isQues = false } = props;

  React.useEffect(() => {});

  return (
    <div style={{ padding: '0px 8px' }}>
      <PuzzleWrapper src={url}>
        {data?.map((item: any, index) => {
          return (
            <div
              onClick={() => {
                if(!isView) handleChange(!item?.isAnswer, 'isAnswer', index)
              }}
            >
                <Typography className="check-box">
                  {!isQues && <Checkbox checked={item?.isAnswer} />}        
                </Typography>
            </div>
          );
        })}
      </PuzzleWrapper>
    </div>
  );
};
