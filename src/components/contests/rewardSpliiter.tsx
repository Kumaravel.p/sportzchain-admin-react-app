import React from 'react';
import CustomTable from 'components/customTable';
import { rankBasedDataStructure, rangeBasedDataStructure } from './data';

interface rewardSplitterProps {
  type: string;
  tableTypeOptions: { equal: string; range: string };
  rankBasedRewardDataSource: rankBasedDataStructure[];
  rangeBasedRewardDataSource: rangeBasedDataStructure[];
  handleNewRecord: (newRecord: any, rowIndex: number | null, type: "edit" | "delete") => void;
  isView: boolean;
}

export const RewardSplitter = (props: rewardSplitterProps) => {
  const {
    type = '',
    tableTypeOptions,
    rankBasedRewardDataSource,
    rangeBasedRewardDataSource,
    handleNewRecord,
    isView,
  } = props;

  const rewardColumns: any = {
    [tableTypeOptions.equal]: [
      {
        title: 'RANK',
        dataIndex: 'rank',
        component : "RANK",
        sorter: (a:any,b:any) => a.rank - b.rank,
      },
      {
        title: 'REWARD VALUE',
        dataIndex: 'rewardValue',
        component: 'EDITABLE',
        sorter: (a:any,b:any) => a.rewardValue - b.rewardValue,
        suffix: 'USD',
        options: {
          suffix: (
            <span
              style={{
                fontSize: 14,
                color: '#4DA1FF',
                fontWeight: '500',
              }}
            >
              USD
            </span>
          ),
          type: 'number',
        },
      },
      {
        title: 'RWRD QUANTITY',
        dataIndex: 'rewardQuantity',
        sorter: (a:any,b:any) => a.rewardQuantity - b.rewardQuantity,
      },
      {
        title: '',
        dataIndex: '',
        component: 'INSERT',
      },
    ],
    [tableTypeOptions.range]: [
      {
        title: 'Winners Rank from',
        dataIndex: 'rankFrom',
        component: 'EDITABLE',
        sorter: (a:any,b:any) => a.rankFrom - b.rankFrom,
      },
      {
        title: 'Winners Rank till',
        dataIndex: 'rankTo',
        component: 'EDITABLE',
        sorter: (a:any,b:any) => a.rankTo - b.rankTo,
      },
      {
        title: 'REWARD VALUE',
        dataIndex: 'rewardValue',
        component: 'EDITABLE',
        suffix: 'USD',
        sorter: (a:any,b:any) => a.rewardValue - b.rewardValue,
        options: {
          suffix: (
            <span
              style={{
                fontSize: 14,
                color: '#4DA1FF',
                fontWeight: '500',
              }}
            >
              USD
            </span>
          ),
          type: 'number',
        },
      },
      {
        title: 'RWRD QUANTITY',
        dataIndex: 'rewardQuantity',
        sorter: (a:any,b:any) => a.rewardQuantity - b.rewardQuantity,
      },
      {
        title: '',
        dataIndex: '',
        component: 'INSERT',
      },
    ],
  };

  const getDataSource: any = () => {
    if (type == tableTypeOptions?.equal) {
      return rankBasedRewardDataSource;
    } else {
      return rangeBasedRewardDataSource;
    }
  };

  const getRewardColms = (type: string) => {
    // if view enabled ! remove edit button column
    if (type) {
      if (!isView) return rewardColumns[type];
      let data = [...rewardColumns[type]];
      data.splice(-1, 1);
      if (isView) return data;
    }
  };

  return (
    <>
      <CustomTable
        dataSource={
          type == tableTypeOptions?.equal
            ? rankBasedRewardDataSource
            : rangeBasedRewardDataSource
        }
        columns={getRewardColms(type)}
        onRowSave={handleNewRecord}
        hidePagination={true}
        onRowClick={(record: any) => {
          return {
            onClick: () => {
              ///row click action
            },
          };
        }}
      />
    </>
  );
};
