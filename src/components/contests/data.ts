export const rankBasedRewardDataSource: rankBasedDataStructure[] = [
  // {
  //   rank: '1',
  //   rewardValue: '0.00',
  //   rewardQuantity: 0,
  // },
  // {
  //   rank: '2',
  //   rewardValue: '0.00',
  //   rewardQuantity: 0,
  // },
  // {
  //   rank: '3',
  //   rewardValue: '0.00',
  //   rewardQuantity: 0,
  // },
];

export const optionsInitialState = (optionId: any) => {
  switch (optionId) {
    case 2: 
      return [{ description: '' }, { description: '' }]; 
    case 5:
      return [{ isAnswer: false }, { isAnswer: false }, { isAnswer: false }, { isAnswer: false }, { isAnswer: false }, { isAnswer: false }, { isAnswer: false }, { isAnswer: false }, { isAnswer: false }];
    case 6:
      return [
        {
          description: '',
          isAnswer: true,
        }
      ];
    default:
      return [
        {
          description: '',
          isAnswer: false,
        },
        {
          description: '',
          isAnswer: false,
        },
      ];
  }
};

export interface rankBasedDataStructure {
  rank: string;
  rewardValue: string | number;
  rewardQuantity: number;
}

export interface rangeBasedDataStructure {
  rankFrom: number | null;
  rankTo: number | null;
  rewardValue: string | number;
  rewardQuantity: number;
}

const constructOption = (item: any) => {
  return {
    id: item?.id ?? undefined,
    description: item?.description ?? undefined,
    imageUrl: item?.imageUrl ?? undefined,
    answerImageUrl: item?.answerImageUrl ?? undefined,
    questionUrl: item?.questionUrl ?? undefined,
    answerUrl: item?.answerUrl ?? undefined,
    isActive: item?.isActive ?? false,
    isAnswer: item?.isAnswer ?? undefined,
    optionId: item?.id
  }
}

export const constructQuestionState = (questionsData: any, optionKeyName: string, templateId?: number) => {
  let questions: any = []
  if (questionsData && questionsData?.length) {
    questionsData?.map((item: any) => {
      let options: any = [];
      let teamOptions: any = {
        team1: {
          options: []
        },
        team2: {
          options: []
        }
      };
      item?.[optionKeyName]?.nodes.forEach(
        (item1: any) => {
          // splicing up team option seperately
          if (templateId === 3) {
            if (item1?.team1Name) {
              teamOptions.team1.options.push(constructOption(item1))
            } else {
              teamOptions.team2.options.push(constructOption(item1))
            }
          } else {
            options.push(constructOption(item1))
          }
        }
      );
      let construct: any = {
        answerType: item?.answerType ?? undefined,
        questionId: item?.id,
        id: item?.id,
        question: item?.question ?? undefined,
        canMultiSelect: item?.canMultiSelect ?? undefined,
        isActive: item?.isActive ?? undefined,
        imageUrl: item?.imageUrl ?? undefined,
        answerImageUrl: item?.answerImageUrl ?? undefined,
        questionUrl: item?.questionUrl ?? undefined,
        answerUrl: item?.answerUrl ?? undefined,
        teamImageUrl1: item?.teamImageUrl1 ?? undefined,
        teamImageUrl2: item?.teamImageUrl2 ?? undefined,
        uploadQuestionType:item?.questionType ?? undefined,
        uploadAnswerType:item?.answerType ?? undefined,

      };
      if (templateId === 3) {
        construct = {
          ...construct,
          ...teamOptions
        };
      } else {
        construct["option"] = options;
      }
      questions.push(construct);
      return true;
    });
  }
  return questions;
}

export const questionTableManagement = (nodes: any, templateId: number) => {
  if (templateId === 0) {
    return constructQuestionState(
      nodes,
      'contestQuestionOptionsByContestQuestionId'
    );
  } else if (templateId === 1) {
    return constructQuestionState(
      nodes?.contestMatchPredictQusesByContestId?.nodes,
      'contestMatchPredictQusOptionsByQuestionId'
    );
  } else if (templateId === 2) {
    return constructQuestionState(
      nodes?.contestScorePredictQusesByContestId?.nodes,
      'contestScorePredictQusOptionsByQuestionId'
    );
  }
  else if (templateId === 3) {
    return constructQuestionState(
      nodes?.contestManoftheMatchQusesByContestId?.nodes,
      'contestManoftheMatchQusOptionsByQuestionId',
      3
    );
  }
  else if (templateId === 4) {
    return constructQuestionState(
      nodes?.contestHappenNextQusesByContestId?.nodes,
      'contestHappenNextQusOptionsByQuestionId'
    );
  }
  else if (templateId === 5) {
    return constructQuestionState(
      nodes?.contestSpotBallQusesByContestId?.nodes,
      'contestSpotBallQusOptionsByQuestionId'
    );
  }
  else if (templateId === 6) {
    return constructQuestionState(
      nodes?.contestPlayerPositionQusesByContestId?.nodes,
      'contestPlayerPositionQusOptionsByQuestionId'
    );
  }
};

export const filterQuestionData = (nodes: any, templateId: number) => {
  if (templateId === 0) {
    return nodes
  } else if (templateId === 1) {
    let data = nodes?.contestMatchPredictQusesByContestId?.nodes?.[0];
    return [{
      ...data,
      option: data?.["contestMatchPredictQusOptionsByQuestionId"]?.nodes
    }]
  } else if (templateId === 2) {
    let data = nodes?.contestScorePredictQusesByContestId?.nodes?.[0];
    return [{
      ...data,
      option: data?.["contestScorePredictQusOptionsByQuestionId"]?.nodes
    }]
  } else if (templateId === 3) {
    let data = nodes?.contestManoftheMatchQusesByContestId?.nodes?.[0];
    return [{
      ...data,
      option: data?.["contestManoftheMatchQusOptionsByQuestionId"]?.nodes
    }]
  }
  else if (templateId === 4) {
    let data = nodes?.contestHappenNextQusesByContestId?.nodes?.[0];
    return [{
      ...data,
      option: data?.["contestHappenNextQusOptionsByQuestionId"]?.nodes
    }]

  } else if (templateId === 5) {
    let data = nodes?.contestSpotBallQusesByContestId?.nodes?.[0];
    return [{
      ...data,
      option: data?.["contestSpotBallQusOptionsByQuestionId"]?.nodes
    }]
  } else if (templateId === 6) {
    return constructQuestionState(
      nodes?.contestPlayerPositionQusesByContestId?.nodes,
      'contestPlayerPositionQusOptionsByQuestionId'
    );
  }
};