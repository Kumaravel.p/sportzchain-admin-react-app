import { useEffect, useState } from 'react';
import { Form, Input, Typography, Row, Col, Button } from 'antd';
import Text from 'antd/lib/typography/Text';
import styled from 'styled-components/macro';
import LabelInput from 'components/common/LabelInput';
import { UploadOptionsCard } from './uploadOptionsCard';
import { PlusOutlined } from '@ant-design/icons';

export interface ManOfMatchFormProps { }

const ManOfMatchFormWrapperComp = styled.div`
  .ant-radio-group {
    width: 100%;
  }
  .mb-20 {
    margin-bottom: 20px;
  }
  .column-wrapper {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .radio-center-align {
    flex: 1;
    display: flex;
    align-items: center;
  }
  .base-text {
    color: #051f2499;
    font-size: 14px;
    font-weight: 500;
    margin-top:18px;
  }
`;


export const ManOfMatchForm = (props: {
  form: any;
  handleChange: (keyName:any,value:any ) => void;
  handleAnswerUpdate: (value: any, index: number , teamType : string) => void;
  questionMasterTitle: string;
  isSelectAnswer: boolean;
  isView: boolean;
  data:any;
  isDescriptionDisabled?: boolean;
}) => {
  const { form, handleChange, handleAnswerUpdate, questionMasterTitle, isSelectAnswer, isView, data, isDescriptionDisabled=false } = props;

  useEffect(() => {
    form.setFieldsValue({ 'question-master': questionMasterTitle });
  }, []);

  const handleTeam1Options = (value: any, keyName: string, index: number) => {
    if (keyName === "isAnswer") {
      handleSelectAnswer(value, index, "team1")
      return false
    }
    let questionMaster = data.team1;
    questionMaster.options[index][keyName] = value;
    handleChange("team1",questionMaster);
  };

  const handleTeam2Options = (value: any, keyName: string, index: number) => {
    if (keyName === "isAnswer") {
      handleSelectAnswer(value, index, "team2")
      return false
    }
    let questionMaster = data.team2;
    questionMaster.options[index][keyName] = value;
    handleChange("team2",questionMaster);
  };

  const handleSelectAnswer = (value: any, index: number, keyName: string) => {

    const { team1 = [], team2 = [] } = data;

    const findIsAnswerInTeam = (teamData: any[] = []) => teamData?.some((_: any) => _?.isAnswer);

    const setIsAnswerFalseForAllTeams = (teamData: any[] = []) => {
      return teamData?.map((_: any) => {
        if (_?.isAnswer) {
          _.isAnswer = false
        }
        return _
      })
    }

    let isAnswerInTeam1 = findIsAnswerInTeam(team1?.options);
    let isAnswerInTeam2 = findIsAnswerInTeam(team2?.options);
    if (isAnswerInTeam1) {
      team1.options = setIsAnswerFalseForAllTeams(team1?.options)
    }
    if (isAnswerInTeam2) {
      team2.options = setIsAnswerFalseForAllTeams(team2?.options)
    }

    data[keyName].options[index]['isAnswer'] = value

    handleAnswerUpdate(value, index, keyName)
    handleChange("answer", { team1, team2 });

  }

  const onDeleteOption = (recordIndex: number , keyName:string) => {
    let options = data[keyName];
    // on delete form state not updated
    let modifyOption = options?.options?.reduce((total: any, item: any, index: number) => (index >= recordIndex) ? ({ ...total, [`description_${keyName}_` + index]: options?.options?.[index + 1]?.description ?? "" }) : total, {})
    form.setFieldsValue({ ...modifyOption })
    //
    options.options.splice(recordIndex,1)
    handleChange(keyName,options);
  };

  const onAddNewOption = (keyName:string) => {
    let options = data[keyName];
    options.options.push({
        description: '',
        isAnswer: false,
    })
    handleChange(keyName,options);
  };

  return (
    <>
      <Typography className="header">Contest Questions & Options</Typography>
      <ManOfMatchFormWrapperComp>
        <LabelInput
          label="Question"
          name="question-master"
          className="label-input"
          rules={[
            {
              required: true,
              message: 'Question fields is required!',
            },
          ]}
          inputElement={
            <Input
              placeholder="Type Question"
              disabled={isView}
              onChange={(e) => {
                form.setFieldsValue({ 'question-master': e.target.value });
              }}
            />
          }
        />
        <div>
          <div className="base-text">Team 1 players</div>
          <Row gutter={[16, 16]} style={{ marginTop: '16px' }}>
              {
                data?.team1?.options?.map((item: any, index: number) => {
                  return (
                    <Col span={12}>
                      <UploadOptionsCard
                        isAnswer={item?.isAnswer}
                        index={index}
                        description={item?.description}
                        onDeleteOption={(recordIndex:number)=>onDeleteOption(index,"team1")}
                        handleOptionChange={(value:string,keyName:string,index:number)=>handleTeam1Options(value,keyName,index)}
                        imageUrl={item?.imageUrl}
                        isView={isView}
                        isSelectAnswer={isSelectAnswer}
                        optionType={"image"}
                        prefix={"_team1_"}
                        isDescriptionDisabled={isDescriptionDisabled}
                      />
                    </Col>
                  )
                })
              }
          </Row>
          {
            !isView && 
            <Col span={24}>
              <Button
                // type="primary"
                style={{ borderStyle: 'dashed !important', marginTop:16 }}
                icon={<PlusOutlined />}
                disabled={isView}
                onClick={() => onAddNewOption("team1")}
              >
                <Text>Add option</Text>
              </Button>
            </Col>
            }
        </div>
        <div>
          <div className="base-text">Team 2 players</div>
          <Row gutter={[16, 16]} style={{ marginTop: '16px' }}>
              {
                data?.team2?.options?.map((item: any, index: number) => {
                  return (
                    <Col span={12}>
                      <UploadOptionsCard
                        isAnswer={item?.isAnswer}
                        index={index}
                        description={item?.description}
                        onDeleteOption={(recordIndex:number)=>onDeleteOption(index,"team2")}
                        handleOptionChange={(value:string,keyName:string,index:number)=>handleTeam2Options(value,keyName,index)}
                        imageUrl={item?.imageUrl}
                        isView={isView}
                        isSelectAnswer={isSelectAnswer}
                        optionType={"image"}
                        prefix={"_team2_"}
                        isDescriptionDisabled={isDescriptionDisabled}
                      />
                    </Col>
                  )
                })
              }
          </Row>
         {
          !isView &&
          <Col span={24}>
              <Button
                // type="primary"
                style={{ borderStyle: 'dashed !important', marginTop:16 }}
                icon={<PlusOutlined />}
                disabled={isView}
                onClick={() => onAddNewOption("team2")}
              >
                <Text>Add option</Text>
              </Button>
            </Col>
            }
        </div>
      </ManOfMatchFormWrapperComp>
    </>
  );
};
