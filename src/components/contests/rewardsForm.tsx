import React, { useMemo } from 'react';
import styled from 'styled-components/macro';
import {
  Typography,
  Row,
  Radio,
  Col,
  Divider,
  Input,
  Button,
  Form,
} from 'antd';
import LabelInput from 'components/common/LabelInput';
import { PlusOutlined } from '@ant-design/icons';
import { RewardSplitter } from './rewardSpliiter';
import { rangeBasedDataStructure, rankBasedDataStructure } from './data';
import { rankBasedRewardDataSource } from './data';
import { FormInstance } from 'antd/es/form';

const RewardWarmWrapper = styled.div`
  .ant-radio-group {
    width: 100%;
  }
  .mb-20 {
    margin-bottom: 20px;
  }
  .column-wrapper {
    display: flex;
    flex-direction: column;
    height: 100%;
  }
  .radio-center-align {
    flex: 1;
    display: flex;
    align-items: center;
  }
`;

const HighlightWrapper = styled.div`
  padding: 10px;
  .highlight-text {
    margin-bottom: 2%;
    color: gray;
    font-size: 15px;
  }
  .highlight-sub {
    font-size: 24px;
    font-weight: 500;
    color: #4da1ff;
  }
`;

const tableTypeOptions: { equal: string; range: string } = {
  equal: 'EACH_WINNER',
  range: 'RANGE_OF_WINNER',
};

interface RewardFormProps {
  radioOptions: any;
  form: FormInstance;
  result: any;
  handleTotalRewardsMaster: (record: any) => void;
  isView?: boolean;
  tableTypeOptions?: { equal: string; range: string };
  rewardType:string;
  winnerSelection:string;
  noOfWinners?:number;
}

export const RewardsForm = (props: RewardFormProps) => {
  const {
    radioOptions,
    form,
    result,
    handleTotalRewardsMaster,
    isView = false,
    rewardType="EQUAL_REWARD",
    winnerSelection="ALL",
    noOfWinners=0
  } = props;
  const [applicableType, setApplicableType] = React.useState(radioOptions?.ALL);
  const [totalReward, setTotalRewards] = React.useState<any>({
    value: 0,
    quantity: 0,
  });
  const [tableType, setTableType] = React.useState<string>(
    tableTypeOptions?.equal
  );
  const [
    rankBasedRewardDataSourceState,
    setRankBasedRewardDataSourceState,
  ] = React.useState(rankBasedRewardDataSource);
  const [
    rangeBasedRewardDataSourceState,
    setRangeBasedRewardDataSourceState,
  ] = React.useState<rangeBasedDataStructure[]>([]);

  React.useEffect(() => {
    if (result?.rewardApplicable) {
      setTotalRewards({
        value: result?.totalWinnerRewards ?? 0,
        quantity: result?.totalQuantityReward ?? 0,
      });
      let tableType = result?.rewardGivenFor ?? tableTypeOptions?.equal;
      setTableType(tableType);
      setApplicableType(result?.rewardApplicable ?? radioOptions?.ALL);
      ///set reward table data
      let winnerSplitup =
        result?.contestRewardWinnerSplitsByContestRewardId?.nodes;
      let data: any = [];
      if (winnerSplitup && winnerSplitup?.length) {
        if (tableType === tableTypeOptions?.range) {
          winnerSplitup.map((item: any) => {
            data.push({
              rankFrom: item?.rankFrom,
              rankTo: item?.rankTo,
              rewardQuantity: parseFloat?.(item?.rewardQuantity)?.toFixed(2),
              rewardValue: parseFloat?.(item?.rewardValue)?.toFixed(2),
            });
            return true;
          });
          setRangeBasedRewardDataSourceState([...data]);
        } else {
          winnerSplitup.map((item: any) => {
            data.push({
              rank: item?.rank,
              rewardQuantity: parseFloat?.(item?.rewardQuantity)?.toFixed(2),
              rewardValue: parseFloat?.(item?.rewardValue)?.toFixed(2),
            });
            return true;
          });
          data?.sort((a: any, b: any) => (a.rank < b.rank ? -1 : 1));
          setRankBasedRewardDataSourceState([...data]);
        }
      }

      // setting up up total records on parent
      let totalRecords = {
        value: result?.totalWinnerRewards ?? 0,
        quantity: result?.totalQuantityReward ?? 0,
        data: data,
      };
      handleTotalRewardsMaster(totalRecords);
    }
  }, [result]);

  React.useEffect(() => {
    if (result?.rewardApplicable && result?.rewardGivenFor) {
      setTableType(result?.rewardGivenFor)
    }
    else {
      setTableType(tableTypeOptions?.equal)
    }
  }, [rewardType, result])

  React.useEffect(()=>{
    if(tableType === "EACH_WINNER"){
      handleTotalRewards(rankBasedRewardDataSourceState)
    }
    else if(tableType === "RANGE_OF_WINNER"){
      handleTotalRewards(rangeBasedRewardDataSourceState)
    }
  },[tableType])

  const onRowAdd = () => {
    let newData = [];
    if (tableType === tableTypeOptions?.range) {
      let newRecord: rangeBasedDataStructure = {
        rankFrom: null,
        rankTo: null,
        rewardValue: 0.0,
        rewardQuantity: 0,
      };
      newData = [
        ...rangeBasedRewardDataSourceState,
        newRecord,
      ]
      setRangeBasedRewardDataSourceState(newData);
    } else if (tableType === tableTypeOptions?.equal) {
      let newRecord: rankBasedDataStructure = {
        rank: `${rankBasedRewardDataSourceState?.length + 1}`,
        rewardValue: '0.00',
        rewardQuantity: 0,
      };
      newData = [
        ...rankBasedRewardDataSourceState,
        newRecord,
      ]
      setRankBasedRewardDataSourceState(newData);
      let totalRecords = {
        ...totalReward,
        data: newData
      }
      handleTotalRewardsMaster(totalRecords);
      setTotalRewards(totalRecords);
    }
  };

  /// updating total rewards
  const handleTotalRewards = (data: any) => {
    let totalRewards: number = 0;
    let totalQuantity: number = 0;
    data?.map((item: any) => {
      totalRewards += parseFloat(item?.rewardValue);
      totalQuantity += parseFloat(item?.rewardQuantity);
      return true;
    });
    let totalRecords = {
      value: totalRewards,
      quantity: totalQuantity,
      data: data,
    };
    handleTotalRewardsMaster(totalRecords);
    setTotalRewards(totalRecords);
  };

  const calculateReward = (newRecord: { rewardValue: any }, record: { rewardValue: any }, rewardValue: any) => {
    if ((newRecord?.rewardValue || record?.rewardValue) && parseFloat(rewardValue)) {
      let isStatus = newRecord?.rewardValue ? newRecord?.rewardValue / rewardValue : record?.rewardValue / rewardValue;
      return isStatus.toFixed(2);
    } else return 0
  }
  // append record and return origin Array
  const appendNewReward = (
    tableData: any,
    rowIndex: number,
    newRecord: any
  ) => {
    let data: any = tableData;
    let rewardValue = form.getFieldValue('rewardValue');

    data[rowIndex] = {
      ...data[rowIndex],
      ...{
        ...newRecord,
        rewardQuantity: calculateReward(newRecord, data[rowIndex], rewardValue)
      },
    };
    handleTotalRewards(data);
    return data;
  };

  const updateAllRecordCount = (value: any) => {

  }

  const handleEditRow = (newRecord: any, rowIndex: number) => {
    let data: any = [];
    if (tableType === tableTypeOptions?.range) {
      data = appendNewReward(rangeBasedRewardDataSourceState, rowIndex, newRecord);
      setRangeBasedRewardDataSourceState([...data]);
    } else if (tableType === tableTypeOptions?.equal) {
      data = appendNewReward(rankBasedRewardDataSourceState, rowIndex, newRecord);
      setRankBasedRewardDataSourceState([...data]);
    }
  }

  const handleDeleteRow = (rowIndex: number) => {
    let data: any[] = [];
    const filteredTableData = (tableData: any[] = []) => tableData?.filter((_: any, i: number) => rowIndex !== i);
    if (tableType === tableTypeOptions?.range) {
      data = filteredTableData(rangeBasedRewardDataSourceState);
      setRangeBasedRewardDataSourceState([...data]);      
    }
    else if (tableType === tableTypeOptions?.equal) {
      data = filteredTableData(rankBasedRewardDataSourceState);
      setRankBasedRewardDataSourceState([...data]?.map((_: rankBasedDataStructure, i: number) => ({ ..._, "rank": `${i + 1}` })));
    }
    handleTotalRewards(data);
  }

  const handleNewReward = (newRecord: any, rowIndex: number | null, type: "edit" | "delete") => {
    if (rowIndex !== null) {
      if (type === "edit") {
        handleEditRow(newRecord, rowIndex)
      }
      else if (type === "delete") {
        handleDeleteRow(rowIndex)
      }
    }
  };

  const checkDisabled = useMemo(() => {
    if (tableType === tableTypeOptions?.equal && noOfWinners) {
      if (rankBasedRewardDataSourceState?.length >= noOfWinners) {
        return true
      }
      return false
    }
    return false
  }, [tableType, tableTypeOptions, rankBasedRewardDataSourceState, noOfWinners, rewardType])

  return (
    <RewardWarmWrapper>
      {/* Reward Applicable field START*/}
      <>
        <Typography className="sub-header">
          Participation reward applicable
        </Typography>
        <Form.Item
          name={'rewardApplicable'}
          initialValue={result?.rewardApplicable ?? radioOptions?.ALL}
        >
          <Radio.Group
            onChange={(e) => setApplicableType(e.target.value)}
            disabled={isView}
          >
            <Row gutter={[12, 12]} justify="space-between">
              <Col xs={24} sm={12} md={8}>
                <Radio value={radioOptions?.ALL}>
                  Applicable for all participants
                </Radio>
              </Col>
              <Col xs={24} sm={12} md={8}>
                <Radio value={radioOptions?.SELECTED}>
                  Applicable for all, except winners{' '}
                </Radio>
              </Col>
              <Col xs={24} sm={12} md={8}>
                <Radio value={radioOptions?.NONE}>Applicable to none </Radio>
              </Col>
            </Row>
          </Radio.Group>
        </Form.Item>
        <Divider />
      </>
      {/* Reward Applicable field END*/}
      {/* Reward Type field START*/}
      <>
        <Typography className="sub-header">Reward Type</Typography>
        <Form.Item
          name={'rewardType'}
          initialValue={result?.rewardType ?? 'EQUAL_REWARD'}
        >
          <Radio.Group
            // onChange={(e) => setRewardType(e.target.value)}
            disabled={isView}
          >
            <Row gutter={[12, 12]} justify="space-between">
              <Col xs={24} sm={12} md={8}>
                <Radio value="EQUAL_REWARD">Equal Rewards</Radio>
              </Col>
              {
                winnerSelection !== "ALL" && 
                <Col xs={24} sm={12} md={8}>
                  <Radio value="RANKING">Ranking </Radio>
                </Col>
              }
              <Col xs={24} sm={12} md={8}></Col>
            </Row>
          </Radio.Group>
        </Form.Item>
        {rewardType === 'EQUAL_REWARD' && (
          <Row gutter={[12, 12]} justify="space-between">
            <Col xs={24} sm={12} md={6}>
              <LabelInput
                label="Reward Per Winner"
                name="rewardPerWinner"
                initialValue={parseFloat?.(result?.rewardPerWinner)?.toFixed(2)}
                rules={[
                  {
                    required: true,
                    message: 'Reward value is required!',
                  },
                ]}
                inputElement={
                  <Input
                    type="number"
                    suffix={
                      <span
                        style={{
                          fontSize: 14,
                          color: '#4DA1FF',
                          fontWeight: '500',
                        }}
                      >
                        USD
                      </span>
                    }
                    onChange={(e) => {
                      if (
                        form.getFieldValue('perRewardValue') !== 'NaN'
                      ) {
                        let res: any = e.target.value;
                        let totalRecords = {
                          value: null,
                          quantity: res ? res / form.getFieldValue('perRewardValue') : 0,
                          data:
                            tableType == tableTypeOptions?.equal
                              ? rankBasedRewardDataSourceState
                              : rangeBasedRewardDataSourceState,
                        };
                        handleTotalRewardsMaster(totalRecords);
                        setTotalRewards(totalRecords);
                      }
                    }}
                    placeholder={'Enter Value'}
                    style={{ width: '100%' }}
                    disabled={isView}
                  />
                }
              />
            </Col>
            <Col xs={24} sm={12} md={6}>
              <LabelInput
                label="Per Reward Value"
                name="perRewardValue"
                initialValue={parseFloat?.(result?.perRewardValue)?.toFixed(2)}
                rules={[
                  {
                    required: true,
                    message: 'Reward value is required!',
                  },
                ]}
                inputElement={
                  <Input
                    type="number"
                    onChange={(e) => {
                      if (
                        form.getFieldValue('rewardPerWinner') !== 'NAN'
                      ) {
                        let res: any = e.target.value;
                        let totalRecords = {
                          value: null,
                          quantity: res ? form.getFieldValue('rewardPerWinner') / res : 0,
                          data:
                            tableType == tableTypeOptions?.equal
                              ? rankBasedRewardDataSourceState
                              : rangeBasedRewardDataSourceState,
                        };
                        handleTotalRewardsMaster(totalRecords);
                        setTotalRewards(totalRecords);
                      }
                    }}
                    prefix={
                      <span
                        style={{
                          fontSize: 14,
                          color: '#000',
                          fontWeight: '500',
                        }}
                      >
                        1 RWRD =
                      </span>
                    }
                    suffix={
                      <span
                        style={{
                          fontSize: 14,
                          color: '#4DA1FF',
                          fontWeight: '500',
                        }}
                      >
                        USD
                      </span>
                    }
                    placeholder={'Enter Value'}
                    style={{ width: '100%' }}
                    disabled={isView}
                  />
                }
              />
            </Col>
            <Col xs={24} sm={12} md={8}>
              <HighlightWrapper>
                <div className="highlight-text">
                  Total Quantity of Rewards Per Winners
                </div>
                <div>
                  <span className="highlight-sub">
                    {parseFloat?.(totalReward?.quantity)?.toFixed(2) ?? 0} RWRD
                  </span>
                </div>
              </HighlightWrapper>
            </Col>
          </Row>
        )}
        {rewardType !== 'EQUAL_REWARD' && (
          <Row gutter={[12, 12]} justify="space-between">
            <Col xs={24} sm={12} md={6}>
              <LabelInput
                label="Reward Per Value"
                name="rewardValue"
                initialValue={parseFloat?.(result?.rewardValue)?.toFixed(2)}
                rules={[
                  {
                    required: true,
                    message: 'Reward Per value is required!',
                  },
                ]}
                inputElement={
                  <Input
                    type="number"
                    prefix={
                      <span
                        style={{
                          fontSize: 14,
                          color: '#000',
                          fontWeight: '500',
                        }}
                      >
                        1 RWRD =
                      </span>
                    }
                    suffix={
                      <span
                        style={{
                          fontSize: 14,
                          color: '#4DA1FF',
                          fontWeight: '500',
                        }}
                      >
                        USD
                      </span>
                    }
                    onChange={(e) => updateAllRecordCount(e.target.value)}
                    placeholder={'Enter Value'}
                    style={{ width: '100%' }}
                    disabled={isView}
                  />
                }
              />
            </Col>
            <Col xs={24} sm={12} md={6}>
              <HighlightWrapper>
                <div className="highlight-text">Total Winner Rewards</div>
                <div>
                  <span className="highlight-sub">
                    {parseFloat?.(totalReward?.value)?.toFixed(2) ?? 0} USD
                  </span>
                </div>
              </HighlightWrapper>
            </Col>
            <Col xs={24} sm={12} md={8}>
              <HighlightWrapper>
                <div className="highlight-text">
                  Total Quantity of Rewards for Winners
                </div>
                <div>
                  <span className="highlight-sub">
                    {parseFloat?.(totalReward?.quantity)?.toFixed(2) ?? 0} RWRD
                  </span>
                </div>
              </HighlightWrapper>
            </Col>
          </Row>
        )}
        <Divider />
      </>
      {/* Reward Type field END*/}
      {/* Rewards Given field START*/}
      {rewardType === 'RANKING' && (
        <>
          <Typography className="sub-header">Rewards given for </Typography>
          <Form.Item
            name={'rewardGivenFor'}
            initialValue={result?.rewardGivenFor ?? tableTypeOptions?.equal}
          // valuePropName="checked"
          >
            <Radio.Group
              // value={tableType}
              onChange={(e) => {
                form.setFieldsValue({rewardGivenFor:e.target.value})
                setTableType(e.target.value)
              }}
              disabled={isView}
            >
              <Row gutter={[12, 12]} justify="space-between">
                <Col xs={24} sm={12} md={8}>
                  <Radio value={tableTypeOptions?.equal}>Each Winner</Radio>
                </Col>
                <Col xs={24} sm={12} md={8}>
                  <Radio value={tableTypeOptions?.range}>
                    Range of winners{' '}
                  </Radio>
                </Col>
                <Col xs={24} sm={12} md={8}></Col>
              </Row>
            </Radio.Group>
          </Form.Item>
        </>
      )}
      {/* Rewards Given field End*/}
      {/* Rewards split field START*/}
      <>
        {rewardType === 'RANKING' && (
          <>
            <Row
              justify="space-between"
              style={{ width: '100%', marginTop: 20 }}
            >
              <Col lg={12}>
                <Typography className="sub-header">Reward Split-up</Typography>
              </Col>
              {!isView && (
                <Button
                  type="link"
                  icon={<PlusOutlined />}
                  onClick={() => onRowAdd()}
                  disabled={checkDisabled}
                >
                  Add Row
                </Button>
              )}
            </Row>
            <RewardSplitter
              type={tableType}
              tableTypeOptions={tableTypeOptions}
              rankBasedRewardDataSource={rankBasedRewardDataSourceState}
              rangeBasedRewardDataSource={rangeBasedRewardDataSourceState}
              handleNewRecord={handleNewReward}
              isView={isView}
            />
          </>
        )}
        <div>
          <Row>
            <Col xs={8}>
              <Typography className="sub-header">Bonus Rewards</Typography>
              <LabelInput
                label="Reward"
                name="bonusReward"
                initialValue={result?.bonusReward}
                // rules={[
                //   {
                //     required: true,
                //     message: 'bonusReward Reward value is required!',
                //   },
                // ]}
                inputElement={
                  <Input
                    placeholder={'Provide Reward'}
                    style={{ width: '100%' }}
                    disabled={isView}
                  />
                }
              />
            </Col>
          </Row>
        </div>
      </>
      {/* Rewards split field End*/}
    </RewardWarmWrapper>
  );
};
