import React from 'react';
import { Modal, Form, Button, Row, Col, Input, Upload, DatePicker } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import moment from 'moment';
import { s3FileUpload } from 'apis/storage';
import { DeleteOutlined } from '@ant-design/icons';

interface contactInfoProps {
    form:any;
    setCreateSto: any;
    createSto: any;
    disableComponent: any
}

const RestrictionLimits = ({form, setCreateSto, createSto, disableComponent}: contactInfoProps): JSX.Element => {
  

  return (
    <>
        <h2 className="bold">Restriction Limits</h2>
        <Form form={form} layout={'vertical'} preserve={false}>
            <Row justify="space-between" style={{marginBottom:"2%"}}>
                <Col lg={6}>
                    <LabelInput
                        label="Max Token Purchase Limit Per User"
                        name="maxTokenPurchaseLimitPerUser"
                        rules={[{ required: false, message: 'Max Token Purchase Limit Per User required !' }]}
                        inputElement={
                            <Input
                                suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>{createSto?.coinSymbol?? "-"}</span>}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    setCreateSto({ ...createSto, maxTokenPurchaseLimitPerUser: e.target.value });
                                    form.setFieldsValue({ maxTokenPurchaseLimitPerUser: e.target.value });
                                }}
                                placeholder="Enter amount"
                                disabled={disableComponent?.maxTokenPurchaseLimitPerUser}
                            />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <LabelInput
                        label="Min Token Purchase Limit Per User"
                        name="minTokenPurchaseLimitPerUser"
                        rules={[{ required: false, message: 'No. of Transactions Limit Per User is required!' }]}
                        // initialValue={teamDetails?.categoryId}
                        inputElement={
                            <Input
                                suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>{createSto?.coinSymbol?? "-"}</span>}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    setCreateSto({ ...createSto, minTokenPurchaseLimitPerUser: e.target.value });
                                    form.setFieldsValue({ minTokenPurchaseLimitPerUser: e.target.value });
                                }}
                                placeholder="Enter Limit"
                                disabled={disableComponent?.minTokenPurchaseLimitPerUser}
                            />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <LabelInput
                        label="No. of Transactions Limit Per User"
                        name="noofTransactionsLimitPerUser"
                        rules={[{ required: false, message: 'No. of Transactions Limit Per User is required!' }]}
                        // initialValue={teamDetails?.categoryId}
                        inputElement={
                            <Input
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    setCreateSto({ ...createSto, noofTransactionsLimitPerUser: e.target.value });
                                    form.setFieldsValue({ noofTransactionsLimitPerUser: e.target.value });
                                }}    
                                placeholder="Enter Limit"
                                disabled={disableComponent?.noofTransactionsLimitPerUser}
                            />
                        }
                    />
                </Col>
            </Row>
        </Form>
    </>
  );
};

export default RestrictionLimits;
