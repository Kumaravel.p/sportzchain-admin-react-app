import React from 'react';
import { Form, Row, Col, Input, Button, DatePicker } from 'antd';
import LabelInput from 'components/common/LabelInput';
import { DeleteOutlined, PlusCircleOutlined } from '@ant-design/icons';

interface contactInfoProps {
    form: any;
    onChage: any;
    addNewSPORow: any;
    addNewPurchaseRow:any;
    RemoveFun: any;
    createSto: any
    disableComponent: any;
}

const Discounts = ({form, onChage,addNewSPORow, addNewPurchaseRow, RemoveFun, createSto, disableComponent}: contactInfoProps): JSX.Element => {


  return (
    <>
        <Row justify="space-between" style={{marginBottom:"3%"}}>
            <h2 className="bold">Discounts</h2>
        </Row>
        <Row justify="space-between" style={{marginBottom:"0%"}}>
            <h3 className="bold">Discount by STO Date</h3>
            <Button type="text" disabled={disableComponent?.discount} style={{color:"#07697D", fontSize: 15}} onClick={()=> addNewSPORow()}><PlusCircleOutlined style={{color:"#07697D"}}/>Add Discount</Button>
        </Row>
        <Form form={form} layout={'vertical'} preserve={false}>
            {createSto?.discount?.map((val:any,i:any)=>{
                return(
                    <Row justify="space-between" style={{marginBottom:"1%"}} align={"bottom"}>
                        <Col lg={6}>
                            <LabelInput
                                label="From Date & Time"
                                inputElement={
                                    <DatePicker
                                        showTime
                                        style={{width: "100%"}}
                                        onChange={(e: any) =>  onChage(e, Object.keys(val)[0], i, "SPO")} 
                                        placeholder="Select Date and Time"
                                        defaultValue={val?.fromDateTime}
                                        disabled={disableComponent?.discount}
                                    />
                                }
                            />
                        </Col>
                        <Col lg={6}>
                            <LabelInput
                                label="To Date & Time"
                                inputElement={
                                    <DatePicker
                                        showTime
                                        style={{width: "100%"}}
                                        onChange={(e: any) => onChage(e, Object.keys(val)[1], i, "SPO")}
                                        placeholder="Select Date and Time"
                                        defaultValue={val?.toDateTime}
                                        disabled={disableComponent?.discount}
                                    />
                                }
                            />
                        </Col>
                        <Col lg={6}>
                            <LabelInput
                                label="Discount Percentage"
                                inputElement={
                                    <Input
                                        suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>%</span>}
                                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChage(e?.target?.value, Object.keys(val)[2], i, "SPO")} 
                                        placeholder="Enter amount"
                                        style={{width:"100%"}}
                                        defaultValue={val?.discountPercentage}
                                        disabled={disableComponent?.discount}
                                    />
                                }
                            />
                        </Col>
                        {
                            disableComponent?.discount ?  null : 
                            <Button disabled={disableComponent?.discount} onClick={()=> RemoveFun("SPO", i)} type="ghost" shape="default" icon={ <DeleteOutlined />} size={"middle"} style={{marginBottom:"1%", color:"#fff", backgroundColor:"#FF4D4A"}}/>
                        }
                    </Row>
                )
            })}
        </Form>
        <Row justify="space-between" style={{marginTop:"3%"}}>
            <h3 className="bold">Discount by Purchase Value</h3>
            <Button type="text" disabled={disableComponent?.discountsPurchase} style={{color:"#07697D", fontSize: 15}} onClick={()=> addNewPurchaseRow()}><PlusCircleOutlined style={{color:"#07697D"}}/>Add Discount</Button>
        </Row>
        <Form form={form} layout={'vertical'} preserve={false}>
            {createSto?.discountsPurchase?.map((val:any,i:any)=>{
                return(
                    <Row justify="space-between" style={{marginBottom:"2%"}} align="bottom">
                        <Col lg={6}>
                            <LabelInput
                                label="Purchase Value"
                                inputElement={
                                    <Input
                                        prefix={<span style={{fontSize:14, color:'#000', fontWeight:"500"}}>Greater than</span>}
                                        onChange={(e: any) =>  onChage(e, Object.keys(val)[0], i, "purchase")} 
                                        placeholder="Enter Value"
                                        suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>{createSto?.coinSymbol?? "-"}</span>}
                                        defaultValue={val?.purchaseValue}
                                        disabled={disableComponent?.discountsPurchase}
                                    />
                                }
                            />
                        </Col>
                        <Col lg={6}>
                            <LabelInput
                                label="Discount Percentage"
                                inputElement={
                                    <Input
                                        onChange={(e: any) => onChage(e, Object.keys(val)[1], i, "purchase")}
                                        placeholder="Enter amount"
                                        suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>%</span>}
                                        defaultValue={val?.purchaseDiscountPercentage}
                                        disabled={disableComponent?.discountsPurchase}
                                    />
                                }
                            />
                        </Col>
                        {
                            disableComponent?.discountsPurchase ? null 
                            : <Button disabled={disableComponent?.discount} onClick={()=> RemoveFun("purchase", i)} type="ghost" shape="default" icon={ <DeleteOutlined />} size={"middle"} style={{marginBottom:"1%", color:"#fff", backgroundColor:"#FF4D4A"}}/>
                        }
                        <Col lg={6}>
                        </Col>
                    </Row>
                )
            })}
        </Form>
    </>
  );
};

export default Discounts;
