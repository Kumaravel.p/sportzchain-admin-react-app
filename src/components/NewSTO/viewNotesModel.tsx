import React from 'react';
import { Modal, Button, Row, Col, Input,} from 'antd';

const { TextArea } = Input;
interface NotesInfoProps {
  Data: any;
  ModalVisible: boolean;
  handleCancel: any;
}

const ViewNoteModal = ({Data, ModalVisible, handleCancel}: NotesInfoProps): JSX.Element => {
    
  return (
    <>
      <Modal 
        title={'View Notes'}
        okText="View"
        visible={ModalVisible}
        closable={true}
        onCancel={()=>{
          handleCancel();
        }}
        destroyOnClose={true}
        footer={
        <Button key="submit" type="primary" onClick={()=> handleCancel()}>
          Close
        </Button>
      }
      >
          <div style={{display:"flex", marginBottom:"3%"}}><p style={{color:"#97A0C3", fontSize: 14, fontWeight: 700}}>Added by</p><p style={{fontSize: 14,marginLeft:"3%"}}>{Data?.addedBy}</p></div>
          <div><p style={{color:"#97A0C3", fontSize: 14, fontWeight: 700,marginBottom:"1%" }}>Note Title</p></div>
          <div><p style={{fontSize: 16,marginBottom:"5%"}}>{Data?.name}</p></div>
          <div><p style={{color:"#97A0C3", fontSize: 14, fontWeight: 700,marginBottom:"1%"}}>Note Description</p></div>
          <div><p style={{fontSize: 16}}>{Data?.notes}</p></div>
      </Modal>
    </>
  );
};

export default ViewNoteModal;
