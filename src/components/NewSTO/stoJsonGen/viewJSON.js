import moment from "moment";
// 
import { ternaryOperator } from "../../../utils";
export const viewGenJSON = (data, statusData, token) => {


  let coinSymbol = ternaryOperator(data?.teamByTeamId?.walletsByTeamId?.nodes[0]?.coinSymbol);
  let getStatusData = statusData?.allMasterStoStatuses?.nodes?.filter((val) => val?.id === data?.stoStatusId);

  let BasicJson = [{
    title: "Basic Info",
    teamName: data?.teamByTeamId?.name,
    profileImageUrl: data?.teamByTeamId.profileImageUrl,
    valuesArr: [
      {
        lable: "Team Name",
        value: data?.teamByTeamId?.name,
        col: 8,
        divider: true
      },
      {
        lable: "ID",
        value: data?.teamId,
        col: 8,
        divider: true
      },
      {
        lable: "STO Status",
        value: ternaryOperator(getStatusData?.[0]?.type, ""),
        col: 8,
        divider: true
      },

      {
        lable: "Currency Symbol",
        value: data?.teamByTeamId?.walletsByTeamId?.nodes[0]?.coinSymbol,
        col: 8,
        divider: true
      },
      {
        lable: "Currency Type",
        value: data?.teamByTeamId?.currencyTypeId,
        col: 8,
        divider: true
      },
      {
        lable: "Total Tokens",
        value: (token && data?.teamByTeamId?.walletsByTeamId?.nodes[0]?.coinSymbol) ? `${token?.totaltoken} ${data?.teamByTeamId?.walletsByTeamId?.nodes[0]?.coinSymbol} (${token?.remainingToken} left)` : 0,
        col: 8,
        divider: true
      },

      {
        lable: "Country",
        value: data?.teamByTeamId?.countryTitle,
        col: 8,
      },
      {
        lable: "Category",
        value: data?.teamByTeamId?.categoryId,
        col: 8,
      },
      {
        lable: "League",
        value: data?.teamByTeamId?.selectedLeaguesByTeamId?.nodes?.map((val) => val?.league).toString(""),
        col: 8
      },
    ]
  }];
  let SmartContract = [
    {
      title: "Smart Contract Details",
      teamName: data?.teamByTeamId?.name,
      profileImageUrl: data?.teamByTeamId.profileImageUrl,
      valuesArr: [
        {
          lable: "Contract address",
          value: data?.smartContractAddress ?? '-',
          col: 8,
          divider: false
        },
        {
          lable: "Status",
          value: data?.smartContractStatus ? data?.smartContractStatus?.toUpperCase(): '-',
          col: 8,
          divider: false
        },
      ]
    }
  ]
  let InitialJson = [{
    title: `Initial Offering`,
    noOfRound: data?.round,
    valuesArr: [
      {
        lable: "Target Amount",
        value: parseInt(data?.targetAmount) ? parseInt(data?.targetAmount) : "",
        col: 8,
        divider: true
      },
      {
        lable: "Supply Size",
        value: parseInt(data?.supplySize) ? parseInt(data?.supplySize) : "",
        col: 8,
        divider: true
      },
      {
        lable: "Initial Price",
        value: `1 ${coinSymbol} = ${parseFloat(data?.initialPrice) ? parseFloat(data?.initialPrice) : ""} USD`,
        col: 8,
        divider: true
      },
      {
        lable: "Start Date Time",
        value: data?.startDate ? moment(data?.startDate + "z").format('MMMM D YYYY, hh:mm') : "",
        col: 8,
        divider: true
      },
      {
        lable: "End Date Time",
        value: data?.endDate ? moment(data?.endDate + "z").format('MMMM D YYYY, hh:mm') : "",
        col: 8,
        divider: true
      },
      {
        lable: "Display Start Date Time",
        value: data?.displayStartDate ? moment(data?.displayStartDate + "z").format('MMMM D YYYY, hh:mm') : "",
        col: 8,
        divider: true
      },

      {
        lable: "Display End Date Time",
        value: data?.displayEndAt ? moment(data?.displayEndAt + "z").format('MMMM D YYYY, hh:mm') : "",
        col: 8
      },
    ]
  }];

  const TimeLocking = [{
    title: "Time Locking",
    valuesArr: [
      {
        lable: "User Type",
        value: "Fans",
        col: 8,
        divider: true
      },
      {
        lable: "Initial Token Release",
        value: `After ${data?.initalTokenRelease ? data?.initalTokenRelease : ""} Days`,
        col: 8,
        divider: true
      },
      {
        lable: "InitialTokens to be unlocked",
        value: `${data?.initalTokenUnlocked ? parseInt(data?.initalTokenUnlocked) : ""}%`,
        col: 8,
        divider: true
      },
      {
        lable: "Remaining Token Release",
        value: `After ${data?.remainingTokenRelease ? data?.remainingTokenRelease : ""} Days`,
        col: 8
      },
      {
        lable: "Remaining Tokens",
        value: `${data?.initalTokenUnlocked ? parseInt(100 - data?.initalTokenUnlocked) : ""}%`,
        col: 8
      },
      {
        col: 8
      },
    ]
  }];

  const RestrictionLimits = [{
    title: "Restriction Limits",
    valuesArr: [
      {
        lable: "Max Token Purchase Limit Per User",
        value: data?.maxTokenLimit ? data?.maxTokenLimit + coinSymbol : "" + coinSymbol,
        col: 8
      },
      {
        lable: "Min Token Purchase Limit Per User",
        value: data?.minTokenLimit ? data?.minTokenLimit + coinSymbol : "" + coinSymbol,
        col: 8
      },
      {
        lable: "No. of Transactions Limit Per User",
        value: data?.transactionLimit ? data?.transactionLimit : "",
        col: 8
      }
    ]
  }];

  const Discounts = [{
    title: "Discount by SPO Date",
    valuesArr: [
      {
        lable: "From Date & Time",
        value: "May 24, 2022, 13:00",
        col: 8,
        divider: true
      },
      {
        lable: "To Date & Time",
        value: "May 24, 2022, 13:00",
        col: 8,
        divider: true
      },
      {
        lable: "Discount Percentage",
        value: "5%",
        col: 8,
        divider: true
      },
      {
        lable: "From Date & Time",
        value: "May 24, 2022, 13:00",
        col: 8
      },
      {
        lable: "To Date & Time",
        value: "May 24, 2022, 13:00",
        col: 8
      },
      {
        lable: "Discount Percentage",
        value: "3%",
        col: 8
      },
    ]
  },
  {
    title: "Discount by Purchase Value",
    valuesArr: [
      {
        lable: "Purchase Value",
        value: "Greater than 10,000MI",
        col: 8,
        divider: true
      },
      {
        lable: "Discount Percentage",
        value: "5%",
        col: 8,
        divider: true
      },
      {
        col: 8,
      },
      {
        lable: "Purchase Value",
        value: "Greater than 5,000MI",
        col: 8
      },
      {
        lable: "Discount Percentage",
        value: "4%",
        col: 8
      },
      {
        col: 8,
      },
    ]
  }];

  let result = {
    BasicJson,
    InitialJson,
    TimeLocking,
    RestrictionLimits,
    Discounts,
    SmartContract
  }

  return result;
}

export const editGenJSON = (data) => {
  let BY_VALUE = [];
  let BY_DATE = [];
  if (data.stoDiscountsByStoId.nodes) {
    data.stoDiscountsByStoId.nodes.map((val) => {
      if (val.discountType === "BY_VALUE") {
        let data = {
          "purchaseValue": parseInt(ternaryOperator(val?.purchaseValue, "")),
          "purchaseDiscountPercentage": ternaryOperator(val?.discountPercentage, "")
        }
        BY_VALUE.push(data);
      }
      else if (val.discountType === "BY_DATE") {
        let data = {
          "fromDateTime": moment(ternaryOperator(val?.discountFromDate + "z", "")),
          "toDateTime": moment(ternaryOperator(val?.discountToDate + "z", "")),
          "discountPercentage": ternaryOperator(val?.discountPercentage, "")
        }
        BY_DATE.push(data);
      }
    })
  }
  return {
    "id": ternaryOperator(data?.id, ""),
    "userType": ternaryOperator(data?.userTypeId, ""),
    "roundCount": ternaryOperator(data?.round, ""),
    "teamName": ternaryOperator(data?.teamByTeamId?.name, ""),
    "coinSymbol": ternaryOperator(data?.teamByTeamId?.walletsByTeamId?.nodes[0]?.coinSymbol, ""),
    "teamId": ternaryOperator(data?.teamId, ""),
    "statusId": ternaryOperator(data?.stoStatusId, ""),
    "targetAmount": "" + parseInt(ternaryOperator(data?.targetAmount, "")),
    "supplySize": "" + ternaryOperator(parseInt(data?.supplySize), ""),
    "initialPrice": "" + ternaryOperator(parseFloat(data?.initialPrice), ""),
    "startDateTime": ternaryOperator(moment(data?.startDate + "z"), ""),
    "endDateTime": ternaryOperator(moment(data?.endDate + "z"), ""),
    "displayStartDateTime": ternaryOperator(moment(data?.displayStartDate + "z"), ""),
    "displayEndDateTime": ternaryOperator(moment(data?.displayEndAt + "z"), ""),
    "initialTokenRelease": ternaryOperator(data?.initalTokenRelease, ""),
    "initialTokensToBeUnlocked": "" + parseInt(ternaryOperator(data?.initalTokenUnlocked, "")),
    "remainingTokenRelease": ternaryOperator(data?.remainingTokenRelease, ""),
    "discount": BY_DATE,
    "discountsPurchase": BY_VALUE,
    "maxTokenPurchaseLimitPerUser": ternaryOperator(data?.maxTokenLimit, ""),
    "minTokenPurchaseLimitPerUser": ternaryOperator(data?.minTokenLimit, ""),
    "noofTransactionsLimitPerUser": ternaryOperator(data?.transactionLimit, ""),
    "NotedataSource": data?.stoNotesByStoId?.nodes?.map((val) => ({
      "documentNote": val.notes,
      "note": val?.name,
      "addedDate": moment(val?.createdDate + "z"),
      "addedby": val?.addedBy,
      "id":val?.id
    }))
  }
}
export default viewGenJSON;
