import React, { useCallback, useMemo } from 'react';
import {  Form, Row, Col, Input, DatePicker } from 'antd';
import LabelInput from 'components/common/LabelInput';
import moment, { MomentInput } from 'moment';


interface contactInfoProps {
    form:any;
    setCreateSto: any;
    createSto: any;
    disableComponent: any;
    noofRoundSto: any;
    teamId:string;
    teamList:any[]
}

const InitialOffering = ({form, setCreateSto, createSto, disableComponent, noofRoundSto, teamList=[], teamId}: contactInfoProps): JSX.Element => {

    //get team Details by id
    const getTeamById = useMemo(() => teamList?.find(team => team?.teamByTeamId?.id === teamId)?.teamByTeamId, [teamList, teamId])

    //disable start date
    const isDisableStartDate = useCallback((date) => {
        return date && moment(date).isBefore(moment(), 'day')
    }, [form])

    //disable end date
    const isDisableEndDate = useCallback((date, key) => {
        if (!form.getFieldValue(key)) {
            return true
        }
        let checkDate: MomentInput = moment(form.getFieldValue(key)).isBefore(moment(), 'day') ? moment() : moment(form.getFieldValue(key))
        return date && moment(date).isBefore(checkDate, 'day')
    }, [form])

    //status validation for Start Date Time
    const startDateTimeValidation = (getFieldValue: any, value: any) => {
        if (getFieldValue('endDateTime')) {
            if (!value.isBefore(getFieldValue('endDateTime'))) {
                return Promise.reject(new Error(`Start Date Time should be lesser than End Date Time(${getFieldValue('endDateTime').format('DD MM yyyy hh:mm')})`));
            }
        }
        if (getFieldValue('displayStartDateTime')) {
            if (!value.isAfter(getFieldValue('displayStartDateTime'))) {
                return Promise.reject(new Error(`Start Date Time should be greater than Display Start Date Time(${getFieldValue('displayStartDateTime').format('DD MM yyyy hh:mm')})`));
            }
        }
        if (getTeamById) {
            if (!value.isAfter(moment(getTeamById?.activeStartAt?.concat('Z')))) {
                return Promise.reject(new Error(`Start Date Time should be greater than Team Start Date Time(${moment(getTeamById?.activeStartAt?.concat('Z')).format('DD MM yyyy hh:mm')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for End Date Time
    const endDateTimeValidation = (getFieldValue: any, value: any) => {
        if (getFieldValue('startDateTime')) {
            if (!value.isAfter(getFieldValue('startDateTime'))) {
                return Promise.reject(new Error(`End Date Time should be greater than Start Date Time(${getFieldValue('startDateTime').format('DD MM yyyy hh:mm')})`));
            }
        }
        if (getFieldValue('displayEndDateTime')) {
            if (!value.isBefore(getFieldValue('displayEndDateTime'))) {
                return Promise.reject(new Error(`End Date Time should be lesser than Display End Date Time(${getFieldValue('displayEndDateTime').format('DD MM yyyy hh:mm')})`));
            }
        }
        if (getTeamById) {
            if (!value.isBefore(moment(getTeamById?.activeEndAt?.concat('Z')))) {
                return Promise.reject(new Error(`End Date Time should be lesser than Team End Date Time(${moment(getTeamById?.activeEndAt?.concat('Z')).format('DD MM yyyy hh:mm')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for Display Start Date Time
    const displayStartDateTimeValidation = (getFieldValue: any, value: any) => {
        if (getFieldValue('startDateTime')) {
            if (!value.isBefore(getFieldValue('startDateTime'))) {
                return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('startDateTime').format('DD MM yyyy hh:mm')})`));
            }
        }
        if (getTeamById) {
            if (!value.isAfter(moment(getTeamById?.displayStartAt?.concat('Z')))) {
                return Promise.reject(new Error(`Display Start Date Time should be lesser than Team Start Date Time(${moment(getTeamById?.displayStartAt?.concat('Z')).format('DD MM yyyy hh:mm')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for Display End Date Time
    const displayEndDateTimeValidation = (getFieldValue: any, value: any) => {
        if (getFieldValue('endDateTime')) {
            if (!value.isAfter(getFieldValue('endDateTime'))) {
                return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('endDateTime').format('DD MM yyyy hh:mm')})`));
            }
        }
        if (getTeamById) {
            if (!value.isBefore(moment(getTeamById?.displayEndAt?.concat('Z')))) {
                return Promise.reject(new Error(`Display End Date Time should be lesser than Team End Date Time(${moment(getTeamById?.displayEndAt?.concat('Z')).format('DD MM yyyy hh:mm')})`));
            }
        }
        return Promise.resolve();
    }

  return (
    <>
        <h2 className="bold">Initial Offering <span><span className="normal"  style={{backgroundColor:"#F4F4F4", color:"#07697D", padding: 6, borderRadius:"6px"}}> Round <span className='bold'>{noofRoundSto > 0 ? noofRoundSto : 1}</span></span></span></h2>
        <Form form={form} layout={'vertical'} preserve={false}>
            <Row justify="space-between" style={{marginBottom:"2%"}}>
                <Col lg={6}>
                    <LabelInput
                        label="Target Amount*"
                        name="targetAmount"
                        rules={[
                            { 
                                required: true, message: 'Target Amount is required!',
                            }
                        ]}
                        inputElement={
                            <Input
                                suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>USD</span>}
                                onChange={(e: any) => {
                                    setCreateSto({ 
                                        ...createSto, 
                                        targetAmount: e.target.value,
                                        supplySize: createSto?.initialPrice * e.target.value,
                                    });
                                    form.setFieldsValue({ targetAmount: e.target.value });
                                }}
                                placeholder="Supply Size"
                                disabled={disableComponent?.targetAmount}
                            />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <LabelInput
                        label="Initial Price*"
                        name="initialPrice"
                        rules={[
                            { 
                                required: true, message: 'Initial Price is required!',
                            }
                        ]}
                        inputElement={
                            <Input
                                prefix={<span style={{fontSize:14, color:"gray", fontWeight:"500"}}>{createSto?.coinSymbol?? "-"} = </span>}
                                onChange={(e: any) => {
                                    setCreateSto({ 
                                        ...createSto,
                                        initialPrice: e.target.value,
                                        supplySize: createSto?.targetAmount * e.target.value
                                    });
                                    form.setFieldsValue({ initialPrice: e.target.value });
                                }}    
                                placeholder="Initial Price"  
                                disabled={disableComponent?.initialPrice}
                                suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>USD</span>}
                            />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <div style={{marginBottom: "2%",marginTop: "6%", color: "gray", fontSize: 14}}>Supply Size</div>
                    <div  style={{marginBottom: "4%", border:"1px solid #ddd", padding:"5px 8px", borderRadius:"3px", display:"flex", justifyContent:"space-between", backgroundColor:"#f5f5f5", cursor: "no-drop"}}>
                        <span style={{fontSize:"14"}}>
                            {createSto?.supplySize ? createSto?.supplySize : null}
                        </span>
                        <span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>
                            {createSto?.coinSymbol?? "-"}
                        </span>
                    </div>
                </Col>
            </Row>
            <Row justify="space-between" style={{marginBottom:"2%"}}>
                <Col lg={6}>
                    <LabelInput
                        label="Start Date & Time*"
                        name="startDateTime"
                        rules={[
                            { required: true, message: 'Start Date & Time' },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                  return startDateTimeValidation(getFieldValue,value)
                                },
                              }),
                        ]}
                        inputElement={
                        <DatePicker
                                format={"DD-MM-YYYY hh:mm"}
                                showTime
                                showNow={false}
                                showSecond={false}
                            onChange={(e: any) => {
                                
                                setCreateSto({ ...createSto, startDateTime: e });
                                form.setFieldsValue({ startDateTime: e });
                            }}
                            placeholder={"Start Date & Time"}
                            style={{width:"100%"}}
                            disabled={disableComponent?.startDateTime}
                            disabledDate={(value) => isDisableStartDate(value)}
                        />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <LabelInput
                        label="End Date & Time*"
                        name="endDateTime"
                        // initialValue={OfferingData.name}
                        rules={[
                            { required: true, message: 'End Date & Time' },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                  return endDateTimeValidation(getFieldValue,value)
                                },
                              }),
                        ]}
                        inputElement={
                        <DatePicker
                                format={"DD-MM-YYYY hh:mm"}
                                showTime
                                showNow={false}
                                showSecond={false}
                            onChange={(e: any) => {
                                setCreateSto({ ...createSto, endDateTime: e });
                                form.setFieldsValue({ endDateTime: e });
                            }}
                            placeholder={"End Date & Time"}
                            style={{width:"100%"}}
                            disabled={disableComponent?.endDateTime}
                            disabledDate={(value) => isDisableEndDate(value,'startDateTime')}
                        />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <LabelInput
                        label="Display Start Date & Time*"
                        name="displayStartDateTime"
                        rules={[
                            { required: true, message: 'Display Start Date & Time' },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                  return displayStartDateTimeValidation(getFieldValue,value)
                                },
                              }),
                        ]}
                        inputElement={
                        <DatePicker
                                format={"DD-MM-YYYY hh:mm"}
                                showTime
                                showNow={false}
                                showSecond={false}
                            onChange={(e: any) => {
                                setCreateSto({ ...createSto, displayStartDateTime: e });
                                form.setFieldsValue({ displayStartDateTime: e });
                            }}
                            placeholder={"Start Display Date & Time"}
                            style={{width:"100%"}}
                            disabled={disableComponent?.displayStartDateTime}
                            disabledDate={(value) => isDisableStartDate(value)}
                        />
                        }
                    />
                </Col>
            </Row>
            <Row justify="space-between">
                <Col lg={6}>
                    <LabelInput
                        label="Display End Date & Time*"
                        name="displayEndDateTime"
                        // initialValue={OfferingData.name}
                        rules={[
                            { required: true, message: 'Display - End date & Time' },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                  return displayEndDateTimeValidation(getFieldValue,value)
                                },
                              }),
                        ]}
                        inputElement={
                        <DatePicker
                                format={"DD-MM-YYYY hh:mm"}
                                showTime
                                showNow={false}
                                showSecond={false}
                            onChange={(e: any) => {
                                setCreateSto({ ...createSto, displayEndDateTime: e });
                                form.setFieldsValue({ displayEndDateTime: e });
                            }}
                            placeholder={"End Display Date & Time"}
                            style={{width:"100%"}}
                            disabled={disableComponent?.displayEndDateTime}
                            disabledDate={(value) => isDisableEndDate(value,'displayStartDateTime')}
                        />
                        }
                    />
                </Col>
            </Row>
        </Form>
    </>
  );
};

export default InitialOffering;
