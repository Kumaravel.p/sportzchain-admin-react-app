import { Button, Table } from 'antd';

import { EditOutlined, EyeOutlined } from '@ant-design/icons';
import moment from 'moment';

interface contactInfoProps {
  JsonData: any;
  openEdit: any;
  Notesdisabled: any;
  readOnly?:boolean;
}

function onChange(pagination: any, filters: any, sorter: any, extra: any) {
  console.log('params', pagination, filters, sorter, extra);
}
const NewNotes = ({JsonData, openEdit, Notesdisabled,readOnly=false}: contactInfoProps): JSX.Element => {


  const columns = [
    {
      title: '#',
      dataIndex: 'key',
    },
    {
      title: 'NOTE',
      dataIndex: 'documentNote',
    },
    {
      title: 'Added by',
      dataIndex: 'addedby',
    },
    {
      title: 'Added date',
      dataIndex: 'addedDate',
    },
    {
      title: '',
      dataIndex: '',
      key: 'x',
      render: (_:any, record: { key: React.Key }) =>
        data?.length >= 1 ? (
          <Button disabled={Notesdisabled} onClick={() => editClick(record.key)} type="link">
            {
              (readOnly || _?.id) ? (
                <EyeOutlined style={{ color: '#4DA1FF' }} />
              ) : (
                <EditOutlined style={{ color: '#4DA1FF' }} />
              )
          }
          </Button>
        ) : null,
    },
  ];

  const editClick = (record: any) => {
    openEdit(record);
  }

  const data = JsonData?.map((val: any,i: number)=>{
    return({
        key: i + 1,
        documentNote: val.documentNote,
        addedby: val.addedby,
        addedDate: (val.addedDate instanceof moment) ? moment(val.addedDate).format('DD/MM/YYYY, hh:mm') : val.addedDate,
        id:val?.id
      })
  });
  return(
    <Table columns={columns} dataSource={data} onChange={onChange} size="middle" scroll={{ x: 500 }} pagination={false}/>
  )
}
export default NewNotes