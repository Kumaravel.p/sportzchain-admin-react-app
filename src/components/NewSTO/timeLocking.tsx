import React from 'react';
import { Form, Row, Col, Input, DatePicker } from 'antd';
import LabelInput from 'components/common/LabelInput';
import { useGetTeamStatusesQuery } from 'generated/graphql';
import SelectElement from 'components/common/SelectElement';
interface contactInfoProps {
    form:any;
    setCreateSto: any;
    createSto: any;
    disableComponent: any;
}

const TimeLocking = ({form, setCreateSto, createSto, disableComponent}: contactInfoProps): JSX.Element => {
    const { data: teamStatuses } = useGetTeamStatusesQuery();
  return (
    <>
        <h2 className="bold">Time Locking </h2>
        <Form form={form} layout={'vertical'} preserve={false}>
            <Row justify="space-between" style={{marginBottom:"2%"}}>
                <Col lg={6}>
                    <LabelInput
                        label="User Type*"
                        name="userType"
                        rules={[{ required: true, message: 'User Type is required!' }]}
                        initialValue={"Fan"}
                        inputElement={
                            <SelectElement
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                                    setCreateSto({ ...createSto, userType: e });
                                    form.setFieldsValue({ userType: e });
                                }}
                                defaultValue={createSto.userType}
                                options={teamStatuses?.teamStatus ?? []}
                                toFilter="status"
                                placeholder="User Type"
                                searchable={true}
                                disabled={true}
                            />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <LabelInput
                        label="Initial Token Release*"
                        name="initialTokenRelease"
                        // initialValue={OfferingData.name}
                        rules={[{ required: true, message: 'Initial Token Release is required!' }]}
                        inputElement={
                        <Input
                            prefix={<span style={{fontSize:14, color:'#000', fontWeight:"500"}}>After</span>}
                            suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>Days</span>}
                            onChange={(e: any) => {
                                setCreateSto({ ...createSto, initialTokenRelease: e?.target?.value });
                                form.setFieldsValue({ initialTokenRelease: e?.target?.value });
                            }}
                            placeholder={"Initial Token Release"}
                            style={{width:"100%"}}
                            disabled={disableComponent?.initialTokenRelease}
                        />
                        }
                    />
                </Col>
                <Col lg={6}>
                    <LabelInput
                        label="Initial Tokens to be unlocked*"
                        name="initialTokensToBeUnlocked"
                        // initialValue={OfferingData.name}
                        rules={[{ required: true, message: 'Initial Token Release is required' }]}
                        inputElement={
                        <Input
                            suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>%</span>}
                            onChange={(e: any) => {
                                setCreateSto({ ...createSto, initialTokensToBeUnlocked: e?.target?.value });
                                form.setFieldsValue({ initialTokensToBeUnlocked: e?.target?.value });
                            }}
                            placeholder={"Initial Tokens to be unlocked"}
                            style={{width:"100%"}}
                            disabled={disableComponent?.initialTokensToBeUnlocked}
                        />
                        }
                    />
                </Col>
            </Row>
            <Row justify="space-between">
                <Col lg={6}>
                    <LabelInput
                        label="Remaining Token Release*"
                        name="remainingTokenRelease"
                        // initialValue={OfferingData.name}
                        rules={[{ required: true, message: 'Remaining Token Release is required' }]}
                        inputElement={
                        <Input
                            prefix={<span style={{fontSize:14, color:'#000', fontWeight:"500"}}>After</span>}
                            suffix={<span style={{fontSize:14, color:'#4DA1FF', fontWeight:"500"}}>Days</span>}
                            onChange={(e: any) => {
                                setCreateSto({ ...createSto, remainingTokenRelease: e?.target?.value });
                                form.setFieldsValue({ remainingTokenRelease: e?.target?.value });
                            }}
                            placeholder={"Remaining Token Release"}
                            style={{width:"100%"}}
                            disabled={disableComponent?.remainingTokenRelease}
                        />
                        }
                    />
                </Col>
                <Col lg={6}>
                  <div style={{marginBottom: "2%",marginTop: "6%", color: "gray", fontSize: 16}}>Remaining Tokens</div>
                  <div  style={{marginBottom: "4%"}}><span style={{fontSize: "24px", color: "#07697D"}}>{createSto?.initialTokensToBeUnlocked ? 100 - createSto?.initialTokensToBeUnlocked : "-"}%</span></div>
                </Col>
                <Col lg={6}>
                    
                </Col>
            </Row>
        </Form>
    </>
  );
};

export default TimeLocking;
