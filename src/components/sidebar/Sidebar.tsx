import * as React from "react";
import styled from 'styled-components/macro';
import { Divider, Layout, Menu } from 'antd';
import Sider from 'antd/lib/layout/Sider';
import Text from 'antd/lib/typography/Text';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import { ReactComponent as DashboardIcon } from 'assets/icons/dashboard.svg';
import { ReactComponent as UsersIcon } from 'assets/icons/users.svg';
import { ReactComponent as GroupsIcon } from 'assets/icons/groups.svg';
import { ReactComponent as SwapIcon } from 'assets/icons/swap.svg';
import { ReactComponent as TransactionsIcon } from 'assets/icons/transactions.svg';
import { ReactComponent as SettingsIcon } from 'assets/icons/settings.svg';
import { ReactComponent as RewardsIcon } from 'assets/icons/rewards.svg';
import { ReactComponent as ContestsIcon } from 'assets/icons/contests.svg';
import { ReactComponent as ReportsIcon } from 'assets/icons/reports.svg';
import { ReactComponent as NotificationsIcon } from 'assets/icons/notifications.svg';
import { Link } from 'react-router-dom';
import { SwapOutlined } from '@ant-design/icons';
import { TeamSelectionContext } from 'App';
import { state as ProxyState } from 'state';
import { useSnapshot } from "valtio";

const SidebarWrapper = styled.div`
  padding-left: 20px;
  .user-info-wrapper {
    margin-top: 20px;
  }
  .ant-menu {
    height: 100vh;
    padding-right: 10px;
  }
  .ant-menu-item {
    padding-left: 0 !important;
    font-size: 17px;
    line-height: 21px;
    /* identical to box height */
    font-weight: 600;
    color: rgba(5, 31, 36, 0.6);
  }
  .t-name {
    font-weight: bold;
    font-size: 20px;
    line-height: 24px;
    color: #051f24;
  }
  .t-designation {
    font-size: 15px;
    line-height: 18px;
    color: rgba(5, 31, 36, 0.6);
    margin: 7px 0px;
  }
  .t-subheader {
    font-size: 16px;
    line-height: 19px;
    text-transform: uppercase;
    color: #9ab1b6;
  }
`;

interface SidebarProps { }

const Sidebar = ({ }: SidebarProps): JSX.Element => {

  const { profile } = useSnapshot(ProxyState);

  let team: any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  const isTeamUser = isIamTeamUser();

  const teamSelectionContext = React.useContext(TeamSelectionContext);

  // React.useEffect(() => {

  // }, []);
  const canIShow = (teamId: any, module: any, action: any) => {
    return profile?.teamDetail?.permissions?.[teamId]?.[module]?.includes(action) ?? false
  };

  return (
    <SidebarWrapper>
      <Layout hasSider>
        <Sider
          theme="light"
          className="sidebar"
          breakpoint="lg"
          collapsedWidth={0}
          collapsible
          width={260}
          trigger={null}
        // collapsed={isSidebarCollapsed}
        >
          <div className="logo" />
          <Menu
            theme="light"
            mode="inline"
          //   onClick={({ key }) => history.push(`${url}/${key}`)}
          >
            <FlexColumnWrapper className="user-info-wrapper">
              {/* <Text className="t-name">Jane Cooper</Text>
              <Text className="t-designation">Enterprise Architect</Text> */}
            </FlexColumnWrapper>
            {/* <Menu.Divider /> */}
            {/* <Menu.Item key="dashboard" icon={<DashboardIcon />}>
              <Link to="">Dashboard</Link>
            </Menu.Item> */}
            {!isTeamUser && <Menu.Item key="users" icon={<UsersIcon />}>
              <Link to="">Users</Link>
            </Menu.Item>}

            <Menu.Item key="teams" icon={<UsersIcon />}>
              <Link to="/team-home">Teams</Link>
            </Menu.Item>
            {!isTeamUser && <Menu.Item key="groups" icon={<GroupsIcon />}>
              <Link to="/groups">Groups</Link>
            </Menu.Item>}

            <Menu.Item key="group-criteria" icon={<GroupsIcon />}>
              <Link to="/group-criteria">Group Criteria</Link>
            </Menu.Item>



            <Menu.Item key="sto" icon={<SwapOutlined />}>
              <Link to="/listing-sto">STO</Link>
            </Menu.Item>

            {/* <Menu.Item key="swap-price" icon={<SwapIcon />}>
              <Link to="">Swap Price</Link>
            </Menu.Item> */}

            {!isTeamUser && <Menu.Item key="transactions" icon={<TransactionsIcon />}>
              <Link to="/transactions">Transactions</Link>
            </Menu.Item>}

            {!isTeamUser && <Menu.Item key="activity-points" icon={<RewardsIcon />}>
              <Link to="/activity-points">Activity Points</Link>
            </Menu.Item>}

            {!isTeamUser && <Menu.Item key="leaderboard-levels" icon={<RewardsIcon />}>
              <Link to="/leaderboard-levels">Leaderboard Levels</Link>
            </Menu.Item>}

            {!isTeamUser && <Menu.Item key="rewards-and-leaderboard" icon={<RewardsIcon />}>
              <Link to="/rewards">Rewards</Link>
            </Menu.Item>}
            {!isTeamUser && <Menu.Item key="scheduler-dashboard" icon={<DashboardIcon />}>
              <Link to="/scheduler-dashboard">Scheduler Dashboard</Link>
            </Menu.Item>}
            {!isTeamUser && <Menu.Item key="reports" icon={<ReportsIcon />}>
              <Link to="/reports">Reports</Link>
            </Menu.Item>}

            <Divider />

            <Text className="t-subheader">CONTENT MANAGEMENT</Text>


            {(canIShow(teamSelectionContext.team.id, "Contests", "R") || !isTeamUser) && <Menu.Item key="contests" icon={<ContestsIcon />}>
              <Link to="/contests">Contests</Link>
            </Menu.Item>}

            {(canIShow(teamSelectionContext.team.id, "Polls", "R") || !isTeamUser) && <Menu.Item key="polls" icon={<ContestsIcon />}>
              <Link to="/polls">Polls</Link>
            </Menu.Item>}{' '}

            {(canIShow(teamSelectionContext.team.id, "Posts", "R") || !isTeamUser) && <Menu.Item key="posts" icon={<ContestsIcon />}>
              <Link to="/news">Posts</Link>
            </Menu.Item>}

            <Menu.Item key="BattleGround" icon={<ContestsIcon />}>
              <Link to="/BattleGround">Battleground</Link>
            </Menu.Item>

            {(canIShow(teamSelectionContext.team.id, "Cms", "R") || !isTeamUser) && <Menu.Item key="cms" icon={<ContestsIcon />}>
              <Link to="/cms">Cms</Link>
            </Menu.Item>}





            {/* <Divider /> */}
            {/* <Text className="t-subheader">MANAGE</Text> */}
            {/* <Menu.Item key="notifications" icon={<NotificationsIcon />}>
              <Link to="">Notifications</Link>
            </Menu.Item>
            <Menu.Item key="settings" icon={<SettingsIcon />}>
              <Link to="">Settings</Link>
            </Menu.Item> */}
          </Menu>
        </Sider>
      </Layout>
    </SidebarWrapper>
  );
};

export default Sidebar;
