import styled from 'styled-components/macro';
import * as React from 'react';
import {
  Breadcrumb,
  Button,
  Card,
  Col,
  Divider,
  Input,
  Row,
  Menu,
  Dropdown,
  AutoComplete,
  DatePicker,
  Form,
  Upload,
} from 'antd';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import Text from 'antd/lib/typography/Text';
import { DownOutlined } from '@ant-design/icons';
import {
  useCreatePostMutation,
  // useCreateTeamMutation,
  useGetAllCountriesQuery,
  useGetPostByIdQuery,
  useGetTeamCategoriesQuery,
  useGetTeamCurrencyQuery,
  useGetTeamsQuery,
  useGetTeamStatusesQuery,
  useUpdatePostByIdMutation,
} from 'generated/graphql';
import { useHistory, useParams } from 'react-router-dom';
import { handleMediaUpload } from 'utils/handleMediaUpload';
import { RcFile } from 'antd/lib/upload';
import { StatusOptionsEnum } from 'utils/enums/DropdownOptions';
import LabelInput from 'components/common/LabelInput';
import { toast } from 'react-toastify';
import SelectElement from 'components/common/SelectElement';
import TitleCta from 'components/common/TitleCta';
import Title from 'antd/lib/typography/Title';
import TextEditorInput from 'components/common/TextEditorInput';
import moment from 'moment';

const EditPostWrapper = styled.div`
  padding: 0 40px;
  width: 100%;

  .t-transactions {
    font-size: 25px;
    line-height: 30px;
    color: rgba(5, 31, 36, 0.3);
  }
  .t-transaction-id {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .ant-card {
    margin-top: 40px;
    width: 100%;
    .info-edit-wrapper {
      margin-bottom: 40px;
    }
    .t-info {
      font-weight: 600;
      font-size: 20px;
      line-height: 24px;
      color: #051f24;
    }
    .t-transaction-key-field {
      font-size: 15px;
      line-height: 18px;
      color: rgba(5, 31, 36, 0.6);
    }
    .t-transaction-value {
      font-size: 16px;
      line-height: 19px;
      font-weight: 500;
      color: #051f24;
      margin: 10px;
      &.blue {
        color: #4da1ff;
      }
    }
    .key-value-wrapper {
      row-gap: 10px;
    }
    .edit-col {
      display: flex;
      align-items: center;
      column-gap: 20px;
    }
  }

  .ant-btn.add-new-button {
    border: 1px solid rgba(7, 105, 125, 0.25);
    color: rgba(7, 105, 125, 1);
    box-sizing: border-box;
    border-radius: 5px;
    height: fit-content;
    padding: 15px;
    display: flex;
    align-items: center;
    column-gap: 10px;
  }

  .sr-no-wrapper {
    position: relative;
    .srno-default-pop {
      position: absolute;
      background: #4aaf05;
      border: 2px solid #4aaf05;
      font-size: 12px;
      padding: 0 6px;
      top: -10px;
      left: 12px;
      color: #fff;
      border-radius: 100px;
    }
  }

  .sub-routes-wrapper {
    column-gap: 8px;
    margin: 60px auto;
    text-align: center;
    .sub-route {
      padding: 8px;
      width: 100%;
      background: rgba(198, 198, 198, 0.6);
      color: rgba(5, 31, 36, 0.6);
      border-radius: 4px;
      cursor: pointer;
      font-weight: 600;
      &.active {
        background: #fff;
        border: 1px solid #eee;
        color: #4da1ff;
      }
    }
  }
  a {
    text-decoration: none;
    color: inherit;
  }
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.discard {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface EditPostProps { }

const EditPost = ({ }: EditPostProps): JSX.Element => {
  const [form] = Form.useForm();
  const { id } = useParams<{ id: string }>();

  const { data: postById } = useGetPostByIdQuery({
    variables: { postId: +id },
  });

  const [updatePost] = useUpdatePostByIdMutation();

  const { data: teams } = useGetTeamsQuery();


  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const history = useHistory();

  const onSave = async (formValues: any) => {
    const { teamId, postUrl, title, publishedAt, body } = formValues;
    setConfirmLoading(true);

    await updatePost({
      variables: {
        teamId,
        postUrl,
        title,
        publishedAt,
        postId: +id,
        description:body,
      },
    });
    setConfirmLoading(false);

    // history.push('/team-home');
    toast.success(`Updated Post: ${title}`);
  };

  const onMessageStore = (body: any) => {
    setMessage(body);
    form.setFieldsValue({ body });
  };

  return (
    <EditPostWrapper>
      <Form onFinish={onSave} form={form} preserve={false}>
        <Card>
          <TitleCta
            title="Edit Post"
            cta={
              <>
                <Button className="save cta-button" onClick={form.submit}>
                  {confirmLoading ? 'Creating...' : 'Save'}
                </Button>
                <Button
                  className="cta-button discard"
                  onClick={() => history.push('/team-home')}
                >
                  Discard
                </Button>
              </>
            }
          />

          <Row gutter={[12, 0]}>
            <Col span={6}>
              <LabelInput
                label="Title"
                name="title"
                initialValue={postById?.post_by_pk?.title ?? ""}
                rules={[
                  {
                    required: true,
                    message: 'Title is required!',
                  },
                ]}
                inputElement={
                  <Input
                    defaultValue={postById?.post_by_pk?.title ?? ""}
                    type="text"
                    onChange={(e) =>
                      form.setFieldsValue({ name: e.target.value })
                    }
                  />
                }
              />
            </Col>
            <Col span={6}>
              <LabelInput
                label="Team"
                name="teamId"
                initialValue={postById?.post_by_pk?.teamId + ''}
                rules={[
                  {
                    required: true,
                    message: 'Team Name is required!',
                  },
                ]}
                inputElement={
                  <SelectElement
                    onChange={(e: any) => {
                      form.setFieldsValue({ teamId: e });
                    }}
                    toStore="id"
                    defaultValue={postById?.post_by_pk?.team?.name}
                    options={teams?.team ?? []}
                    toFilter="name"
                    placeholder="Team"
                  />
                }
              />
            </Col>
            <Col span={6}>
              <LabelInput
                label="Published Date"
                name="publishedAt"
                initialValue={postById?.post_by_pk?.publishedAt}
                inputElement={
                  <DatePicker
                    format="DD-MM-YYYY"
                    defaultValue={moment(postById?.post_by_pk?.publishedAt)}
                    onChange={(e: any) => {
                      form.setFieldsValue({
                        publishedAt: moment(e?._d).format(
                          'YYYY-MM-DDTHH:mm:ssZ'
                        ),
                      });
                    }}
                  />
                }
              />
            </Col>
          </Row>
        </Card>

        <Card>
          <LabelInput
            label="Message"
            name="body"
            rules={[{ required: true, message: 'Post Message is required!' }]}
            initialValue={postById?.post_by_pk?.description ?? ""}
            inputElement={
              <TextEditorInput value={message} setValue={onMessageStore} />
            }
          />
          <LabelInput
            label="Attachment"
            name="postUrl"
            rules={[{ required: true, message: 'Post Image is required!' }]}
            initialValue={postById?.post_by_pk?.postUrl ?? ''}
            inputElement={
              <Upload
                iconRender={() => <></>}
                customRequest={async ({ file, fileList, onProgress }: any) => {
                  form.setFieldsValue({
                    postUrl: await handleMediaUpload(
                      file as RcFile,
                      onProgress
                    ),
                  });
                }}
              >
                <Button className="upload-button">+</Button>
              </Upload>
            }
          />
          <a
            href={postById?.post_by_pk?.postUrl ?? '#'}
            target="_blank"
            rel="noopener noreferrer"
          >
            Current Uploaded
          </a>
        </Card>
      </Form>
    </EditPostWrapper>
  );
};

export default EditPost;
