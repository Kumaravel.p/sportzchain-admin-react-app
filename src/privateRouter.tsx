import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import currentSession from 'utils/getAuthToken';
import { useState, useEffect } from 'react';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const access = (path: any, isIamTeamUser: any) => {
  const isTeamUser = isIamTeamUser();
  let canIAllow = true;

  if (isTeamUser) {
    canIAllow = [
      '/team-home',
      '/team/:id',
      '/contests/add',
      '/contest/:contestId/edit',
      '/contests',
      '/cms',
      '/contest/:teamId/:contestId',
      '/polls',
      '/polls/:teamId/:id',
      '/poll/add',
      '/poll/edit/:id',
      '/posts',
      '/posts/:id',
      '/post/add',
      '/post/edit/:id',
      '/listing-sto',
      '/view-sto',
      '/news',
      '/news/edit/:id',
      '/news/:teamId/:id',
      '/news/add',
      '/profile' 
    ].includes(path);
  }

  return canIAllow;
};

const PrivateRoute = ({ path, children, ...rest }: any): JSX.Element => {
  const [isAuthenticate, setIsAuthenticate] = useState(true);

  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  useEffect(() => {
    currentSession()
      .then((res) => {
        const token = res?.getJwtToken();
        if (token) {
          setIsAuthenticate(true);
        } else {
          setIsAuthenticate(false);
        }
      })
      .catch((ex) => {
        setIsAuthenticate(false);
      });
  }, []);
  // console.log(isAuthenticate)
  const RouterRedirect = () => {
    return (
      <>
        <Route
          {...rest}
          render={(_) =>
            isAuthenticate && access(path, isIamTeamUser) ? (
              children
            ) : (
              <Redirect
                to={{
                  pathname: '/signin/email',
                  state: { from: _?.location },
                }}
              />
            )
          }
        />
      </>
    );
  };
  return <RouterRedirect />;
};

export default PrivateRoute;
