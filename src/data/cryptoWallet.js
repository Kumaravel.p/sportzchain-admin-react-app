import format from 'date-fns/format';

const walletColumns = [
  {
    title: 'TEAM',
    dataIndex: ['coinSymbol'],
    render: (coinSymbol) => <div>{coinSymbol ?? ''}</div>,
  },
  {
    title: 'QUANTITY',
    dataIndex: 'balance',
    render: (balance) => <div>{balance ?? 0}</div>,
  },
  // {
  //   title: 'USD',
  //   dataIndex: 'usd',
  // },
  // {
  //   title: 'GAS FEES',
  //   dataIndex: 'gasFee',
  // },
  // {
  //   title: 'LEVEL',
  //   dataIndex: 'level',
  // },
  {
    title: 'LAST USED',
    dataIndex: 'lastUsed',
    render: (lastUsed) => (
      <div className="last-used-leaderboard">
        {format(new Date(lastUsed), 'PP')}
      </div>
    ),
  },
  // { title: '', render: () => <EditIcon /> },
];
const walletData = [
  {
    team: 'Mumbai Indians',
    quantity: '100 MI',
    usd: '100 USD',
    gasFees: '10 USD',
    level: 'Vested',
    lastUsed: '2019/12/01, 08:05',
  },
  {
    team: 'Brooklyn Simmons',
    quantity: '100 MI',
    usd: '100 USD',
    gasFees: '10 USD',
    level: 'Leader Focused',
    lastUsed: '2019/12/01',
  },
];

export { walletColumns, walletData };
