import config from 'config';
import { format } from 'date-fns';

const teamDocumentsColumns = [
  {
    title: '#',
    dataIndex: '',
    render: (text, record,index) => index + 1,
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    render: (_, data) => (
      <a target="_blank" rel="noopener noreferrer" href={`${config.apiBaseUrl}files/${data?.url}`} style={{color:'#1890ff'}}>
        {data.name}
      </a>
    ),
  },
  {
    title: 'ADDED BY',
    dataIndex: ['user', 'name'],
  },
  {
    title: 'ADDED DATE',
    dataIndex: 'uploadedAt',
    render: (uploadedAt) => format(new Date(uploadedAt), 'Pp')
  },
];

const teamDocumentsData = [
  {
    key: 1,
    name: 'Cameron Williamson',
    addedBy: 'Cameron Williamson',
    addedDate: '20/02/2020,  08:15',
  },
  {
    key: 2,
    name: 'Brooklyn Simmons',
    addedBy: 'Brooklyn Simmons',
    addedDate: '20/02/2020,  08:15',
  },
];

export { teamDocumentsColumns, teamDocumentsData };
