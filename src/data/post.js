import { format } from 'date-fns';

const postColumns = [
  {
    title: 'ID',
    dataIndex: 'id',
  },
  {
    title: 'Team',
    dataIndex: ['team', 'name'],
  },
  {
    title: 'Title',
    dataIndex: 'title',
  },
  {
    title: 'Published At',
    dataIndex: 'publishedAt',
    render: (publishedAt) => format(new Date(publishedAt), 'Pp'),
  },
  {
    title: 'Views',
    dataIndex: 'viewsCount',
  },
  //   {
  //     title: 'Shares',
  //     dataIndex: 'createdAt',
  //   },
];

export { postColumns };
