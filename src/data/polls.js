import { format } from 'date-fns';
import moment from 'moment';

const pollsColumns = [
  {
    title: 'ID',
    dataIndex: 'id',
    sorter: (a,b) => a.id-b.id
  },
  {
    title: 'Team',
    dataIndex: ['teamByTeamId', 'name'],
    sorter: (a,b) => a.teamByTeamId.name.localeCompare(b.teamByTeamId.name)
  },
  {
    title: 'Title',
    dataIndex: 'title',
    sorter: (a,b) => a.title.localeCompare(b.title)},
  {
    title: 'Duration',
    dataIndex: 'durations',
    render: (_, data) => {
      return (
      <div>
        {/* {format(new Date(data.startAt), 'P')} -{' '}
        {format(new Date(data.endAt), 'P')} */}
        {`${moment(data?.startAt+'z').format("Do MMM YY hh:mm A")} - ${moment(data?.endAt+'z').format("Do MMM YY hh:mm A")}`}
      </div>
    )},
  },
  {
    title: 'Participation',
    dataIndex:'totalVoteCount',
  },

  //   {
  //     title: 'Views',
  //     dataIndex: 'createdAt',
  //   },
  //   {
  //     title: 'Shares',
  //     dataIndex: 'createdAt',
  //   },
];

export { pollsColumns };
