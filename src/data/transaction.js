import Text from 'antd/lib/typography/Text';
import format from 'date-fns/format';

const transactionColumns = [
  {
    title: 'Status',
    dataIndex: 'status',
    render: (status) => <Text className="t-transaction-status">{status}</Text>,
  },
  {
    title: 'Time',
    dataIndex: 'endAt',
    render: (endAt) => endAt && <div>{format(new Date(endAt), 'PP')}</div>,
  },
  {
    title: 'Transaction Type',
    dataIndex: 'type',
  },
  {
    title: 'From',
    dataIndex: 'from',
    render: (_, data) => (
      <div>
        {data?.fromCoinAmount}
        &nbsp;
        {data?.fromCoin}
      </div>
    ),
  },
  {
    title: 'To',
    dataIndex: 'to',
    render: (_, data) => (
      <div>
        {data?.toCoinAmount}
        &nbsp;
        {data?.toCoin}
      </div>
    ),
  },
  {
    title: 'Exchange Rate',
    dataIndex: 'exchangeRate',
  },
];

const transactionData = [
  {
    status: 'Successful',
    time: 'Feb 28, 7:00pm',
    transactionType: 'Token swap',
    from: '2MI',
    to: '100 SPN',
    exchangeRate: '10MI/SPN',
  },
  {
    status: 'Successful',
    time: 'Feb 28, 7:00pm',
    transactionType: 'Token swap',
    from: '2MI',
    to: '100 SPN',
    exchangeRate: '10MI/SPN',
  },
  {
    status: 'Successful',
    time: 'Feb 28, 7:00pm',
    transactionType: 'Token swap',
    from: '2MI',
    to: '100 SPN',
    exchangeRate: '10MI/SPN',
  },
];

export { transactionColumns, transactionData };
