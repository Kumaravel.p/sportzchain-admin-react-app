import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import { format } from 'date-fns';

const notesColumns = [
  {
    title: '#',
    dataIndex: 'key',
    render: (text, record,index) => index + 1,
  },
  {
    title: 'NAME',
    dataIndex: 'name',
  },
  {
    title: 'ADDED BY',
    dataIndex: ['user', 'name'],
  },
  {
    title: 'ADDED DATE',
    dataIndex: 'createdAt',
    render: (createdAt) => format(new Date(createdAt), 'PP'),
  },
];

const notesData = [
  {
    key: 1,
    name: 'Cameron Williamson',
    addedBy: 'Cameron Williamson',
    addedDate: '20/02/2020,  08:15',
  },
  {
    key: 2,
    name: 'Brooklyn Simmons',
    addedBy: 'Brooklyn Simmons',
    addedDate: '20/02/2020,  08:15',
  },
];

export { notesColumns, notesData };
