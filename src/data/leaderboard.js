import Text from 'antd/lib/typography/Text';
import defaultImage from 'assets/images/default_image.png';

const leaderboardColumns = [
  {
    title: 'LEADERBOARD',
    dataIndex: 'position',
    render: (position) => <div className="table-text-blue">{position}</div>,
  },
  {
    title: 'RANK',
    dataIndex: ['user']['leaderboardRank'],
    render: (position) => <div className="table-text-blue">{position}</div>,
  },
  {
    title: 'AVATAR',
    dataIndex: ['user']['imageURL'],
    render: (avatar) => (
      <img
        className="table-user-avatar"
        src={avatar ? avatar : defaultImage}
        alt="user"
      />
    ),
  },
  {
    title: 'NAME',
    dataIndex: 'name',
  },

  {
    title: 'RWRD',
    dataIndex: ['user']['wallets'],
    render: (wallet) => <div>{wallet?.balance || '0'}</div>,
  },
  //   {
  //     title: 'STATUS',
  //     dataIndex: 'status',
  //     render: (status) => <Text className="t-table-status">{status}</Text>,
  //   },
];

export { leaderboardColumns };
