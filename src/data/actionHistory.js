import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import { format } from 'date-fns';

const actionHistoryColumns = [
  {
    title: '#',
    // dataIndex: 'key',
  },
  {
    title: 'ACTION',
    dataIndex: 'action',
  },
  {
    title: 'ACTION BY',
    dataIndex: 'teamId',
  },
  {
    title: 'ACTION DATE',
    dataIndex: 'createdAtTimestamp',
    render: (createdAtTimestamp) => format(new Date(createdAtTimestamp), 'Pp'),
  },
  { title: '', render: () => <EditIcon /> },
];

const actionHistoryData = [
  {
    key: 1,
    action: 'Added contact',
    actionBy: 'Cameron Williamson',
    actionDate: '20/02/2020,  08:15',
  },
  {
    key: 2,
    action: 'Added posts',
    actionBy: 'Brooklyn Simmons',
    actionDate: '20/02/2020,  08:15',
  },
];

export { actionHistoryColumns, actionHistoryData };
