import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';

const activityLogsColumns = [
  {
    title: 'SR. NO',
    dataIndex: 'srno',
  },
  {
    title: 'ACTIVITY LOG',
    dataIndex: 'activityLog',
  },
  {
    title: 'EDITED BY',
    dataIndex: 'editedBy',
  },
  {
    title: 'LAST ACTION TIME',
    dataIndex: 'lastActionTime',
  },
];
const activityLogsData = [
  {
    srno: 1,
    activityLog: 'Edited reward from 100-90RWRD',
    editedBy: 'Alex S',
    lastActionTime: '2019/12/01, 08:05',
  },
  {
    srno: 2,
    activityLog: 'Edited reward from 100-90RWRD',
    editedBy: 'Alex S',
    lastActionTime: '2019/12/01',
  },
];

export { activityLogsColumns, activityLogsData };
