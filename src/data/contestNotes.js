import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import { format } from 'date-fns';

const contestNotesColumns = [
  {
    title: '#',
    dataIndex: 'key',
  },
  {
    title: 'content',
    dataIndex: 'content',
  },
  {
    title: 'ADDED BY',
    dataIndex: ['user', 'name'],
  },
  {
    title: 'ADDED DATE',
    dataIndex: 'createdAt',
    render: (createdAt) => format(new Date(createdAt), 'PP'),
  },
];

export { contestNotesColumns };
