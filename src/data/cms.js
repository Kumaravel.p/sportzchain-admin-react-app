import EditIcon from 'assets/icons/editIcon.svg';
const cmsColumns = [
  {
    title: 'Content Type',
    dataIndex: 'contentType',
  },
  {
    title: 'Content Name',
    dataIndex: "contentName",
  },
  {
    title: 'Updated On',
    dataIndex: 'updatedOn',
  }
];

export { cmsColumns };
