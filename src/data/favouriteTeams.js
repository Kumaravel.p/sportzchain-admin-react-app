import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import format from 'date-fns/format';

const favouriteTeamsColumn = [
  {
    title: 'Team',
    dataIndex: ['team', 'name'],
    width: 700,
  },
  {
    title: 'Favourite Date',
    dataIndex: 'likedAt',
    render: (likedAt) => <div>{format(new Date(likedAt), 'PP')}</div>,
  },

  // { title: '', render: () => <EditIcon /> },
];
const favouriteTeamsData = [
  {
    team: 'Mumbai Indians',
    favouriteDate: '2019/12/01, 08:05',
  },
  {
    team: 'Brooklyn Simmons',
    favouriteDate: '2019/12/01, 08:05',
  },
];

export { favouriteTeamsColumn, favouriteTeamsData };
