import EditIcon from 'assets/icons/editIcon.svg';
const teamBannerColumns = [
  {
    title: 'Team Name',
    dataIndex: 'name',
  },
  {
    title: 'Updated On',
    dataIndex: 'updatedOn',
  }
];

export { teamBannerColumns };
