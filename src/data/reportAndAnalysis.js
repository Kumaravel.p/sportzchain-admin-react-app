import Text from 'antd/lib/typography/Text';
import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';

const reportAndAnalysisColumns = [
  {
    title: 'REPORT NAME',
    dataIndex: 'reportName',
    render: (name) => <Text className="t-report-name">{name}</Text>,
  },
  {
    title: 'DESCRIPTION',
    dataIndex: 'description',
  },
  {
    title: 'FREQUENCY',
    dataIndex: 'frequency',
  },
  {
    title: 'METRIC TYPE',
    dataIndex: 'metricType',
    render: (metricType) => (
      <Text className="t-report-metric-type">{metricType}</Text>
    ),
  },
];

const reportAndAnalysisData = [
  {
    key: 1,
    reportName: 'Avg. time on page',
    description: 'Number of page/secs a visitor spends on a page',
    frequency: 'Session',
    metricType: 'Time',
  },
  {
    key: 2,
    reportName: '# views to page',
    description: 'Number of views to a specific page',
    frequency: 'Daily',
    metricType: 'Traffic',
  },
  {
    key: 3,
    reportName: 'Avg. time on page',
    description: 'Number of page/secs a visitor spends on a page',
    frequency: 'Session',
    metricType: 'Time',
  },
  {
    key: 4,
    reportName: 'Avg. time on page',
    description: 'Number of page/secs a visitor spends on a page',
    frequency: 'Session',
    metricType: 'Time',
  },
  {
    key: 5,
    reportName: 'Avg. time on page',
    description: 'Number of page/secs a visitor spends on a page',
    frequency: 'Session',
    metricType: 'Time',
  },
];

export { reportAndAnalysisColumns, reportAndAnalysisData };
