import { format } from 'date-fns';
import moment from 'moment';

const newsColumns = [
  {
    title: 'ID',
    dataIndex: 'teamId',
    sorter: (a,b) => a.id-b.id
  },
  {
    title: 'TEAM',
    dataIndex: ['teamByTeamId', 'name'],
    sorter: (a,b) => a.teamByTeamId.name.localeCompare(b.teamByTeamId.name)
  },
  {
    title: 'TITLE',
    dataIndex: 'title',
    sorter: (a,b) => a.title.localeCompare(b.title)},
  {
    title: 'Published On',
    dataIndex: 'publishedAt',
    render: (_, data) => {
      return (
      <div>
        {data?.publishedAt === null ? "" : moment(data.publishedAt).format("Do MMM YY hh:mm A")}
      </div>
    )},
  },
  {
    title: 'Views',
    dataIndex:'viewsCount',
  },
  {
    title: 'Shares',
    dataIndex:'numOfShares',
  },

  //   {
  //     title: 'Views',
  //     dataIndex: 'createdAt',
  //   },
  //   {
  //     title: 'Shares',
  //     dataIndex: 'createdAt',
  //   },
];

export { newsColumns };
