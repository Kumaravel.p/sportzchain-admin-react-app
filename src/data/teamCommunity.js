const teamCommunityColumns = [
  {
    title: 'AVATAR',
    dataIndex: 'imageURL',
    render: (avatar) => (
      <img
        className="table-user-avatar"
        src={
          avatar
            ? avatar
            : 'https://images.unsplash.com/photo-1553514029-1318c9127859?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mzh8fHdvbWFuJTIwZmFjZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80'
        }
        alt="user"
      />
    ),
  },

  {
    title: 'NAME',
    dataIndex: 'name',
  },

  {
    title: 'TOKENS',
    dataIndex: 'wallets',
    render: (wallets) => <div>{wallets[0].balance}</div>,
  },

  // {
  //   title: 'RANK',
  //   dataIndex: 'leaderboardRank',
  // },

  // {
  //   title: 'LEADERBOARD',
  //   dataIndex: 'leaderboard',
  //   render: (leaderboard) => (
  //     <Text className="t-table-leaderboard">{leaderboard}</Text>
  //   ),
  // },
];

const teamCommunityData = [
  {
    key: 1,
    name: `Tanya Hill`,
    age: 32,
    tokens: '1,000 MI',
    rank: '#001',
    lifetimeValue: '10,000 SPN',
    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    leaderboard: `Vested`,
  },

  {
    key: 2,
    name: `Tanya Hill`,
    age: 32,
    tokens: '1,000 MI',
    rank: '#002',
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',

    leaderboard: `Vested`,
  },
  {
    key: 3,
    contactDetails: 'tanya.hill @example.com',
    name: `Tanya Hill`,
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    tokens: '1,000 MI',
    rank: '#003',
    leaderboard: `Leaderboard`,
  },
  {
    key: 4,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',
    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    tokens: '1,000 MI',
    rank: '#004',
    leaderboard: `Focused`,
  },
  {
    key: 5,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    tokens: '1,000 MI',
    rank: '#001',
    leaderboard: `Focused`,
  },
  {
    key: 6,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    tokens: '1,000 MI',
    rank: '#001',
    leaderboard: `Focused`,
  },
  {
    key: 7,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    tokens: '1,000 MI',
    rank: '#001',
    leaderboard: `Vested`,
  },
];

export { teamCommunityColumns, teamCommunityData };
