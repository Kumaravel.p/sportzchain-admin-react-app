import { format } from 'date-fns';

const contestColumns = [
  {
    title: 'Status',
    dataIndex: 'statusId',
    sorter: (a,b) => a.statusId.localeCompare(b.statusId),
  },
  {
    title: 'Type',
    dataIndex: 'contestTypeId',
  },
  {
    title: 'Team',
    dataIndex: ['teamByTeamId', 'name'],
    sorter: (a,b) => a.teamByTeamId?.name.localeCompare(b?.teamByTeamId?.name),
  },
  {
    title: 'Title',
    dataIndex: 'title',
    sorter: (a,b) => a.title.localeCompare(b.title),
  },
  {
    title: 'Duration',
    dataIndex: 'startAt',
    render: (_, data) => (
      <>
        {format(new Date(data.startAt), 'Pp')} -{' '}
        {format(new Date(data.endAt), 'Pp')}
      </>
    ),
    sorter: (a,b) => {
      var dateA = new Date(a.startAt), dateB = new Date(b.startAt)
	    return dateA - dateB
    },
  },
  {
    title: 'Participation',
    dataIndex: ['teamPollUserOptions_aggregate', 'aggregate', 'count'],
  },

  //   {
  //     title: 'Views',
  //     dataIndex: 'createdAt',
  //   },
  //   {
  //     title: 'Shares',
  //     dataIndex: 'createdAt',
  //   },
];
export { contestColumns };
