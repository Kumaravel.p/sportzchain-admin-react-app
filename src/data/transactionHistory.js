import { format } from 'date-fns';

// interface TransactionType {
//   id: number;
//   endAt: any;
//   type: string;
//   fromCoinAmount: number;
//   fromCoin: string;
//   toCoinAmount: number;
//   toCoin: string;
//   gasFee: number;
//   exchangeRate: number;
//   status: string;
// }[];

const transactionHistoryColumn = [
  {
    title: 'PAY ID',
    dataIndex: 'id',
  },
  {
    title: 'DATE',
    dataIndex: 'endAt',
    render: (endAt) => format(new Date(endAt), 'Pp'),
  },
  {
    title: 'TRANSACTION TYPE',
    dataIndex: 'type',
  },
  {
    title: 'FROM',
    dataIndex: 'from',
    render: (_, data) => (
      <div>
        {data?.fromCoinAmount}
        &nbsp;
        {data?.fromCoin}
      </div>
    ),
  },
  {
    title: 'TO',
    dataIndex: 'to',
    render: (_, data) => (
      <div>
        {data?.toCoinAmount}
        &nbsp;
        {data?.toCoin}
      </div>
    ),
  },
  {
    title: 'GAS FEES',
    dataIndex: 'gasFee',
  },
  {
    title: 'EXCHANGE RATE',
    dataIndex: 'exchangeRate',
  },
  {
    title: 'STATUS',
    dataIndex: 'status',
  },
];
const transactionHistoryData = [
  {
    payId: '898775',
    date: 'Feb 28, 2021',
    transactionType: 'Token Exchange',
    from: '2MI',
    to: '100 SPN',
    gasFees: '2 USD',
    exchangeRate: '10MI/SPN',
    status: 'Processing',
  },
];

export { transactionHistoryColumn, transactionHistoryData };
