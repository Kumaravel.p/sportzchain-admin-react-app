import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import config from 'config';
import moment from 'moment';

const contractsColumns = [
  {
    title: 'DOCUMENT NAME',
    dataIndex: 'fileName',
    render: (_, data) => (
      <a target="_blank" rel="noopener noreferrer" href={`${config.apiBaseUrl}files/${data.contractULR}`} style={{color:'#1890ff'}}>
        {data.fileName}
      </a>
    ),
  },
  {
    title: 'STATUS',
    dataIndex: 'statusId',
  },
  // {
  //   title: 'SPORTZCHAIN',
  //   dataIndex: 'superAdminSignerId',
  // },
  // {
  //   title: 'TEAM SIGN',
  //   dataIndex: 'teamAdminSignerId',
  // },
  {
    title: 'START DATE',
    dataIndex: 'startAt',
    render: (startAt) => moment(startAt).format('DD/MM/YYYY hh:mm A'),
  },
  {
    title: 'EXPIRY DATE',
    dataIndex: 'endAt',
    render: (endAt) => moment(endAt).format('DD/MM/YYYY hh:mm A'),
  },
  // { title: '', render: () => <EditIcon /> },
];

const contractsData = [
  {
    documentName: 'First Contract',
    status: 'Active',
    sportzchain: 'Arjit Gupta',
    teamSign: 'Jake Paul',
    startDate: '2019/12/01',
    expiryDate: '2019/12/01',
  },
  {
    documentName: 'MOU',
    status: 'Active',
    sportzchain: 'Arjit Gupta',
    teamSign: 'Jake Paul',
    startDate: '2019/12/01',
    expiryDate: '2019/12/01',
  },
];

export { contractsColumns, contractsData };
