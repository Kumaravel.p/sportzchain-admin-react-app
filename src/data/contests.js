import Text from 'antd/lib/typography/Text';
import { format } from 'date-fns';

const contestsColumns = [
  {
    title: 'STATUS',
    dataIndex: 'status',
    render: () => <Text className="t-contest-active">Active</Text>,
  },
  {
    title: 'ID',
    dataIndex: 'key',
  },
  {
    title: 'TITLE',
    dataIndex: 'title',
  },
  {
    title: 'DURATION',
    dataIndex: 'duration',
    render: (_, data) => (
      <>
        {format(new Date(data.startAt), 'Pp')} -
        {format(new Date(data.endAt), 'Pp')}
      </>
    ),
  },
  // {
  //   title: 'PARTICIPATION',
  //   dataIndex: 'participation',
  //   render: (participation, data) => (
  //     <Text className="t-contest-participation">
  //       {data?.participation} &nbsp;
  //       <span>{data?.change}</span>
  //     </Text>
  //   ),
  // },
];
const contestsData = [
  {
    status: 'Active',
    id: '123829',
    title: 'How well do you know the team?',
    duration: '20 Dec- 3Jan 2022',
    participation: '20,049',
    change: '+300%/ day',
  },
  {
    status: 'Active',
    id: '123829',
    title: 'How well do you know the team?',
    duration: '20 Dec- 3Jan 2022',
    participation: '20,049',
    change: '+300%/ day',
  },
];

export { contestsColumns, contestsData };
