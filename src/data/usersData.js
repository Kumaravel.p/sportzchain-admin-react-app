import Text from 'antd/lib/typography/Text';
import defaultImage from 'assets/images/default_image.png';

const usersColumns = [
  {
    title: 'AVATAR',
    dataIndex: 'imageURL',
    render: (avatar) => (
      <img
        className="table-user-avatar"
        src={avatar ? avatar : defaultImage}
        alt="user"
      />
    ),
  },
  {
    title: 'NAME',
    dataIndex: 'name',
  },

  {
    title: 'CONTACT DETAILS',
    dataIndex: 'email',
    render: (email) => <div>{email || '-'}</div>,
  },
  {
    title: 'LIFETIME VALUE',
    dataIndex: 'lifetimeSPNValue',
    render: (lifetimeSPNValue) => <div>{lifetimeSPNValue || '0'}</div>,
  },
  {
    title: 'STATUS',
    dataIndex: 'status',
    render: (status) => <Text className="t-table-status">{status}</Text>,
  },
];

const usersData = [
  {
    key: 1,
    name: `Tanya Hill`,
    age: 32,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',
    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    status: `Active`,
  },

  {
    key: 2,
    name: `Tanya Hill`,
    age: 32,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',

    status: `Active`,
  },
  {
    key: 3,
    contactDetails: 'tanya.hill @example.com',
    name: `Tanya Hill`,
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    status: `Inactive`,
  },
  {
    key: 4,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    status: `KYC Approval`,
  },
  {
    key: 5,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    status: `KYC Approval`,
  },
  {
    key: 6,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    status: `KYC Approval`,
  },
  {
    key: 7,
    name: `Tanya Hill`,
    contactDetails: 'tanya.hill @example.com',
    lifetimeValue: '10,000 SPN',

    avatar:
      'https://images.unsplash.com/photo-1438761681033-6461ffad8d80?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80',
    age: 32,
    status: `Active`,
  },
];

export { usersColumns, usersData };
