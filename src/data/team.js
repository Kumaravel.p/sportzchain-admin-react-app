import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';

const teamColumns = [
  {
    title: 'NAME',
    dataIndex: 'name',
  },
  {
    title: 'ROLE',
    dataIndex: 'roleId',
  },
  {
    title: 'MOBILE NUMBER',
    dataIndex: 'primaryPhoneNumber',
  },
  {
    title: 'EMAIL',
    dataIndex: 'email',
  },
];
const teamData = [
  {
    name: 'Cameron Williamson',
    role: 'Manager',
    mobileNumber: '+91 9853278715',
    email: 'frostman@mac.com',
  },
  {
    name: 'Brooklyn Simmons',
    role: 'Marketing',
    mobileNumber: '+91 9853278715',
    email: 'frostman@mac.com',
  },
];

export { teamColumns, teamData };
