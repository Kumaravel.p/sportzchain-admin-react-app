import format from 'date-fns/format';

const rewardsRulesColumns = [
  // {
  //   title: 'SR. NO',
  //   dataIndex: 'id',
  //   render: (id) => (
  //     <div className="sr-no-wrapper">
  //       <Text className="srno">{id}</Text>
  //       {id === 1 && <div className="id-default-pop">Default</div>}
  //     </div>
  //   ),
  // },
  {
    title: 'RULE NAME',
    dataIndex: 'ruleName',
    render: (ruleName) => ruleName || 'Default rule',
  },
  {
    title: 'REWARD',
    dataIndex: 'amount',
  },
  {
    title: 'START',
    dataIndex: 'activeFrom',
    render: (activityFrom) =>
      (activityFrom && format(new Date(activityFrom), 'PP')) || '-',
  },
  {
    title: 'END',
    dataIndex: 'activeTill',
    render: (activityTill) =>
      (activityTill && format(new Date(activityTill), 'PP')) || '-',
  },
  {
    title: 'LIMIT',
    dataIndex: 'maxApplyCount',
    render: (_, data) => (
      <>
        {data.maxApplyCount} per {data.duration}
      </>
    ),
  },
];

const rewardsRulesData = [
  {
    id: 1,
    ruleName: 'Christmas Reward',
    reward: '15 RWRD',
    start: '15 Dec 2021',
    end: '31 Dec 2025',
    limit: '2 per day',
  },
  {
    id: 2,
    ruleName: 'IPL Reward',
    reward: '25 RWRD',
    start: '31 Dec 2021',
    end: '10 Jan 2022',
    limit: '10 per week',
  },
];

export { rewardsRulesColumns, rewardsRulesData };
