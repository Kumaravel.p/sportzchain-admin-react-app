import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import { format } from 'date-fns';

const contestActionHistoryColumns = [
  {
    title: '#',
    dataIndex: 'id',
  },
  {
    title: 'ACTION',
    dataIndex: 'action',
  },
  {
    title: 'ACTION BY',
    dataIndex: ['user', 'name'],
  },
  {
    title: 'ACTION DATE',
    dataIndex: 'createdAt',
    render: (createdAt) => format(new Date(createdAt), 'Pp'),
  },
];

const contestActionHistoryData = [
  {
    key: 1,
    action: 'Added contact',
    actionBy: 'Cameron Williamson',
    actionDate: '20/02/2020,  08:15',
  },
  {
    key: 2,
    action: 'Added posts',
    actionBy: 'Brooklyn Simmons',
    actionDate: '20/02/2020,  08:15',
  },
];

export { contestActionHistoryColumns, contestActionHistoryData };
