import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';

const marketColumns = [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Age',
    dataIndex: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
  {
    title: 'Address',
    dataIndex: 'address',
  },
];
const marketData = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York ',
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London ',
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney ',
  },
];

export { marketColumns, marketData };
