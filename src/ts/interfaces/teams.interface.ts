export interface TeamCommunityPayload {
    start: number,
    length: number,
    member: string,
    teamId: number,
}

export interface TeamContactPayload {
    id?: number
    division: string[];
    email: string;
    name: string;
    primaryPhoneNumber: string;
    roleId: string;
    secondaryPhoneNumber: string;
    secondaryCountryCode: string;
    primaryCountryCode: string;
    teamId: number;
    userId: number;
}