export interface ListingActivityPointsProps {
    length: number;
    start: number;
    category: string;
    activity: string;
    searchText: string;
}


export interface Rule {
    ruleName: string;
    activityPointPerUser: string;
    startAt: string;
    endAt: string;
    maxLimit: number | null;
    statusId: number;
    duration: number | null;
}

export interface Note {
    addedDate: string;
    title: string;
    description: string;
    addedby: string;
}


export interface ActivityPointCreationProps {
    id?: string;
    startAt: string;
    endAt: string;
    description: string;
    categoryId: number;
    subCategoryId: number;
    rules: Rule[];
    notes: Note[];
}