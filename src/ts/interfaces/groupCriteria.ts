export interface UpsertGroupCriteriaPayload{
    id?:number;
    criteriaId:number;
    usersCount:number;
    pollsCount:number;
    contestCount:number;
    pollParticipatePercentage:string;
    contestParticipatePercentage:string;
    effectiveFrom:string;
    effectiveTill:string;
}