export interface  CmsListPayload{
    // start?: number;
    // length?: number;
    searchText?: string;
}

export interface CmsCreatePayload  {
    contentId?:any;
    fileName?: any[];
    mobileViewFileName?: any[];
    teamId?: any;
}

export interface TeamListPayload {
    start?: number;
    length?: number;
    searchText?: string;
}
