export interface TeamPollOption {
    description: string;
    orderBy: number;
    uniqueKey?: string;
    imageUrl?: string;
    id?: number;
}

enum categorytype {
    PLATFORM = "PLATFORM",
    GROUPS = "GROUPS"
}
export interface Poll {
    id?: number;
    status: string;
    canMultiselect: boolean;
    minJoinBalance: number | string;
    teamId: number | string;
    title: string;
    endAt: any;
    startAt: any;
    displayStartAt: any;
    displayEndAt: any;
    pollPlatform: string;
    currency: string;
    pollTypeId: string;
    allowedParticipants: number | string;
    teamPollContentType: string;
    question: string;
    description: string;
    teamPollOptions: TeamPollOption[];
    teamByTeamId: any
    notes?: any[];
    category?: "PLATFORM" | "GROUPS";
    groupId?: string;

}

export interface PollFilterProps {
    start?: number;
    length?: number;
    status?: string | null;
    team?: string | number | null;
    duration?: string[] | null;
    pollType?: string | null;
    search?: string;
}