export interface Rule {
    ruleName: string;
    rewardsPerUser: string;
    rewardValue: string;
    startDateTime: string;
    endDateTime: string;
    maxLimit: number | null;
    duration: number | null;
    durationUnit: string;
    status: string;
    totalQuantityOfRewardsPerUser: number;
    id:string
}

export interface Note {
    title: string;
    description: string;
    addedDate: string;
    addedby: string;
}

export interface RewardCreationProps {
    id?:string;
    startDateTime: string;
    endDateTime: string;
    rewardDescription: string;
    category: string;
    activity: string;
    rules: Rule[];
    notes: Note[];

}
