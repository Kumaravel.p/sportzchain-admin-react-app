
export interface userListingPayload {
    start: number;
    length: number;
    searchText?: string;
    status?: string;
    location?: string;
    leaderboard?: string;
}

export interface tableListPayload {
    start: number;
    length: number;
    userId: number;
}

export interface teamTokenHoldingsIF {
    team: string;
    quantity: number;
    usd: number;
}

export interface followingTeamsIF {
    team: string;
    leaderboard: string;
    rank: string;
    favourite: string;
    followingDate: string;
}

export interface rewardsListIF {
    dateTime: string;
    category: string;
    activity: string;
    ruleName: string;
    reward: string;
    spn: string;
    status: string;
}

export interface activityPointListIF {
    dateTime: string;
    category: string;
    activity: string;
    ruleName: string;
    activityPoint:string
}

export interface transactionHistoryIF {
    transactionHash: string;
    dateTime: string;
    transactionType: string;
    from: string;
    fromQuantity: string;
    to: string;
    toQuantity: string;
    gasFees: string;
    status: string;
}

export interface UpdateStatusInterface{
    userId:number;
    userStatusId:number;
}