export interface TableListInterface {
    start: number;
    length: number;
    searchText: string;
}

export interface ListingBattleGroundPayload extends TableListInterface {
    status?: string;
    filterDateRange: {
        startAt: string,
        endAt: string,
    },
}

export interface UpsertBattleGroundPayload {
    title: string;
    id?: string;
    statusId: string;
    startAt: string;
    endAt: string;
    sportCategory: string;
    displayStartAt: string;
    displayEndAt: string;
    winningPrize?: string;
    displayPriority: number;
    matchId?: string;
    leagueId?: string;
    team1Color?: string;
    team2Color?: string;
    winningPrizeInSpn?: string;
    isInstantRedemption?: boolean;


}

export interface InsertMatchPayload {
    sportName?: string;
    Events?: any[];
    team1Logo?: string;
    team2Logo?: string;
}