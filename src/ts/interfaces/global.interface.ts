export interface AppConfig {
  apiBaseUrl: string;
  hasuraGraphQL: string;
  graphQL: string;
  hasuraSecretKey: string;
  userPoolId: string;
  clientId: string;
  countriesStateCitiesApiKey: string;
  region: string;
  identityPoolId: string;
  sportzchainFanPageURL: string;
}

export interface ApiResponse {
  statusCode: string;
  type: string;
  message: string;
  data?: any;
}
