export interface ListingStoProps {
    start: number,
    length: number,
    searchText: string,
    filterStatus: number[],
    filterDateRange: {
        startDate: string,
        endDate: string,
    },
    teamId: any
}

export interface StoSpnValueByTokenPayload {
    tokenSymbol: string;
}