export interface TableListInterface {
    start: number;
    length: number;
    searchText: string;
}

export interface ListingGroupsPayload extends TableListInterface {
    privacyId?: number;
}

export interface UpsertGroupPayload {
    groupName: string;
    description?: string;
    imageUrl: string;
    bannerUrl: string[];
    statusId: number;
    privacyId: number;
    permissionId: number;
    teamId?: number;
    moderators: string[];
    groupId?: string;
    startAt:string;
    endAt:string;
    displayStartAt:string;
    displayEndAt:string;
    notes: {
        addedDate:string;
        name:string;
        notes:string;
        addedBy:string;
    }[];
}


export interface GroupMemberList extends TableListInterface {
    groupId?: string;
}