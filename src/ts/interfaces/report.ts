export interface GenerateReportsInterface {
    startDate: string;
    endDate: string;
    contestId?: number
    pollId?: number
}

export enum ReportsTypeEnum {
    "CONTEST_REPORT" = "contest",
    "POLL_REPORT" = "poll",
    "REWARD_REPORT" = "reward",
    "USER_REPORT" = "user",
}

export type ReportsType = keyof typeof ReportsTypeEnum