export interface LeaderboardLevelPayload {
    id?: number;
    leaderboardId: number;
    minActivityPoint: string;
    maxActivityPoint: string;
    effectiveFrom: string;
    effectiveTill: string;
}
