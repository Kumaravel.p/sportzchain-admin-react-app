
export interface NewsFilterProps {
    start?: number;
    length?: number;
    teamId?: string | number | null;
    filterDateRange?: {};
    searchText?: string;
}

export interface News {
    id?: number;
    postId?: number;
    status: string;
    canMultiselect: boolean;
    minJoinBalance: number | string;
    teamId: number | string;
    title: string;
    endAt: any;
    startAt: any;
    displayStartAt: any;
    displayEndAt: any;
    pollPlatform: string;
    currency: string;
    allowedParticipants: number | string;
    teamPollContentType: string;
    question: string;
    description: string;
    type: "PLATFORM" | "GROUPS";
    groupId: string;
}