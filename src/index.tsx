import React from 'react';
import ReactDOM from 'react-dom';
import {
  HttpLink,
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  ApolloLink,
  concat,
} from '@apollo/client';
import { postGraphileQueries } from 'utils/constants';

import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';

import reportWebVitals from './reportWebVitals';
import config from 'config';
import { Amplify } from 'aws-amplify';
import currentSession from 'utils/getAuthToken';

// const authMiddleware = new ApolloLink((operation, forward) => {
//   // add the authorization to the headers
//   operation.setContext(async ({ headers = {} }) => {
//     let authorization = `Bearer eyJraWQiOiJKWms4TXZXNEc0U05zV3dWTFwvQ0FobWY0VnoybFQzRUd4QmhHNFZUbjNvTT0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJjM2IzNGIxMC1jMGIxLTRjZDgtOWUxZC01Y2YyYzExYTUxZGUiLCJhZGRyZXNzIjoiMHgzMDczYzJiYTMwMTA0ZGNhMjdmNDE2NDgyODBkMDU1OWQxNTA4MjE4Iiwicm9sZSI6ImFkbWluIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImh0dHBzOlwvXC9oYXN1cmEuaW9cL2p3dFwvY2xhaW1zIjoie1wieC1oYXN1cmEtdXNlci1pZFwiOlwiYzNiMzRiMTAtYzBiMS00Y2Q4LTllMWQtNWNmMmMxMWE1MWRlXCIsXCJ4LWhhc3VyYS1kZWZhdWx0LXJvbGVcIjpcImFkbWluXCIsXCJ4LWhhc3VyYS1hbGxvd2VkLXJvbGVzXCI6W1wiYWRtaW5cIl19IiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1lYXN0LTFfeEV0QlI0UTFXIiwiY29nbml0bzp1c2VybmFtZSI6ImFkbWluQHNwb3J0emNoYWluLmNvbSIsInByZWZlcnJlZF91c2VybmFtZSI6ImFkbWluIiwib3JpZ2luX2p0aSI6IjIxZWIxN2QzLWFlNDItNDFkNC1iOTc1LTg2OGM2NWExNDU4OSIsImF1ZCI6IjNlNDFiYjdhdWhraGNtYWEyczVlcjhwZ2NuIiwiZXZlbnRfaWQiOiIzOTllODU4YS1hYzY2LTRhMGQtODNlYy0zN2RmZWU1Yzc0OTYiLCJ1c2VyX2lkIjoiMSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNjU0NzQ5MzU2LCJuYW1lIjoiYWRtaW4iLCJleHAiOjE2NTQ3NTI5NTYsImlhdCI6MTY1NDc0OTM1NiwianRpIjoiNDg2MjE0ZjgtMzkzNi00MmYzLWFlYzMtNDE0MDQwNTM1NjhkIiwiZW1haWwiOiJhZG1pbkBzcG9ydHpjaGFpbi5jb20ifQ.ZAp1ZXwLMKtH401GZa3pTSO0I6KmWIaSnKfhZvnunPCtuVLvxuRWcCrUpm3FWZ6DVn86WTKhfIFlb7sTT_1PdY9WwmOr4mjtg-AdyCt3fgtGZ-V0ZA2Z00rSAC76i5c4B-byLZh7-xflDytQePBvsFHSmLQSZhSINi4H0y-kT-rBHZBVGTukM7svJiUuFTGpKqkX3bb9cMz45CjODuU9ZJrWz35D6UrBGc15A9Xm2_QP7ZTtOfq5RUjDv8XK1cr6deDZS7hiz8cpEJOXAfKGR5eOtiN5WR3oMtOhHjFllXjeZadnu52ooXa1X50umv7cOj9Li6O8aLU1tdu8pYYfEw`;
//     return {
//       headers: {
//         ...headers,
//         authorization,
//         //'eyJraWQiOiJ6ZVNWcVlLMVFUc3JCT2xIbXo5QlNDd2VRNDhyVFdNWlwvTFM4WUk1TmhCOD0iLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiTkM3WXI1R3hFRW5Ub054b1VNSTZuQSIsInN1YiI6IjQwNDcwYzE5LTVlM2EtNGFmMS1iNTY1LThjN2QxZDE3ZjY1OCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJodHRwczpcL1wvaGFzdXJhLmlvXC9qd3RcL2NsYWltcyI6IntcIngtaGFzdXJhLXVzZXItaWRcIjpcIjQwNDcwYzE5LTVlM2EtNGFmMS1iNTY1LThjN2QxZDE3ZjY1OFwiLFwieC1oYXN1cmEtZGVmYXVsdC1yb2xlXCI6XCJ1c2VyXCIsXCJ4LWhhc3VyYS1hbGxvd2VkLXJvbGVzXCI6W1widXNlclwiXX0iLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtZWFzdC0xLmFtYXpvbmF3cy5jb21cL3VzLWVhc3QtMV9RaUtOVGtLaHIiLCJjb2duaXRvOnVzZXJuYW1lIjoiNDA0NzBjMTktNWUzYS00YWYxLWI1NjUtOGM3ZDFkMTdmNjU4IiwicHJlZmVycmVkX3VzZXJuYW1lIjoibW9jYW1hMTYzOSIsImF1ZCI6IjNuN2U2ZXU0czdiZDgybnZyZnZudWszNTYyIiwiZXZlbnRfaWQiOiI0MjNjYTE0Yy05NDFmLTQyMjUtYWNlYi00ZTEwN2JkZmM3ODMiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTY0NTI3ODM2MCwibmFtZSI6Ik1vY2FtYSIsImV4cCI6MTY0NTM2NDc2MCwiaWF0IjoxNjQ1Mjc4MzYwLCJqdGkiOiIyNDhiYzdjMi0wNDE1LTRlYTAtYmViNC1hNjQzZDhmODljMzEiLCJlbWFpbCI6Im1vY2FtYTE2MzlAcmVpbW9uZG8uY29tIn0.Yb68TM6b8rUkSnnBpswNUQrDNygiPBIBmkQWAK9hAIc0mRpq1HaFa-HasnPWcr2kvf2LNKwQRNWUdeXD71YtBFgLr-DgCXR4UfWMDs6wjVEUHlPtEiCCmA47x2lFJDYAPwXh9o-qlDa1FVsjV9hsa9kMAX96frxAUmZ9nwZNhwtthpwtcTw1jp3RN0RgdPdZZBTySPLhqo7LvbT0SAXgxRB5-cLeAl3eZqNgDg5Shc-kKJAKFf7O-JqFAON42SxSCJx89M5CfQ5XdLqYWQEJQCz5avuvzeDmbLKy8a3sPSMwP4o0JoZDxT0y0MgSmbfFZ5qksSZYfjom-FArZYrhhg',
//         'x-hasura-admin-secret': config.hasuraSecretKey,
//       },
//     };
//   });

//   return forward(operation);
// });

Amplify.configure({
  Auth: {
    region: config.region,
    userPoolId: config.userPoolId,
    identityPoolId: config.identityPoolId,
    userPoolWebClientId: config.clientId,
  },
});

const customFetch = async (uri: string, options: any) => {
  options.headers.Authorization = await (await currentSession()).getJwtToken();
  options.headers["x-hasura-admin-secret"] = config.hasuraSecretKey;
  return fetch(
    `${
      postGraphileQueries.some((query) => {
        return query === JSON.parse(options.body).operationName;
      })
        ? config.graphQL
        : config.hasuraGraphQL
    }`,
    options
  );
};

// const httpLink = new HttpLink({ uri: config.hasuraGraphQL });
const httpLink = new HttpLink({ fetch: customFetch });

const cache = new InMemoryCache({
  typePolicies: {},
});

const client = new ApolloClient({
  cache,
  link: httpLink,
  credentials: 'allow',
});

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <ApolloProvider client={client}>
        <App />
      </ApolloProvider>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
