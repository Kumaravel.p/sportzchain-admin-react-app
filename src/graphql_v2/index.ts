import { gql } from 'graphql-request';

export const GetAllTeamLeaguesQuery = gql`
query GetAllTeamLeagues {
    allTeamLeagues {
      nodes {
        nodeId
        leagueName
      }
    }
  }
  
`;

interface DateRange {
  startDate?: Date;
  endDate?: Date;
}

export const GetAllSTO = (offset: number = 0, limit: number = 10, searchText: string = "", filterStatus: string[] = [], filterDateRange: DateRange = {}) => gql`
query GetAllSTO(offset: number = 0, limit: number = 10, searchText: string = "", filterStatus: string[] = [], filterDateRange: DateRange = {}) {
  allStos(
    offset: ${offset}
    first: ${limit}
    filter: {
      teamByTeamId: {
        name: {includesInsensitive: "${searchText}"}
      }
      ${filterStatus?.length ? `
      masterStoStatusByStoStatusId:{
        id:{
          in:[${filterStatus}]
        }
      }
      ` : ''}
     ${(filterDateRange?.startDate && filterDateRange?.endDate) ? `
      or: {
        startDate: { greaterThanOrEqualTo: "${filterDateRange?.startDate}" }
        endDate: { lessThanOrEqualTo: "${filterDateRange?.endDate}" }
      }
     ` : ''}
    }
  ){
    nodes {
      id
      endDate
      startDate
      status:masterStoStatusByStoStatusId {
        type
      }
      teamByTeamId {
        name
        id
      }
    }
    totalCount
  }
}  
`;

export const GetSTOStatusQuery = gql`
  query GetSTOStatusQuery {
    allMasterStoStatuses {
      nodes {
        type
        id
      }
    }
  }
`;

export const GetSTOTeamList = gql`
query MyQuery {
  allWallets(filter: {teamByTeamId: {id: {isNull: false}}}) {
    nodes {
      coinSymbol
      teamByTeamId {
        name
        id
      }
      balance
    }
  }
}`;

export const GetNoOfRounds = (id: any) => gql`query MyQuery {
  allStos(filter: {teamId: {equalTo: ${id}}}) {
    nodes {
      teamId
    }
    totalCount
  }
}
`;

export const GetNews = (id: any) => gql
`query MyQuery4{
  allPosts(condition:{id:${id}}) {
    nodes {
      description
      coinSymbol
      createdAt
      createdBy
      displayEndAt
      displayPriority
      displayStartAt
      endAt
      id
      groupId
      groupByGroupId{
        id
        groupName
      }
      postNotesByPostId(orderBy: CREATED_DATE_ASC) {
        nodes {
          addedBy
          createdDate
          id
          name
          notes
        }
      }
      type
      minJoinBalance
      numOfShares
      postUrl
      startAt
      statusId
      title
      updatedAt
      viewsCount
      teamByTeamId {
        name
        id
        profileImageUrl
      }
    }
  }
}
`;

export const GetImageList = (id: string) => gql
`query MyQuery{
  allContents(condition: {id: "${id}"}) {
    totalCount
    nodes {
      name
      masterContentTypeByTypeId {
        name
        description
      }
      typeId
      contentItemsByContentId {
        totalCount
        nodes {
          id
          fileName
          contentId
          isMobileView
        }
      }
    }
  }
}
`;

export const GetTeamImageList = (id:string,teamId:number) => gql
`query MyQuery {
  allContents(condition: {id: "${id}"}) {
    totalCount
    nodes {
      name
      masterContentTypeByTypeId {
        name
        description
      }
      typeId
      contentItemsByContentId(filter: {teamId: {equalTo: ${teamId}}}) {
        totalCount
        nodes {
          id
          fileName
          contentId
          teamId
          isMobileView
        }
      }
    }
  }
}






`;

export const GetPollsCount =gql`
query GetPollsCount {
  allPosts {
    totalCount
  }
}
`