import type { AppConfig } from 'ts/interfaces/global.interface';

const config: AppConfig = {
  apiBaseUrl: process?.env?.REACT_APP_REST_API_ENDPOINT ?? "",
  graphQL: process?.env?.REACT_APP_GRAPHQL_ENDPOINT ?? "",
  hasuraGraphQL: process?.env?.REACT_APP_HASURA_GRAPHQL_ENDPOINT ?? "",
  hasuraSecretKey: process?.env?.REACT_APP_HASURA_SECRET_KEY ?? "",
  userPoolId: process?.env?.REACT_APP_USER_POOL_ID ?? "",
  clientId: process?.env?.REACT_APP_CLIENT_ID ?? "",
  countriesStateCitiesApiKey: process?.env?.REACT_APP_COUNTRIES_STATES_CITIES_API_KEY ?? "",
  region: process?.env?.REACT_APP_AWS_REGION ?? "",
  identityPoolId: process?.env?.REACT_APP_IDENTITY_POOL_ID ?? "",
  sportzchainFanPageURL: process?.env?.REACT_APP_SPORTZCHAIN_FAN_PAGE ?? "",
};

export default (function () {
  return config;
})();
