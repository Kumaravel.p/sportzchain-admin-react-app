import { ActivityPointCreationProps, ListingActivityPointsProps } from 'ts/interfaces/activityPoints';
import axiosInstance from './axiosInstance';

export const activityPointsListing = (body: ListingActivityPointsProps) => {
  return axiosInstance.post('/activity/list', body);
};

export const activityPointCreation = (body: ActivityPointCreationProps) => {
  return axiosInstance.post('/activity/create', body);
};

export const activityPointUpdation = (body: ActivityPointCreationProps) => {
  return axiosInstance.post('/activity/update', body);
};