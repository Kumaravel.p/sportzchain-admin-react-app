import axiosInstance from './axiosInstance';

export const s3FileUpload = (file: File) => {
  const formData = new FormData();
  formData.append("file", file);
  return axiosInstance.post('/files/upload', formData, {
    headers: { 'Content-Type': 'multipart/form-data' },
  });
};