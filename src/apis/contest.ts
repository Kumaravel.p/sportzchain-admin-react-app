import axiosInstance from './axiosInstance';

export interface DisplayWinners {
  contestId: string;
  start: number;
  length: number;
  searchText: string;
  userId: number;
}

export const createContestApi = (body: any) => {
  return axiosInstance.post('/contest/create', body);
};

export const updateContestApi = (body: any) => {
  return axiosInstance.post('/contest/update', body);
};

export const getAllContests = (body: object) => {
  return axiosInstance.post('/contest/list', body);
};

export const updateOptionAnswer = (body: object) => {
  return axiosInstance.post('/contest/updateanswer', body);
};

export const getParticipantCount = (body: object) => {
  return axiosInstance.post('/contest/participatecount', body);
};

export const getContestWinner = (body: DisplayWinners) => {
  return axiosInstance.post('/contest/displaywinnerlist', body);
};

export const battlegroundList = () => {
  return axiosInstance.get('/battleground/list-battle-title');
}