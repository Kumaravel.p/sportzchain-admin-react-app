import { TeamCommunityPayload, TeamContactPayload } from "ts/interfaces/teams.interface";
import axiosInstance from "./axiosInstance";

export const teamInsertApi = (teamsState: any) => {
    return axiosInstance.post('teams/create', teamsState)
};

export const teamUpdateApi =(payload: any) => {
    return axiosInstance.post('teams/update', payload)
}

export const getCommunityByTeamID = (payload:TeamCommunityPayload) => {
    return axiosInstance.post('teams/listcommunity',payload)
};

export const upsertTeamContact = (payload: TeamContactPayload) => {
    return axiosInstance.post('teams/addmember', payload)
};

