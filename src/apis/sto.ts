import axiosInstance from './axiosInstance';
import type { ListingStoProps, StoSpnValueByTokenPayload } from 'ts/interfaces/sto.interface'

export const stoListing = (body: ListingStoProps) => {
  return axiosInstance.post('/sto/filter', body);
};

export const stoInsertApi = (teamsState: any, id: any) => {
  if (id) {
    return axiosInstance.post('/sto/update', teamsState);
  } else {
    return axiosInstance.post('/sto/create', teamsState);
  }
};

export const stoTokenData = (tokenAddress: any) => {
  return axiosInstance.post('/sto/totaltoken', { tokenAddress });
};

export const stoSpnValueByToken = (body:StoSpnValueByTokenPayload) => {
  return axiosInstance.post('/sto/spntokenvalue', body);
};