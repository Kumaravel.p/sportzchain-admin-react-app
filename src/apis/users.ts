
import { tableListPayload, UpdateStatusInterface, userListingPayload } from "ts/interfaces/users";
import axiosInstance from "./axiosInstance";

export const userListingApi = (body: userListingPayload) => {
  return axiosInstance.post(`/auth/listusers`, body);
};

export const followingTeamsListApi = (body: tableListPayload) => {
  return axiosInstance.post(`/user/listfollowingteams`, body);
};

export const rewardsListApi = (body: tableListPayload) => {
  return axiosInstance.post(`/user/listrewards`, body);
};

export const activityPointListApi = (body: tableListPayload) => {
  return axiosInstance.post(`/user/listactivitypoints`, body);
};

export const transactionHistoryListApi = (body: tableListPayload) => {
  return axiosInstance.post(`/user/listtransaction`, body);
};

export const getSpnValuesApi = (userId: number) => {
  return axiosInstance.post('/user/displayspnvalues ', { userId })
};

export const getUserCountsApi = (userId: number) => {
  return axiosInstance.post('/auth/listuserscount', { userId })
};

export const getTeamTokensApi = (body: tableListPayload) => {
  return axiosInstance.post('/user/listteamtokens', body)
};

export const updateUserStatus = (body: UpdateStatusInterface) => {
  return axiosInstance.post('/user/updatestatus', body)
};

