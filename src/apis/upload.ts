import axios from 'axios';

export const uploadFile = (presignedUrl: string, file: File) => {
  return axios.put(presignedUrl, file, {
    headers: { 'Content-Type': file.type },
  });
};
