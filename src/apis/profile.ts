import axiosInstance from './axiosInstance';

export const fetchUserMeta = (body:any) => {
    return axiosInstance.post('/auth/profile', body);
  }