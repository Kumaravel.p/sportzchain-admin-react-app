import { News } from 'ts/interfaces/news.interface';
import config from 'config';
import axios from 'axios';
import axiosInstance from 'apis/axiosInstance';
import { ApiResponse } from 'ts/interfaces/global.interface';

export interface PayloadProps extends News{}

export const updateNewsApi = (payload: PayloadProps) => {
     return axiosInstance.post('post/update', 
             JSON.stringify(payload),{
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'}
         });
};

