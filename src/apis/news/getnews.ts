import { NewsFilterProps } from 'ts/interfaces/news.interface';
import axiosInstance from 'apis/axiosInstance';


export const getNewsApi = (payload: NewsFilterProps) => {
     return axiosInstance.post('post/list', 
             JSON.stringify(payload),{
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'}
         });
};

