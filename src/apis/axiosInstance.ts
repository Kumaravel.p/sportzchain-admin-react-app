import axios from 'axios';

import config from 'config';
import currentSession from 'utils/getAuthToken';

const axiosInstance = () => {
  const defaultOptions = {
    baseURL: config.apiBaseUrl,
  };

  // Create instance
  let instance = axios.create(defaultOptions);

  // Set the AUTH token for any request
  instance.interceptors.request.use(async (config: any) => {
    const token = await (await currentSession()).getJwtToken();
    config.headers.Authorization = token ? `Bearer ${token}` : '';
    return config;
  });

  return instance;
};
export default axiosInstance();
