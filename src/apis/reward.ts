import axiosInstance from './axiosInstance';
import type { ListingStoProps } from 'ts/interfaces/sto.interface'
import { RewardCreationProps } from 'ts/interfaces/reward.interface';

export const rewardListing = (body: ListingStoProps) => {
    return axiosInstance.post('/reward/list', body);
};

export const rewardCreation = (body: RewardCreationProps) => {
    return axiosInstance.post('/reward/create', body);
};

export const rewardUpdation = (body: RewardCreationProps) => {
    return axiosInstance.post('/reward/update', body);
};