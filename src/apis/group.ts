import { GroupMemberList, ListingGroupsPayload, UpsertGroupPayload } from 'ts/interfaces/groups';
import axiosInstance from './axiosInstance';

export const groupListing = (body: ListingGroupsPayload) => {
  return axiosInstance.post('/groups/list', body);
};

export const groupUpsert = (body: UpsertGroupPayload) => {
  return axiosInstance.post(`/groups/${body.groupId ? "update" : "create"}`, body);
};

export const groupMemberListApi = (body: GroupMemberList) => {
  return axiosInstance.post(`/groups/listfansdetail`, body);
};