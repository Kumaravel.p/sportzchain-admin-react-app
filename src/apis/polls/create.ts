import type {Poll} from 'ts/interfaces/polls.inteface'
import config from 'config';
import axios from 'axios';
import axiosInstance from 'apis/axiosInstance';
import { ApiResponse } from 'ts/interfaces/global.interface';

export interface PayloadProps extends Poll{}

export const createPollApi = (payload: PayloadProps) => {
     return axiosInstance.post('teampoll/create', 
             JSON.stringify(payload),{
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'}
         });
};

