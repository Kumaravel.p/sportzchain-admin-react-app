import type {Poll} from 'ts/interfaces/polls.inteface'
import config from 'config';
import axios from 'axios';
import axiosInstance from 'apis/axiosInstance';

export interface PayloadProps extends Poll{}

export const updatePollApi = (payload: PayloadProps) => {
    return axiosInstance.post('teampoll/update', 
             JSON.stringify(payload),{
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'}
         });
};

