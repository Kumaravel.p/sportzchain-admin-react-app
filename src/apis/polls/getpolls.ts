import type {PollFilterProps} from 'ts/interfaces/polls.inteface'
import axiosInstance from 'apis/axiosInstance';


export const getPollsApi = (payload: PollFilterProps) => {
     return axiosInstance.post('teampoll/list', 
             JSON.stringify(payload),{
                headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'}
         });
};

