
import { ListingBattleGroundPayload, UpsertBattleGroundPayload, InsertMatchPayload } from 'ts/interfaces/battleground';
import axiosInstance from './axiosInstance';

export const battleListing = (body: ListingBattleGroundPayload) => {
    return axiosInstance.post('/battleground/list', body);
};

export const battleUpsert = (body: UpsertBattleGroundPayload) => {
    return axiosInstance.post(`/battleground/${body.id ? "update" : "create"}`, body);
};
export const matchDataInsert = (body: InsertMatchPayload) => {
    return axiosInstance.post(`/match/matchseed`, body)
}