import { GraphQLClient } from 'graphql-request';
import config from 'config';
import currentSession from 'utils/getAuthToken';

const graphqlReq = ({ document, variables }: any) => {
  return new Promise(async (resolve, reject) => {
    const endpoint = config.graphQL;
    const token = await (await currentSession()).getJwtToken();

    const graphQLClient = new GraphQLClient(endpoint, {
      headers: {
        Authorization: `Bearer ${token}`,
        'x-hasura-admin-secret': config.hasuraSecretKey
      },
    });

    const data = await graphQLClient.request(document, variables);
    resolve(data);
  });
};

export default graphqlReq;
