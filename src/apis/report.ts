
import { GenerateReportsInterface, ReportsTypeEnum, ReportsType } from 'ts/interfaces/report';
import axiosInstance from './axiosInstance';


export const generateReportApi = (body: GenerateReportsInterface, type: ReportsType) => {
      return axiosInstance.post(`/report/${ReportsTypeEnum[type]}`, body);
    // return axiosInstance.post(`https://tfptpmruaj.us-east-1.awsapprunner.com/api/v1/report/${ReportsTypeEnum[type]}`, body);
};