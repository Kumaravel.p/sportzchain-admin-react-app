// import axiosInstance from 'apis/axiosInstance';
import { TeamListPayload } from 'ts/interfaces/cms';
import axios from 'axios';
import axiosInstance from 'apis/axiosInstance';


export const teamBannerList = (body: TeamListPayload) => {
    return axiosInstance.post('content/listteamadbanner', body);
  };
