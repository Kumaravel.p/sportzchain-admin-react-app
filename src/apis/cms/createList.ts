import axiosInstance from 'apis/axiosInstance';
import { CmsCreatePayload, CmsListPayload } from 'ts/interfaces/cms';
// import axios from 'axios';

export const cmsCreate = (body: CmsCreatePayload) => {
  return axiosInstance.post('content/create', body);
};

export const cmsList = (body: CmsListPayload) => {
  return axiosInstance.post('content/list', body);
};