import { UpsertGroupCriteriaPayload } from "ts/interfaces/groupCriteria";
import axiosInstance from "./axiosInstance";

export const groupCriteriaUpsert = (body: UpsertGroupCriteriaPayload) => {
  return axiosInstance.post(`/groups/${body.id ? "updatecriteria" : "createcriteria"}`, body);
};