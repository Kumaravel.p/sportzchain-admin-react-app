import { LeaderboardLevelPayload } from 'ts/interfaces/leaderboardLevel';
import axiosInstance from './axiosInstance';

export const LeaderboardLevelCreation = (body: LeaderboardLevelPayload) => {
  return axiosInstance.post('/leaderboard/create', body);
};

export const LeaderboardLevelUpdation = (body: LeaderboardLevelPayload) => {
    return axiosInstance.post('/leaderboard/update', body);
};