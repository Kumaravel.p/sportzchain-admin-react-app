import React from 'react';
import { ReactComponent as SportzchainLogo } from 'assets/Sportzchain_logo_dark.svg';
import { Row, Col, Form, Input, Typography, Button } from 'antd';
import styled from 'styled-components/macro';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import { useParams, useHistory } from 'react-router-dom';
import SelectWithInput from 'components/common/selectWithInput';
import { countryCode } from 'utils/countryCodeList'
import { getMobileLimitBasedOnCC } from 'utils';
import { toast } from 'react-toastify';
import { Auth } from 'aws-amplify';

const Wrapper = styled.div`
height:100vh;
width:100%;
& .row-wrapper{
    height:100vh;
    & .background-column{
        background-color: #eee;
    }
    & .form-fields{
        padding: 40px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        & .sportzchain-logo{
            margin-bottom: 32px;
        }
        & .signup-options-wrapper {
            margin-bottom:40px;
            background: #262626;
            border-radius: 4px;
            padding: 4px;
            width: fit-content;
            & .t-signup-option {
            font-size: 16px;
            line-height: 24px;
            letter-spacing: -0.18px;
            padding: 4px 14px;
            color: #a3a3a3;
            cursor: pointer;
            &.active {
                color: #171717;
                background: #fb923c;
                border-radius: 4px;
            }
            }
        }
        & .ant-form-item-label > label{
            font-weight: 500;
        }
        & .ant-typography{
            text-align:center;
        }
        & .subtitle{
            margin-top:6px;
            margin-bottom:24px;
        }
        & .verify-btn{
            width: 100%;
            padding: 10px;
            background: #fb923c;
            height: 100%;
            &:hover,&:focus{
                color: #000;
                border-color: #fb923c;
            }
        }
    }
}
`;

const ForgotPassword = (): JSX.Element => {

    const [form] = Form.useForm();
    const [loading, setLoading] = React.useState<boolean>(false);
    const history = useHistory();

    const { mode } = useParams<{ mode: 'email' | 'mobile' }>();

    const onSave = () => {
        console.log(form.getFieldsValue(), 'save')
    }

    const sendVerificationCode = () => {
        form.submit()
        form.validateFields()
            .then(async () => {
                setLoading(true);
                try {
                    const { email, mobile } = form.getFieldsValue();
                    let value = mode === 'email' ? email : `${mobile.countryCode}${mobile.mobileNumber}`;
                    const data = await Auth.forgotPassword(value);
                    if (data) {
                        toast.success('Code sent successfully');
                    }
                    history.push({
                        pathname: '/reset-password',
                        state: {
                            // mode,
                            value
                        }
                    });
                }
                catch (error: any) {
                    console.log(error);
                    toast.error(error?.message || JSON.stringify(error));
                }
                finally {
                    setLoading(false);
                }
            })
            .catch(e => {
                console.log(e, 'e')
            })
    }

    const phoneNumebrValidation = (value: any) => {
        if (!(!!value?.countryCode)) {
            return Promise.reject(new Error('Country code is required!'));
        }
        else if (!(!!value?.mobileNumber)) {
            return Promise.reject(new Error('Mobile Number is required!'));
        }
        else if (value?.mobileNumber) {
            const limit = getMobileLimitBasedOnCC(value?.countryCode);
            if (value?.mobileNumber.length !== limit) {
                return Promise.reject(new Error(`Mobile Number should be in ${limit} digits`));
            }
            else {
                return Promise.resolve();
            }
        }
        return Promise.resolve();
    }

    return (
        <Wrapper>
            <Row className="row-wrapper">
                <Col xs={24} md={14} className="background-column"></Col>
                <Col xs={24} md={10} className="form-fields">
                    <div className="sportzchain-logo">
                        <SportzchainLogo />
                    </div>
                    <Typography.Title level={2}>Forgot Password</Typography.Title>
                    <Typography.Title level={5} className="subtitle">
                        Please enter your {mode === "email" ? "email address" : "mobile number"} used for verifying
                        sportzchain account.
                    </Typography.Title>
                    <Row>
                        <Col span={24} md={18}>
                            <FlexRowWrapper className="signup-options-wrapper">
                                <div
                                    onClick={() => history.push('/forgot-password/email')}
                                    className={`t-signup-option ${mode === 'email' && 'active'}`}
                                >
                                    Email
                                </div>
                                <div
                                    onClick={() => history.push('/forgot-password/mobile')}
                                    className={`t-signup-option ${mode === 'mobile' && 'active'}`}
                                >
                                    Mobile
                                </div>
                            </FlexRowWrapper>
                        </Col>
                    </Row>
                    <Form
                        onFinish={onSave}
                        form={form}
                        preserve={false}
                        layout="vertical"
                        validateTrigger="onSubmit"
                    >

                        <Row gutter={[24, 24]} justify="center">
                            <Col span={24} md={18}>
                                {
                                    mode === 'email' ? (
                                        <Form.Item
                                            name="email"
                                            label="Email Address"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Email is required!',
                                                },
                                                {
                                                    type: "email",
                                                    message: "Enter a valid email",
                                                }
                                            ]}
                                        >
                                            <Input
                                                placeholder='Enter Email'
                                                style={{ borderRadius: 5 }}
                                            />
                                        </Form.Item>
                                    ) : (
                                        <Form.Item
                                            name="mobile"
                                            label="Mobile Number"
                                            required={true}
                                            rules={[
                                                () => ({
                                                    validator(_, value) { return phoneNumebrValidation(value) },
                                                }),
                                            ]}
                                        >
                                            <SelectWithInput
                                                options={countryCode}
                                                optionLabel="dial_code"
                                                optionValue="dial_code"
                                                placeholder="Enter Mobile Number"
                                            />
                                        </Form.Item>
                                    )
                                }
                            </Col>
                            <Col span={24} md={18}>
                                <Button
                                    style={{ width: '100%' }}
                                    className="verify-btn"
                                    onClick={sendVerificationCode}
                                    loading={loading}
                                >
                                    Send verification code
                                </Button>
                            </Col>
                            <Col span={24} md={18}>
                                <Typography.Text>
                                    Already have an account ?
                                    <Typography.Link href="/signin/email">
                                        {" Sign in here"}
                                    </Typography.Link>
                                </Typography.Text>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </Wrapper>
    )
}

export default ForgotPassword;