import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Row, Col, Typography, Input, Card, Upload as AntUpload, Button, Form, Spin, Radio, Space, TablePaginationConfig, DatePicker } from 'antd';
import SelectElement from 'components/common/SelectElement';
import styled from 'styled-components/macro';
import CustomTable from 'components/customTable';
import LabelInput from 'components/common/LabelInput';
import BreadcrumbComp from 'components/common/breadcrumb';
import { toast } from 'react-toastify';
import { useHistory, useParams } from 'react-router-dom';
import TextEditorInput from 'components/common/TextEditorInput';
import { Upload } from 'components/common/Upload';
import { TagInput } from 'components/common/TagInput';
import { isValidEmail, parseJson } from 'utils/functions';
import { useGetGroupByIdQuery, useGetGroupMembersByGroupIdLazyQuery, useGetGroupPermissionQuery, useGetGroupPrivaciesQuery, useGetGroupStatusesQuery, useGetModeratorsByGroupIdLazyQuery } from 'generated/pgraphql';
import { useGetTeamsByStatusIdQuery } from 'generated/graphql';
import { groupMemberListApi, groupUpsert } from 'apis/group';
import config from 'config';
import { useDebounce } from 'hooks/useDebounce';
import { SearchOutlined } from '@ant-design/icons';
import { groupFilterStatus, ternaryOperator } from 'utils';
import moment, { MomentInput } from 'moment';
import { UpsertGroupPayload } from 'ts/interfaces/groups';
import { PlusOutlined } from '@ant-design/icons';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import currentSession from 'utils/getAuthToken';

const Wrapper = styled('div')`
width:100%;
padding:40px;
background-color: #F5F5F7;
& .card-wrap{
    margin-top: 16px;
    & .ant-card-bordered{
        border-radius: 10px;
        margin-bottom: 16px;
    }
}
& .header{
    color:#051F24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
}
& .label{
    font-weight: 400;
    font-size: 14px;
    color:#051F2499;
    margin-bottom: 16px;
    & span{
        color:red;
        margin-left: 8px;
    }
}
& .ant-select-selector,.datepicker{
    border:1px solid #d9d9d9 !important;
    border-radius: 2px !important;
}
& .datepicker{
    padding:4px 11px !important;
}
& .tabs{
    width:100%;
    margin-block:24px;
    display:flex;
    gap:12px;
    align-items:center;
    transition:0.5s;
    & .tab-item{
        flex:1;
        cursor:pointer;
        padding:8px;
        text-align: center;
        background: rgba(198, 198, 198, 0.6);
        border-radius: 4px;
        font-weight: 600;
        font-size: 16px;
        color:rgba(5, 31, 36, 0.6);
    }
    & .active{
        color:#4DA1FF;
        background: #FFFFFF;
    }
}
& .remove-btn{
    color:#1890ff;
}
& .table-list{
        width: 100%;
        margin-bottom: 16px;
        & .ant-space-item:first-of-type{
            flex:1;
            & .table-title{
                margin-bottom: 0;
                color:#000000;
                font-size: 16px;
                font-weight: 600;
            }
        }
}
& .search-input{
    border: 1px solid rgba(5, 31, 36, 0.1);
    border-radius: 5px;
    & .ant-input-prefix{
        color:#97a0c3;
    }
}
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

interface GroupProps { }

type GroupOptionsType = {
    id: number;
    name: string;
    description: string
};

interface TableDataType {
    avatar: {
        src: string;
        firstChar: string;
    };
    name: string;
    email: string;
    status?: string;
    joinedOn?: string;
}

const toLowerCase = (val: string) => val?.toLowerCase()?.trim();

let pageSize = 5;
let initialPagination: TablePaginationConfig = {
    current: 1,
    pageSize,
    total: 0,
}

let tableColumns = [
    { title: 'Avatar', dataIndex: "avatar", component: "avatar" },
    { title: 'Name', dataIndex: "name", sorter: (a: any, b: any) => toLowerCase(a?.name).localeCompare(toLowerCase(b?.name)) },
    { title: 'Email', dataIndex: "email", sorter: (a: any, b: any) => toLowerCase(a?.email).localeCompare(toLowerCase(b?.email)) },
];

const UpsertGroup = (props: GroupProps): JSX.Element => {
    //debounce hook
    const debounce = useDebounce();
    const [form] = Form.useForm();
    const params: { id?: string } = useParams();
    const history = useHistory();

    //states
    const [formData, setFormData] = React.useState<object>({});
    const [loading, setLoading] = useState<'save' | 'fan' | null>(null);
    const [activeTab, setActiveTab] = useState<number>(0);
    const [groupDesc, setGroupDesc] = useState<string>("");
    const [emailIds, setEmailIds] = useState<string[]>([]);
    const [dataSource, setDataSource] = useState<any[]>([]);
    const [searchText, setSearchText] = useState<string>("");
    const [groupMemberList, setGroupMemberList] = useState<{
        data: any[];
        totalCount: number
    }>({
        data: [],
        totalCount: 0
    });
    const [pagination, setPagination] = useState<TablePaginationConfig>(initialPagination);
    const [removeModerators, setRemoveModerators] = useState<string[]>([]);
    const [removeFans, setRemoveFans] = useState<string[]>([]);
    const [uploads, setUploads] = useState<{
        groupImage?: any[],
        groupBannerImage?: any[],
    }>({
        groupImage: [],
        groupBannerImage: [],
    });
    const [isVisible, setIsVisible] = React.useState<boolean>(false);
    const [notesList, setNotesList] = React.useState<any[]>([]);
    const [editIndex, setEditIndex] = React.useState<number>(-1);
    const [checkStatus, setCheckStatus] = React.useState<boolean>(false);
    const [userData, setUserData] = React.useState<any>({});

    //column for moderator and fan table
    const moderatorFanColumn = [
        ...tableColumns,
        ...(activeTab === 0 ? [{ title: 'Invite Status', dataIndex: "status" }] : []),
        {
            title: 'Joined On',
            dataIndex: "joinedOn",
            valueFormat: (value: Date) => moment(value).format('DD-MM-yyyy, hh:mm a'),
        },
        { title: 'Actions', dataIndex: "actions", component: 'icons', align: 'center' },
    ];

    //get groupBy id
    const { data: groupById, loading: groupByIdLoading } = useGetGroupByIdQuery({
        ...(params?.id && { variables: { id: params?.id }, }),
        skip: !(!!params.id),
        fetchPolicy: 'network-only',
    });

    //group privacy options
    const { data: groupPrivacyOptions, loading: groupPrivacyOptionLoading } = useGetGroupPrivaciesQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    //group permission options
    const { data: groupPermissionOptions, loading: groupPermissionOptionLoading } = useGetGroupPermissionQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    //group status options
    const { data: groupStatusOptions, loading: groupStatusOptionLoading } = useGetGroupStatusesQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    //team listing
    const { data: teamOptions, loading: teamOptionLoading } = useGetTeamsByStatusIdQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        variables: {
            statusId: ["APPROVED", "PUBLISHED", "ACTIVE"]
        }
    });

    //moderator listing query
    const [getModeratorsList, {
        data: moderatorsList,
        loading: moderatorsListLoading,
        // refetch: refetchModeratorsList,
    }] = useGetModeratorsByGroupIdLazyQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        nextFetchPolicy: "standby",
        ...(params?.id && {
            variables: {
                id: params?.id,
            }
        }),
    })

    const changeTab = (tabNumber: number) => {
        setActiveTab(tabNumber)
    }

    const onBeforeSave = () => {
        form.submit()
        form.validateFields()
            .then(async (res) => {
                // check status if status is in ['SUBMITTED', 'APPROVED', 'DISCONTINUED', 'SUSPENDED'] then  add notes before save 
                if (params?.id && [5, 8, 9, 6].includes(form.getFieldValue('groupStatus'))) {
                    setIsVisible(true);
                    setCheckStatus(true)
                }
                else {
                    if (checkStatus) {
                        setCheckStatus(false)
                    }
                    onSave(res);
                }
            })
            .catch(e => {
                console.log(e)
                toast.info('Please fill the mandatory fields!')
            })
    }

    //on save or update
    const onSave = (res: any) => {
        setLoading('save');

        //construct notes payload
        let contructNotes: any[] = [];

        if (notesList?.length) {
            contructNotes = notesList?.map(note => ({
                "addedDate": note?.addedDate,
                "name": note?.name,
                "notes": note?.notes,
                "addedBy": note?.addedby
            }))
        }

        let payload: UpsertGroupPayload = {
            ...(params?.id && { "groupId": params?.id, }),
            "groupName": res?.groupName,
            "description": groupDesc,
            "imageUrl": res?.groupImage?.[0]?.fileId,
            "bannerUrl": res?.groupBannerImage?.map((_: any) => _?.fileId),
            "statusId": res?.groupStatus,
            "privacyId": res?.groupPrivacy,
            "permissionId": Number(res?.groupPermission),
            "moderators": dataSource,
            "teamId": res?.team ?? null,
            "startAt": res?.startAt?.seconds('0o0').toISOString(),
            "endAt": res?.endAt?.seconds('0o0').toISOString(),
            "displayStartAt": res?.displayStartAt?.seconds('0o0').toISOString(),
            "displayEndAt": res?.displayEndAt?.seconds('0o0').toISOString(),
            ...(params?.id && { removeModerators }),
            notes: contructNotes
        }

        groupUpsert(payload)
            .then(data => {
                if (data?.data?.statusCode === "500") {
                    catchError(data?.data?.message)
                }
                setLoading(null);
                toast.success(data?.data?.message);
                history.push(`${params?.id ? `/view-group/${params.id}` : '/groups'}`)
            })
            .catch(e => catchError(e?.message))
    }

    const catchError = (msg: string) => {
        console.log(msg);
        setLoading(null);
        toast.error(msg ? msg : 'Something went wrong!');
        return false
    }

    const onValuesChange = (field: any) => {
        if (['groupImage', 'groupBannerImage']?.some(_ => field.hasOwnProperty(_))) {
            let key = Object.keys(field)[0];
            setUploads({
                ...uploads,
                [key]: field?.[key]
            })
        }
        setFormData({ ...form.getFieldsValue(true) })
    }

    const beforeUpload = (file: any) => {
        const allowedTypes =
            file.type === 'image/png' || file.type === 'image/jpeg';
        if (!allowedTypes) {
            toast.error(`${file.name} is not a png or jpeg file`);
        }
        return allowedTypes || AntUpload.LIST_IGNORE;
    };

    const onPressEnter = (val: string) => {
        return isValidEmail(val)
    }

    const onChangeEmailInput = (val: string) => {
        setEmailIds([...emailIds, val])
    }

    const OneDelteEmailId = (val: string) => {
        let filteredEmailId = emailIds?.filter(_ => _ !== val);
        setEmailIds(filteredEmailId)
    }

    const OnSubmitInput = (list: string[] = []) => {
        setEmailIds([])
        setDataSource([...dataSource, ...list])
    }

    const onClickIcons = (e: any, rowRecord: any, rowIndex: number) => {
        e.stopPropagation();
        const filteredData = dataSource?.filter((_, index) => index !== rowIndex);
        setDataSource(filteredData);
    }

    const onChangeSearch = (val: string = "") => {
        setSearchText(val);
        debounce(() => getRetchList(val, 1), 800);
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => {
        const { current = 1, pageSize } = newPagination;
        let offset = current;
        if (pageSize !== newPagination?.pageSize) {
            offset = 1;
        }
        getRetchList(searchText, offset, pageSize)
    }

    useEffect(() => {
        if (params?.id) {
            setPagination(initialPagination)
            setSearchText("")
            getRetchList()
        }
    }, [activeTab, params?.id])


    const setPaginationData = (total: number = 0, current: number = 1, pageSize: number = 5) => {
        setPagination({
            ...pagination,
            total,
            current,
            pageSize
        })
    }

    //table api call
    const getRetchList = (searchText: string = "", current: number = 1, pageSize: number = 5,) => {

        if (activeTab === 0) {
            getModeratorsList({
                variables: {
                    offset: ((current * pageSize) - pageSize),
                    first: pageSize,
                    id: params.id,
                    searchText
                }
            })
                .then((res: any) => {
                    setPaginationData(
                        res?.data?.groupById?.groupModeratorsByGroupId?.totalCount,
                        current,
                        pageSize
                    )
                })
                .catch((e: any) => {
                    toast.error(e?.message)
                })
        }
        else {
            setLoading('fan')
            groupMemberListApi({
                start: ((current * pageSize) - pageSize),
                length: pageSize,
                groupId: params.id,
                searchText
            })
                .then((res: any) => {
                    if (res?.data?.statusCode === "500") {
                        catchError(res?.data?.message)
                    }
                    let data = res?.data?.data
                    if (data?.resultData) {
                        let setFanList = data?.resultData?.map((_: any) => ({
                            avatar: {
                                src: `${config.apiBaseUrl}files/${_?.userByUserId?.imageUrl}`,
                                firstChar: ternaryOperator(_?.userByUserId?.name, "")?.charAt(0),
                            },
                            name: ternaryOperator(_?.userByUserId?.name, "-"),
                            email: ternaryOperator(_?.userByUserId?.email, '-'),
                            joinedOn: _?.updatedAt ? moment(_?.updatedAt + 'z').toISOString() : '-',
                            id: _?.id,
                            // actions: (rowRecord: any, rowIndex: number) => (
                            //     <Typography.Text className='remove-btn' onClick={(e) => onClickRemove(e, rowRecord, rowIndex, "fan")} >Remove</Typography.Text>
                            // )
                        }))
                        setGroupMemberList({
                            data: setFanList,
                            totalCount: data?.totalCount
                        })
                    }
                    setPaginationData(
                        res?.data?.totalCount,
                        current,
                        pageSize
                    )
                    setLoading(null)
                })
                .catch((e: any) => {
                    catchError(e?.message || e?.response?.message || JSON.stringify(e))
                })
        }
    }

    //table title
    const getTableName = useMemo(() => {
        const getCount = (count: number = 0, length: number = 0) => count ? count - length : 0;
        if (activeTab === 0) {
            return `Moderators (${getCount(moderatorsList?.groupById?.groupModeratorsByGroupId.totalCount, removeModerators?.length)})`
        }
        return `Fans (${getCount(groupMemberList?.totalCount, removeFans?.length)})`
    }, [activeTab, moderatorsList, groupMemberList, removeModerators, removeFans]);


    const contructFileList = (fileList: string[]) => {
        if (!fileList?.length) return [];
        return fileList?.map(_ => ({
            fileId: _,
            uid: _,
            url: `${config.apiBaseUrl}files/${_}`
        }))
    }

    //table data for invite table
    const returnInviteTable: TableDataType[] = useMemo(() => {
        if (!dataSource?.length) return [];
        return dataSource?.map(_ => ({
            avatar: {
                src: '',
                firstChar: _?.charAt(0),
            },
            name: '-',
            email: _,
            actions: (rowRecord: any, rowIndex: number) => (
                <Typography.Text className='remove-btn' onClick={(e) => onClickIcons(e, rowRecord, rowIndex)} >Remove</Typography.Text>
            )
        }))
    }, [dataSource]);

    // Moderator table list
    const returnModeratorsList = useMemo(() => {
        if (moderatorsListLoading) return [];
        let list = moderatorsList?.groupById?.groupModeratorsByGroupId.nodes;
        if (removeModerators?.length) {
            list = list?.filter(_ => !removeModerators.includes(_?.id));
        }
        if (!list?.length) return []
        return list?.map(_ => ({
            avatar: {
                src: `${config.apiBaseUrl}files/${_?.userByUserId?.imageUrl}`,
                firstChar: ternaryOperator(_?.userByUserId?.name, "")?.charAt(0),
            },
            name: ternaryOperator(_?.userByUserId?.name, "-"),
            email: ternaryOperator(_?.emailId),
            status: _?.status,
            id: _?.id,
            joinedOn: moment(_?.invitedAt + 'z').toISOString(),
            actions: (rowRecord: any, rowIndex: number) => (
                <Typography.Text className='remove-btn' onClick={(e) => onClickRemove(e, rowRecord, rowIndex, "moderator")} >Remove</Typography.Text>
            )
        }))
    }, [moderatorsList, moderatorsListLoading, removeModerators])


    const onClickRemove = (e: any, rowRecord: any, rowIndex: number, type: "moderator" | "fan") => {
        e.stopPropagation();
        if (type === "moderator") {
            const filteredList = returnModeratorsList?.find(_ => _?.id === rowRecord?.id)?.id;
            setRemoveModerators([...removeModerators, filteredList])
        }
    }

    const returnDataSource = useMemo(() => {
        return activeTab === 0 ? returnModeratorsList : groupMemberList?.data
    }, [activeTab, returnModeratorsList, groupMemberList]);

    //disable start date
    const isDisableStartDate = useCallback((date) => {
        return date && moment(date).isBefore(moment(), 'day')
    }, [form])

    //disable end date
    const isDisableEndDate = useCallback((date, key) => {
        if (!form.getFieldValue(key)) {
            return true
        }
        let checkDate: MomentInput = moment(form.getFieldValue(key)).isBefore(moment(), 'day') ? moment() : moment(form.getFieldValue(key))
        return date && moment(date).isBefore(checkDate, 'day')
    }, [form])

    //status validation for Start Date Time
    const startDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`Start Date Time is required!`));
        }
        if (getFieldValue('endAt')) {
            if (!value.isBefore(getFieldValue('endAt'))) {
                return Promise.reject(new Error(`Start Date Time should be lesser than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        if (getFieldValue('displayStartAt')) {
            if (!value.isAfter(getFieldValue('displayStartAt'))) {
                return Promise.reject(new Error(`Start Date Time should be greater than Display Start Date Time(${getFieldValue('displayStartAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for End Date Time
    const endDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`End Date Time is required!`));
        }
        if (getFieldValue('startAt')) {
            if (!value.isAfter(getFieldValue('startAt'))) {
                return Promise.reject(new Error(`End Date Time should be greater than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        if (getFieldValue('displayEndAt')) {
            if (!value.isBefore(getFieldValue('displayEndAt'))) {
                return Promise.reject(new Error(`End Date Time should be lesser than Display End Date Time(${getFieldValue('displayEndAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for Display Start Date Time
    const displayStartDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`Display Start Date Time is required!`));
        }
        if (getFieldValue('startAt')) {
            if (!value.isBefore(getFieldValue('startAt'))) {
                return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for Display End Date Time
    const displayEndDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`Display End Date Time is required!`));
        }
        if (getFieldValue('endAt')) {
            if (!value.isAfter(getFieldValue('endAt'))) {
                return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    const returnGroupById = useMemo(() => {
        if (groupByIdLoading) return {}
        let group = groupById?.allGroups?.nodes?.[0];
        return {
            groupName: group?.groupName,
            team: group?.teamId,
            groupPrivacy: group?.masterGroupPrivacyByPrivacyId?.id,
            groupStatus: group?.masterGroupStatusByStatusId?.id,
            groupImage: contructFileList(group?.imageUrl ? [group?.imageUrl] : []),
            groupBannerImage: contructFileList(parseJson(group?.bannerUrl)),
            groupPermission: group?.masterGroupPermissionByPermissionId?.id,
            startAt: moment(group?.startAt + "z"),
            endAt: moment(group?.endAt + "z"),
            displayStartAt: moment(group?.displayStartAt + "z"),
            displayEndAt: moment(group?.displayEndAt + "z"),
        }
    }, [groupById, groupByIdLoading]);

    //group status validation
    const statusValidations = useMemo(() => {
        return groupFilterStatus(groupStatusOptions?.allMasterGroupStatuses?.nodes, returnGroupById?.groupStatus)
    }, [groupStatusOptions, returnGroupById])

    //find disable components
    const disabledComponents = useCallback((key: string) => {
        if (!(!!params?.id)) return false
        let validate = statusValidations?.disableComponents?.includes(key)
        return statusValidations?.except ? validate : !validate
    }, [statusValidations])

    //on delete Notes
    const deleteNotes = () => {
        let filteredNotes = notesList?.filter((list, index) => index !== editIndex)
        setNotesList(filteredNotes)
        handleCancelNote()
    }

    //on cancel notes
    const handleCancelNote = () => {
        setIsVisible(false)
        if (editIndex > -1) {
            setEditIndex(-1)
        }
    }

    //on upserting notes
    const onUpsertNotes = (val: any, editdata: any) => {

        if (Object.keys(editdata).length > 0 && editIndex > -1) {
            notesList[editIndex] = {
                ...notesList[editIndex],
                name: val?.name,
                notes: val?.note,
            }
            setNotesList(notesList)
        }
        else {
            setNotesList([
                ...notesList, {
                    name: val?.name,
                    notes: val?.note,
                    addedDate: moment(),
                    addedby: userData?.name,
                }
            ])
        }
        handleCancelNote()
    }

    //on open individual note
    const openEditNote = (obj: any, key: number) => {
        setEditIndex(key - 1);
        setIsVisible(true)
    }

    // note data
    const editNoteData = React.useMemo(() => {
        if (editIndex > -1) {
            return {
                name: notesList[editIndex]?.name,
                note: notesList[editIndex]?.notes,
                id: notesList[editIndex]?.id,
            }
        }
        return {}
    }, [editIndex])

    useEffect(() => {
        if (checkStatus) {
            onSave(form.getFieldsValue())
        }
    }, [notesList])

    useEffect(() => {
        currentSession().then(res => {
            setUserData(res.payload)
        })
    }, [])


    useEffect(() => {
        let group = groupById?.allGroups?.nodes?.[0];
        if (!groupByIdLoading && group?.groupNotesByGroupId?.nodes?.length) {
            let contructNotesList = group?.groupNotesByGroupId?.nodes?.map((note: any) => ({
                name: note?.name,
                notes: note?.notes,
                addedDate: moment(note?.createdDate.concat('z')),
                addedby: note?.addedBy,
                id: note?.id,
            }))
            setNotesList(contructNotesList)
        }
    }, [groupById, groupByIdLoading])


    useEffect(() => {
        if (!groupByIdLoading) {
            let group = groupById?.allGroups?.nodes?.[0];
            setGroupDesc(group?.description ?? "");
            setUploads({
                groupBannerImage: contructFileList(parseJson(group?.bannerUrl)),
                groupImage: contructFileList(group?.imageUrl ? [group?.imageUrl] : [])
            })
        }
    }, [groupById, groupByIdLoading]);


    return (
        <Wrapper style={{ width: '100%' }}>
            <BreadcrumbComp
                backPath='/groups'
                cta={<>
                    <Button
                        type="primary"
                        style={{ marginRight: "16px" }}
                        onClick={() => onBeforeSave()}
                        disabled={loading === "save"}
                    >
                        {params?.id ? 'Update' : 'Add'} Group
                    </Button>
                    <Button href={`${params?.id ? `/view-group/${params.id}` : '/groups'}`}>Discard</Button>
                </>}
                breadCrumbs={[
                    { name: "Groups", url: "/groups" },
                    { name: `${params?.id ? 'Update' : 'Add New'} Group`, url: params.id ? `/edit-group/${params.id}` : "/create-group" },
                ]}
            />
            {(loading === "save" || groupByIdLoading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <div className='card-wrap'>
                        <Form
                            // onFinish={onSave}
                            form={form}
                            preserve={false}
                            initialValues={
                                params?.id ? returnGroupById : {
                                    groupStatus: 1,
                                    groupPrivacy: 2,
                                    groupPermission: 2,
                                }
                            }
                            onValuesChange={onValuesChange}
                        >
                            <Card>
                                <Typography className='header'>Basic Info</Typography>
                                <Row gutter={[24, 24]}>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Group Name"
                                            name="groupName"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Group Name is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Input
                                                    placeholder="Type Title"
                                                    disabled={disabledComponents('groupName')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Affiliated to"
                                            name="team"
                                            className="label-input"
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                    options={teamOptions?.team ?? []}
                                                    placeholder="Select Team Name"
                                                    searchable={true}
                                                    toFilter="name"
                                                    toStore='id'
                                                    loading={teamOptionLoading}
                                                    disabled={disabledComponents('team')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Group Privacy"
                                            name="groupPrivacy"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Group Privacy is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                    placeholder="Group Privacy"
                                                    searchable={true}
                                                    options={groupPrivacyOptions?.allMasterGroupPrivacies?.nodes ? groupPrivacyOptions?.allMasterGroupPrivacies?.nodes?.filter((_: any) => _?.id === 2) as unknown as GroupOptionsType[] : []}
                                                    toFilter="description"
                                                    toStore='id'
                                                    loading={groupPrivacyOptionLoading}
                                                    disabled={disabledComponents('groupPrivacy')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Start Date Time"
                                            name={'startAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return startDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabledDate={(value) => isDisableStartDate(value)}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                    disabled={disabledComponents('startAt')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="End Date Time"
                                            name={'endAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return endDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime={Boolean(form.getFieldValue('startAt'))}
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabledDate={(value) => isDisableEndDate(value, 'startAt')}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                    disabled={disabledComponents('endAt')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Display Start Date Time"
                                            name={'displayStartAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return displayStartDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabledDate={(value) => isDisableStartDate(value)}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                    disabled={disabledComponents('displayStartAt')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Display End Date Time"
                                            name={'displayEndAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return displayEndDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime={Boolean(form.getFieldValue('displayStartAt'))}
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabledDate={(value) => isDisableEndDate(value, 'displayStartAt')}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                    disabled={disabledComponents('displayEndAt')}
                                                />
                                            }
                                        />
                                    </Col>
                                    {
                                        params?.id &&
                                        <Col xs={24} sm={12} md={8}>
                                            <LabelInput
                                                label="Group Status"
                                                name="groupStatus"
                                                className="label-input"
                                                required
                                                rules={[
                                                    {
                                                        required: true,
                                                        message: 'Group Status is required!',
                                                    },
                                                ]}
                                                inputElement={
                                                    <SelectElement
                                                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                        options={groupStatusOptionLoading ? [] : statusValidations.options ?? []}
                                                        placeholder="Group Status"
                                                        searchable={true}
                                                        toFilter="description"
                                                        toStore='id'
                                                        loading={groupStatusOptionLoading}
                                                        disabled={disabledComponents('status')}
                                                    />
                                                }
                                            />
                                        </Col>
                                    }
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Group Description"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Group Description is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <TextEditorInput
                                                    editData={groupById?.allGroups?.nodes?.[0]?.description ?? ""}
                                                    value={groupDesc}
                                                    setValue={(val: any) => setGroupDesc(val)}
                                                    view={disabledComponents('groupDescription')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Group Image"
                                            name="groupImage"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Group Image is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Upload
                                                    fileList={uploads?.groupImage}
                                                    maxCount={1}
                                                    accept="image/png, image/jpeg"
                                                    beforeUpload={beforeUpload}
                                                    disabled={disabledComponents('groupImage')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Group Banner Image"
                                            name="groupBannerImage"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Group Banner Image is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Upload
                                                    fileList={uploads?.groupBannerImage}
                                                    accept="image/png, image/jpeg"
                                                    beforeUpload={beforeUpload}
                                                    disabled={disabledComponents('groupBannerImage')}
                                                // maxCount={2}
                                                />
                                            }
                                        />
                                    </Col>
                                </Row>
                            </Card>
                            <Card>
                                <Typography className='header'>Group Permissions</Typography>
                                <LabelInput
                                    name="groupPermission"
                                    className="label-input"
                                    required
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Group Permission is required!',
                                        },
                                    ]}
                                    inputElement={
                                        <Radio.Group disabled={groupPermissionOptionLoading || disabledComponents('groupPermission')}>
                                            {
                                                groupPermissionOptions?.allMasterGroupPermissions?.nodes?.map(_ => (
                                                    <Radio disabled={_?.id === 1} value={_?.id}>{_?.description}</Radio>
                                                ))
                                            }
                                        </Radio.Group>
                                    }
                                />
                            </Card>
                        </Form>
                        <Card>
                            <Typography className='header'>Invite</Typography>
                            <TagInput
                                list={emailIds}
                                onPressEnter={onPressEnter}
                                onChange={onChangeEmailInput}
                                onDelete={OneDelteEmailId}
                                onSubmit={OnSubmitInput}
                                errorMessage="Please enter a valid email"
                            />
                            <CustomTable
                                columns={[
                                    ...tableColumns,
                                    { title: 'Actions', dataIndex: "actions", component: 'icons', align: 'center' },
                                ]}
                                dataSource={returnInviteTable ?? []}
                                hidePagination={true}
                                onChangeTable={() => { }}
                            />
                        </Card>
                        {
                            params?.id &&
                            <>
                                <div className="tabs">
                                    <Typography.Text className={`tab-item ${activeTab === 0 ? "active" : ""}`} onClick={() => changeTab(0)}>Moderators</Typography.Text>
                                    <Typography.Text className={`tab-item ${activeTab === 1 ? "active" : ""}`} onClick={() => changeTab(1)}>Fans</Typography.Text>
                                </div>
                                <Card>
                                    <Space align="center" size={24} wrap className='table-list'>
                                        <Typography className='table-title'>{getTableName}</Typography>
                                        <Input
                                            type="text"
                                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeSearch(e.target.value)}
                                            value={searchText}
                                            autoComplete={"off"}
                                            placeholder="Search by Name or Email Id"
                                            className="search-input"
                                            prefix={<SearchOutlined />}
                                        />
                                    </Space>
                                    <CustomTable
                                        columns={moderatorFanColumn}
                                        dataSource={returnDataSource ?? []}
                                        loading={loading === "fan" || moderatorsListLoading}
                                        pagination={pagination}
                                        onChangeTable={onChangeTable}
                                    />
                                </Card>
                            </>
                        }
                        <Card>
                            <Row style={{ margin: 0 }} gutter={[24, 24]}>
                                <Typography className="header" style={{ flex: 1 }}>Notes</Typography>
                                <Button type="link" icon={<PlusOutlined />} onClick={() => setIsVisible(true)}>
                                    Add New Note
                                </Button>
                            </Row>
                            <NoteModal
                                handleDelete={deleteNotes}
                                EditData={editNoteData}
                                ModalVisible={isVisible}
                                handleOk={(val: any, editdata: any) => onUpsertNotes(val, editdata)}
                                handleCancel={handleCancelNote}
                                readOnly={editNoteData?.id}
                            />
                            <Note
                                JsonData={notesList}
                                openEdit={(obj: any, key: number) => openEditNote(obj, key)}
                            />
                        </Card>
                    </div>

                </>)}
        </Wrapper>
    )
}

export default UpsertGroup;