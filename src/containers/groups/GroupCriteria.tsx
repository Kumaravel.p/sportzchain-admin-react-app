import { Button, Modal, Space, Typography, Spin, FormInstance } from 'antd';
import React, { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components/macro';
import { StatusCard } from './statusCard';
import { ReactComponent as EyeIcon } from 'assets/icons/EyePlus.svg';
import CustomTable from 'components/customTable';
import moment, { MomentInput } from 'moment';
import { CheckIcon } from 'components/svg';
import { toast } from 'react-toastify';
import { LeaderboardCards } from 'containers/leaderboardLevels/components';
import { useDeleteGroupCriterionByIdMutation, useGetAllGroupCriteriaQuery } from 'generated/pgraphql';
import { UpsertGroupCriteriaPayload } from 'ts/interfaces/groupCriteria';
import { groupCriteriaUpsert } from 'apis/groupCriteria';

interface ModalProps {
    widthSize: number;
}

const Wrapper = styled.div`
    width:100%;
    padding:40px;
    background-color: #F5F5F7;
    .groupCriteria{
        .title-head{
            margin-top: 32px;
            margin-bottom: 24px;
        }
        .group-criteria-card{
            margin-bottom: 16px;
            .status-icon{
                cursor: pointer;
                & div:first-of-type{
                    display:flex;
                }
            }
        }
    }
   
`;

const ModalWrapper = styled(Modal) <ModalProps>`
    width: ${props => props.widthSize} !important;
    @media (max-width: 600px) {
        width:95%;
    }
    .table-container{
        border: 1px solid #E6E6E6;
        width: 100%;
        border-radius: 4px;
        padding: 1px;
        margin-bottom: 24px;
        .status{
            .ant-space-item:first-of-type{
                display:flex;
            }
        }
    }
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

interface GroupCriteriaProps { };

interface TableDataType {
    sno?: number;
    usersCount: number;
    pollsCount: number;
    contestCount: number;
    pollParticipatePercentage: number;
    contestParticipatePercentage: number;
    effectiveFrom: string;
    effectiveTill: string;
    status: any;
    id?: number;
    criteriaId?: number;
}

const tableInitialState: TableDataType = {
    // sno: 1,
    usersCount: 0,
    pollsCount: 0,
    contestCount: 0,
    pollParticipatePercentage: 0,
    contestParticipatePercentage: 0,
    effectiveFrom: moment().toISOString(),
    effectiveTill: moment().toISOString(),
    status: "Upcoming"
}

const labels = [
    "No. of Users",
    "No. of Polls",
    "No. of Contests",
    "Poll Participation Rate (%)",
    "Contest Participation Rate (%)",
    "Status",
];

type groupCriteriaTypeEnum = 'Gold' | 'Silver' | 'Bronze'

enum groupCriteriaId {
    "Gold" = 7,
    "Silver" = 8,
    "Bronze" = 9,
}

const appendTimeZone = (date: string) => date ? date.includes('Z') ? date : date.concat('Z') : "";
const formatDate = (value: Date | string) => moment(value).format('DD-MM-yyyy, hh:mm a');
const fixedNumber = (value: number, digit: number = 2) => isNaN(value) ? 0 : Number(Number(value).toFixed(digit));

const GroupCriteria = (props: GroupCriteriaProps): JSX.Element => {

    const modelWidth: any = React.useRef();

    const [openModal, setOpenModal] = useState<'status' | 'table' | null>(null);
    const [loading, setLoading] = useState<string | null>(null);
    const [tableDataSource, setTableDataSource] = useState<TableDataType[] | []>([]);
    const [statusData, setStatusData] = useState<any>({});
    const [editIndex, setEditIndex] = useState<number>(-1);
    const [groupCriteriaType, setGroupCriteriaType] = useState<groupCriteriaTypeEnum>();

    const {
        data: groupCriteriaData,
        loading: groupCriteriaLoading,
        refetch: refetchGroupCriteriaData
    } = useGetAllGroupCriteriaQuery({
        notifyOnNetworkStatusChange: true,
        fetchPolicy: "no-cache",
    });

    const [
        deleteGroupCriteriaById,
        { loading: deletedGroupCriteriaByIdLoading }
    ] = useDeleteGroupCriterionByIdMutation({
        notifyOnNetworkStatusChange: true,
    });

    const getGroupCriteria: any = useMemo(() => {
        let data = groupCriteriaData?.allGroupCriteria?.nodes;
        if (!data?.length) return [];
        return data.reduce((total: any, item: any) => ({
            ...total,
            [item?.leaderboardByCriteriaId?.masterLeaderboardByTypeId?.name]: [...total?.[item?.leaderboardByCriteriaId?.masterLeaderboardByTypeId?.name] ?? [], item]
        }), {})
    }, [groupCriteriaData]);

    React.useEffect(() => {
        if (groupCriteriaType) {
            setTableDataSource(getGroupCriteria?.[groupCriteriaType])
        }
    }, [groupCriteriaData])

    //construct Group Criteria card's data
    const constructGroupCriteriaData = (labels: string[] = [], values: any[] = []) => {
        return values?.map((value, index) => ({
            label: labels?.[index] ? labels?.[index] : "",
            value: value
        }))
    }

    const renderStatus = useCallback((val: 'Completed' | 'Upcoming' | 'Active' | null) => {
        let color = "#10B981";
        if (val === "Upcoming") {
            color = "#0075FF"
        }
        else if (val === "Completed") {
            color = "#A3A3A3"
        }
        return (
            <Space className='status' align="center" size={8}>
                {val && <CheckIcon color={color} />}
                <Typography.Text style={{ color }}>{val ?? '-'}</Typography.Text>
            </Space>
        )
    }, [tableDataSource])

    const openStatusModal = (value: any = {}) => {
        let data = {
            ...value,
            effectiveFrom: moment(value?.effectiveFrom + 'Z'),
            effectiveTill: moment(value?.effectiveTill + 'Z'),
        }
        modelWidth.current = { width: "500px" }
        setOpenModal("status")
        setStatusData(data)
    }

    const openTableModal = (val: any[], type: groupCriteriaTypeEnum) => {
        modelWidth.current = { width: "95%" }
        setOpenModal('table')
        setTableDataSource(val)
        setGroupCriteriaType(type);
    }


    //construct Group Criteria card
    const contructValues = (value: any = {}, data: any[], type: groupCriteriaTypeEnum) => {
        return constructGroupCriteriaData(labels, [
            fixedNumber(value?.usersCount),
            fixedNumber(value?.pollsCount),
            fixedNumber(value?.contestCount),
            `${fixedNumber(value?.pollParticipatePercentage)}%`,
            `${fixedNumber(value?.contestParticipatePercentage)}%`,
            <div className='status-icon' onClick={() => openStatusModal(value)}>
                {renderStatus("Active")}
            </div>,
            <EyeIcon
                style={{ cursor: "pointer" }}
                onClick={() => openTableModal(data, type)}
            />
        ])
    }


    const tableColumns = useMemo(() => [
        {
            title: 'S.No',
            dataIndex: "sno",
            width: 100
        },
        {
            title: 'No. of Users',
            dataIndex: "usersCount",
            component: 'EDITABLE',
            width: 120
        },
        {
            title: 'No. of Polls',
            dataIndex: "pollsCount",
            component: 'EDITABLE',
            width: 120
        },
        {
            title: 'No. of Contests',
            dataIndex: "contestCount",
            component: 'EDITABLE',
            width: 120
        },
        {
            title: 'Poll Participation Rate (%)',
            dataIndex: "pollParticipatePercentage",
            component: 'EDITABLE',
        },
        {
            title: 'Contest Participation Rate (%)',
            dataIndex: "contestParticipatePercentage",
            component: 'EDITABLE',
        },
        {
            title: 'Effective from',
            dataIndex: "effectiveFrom",
            component: 'EDITABLE',
            component_name: "date-picker",
            valueFormat: (value: Date) => formatDate(value),
            disabledDate: (value: MomentInput) => value && moment(value).isBefore(moment(), 'day')
        },
        {
            title: 'Effective till',
            dataIndex: "effectiveTill",
            component: 'EDITABLE',
            component_name: "date-picker",
            valueFormat: (value: Date) => formatDate(value),
            disabledDate: (value: MomentInput, rowRecord: any, newRecord: any) => {
                let val = newRecord?.effectiveFrom ? newRecord?.effectiveFrom : rowRecord?.effectiveFrom;
                let checkDate = moment(val).isBefore(moment(), 'day') ? moment() : moment(val)
                return value && moment(value).isBefore(checkDate, 'day')
            }
        },
        {
            title: 'Status',
            dataIndex: "status",
            valueFormat: (value: 'Completed' | 'Upcoming' | 'Active') => {
                return renderStatus(value)
            }
        },
        { title: 'Action', dataIndex: '', component: 'INSERT', },
    ], [tableDataSource])

    const returnEditComponents = (rowRecord: any) => {
        const checkStatus = (val: string) => (editIndex === 0) ? true : val === "Upcoming"

        return {
            usersCount: checkStatus(rowRecord?.status),
            pollsCount: checkStatus(rowRecord?.status),
            contestCount: checkStatus(rowRecord?.status),
            pollParticipatePercentage: checkStatus(rowRecord?.status),
            contestParticipatePercentage: checkStatus(rowRecord?.status),
            effectiveFrom: checkStatus(rowRecord?.status),
            effectiveTill: true,
        }
    }

    const isEditIcon = (rowRecord: any) => {
        return ['Active', 'Upcoming'].includes(rowRecord?.status);
    }

    const isDeleteIcon = (rowRecord: any) => {
        return rowRecord?.status === "Upcoming"
    }

    const onAddRow = () => {
        let data = [...tableDataSource, tableInitialState];
        setTableDataSource(data)
        setEditIndex(data.length - 1);
    }

    const onCancelRow = (index: number) => {
        if (editIndex > -1) {
            setEditIndex(-1);
            let filteredData = tableDataSource?.filter((_, i) => i !== index);
            setTableDataSource(filteredData)
        }
    }

    const returnTableData = useMemo(() => {
        if (!tableDataSource?.length) return []
        return tableDataSource?.map((value: any, index) => ({
            sno: index + 1,
            usersCount: fixedNumber(value?.usersCount),
            pollsCount: fixedNumber(value?.pollsCount),
            contestCount: fixedNumber(value?.contestCount),
            pollParticipatePercentage: fixedNumber(value?.pollParticipatePercentage),
            contestParticipatePercentage: fixedNumber(value?.contestParticipatePercentage),
            status: value?.masterLeaderboardStatusByStatusId?.description ?? "Upcoming",
            effectiveFrom: appendTimeZone(value?.effectiveFrom) ?? moment().toISOString(),
            effectiveTill: appendTimeZone(value?.effectiveTill) ?? moment().toISOString(),
            id: value?.id,
            criteriaId: value?.criteriaId,
            index
        }))
    }, [tableDataSource])

    const onSubmitModal = (formValues: FormInstance) => {
        let values = formValues.getFieldsValue();
        let payload = {
            "id": statusData?.id,
            "criteriaId": statusData?.criteriaId,
            "usersCount": statusData?.usersCount,
            "pollsCount": statusData?.pollsCount,
            "contestCount": statusData?.contestCount,
            "pollParticipatePercentage": statusData?.pollParticipatePercentage?.toString(),
            "contestParticipatePercentage": statusData?.contestParticipatePercentage?.toString(),
            "effectiveFrom": statusData?.effectiveFrom?.toISOString(),
            "effectiveTill": values?.effectiveTill?.toISOString()
        }
        groupCriteriaUpsertion(payload, () => onCancelModal(), "status")
    }

    const onCancelModal = () => {
        setOpenModal(null);
        setEditIndex(-1);
    }

    const catchError = (msg: string) => {
        console.log(msg);
        setLoading(null);
        toast.error(msg ? msg : 'Something went wrong!')
        return false
    }


    const onRowSave = (newRecord: TableDataType, rowIndex: number, type: "edit" | "delete") => {

        let data = { ...tableDataSource?.[rowIndex], ...newRecord };

        let criteriaId = data?.criteriaId ? data?.criteriaId : groupCriteriaType ? groupCriteriaId?.[groupCriteriaType] : null;
        if (type === "edit") {
            if (criteriaId) {
                let payload = {
                    ...(data?.id && { "id": data?.id }),
                    "criteriaId": criteriaId,
                    "usersCount": data?.usersCount,
                    "pollsCount": data?.pollsCount,
                    "contestCount": data?.contestCount,
                    "pollParticipatePercentage": data?.pollParticipatePercentage?.toString(),
                    "contestParticipatePercentage": data?.contestParticipatePercentage?.toString(),
                    "effectiveFrom": data?.effectiveFrom,
                    "effectiveTill": data?.effectiveTill
                }
                groupCriteriaUpsertion(payload, () => setEditIndex(-1))
            }
            else {
                toast.error('criteriaId is missing!')
            }
        }
        else if (type === "delete") {
            if (data?.id) {
                deleteGroupCriteriaById({
                    variables: {
                        id: data?.id
                    }
                })
                    .then((res: any) => {
                        if (res?.data?.deleteGroupCriterionById?.groupCriterion?.id) {
                            refetchGroupCriteriaData()
                            toast.success('Delete Successfully!')
                        }
                        else {
                            toast.error('Something went wrong!')
                        }
                    })
                    .catch(e => catchError(e))
            }
            else {
                toast.error('id is missing!')
            }
        }

    }

    const groupCriteriaUpsertion = (payload: UpsertGroupCriteriaPayload, callback: Function = () => null, loading = "table") => {
        setLoading(loading)
        groupCriteriaUpsert(payload)
            .then((res: any) => {
                if (res.data.statusCode === "500") {
                    callback && callback();
                    catchError(res.data.message);
                }
                setLoading(null)
                toast.success(res.data.message);
                callback && callback();
                refetchGroupCriteriaData()
            })
            .catch((e) => catchError(e?.response?.data?.message))
    }

    const returnModalBody = () => {
        if (openModal === "status") {
            return (
                <StatusCard
                    onCancel={onCancelModal}
                    onSave={onSubmitModal}
                    data={statusData ?? {}}
                    loading={loading === "status" || groupCriteriaLoading}
                />
            )
        }
        else if (openModal === "table") {
            return (
                <div>
                    <div className="table-container">
                        <CustomTable
                            dataSource={returnTableData}
                            columns={tableColumns}
                            onRowSave={onRowSave}
                            loading={(loading === "table" || groupCriteriaLoading || deletedGroupCriteriaByIdLoading)}
                            editIndex={editIndex > -1 ? editIndex : null}
                            onCancel={(index: number) => onCancelRow(index)}
                            returnEditComponents={returnEditComponents}
                            isEditIcon={isEditIcon}
                            isDeleteIcon={isDeleteIcon}
                            editValue={"index"}
                            scrollY={300}
                            columnWidth={200}
                        />
                    </div>
                    <Button
                        type='primary'
                        disabled={editIndex > -1}
                        onClick={onAddRow}
                    >
                        Add Row
                    </Button>
                </div>
            )
        }
    }

    //render Group Criteria only if its active
    const returnActiveGroupCriteria = (data = []): any[] => {
        return data?.filter((_: any) => _?.statusId === 2) // 2 is Active
    }

    const mapActiveGroupCriteria = useCallback((data = [], key: string) => {

        let returnActiveData = returnActiveGroupCriteria(data);

        if (returnActiveData?.length) {
            return returnActiveData?.map((values: any, index: number) => {
                return (
                    <div className='group-criteria-card' key={`group-criteria-card-${index}`}>
                        <LeaderboardCards
                            label={key}
                            jsonData={contructValues(values, data, key as groupCriteriaTypeEnum)}
                        />
                    </div>
                )
            })
        }
        else {
            return (
                <div className='group-criteria-card'>
                    <LeaderboardCards
                        label={key}
                        jsonData={constructGroupCriteriaData(labels, [
                            '-', '-', '-', '-', '-', '-',
                            <EyeIcon
                                style={{ cursor: "pointer" }}
                                onClick={() => openTableModal(data, key as groupCriteriaTypeEnum)}
                            />
                        ])}
                    />
                </div>
            )
        }
    }, [getGroupCriteria]);

    const isVisible = useMemo(() => openModal ? ['status', 'table']?.includes(openModal) : false, [openModal]);

    const modalTitle = useMemo(() => {
        if (openModal === "table") {
            return `Group Criteria > ${groupCriteriaType}`
        }
        else if (openModal === "status") {
            return `Status`
        }
        return null
    }, [openModal]);

    return (
        <Wrapper>

            <Typography.Title level={4}>Group Criteria</Typography.Title>
            {
                groupCriteriaLoading ? (
                    <SpinWrapper>
                        <Spin />
                    </SpinWrapper>
                ) : (
                    <div className="groupCriteria">
                        {['Gold', 'Silver', 'Bronze']?.map((gc: string) => mapActiveGroupCriteria(getGroupCriteria?.[gc], gc))}
                    </div>
                )
            }
            <ModalWrapper
                widthSize={modelWidth?.current?.width}
                title={modalTitle}
                centered
                visible={isVisible}
                onCancel={() => onCancelModal()}
                destroyOnClose={true}
                footer={null}
            >
                {returnModalBody()}
            </ModalWrapper>
        </Wrapper>
    )
}

export default GroupCriteria