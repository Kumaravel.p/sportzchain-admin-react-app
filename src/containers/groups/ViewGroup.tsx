import React, { useMemo, useState } from 'react';
import { Avatar, Button, Card, Col, Input, Radio, Row, Space, Spin, TablePaginationConfig, Typography } from 'antd';
import BreadcrumbComp from 'components/common/breadcrumb';
import LabelValue from 'components/common/LabelValue';
import { useParams } from 'react-router-dom';
import styled from 'styled-components/macro';
import CustomTable from 'components/customTable';
import { useGetGroupByIdQuery, useGetGroupMembersByGroupIdQuery, useGetGroupPermissionQuery, useGetModeratorsByGroupIdQuery } from 'generated/pgraphql';
import { ternaryOperator } from 'utils';
import { parseJson } from 'utils/functions';
import config from 'config';
import { useDebounce } from 'hooks/useDebounce';
import { SearchOutlined } from '@ant-design/icons';
import moment from 'moment';
import { toast } from 'react-toastify';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";

interface ViewGroupProps { }

const Wrapper = styled('div')`
    width:100%;
    padding:40px;
    background-color: #F5F5F7;
    .ant-card{
        margin-top: 24px;
    }
    & .table-title{
        color:#000000;
        font-size: 16px;
        font-weight: 600;
        margin-bottom:16px;
    }
    .row-label-value{
        .ant-col > div{
            gap:16px;
        }
    }
    & .tabs{
        width:100%;
        margin-bottom:16px;
        display:flex;
        gap:12px;
        margin-top:24px;
        align-items:center;
        transition:0.5s;
        & .tab-item{
            flex:1;
            cursor:pointer;
            padding:8px;
            text-align: center;
            background: rgba(198, 198, 198, 0.6);
            border-radius: 4px;
            font-weight: 600;
            font-size: 16px;
            color:rgba(5, 31, 36, 0.6);
        }
        & .active{
            color:#4DA1FF;
            background: #FFFFFF;
        }
    }   
    & .radio-readonly{
        pointer-events: none;
    }
    & .card-chips{  
        & .ant-card-bordered{
            border-radius:8px;
            & .ant-card-body{
                display:flex;
                flex-direction:column;
                gap:8px;
                & .ant-typography{
                    &:first-of-type{
                        color:#A3A3A3;
                        font-weight: 500;
                        font-size: 16px;
                    }
                    &:last-of-type{
                        color:#4DA1FF;
                        line-height:1;
                        font-weight: 700;
                        font-size: 24px;
                    }
                }
            }
        }
    }
    & .image-col{
        display:flex;
        flex-direction:column;
        gap:16px;
        & .ant-typography{
            &:first-of-type{
                font-size: 15px;
                line-height: 18px;
                color: rgba(5,31,36,0.6);
                margin-bottom: 6px;
            }     
        }
    }
    & .search-input{
        border: 1px solid rgba(5, 31, 36, 0.1);
        border-radius: 5px;
        & .ant-input-prefix{
            color:#97a0c3;
        }
    }
    & .table-list{
        width: 100%;
        margin-bottom: 16px;
        & .ant-space-item:first-of-type{
            flex:1;
            & .table-title{
                margin-bottom: 0;
            }
        }
    }
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;
const toLowerCase = (val: string) => val?.toLowerCase()?.trim();

let pageSize = 5;
let initialPagination: TablePaginationConfig = {
    current: 1,
    pageSize,
    total: 0,
}

const formatDate = (date: string) => date ? moment(date + 'z').format('MMMM D YYYY, HH:mm') : '-'

const ViewGroup = (props: ViewGroupProps) => {

    //debounce hook
    const debounce = useDebounce();
    const params: { id?: string } = useParams();
    const [activeTab, setActiveTab] = useState<number>(0);
    const [pagination, setPagination] = useState<TablePaginationConfig>(initialPagination);
    const [searchText, setSearchText] = useState<string>("");
    const [isVisible, setIsVisible] = React.useState<boolean>(false);
    const [editIndex, setEditIndex] = React.useState<number>(-1);

    //get groupBy id
    const { data, loading } = useGetGroupByIdQuery({
        ...(params?.id && { variables: { id: params?.id }, }),
        skip: !(!!params.id),
        fetchPolicy: 'network-only',
    });

    const group = useMemo(() => data?.allGroups?.nodes?.[0], [data]);

    //moderator listing query
    const {
        data: moderatorsList,
        loading: moderatorsListLoading,
        refetch: refetchModeratorsList,
    } = useGetModeratorsByGroupIdQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        variables: {
            id: params.id,
        },
        skip: (!(!!params.id) || activeTab === 1),
    })

    //group member listing query
    const {
        data: groupMembersList,
        loading: groupMembersListLoading,
        refetch: refetchGroupMemberList,
    } = useGetGroupMembersByGroupIdQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        variables: {
            id: params.id,
        },
        skip: (!(!!params.id) || activeTab === 0),
    })

    //group permission options
    const { data: groupPermissionOptions, loading: groupPermissionOptionLoading } = useGetGroupPermissionQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const changeTab = (tabNumber: number) => {
        setActiveTab(tabNumber)
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => {
        const { current = 1, pageSize } = newPagination;
        let offset = current;
        if (pageSize !== newPagination?.pageSize) {
            offset = 1;
        }
        getRetchList(searchText, offset, pageSize)
    }

    const onChangeSearch = (val: string = "") => {
        setSearchText(val);
        debounce(() => getRetchList(val, 1), 800);
    }

    React.useEffect(() => {
        setPagination(initialPagination)
        setSearchText("")
        getRetchList()
    }, [activeTab])

    //table api call
    const getRetchList = (searchText: string = "", current: number = 1, pageSize: number = 5,) => {
        let api = activeTab === 0 ? refetchModeratorsList : refetchGroupMemberList;
        let key = activeTab === 0 ? 'groupModeratorsByGroupId' : "groupMembersByGroupId";
        api({
            offset: ((current * pageSize) - pageSize),
            first: pageSize,
            id: params.id,
            searchText
        })
            .then((res: any) => {
                setPagination({
                    ...pagination,
                    total: res?.data?.groupById?.[key]?.totalCount,
                    current,
                    pageSize
                })
            })
            .catch((e) => {
                toast.error(e?.message)
            })
    }

    //table title
    const getTableName = useMemo(() => {
        if (activeTab === 0) {
            return `Moderators (${moderatorsList?.groupById?.groupModeratorsByGroupId.totalCount ?? 0})`
        }
        return `Fans (${groupMembersList?.groupById?.groupMembersByGroupId.totalCount ?? 0})`
    }, [activeTab, moderatorsList, groupMembersList]);

    //table column
    const tableColumns = useMemo(() => [
        { title: 'Avatar', dataIndex: "avatar", component: "avatar" },
        { title: 'Name', dataIndex: "name", sorter: (a: any, b: any) => toLowerCase(a?.name).localeCompare(toLowerCase(b?.name)) },
        { title: 'Email', dataIndex: "email", sorter: (a: any, b: any) => toLowerCase(a?.email).localeCompare(toLowerCase(b?.email)) },
        ...(activeTab === 0 ? [{ title: 'Invite Status', dataIndex: "status" }] : []),
        {
            title: 'Joined On',
            dataIndex: "joinedOn",
            valueFormat: (value: Date) => moment(value).format('DD-MM-yyyy, hh:mm a'),
        },
    ], [activeTab])

    // Moderator table list
    const returnModeratorsList = useMemo(() => {
        if (moderatorsListLoading) return [];
        let list = moderatorsList?.groupById?.groupModeratorsByGroupId.nodes;
        if (!list?.length) return []
        return list?.map(_ => ({
            avatar: {
                src: `${config.apiBaseUrl}files/${_?.userByUserId?.imageUrl}`,
                firstChar: ternaryOperator(_?.userByUserId?.name, "")?.charAt(0),
            },
            name: ternaryOperator(_?.userByUserId?.name, "-"),
            email: ternaryOperator(_?.emailId),
            status: _?.status,
            joinedOn: moment(_?.invitedAt + 'z').toISOString(),
        }))
    }, [moderatorsList, moderatorsListLoading])

    // Fan table list
    const returnGroupMemberList = useMemo(() => {
        if (groupMembersListLoading) return [];
        let list = groupMembersList?.groupById?.groupMembersByGroupId.nodes;
        if (!list?.length) return []
        return list?.map(_ => ({
            avatar: {
                src: `${config.apiBaseUrl}files/${_?.userByUserId?.imageUrl}`,
                firstChar: ternaryOperator(_?.userByUserId?.name, "")?.charAt(0),
            },
            name: ternaryOperator(_?.userByUserId?.name, "-"),
            email: ternaryOperator(_?.userByUserId?.email),
            joinedOn: moment(_?.updatedAt + 'z').toISOString(),
        }))
    }, [groupMembersList, groupMembersListLoading]);

    const handleNoteModal = (bool: boolean = false, index: number = -1) => {
        setIsVisible(bool)
        setEditIndex(!bool ? -1 : index)
    }

    const returnDataSource = useMemo(() => {
        return activeTab === 0 ? returnModeratorsList : returnGroupMemberList
    }, [activeTab, returnModeratorsList, returnGroupMemberList])    

    const contructNoteJson = useMemo(() => {
        let val = group?.groupNotesByGroupId?.nodes
        if (!val?.length) return []
        return val?.map((note: any) => ({
            name: note?.name,
            notes: note?.notes,
            addedDate: moment(note?.createdDate.concat('z')),
            addedby: note?.addedBy,
            id: note?.id,
        }))
    }, [group])

    const viewNoteData = useMemo(() => {
        if (editIndex > -1) {
            return {
                name: contructNoteJson[editIndex]?.name,
                note: contructNoteJson[editIndex]?.notes
            }
        }
        return {}
    }, [editIndex])

    return (
        <Wrapper>
            <BreadcrumbComp
                backPath='/groups'
                cta={<>
                    {
                        group?.statusId && ![2, 9, 11].includes(group?.statusId) &&
                        <Button type="primary" href={`/edit-group/${params?.id}`}>Edit</Button>
                    }
                </>}
                breadCrumbs={[
                    { name: "Groups", url: "/groups" },
                    { name: group?.groupName ?? '-', url: `/view-group/${params?.id}` },
                ]}
            />
            {(loading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <Card>
                        <Typography className='table-title'>Basic Info</Typography>
                        <Row gutter={[24, 40]} className="row-label-value">
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Group Name"
                                    value={ternaryOperator(group?.groupName, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="ID"
                                    value={ternaryOperator(group?.id, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Group Privacy"
                                    value={ternaryOperator(group?.masterGroupPrivacyByPrivacyId?.description, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Group Status"
                                    value={ternaryOperator(group?.masterGroupStatusByStatusId?.description, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Start Date & Time"
                                    value={formatDate(group?.startAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="End Date & Time"
                                    value={formatDate(group?.endAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Display Start Date & Time"
                                    value={formatDate(group?.displayStartAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Display End Date & Time"
                                    value={formatDate(group?.displayEndAt)}
                                />
                            </Col>
                            <Col xs={24}>
                                <LabelValue
                                    field="Group Description"
                                    value={
                                        <div
                                            dangerouslySetInnerHTML={{ __html: ternaryOperator(group?.description, '-') }}
                                        />}
                                />
                            </Col>
                            <Col xs={24} className="image-col">
                                <Typography.Text>Group Image</Typography.Text>
                                <div>
                                    <Avatar
                                        shape="square"
                                        size={160}
                                        src={`${config.apiBaseUrl}files/${group?.imageUrl}`}
                                    />
                                </div>
                            </Col>
                            <Col xs={24} className="image-col">
                                <Typography.Text>Group Banner Image</Typography.Text>
                                <Space align='center' wrap size={24}>
                                    {
                                        (group?.bannerUrl && parseJson(group?.bannerUrl)?.length > 0) ? (
                                            <>
                                                {
                                                    parseJson(group?.bannerUrl)?.map((_: any) => (
                                                        <Avatar
                                                            shape="square"
                                                            size={160}
                                                            src={`${config.apiBaseUrl}files/${_}`}
                                                        />
                                                    ))
                                                }
                                            </>
                                        ) : "-"
                                    }
                                </Space>
                            </Col>
                        </Row>
                    </Card>
                    <Card>
                        <Typography className='table-title'>Group Permissions</Typography>
                        {groupPermissionOptions?.allMasterGroupPermissions?.nodes?.length ? (
                            <Radio.Group value={group?.masterGroupPermissionByPermissionId?.id} className="radio-readonly" disabled={groupPermissionOptionLoading}>
                                {
                                    groupPermissionOptions?.allMasterGroupPermissions?.nodes?.map(_ => (
                                        <Radio value={_?.id}>{_?.description}</Radio>
                                    ))
                                }
                            </Radio.Group>
                        ) : null}
                    </Card>
                    <div className="tabs">
                        <Typography.Text className={`tab-item ${activeTab === 0 ? "active" : ""}`} onClick={() => changeTab(0)}>Moderators</Typography.Text>
                        <Typography.Text className={`tab-item ${activeTab === 1 ? "active" : ""}`} onClick={() => changeTab(1)}>Fans</Typography.Text>
                    </div>
                    <Card>
                        <Space align="center" size={24} wrap className='table-list'>
                            <Typography className='table-title'>{getTableName}</Typography>
                            <Input
                                type="text"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeSearch(e.target.value)}
                                value={searchText}
                                autoComplete={"off"}
                                placeholder="Search by Name or Email Id"
                                className="search-input"
                                prefix={<SearchOutlined />}
                            />
                        </Space>
                        <CustomTable
                            columns={tableColumns}
                            dataSource={returnDataSource ?? []}
                            loading={groupMembersListLoading || moderatorsListLoading}
                            pagination={pagination}
                            onChangeTable={onChangeTable}
                        />
                    </Card>
                    <Row gutter={[24, 24]} className="card-chips">
                        <Col xs={24} sm={12}>
                            <Card>
                                <Typography.Text>Group Members</Typography.Text>
                                <Typography.Text>{group?.groupMembersByGroupId.totalCount ?? 0}</Typography.Text>
                            </Card>
                        </Col>
                        <Col xs={24} sm={12}>
                            <Card>
                                <Typography.Text>Number of Moderators</Typography.Text>
                                <Typography.Text>{group?.moderator_count?.totalCount ?? 0}</Typography.Text>
                            </Card>
                        </Col>
                    </Row>
                    <Card>
                        <Typography className='table-title'>Notes</Typography>
                        <NoteModal
                            EditData={viewNoteData}
                            ModalVisible={isVisible}
                            handleOk={handleNoteModal}
                            handleCancel={handleNoteModal}
                            readOnly
                        />
                        <Note
                            JsonData={contructNoteJson}
                            openEdit={(obj: any, key: number) => handleNoteModal(true, key - 1)}
                            readOnly
                        />
                    </Card>
                </>
            )}
        </Wrapper>
    )
}

export default ViewGroup