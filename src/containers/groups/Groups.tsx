import React, { useEffect, useState } from 'react';
import { Col, Input, Button, Space, Typography, Card } from 'antd';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import styled from 'styled-components/macro';
import CustomTable from 'components/customTable'
import type { TablePaginationConfig } from 'antd/lib/table';
import SelectElement from 'components/common/SelectElement';
import { useGetGroupPrivaciesQuery } from 'generated/pgraphql';
import { toast } from 'react-toastify';
import { useDebounce } from 'hooks/useDebounce';
import { CloseOutlined, SearchOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { ListingGroupsPayload } from 'ts/interfaces/groups';
import { groupListing } from 'apis/group';
import moment from 'moment';

const GroupsWrapper = styled('div')`
    width:100%;
    padding:32px;
    background-color:#F5F5F7;
    ${FlexRowWrapper}{
        align-items:center;   
        margin-bottom:32px;
        gap:16px;
        .title{
            color: #051f24;
            font-weight: 600;
            font-size: 25px;
            flex:1;
            margin:0;
        }
        .create-rewards-btn{
            display:flex;
            align-items:center;
            color:#07697D;
            border-color:#07697D;
            border-radius: 5px;
            font-weight: 500;
            font-size: 16px;
            gap: 12px;
            height: 100%;
            padding: 10px;
            & span{
                line-height:1;
            }
        }
    }
    .ant-space{
        width:100%;
        margin-bottom:32px;
        & .ant-space-item:first-child{
            flex:1;
        }
        & .ant-typography{
            color:#051F2499;
            font-weight: 400;
            font-size: 14px;
            margin-bottom:8px
        }
        & .ant-col{
            & .ant-input,.ant-picker{
                border-radius: 5px;
                border: 1px solid #97a0c3;
            }
        }
        & .clear-filter{
            color: #FF4D4A;
            border-color: #FF4D4A;
            border-radius: 5px;
            & .anticon{
                font-size:12px;
            }
        }
    }
    .ant-card-body{
        padding:24px;
        padding-left:8px;
        ${FlexRowWrapper}{
            margin-bottom:16px;
            & .title{
                font-size:18px;
                padding-left:16px
            }
            & .search-input{
                border: 1px solid rgba(5, 31, 36, 0.1);
                border-radius: 5px;
                & .ant-input-prefix{
                color:#97a0c3;
            }
            }
        }
    }
`;

interface ListingProps { }

interface TableDataType {
    status: string;
    groupName: string;
    privacy: string;
    createdOn: string;
    moderators: number;
    fans: number;
}

const toLowerCase = (val: string) => val?.toLowerCase()?.trim();

const tableColumns = [
    { title: 'Status', dataIndex: "status", },
    { title: 'Group Name', dataIndex: "groupName", sorter: (a: any, b: any) => toLowerCase(a?.groupName).localeCompare(toLowerCase(b?.groupName)) },
    // { title: 'Badge', dataIndex: "badge", sorter: (a: any, b: any) => toLowerCase(a?.badge).localeCompare(toLowerCase(b?.badge)) },
    { title: 'Privacy', dataIndex: "privacy", sorter: (a: any, b: any) => toLowerCase(a?.privacy).localeCompare(toLowerCase(b?.privacy)) },
    {
        title: 'Created On',
        dataIndex: "createdOn",
        valueFormat: (value: Date) => moment(value).format('DD-MM-yyyy, hh:mm a'),
        sorter: (a: any, b: any) => moment(a.createdOn).unix() - moment(b.createdOn).unix()
    },
    { title: 'Moderators', dataIndex: "moderators", sorter: (a: any, b: any) => a.moderators - b.moderators },
    { title: 'Fans', dataIndex: "fans", sorter: (a: any, b: any) => a.fans - b.fans },
]

let initialState = {
    search: '',
    privacy: null,
}

type PrivacyOptionsType = {
    id: number;
    name: string;
    description: string
};


let pageSize = 5;

const Groups = ({ }: ListingProps): JSX.Element => {

    //debounce hook
    const debounce = useDebounce();
    const history = useHistory();

    const { data: groupPrivacyOptions, loading: groupPrivacyOptionLoading } = useGetGroupPrivaciesQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const [pagination, setPagination] = React.useState<TablePaginationConfig>({
        current: 1,
        pageSize,
        total: 0,
    });

    const [state, setState] = React.useState<any>(initialState);
    const [loading, setLoading] = useState<any>(null);
    const [dataSource, setDataSource] = useState<TableDataType[]>([]);

    useEffect(() => {
        getAllGroups()
    }, []);

    //on search input in the table
    const onSearchTableList = (value: string) => {
        onChangeState('search', value)
        debounce(() => getAllGroups(1, pageSize, value, state?.privacy), 800);
    }

    // on change privacy
    const onChangePrivacy = (value: any) => {
        onChangeState('privacy', value)
        getAllGroups(1, pageSize, state?.search, value)
    }

    //to clear filter
    const clearFilters = () => {
        setState(initialState);
        getAllGroups();
    }

    // get all Groups
    const getAllGroups = (offset: number = 1, limit: number = pageSize, searchText: string = "", privacy: any = null, from: string = "initial") => {
        setLoading('table');
        let payload: ListingGroupsPayload = {
            "start": ((offset * limit) - limit),
            "length": limit,
            "searchText": searchText,
            ...(privacy && { "privacy": privacy, })
        }

        groupListing(payload).then((res: any) => {
            let groups = res?.data?.data?.list;
            let contructGroups = [];
            if (res?.data?.statusCode === "500") {
                catchError(res?.data?.message)
            }
            if (groups?.allGroups?.nodes?.length) {
                contructGroups = groups?.allGroups?.nodes?.map((ap: any) => {
                    return (
                        {
                            status: ap?.status?.description ?? '-',
                            groupName: ap?.groupName ?? '-',
                            privacy: ap?.privacy?.description ?? '-',
                            createdOn: moment(ap?.createdAt + 'z')?.toISOString(),
                            moderators: ap?.Moderate_count?.totalCount ?? 0, 
                            fans: ap?.Fans_count?.totalCount ?? 0,
                            id: ap?.id
                        }
                    )
                })
            }
            setDataSource(contructGroups);
            setPagination({
                ...pagination,
                current: from === "initial" ? 1 : offset,
                pageSize: limit,
                total: groups?.allGroups?.totalCount
            })
            setLoading(null)
        }).catch(err => catchError(err));
    }

    const catchError = (err: string) => {
        console.log(err);
        setLoading(null)
        toast.error('Something went wrong!');
        return false
    }

    const onChangeState = (key: string, value: any) => {
        setState({
            ...state, [key]: value
        })
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => {
        const { search, privacy } = state;
        const { current = 1, pageSize } = newPagination;
        let offset = current;
        let from = "pagination"
        if (pagination?.pageSize !== newPagination?.pageSize) {
            offset = 1;
            from = ""
        }
        getAllGroups(offset, pageSize, search, privacy, from)
    }

    const onRow = (record: any, rowIndex: number) => {
        history.push(`/view-group/${record?.id}`);
    }

    return (
        <GroupsWrapper>
            <FlexRowWrapper>
                <p className='title'>Groups</p>
                <Button className='create-rewards-btn' onClick={() => history.push('/create-group')}>
                    <AddPersonIcon /> Add New Group
                </Button>
            </FlexRowWrapper>
            <Space size={24} align="end">
                <Col xs={24} sm={8} md={6}>
                    <Typography>Privacy</Typography>
                    <SelectElement
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangePrivacy(e)}
                        value={state?.privacy ?? null}
                        options={groupPrivacyOptions?.allMasterGroupPrivacies?.nodes ? groupPrivacyOptions?.allMasterGroupPrivacies?.nodes?.filter((_: any) => _?.id === 2) as unknown as PrivacyOptionsType[] : []}
                        placeholder="Privacy"
                        searchable={true}
                        toFilter="description"
                        toStore='id'
                        loading={groupPrivacyOptionLoading}
                    />
                </Col>
                <Button onClick={clearFilters} className='clear-filter'>
                    <CloseOutlined />
                    Clear Filter
                </Button>
            </Space>
            <Card>
                <FlexRowWrapper>
                    <p className='title'>List of Groups ({pagination?.total ?? 0})</p>
                    <Col xs={24} sm={8} md={6}>
                        <Input
                            type="text"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onSearchTableList(e.target.value)}
                            value={state?.search}
                            autoComplete={"off"}
                            placeholder="Search by Group Name"
                            className="search-input"
                            prefix={<SearchOutlined />}
                        />
                    </Col>
                </FlexRowWrapper>
                <CustomTable
                    columns={tableColumns}
                    dataSource={dataSource}
                    loading={loading === "table"}
                    pagination={pagination}
                    onChangeTable={onChangeTable}
                    onRowClick={onRow}
                />
            </Card>
        </GroupsWrapper>
    );
};

export default Groups;

