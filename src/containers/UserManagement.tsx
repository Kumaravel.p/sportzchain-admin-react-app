import * as React from 'react';
import styled from 'styled-components/macro';
import { add } from 'date-fns';
import Text from 'antd/lib/typography/Text';

import {
  useGetAllUsersQuery,
  useGetUserInfoStatsQuery,
} from 'generated/graphql';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import UserInfo from 'components/user-management/UserInfo';
import StatCard from 'components/common/cards/StatCard';

import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';

const UserManagementWrapper = styled.div`
  width: 100%;
  padding: 40px;
  .ant-card {
    width: 303px;
    .ant-card-head {
      border-bottom: none;
    }
  }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-user-management {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
    .ant-btn.add-new-button {
      border: 1px solid rgba(7, 105, 125, 0.25);
      color: rgba(7, 105, 125, 1);
      box-sizing: border-box;
      border-radius: 5px;
      height: fit-content;
      padding: 15px;
      display: flex;
      align-items: center;
      column-gap: 10px;
    }
  }

  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
`;

interface UserManagementProps {}

const UserManagement = ({}: UserManagementProps): JSX.Element => {
  const [currentDate, setCurrentDate] = React.useState('');
  const [filters, setFilters] = React.useState({
    team: '',
    status: '',
    leaderboard: '',
    age: '',
    location: '',
  });

  const { data: usersData, refetch: refetchUsersData } = useGetAllUsersQuery({
    variables: { where: {} },
  });

  React.useEffect(() => {
    setCurrentDate(add(new Date(), { months: 1 }).toISOString());
  }, []);

  const { data: userInfoStatsData, refetch: refetchUserInfoStats } =
    useGetUserInfoStatsQuery();

  // const getNewUsers = async () => {
  //   const newUsers = await refetchUserInfoStats({
  //     filterBy: { lastActive: { _gt: '2022-01-18T08:48:43.462Z' } },
  //   }).then((data) => data);
  //   return newUsers;
  // };

  // const getInactiveUsers = async () => {
  //   const inactiveUsers = await refetchUserInfoStats({
  //     filterBy:{ lastActive: {_gt: "2022-01-18T08:48:43.462Z"}},
  //   }).then((data) => data);
  //   return inactiveUsers;
  // };

  return (
    <UserManagementWrapper>
      <FlexRowWrapper className="user-text-create-wrapper">
        <Text className="t-user-management">User Management</Text>
        {/* <Button className="add-new-button">
          {' '}
          <AddPersonIcon /> Add New User
        </Button> */}
      </FlexRowWrapper>

      <FlexRowWrapper className="cards-wrapper">
        <StatCard
          title="Total users"
          cta={<MoreIcon />}
          value={
            <div className="green">
              {userInfoStatsData?.user_aggregate?.aggregate?.count ?? 0}
            </div>
          }
          supportingValueText="This Month"
        />

        <StatCard
          title="New users"
          cta={<MoreIcon />}
          value={
            <div className="green">
              {userInfoStatsData?.user_aggregate?.aggregate?.count ?? 0}
            </div>
          }
          supportingValueText="This Month"
        />

        <StatCard
          title="Inactive Users"
          cta={<MoreIcon />}
          value={
            <div className="green">{userInfoStatsData?.user.length ?? 0}</div>
          }
          supportingValueText="This Month"
        />
      </FlexRowWrapper>

      <UserInfo
        usersData={usersData?.user ?? []}
        filters={filters}
        setFilters={setFilters}
        refetchUsersData={refetchUsersData}
      />
    </UserManagementWrapper>
  );
};

export default UserManagement;
