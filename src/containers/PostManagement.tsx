import { Button, Card } from 'antd';
import * as React from 'react';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import UserInfo from 'components/user-management/UserInfo';
import styled from 'styled-components/macro';
import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import TeamInfo from 'components/team/TeamInfo';
import {
  GetPostByIdQuery,
  GetPostsQuery,
  useGetPostsQuery,
  useGetTeamsQuery,
} from 'generated/graphql';
import { Link, useHistory } from 'react-router-dom';
import TitleCta from 'components/common/TitleCta';
import TitleCtaTable from 'components/common/TitleCtaTable';
import { postColumns } from 'data/post';

const PostManagementWrapper = styled.div`
  width: 100%;
  padding: 40px;
  .ant-card {
    .ant-card-head {
      border-bottom: none;
    }
  }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-user-management {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
    .ant-btn.add-new-button {
      border: 1px solid rgba(7, 105, 125, 0.25);
      color: rgba(7, 105, 125, 1);
      box-sizing: border-box;
      border-radius: 5px;
      height: fit-content;
      /* padding: 15px; */
      display: flex;
      align-items: center;
      column-gap: 10px;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */

    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      /* Red / 1 */
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }

  .t-month {
    font-size: 14px;
    line-height: 19px;
    display: flex;
    align-items: flex-end;
    letter-spacing: -0.1px;
    color: rgba(50, 60, 71, 0.4);
  }

  .count-monthly-wrapper {
    justify-content: space-between;
    align-items: center;
  }

  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
`;

interface PostManagementProps {}

const PostManagement = ({}: PostManagementProps): JSX.Element => {
  const history = useHistory();
  const { data: postsData, refetch: refetchPosts } = useGetPostsQuery();

  React.useEffect(() => {
    refetchPosts();
  }, []);

  return (
    <PostManagementWrapper>
      <TitleCta
        title="Post Management"
        cta={
          <Link to="/post/add">
            <Button className="add-new-button">Add New Post</Button>
          </Link>
        }
      />

      <TitleCtaTable
        data={postsData?.post}
        columns={postColumns}
        title=""
        onRow={(record: GetPostByIdQuery['post_by_pk'], _: any) => {
          return {
            onClick: () => {
              history.push(`/posts/${record?.id}`);
            },
          };
        }}
      />
    </PostManagementWrapper>
  );
};

export default PostManagement;
