import React, { useEffect, useMemo, useState } from 'react';
import { Row, Col, Typography, DatePicker, Input, Card, Space, Button, TablePaginationConfig, Modal, Form, Spin, FormInstance } from 'antd';
import SelectElement from 'components/common/SelectElement';
import styled from 'styled-components/macro';
import { DeleteOutlined } from '@ant-design/icons';
import CustomTable from 'components/customTable';
import { RulesModal } from 'components/modals/rewards';
import { useGetRwrdActivitiesLazyQuery, useGetRwrdCategoriesQuery, useGetRwrdLineItemQuery } from 'generated/pgraphql';
import LabelInput from 'components/common/LabelInput';
import BreadcrumbComp from 'components/common/breadcrumb';
import { format } from 'date-fns';
import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import moment from 'moment';
import { RewardCreationProps } from 'ts/interfaces/reward.interface';
import { rewardCreation, rewardUpdation } from 'apis/reward';
import { toast } from 'react-toastify';
import { useHistory, useParams } from 'react-router-dom';
import { range } from 'utils';
import currentSession from 'utils/getAuthToken';

const Wrapper = styled('div')`
width:100%;
padding:40px;
background-color: #F5F5F7;
& .card-wrap{
    margin-top: 16px;
    & .ant-card-bordered{
        border-radius: 10px;
        margin-bottom: 16px;
        & .edit-icon{
            & path{
                fill: #4DA1FF;
            }
        }
        & .delete-icon{
            & path{
                fill: red;
            }
        }
    }
}
& .header{
    color:#051F24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
}
& .label{
    font-weight: 400;
    font-size: 14px;
    color:#051F2499;
    margin-bottom: 16px;
    & span{
        color:red;
        margin-left: 8px;
    }
}
& .ant-select-selector{
    border:1px solid #d9d9d9 !important;
    border-radius: 2px !important;
}
& .space{
    width:100%;
    padding-inline: 16px;
    margin-bottom: 16px;
    & .ant-space-item:first-of-type{
        flex:1;
    }
    & .table-title{
        color:#000000;
        font-size: 16px;
        font-weight: 600;
    }
    & .ant-btn-text{
        font-weight: 600;
        font-size: 16px;
        color: #4DA1FF;
    }
}
`;

const ModalWrapper = styled.div`
& .label{
    font-weight: 400;
    font-size: 14px;
    color:#051F2499;
    margin-bottom: 12px;
}
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

interface RewardsProps { }


interface RulesType {
    status: string;
    ruleName: string;
    rewardPerUser: string;
    totalQuantity: string;
    timePeriod: string;
    actions?: any;
}

interface NotesType {
    index: number;
    note: string;
    addedBy: string;
    addedDate: string;
    actions: any;
}

const rulesColumns = [
    { title: 'Status', dataIndex: "status", component: 'status', },
    { title: 'Rule Name', dataIndex: "ruleName", sorter: (a: any, b: any) => a.ruleName.localeCompare(b.ruleName) },
    { title: 'Reward Per User', dataIndex: "rewardPerUser" },
    { title: 'Total Quantity', dataIndex: "totalQuantity" },
    { title: 'Time Period', dataIndex: "timePeriod" },
    { title: 'Actions', dataIndex: "actions", component: 'icons', align: 'center' },
]

const notesColumns = [
    { title: '#', dataIndex: "index", },
    { title: 'Note', dataIndex: "note", sorter: (a: any, b: any) => a.note.localeCompare(b.note) },
    { title: 'Added By', dataIndex: "addedBy" },
    { title: 'Added Date', dataIndex: "addedDate" },
    { title: 'Actions', dataIndex: "actions", component: 'icons', align: 'center' },
]

type RwrdCategoriesOptionType = {
    category: string;
    id: number;
    __typename: number;
}[];

type RwrdActivitiesOptionType = {
    description: string;
    id: number;
    __typename: number;
}[];


const CreateRewards = (props: RewardsProps): JSX.Element => {

    const [form] = Form.useForm();
    const [ruleForm] = Form.useForm();
    const [noteForm] = Form.useForm();
    const params: { id?: string } = useParams();
    const history = useHistory();

    const [getActivites, { data: RwrdActivitiesOption, loading: RwrdActivitiesOptionLoading }] = useGetRwrdActivitiesLazyQuery({
        fetchPolicy:"network-only",
        notifyOnNetworkStatusChange:true
    });

    const {
        data: RwrdCategoriesOption,
        loading: RwrdCategoriesOptionLoading
    } = useGetRwrdCategoriesQuery({
        fetchPolicy:"network-only",
        notifyOnNetworkStatusChange:true
    });

    const {
        data: RwrdDetailsData,
        loading: RwrdDetailsLoading,
    } = useGetRwrdLineItemQuery({
        variables: { id: params?.id },
        skip: !(!!params.id),
        fetchPolicy: 'network-only',
    });

    const [loading, setLoading] = useState<boolean>(false);
    const [visibleModal, setVisibleModal] = useState<'notes' | 'rules' | null>(null);
    const [editIndex, setEditIndex] = useState<number>(-1);
    const [rulesList, setRulesList] = useState<RulesType[] | []>([]);
    const [notesList, setNotesList] = useState<NotesType[] | []>([]);
    const [userData, setUserData] = React.useState<any>({});
    const [ruleId, setRuleId] = React.useState<string>();

    //Fetching reward details
    React.useEffect(() => {
        let data = RwrdDetailsData?.allRwrds?.nodes?.[0];
        if (data) {
            setLoading(true);
            let rules: any[] = data?.rwrdRulesByRwrdId?.nodes?.map((rule: any) => ({
                status: {
                    label: rule?.masterRwrdRuleStatusByRuleStatusId?.status,
                    value: rule?.ruleStatusId,
                },
                ruleName: rule?.ruleName,
                rewardsPerUser: Number(rule?.rewardPerUser),
                totalQuantityOfRewardsPerUser: Number(rule?.totalQuantity),
                startDateTime: moment(rule?.startDate?.concat('z')),
                endDateTime: moment(rule?.endDate?.concat('z')),
                rewardValue: Number(rule?.rewardValue),
                maxLimit: rule?.maxLimit,
                duration: rule?.duration,
                id:rule?.id
            }));
            let notes: any[] = data?.rwrdNotesByRwrdId?.nodes?.map((note: any) => ({
                title: note?.name,
                description: note?.notes,
                addedDate: moment(note?.createdDate?.concat('z')),
                addedBy: note?.addedBy
            }))
            let categoryId = RwrdDetailsData?.allRwrds?.nodes?.[0]?.categoryId;
            if(categoryId){
                getActivites({
                    variables: {
                        categoryId
                    }
                })
            }
            setRulesList(rules);
            setNotesList(notes);
            setLoading(false);
        }
    }, [RwrdDetailsData])

    useEffect(() => {
        currentSession().then(res=> {
          setUserData(res.payload)
        })
      }, []);

    const formatDate = (date: Date | number, dateFormat: string = "dd MMM, yy", delimiter: string = "",) => date ? format(new Date(date), dateFormat) : delimiter;

    //contruct rules table
    const rulesDataSource: RulesType[] = useMemo(() => {
        if (!rulesList?.length) return []
        return rulesList?.map((rule: any) => ({
            status: rule?.status?.label ?? '-',
            ruleName: rule?.ruleName,
            rewardPerUser: rule?.rewardsPerUser,
            totalQuantity: rule?.totalQuantityOfRewardsPerUser,
            timePeriod: `${formatDate(rule?.startDateTime)} - ${formatDate(rule?.endDateTime)}`,
            actions: (rowRecord: any, rowIndex: number) => (
                <Space size={12}>
                    <EditIcon className='edit-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "rules", "edit",rule)} />
                    <DeleteOutlined className='delete-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "rules", "delete")} />
                </Space>
            )
        }))
        // eslint-disable-next-line
    }, [rulesList])

    //contruct notes table
    const notesDataSource: NotesType[] = useMemo(() => {
        if (!notesList?.length) return []
        return notesList?.map((note: any, index: number) => ({
            index: index + 1,
            note: note?.title ?? '-',
            addedBy: note?.addedBy,
            addedDate: `${formatDate(note?.addedDate, 'dd MMM, yy hh:mm')}`,
            actions: (rowRecord: any, rowIndex: number) => (
                <Space size={12}>
                    <EditIcon className='edit-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "notes", "edit")} />
                    <DeleteOutlined className='delete-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "notes", "delete")} />
                </Space>
            )
        }))
        // eslint-disable-next-line
    }, [notesList])

    //on click table icons
    const onClickIcons = (e: any, rowRecord: any, rowIndex: number, key: 'notes' | 'rules' | null = null, from: string,data?:any) => {
        e.stopPropagation();
        let form: FormInstance, list: any[] = [], setState: Function;
        if (key === "rules") {
            form = ruleForm;
            list = rulesList;
            setState = setRulesList;
        }
        else {
        form = noteForm;
            list = notesList;
            setState = setNotesList;
        }
        if (from === "edit") {
            form.setFieldsValue(list[rowIndex]);
            setVisibleModal(key);
            setEditIndex(rowIndex)
        }
        if(key === "rules" && from === "edit"){
            setRuleId(data.id)
        }
        else if (from === "delete") {
            setState(list?.filter((_: any, index: number) => index !== rowIndex))
        }
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => { }

    const onCancelModal = () => {
        setVisibleModal(null)
        setEditIndex(-1)
    }

    //on save or update check validations
    const onSubmitModal = () => {

        let list: any[] = [], setState: Function, form: FormInstance, keys: string[] = [];

        const checkError = (array: string[] = [], form: FormInstance) => (array.some((key: string) => !form.getFieldValue(key)))

        if (visibleModal === "rules") {
            keys = ['ruleName', 'status', 'rewardsPerUser', 'rewardValue', 'totalQuantityOfRewardsPerUser', 'startDateTime', 'endDateTime']
            list = rulesList;
            form = ruleForm;
            setState = setRulesList;
        }
        else {
            keys = ['title', 'description'];
            list = notesList;
            form = noteForm;
            setState = setNotesList;
        }

        form.submit();

        if (!checkError(keys, form)) {
            if (editIndex > -1) {
                let copyList = [...list];
                copyList[editIndex] = { ...copyList[editIndex], ...form.getFieldsValue() }
                setState(copyList)
                setEditIndex(-1);
            }
            else {
                if (visibleModal === "rules") {
                    setState([...list, form.getFieldsValue()])
                }
                else {
                    setState([
                        ...list, {
                            ...form.getFieldsValue(),
                            addedBy: userData?.name,
                            addedDate: moment()
                        }])
                }
            }
            setVisibleModal(null);
        }
    }

    //on save or update
    const onSave = () => {

        if (rulesList?.length) {
            let rewardDetails = form.getFieldsValue();

        
            const dateToISOString = (date: Date): string => moment(date).seconds('0o0' as unknown as number). toISOString();
            setLoading(true);
            let payload: RewardCreationProps = {
                ...(params?.id && { id: params.id }),
                startDateTime: dateToISOString(rewardDetails?.startDateTime),
                endDateTime: dateToISOString(rewardDetails?.endDateTime),
                rewardDescription: rewardDetails?.rewardDescription,
                category: rewardDetails?.category.toString(),
                activity: rewardDetails?.activity.toString(),
                rules: rulesList?.map((rule: any) => ({
                    ruleName: rule?.ruleName,
                    rewardsPerUser: rule?.rewardsPerUser,
                    rewardValue: rule?.rewardValue,
                    startDateTime: dateToISOString(rule.startDateTime),
                    endDateTime: dateToISOString(rule.endDateTime),
                    maxLimit: (rule?.maxLimit && typeof Number(rule.maxLimit)) ? Number(rule.maxLimit) : null,
                    duration: (rule?.duration && typeof Number(rule.duration)) ? Number(rule.duration) : null,
                    durationUnit: rule.durationUnit,
                    status: rule.status.value,
                    totalQuantityOfRewardsPerUser: rule.totalQuantityOfRewardsPerUser,
                    id:rule?.id
                })),
                notes: notesList?.map((note: any) => ({
                    title: note?.title,
                    description: note?.description,
                    addedDate: dateToISOString(note?.addedDate),
                    addedby: note?.addedBy,
                }))
            }

            let api = params?.id ? rewardUpdation : rewardCreation;
            api(payload)
                .then(res => {
                    if (res.data.statusCode === "500") {
                        toast.error(res.data.message);
                        return false
                    }
                    setLoading(false);
                    toast.success(res?.data?.message)
                    history.push('/rewards');
                })
                .catch(e => {
                    console.log(e)
                    setLoading(false);
                    toast.error('Something went wrong')
                })
        }
        else {
            toast.error('Atleast one rule should be added. !!')
        }
    }

    const rewardDetails = RwrdDetailsData?.allRwrds?.nodes?.[0];
    //setting reward details to form
    const returnInitialValues = useMemo(() => {
        return {
            category: rewardDetails?.categoryId,
            activity: rewardDetails?.activityId,
            startDateTime: moment(rewardDetails?.startDate?.concat('z')),
            endDateTime: moment(rewardDetails?.endDate?.concat('z')),
            rewardDescription: rewardDetails?.description
        }
    }, [rewardDetails])

    const onValuesChange = (field: any) => {
        if (field?.category) {
            getActivites({
                variables: {
                    categoryId: field?.category
                }
            })
            form.setFieldsValue({ activity: null })
        }
    }

    return (
        <Wrapper style={{ width: '100%' }}>
            <BreadcrumbComp
                backPath='/rewards'
                cta={<>
                    <Button
                        type="primary"
                        style={{ marginRight: "16px" }}
                        onClick={form.submit}
                        disabled={loading}
                    >
                        {params?.id ? 'Update' : 'Add'} Reward
                    </Button>
                    <Button href='/rewards'>Discard</Button>
                </>}
                breadCrumbs={[
                    { name: "Rewards", url: "/rewards" },
                    { name: `${params?.id ? 'Update' : 'Add New'} Reward`, url: params.id ? `/edit-reward/${params.id}` : "/create-reward" },
                ]}
            />
            {(loading || RwrdDetailsLoading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <div className='card-wrap'>
                        <Card>
                            <Typography className='header'>Reward Details</Typography>
                            <Form
                                onFinish={onSave}
                                form={form}
                                preserve={false}
                                initialValues={
                                    params?.id ? returnInitialValues : {}
                                }
                                onValuesChange={onValuesChange}
                            >
                                <Row gutter={[24, 24]}>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="Select Category"
                                            name="category"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Category is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                    options={
                                                        RwrdCategoriesOption?.allMasterRwrdCategories?.nodes as unknown as RwrdCategoriesOptionType ?? []
                                                    }
                                                    placeholder="Select Category"
                                                    searchable={true}
                                                    toFilter="category"
                                                    toStore='id'
                                                    loading={RwrdCategoriesOptionLoading}
                                                    disabled={RwrdCategoriesOptionLoading}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="Select Activity"
                                            name="activity"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Activity is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                    options={RwrdActivitiesOption?.allMasterRwrdActivities?.nodes as unknown as RwrdActivitiesOptionType ?? []}
                                                    placeholder="Select Activity"
                                                    searchable={true}
                                                    toFilter="description"
                                                    toStore='id'
                                                    // mode="multiple"
                                                    loading={RwrdActivitiesOptionLoading}
                                                    disabled={RwrdActivitiesOptionLoading}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="Start Date & Time"
                                            name="startDateTime"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Start Date & Time is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showTime
                                                    showNow={false}
                                                    showSecond={false}
                                                    // value={state?.startDateTime}
                                                    // onChange={(e: any) => onChangeState('startDateTime', e)}
                                                    style={{ width: '100%' }}
                                                    disabledDate={d => d && d < moment().subtract(1, 'days')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="End Date & Time"
                                            name="endDateTime"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'End Date & Time is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showTime
                                                    showNow={false}
                                                    showSecond={false}
                                                    // value={state?.endDateTime}
                                                    // onChange={(e: any) => onChangeState('endDateTime', e)}
                                                    style={{ width: '100%' }}
                                                    disabledDate={d => (form.getFieldValue('startDateTime') ? d && d < moment(form.getFieldValue('startDateTime')) : true)}
                                                    disabledHours={() => form.getFieldValue('startDateTime') ? [] : range(0, 24)}
                                                    disabledMinutes={() => form.getFieldValue('startDateTime') ? [] : range(0, 59)}
                                                    disabledSeconds={() => form.getFieldValue('startDateTime') ? [] : range(0, 59)}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Reward Description"
                                            name="rewardDescription"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Reward Description is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Input
                                                    placeholder="Description"
                                                />
                                            }
                                        />
                                    </Col>
                                </Row>
                            </Form>
                        </Card>
                        <Card>
                            <Space className='space'>
                                <Typography className='table-title'>Rules ({rulesList?.length})</Typography>
                                <Button type="text" onClick={() => setVisibleModal('rules')}>+ Add New Rule</Button>
                            </Space>
                            <CustomTable
                                columns={rulesColumns}
                                dataSource={rulesDataSource}
                                hidePagination={true}
                                onChangeTable={onChangeTable}
                            />
                        </Card>
                        <Card>
                            <Space className='space'>
                                <Typography className='table-title'>Notes ({notesList?.length})</Typography>
                                <Button type="text" onClick={() => setVisibleModal('notes')}>+ Add New Note</Button>
                            </Space>
                            <CustomTable
                                columns={notesColumns}
                                dataSource={notesDataSource}
                                hidePagination={true}
                                onChangeTable={onChangeTable}
                            />
                        </Card>
                    </div>
                </>)}
            <Modal
                title={visibleModal === "rules" ? 'Rules' : 'Notes'}
                centered
                visible={visibleModal ? ['notes', 'rules']?.includes(visibleModal) : false}
                onOk={() => onSubmitModal()}
                onCancel={() => onCancelModal()}
                width={visibleModal === "rules" ? 1000 : 500}
                destroyOnClose={true}
            >
                <ModalWrapper>
                    {visibleModal === "rules" ? (
                        <RulesModal
                            form={ruleForm}
                        />
                    ) : (
                        <div>
                            <Form
                                // onFinish={onSaveNoteForm}
                                form={noteForm}
                                preserve={false}
                                initialValues={{}}
                            >
                                <Row gutter={[1 / 2, 1 / 2]}>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Note Title"
                                            name="title"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Title is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Input
                                                    placeholder="Enter Title"

                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Enter Description"
                                            name="description"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Description is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Input
                                                    placeholder="Enter Description"
                                                />
                                            }
                                        />
                                    </Col>
                                </Row>
                            </Form>
                        </div>
                    )}
                </ModalWrapper>
            </Modal>
        </Wrapper>
    )
}

export default CreateRewards