import React from 'react';
import { ReactComponent as SportzchainLogo } from 'assets/Sportzchain_logo_dark.svg';
import { Row, Col, Form, Input, Typography, Button, Spin, Space, Tooltip } from 'antd';
import styled from 'styled-components/macro';
import { useHistory, useLocation } from 'react-router-dom';
import { Auth } from 'aws-amplify';
import { toast } from 'react-toastify';
import { passwordStrength, Result } from 'check-password-strength';
import { InfoCircleOutlined } from '@ant-design/icons';

const Wrapper = styled.div`
height:100vh;
width:100%;
& .row-wrapper{
    height:100vh;
    & .background-column{
        background-color: #eee;
    }
    & .form-fields{
        padding: 40px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        & .sportzchain-logo{
            margin-bottom: 32px;
        }
        & .ant-form-item-label > label{
            font-weight: 500;
        }
        & .header{
            margin-bottom: 32px;
        }
        & .ant-typography{
            text-align:center;
        }
        & .help{
                font-size: 14px;
                font-weight: 600;
                & .ant-typography{
                    color:#fb923c;
                    cursor:pointer;
                }
            }
        & .verify-btn{
            width: 100%;
            padding: 10px;
            background: #fb923c;
            height: 100%;
            &:hover,&:focus{
                color: #000;
                border-color: #fb923c;
            }
        }
    }
}
.password-strength-wrapper {
    width:100%;
      .password-strength {
        width: 100%;
        /* display: flex; */
        column-gap: 2px;
        text-align: left;
        .ant-space-item{
            flex: 1;
            display: flex;
            justify-content: center;
        }
        .bottom-border{
            width: 60%;
            height: 2.5px;
            border-radius: 8px;
        }
        .level {
          background: #10b981;
        }
        .empty {
            background: #a3a3a3;
          }
      }
      .t-password-strength {
        display: flex;
        margin-left: 0;
        color: #10b981;
        margin-right: auto;
      }
}
`;


const ResetPassword = (): JSX.Element => {

    const [form] = Form.useForm();
    const [loading, setLoading] = React.useState<'reset' | 'resent' | null>(null);
    const history = useHistory();
    const [passwordStrengthResult, setPasswordStrengthResult] =
        React.useState<Result<string> | null>(null);
    const location: { state: { value: string } } = useLocation();

    React.useEffect(() => {
        if (!(!!location?.state?.value)) {
            toast.error('Email or Mobile Number is required')
            history.push('/forgot-password/email')
        }
    }, [location?.state?.value])

    const onSave = () => {
        console.log(form.getFieldsValue())
    }

    const resetPassword = () => {
        form.submit()
        form.validateFields()
            .then(async () => {
                setLoading('reset');
                try {
                    const { code, password } = form.getFieldsValue();
                    const data = await Auth.forgotPasswordSubmit(
                        location?.state?.value,
                        code,
                        password
                    );
                    if (data) {
                        toast.success('Password resets successfully!');
                        history.push('/signin/email');
                    }
                }
                catch (error: any) {
                    console.log(error);
                    toast.error(error?.message || JSON.stringify(error));
                }
                finally {
                    setLoading(null);
                }
            })
            .catch(e => {
                console.log(e, 'e')
            })
    }


    const resendOTP = async () => {
        setLoading('resent');
        try {
            const data = await Auth.forgotPassword(
                location?.state?.value,
            );
            if (data) {
                toast.success('Code resent successfully!');
            }
        } catch (error: any) {
            console.log(error);
            toast.error(error?.message || JSON.stringify(error));
        }
        finally {
            setLoading(null);
        }
    }

    const onValuesChange = (changedValues: any) => {
        if (changedValues?.hasOwnProperty("password")) {
            const result = passwordStrength(changedValues?.password);
            setPasswordStrengthResult(result);
        }
    }

    return (
        <Wrapper>
            <Row className="row-wrapper">
                <Col xs={24} md={14} className="background-column"></Col>
                <Col xs={24} md={10} className="form-fields">
                    <div className="sportzchain-logo">
                        <SportzchainLogo />
                    </div>
                    <Typography.Title level={2} className="header">Reset Password</Typography.Title>
                    <Form
                        onFinish={onSave}
                        form={form}
                        preserve={false}
                        // layout="vertical"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        // validateTrigger="submit"
                        onValuesChange={onValuesChange}
                    >

                        <Row gutter={[12, 32]}>
                            <Col xs={24}>
                                <Form.Item
                                    name="code"
                                    label="Verification Code"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Verification code is required!',
                                        }
                                    ]}
                                >
                                    <Input.Password
                                        placeholder='Verification code'
                                        type={"password"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24}>
                                <Form.Item
                                    name="password"
                                    label="Password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Password is required!',
                                        },
                                        () => ({
                                            validator(_, value) {
                                                if (value && (!passwordStrengthResult?.id || (passwordStrengthResult?.id < 3))) {
                                                    return Promise.reject(new Error(`Password is ${passwordStrengthResult?.value}`));
                                                }
                                                return Promise.resolve();
                                            },
                                        }),
                                    ]}
                                >
                                    <Input.Password
                                        placeholder='Enter Password'
                                        prefix={(
                                            <Tooltip placement="right" title={(
                                                <>
                                                    Password must contain at least 8 characters
                                                    <ul>
                                                        <li>lower-case letters (a, b, c)</li>
                                                        <li>upper-case letters (A, B, C)</li>
                                                        <li>digits (1, 2 3)</li>
                                                        <li>
                                                            special characters,” which include punctuation <br /> (. ; !) and
                                                            other characters (# * &)
                                                        </li>
                                                    </ul>
                                                </>
                                            )}>
                                                <InfoCircleOutlined style={{color:"rgba(0, 0, 0, 0.45)"}} />
                                            </Tooltip>
                                        )}
                                    // type={"password"}
                                    />
                                </Form.Item>
                                <Row>
                                    <Col span={8}></Col>
                                    <Col span={16}>
                                        <div className="password-strength-wrapper">
                                            <Space align='center' className="password-strength">
                                                {
                                                    new Array(4)
                                                        .fill(0)
                                                        .map((_, i) => (
                                                            <div className={
                                                                `bottom-border ${(passwordStrengthResult && (passwordStrengthResult.id >= i)) ? "level" : "empty"}`
                                                            }
                                                                key={i}
                                                            />
                                                        ))}
                                            </Space>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24}>
                                <Form.Item
                                    name="confirmPassword"
                                    label="Confirm Password"
                                    dependencies={['password']}
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Confirm Password is required!',
                                        },
                                        ({ getFieldValue }) => ({
                                            validator(_, value) {
                                                if (!value || getFieldValue('password') === value) {
                                                    return Promise.resolve();
                                                }
                                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                            },
                                        }),
                                    ]}
                                >
                                    <Input.Password
                                        placeholder='Confirm Password'
                                    // type={"password"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} style={{ textAlign: 'center' }}>
                                <Typography.Text className='help'>Didn't receive the code? <Typography.Text onClick={resendOTP}>Resend OTP {loading === "resent" && <Spin />}</Typography.Text></Typography.Text>
                            </Col>
                            <Col span={24}>
                                <Button
                                    style={{ width: '100%' }}
                                    className="verify-btn"
                                    onClick={resetPassword}
                                    loading={loading === "reset"}
                                    disabled={loading === "resent"}
                                >
                                    Reset Password
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </Wrapper>
    )
}

export default ResetPassword;