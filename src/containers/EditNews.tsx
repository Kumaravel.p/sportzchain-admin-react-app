import React from 'react';
import { useParams } from 'react-router-dom';
import { Spin, Row } from 'antd';
import Addpost from 'components/news/Addpost';
import { GetNews } from 'graphql_v2';
import graphqlReq from 'apis/graphqlRequest';

const EditNews = () => {

    const { id } = useParams<{ id: string }>();

    const [loading,setLoading] = React.useState(false);
    const [pollData,setPolls] = React.useState<any>();

    React.useEffect(()=>{
        setLoading(true)
        graphqlReq({ document: GetNews(parseInt(id)) })
        .then((data: any) => {
           setPolls(data?.allPosts?.nodes[0])
           setLoading(false)
        })
        .catch(e => console.log(e))
     },[])

    return (
        <Row style={{ width: "100%" }}>
          {!loading && pollData ?
                <Addpost newsFormData={pollData} /> :
                <div style={{ width: "100%", display: "grid", placeItems: "center" }}> <Spin /></div>
            }
        </Row>
    )

}

export default EditNews