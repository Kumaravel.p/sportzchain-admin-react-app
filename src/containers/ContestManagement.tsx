import {
  Button,
  Card,
  Row,
  Input,
  Col,
  Select,
  DatePicker,
  TablePaginationConfig,
} from 'antd';
import * as React from 'react';
import styled from 'styled-components/macro';
import { SearchOutlined, CloseOutlined } from '@ant-design/icons';
import { Label } from 'components/common/atoms';
import { useGetTeamsLazyQuery, useGetTeamsQuery } from 'generated/graphql';
import { Link, useHistory } from 'react-router-dom';
import TitleCta from 'components/common/TitleCta';
import CustomTable from 'components/customTable';
import { contestColumns } from 'data/contest';
import { useDebounce } from 'hooks/useDebounce';
import { getAllContests } from 'apis/contest';
import { toast } from 'react-toastify';
import { useGetAllRequiredContestOptionsQuery } from 'generated/pgraphql';
import { TeamSelectionContext } from 'App';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const ContestManagementWrapper = styled.div`
  width: 100%;
  padding: 40px;
  background: rgba(245, 245, 247, 1);
  .ant-select {
    width: 100%;
  }
  .ant-select .ant-select-selector {
    border-radius: 5px;
    border: 1px solid #efefef;
  }
  .ant-card {
    .ant-card-head {
      border-bottom: none;
    }
  }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-user-management {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
    .ant-btn.add-new-button {
      border: 1px solid rgba(7, 105, 125, 0.25);
      color: rgba(7, 105, 125, 1);
      box-sizing: border-box;
      border-radius: 5px;
      height: fit-content;
      /* padding: 15px; */
      display: flex;
      align-items: center;
      column-gap: 10px;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */

    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      /* Red / 1 */
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }

  .t-month {
    font-size: 14px;
    line-height: 19px;
    display: flex;
    align-items: flex-end;
    letter-spacing: -0.1px;
    color: rgba(50, 60, 71, 0.4);
  }

  .count-monthly-wrapper {
    justify-content: space-between;
    align-items: center;
  }

  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
`;

interface ContestManagementProps { }

interface ContestManagementFilter {
  status: string | null;
  teamId: string | null;
  filterDateRange: {
    startAt: any;
    endAt: any;
  };
  contestType: string | null;
  length: number;
  start: number;
  searchText: string;
}

let initialFilterData = {
  status: null,
  teamId: null,
  filterDateRange: {
    startAt: null,
    endAt: null,
  },
  contestType: null,
  length: 10,
  start: 0,
  searchText: '',
};

const ContestManagement = ({ }: ContestManagementProps): JSX.Element => {

  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }
  const isTeamUser = isIamTeamUser();
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  const [loading, setLoading] = React.useState(true);
  const [contestsData, setContestsData] = React.useState<{
    totalCount: Number;
    nodes: any[];
  }>({
    totalCount: 0,
    nodes: [],
  });
  const [filterData, setFilterData] = React.useState<ContestManagementFilter>({
    ...initialFilterData,
    teamId: isTeamUser ? teamSelectionContext.team.id + "" : null
  });

  const [pagination, setPagination] = React.useState<TablePaginationConfig>({
    current: 1,
    pageSize: 10,
    total: 0,
  });

  const fetchAllContests = async (body: any, paginationData?: TablePaginationConfig) => {
    let data = await getAllContests(body);
    setLoading(false);
    if (data?.status) {
      let contestData = data?.data?.data?.list?.allContests ?? {
        totalCount: 0,
        nodes: [],
      };
      setContestsData(contestData);

      let paginationData1 = paginationData?.current ? paginationData : pagination;
      setPagination({
        ...paginationData1,
        total: contestData?.totalCount,
      });
    } else {
      toast.error('Some Error Occured');
    }
  };

  React.useEffect(() => {
    fetchAllContests(filterData);

    if (!isTeamUser) {
      getAllTeams();
    }
  }, []);

  const history = useHistory();
  const debounce = useDebounce();

  const { Option } = Select;
  const { RangePicker } = DatePicker;

  const [getAllTeams, { data: teams }] = useGetTeamsLazyQuery();

  const { data: allRequiredOptions } = useGetAllRequiredContestOptionsQuery();

  let handleTableSearch = (value: string) => {
    /// call search api with debounce call
    debounce(() => handleFilterChange(value, 'searchText'), 800);
  };

  const clearFilter = () => {
    let updateFilter = { ...initialFilterData };
    setLoading(false);
    setFilterData(updateFilter);
    fetchAllContests(updateFilter);
  };

  const handleFilterChange = (value: any, key: string) => {
    let updateFilter = {
      ...filterData,
      [key]: value,
    };
    setFilterData(updateFilter);
    fetchAllContests(updateFilter);
  };

  const onChangeTable = (newPagination: TablePaginationConfig) => {
    const { current = 1, pageSize } = newPagination;
    let offset = current;
    let from = 'pagination';
    if (pagination?.pageSize !== newPagination?.pageSize) {
      offset = 1;
      from = '';
    }
    let paginationData = {
      ...pagination,
      current: from === 'initial' ? 1 : offset,
      pageSize: pageSize,
    };
    setLoading(false);
    let updateFilter = {
      ...filterData,
      offset: offset,
    };
    fetchAllContests(updateFilter, paginationData);
  };

  const onRowClick = (record:any) =>{
    history.push(`/contests/${record?.teamByTeamId?.id}/${record.id}`)
  }

  return (
    <ContestManagementWrapper>
      <TitleCta
        title="Contests"
        cta={
          <Link to="/contests/add">
            <Button className="add-new-button">Add New Contest</Button>
          </Link>
        }
      />
      <Row gutter={[12, 0]} align="bottom" style={{ marginBottom: '44px' }}>
        <Col span={5}>
          <Label>Status</Label>
          <Select
            placeholder="Select Status"
            allowClear
            value={filterData?.['status']}
            onChange={(value: any) => {
              handleFilterChange(value, 'status');
            }}
          >
            {allRequiredOptions?.allTeamStatuses?.nodes &&
              allRequiredOptions?.allTeamStatuses?.nodes?.length &&
              allRequiredOptions?.allTeamStatuses?.nodes?.map((item: any) => {
                return (
                  <Option value={item?.status}>{item?.description}</Option>
                );
              })}
          </Select>
        </Col>
        <Col span={5}>
          <Label>Team</Label>
          <Select
            placeholder="Select team"
            allowClear
            value={filterData?.['teamId']}
            onChange={(value: any) => {
              handleFilterChange(value, 'teamId');
            }}
            disabled={isTeamUser}
          >
            {' '}
            {teams?.team.map((team) => {
              return (
                <Option key={team.id} value={team.id}>
                  {team.name}
                </Option>
              );
            })}
          </Select>
        </Col>
        <Col span={7}>
          <Label>Duration</Label>
          <RangePicker
            value={
              [
                filterData['filterDateRange']?.startAt,
                filterData['filterDateRange']?.endAt,
              ] as any
            }
            onChange={(value) => {
              if (value) {
                handleFilterChange(
                  {
                    startAt: value[0],
                    endAt: value[1],
                  },
                  'filterDateRange'
                );
              } else {
                handleFilterChange(null, 'filterDateRange');
              }
            }}
          />
        </Col>
        <Col span={4}>
          <Label>Type of Contest</Label>
          <Select
            placeholder="Select contest"
            allowClear
            value={filterData?.['contestType']}
            onChange={(value) => {
              handleFilterChange(value, 'contestType');
            }}
          >
            <Option value="PREDICT">Predict</Option>
            <Option value="QUIZ">Quiz</Option>
          </Select>
        </Col>
        <Col span={2}>
          <Button
            type="primary"
            size="middle"
            danger
            ghost
            disabled={
              Object.entries(filterData).some(
                (filter: any) => filter[1] && filter[1].length > 0
              ) || filterData?.searchText.length > 0
                ? false
                : true
            }
            onClick={clearFilter}
            icon={<CloseOutlined />}
          >
            Clear Filter
          </Button>
        </Col>
      </Row>
      <Card>
        <TitleCta
          title={`List of Contest (${contestsData?.totalCount})`}
          cta={
            <Input
              size="middle"
              type="text"
              className="search"
              allowClear
              prefix={<SearchOutlined />}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleTableSearch(e.target.value)
              }
              placeholder={'Search'}
            />
          }
        />
        <CustomTable
          dataSource={contestsData?.nodes}
          columns={contestColumns}
          pagination={pagination}
          loading={loading}
          onChangeTable={onChangeTable}
          onRowClick={onRowClick}
        />
      </Card>
    </ContestManagementWrapper>
  );
};

export default ContestManagement;
