import { Button, Card, Spin, Row, Input, DatePicker, Col, Space } from 'antd';
import * as React from 'react';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import UserInfo from 'components/user-management/UserInfo';
import styled from 'styled-components/macro';
import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import TeamInfo from 'components/team/TeamInfo';
import {
  useGetPollsQuery,
  useGetPostsQuery,
  useGetTeamsQuery,
  useGetTeamPollTypesQuery,
  GetPollsQuery,
  useGetTeamsLazyQuery,
} from 'generated/graphql';
import { Link } from 'react-router-dom';
import TitleCtaTable from 'components/common/TitleCtaTable'
import type { TablePaginationConfig } from 'antd/lib/table';
import { pollsColumns } from 'data/polls';
import { UserAddOutlined, CloseOutlined, SearchOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router-dom';
import SelectElement from 'components/common/SelectElement';
import LabelInput from 'components/common/LabelInput';
import { getDate } from 'date-fns';
import { Select } from 'antd';
import { Label } from 'components/common/atoms'
import moment, { duration } from "moment";
import CustomTable from 'components/customTable'
import { getPollsApi } from 'apis/polls/getpolls'
import TitleCta from 'components/common/TitleCta';
import { Poll } from 'ts/interfaces/polls.inteface';
import { useDebounce } from 'hooks/useDebounce';
import { toast } from 'react-toastify';
import { TeamSelectionContext } from 'App';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const PollsManagementWrapper = styled.div`
  width: 100%;
  padding: 40px;
  background:rgba(245, 245, 247, 1);
  .ant-select{
  width:100%;
  }
  .ant-select .ant-select-selector{
    border-radius:5px;
    border:1px solid #efefef;
  }
   .ant-card{
      border-radius:10px;
      padding:5px;
      width:100%;
      // height:60%;
    }
  .search{
    border-radius:5px;
    & input{
       font-weight: 500;
    }
    & .ant-input-prefix{
      margin-right:8px;
      color:#A3A3A3;
    }
  }
  .ant-card {
    .ant-card-head {
      border-bottom: none;
    }
  }
   .add-new-button {
    border-radius: 5px;
    font-weight: 500;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;
    }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-user-management {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */

    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      /* Red / 1 */
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }

  .t-month {
    font-size: 14px;
    line-height: 19px;
    display: flex;
    align-items: flex-end;
    letter-spacing: -0.1px;
    color: rgba(50, 60, 71, 0.4);
  }

  .count-monthly-wrapper {
    justify-content: space-between;
    align-items: center;
  }

  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
`;

const FilterSelectBg = styled.div`
    background-color:white;
    padding:5px;
    display:grid;
    place-items:center;
    border-radius:5px;
    .ant-select{
      // width:120px;
    }
    .ant-select:not(.ant-select-customize-input) .ant-select-selector{
      // border:none;
      // padding:0px;
    }
    .ant-select-selection-item{
      font-size:14px;
    }
`

interface pollsFilter {
  status: string | null,
  team: string | null,
  duration: any | null,
  pollType: string | null
}

type PollsType = NonNullable<GetPollsQuery["teamPoll"]>;


const PollsManagement = () => {

  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }
  const isTeamUser = isIamTeamUser();
  const { data: pollTypes } = useGetTeamPollTypesQuery();
  const [getAllTeams, { data: teams }] = useGetTeamsLazyQuery();
  const teamSelectionContext = React.useContext(TeamSelectionContext);

  const debounce = useDebounce();
  const searchRef = React.useRef<any>(null);
  const history = useHistory();
  const { RangePicker } = DatePicker;

  const [polls, setPolls] = React.useState<PollsType>([]);
  const [searchInput, setSearchInput] = React.useState("");
  const [pollsFilter, setPollsFilter] = React.useState<pollsFilter>({
    status: null,
    team:  isTeamUser ? teamSelectionContext.team.id + "" : null,
    duration: [],
    pollType: null,
  });
  const [pollsLoading, setPollsLoading] = React.useState<boolean>(false);


  let pageSize = 5;

  const [pagination, setPagination] = React.useState<TablePaginationConfig>({
    current: 1,
    pageSize,
  });

  const { Option } = Select;

  let handleSearch = (value: string) => {
    debounce(() => setSearchInput(value), 800);
  }

  const handleFilterChange = (value: any, key: string) => {
    setPollsFilter({
      ...pollsFilter,
      [key]: value
    })
  }

  const clearFilter = () => {
    setSearchInput("");
    setPollsFilter({
      status: null,
      team: null,
      duration: [],
      pollType: null,
    });
    if (searchRef?.current) {
      let inputValue = searchRef.current.state
      inputValue.value = "";
    };
  }



  const getPollsFromDb = async (offset: number = 1, from: string = "initial") => {
    await getPollsApi(
      {
        start: ((offset * (pagination?.pageSize ?? 0)) - (pagination?.pageSize ?? 0)),
        length: pagination?.pageSize,
        status: pollsFilter.status,
        team: pollsFilter.team,
        duration: pollsFilter.duration,
        pollType: pollsFilter.pollType,
        search: searchInput
      }).then((result) => {
        setPagination({
          ...pagination,
          current: from === "initial" ? 1 : offset,
          total: result?.data?.data?.allTeamPolls?.totalCount
        });
        setPolls(result?.data?.data?.allTeamPolls?.nodes);
        setPollsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        toast.error(`Not able to load the polls. Please try again!`);
      })
  }

  const onChangeTable = (newPagination: TablePaginationConfig) => {
    setPollsLoading(true);
    const { current = 1 } = newPagination;
    let offset = current;
    let from = "pagination"
    if (pagination?.pageSize !== newPagination?.pageSize) {
      offset = 1;
      from = ""
    }
    getPollsFromDb(offset, from)
  }

  const onRow = (poll: Poll, rowIndex: number) => {
    history.push(`/polls/${poll.teamId}/${poll.id}`)
  }

  React.useEffect(() => {
    setPollsLoading(true);
    getPollsFromDb();
  }, [pollsFilter, searchInput]);

  React.useEffect(() => {
    if (!isTeamUser) {
      getAllTeams();
    }
  },[])


  return (
    <PollsManagementWrapper>
      <TitleCta
        title="Polls"
        cta={
          <Link to="/poll/add">
            <Button icon={<UserAddOutlined />} className="add-new-button">Add New Poll</Button>
          </Link>
        }
      />
      <Row gutter={[12, 0]} align="bottom" style={{ marginBottom: "44px" }}>
        <Col span={4}>
          <Label>Status</Label>
          <Select placeholder="Select Status" allowClear value={pollsFilter["status"]} onChange={(value) => { handleFilterChange(value, "status") }}>
            <Option value="BLANK">BLANK</Option>
            <Option value="SAVED">SAVED</Option>
            <Option value="SUBMITTED">SUBMITTED</Option>
            <Option value="PUBLISHED">PUBLISHED</Option>
            <Option value="SUSPENDED">SUSPENDED</Option>
            <Option value="CLOSED">CLOSED</Option>
          </Select>
        </Col>
        <Col span={5}>
          <Label>Team</Label>
          <Select
            disabled={isTeamUser}
            placeholder="Select team" allowClear value={pollsFilter["team"]}
            onChange={(value) => { handleFilterChange(value, "team") }}>
            {teams?.team.map((team) => {
              return (
                <Option key={team.id} value={team.id}>{team.name}</Option>
              )
            }
            )}
          </Select>
        </Col>
        <Col span={7}>
          <Label>Duration</Label>
          <RangePicker value={pollsFilter["duration"] as any} onChange={(value) => {
            if (value) {
              handleFilterChange([value[0], value[1]], "duration")
            } else {
              handleFilterChange(null, "duration")
            }
          }} />
        </Col>
        <Col span={4}>
          <Label>Poll Type</Label>
          <SelectElement
            value={pollsFilter["pollType"]}
            allowClear
            onChange={(value) => {
              handleFilterChange(value as unknown as string, "pollType")
            }}
            options={pollTypes?.teamPollType ?? []}
            toFilter="type"
            placeholder="Poll Type"
          />
        </Col>
        <Col span={2}>
          <Button type='primary' size='middle' danger ghost onClick={clearFilter} disabled={Object.entries(pollsFilter).some((filter: any) => filter[1] && filter[1].length > 0) || searchInput.length > 0 ? false : true} icon={<CloseOutlined />}>Clear Filter</Button>
        </Col>
      </Row>
      <Card>
        <TitleCta
          title={`List of Polls (${pagination?.total ?? "0"})`}
          cta={
            <Input size="middle" type="text"
              className='search'
              ref={searchRef}
              // value={searchInput}
              allowClear
              prefix={<SearchOutlined />}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => handleSearch(e.target.value)}
              placeholder={"Search by poll name or id"}
            />
          }
        />
        <CustomTable
          columns={pollsColumns}
          dataSource={polls}
          loading={pollsLoading}
          pagination={pagination}
          onChangeTable={onChangeTable}
          // sorter
          onRowClick={onRow}
        />
      </Card>
    </PollsManagementWrapper >
  );
};

export default PollsManagement;
