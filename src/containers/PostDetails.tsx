import styled from 'styled-components/macro';
import { Breadcrumb, Card, Col, Divider, Row } from 'antd';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import Text from 'antd/lib/typography/Text';
import { ReactComponent as MoreIcon } from 'assets/icons/more.svg';
import { Link, useParams } from 'react-router-dom';
import {
  useGetPostByIdQuery,
  useGetTransactionByIdQuery,
} from 'generated/graphql';
import { format } from 'date-fns/esm';
import LabelValue from 'components/common/LabelValue';
import Title from 'antd/lib/typography/Title';
import PostCard from 'components/common/cards/PostCard';

const PostDetailsWrapper = styled.div`
  padding: 40px;
  width: 100%;

  .t-transactions {
    font-size: 25px;
    line-height: 30px;
    color: rgba(5, 31, 36, 0.3);
  }
  .t-transaction-id {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .ant-card {
    margin-top: 40px;
    width: 100%;
    .info-edit-wrapper {
      margin-bottom: 40px;
    }
    .t-info {
      font-weight: 600;
      font-size: 20px;
      line-height: 24px;
      color: #051f24;
    }
    .t-transaction-key-field {
      font-size: 15px;
      line-height: 18px;
      color: rgba(5, 31, 36, 0.6);
    }
    .t-transaction-value {
      font-size: 16px;
      line-height: 19px;
      font-weight: 500;

      color: #051f24;
    }
    .key-value-wrapper {
      row-gap: 10px;
    }
  }
`;

interface PostDetailsProps {}

const PostDetails = ({}: PostDetailsProps): JSX.Element => {
  const { id } = useParams<{ id: string }>();

  const { data: post } = useGetPostByIdQuery({
    variables: { postId: +id },
  });

  return (
    <PostDetailsWrapper>
      <Breadcrumb separator=">">
        <Breadcrumb.Item className="t-transactions">Post</Breadcrumb.Item>
        <Breadcrumb.Item className="t-transaction-id">{id}</Breadcrumb.Item>
      </Breadcrumb>

      <Card>
        <Row className="info-edit-wrapper" justify="space-between">
          <Col className="t-info">Info</Col>
          <Col>
            <Link to={`/post/edit/${id}`}>Edit</Link>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <LabelValue field="Name" value={post?.post_by_pk?.title} />
          </Col>

          <Col span={8}>
            <LabelValue field="ID" value={post?.post_by_pk?.id} />
          </Col>
          <Col span={8}>
            {/* <LabelValue
              field="Status"
              value={transactionData?.transactions_by_pk?.status}
            /> */}
          </Col>
        </Row>
        <Divider />
        <Row>
          <Col span={8}>
            <LabelValue field="Team" value={post?.post_by_pk?.team?.name} />
          </Col>
          <Col span={8}>
            <LabelValue
              field="Published At"
              value={
                post?.post_by_pk?.publishedAt &&
                format(new Date(post?.post_by_pk?.publishedAt), 'Pp')
              }
            />
          </Col>
          <Col span={8}>
            <LabelValue field="Views" value={post?.post_by_pk?.viewsCount} />
          </Col>
        </Row>
        <Divider />
        <Row>
          {/* <Col span={8}>
            <LabelValue
              field="Shares"
              value={`${transactionData?.transactions_by_pk?.fromCoinAmount}
              ${transactionData?.transactions_by_pk?.fromCoin}`}
            />
          </Col>

          <Col span={8}>
            <LabelValue
              field="To"
              value={`${transactionData?.transactions_by_pk?.toCoinAmount}
              ${transactionData?.transactions_by_pk?.toCoin}`}
            />
          </Col> */}
        </Row>
      </Card>

      <Card>
        <Title level={3}>Message</Title>
        <LabelValue
          field="Message"
          value={
            <div
              dangerouslySetInnerHTML={{ __html: post?.post_by_pk?.description ?? '' }}
            />
          }
        />
      </Card>

      <Card>
        <LabelValue
          field="Preview"
          value={
            <PostCard
              teamName={post?.post_by_pk?.team?.name ?? ''}
              teamProfileImage={post?.post_by_pk?.team?.profileImageURL ?? ''}
              postCreationDate={post?.post_by_pk?.publishedAt}
              body={post?.post_by_pk?.description ?? ''}
              postBannerImageUrl={post?.post_by_pk?.postUrl ?? ''}
            />
          }
        />
      </Card>
    </PostDetailsWrapper>
  );
};

export default PostDetails;
