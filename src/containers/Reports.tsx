import React, { useCallback, useState } from 'react';
import { Row, Col, Typography, DatePicker, Card, Space, Button, TablePaginationConfig, Form, Tooltip } from 'antd';
import styled from 'styled-components/macro';
import LabelInput from 'components/common/LabelInput';
import moment from 'moment';
import SelectElement from 'components/common/SelectElement';
import { ReloadOutlined } from '@ant-design/icons'
import CustomTable from 'components/customTable';
import { generateReportApi } from 'apis/report';
import { GenerateReportsInterface, ReportsType } from 'ts/interfaces/report';
import { toast } from 'react-toastify';
import { useGetAllContestListLazyQuery, useGetAllMasterReportTypesQuery, useGetAllPollListLazyQuery, useGetAllReportsQuery } from 'generated/pgraphql';
import { formatDate } from './users/utils';

const Wrapper = styled('div')`
width:100%;
padding:40px;
background-color: #F5F5F7;
    .reports{
        align-items: center;
        border-radius:7px;
    }
    .card-wrap{
        margin-top: 16px;
        & .ant-card-bordered{
            border-radius: 10px;
            margin-bottom: 16px;
            & .edit-icon{
                & path{
                    fill: #4DA1FF;
                }
            }
            & .delete-icon{
                & path{
                    fill: red;
                }
            }
        }
        .space-div{
            width:100%;
            & .ant-space-item:first-child{
                flex:1;
            }
            & .ant-col{
                & .ant-select-selector{
                    border-radius: 2px;
                    border: 1px solid #d9d9d9;
                }
            }
        }
    }       
    .header{
        color:#051F24;
        font-weight: 600;
        font-size: 20px;
        margin-bottom: 24px;
        }
    .table-section{
        margin-bottom: 24px;
        justify-content: space-between;
        width:100%;
        & .header{
            margin-bottom: 0px;
            flex:1;
        }
        & .anticon-reload{
            cursor:pointer;
            font-size:20px;
        }
    }
    .generate-report{
        margin-block:10px;
    }
`;


interface TableDataType {
    index: number;
    startDateTime: string;
    endDateTime: string;
    reportType: string;
    fileName: string;
    status: string;
}

type allMasterReportOptionType = {
    description: string;
    name: string;
    id: number;
    __typename: number;
}[];

enum reportStatusColor {
    "COMPLETED" = "#10B981",
    "INPROGRESS" = "#0075FF",
    "NOT COMPLETED" = "#ff2200",
    "STARTED" = "#ffd500",
}

type reportStatusType = keyof typeof reportStatusColor;

const tableColumns = [
    { title: 'SI.No', dataIndex: "index", },
    { title: 'Title', dataIndex: "title", },
    {
        title: 'Start Date Time',
        dataIndex: "startDateTime",
        valueFormat: (value: string) => value ? `${formatDate(value,"DD MMM YYYY, hh:mm:ss a")}` : '-',
        sorter: (a: any, b: any) => (a.startDateTime && (b.startDateTime)) ? moment(a.startDateTime).unix() - moment(b.startDateTime).unix() : 0
    },
    {
        title: 'End Date Time',
        dataIndex: "endDateTime",
        valueFormat: (value: string) => value ? `${formatDate(value,"DD MMM YYYY, hh:mm:ss a")}` : '-',
        sorter: (a: any, b: any) => (a.endDateTime && (b.endDateTime)) ? moment(a.endDateTime).unix() - moment(b.endDateTime).unix() : 0
    },
    { title: 'Report Type', dataIndex: "reportType" },
    {
        title: 'URL',
        dataIndex: "fileName",
        valueFormat: (value: string) => {
            if (!value) return <Button type="text">-</Button>
            return (
                <Tooltip
                    placement="top"
                    title={`Click to download the report ${value}`}
                >
                    <Button type="link" href={value}>{value?.length > 20 ? value?.slice(0, 20)?.concat('...') : value}</Button>
                </Tooltip>
            )
        },
    },
    {
        title: 'Status',
        dataIndex: "status",
        valueFormat: (value: reportStatusType) => {
            return <Typography.Text style={{ color: reportStatusColor[value], fontSize: 14 }}>{value}</Typography.Text>
        }
    },
]

let pageSize = 5;

const initialPagination: TablePaginationConfig = {
    current: 1,
    pageSize,
}

interface reloadingType {
    current: {
        loading: boolean;
    }
}

const Reports = (): JSX.Element => {

    const [form] = Form.useForm();
    //for reload reference
    const reloading: reloadingType = React.useRef({ loading: false });
    //for rerender form state
    const [formData, setFormData] = useState<object>({});
    //for loading
    const [loading, setLoading] = useState<boolean>(false);
    //for table pagination
    const [pagination, setPagination] = useState<TablePaginationConfig>(initialPagination);

    //PostGraphiQl queries

    const [getAllContests, { data: contestList, loading: contestLoading }] = useGetAllContestListLazyQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const [getAllPolls, { data: pollList, loading: pollLoading }] = useGetAllPollListLazyQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const { data: allMasterReportsType, loading: allMasterReportsTypeLoading } = useGetAllMasterReportTypesQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const {
        data: allReportsList,
        loading: allReportsListLoading,
        refetch: refetchAllReportList
    } = useGetAllReportsQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        variables: {
            offset: 0,
            first: 5
        }
    });

    const onValuesChange = (field: any) => {

        if (field?.report) {
            if (field?.report === "CONTEST_REPORT") {
                getAllContests()
                form.setFieldsValue({ contestId: null, })
            }
            else if (field?.report === "POLL_REPORT") {
                getAllPolls()
                form.setFieldsValue({ pollId: null })
            }
        }

        setFormData({ ...form.getFieldsValue(true) })
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => {
        const { current = 1, pageSize = 5 } = newPagination;
        let offset = current;
        if (pagination?.pageSize !== newPagination?.pageSize) {
            offset = 1;
        }
        if (reloading.current.loading) {
            reloading.current = { loading: false }
        }
        setPagination({
            current: offset,
            pageSize
        })
        refetchAllReportList({
            offset: ((offset * pageSize) - pageSize),
            first: pageSize
        })
    }

    const onReloadReports = () => {
        reloading.current = { loading: true }
        const { current = 1, pageSize = 5 } = pagination;
        refetchAllReportList({
            offset: (current * pageSize) - pageSize,
            first: pageSize
        })
        toast.info('Reloading to fetch current status')
    }

    const generateReport = () => {

        form.submit()
        form.validateFields()
            .then(async (res: any) => {

                setLoading(true);
                let payload: GenerateReportsInterface = {
                    startDate: res?.fromDateTime,
                    endDate: res?.toDateTime,
                    ...((res?.report === "CONTEST_REPORT") && { contestId: res?.contestId }),
                    ...((res?.report === "POLL_REPORT") && { pollId: res?.pollId }),
                }

                generateReportApi(payload, res.report)
                    .then((res: any) => {
                        if (res?.data?.statusCode === "500") {
                            catchError(res?.data?.message)
                        }
                        setLoading(false);
                        onChangeTable({
                            current: 1,
                            pageSize: 5
                        })
                        toast.success(res?.data?.message);
                    }).catch((e: any) => catchError(e?.message))

            }).catch(e => {
                console.log(e)
                toast.info('Please fill the mandatory fields!')
            })
    }

    const catchError = (msg: string) => {
        console.log(msg);
        setLoading(false);
        toast.error(msg ? msg : 'Something went wrong!');
        return false
    }

    // decide props by report type
    const getPropsByType = () => {
        let type = form.getFieldValue('report');
        const returnValues = (contest: any, poll: any) => type === "CONTEST_REPORT" ? contest : type === "POLL_REPORT" ? poll : null;
        return {
            label: `Select ${returnValues("Contest", "Poll")}`,
            name: returnValues("contestId", "pollId"),
            rules: {
                message: `${returnValues("Contest", "Poll")} is required!`
            },
            placeholder: `Select ${returnValues("Contest", "Poll")}`,
            loading: returnValues(contestLoading, pollLoading),
            options: returnValues(contestList?.allContests?.nodes, pollList?.allTeamPolls?.nodes)
        }
    }

    //disable start date
    const isDisableStartDate = useCallback((date) => {
        return date && moment(date).isAfter(moment(), 'day')
    }, [form])

    //disable end date
    const isDisableEndDate = useCallback((date) => {
        let fromDateTime = form.getFieldValue('fromDateTime')
        if (!fromDateTime) {
            return true
        }
        return date && (moment(date).isBefore(moment(fromDateTime), 'day') || moment(date).isAfter(moment(), 'day'))
    }, [form])

    //status validation for End Date Time
    const endDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`To Date Time is required!`));
        }
        if (getFieldValue('fromDateTime')) {
            if (!value.isAfter(getFieldValue('fromDateTime'))) {
                return Promise.reject(new Error(`To Date Time should be greater than From Date Time(${getFieldValue('fromDateTime').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    const getTitle = (type: ReportsType, data: any) => {
        if (type === "CONTEST_REPORT") {
            return data?.contestByContestId?.title
        }
        else if (type === "POLL_REPORT") {
            return data?.teamPollByPollId?.title
        }
        return '-'
    }

    const dataSource = React.useMemo(() => {
        let list = allReportsList?.allReports;
        if (list?.totalCount === 0) {
            return []
        }
        const { current = 1, pageSize = 5 } = pagination;

        return list?.nodes?.map((_: any, index: number) => ({
            index: ((current * pageSize) - pageSize) + index + 1,
            startDateTime: _?.startDate,
            endDateTime: _?.endDate,
            reportType: _?.masterReportTypeByTypeId?.description,
            fileName: _?.fileName,
            status: _?.masterReportStatusByStatus?.status,
            title: getTitle(_?.masterReportTypeByTypeId?.name, _)
        }))
    }, [allReportsList])

    return (
        <Wrapper style={{ width: '100%' }}>
            <Typography className='header'>Generate Report</Typography>
            <div className='card-wrap'>
                <Card>
                    <Form
                        form={form}
                        preserve={false}
                        onValuesChange={onValuesChange}
                    >
                        <Space size={24} align="end" className="space-div">
                            <Row gutter={[24, 24]} >
                                <Col xs={24} sm={12} md={6}>
                                    <LabelInput
                                        label="Select Report"
                                        name="report"
                                        className="label-input"
                                        required
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Report is required!',
                                            },
                                        ]}
                                        inputElement={
                                            <SelectElement
                                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                placeholder="Select Report"
                                                searchable={true}
                                                toFilter="description"
                                                toStore='name'
                                                disabled={loading}
                                                loading={allMasterReportsTypeLoading}
                                                options={allMasterReportsType?.allMasterReportTypes?.nodes as unknown as allMasterReportOptionType ?? []}
                                            />}
                                    />
                                </Col>
                                {
                                    ["CONTEST_REPORT", "POLL_REPORT"]?.includes(form.getFieldValue('report')) &&
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label={getPropsByType().label}
                                            name={getPropsByType().name}
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: getPropsByType().rules.message,
                                                },
                                            ]}
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                    placeholder={getPropsByType().placeholder}
                                                    searchable={true}
                                                    toFilter="title"
                                                    toStore='id'
                                                    disabled={loading}
                                                    loading={getPropsByType().loading}
                                                    options={getPropsByType().options ?? []}
                                                />}
                                        />
                                    </Col>
                                }
                                <Col xs={24} sm={12} md={6}>
                                    <LabelInput
                                        label="From Date & Time"
                                        name="fromDateTime"
                                        className="label-input"
                                        required
                                        rules={[
                                            {
                                                required: true,
                                                message: 'From Date & Time is required!',
                                            },
                                        ]}
                                        inputElement={
                                            <DatePicker
                                                format={"DD-MM-YYYY hh:mm:ss"}
                                                showTime
                                                style={{ width: '100%' }}
                                                disabled={loading}
                                                disabledDate={(value) => isDisableStartDate(value)}
                                            />
                                        }
                                    />
                                </Col>
                                <Col xs={24} sm={12} md={6}>
                                    <LabelInput
                                        label="To Date & Time"
                                        name="toDateTime"
                                        className="label-input"
                                        required
                                        rules={
                                            [
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return endDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                        inputElement={
                                            <DatePicker
                                                format={"DD-MM-YYYY hh:mm ss"}
                                                showTime={Boolean(form.getFieldValue('fromDateTime'))}
                                                style={{ width: '100%' }}
                                                disabled={loading}
                                                disabledDate={(value) => isDisableEndDate(value)}
                                            />
                                        }
                                    />
                                </Col>
                            </Row>
                            <Col xs={24} sm={12} md={6}>
                                <Button
                                    type="primary"
                                    className='generate-report'
                                    onClick={generateReport}
                                    loading={loading}
                                    disabled={loading}
                                >
                                    Generate Report
                                </Button>
                            </Col>
                        </Space>
                    </Form>
                </Card>
                <Card>
                    <Space className='table-section' size={24}>
                        <Typography className='header'>Reports ({allReportsList?.allReports?.totalCount ?? 0})</Typography>
                        <Tooltip placement="topRight" title={'Click to get the current status of the report'}>
                            <ReloadOutlined spin={allReportsListLoading && reloading?.current?.loading} onClick={onReloadReports} />
                        </Tooltip>
                    </Space>
                    <Space>

                    </Space>
                    <CustomTable
                        columns={tableColumns}
                        dataSource={dataSource as TableDataType[] | [] ?? []}
                        loading={allReportsListLoading}
                        pagination={{
                            ...pagination,
                            total: allReportsList?.allReports?.totalCount ?? 0
                        }}
                        onChangeTable={onChangeTable}
                    />
                </Card>
            </div>
        </Wrapper>
    )
}

export default Reports