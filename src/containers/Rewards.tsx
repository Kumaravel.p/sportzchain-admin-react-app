import React, { useEffect, useState } from 'react';
import { Row, Col, Input, Button, Space, Typography, Card } from 'antd';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import styled from 'styled-components/macro';
import CustomTable from 'components/customTable'
import type { TablePaginationConfig } from 'antd/lib/table';
import SelectElement from 'components/common/SelectElement';
import { useGetRwrdActivitiesLazyQuery, useGetRwrdCategoriesQuery } from 'generated/pgraphql';
import { toast } from 'react-toastify';
import { useDebounce } from 'hooks/useDebounce';
import { CloseOutlined, SearchOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { rewardListing } from 'apis/reward';

const ListingSTOWrapper = styled('div')`
    width:100%;
    padding:32px;
    background-color:#F5F5F7;
    ${FlexRowWrapper}{
        align-items:center;   
        margin-bottom:32px;
        gap:16px;
        .title{
            color: #051f24;
            font-weight: 600;
            font-size: 25px;
            flex:1;
            margin:0;
        }
        .create-rewards-btn{
            display:flex;
            align-items:center;
            color:#07697D;
            border-color:#07697D;
            border-radius: 5px;
            font-weight: 500;
            font-size: 16px;
            gap: 12px;
            height: 100%;
            padding: 10px;
            & span{
                line-height:1;
            }
        }
    }
    .ant-space{
        width:100%;
        margin-bottom:32px;
        & .ant-space-item:first-child{
            flex:1;
        }
        & .ant-typography{
            color:#051F2499;
            font-weight: 400;
            font-size: 14px;
            margin-bottom:8px
        }
        & .ant-col{
            & .ant-input,.ant-picker{
                border-radius: 5px;
                border: 1px solid #97a0c3;
            }
        }
        & .clear-filter{
            color: #FF4D4A;
            border-color: #FF4D4A;
            border-radius: 5px;
            & .anticon{
                font-size:12px;
            }
        }
    }
    .ant-card-body{
        padding:24px;
        padding-left:8px;
        ${FlexRowWrapper}{
            margin-bottom:16px;
            & .title{
                font-size:18px;
                padding-left:16px
            }
            & .search-input{
                border: 1px solid rgba(5, 31, 36, 0.1);
                border-radius: 5px;
                & .ant-input-prefix{
                color:#97a0c3;
            }
            }
        }
    }
`;

interface ListingProps { }

interface TableDataType {
    status: string;
    teamName: string;
    tokenName: string;
    round: string;
    timePeriod: string;
    offering: string;
    price: string;
    subscribed: string;
}

const toLowerCase = (val: string) => val?.toLowerCase()?.trim();

const tableColumns = [
    { title: 'Category', dataIndex: "category", component: 'Category', sorter: (a: any, b: any) => toLowerCase(a.category).localeCompare((b.category)) },
    { title: 'Activity', dataIndex: "activity", sorter: (a: any, b: any) => toLowerCase(a.activity).localeCompare((b.activity)) },
    { title: 'Rule Name', dataIndex: "ruleName", sorter: (a: any, b: any) => toLowerCase(a.ruleName).localeCompare((b.ruleName)) },
    { title: 'Reward', dataIndex: "reward", sorter: (a:any, b:any) => a.reward - b.reward },
    { title: 'User Adoption', dataIndex: "userAdoption", sorter: (a: any, b: any) => toLowerCase(a.userAdoption).localeCompare((b.userAdoption)) },
]

let initialState = {
    search: '',
    category: null,
    activity: null
}

type RwrdActivitiesOptionsType = {
    type:string
    id:number
    __typename: string
  }[];

  type RwrdCategoriesOptionType = {
    type:string
    id:number
  }[];

let pageSize = 5;

const Rewards = ({ }: ListingProps): JSX.Element => {

    //debounce hook
    const debounce = useDebounce();
    const history = useHistory();

    const [pagination, setPagination] = React.useState<TablePaginationConfig>({
        current: 1,
        pageSize,
        // total: 6,
    });

    const [state, setState] = React.useState<any>(initialState);
    const [loading, setLoading] = useState<any>(null);
    const [dataSource, setDataSource] = useState<TableDataType[]>([]);

    // get all Rewards Activitie
    const [getActivites, { data: RwrdActivitiesOption, loading: RwrdActivitiesOptionLoading }] = useGetRwrdActivitiesLazyQuery({
        fetchPolicy:"network-only",
        notifyOnNetworkStatusChange:true
    });
    // get all Rewards Categories
    const {data:RwrdCategoriesOption,loading:RwrdCategoriesOptionLoading} = useGetRwrdCategoriesQuery({
        fetchPolicy:"network-only",
        notifyOnNetworkStatusChange:true
    });

    useEffect(() => {
        getAllRewards()
    }, []);

    const onSearchTableList = (value: string) => {
        const { activity, category } = state;
        onChangeState('search', value)
        debounce(() => getAllRewards(1, pageSize, value, activity, category), 800);
    }

    const onChangeActivities = (value: any) => {
        const { search, category } = state;
        onChangeState('activity', value)
        getAllRewards(1, pageSize, search, value, category)
    }

    const onChangeCategory = (value: any) => {
        const { search } = state; 
        getActivites({
            variables: {
                categoryId: value?.value
            }
        })
        setState({
            ...state,
            category:value,
            activity:null
        })
        
        getAllRewards(1, pageSize, search, null, value)
    }

    //to clear filter
    const clearFilters = () => {
        setState(initialState);
        getAllRewards();
    }

    // get all Rewards
    const getAllRewards = (offset: number = 1, limit: number = pageSize, searchText: string = "", activity:any = null, category:any = null, from: string = "initial") => {
        setLoading('table');
        
        let payload: any = {
            "start": ((offset * limit) - limit),
            "length": limit,
            "searchText": searchText,
            "category": category?.label ?? null,
            "activity": activity,
        }
        
        rewardListing(payload).then(res => {
                let rewards = res?.data?.data;
                let updateSto;
                if (rewards?.allRwrds?.nodes?.length) {
                    updateSto = rewards?.allRwrds?.nodes?.map((rewards: any) => {
                        return(
                            {
                                category: rewards?.masterRwrdCategoryByCategoryId?.category,
                                activity: rewards?.masterRwrdActivityByActivityId?.description,
                                ruleName: rewards?.rwrdRulesByRwrdId?.nodes[0]?.ruleName,
                                reward: parseInt(rewards?.rwrdRulesByRwrdId?.nodes[0]?.totalQuantity),
                                userAdoption: "",
                                id: rewards?.id
                            }
                        )    
                    })
                }
                setDataSource(updateSto);
                setPagination({
                    ...pagination,
                    current: from === "initial" ? 1 : offset,
                    pageSize: limit,
                    total: rewards?.allRwrds?.totalCount
                })
                setLoading(null)
            }).catch(err => {
                console.log(err);
                setLoading(null)
                toast.error('Something went wrong!')
            });
    }

    const onChangeState = (key: string, value: any) => {
        setState({
            ...state, [key]: value
        })
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => {
        const { search, activity, category } = state;
        const { current = 1, pageSize } = newPagination;
        let offset = current;
        let from = "pagination"
        if (pagination?.pageSize !== newPagination?.pageSize) {
            offset = 1;
            from = ""
        }
        getAllRewards(offset, pageSize, search, activity, category, from)
    }

    const onRow = (record: any, rowIndex: number) => {
        history.push('/view-rewards', {id:record?.id});
    }

    return (
        <ListingSTOWrapper>
            <FlexRowWrapper>
                <p className='title'>Rewards</p>
                <Button className='create-rewards-btn' onClick={() => history.push('/create-reward')}>
                    <AddPersonIcon /> Add New Reward
                </Button>
            </FlexRowWrapper>
            <Space size={24} align="end">
                <Row gutter={[24, 24]}>
                <Col xs={24} sm={8} md={6}>
                        <Typography>Category</Typography>
                        <SelectElement
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeCategory(e)}
                            value={state?.category ?? null}
                            options={RwrdCategoriesOption?.allMasterRwrdCategories?.nodes as unknown as RwrdCategoriesOptionType ?? []}
                            placeholder="Category"
                            searchable={true}
                            toFilter="category"
                            loading={RwrdCategoriesOptionLoading}
                            labelInValue
                            toStore='id'
                        />
                    </Col>
                    <Col xs={24} sm={8} md={6}>
                        <Typography>Activity</Typography>
                        <SelectElement
                            onChange={(e: any) => onChangeActivities(e)}
                            value={state?.activity ?? null}
                            options={state?.category?.value ? RwrdActivitiesOption?.allMasterRwrdActivities?.nodes as unknown as RwrdActivitiesOptionsType ?? [] : []}
                            placeholder="Activity"
                            searchable={true}
                            toFilter="description"
                            loading={RwrdActivitiesOptionLoading}
                        />
                    </Col>
                </Row>
                <Button onClick={clearFilters} className='clear-filter'>
                    <CloseOutlined />
                    Clear Filter
                </Button>
            </Space>
            <Card>
                <FlexRowWrapper>
                    <p className='title'>List of rewards ({pagination?.total})</p>
                    <Col xs={24} sm={8} md={6}>
                        <Input
                            type="text"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onSearchTableList(e.target.value)}
                            value={state?.search}
                            autoComplete={"off"}
                            placeholder="Search"
                            className="search-input"
                            prefix={<SearchOutlined />}
                        />
                    </Col>
                </FlexRowWrapper>
                <CustomTable
                    columns={tableColumns}
                    dataSource={dataSource}
                    loading={loading === "table"}
                    pagination={pagination}
                    onChangeTable={onChangeTable}
                    onRowClick={onRow}
                />
            </Card>
        </ListingSTOWrapper>
    );
};

export default Rewards;

