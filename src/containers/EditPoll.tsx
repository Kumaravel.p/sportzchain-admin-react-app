import React from 'react';
import AddPoll from 'components/poll/AddPoll';
import { useParams } from 'react-router-dom';
import {
    useGetPollQuery,
} from 'generated/graphql';
import { Spin, Row } from 'antd';
import { v4 as uuidv4 } from 'uuid';

const EditPoll = () => {
    const { id } = useParams<{ id: string }>();
    const { data: pollData, loading, refetch: refetchPolls } = useGetPollQuery({
        variables: { id: parseInt(id) }
    });

    React.useEffect(() => {
        refetchPolls();
    }, [])
    return (
        <Row style={{ width: "100%" }}>
            {!loading && pollData ?
                <AddPoll pollFormData={pollData?.teamPoll_by_pk} /> :
                <div style={{ width: "100%", display: "grid", placeItems: "center" }}> <Spin /></div>
            }
        </Row>
    )

}

export default EditPoll