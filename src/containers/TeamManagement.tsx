
import { Button, Card } from 'antd';
import * as React from 'react';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import UserInfo from 'components/user-management/UserInfo';
import styled from 'styled-components/macro';
import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import TeamInfo from 'components/team/TeamInfo';
import { useGetTeamsForIdLazyQuery, useGetTeamsForIdQuery, useGetTeamsLazyQuery, useGetTeamsQuery } from 'generated/graphql';
import { Link } from 'react-router-dom';
import { TeamSelectionContext } from 'App';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const TeamManagementWrapper = styled.div`
  width: 100%;
  padding: 40px;
  .ant-card {
    width: 303px;
    .ant-card-head {
      border-bottom: none;
    }
  }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-user-management {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
    .ant-btn.add-new-button {
      border: 1px solid rgba(7, 105, 125, 0.25);
      color: rgba(7, 105, 125, 1);
      box-sizing: border-box;
      border-radius: 5px;
      height: fit-content;
      padding: 15px;
      display: flex;
      align-items: center;
      column-gap: 10px;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */

    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      /* Red / 1 */
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }

  .t-month {
    font-size: 14px;
    line-height: 19px;
    display: flex;
    align-items: flex-end;
    letter-spacing: -0.1px;
    color: rgba(50, 60, 71, 0.4);
  }

  .count-monthly-wrapper {
    justify-content: space-between;
    align-items: center;
  }

  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
`;

interface TeamManagementProps { }

const TeamManagement = ({ }: TeamManagementProps): JSX.Element => {
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  let teamsData: any;
  let [myTeamRefetch, { data: myTeam }] = useGetTeamsForIdLazyQuery({
    variables: { teamId: teamSelectionContext.team.id }
  })
  let [allTeamRefetch, { data: allTeam }] = useGetTeamsLazyQuery();
  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }
  const isTeamUser = isIamTeamUser();
  if (isTeamUser) {
    teamsData = myTeam;
  } else {
    teamsData = allTeam;
  }
  const [getdata, setGetata] = React.useState<any>([]);
  const [original, setOriginal] = React.useState<any>([]);


  let handleChange = (value: string, type: string) => {
    let filteredVal = [];
    if (type === "SportsType") {
      filteredVal = getdata.filter((val: any) => val?.categoryId === value);
      setGetata(filteredVal);
    } else if (type === "currency") {
      filteredVal = getdata.filter((val: any) => val?.currencyTypeId === value);
      setGetata(filteredVal);
    } else if (type === "name") {
      if (value) {
        filteredVal = getdata.filter((val: any) => val?.name.toLowerCase().includes(value.toLocaleLowerCase()));
        setGetata(filteredVal);
      } else {
        setGetata(original);
      }
    } else if (type === "Status") {
      filteredVal = getdata.filter((val: any) => val.teamStatus.status === value);
      setGetata(filteredVal);
    }
  }
  const funClearFlitter = () => {
    setGetata(original);
  }
  React.useEffect(() => {
    if (isTeamUser) {
      myTeamRefetch();
    } else {
      allTeamRefetch();
    }
  }, []);

  React.useEffect(() => {
    setGetata(teamsData?.team);
    setOriginal(teamsData?.team);
  }, [teamsData])

  return (
    <TeamManagementWrapper>
      <FlexRowWrapper className="user-text-create-wrapper">
        <Text className="t-user-management">Team Management</Text>
        {!isIamTeamUser() && <Link to="/team/add">
          <Button className="add-new-button">
            <AddPersonIcon /> Add New Team
          </Button>
        </Link>}
      </FlexRowWrapper>
      <TeamInfo teamsData={getdata} handleChange={handleChange} clearFlitter={funClearFlitter} />
    </TeamManagementWrapper>
  );
};

export default TeamManagement;
