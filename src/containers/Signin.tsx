import * as React from 'react';
import styled from 'styled-components/macro';
import { Link, useParams, useHistory } from 'react-router-dom';
import { useForm, FormProvider } from 'react-hook-form';
import * as Yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { toast } from 'react-toastify';
import { AccountContext } from 'context/AccountContext';

import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import FormInput from 'components/form-elements/FormInput';
import Spinner from 'components/Spinner';

// import CheckIcon from 'assets/icons/check.svg';
import MobileIcon from 'assets/icons/mobile.svg';
import EmailIcon from 'assets/icons/email.svg';

import PasswordLockIcon from 'assets/icons/password-lock.svg';
import { ReactComponent as EyeOpenIcon } from 'assets/icons/eye-open.svg';
import { ReactComponent as EyeClosedIcon } from 'assets/icons/eye-close.svg';
import currentSession from 'utils/getAuthToken';
import { fetchUserMeta } from 'apis/profile';

const SignInWrapper = styled.div`
  background: #fff;
  width: 100%;
  height: 100vh;
  color: #000000;
  .t-welcome-back {
    font-weight: 600;
    font-size: 40px;
    line-height: 56px;
    letter-spacing: -0.89px;
    color: #151516;
  }
  .t-dont-have-account {
    font-size: 18px;
    line-height: 28px;
    letter-spacing: -0.26px;
    color: #737373;
    white-space: nowrap;
    span {
      color: #f57105;
    }
    a {
      text-decoration: none;
      color: #fff;
    }
  }
  .signin-form-wrapper {
    padding: 100px 150px;
    width: 45%;
    row-gap: 30px;
  }

  .login-button {
    background: #fb923c;
    border-radius: 4px;
    width: 100%;
    font-weight: bold;
    border: none;
    cursor: pointer;
    padding: 16px;

    display: flex;
    align-items: center;
    justify-content: center;
  }
  .t-or {
    align-items: center;
    color: #525252;
    font-weight: 600;
  }
  .divider {
    height: 2px;
    width: 50%;
    background: #171717;
  }
  .signin-with-google-button {
    display: flex;
    cursor: pointer;
    align-items: center;
    font-weight: bold;

    background: transparent;
    color: #a3a3a3;
    border: 1px solid #a3a3a3;
    border-radius: 4px;
    width: 100%;
    padding: 12px;
    justify-content: center;
    outline: none;
    column-gap: 10px;
  }
  .right-image {
    background: #eee;
    width: 55%;
    height: 100vh;
  }

  .t-forgot-password {
    text-align: center;
    font-size: 14px;
    line-height: 20px;
    letter-spacing: -0.09px;
    color: #a3a3a3;
    cursor: pointer;
  }
  .signup-options-wrapper {
    background: #262626;
    border-radius: 4px;
    padding: 4px;
    width: fit-content;
    .t-signup-option {
      font-size: 16px;
      line-height: 24px;
      letter-spacing: -0.18px;
      padding: 4px 14px;
      color: #a3a3a3;
      cursor: pointer;
      &.active {
        color: #171717;
        background: #fb923c;
        border-radius: 4px;
      }
    }
  }
`;

const SignIn = (): JSX.Element => {
  const [showPassword, setShowPassword] = React.useState(false);
  const [isSigningIn, setIsSigningIn] = React.useState(false);
  const [userDetails, setUserDetails] = React.useState<any>(null);

  const { authenticate } = React.useContext(AccountContext);

  const history = useHistory();
  const { mode } = useParams<{ mode: 'email' | 'mobile' }>();

  const initialValues = React.useMemo(
    () => ({
      email: '',
      phoneNumber: '',
      password: '',
    }),
    []
  );

  const schema = React.useMemo(() => {
    if (mode === 'email') {
      return Yup.object()
        .shape({
          email: Yup.string().email().required(),
          phoneNumber: Yup.string().optional(),
          password: Yup.string().required(),
        })
        .required();
    }
    return Yup.object()
      .shape({
        email: Yup.string().email().optional(),
        phoneNumber: Yup.string().required(),
        password: Yup.string().required(),
      })
      .required();
  }, [mode]);

  const methods = useForm({
    defaultValues: initialValues,
    resolver: yupResolver(schema),
  });

  React.useEffect(() => {
    if (!mode) {
      history.replace('/signin/email');
    }
  }, [mode, history]);

  React.useEffect(() => {
    if (mode === 'email') {
      methods.setValue('phoneNumber', '');
      methods.setValue('password', '');
    } else {
      methods.setValue('email', '');
      methods.setValue('password', '');
    }
  }, [mode, methods]);
  const userDetail = (sub:any) => {
    let data = fetchUserMeta({remoteId: sub}).then(res => {
      return res?.data?.data;
    })
    return data
  }

  const onSubmit = async (formData: typeof initialValues) => {
    setIsSigningIn(true);
    if (mode === 'email') {
      authenticate(formData.email, formData.password)
        .then(async(data: any) => {

          if (data.challengeName === "NEW_PASSWORD_REQUIRED") {
            history.push({
              pathname: '/change-password',
              state: {
                // mode,
                email: formData.email,
                password: formData.password
              }
            });
            return false;
          }
          let response:any = await userDetail(data?.signInUserSession?.idToken?.payload?.sub);
          console.log(response)
          // state.profile = response;
          let teamDetails: any = response?.teamDetail;
          let platformRole = response?.role;
          let teamRole = teamDetails?.team_roles?.map((role: string) => role.toLowerCase()) ?? [];
          const isTeamUser = teamDetails?.teams?.length > 0;
          if ([platformRole, ...teamRole].includes('admin') || [platformRole, teamRole].includes('pro')) {
            localStorage.setItem('token', data?.signInUserSession?.idToken?.jwtToken);
            methods.reset();
            window.location.replace("/");
          } else {
            toast.error('Only Super Admins/Team Users can login');
          }
        })
        .catch((err: any) => {
          console.error('Failed to login', err);
          toast.error(err.message);
        })
        .finally(() => {
          setIsSigningIn(false);
        });
    } else {
      await authenticate(formData.phoneNumber, formData.password)
        .then(async(data: any) => {
          let response:any = await userDetail(data?.signInUserSession?.idToken?.payload?.sub);

          console.log(response?.role, 'log');
          if (response?.role === 'admin') {
            // state.profile = response;
            localStorage.setItem('token', data?.signInUserSession?.idToken?.jwtToken);

            methods.reset();
            history.replace('/');
          } else {
            toast.error('Only Super Users can login');
          }
        })
        .catch((err: any) => {
          console.error('Failed to login', err);
          toast.error(err.message);
        })
        .finally(() => {
          setIsSigningIn(false);
        });
    }
  };

  React.useEffect(() => {
    currentSession().then(async res => {
      const token = res?.getJwtToken();
      if (token) {
          let response:any = await userDetail(res?.payload?.sub);
          let userData = response?.data?.data;
          const teamDetails = userData?.teamDetail;
          const isTeamUser = teamDetails.teams.length > 0;
          debugger
          setUserDetails(userData);
          if (isTeamUser) {
            window.location.replace("/team-home")
          } else {
            window.location.replace("/");
          }
      }
    })
  }, []);

  return (
    <SignInWrapper>
      <FlexRowWrapper>
        <div className="right-image"></div>
        {console.log(methods.formState.errors)}
        <FlexColumnWrapper className="signin-form-wrapper">
          <div className="t-welcome-back">Welcome Back!</div>
          <div className="t-dont-have-account">
            Need an Account? <span>Contact Sportzchain</span>
            <span>
              <Link to="/signup/email">Sign up</Link>
            </span>
          </div>
          <FlexRowWrapper className="signup-options-wrapper">
            <div
              onClick={() => history.push('/signin/email')}
              className={`t-signup-option ${mode === 'email' && 'active'}`}
            >
              Email
            </div>
            <div
              onClick={() => history.push('/signin/mobile')}
              className={`t-signup-option ${mode === 'mobile' && 'active'}`}
            >
              Mobile
            </div>
          </FlexRowWrapper>
          <FormProvider {...methods}>
            <FlexColumnWrapper>
              {/* EMAIL LOGIN */}
              {mode === 'email' && (
                <FormInput
                  className="form-input"
                  label="Email Address"
                  placeholder="Email"
                  name="email"
                  leftIcon={EmailIcon}
                  disabled={isSigningIn}
                />
              )}

              {/* PHONE NUMBER LOGIN */}
              {mode === 'mobile' && (
                <FormInput
                  className="form-input"
                  label="Phone Number"
                  placeholder="Phone Number"
                  name="phoneNumber"
                  leftIcon={MobileIcon}
                  disabled={isSigningIn}
                />
              )}

              <FormInput
                className="form-input"
                name="password"
                label="Password"
                type={showPassword ? 'text' : 'password'}
                placeholder="*********"
                disabled={isSigningIn}
                rightIcon={
                  showPassword
                    ? () => (
                      <EyeOpenIcon
                        className="eye-icon"
                        onClick={() =>
                          setShowPassword((prevState) => !prevState)
                        }
                      />
                    )
                    : () => (
                      <EyeClosedIcon
                        className="eye-icon"
                        onClick={() =>
                          setShowPassword((prevState) => !prevState)
                        }
                      />
                    )
                }
                leftIcon={PasswordLockIcon}
              />
            </FlexColumnWrapper>
          </FormProvider>

          <button
            className="login-button"
            onClick={methods.handleSubmit(onSubmit)}
            disabled={isSigningIn}
          >
            {isSigningIn ? <Spinner /> : null}&nbsp;&nbsp;
            {isSigningIn ? 'Logging In...' : 'Login now'}
          </button>
          <div className="t-forgot-password" onClick={() => history.push('/forgot-password/email')}>Forgot Password?</div>
        </FlexColumnWrapper>
      </FlexRowWrapper>
    </SignInWrapper>
  );
};

export default SignIn;
