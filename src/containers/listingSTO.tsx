import React, { useEffect, useState } from 'react';
import { Row, Col, Input, Button, DatePicker, Space, Typography, Card } from 'antd';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import styled from 'styled-components/macro';
import CustomTable from 'components/customTable'
import type { TablePaginationConfig } from 'antd/lib/table';
import SelectElement from 'components/common/SelectElement';
import { useGetStoStatusQueryQuery } from 'generated/pgraphql';
import { format } from 'date-fns';
import { toast } from 'react-toastify';
import { useDebounce } from 'hooks/useDebounce';
import { CloseOutlined, SearchOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
// import { axiosInstance } from 'apis/axiosInstance';
import { stoListing } from 'apis/sto';
import { ListingStoProps } from 'ts/interfaces/sto.interface';
import { TeamSelectionContext } from 'App';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const ListingSTOWrapper = styled('div')`
    width:100%;
    padding:32px;
    background-color:#F5F5F7;
    ${FlexRowWrapper}{
        align-items:center;   
        margin-bottom:32px;
        gap:16px;
        .title{
            color: #051f24;
            font-weight: 600;
            font-size: 25px;
            flex:1;
            margin:0;
        }
        .create-sto-btn{
            display:flex;
            align-items:center;
            color:#07697D;
            border-color:#07697D;
            border-radius: 5px;
            font-weight: 500;
            font-size: 16px;
            gap: 12px;
            height: 100%;
            padding: 10px;
            & span{
                line-height:1;
            }
        }
    }
    .ant-space{
        width:100%;
        margin-bottom:32px;
        & .ant-space-item:first-child{
            flex:1;
        }
        & .ant-typography{
            color:#051F2499;
            font-weight: 400;
            font-size: 14px;
            margin-bottom:8px
        }
        & .ant-col{
            & .ant-input,.ant-picker{
                border-radius: 5px;
                border: 1px solid #97a0c3;
            }
        }
        & .clear-filter{
            color: #FF4D4A;
            border-color: #FF4D4A;
            border-radius: 5px;
            & .anticon{
                font-size:12px;
            }
        }
    }
    .ant-card-body{
        padding:24px;
        padding-left:8px;
        ${FlexRowWrapper}{
            margin-bottom:16px;
            & .title{
                font-size:18px;
                padding-left:16px
            }
            & .search-input{
                border: 1px solid rgba(5, 31, 36, 0.1);
                border-radius: 5px;
                & .ant-input-prefix{
                color:#97a0c3;
            }
            }
        }
    }
`;

interface ListingProps { }

interface TableDataType {
    status: string;
    teamName: string;
    tokenName: string;
    // round: { country: string; };
    round: string;
    timePeriod: string;
    offering: string;
    price: string;
    subscribed: string;
}

const tableColumns = [
    { title: 'Status', dataIndex: "status", component: 'status', },
    { title: 'Team Name', dataIndex: "teamName", sorter: (a: any, b: any) => a.teamName.localeCompare(b.teamName) },
    { title: 'Token Name', dataIndex: "tokenName" },
    { title: 'Round', dataIndex: "round" },
    { title: 'Time Period', dataIndex: "timePeriod" },
    { title: 'Offering', dataIndex: "offering" },
    { title: 'Price', dataIndex: "price" },
    { title: 'Subscribed', dataIndex: "subscribed", align: 'right' },
]

let initialState = {
    search: '',
    status: [],
    dateRange: null
}

type StoOptionsType = {
    type: string
    id: number
}[];

let pageSize = 5;

const ListingSTO = ({ }: ListingProps): JSX.Element => {

    //debounce hook
    const debounce = useDebounce();
    const history = useHistory();
    const teamSelectionContext = React.useContext(TeamSelectionContext);

    const [pagination, setPagination] = React.useState<TablePaginationConfig>({
        current: 1,
        pageSize,
        // total: 6,
    });

    const [state, setState] = React.useState<any>(initialState);
    const [loading, setLoading] = useState<any>(null);
    const [dataSource, setDataSource] = useState<TableDataType[]>([]);

    // get all STO status
    const { data: stoStatusOption, loading: stoStatusOptionLoading } = useGetStoStatusQueryQuery();

    useEffect(() => {
        getAllStos()
    }, [])

    const formatDate = (date: Date | number, delimiter: string = "", dateFormat: string = "dd MMM, yy") => date ? format(new Date(date), dateFormat) : delimiter;

    const onSearchTableList = (value: string) => {
        const { status, dateRange } = state;
        onChangeState('search', value)
        debounce(() => getAllStos(1, pageSize, value, status, dateRange), 800);
    }

    const onChangeStatus = (value: any) => {
        const { search, dateRange } = state;
        onChangeState('status', value)
        getAllStos(1, pageSize, search, value, dateRange)
    }

    const onChangeDateRange = (value: any) => {
        const { search, status } = state;
        onChangeState('dateRange', value);
        getAllStos(1, pageSize, search, status, value)
    }

    //to clear filter
    const clearFilters = () => {
        setState(initialState);
        getAllStos()
    }

    // get all STOs
    const getAllStos = (offset: number = 1, limit: number = pageSize, searchText: string = "", statusFilter: number[] = [], dateRangeFilter: Date[] = [], from: string = "initial") => {
        setLoading('table');
        let payload: ListingStoProps = {
            "start": ((offset * limit) - limit),
            "length": limit,
            "searchText": searchText,
            "filterStatus": statusFilter,
            "filterDateRange": {
                "startDate": dateRangeFilter?.[0]?.toISOString(),
                "endDate": dateRangeFilter?.[1]?.toISOString()
            },
            "teamId": teamSelectionContext.team.id
        }

        stoListing(payload)
            .then(res => {
                let allSto = res?.data?.data;
                let updateSto;
                if (allSto?.nodes?.length) {
                    updateSto = allSto?.nodes?.map((sto: any) => {
                        let supplySize = parseInt(sto?.supplySize);
                        let coinSymbol = sto?.teamByTeamId?.walletsByTeamId?.nodes[0] ? sto?.teamByTeamId?.walletsByTeamId?.nodes[0]?.coinSymbol : "";
                        return (
                            {
                                status: sto?.status?.type,
                                teamName: sto?.teamByTeamId?.name,
                                tokenName: coinSymbol,
                                round: sto?.round,
                                timePeriod: `${formatDate(sto?.startDate)} - ${formatDate(sto?.endDate)}`,
                                offering: supplySize + " " + coinSymbol,
                                price: `${parseFloat(sto?.initialPrice)} USD`,
                                subscribed: `${(sto?.totalSold / sto?.supplySize) * 100}%`,
                                id: sto?.id
                            }
                        )
                    })
                }
                setDataSource(updateSto);
                setPagination({
                    ...pagination,
                    current: from === "initial" ? 1 : offset,
                    pageSize: limit,
                    total: allSto?.totalCount
                })
                setLoading(null)
            }).catch(err => {
                console.log(err);
                setLoading(null)
                toast.error('Something went wrong!')
            });
    }

    const onChangeState = (key: string, value: any) => {
        setState({
            ...state, [key]: value
        })
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => {
        const { search, status, dateRange } = state;
        const { current = 1, pageSize } = newPagination;
        let offset = current;
        let from = "pagination"
        if (pagination?.pageSize !== newPagination?.pageSize) {
            offset = 1;
            from = ""
        }
        getAllStos(offset, pageSize, search, status, dateRange, from)
    }

    const onRow = (record: any, rowIndex: number) => {
        history.push('/view-sto', { id: record?.id });
    }
    const { profile } = useSnapshot(ProxyState);
  
    let team:any = profile?.teamDetail
  
    const isIamTeamUser = () => {
      return team?.teams?.length > 0;
    }

    return (
        <ListingSTOWrapper>
            <FlexRowWrapper>
                <p className='title'>STO</p>
                {!isIamTeamUser() && <Button className='create-sto-btn' onClick={() => history.push('/create-sto')}>
                    <AddPersonIcon /> Create STO
                </Button>}
            </FlexRowWrapper>
            <Space size={24} align="end">
                <Row gutter={[24, 24]}>
                    <Col xs={24} sm={8} md={6}>
                        <Typography>Status</Typography>
                        <SelectElement
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeStatus(e)}
                            value={state?.status}
                            options={stoStatusOption?.allMasterStoStatuses?.nodes as unknown as StoOptionsType ?? []}
                            placeholder="Status"
                            searchable={true}
                            toFilter="type"
                            toStore='id'
                            mode="multiple"
                            loading={stoStatusOptionLoading}
                        />
                    </Col>
                    <Col xs={24} sm={8} md={6}>
                        <Typography>Duration</Typography>
                        <DatePicker.RangePicker
                            value={state?.dateRange}
                            onChange={(e: any) => onChangeDateRange(e)}
                            style={{
                                width: '100%'
                            }}
                        />
                    </Col>
                </Row>
                <Button onClick={clearFilters} className='clear-filter'>
                    <CloseOutlined />
                    Clear Filter
                </Button>
            </Space>
            <Card>
                <FlexRowWrapper>
                    <p className='title'>List of STO's ({pagination?.total})</p>
                    <Col xs={24} sm={8} md={6}>
                        <Input
                            type="text"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onSearchTableList(e.target.value)}
                            value={state?.search}
                            autoComplete={"off"}
                            placeholder="Search"
                            className="search-input"
                            prefix={<SearchOutlined />}
                        />
                    </Col>
                </FlexRowWrapper>
                <CustomTable
                    columns={tableColumns}
                    dataSource={dataSource}
                    loading={loading === "table"}
                    pagination={pagination}
                    onChangeTable={onChangeTable}
                    // sorter
                    onRowClick={onRow}
                />
            </Card>
        </ListingSTOWrapper>
    );
};

export default ListingSTO;
