import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import { Menu, Dropdown, Button, Table } from 'antd';
import { transactionColumns, transactionData } from 'data/transaction';
import Text from 'antd/lib/typography/Text';
import { useHistory } from 'react-router-dom';
import { useGetTransactionsQuery } from 'generated/graphql';
import DropdownElement from 'components/DropdownElement';

const TransactionWrapper = styled.div`
  padding: 40px;
  width: 100%;
  .filters-wrapper {
    padding: 20px;
    column-gap: 25px;
    align-items: center;
    margin-top: 40px;
    input {
      padding: 9px;
      width: 300px;
      background: rgba(5, 31, 36, 0.05);
      border: 1px solid rgba(5, 31, 36, 0.05);
      box-sizing: border-box;
      border-radius: 5px;
    }
  }

  .ant-dropdown-trigger {
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.4px;
    color: #9c9c9c;
    font-family: 'Montserrat', sans-serif;
    padding: 20px;
    display: flex;
    align-items: center;
  }
  .ant-table-row {
    cursor: pointer;
    .ant-table-cell {
      font-weight: 500;
      font-size: 16px;
      line-height: 19px;
      color: #051f24;
      .t-transaction-status {
        color: #07697d;
      }
    }
  }
  .t-transactions {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
`;

interface TransactionProps {}
const menu = (
  <Menu>
    <Menu.Item key="1" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="2" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="3" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    lll
  </Menu>
);

const Transaction = ({}: TransactionProps): JSX.Element => {
  const history = useHistory();
  const { data: transactionsData } = useGetTransactionsQuery();

  return (
    <TransactionWrapper>
      <Text className="t-transactions">Transactions</Text>
      {/* <FlexRowWrapper className="filters-wrapper">
        <input type="text" placeholder="Search and filter..." />

        <DropdownElement options={menu} title="Token" />
        <DropdownElement options={menu} title="Status" />
        <DropdownElement options={menu} title="Transaction Type" />
      </FlexRowWrapper> */}
      <Table
        onRow={(record, _) => {
          return {
            onClick: () => {
              history.push(`/transaction/${record.id}`);
            },
          };
        }}
        columns={transactionColumns}
        dataSource={transactionsData?.transactions}
      />
      ,
    </TransactionWrapper>
  );
};

export default Transaction;
