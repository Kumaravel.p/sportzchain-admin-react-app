import React from 'react';
import { Avatar, Card, Divider, Tooltip } from 'antd';
import { Row, Col, Button } from 'antd';
import '../index.css';
import { useHistory } from 'react-router-dom';
import ViewCardGenerator from 'components/ViewCardGenerator/viewCardGenerator';
import CustomTable from 'components/customTable';
import { ArrowLeftOutlined, EyeOutlined } from '@ant-design/icons';
import { useStoViewQuery } from 'generated/pgraphql';
import viewGenJSON from 'components/NewSTO/stoJsonGen/viewJSON';
import moment from 'moment';
import ViewNoteModal from 'components/NewSTO/viewNotesModel';
import { useGetStoStatusQueryQuery } from 'generated/pgraphql';
import { stoTokenData } from 'apis/sto';
import { Spin } from 'antd';
// Status = Discontinue - no updates allowed (no data can be updated including the notes) - hide Edit
// Status = Closed - no updates allowed (no data can be updated including the notes) - hide Edit
const CreateStoDetailPage = (): JSX.Element => {
  const history = useHistory<any>();
  const [viewModel, setViewModel] = React.useState(false);
  const [viewModelData, setViewModelData] = React.useState([]);
  const {
    data,
    loading: loadingShow
  } = useStoViewQuery({
    fetchPolicy: 'no-cache',
    variables: { id: history?.location?.state?.id },
  });
  const { data: stoStatusOption, loading: stoStatusOptionloging } =
    useGetStoStatusQueryQuery();

  const [viewSto, setViewSto] = React.useState<any>([]);
  

  let BY_VALUE: any = [];
  let BY_DATE: any = [];

  if (data?.allStos?.nodes[0]?.stoDiscountsByStoId.nodes) {
    BY_VALUE = data?.allStos?.nodes[0]?.stoDiscountsByStoId.nodes?.filter(
      (val) => val?.discountType === 'BY_VALUE'
    );
    BY_DATE = data?.allStos?.nodes[0]?.stoDiscountsByStoId.nodes?.filter(
      (val) => val?.discountType === 'BY_DATE'
    );
  }
  const tableColumns = [
    { title: 'Profile', dataIndex: 'avatar', component: 'avatar' },
    { title: 'Team Name', dataIndex: 'teamName' },
    { title: 'Token Name', dataIndex: 'tokenName' },
    { title: 'Price', dataIndex: 'price' },
  ];

  const TeamdataSource: any =
    data?.allStos?.nodes[0]?.stoTransactionsByStoId?.nodes.map((val) => ({
      avatar: val?.userByUserId?.imageUrl
        ? { src: val?.userByUserId?.imageUrl }
        : { src: '' },
      teamName: val?.userByUserId ? val?.userByUserId?.name : '',
      tokenName: data?.allStos?.nodes[0]?.teamByTeamId?.walletsByTeamId
        ? data?.allStos?.nodes[0]?.teamByTeamId?.walletsByTeamId?.nodes[0]
          ?.coinSymbol
        : '',
      price: val?.usdPurchaseValue ? parseInt(val?.usdPurchaseValue) : '',
    }));

  const NotetableColumns = [
    { title: '#', dataIndex: 'no' },
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: (a: any, b: any) => a.name.localeCompare(b.name),
    },
    { title: 'Added by', dataIndex: 'addedBy' },
    { title: 'Added date', dataIndex: 'addedDate' },
    {
      title: 'action',
      dataIndex: 'actions',
      component: 'icons',
      align: 'center',
    },
  ];

  const onClickViewModelEye = (e: any, rowRecord: any, rowIndex: any) => {
    setViewModelData(rowRecord);
    setViewModel(!viewModel);
  };

  let NotedataSource: any = data?.allStos?.nodes[0]?.stoNotesByStoId?.nodes.map(
    (val, i) => ({
      no: i + 1,
      name: val?.name,
      addedBy: val?.addedBy,
      addedDate: moment(val?.createdDate + 'z').format('MMMM D YYYY, hh:mm'),
      notes: val?.notes,
      actions: (rowRecord: any, rowIndex: number) => (
        <EyeOutlined
          style={{ color: '#4DA1FF' }}
          onClick={(e) => onClickViewModelEye(e, rowRecord, rowIndex)}
        />
      ),
    })
  );

  let totalSold = data?.allStos?.nodes[0]
    ? data?.allStos?.nodes[0]?.totalSold
    : 0;
  let supplySize = data?.allStos?.nodes[0]
    ? data?.allStos?.nodes[0]?.supplySize
    : 0;
  let initialPrice = data?.allStos?.nodes[0]
    ? data?.allStos?.nodes[0]?.initialPrice
    : 0;

  const handleCloseViewModel = () => {
    setViewModel(!viewModel);
  };
  React.useEffect(() => {
    (async () => {
      let totToken: any = await stoTokenData(
        data?.allStos?.nodes[0]?.teamByTeamId?.tokenAddress
      );
      let getViewSto = viewGenJSON(
        data?.allStos?.nodes[0],
        stoStatusOption,
        totToken?.data?.data
      );
      setViewSto(getViewSto);
    })();
  }, [data?.allStos?.nodes[0] && stoStatusOption]);

  React.useEffect(() => {
    return () => {
      setViewSto([]);
    };
  }, []);

  const stoBtn = () => {
    if ([5, 7].includes(data?.allStos?.nodes[0]?.stoStatusId ?? 0)) {
      return <>
        <Tooltip title="No Edit access" color={'#2db7f5'}>
          <Button
            className="save cta-button"
            disabled={true}
            onClick={() => {
              history.push('/create-sto', { Editdata: data });
            }}
            type={'primary'}
            size={'large'}
            style={{ padding: '0px 24px' }}
          >
            Edit
          </Button>
        </Tooltip>
      </>
    } else {
      return <>
        <Button
          className="save cta-button"
          onClick={() => {
            history.push('/create-sto', { Editdata: data });
          }}
          type={'primary'}
          size={'large'}
          style={{ padding: '0px 24px' }}
        >
          Edit
        </Button>
      </>
    }
  }

  const giveMePrice = () => {
    if (data?.allStos?.nodes[0]) {
      return parseInt(data?.allStos?.nodes[0]?.targetAmount)
    }
    return '';
  }

  return (
    <Row style={{ width: '100%', backgroundColor: '#F4F5F6' }}>
      {loadingShow || stoStatusOptionloging || viewSto.length > 0 ? (
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
          }}
        >
          <Spin />
        </div>
      ) : (
        <>
          <div
            style={{
              display: 'flex',
              width: '100%',
              justifyContent: 'space-between',
              marginRight: '3%',
              marginTop: '2%',
            }}
          >
            <div
              style={{
                display: 'flex',
                width: '100%',
                justifyContent: 'space-between',
                marginRight: '0%',
                marginTop: '2%',
              }}
            >
              <div style={{ marginLeft: '2.8%' }}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <h2 style={{ fontWeight: 'bold', color: '#051F244D' }}>
                    <Button
                      shape="circle"
                      icon={<ArrowLeftOutlined />}
                      onClick={() => history.push('/listing-sto')}
                    />
                    &nbsp;&nbsp;STO &gt;
                  </h2>
                  &nbsp;&nbsp;
                  <Avatar
                    src={data?.allStos?.nodes[0]?.teamByTeamId?.profileImageUrl}
                    style={{ marginBottom: '6%' }}
                  />
                  <h2 style={{ fontWeight: 'bold' }}>&nbsp; View STO</h2>
                </div>
              </div>
              <div style={{ width: '7%' }}>
                {stoBtn()}
              </div>
            </div>
          </div>
          <ViewCardGenerator DataJSON={viewSto.BasicJson} />
          <ViewCardGenerator DataJSON={viewSto.SmartContract} />
          <ViewCardGenerator DataJSON={viewSto.InitialJson} />
          <ViewCardGenerator DataJSON={viewSto.TimeLocking} />
          <Card
            style={{
              width: '100%',
              margin: '30px',
              marginBottom: '0px',
              marginTop: '3%',
            }}
          >
            <h2 className="bold">Discounts</h2>
            <br />
            <h3 className="bold">Discount by STO Date</h3>
            <br />
            <Row justify="space-between" align="bottom">
              {BY_DATE?.map((val: any, j: any) => {
                return (
                  <>
                    <Col lg={8} key={j}>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 14,
                        }}
                      >
                        From Date & Time
                      </div>
                      <div style={{ marginBottom: '6%' }}>
                        <span style={{ fontSize: 16, fontWeight: '500' }}>
                          {moment(val?.discountFromDate).format(
                            'MMMM D YYYY, HH:mm'
                          ) ?? ''}
                        </span>
                      </div>
                      <Divider />
                    </Col>
                    <Col lg={8} key={j}>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 14,
                        }}
                      >
                        To Date & Time
                      </div>
                      <div style={{ marginBottom: '6%' }}>
                        <span style={{ fontSize: 16, fontWeight: '500' }}>
                          {moment(val?.discountToDate).format(
                            'MMMM D YYYY, HH:mm'
                          ) ?? ''}
                        </span>
                      </div>
                      <Divider />
                    </Col>
                    <Col lg={8} key={j}>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 14,
                        }}
                      >
                        Discount Percentage
                      </div>
                      <div style={{ marginBottom: '6%' }}>
                        <span style={{ fontSize: 16, fontWeight: '500' }}>
                          {val?.discountPercentage ?? ''}
                        </span>
                      </div>
                      <Divider />
                    </Col>
                  </>
                );
              })}
            </Row>
            <h3 className="bold">Discount by Purchase Value</h3>
            <br />
            <Row justify="space-between" align="bottom">
              {BY_VALUE?.map((val: any, j: any) => {
                return (
                  <>
                    <Col lg={8} key={j}>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 14,
                        }}
                      >
                        Purchase Value
                      </div>
                      <div style={{ marginBottom: '6%' }}>
                        <span style={{ fontSize: 16, fontWeight: '500' }}>
                          {parseInt(val?.purchaseValue) ?? ''}
                        </span>
                      </div>
                      <Divider />
                    </Col>
                    <Col lg={8} key={j}>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 14,
                        }}
                      >
                        Discount Percentage
                      </div>
                      <div style={{ marginBottom: '6%' }}>
                        <span style={{ fontSize: 16, fontWeight: '500' }}>
                          {val?.discountPercentage ?? ''}
                        </span>
                      </div>
                      <Divider />
                    </Col>
                    <Col lg={8} key={j}>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 14,
                        }}
                      ></div>
                      <div style={{ marginBottom: '6%' }}>
                        <span
                          style={{ fontSize: 16, fontWeight: '500' }}
                        ></span>
                      </div>
                      <Divider />
                    </Col>
                  </>
                );
              })}
            </Row>
          </Card>
          <ViewCardGenerator DataJSON={viewSto.RestrictionLimits} />
          <Row
            justify="space-between"
            align="bottom"
            style={{ width: '96%' }}
            gutter={20}
          >
            <Col lg={8}>
              <Card style={{ width: '100%', margin: '30px' }}>
                <div
                  style={{ marginBottom: '2%', color: 'gray', fontSize: 15 }}
                >
                  Subcribed
                </div>
                <div>
                  <span
                    style={{
                      fontSize: 24,
                      fontWeight: '500',
                      color: '#4DA1FF',
                    }}
                  >
                    {(totalSold / supplySize) * 100}
                  </span>
                </div>
              </Card>
            </Col>
            <Col lg={8}>
              <Card style={{ width: '100%', margin: '30px' }}>
                <div
                  style={{ marginBottom: '2%', color: 'gray', fontSize: 15 }}
                >
                  Target (supply size x rate)
                </div>
                <div>
                  <span
                    style={{
                      fontSize: 24,
                      fontWeight: '500',
                      color: '#4DA1FF',
                    }}
                  >
                    $
                    {giveMePrice()}
                  </span>
                </div>
              </Card>
            </Col>
            <Col lg={8}>
              <Card style={{ width: '100%', margin: '30px' }}>
                <div
                  style={{ marginBottom: '2%', color: 'gray', fontSize: 15 }}
                >
                  Achieved (initialprice x subcribed)
                </div>
                <div>
                  <span
                    style={{
                      fontSize: 24,
                      fontWeight: '500',
                      color: '#FF6D4A',
                    }}
                  >
                    {initialPrice * totalSold}$
                  </span>
                </div>
              </Card>
            </Col>
          </Row>
          <Row style={{ width: '97%', paddingLeft: '3%', marginBottom: '3%' }}>
            <CustomTable
              columns={tableColumns}
              dataSource={TeamdataSource}
              loading={false}
              sorter
            />
          </Row>
          <Row style={{ width: '97%', paddingLeft: '3%', marginBottom: '5%' }}>
            <Card style={{ width: '100%' }}>
              <Row justify="space-between" style={{ width: '100%' }}>
                <Col lg={12}>
                  <h2 className="bold">Notes</h2>
                </Col>
              </Row>
              <CustomTable
                columns={NotetableColumns}
                dataSource={NotedataSource}
                loading={false}
              />
              <ViewNoteModal
                ModalVisible={viewModel}
                handleCancel={handleCloseViewModel}
                Data={viewModelData}
              />
            </Card>
          </Row>
        </>
      )}
    </Row>
  );
};

export default CreateStoDetailPage;
