import React, { useEffect, useMemo } from 'react';
import { Card } from 'antd';
import { Form, Row, Col, Button, Spin } from 'antd';
import '../index.css';
import { useHistory } from 'react-router-dom';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import { toast } from 'react-toastify';
import InitialOffering from 'components/NewSTO/initialOffering';
import TimeLocking from 'components/NewSTO/timeLocking';
import Discounts from 'components/NewSTO/discount';
import RestrictionLimits from 'components/NewSTO/restrictionLimits';
import { ArrowLeftOutlined, PlusOutlined } from '@ant-design/icons';
import {
  useGetStoStatusQueryQuery,
  useGetStoTeamListQueryQuery,
  useGetStoTeamNoRoundLazyQuery,
} from 'generated/pgraphql';
import NoteModal from 'components/NewSTO/notesModel';
import NewNotes from 'components/NewSTO/note';
import moment from 'moment';
import { statusAndEditFuntion, stoFilterStatus } from '../utils/index';
import { editGenJSON } from 'components/NewSTO/stoJsonGen/viewJSON';
import { stoInsertApi, stoTokenData } from 'apis/sto';
import currentSession from 'utils/getAuthToken';
const CreateStoDetailPage = (): JSX.Element => {
  const [form] = Form.useForm();
  const history = useHistory<any>();

  const [createSto, setCreateSto] = React.useState<any>({
    userType: 1,
    discount: [],
    discountsPurchase: [],
  });
  const [teamId, setteamId] = React.useState('');
  const [noofRoundSto, setnoofRoundSto] = React.useState<number>(0);
  // note State
  const [NoteisModalVisible, setNoteIsModalVisible] = React.useState(false);
  const [editId, setEditId] = React.useState<any>(null);
  const [isModalEdit, setIsModalEdit] = React.useState<any>({});
  const [userData, setUserData] = React.useState<any>({});
  const [checkStatus,setCheckStatus] = React.useState<boolean>(false);
  const addNewSPORow = () => {
    let Data = {
      fromDateTime: '',
      toDateTime: '',
      discountPercentage: '',
    };
    createSto?.discount?.push(Data);
    setCreateSto({
      ...createSto,
    });
  };

  const onChage = (val: any, type: string, index: number, variant: string) => {
    // val ===> value, type ===> keys of the object, index ===> index of the particular object, variant ===> set state
    if (variant === 'SPO') {
      
      createSto.discount[index][type] = val;
      setCreateSto({
        ...createSto,
      });
    } else if (variant === 'purchase') {
      createSto.discountsPurchase[index][type] = val?.target?.value;
      setCreateSto({
        ...createSto,
      });
    }
  };

  const addNewPurchaseRow = () => {
    let Data = {
      purchaseValue: '',
      purchaseDiscountPercentage: '',
    };
    createSto?.discountsPurchase?.push(Data);
    setCreateSto({
      ...createSto,
    });
  };
  useEffect(() => {
    currentSession().then(res => {
      setUserData(res.payload)
    })
  }, []);

  React.useEffect(() => {
    if (checkStatus) {
      SubmitFuntion()
    }
  }, [createSto])

  // Notes model and table handeling funcations
  const handleOkNote = (NoteSend: any, isEdidata: any) => {
    if (Object.keys(isEdidata).length > 0) {
      commonEditFunction(NoteSend, 'edit');
    } else {
      let saveJson = [];
      
      NoteSend.addedDate = moment();
      NoteSend.addedby = userData?.name;
      if (createSto.NotedataSource) {
        saveJson = createSto.NotedataSource;
      }
      saveJson.push(NoteSend);
      setCreateSto({
        ...createSto,
        NotedataSource: saveJson,
      });
      setNoteIsModalVisible(false);
    }
  };
  const commonEditFunction = (data: any, type: any) => {
    // data will return the data and type will edit or delete
    if (type === 'edit') {
      setNoteIsModalVisible(false);
      createSto['NotedataSource'][editId] = data;
    } else if (type === 'delete') {
      let _data = createSto['NotedataSource'].filter(
        (_x: any, i: any) => i !== editId
      );
      setCreateSto({
        ...createSto,
        ['NotedataSource']: _data,
      });
    }
    setEditId(null);
    setIsModalEdit({});
    setNoteIsModalVisible(false);
  };
  const onclickModelNote = () => {
    setNoteIsModalVisible(!NoteisModalVisible);
  };
  const handleCancelNote = () => {
    setNoteIsModalVisible(false);
    setEditId(null);
    setIsModalEdit({});
    setCheckStatus(false)
  };


  const onBeforeSave = async () => {
    form.submit();
    form.validateFields()
      .then(async () => {
        if (createSto?.statusId !== history?.location?.state?.Editdata?.allStos?.nodes[0]?.stoStatusId && [7, 8, 2, 3].includes(createSto?.statusId)) {
          setCheckStatus(true)
          onclickModelNote()
        }
        else {
          if (checkStatus) {
            setCheckStatus(false)
          }
          SubmitFuntion()
        }
      })
      .catch(e => {
        console.log(e, 'e')
      })
  };


  const SubmitFuntion = async () => {
    if (
      createSto?.teamId &&
      (createSto?.id ? createSto?.statusId : true) &&
      createSto?.targetAmount &&
      createSto?.initialPrice &&
      createSto?.startDateTime &&
      createSto?.endDateTime &&
      createSto?.displayStartDateTime &&
      createSto?.displayEndDateTime &&
      createSto?.initialTokenRelease &&
      createSto?.initialTokensToBeUnlocked &&
      createSto?.remainingTokenRelease
    ) {
      // api call STO
      // createSto ===> data passing, createSto?.id ===> edit data conditional
      let sto: any = await stoInsertApi(createSto, createSto?.id);
      if (sto.data.statusCode === "500") {
        toast.error(sto.data.message);
      } 
      else {
        toast.success(
          `STO ${createSto?.id ? 'Edited' : 'Created'} Successfully`
        );
        const stoId = sto?.data?.data?.[0]?.createSto?.sto?.id || sto?.data?.data?.[0]?.updateStoById?.sto?.id
        if (stoId) {
          history.push('/view-sto', { id: stoId });
        } else {
          history.push('/listing-sto');
        }
      }
    } else {
      toast.error('Please Fill All Required Field');
    }
  };
  // disCounts SPO and disCount purchase data
  const RemoveFun = (type: any, deleteID: any) => {
    if (type === 'SPO') {
      let data = createSto.discount.filter((x: any, i: any) => i !== deleteID);
      setCreateSto({
        ...createSto,
        discount: [...data],
      });
    } else if (type === 'purchase') {
      let data = createSto.discountsPurchase.filter(
        (_x: any, i: any) => i !== deleteID
      );
      setCreateSto({
        ...createSto,
        discountsPurchase: [...data],
      });
    }
  };
  const openEdit = (key: any) => {
    setEditId(key - 1);
    setIsModalEdit(createSto.NotedataSource[key - 1]);
    setNoteIsModalVisible(true);
  };
  // get all STO status
  const { data: stoStatusOption, loading: stoStatusOptionLoading } =
    useGetStoStatusQueryQuery();
  const { data: StoTeamList, loading: StoTeamListLoading } =
    useGetStoTeamListQueryQuery();
  const GetStoTeamList = StoTeamList?.allWallets?.nodes.map((val: any) => ({
    coinSymbol: val?.coinSymbol,
    balance: val?.balance,
    name: val?.teamByTeamId?.name,
    teamId: val?.teamByTeamId?.id,
    teamAddress: val?.teamByTeamId?.tokenAddress
  }));
  const [
    getStoTeamNoRoundLazyQuery,
    { data: getNoRound, loading: getNoRoundLoading },
  ] = useGetStoTeamNoRoundLazyQuery();
  React.useEffect(() => {
    let getRound = getNoRound?.allStos?.nodes
      ? getNoRound?.allStos?.nodes?.length + 1
      : 0;
    setnoofRoundSto(getRound);
  }, [getNoRoundLoading]);
  React.useEffect(() => {
    const apiCall = async () => {
      let teamTokenData: any = await stoTokenData(createSto?.teamAddress);
      let dataset = teamTokenData?.data?.data;
      setCreateSto({
        ...createSto,
        teamToken: [dataset],
      });
    };
    if (teamId) {
      apiCall();
    }
  }, [teamId]);

  React.useEffect(() => {
    const apiCall = async () => {
      let data = editGenJSON(
        history?.location?.state?.Editdata?.allStos?.nodes[0]
      );
      let teamTokenData: any = await stoTokenData(history?.location?.state?.Editdata?.allStos?.nodes?.[0]?.teamByTeamId?.tokenAddress);
      let dataset = teamTokenData?.data?.data;
      setCreateSto({
        ...data,
        teamToken: [dataset],
        teamAddress: history?.location?.state?.Editdata?.allStos?.nodes?.[0]?.teamByTeamId?.tokenAddress
      });
      setteamId(history?.location?.state?.Editdata?.allStos?.nodes?.[0]?.teamId)
      form.setFieldsValue({ ...data });
    };
    
    if (history?.location?.state?.Editdata?.allStos?.nodes[0]?.id) {
      apiCall();
    }
  }, [history?.location?.state?.Editdata?.allStos?.nodes[0]?.id]);

  // let statusAndCondition: any = statusAndEditFuntion(
  //   stoStatusOption?.allMasterStoStatuses?.nodes,
  //   history?.location?.state?.Editdata?.allStos?.nodes
  // );

  //sto status validation
  const statusValidations = useMemo(() => {
    return stoFilterStatus(stoStatusOption?.allMasterStoStatuses?.nodes, history?.location?.state?.Editdata?.allStos?.nodes?.[0]?.stoStatusId,)
  }, [history?.location?.state?.Editdata, stoStatusOption])

  return (
    <Row style={{ backgroundColor: '#F4F5F6' }}>
      <div
        style={{
          display: 'flex',
          width: '100%',
          justifyContent: 'space-between',
          marginRight: '0%',
          marginTop: '2%',
        }}
      >
        <div style={{ marginLeft: '2.8%' }}>
          <div style={{ display: 'flex', alignItems: 'center' }}>
            <h2 style={{ fontWeight: 'bold', color: '#051F244D' }}>
              <Button
                shape="circle"
                icon={<ArrowLeftOutlined />}
                onClick={() => history.push('/listing-sto')}
              />
              &nbsp;&nbsp;STO &gt;
            </h2>
            <h2 style={{ fontWeight: 'bold' }}>
              &nbsp; {createSto.id ? 'Edit STO' : 'Create STO'}
            </h2>
          </div>
        </div>
        <div style={{ width: '24%' }}>
          <Button
            className="save cta-button"
            onClick={onBeforeSave}
            type={'primary'}
            size={'large'}
            style={{ padding: '0px 24px' }}
          >
            {createSto?.id ? 'Update STO' : 'Create STO'}
          </Button>
          <Button
            size={'large'}
            className="cta-button discard"
            onClick={() => history.push('/listing-sto')}
            style={{ marginLeft: '2%', padding: '0px 24px' }}
          >
            Discard
          </Button>
        </div>
      </div>
      <Card
        style={{ width: '100%', margin: '10px 30px' }}
        // className={
        //   statusAndCondition[0]?.disableComponent?.basicinfo
        //     ? 'itemDisable'
        //     : ''
        // }
      >
        <h2 className="bold">Basic info</h2>
        <br />
        <Form
          form={form}
          initialValues={{
            layout: 'horizontal',
          }}
          layout={'vertical'}
        >
          <Row justify="space-between" align="bottom">
            <Col lg={6}>
              <LabelInput
                label="Team Name*"
                rules={[
                  {
                    required: true,
                    message: 'Team Name is required!',
                  },
                ]}
                name="teamName"
                inputElement={
                  <SelectElement
                    onChange={async (
                      e: React.ChangeEvent<HTMLInputElement>
                    ) => {
                      let teamDetail: any = GetStoTeamList?.filter(
                        (val: any) => val?.name === e
                      );
                      setCreateSto({
                        ...createSto,
                        teamName: e,
                        coinSymbol: teamDetail[0].coinSymbol,
                        teamId: teamDetail[0].teamId,
                        teamAddress: teamDetail[0].teamAddress
                      });
                      setteamId(teamDetail[0].teamId);
                      form.setFieldsValue({ teamName: e });
                      await getStoTeamNoRoundLazyQuery({
                        variables: { equalTo: teamDetail[0]?.teamId },
                      });
                    }}
                    defaultValue={createSto.teamName}
                    options={(GetStoTeamList as unknown as any) ?? []}
                    toFilter="name"
                    placeholder="Team Name"
                    searchable={true}
                    loading={StoTeamListLoading}
                    // disabled={statusAndCondition[0]?.disableComponent?.teamName}
                  />
                }
              />
            </Col>
            <Col lg={6}>
              <div style={{ marginBottom: '6%', color: 'gray', fontSize: 16 }}>
                Total Tokens
              </div>
              {createSto?.teamToken?.[0]?.totaltoken ? (
                <div style={{ marginBottom: '6%' }}>
                  <span style={{ fontSize: '20px' }} className="bold">
                    {createSto.teamToken[0].totaltoken}
                    {' '}
                    {createSto?.coinSymbol ?? ''}
                  </span>{' '}
                  <span style={{ color: '#15151699', fontSize: '14px' }}>
                    (
                    {createSto?.teamToken?.[0]?.remainingToken ?? ''}
                    {createSto?.teamToken?.[0]?.tokenSymbol ?? ''}{' '}
                    left){' '}
                  </span>
                </div>
              ) : (
                '-'
              )}
            </Col>
            {
              createSto?.id &&
              <Col lg={6}>
                <LabelInput
                  label="STO Status*"
                  name="statusId"
                  // initialValue={createSto.statusId}
                  rules={[
                    {
                      required: true,
                      message: 'Status is required!',
                    },
                  ]}
                  inputElement={
                    <SelectElement
                      labelInValue
                      onChange={(e: any) => {
                        setCreateSto({ ...createSto, statusId: e?.value });
                        form.setFieldsValue({ statusId: e?.value });
                      }}
                      options={
                        stoStatusOptionLoading ? [] : statusValidations.options ?? []
                      }
                      toFilter="type"
                      toStore="id"
                      placeholder="Status"
                      searchable={true}
                      loading={stoStatusOptionLoading}
                      // disabled={statusAndCondition[0]?.disableComponent?.statusId}
                    />
                  }
                />
              </Col>
            }
          </Row>
        </Form>
      </Card>
      <Card
        style={{ width: '100%', margin: '10px 30px' }}
        // className={
        //   statusAndCondition[0]?.disableComponent?.InitialOffering
        //     ? 'itemDisable'
        //     : ''
        // }
      >
        <InitialOffering
          form={form}
          setCreateSto={setCreateSto}
          createSto={createSto}
          disableComponent={null}
          noofRoundSto={noofRoundSto}
          teamList={StoTeamList?.allWallets?.nodes ?? []}
          teamId={teamId}
        />
      </Card>
      <Card
        style={{ width: '100%', margin: '10px 30px' }}
        // className={
        //   statusAndCondition[0]?.disableComponent?.TimeLocking
        //     ? 'itemDisable'
        //     : ''
        // }
      >
        <TimeLocking
          form={form}
          setCreateSto={setCreateSto}
          createSto={createSto}
          disableComponent={null}
        />
      </Card>
      <Card
        style={{ width: '100%', margin: '10px 30px' }}
        // className={
        //   statusAndCondition[0]?.disableComponent?.Discounts
        //     ? 'itemDisable'
        //     : ''
        // }
      >
        <Discounts
          form={form}
          createSto={createSto}
          RemoveFun={RemoveFun}
          onChage={onChage}
          addNewSPORow={addNewSPORow}
          addNewPurchaseRow={addNewPurchaseRow}
          disableComponent={null}
        />
      </Card>
      <Card
        style={{ width: '100%', margin: '10px 30px' }}
        // className={
        //   statusAndCondition[0]?.disableComponent?.RestrictionLimits
        //     ? 'itemDisable'
        //     : ''
        // }
      >
        <RestrictionLimits
          form={form}
          setCreateSto={setCreateSto}
          createSto={createSto}
          disableComponent={null}
        />
      </Card>
      <Card
        style={{ width: '100%', margin: '10px 30px' }}
        // className={
        //   statusAndCondition[0]?.disableComponent?.notes ? 'itemDisable' : ''
        // }
      >
        <Row justify="space-between" style={{ width: '100%' }}>
          <Col lg={12}>
            <h2 className="bold">Notes</h2>
          </Col>
          <Button
            type="link"
            icon={<PlusOutlined />}
            onClick={() => onclickModelNote()}
            disabled={false}
          >
            Add New Note
          </Button>
        </Row>
        <NewNotes
          Notesdisabled={
            null
          }
          JsonData={createSto?.NotedataSource}
          openEdit={(key: any) => openEdit(key)}
        />
        <NoteModal
          handleDelete={(val: any, type: any) => commonEditFunction(val, type)}
          EditData={isModalEdit}
          ModalVisible={NoteisModalVisible}
          onclickModel={() => onclickModelNote()}
          handleOk={(val: any, editdata: any) => handleOkNote(val, editdata)}
          handleCancel={handleCancelNote}
          readOnly={isModalEdit?.id}
        />
      </Card>
    </Row>
  );
};
export default CreateStoDetailPage;
