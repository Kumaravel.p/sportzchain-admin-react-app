import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import {
  Menu,
  Dropdown,
  Button,
  message,
  Space,
  Table,
  Tooltip,
  Radio,
  Divider,
} from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import { ReactComponent as UserIcon } from 'assets/icons/users.svg';
import FlexWrapper from 'components/common/wrappers/FlexWrapper';
import {
  reportAndAnalysisData,
  reportAndAnalysisColumns,
} from 'data/reportAndAnalysis';

const ReportAndAnalysisWrapper = styled.div`
  width: 100%;
  padding: 40px;
  .t-report {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .filters-wrapper {
    padding: 40px 0;
    align-items: center;
    input {
      padding: 9px;
      background: rgba(5, 31, 36, 0.05);
      border: 1px solid rgba(5, 31, 36, 0.05);
      box-sizing: border-box;
      border-radius: 5px;
    }
    .cta-wrapper {
      align-items: center;
      column-gap: 8px;
    }
  }
  .add-new-report-button {
    border: 1px solid rgba(7, 105, 125, 0.25);
    color: rgba(7, 105, 125, 1);
    box-sizing: border-box;
    border-radius: 5px;
    height: fit-content;
    padding: 15px;
    display: flex;
    align-items: center;
    column-gap: 10px;
    margin-left: auto;
    margin-right: 0;
  }

  .ant-dropdown-trigger {
    font-size: 12px;
    line-height: 16px;
    letter-spacing: 0.4px;
    color: #9c9c9c;
    font-family: 'Montserrat', sans-serif;
    padding: 20px;
    display: flex;
    align-items: center;
  }

  .t-report-name {
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;

    color: #051f24;
  }
  .t-report-metric-type {
    font-size: 16px;
    line-height: 19px;

    color: #07697d;
  }
`;

interface ReportAndAnalysisProps {}
const menu = (
  <Menu>
    <Menu.Item key="1" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="2" icon={<UserOutlined />}>
      Option
    </Menu.Item>
    <Menu.Item key="3" icon={<UserOutlined />}>
      Option
    </Menu.Item>
  </Menu>
);

const ReportAndAnalysis = ({}: ReportAndAnalysisProps): JSX.Element => {
  return (
    <ReportAndAnalysisWrapper>
      <div className="t-report">Reports</div>
      <FlexRowWrapper className="filters-wrapper">
        <FlexRowWrapper className="cta-wrapper">
          <input type="text" placeholder="Search and filter..." />
          <Dropdown overlay={menu}>
            <Button>
              Reward
              <DownOutlined />
            </Button>
          </Dropdown>
          <Dropdown overlay={menu}>
            <Button>
              User adoption
              <DownOutlined />
            </Button>
          </Dropdown>{' '}
        </FlexRowWrapper>
        <FlexWrapper className="add-new-button-wrapper">
          <Button className="add-new-report-button">
            <UserIcon />
            Create New Report
          </Button>
        </FlexWrapper>
      </FlexRowWrapper>
      <Table
        rowSelection={{
          type: 'checkbox',
          // ...rowSelection,
        }}
        columns={reportAndAnalysisColumns}
        dataSource={reportAndAnalysisData}
      />
    </ReportAndAnalysisWrapper>
  );
};

export default ReportAndAnalysis;
