import React from "react";

export const SchedulerDashboard = (): JSX.Element => {
    return <iframe style={{ width: "100%", display: "block", borderWidth: "0px" }} src={process.env.REACT_APP_SCHEDULER}></iframe>
}