import React,{ useMemo } from 'react';
import styled from 'styled-components/macro';
import { Form, Row, Col, Card, Button, Typography, Modal, TablePaginationConfig, Input } from 'antd';
import {
  WinningForm,
  RewardsForm,
  QuestionsWrapperModal,
  QuestionsFormCommon,
  ManOfMatchForm,
} from 'components/contests';
import config from 'config';
import {
  constructQuestionState,
  questionTableManagement,
  rankBasedRewardDataSource,
} from 'components/contests/data';
import { TemplateWrapper, MatchCard } from 'components/contests';
import ViewCardGenerator from 'components/ViewCardGenerator/viewCardGenerator';
import BreadcrumbComp from 'components/common/breadcrumb';

import { useParams, useHistory } from 'react-router-dom';
import viewGenJSON from 'components/contests/viewGenJSON';
import { useGetContestDetailsQuery } from 'generated/pgraphql';
import { useGetTeamsQuery } from 'generated/graphql';
import { ArrowDownOutlined, ArrowUpOutlined, SearchOutlined } from '@ant-design/icons';
import { ternaryOperator } from 'utils';
import { TeamSelectionContext } from 'App';
import { useGetAllRequiredContestOptionsQuery } from 'generated/pgraphql';
import { DisplayWinners, getContestWinner, getParticipantCount } from 'apis/contest';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import moment from 'moment';
import CustomTable from 'components/customTable';
import currentSession from 'utils/getAuthToken';
import { useDebounce } from 'hooks/useDebounce';
import { fixedNumber, formatDate, toLowerCase } from './users/utils';
import { toast } from 'react-toastify';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const ContestDetailsWrapper = styled.div`
  padding: 20px 20px;
  width: 100%;
  background: rgba(245, 245, 247, 1);
  .breadcumb-btn-mg {
    margin-right: 15px;
  }
  .ant-card {
    margin: 20px !important;
    margin-top: 20px !important;
    width: 100%;
  }
  .ant-form {
    width: 100%;
  }
  .datepicker {
    width: 100%;
  }
  & .header {
    color: #051f24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
  }
  & .sub-header {
    color: #051f24;
    font-weight: 600;
    font-size: 16px;
    margin-bottom: 10px;
  }
  & .body1 {
    color: #051f24;
    font-size: 14px;
    font-weight: 500;
    margin-bottom: 10px;
  }
`;

const ModalWrapper = styled(Modal)`
    width:80% !important;
    @media (max-width: 600px) {
        width:95% !important;
    }
    .table-container{
        border: 1px solid #E6E6E6;
        width: 100%;
        border-radius: 4px;
        padding: 1px;
        margin-bottom: 24px;
    }
    .search-input{
      margin-left: auto;
      margin-bottom: 24px;
      & .ant-input-affix-wrapper{
        border: 1px solid rgba(5, 31, 36, 0.1);
        border-radius: 5px;
      }
      & .ant-input-prefix{
        color:#97a0c3;
      }
  }
`;

const ParticipantCardWrapper = styled.div<{count:boolean,cursor?:string}>`
  .ant-card {
    margin: 20px !important;
    margin-top: 20px !important;
    width: 100%;
    cursor:${props=>props?.cursor ? props?.cursor : "auto"};
  }
  .ant-card-body {
    flex: 1;
    display: flex;
    justify-content: ${props=>props?.count ? "space-between" : "stretch"};
    flex-direction: row;
    align-items: center;
  }
  .badge {
    padding: 2px 15px;
    border-radius: 10px;
    font-size: 14px;
    font-weight: 600;
  }
`;
interface ContestDetailsProps { }

const CommonRadioOptions = {
  ALL: 'ALL',
  SELECTED: 'SELECTED',
  NONE: 'NONE',
};

interface GeneratorProps {
  contestDetails: any[];
  participationCriteria: any[];
}

interface TableDataType {
  rank?: string;
  avatar: {
      src: string;
      firstChar: string;
  };
  fullName: string;
  userName: string;
  participatedOn: string;
  rewardValue: string;
  rewardQuantity: string;
}

let pageSize = 5;

let initialPagination:TablePaginationConfig={
  current: 1,
  pageSize,
  total: 0,
}

const ContestDetails = ({ }: ContestDetailsProps): JSX.Element => {
  const questionsDataRef: any = React.useRef();
  const totalRewardsRef: any = React.useRef();
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  const params = useParams<{ contestId: string; teamId: string }>();
  const [winnerSelection, setWinnerSelection] = React.useState("ALL");
  const [rewardType, setRewardType] = React.useState('EQUAL_REWARD');
  const [isVisible,setIsVisible] = React.useState<boolean>(false);
  const [editIndex,setEditIndex] = React.useState<number>(-1);
  const [dataSource, setDataSource] = React.useState<TableDataType[]>([]);
  const [loading, setLoading] = React.useState<boolean>(false);

  const history = useHistory<any>();
  const {
    data: contestDetailsGql,
    loading: contestDataLoading,
  } = useGetContestDetailsQuery({
    variables: { id: params?.contestId },
    skip: !!!params?.contestId,
    fetchPolicy: 'no-cache',
  });

  const { data: teams } = useGetTeamsQuery();
  const [contestData, setContestData] = React.useState<GeneratorProps>({
    contestDetails: [],
    participationCriteria: [],
  });
  const [participantData, setParticipantData] = React.useState<{
    particiaptePercentage?: number;
    participateCount?: number;
    won: number;
    wonPercentage?: number;
  }>({
    won: 0
  });
  const [result, setResult] = React.useState<any>({});
  const [contestWinnerList, setContestWinnerList] = React.useState<boolean>(false);
  const [pagination, setPagination] = React.useState<TablePaginationConfig>(initialPagination);
  const [userData, setUserData] = React.useState<any>({});
  const [searchText, setSearchText] = React.useState<string>("");

  const [form] = Form.useForm();

  //debounce hook
  const debounce = useDebounce();

  const {
    data: allRequiredOptions,
    loading: requiredOptionsLoading,
  } = useGetAllRequiredContestOptionsQuery();

  React.useEffect(() => {
    let data = viewGenJSON(result);
    setContestData(data);
  }, [result]);

  React.useEffect(() => {
    if (params?.contestId) {
      (async () => {
        let participantData = await getParticipantCount({
          contestId: params?.contestId,
        });
        setParticipantData({
          ...participantData?.data?.data,
          status: true,
        } ?? {});
      })();
    }
  }, []);
  const { profile } = useSnapshot(ProxyState);

  React.useEffect(() => {
    totalRewardsRef.current = {
      data: rankBasedRewardDataSource,
    };
    currentSession().then(res => {
      setUserData(profile)
  })
  }, []);

  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  if (isIamTeamUser()) {
    if (parseInt(params.teamId) !== teamSelectionContext.team.id) {
      history.push('/');
    }
  }

  React.useEffect(() => {
    if (contestDetailsGql?.contestById) {
      let data: any = contestDetailsGql?.contestById;
      let teamName = teams?.team?.filter(
        (item) => item?.id === data?.teamId
      )?.[0]?.name;

      // handle multi-table questions
      let questions: any[] = [];
      if (data?.contestTypeId === 'Quiz') {
        questions = questionTableManagement(
          data?.contestQuestionsByContestId?.nodes,
          0
        );
      } else if (data?.contestTypeId === 'Predict') {
        questions = questionTableManagement(data, data?.templateId);
        let matchData = data?.contestMatchDetailsByContestId?.nodes[0];
        form.setFieldsValue({ 'question-master': questions?.[0]?.question });
        form.setFieldsValue({
          leagueId: matchData?.leagueId,
          matchId: matchData?.matchId,
          team1Color: matchData?.team1Color,
          team2Color: matchData?.team2Color,
        });
      }

      let result = {
        ...data,
        ...data?.contestRewardsByContestId?.nodes[0],
        question: questions,
        teamName: teamName,
      };
      form.setFieldsValue({ ...result });
      setWinnerSelection(result?.winnerSelectionCriteria ?? "ALL");
      setRewardType(result?.rewardType ?? 'EQUAL_REWARD');
      setResult(result);
    }
  }, [contestDataLoading,teams]);

  const onQuestionsFormSubmit = (questionsData: any) => {
    questionsDataRef.current = questionsData;
  };

  const handleTotalRewardsMaster = (record: any) => {
    totalRewardsRef.current = record;
  };

  const getBadgeIcon = (status: boolean) => {
    if (status) {
      return <ArrowUpOutlined />;
    } else {
      return <ArrowDownOutlined />;
    }
  };

  const handleMatchChange = (matchRecord: any) => { };

  const contructNoteJson = useMemo(() => {
    let data = contestDetailsGql?.contestById?.contestNotesByContestId?.nodes;
    if(!data?.length) return []
    return data?.map((note:any)=>({
     name: note?.name,
     notes: note?.notes,
     addedDate: moment(note?.createdDate.concat('z')),
     addedby: note?.addedBy,
     id: note?.id,
    }))
   }, [contestDetailsGql])
 
   const viewNoteData = useMemo(() => {
     if(editIndex > -1){
       return {
         name:contructNoteJson[editIndex]?.name,
         note:contructNoteJson[editIndex]?.notes
       }
     }
     return {}
   }, [editIndex])
 
   const handleNoteModal = (bool:boolean=false,index:number=-1) => {
     setIsVisible(bool)
     setEditIndex(!bool ? -1 : index)
   }

  const onToggleContestWinner = (val: boolean = false) => {
    if (val) {
      //api call
      getAllWinnerList()
    }
    setContestWinnerList(val)
  }

  const onChangeTable = (newPagination: TablePaginationConfig) => {
    const { current = 1, pageSize } = newPagination;
    let offset = current;
    let from = "pagination"
    if (pagination?.pageSize !== newPagination?.pageSize) {
      offset = 1;
      from = ""
    }
    getAllWinnerList(offset, pageSize, searchText, from)
  }

  const getAllWinnerList = (offset: number = 1, limit: number = pageSize, searchText: string = "", from: string = "initial") => {

    setLoading(true)

    let payload: DisplayWinners = {
      "userId": Number(userData?.user_id),
      "contestId": params?.contestId,
      "start": ((offset * limit) - limit),
      "length": limit,
      searchText
    }
    getContestWinner(payload)
      .then((res: any) => {
        if (res?.data?.statusCode === "500") {
          catchError(res?.data?.message)
        }
        let data = res?.data?.data;
        let constructWinners = []
        if (data?.list?.length) {
          constructWinners = data?.list?.map((_: any, index: number) => {
            let userDetails = _?.userByUserId;
            return {
              ...(data.rewardType === "RANKING" && { rank: `${index + 1}` }),
              avatar: {
                src: `${config.apiBaseUrl}files/${userDetails?.imageUrl}`,
                firstChar: userDetails?.name?.charAt(0),
              },
              fullName: userDetails?.name,
              userName: userDetails?.username,
              participatedOn: userDetails?.participation_time?.nodes?.[0]?.createdAt,
              rewardValue: _?.rewardValue ?? "0",
              rewardQuantity: _?.rewardPoint ?? "0",
            }
          })
        }
        setDataSource(constructWinners);
        setPagination({
          ...pagination,
          current: from === "initial" ? 1 : offset,
          pageSize: limit,
          total: data?.totalCount
        })
        setLoading(false)
      })
      .catch((e: any) => {
        catchError(e?.message)
      })
  }

  const catchError = (err: string) => {
    console.log(err);
    setLoading(false)
    toast.error(err ? err : 'Something went wrong!');
    return false
}


  const tableColumns = useMemo(() => [
    ...(rewardType === "RANKING" ? [{
      title: 'Rank',
      dataIndex: "rank",
      component: "status"
    }] : []),
    { title: 'Avatar', dataIndex: "avatar", component: 'avatar' },
    {
      title: 'Full Name',
      dataIndex: "fullName",
      sorter: (a: any, b: any) => toLowerCase(a?.fullName).localeCompare(toLowerCase(b?.fullName))
    },
    {
      title: 'User Name',
      dataIndex: "userName",
      sorter: (a: any, b: any) => toLowerCase(a?.userName).localeCompare(toLowerCase(b?.userName))
    },
    {
      title: 'Participated On',
      dataIndex: "participatedOn",
      valueFormat: (value: string) => `${formatDate(value, "DD MMM YYYY, hh:mm a")}`,
      sorter: (a: any, b: any) => moment(a.participatedOn).unix() - moment(b.participatedOn).unix()
    },
    {
      title: 'Reward Value',
      dataIndex: "rewardValue",
      sorter: (a: any, b: any) => a?.rewardValue - b?.rewardValue,
      valueFormat: (value: string) => `$${ternaryOperator(fixedNumber(value), '0')}`
    },
    {
      title: 'Reward Quantity',
      dataIndex: "rewardQuantity",
      sorter: (a: any, b: any) => a?.rewardQuantity - b?.rewardQuantity,
      valueFormat: (value: string) => `${ternaryOperator(fixedNumber(value), '0')} USD`
    },
  ], [rewardType, dataSource])

   //on search input in the table
   const onSearchTableList = (value: string) => {
    setSearchText(value)
    debounce(() => getAllWinnerList(1, pageSize, value), 800);
}  

  return (
    <ContestDetailsWrapper>
      <Row>
        <BreadcrumbComp
          backPath="/contests"
          cta={
            <>
              {(!['INACTIVE', 'DISCONTINUED', 'CLOSED'].includes(
                result?.statusId ?? ''
              ) ||
                (result?.contestTypeId === 'Predict' &&
                  result?.statusId === 'CLOSED')) && (
                  <Button
                    type="primary"
                    className="breadcumb-btn-mg"
                    onClick={() => {
                      history.push(`/contests/${params.contestId}/edit`);
                    }}
                  >
                    Edit
                  </Button>
                )}
              <Button  onClick={()=>window.open(`${config.sportzchainFanPageURL}/contest-preview?formValues=${btoa(JSON.stringify({...result,contestId:params?.contestId}))}`)}>Live preview</Button>
            </>
          }
          breadCrumbs={[
            { name: 'Contest', url: '/contests' },
            { name: `${result?.title}`, url: `/contests/${params?.contestId}` },
          ]}
        />
        <Form form={form} preserve={false}>
          <Row gutter={[12, 0]}>
            <ViewCardGenerator DataJSON={contestData?.contestDetails} />
            <ViewCardGenerator DataJSON={contestData?.participationCriteria} />
            <Card>
              <Typography className="header">Winner Definition</Typography>
              <WinningForm
                form={form}
                radioOptions={CommonRadioOptions}
                result={result}
                isView={true}
                winnerSelection={winnerSelection}
              />
            </Card>
            <Card>
              <Typography className="header">Rewards</Typography>
              <RewardsForm
                form={form}
                radioOptions={CommonRadioOptions}
                result={result}
                handleTotalRewardsMaster={handleTotalRewardsMaster}
                isView={true}
                rewardType={rewardType}
                winnerSelection={winnerSelection}
              />
            </Card>
            {result?.contestTypeId === 'Quiz' && (
              <Card>
                <QuestionsWrapperModal
                  data={result?.question}
                  onSubmit={onQuestionsFormSubmit}
                  isView={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && (
              <Card>
                <TemplateWrapper
                  form={form}
                  title={'Contest Template'}
                  value={result?.templateId}
                  keyName="templateId"
                  renderJson={
                    requiredOptionsLoading
                      ? []
                      : ((allRequiredOptions?.allMasterContestTemplates
                        ?.nodes as unknown) as [])
                  }
                  isView={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && (
              <Card>
                <Typography className="header">Match Details</Typography>
                <MatchCard
                  form={form}
                  data={{
                    ...result?.contestMatchDetailsByContestId?.nodes[0]
                      ?.matchByMatchId,
                    team1Color:
                      result?.contestMatchDetailsByContestId?.nodes[0]
                        ?.team1Color,
                    team2Color:
                      result?.contestMatchDetailsByContestId?.nodes[0]
                        ?.team2Color,
                  }}
                  isView={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && result?.templateId === 1 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={result?.question?.[0]}
                  isAllowMultiSelect={false}
                  isImageUploadEnabled={false}
                  isSelectAnswer={false}
                  questionMasterTitle={'Predict the Match Winner'}
                  optionType={'text'}
                  handleChange={handleMatchChange}
                  templateId={result?.templateId}
                  isView={true}
                  isDescriptionDisabled={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && result?.templateId === 2 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={result?.question?.[0]}
                  isAllowMultiSelect={false}
                  isImageUploadEnabled={false}
                  isSelectAnswer={false}
                  questionMasterTitle={'Predict the Score'}
                  optionType={
                    result?.question?.[0]?.answerType === 'FREE_TEXT'
                      ? 'image'
                      : 'text'
                  }
                  handleChange={handleMatchChange}
                  templateId={result?.templateId}
                  isView={true}
                  isDescriptionDisabled={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && result?.templateId === 3 && (
              <Card>
                <ManOfMatchForm
                  form={form}
                  data={result?.question?.[0]}
                  handleChange={handleMatchChange}
                  handleAnswerUpdate={handleMatchChange}
                  questionMasterTitle={'Predict Man of the Match'}
                  isSelectAnswer={false}
                  isView={true}
                  isDescriptionDisabled={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && result?.templateId === 4 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={result?.question?.[0]}
                  isImageUploadEnabled={true}
                  isAllowMultiSelect={false}
                  isSelectAnswer={false}
                  questionMasterTitle={'What Happens Next'}
                  optionType={'text'}
                  handleChange={handleMatchChange}
                  isView={true}
                  templateId={result?.templateId}
                  isDescriptionDisabled={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && result?.templateId === 5 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={result?.question?.[0]}
                  isAllowMultiSelect={false}
                  isImageUploadEnabled={true}
                  isSelectAnswer={false}
                  optionType={'puzzle'}
                  questionMasterTitle={'Spot the Ball'}
                  handleChange={handleMatchChange}
                  templateId={result?.templateId}
                  isView={true}
                  isDescriptionDisabled={true}
                />
              </Card>
            )}
            {result?.contestTypeId === 'Predict' && result?.templateId === 6 && (
              <Card>
                <QuestionsFormCommon
                  form={form}
                  data={result?.question?.[0]}
                  isImageUploadEnabled={true}
                  isAllowMultiSelect={false}
                  isSelectAnswer={false}
                  questionMasterTitle={'Predict the Player Position'}
                  optionType={'text'}
                  handleChange={handleMatchChange}
                  isView={true}
                  templateId={result?.templateId}
                  isDescriptionDisabled={true}
                />
              </Card>
            )}
            {/* dashboard status card START*/}
            {/* {participantData?.won > -1 &&  */}
            <Row
              justify="space-between"
              align="bottom"
              style={{ width: '100%' }}
              gutter={30}
            >
              {/* participation card LEFT START */}
              <Col span={12}>
                <ParticipantCardWrapper count={Boolean(participantData?.particiaptePercentage)}>
                  <Card
                    className="custom-style-card"
                    style={{
                      flex:1,
                      width: '100%',
                      margin: '30px',
                      minHeight: 160,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <div>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 17,
                        }}
                      >
                        Participants
                      </div>
                      <div>
                        <span
                          style={{
                            fontSize: 24,
                            fontWeight: '500',
                            color: '#4DA1FF',
                          }}
                        >
                          {participantData?.participateCount ?? 0}
                        </span>
                      </div>
                    </div>
                    {
                      participantData?.particiaptePercentage &&
                      <div
                      style={{
                        backgroundColor: participantData?.particiaptePercentage ? 'green' : 'red',
                        color: '#fff',
                      }}
                      className="badge"
                    >
                      {getBadgeIcon(participantData?.particiaptePercentage ? true : false)}
                       {participantData?.particiaptePercentage?.toFixed(2)}
                    </div>
                    }
                  </Card>
                </ParticipantCardWrapper>
              </Col>
              {/* pariticipation card LEFT END */}


              {/* participation card RIGHT START */}
              <Col span={12}>
                <ParticipantCardWrapper
                  count={Boolean(participantData?.wonPercentage)}
                  onClick={() => onToggleContestWinner(true)}
                  cursor="pointer"
                >
                  <Card
                    className="custom-style-card"
                    style={{
                      flex:1,
                      width: '100%',
                      margin: '30px',
                      minHeight: 160,
                      display: 'flex',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}
                  >
                    <div>
                      <div
                        style={{
                          marginBottom: '2%',
                          color: 'gray',
                          fontSize: 17,
                        }}
                      >
                        Participants Won
                      </div>
                      <div>
                        <span
                          style={{
                            fontSize: 24,
                            fontWeight: '500',
                            color: '#4DA1FF',
                          }}
                        >
                          {participantData?.won ?? 0}
                        </span>
                      </div>
                    </div>
                   {
                    participantData?.wonPercentage &&
                    <div
                      style={{
                        backgroundColor: participantData?.wonPercentage ? 'green' : 'red',
                        color: '#fff',
                      }}
                      className="badge"
                    >
                      {getBadgeIcon(participantData?.wonPercentage ? true : false)}
                       {participantData?.wonPercentage?.toFixed(2)}
                    </div>
                    }
                  </Card>
                </ParticipantCardWrapper>
              </Col>
              {/* pariticipation card RIGHT END */}

              {/* dashboard status card END*/}
            </Row>
             {/* } */}
            <Card>
            <Typography className="header">Notes</Typography>
              <NoteModal
                EditData={viewNoteData}
                ModalVisible={isVisible}
                handleOk={handleNoteModal}
                handleCancel={handleNoteModal}
                readOnly
              />
              <Note
                JsonData={contructNoteJson}
                openEdit={(obj: any, key: number) => handleNoteModal(true,key-1)}
                readOnly
              />
            </Card>
          </Row>
        </Form>
      </Row>
      {/* Winner Table Modal */}
      <ModalWrapper
        title={`Contest Winners (${pagination.total || 0})`}
        centered
        visible={contestWinnerList}
        onCancel={() => onToggleContestWinner()}
        destroyOnClose={true}
        footer={null}
      >
        <Col xs={24} sm={8} md={6} className="search-input">
          <Input
            type="text"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onSearchTableList(e.target.value)}
            value={searchText}
            autoComplete={"off"}
            placeholder="Search by User Name or Full Name"
            prefix={<SearchOutlined />}
          />
        </Col>
        <CustomTable
          columns={tableColumns}
          dataSource={dataSource}
          loading={loading}
          pagination={pagination}
          onChangeTable={onChangeTable}
        // onRowClick={onRow}
        />
      </ModalWrapper>
      { }
    </ContestDetailsWrapper>
  );
};

export default ContestDetails;
