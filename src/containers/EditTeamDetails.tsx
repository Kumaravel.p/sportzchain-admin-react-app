import styled from 'styled-components/macro';
import React,{ useCallback, useMemo } from 'react';
import { Button, Card, Col, Divider, Input, Row, Form, DatePicker, Checkbox } from 'antd';
import moment from 'moment';
import { toast } from 'react-toastify';
import {
  useGetTeamByIdQuery, useGetAllCountriesQuery, useGetTeamCategoriesQuery, useGetTeamStatusesQuery, useGetTeamCurrencyQuery,
  useCreateTeamNoteMutation
} from 'generated/graphql';
import { useHistory, useParams } from 'react-router-dom';
import LabelInput from 'components/common/LabelInput';
import TitleCta from 'components/common/TitleCta';
import SelectElement from 'components/common/SelectElement';
import graphqlReq from 'apis/graphqlRequest';
import { teamFilterStatus } from 'utils';
import NoteModal from 'components/NewTeams/addNoteModal';
import { teamUpdateApi } from 'apis/teamsApi';
import { useGetAllTeamLeaguesQuery } from 'generated/pgraphql';
import { useSnapshot } from 'valtio';
import { state } from 'state';

const EditTeamDetailsWrapper = styled.div`
  padding: 0 40px;
  width: 100%;

  .t-transactions {
    font-size: 25px;
    line-height: 30px;
    color: rgba(5, 31, 36, 0.3);
  }
  .t-transaction-id {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .ant-card {
    margin-top: 40px;
    width: 100%;
    .info-edit-wrapper {
      margin-bottom: 40px;
    }
    .t-info {
      font-weight: 600;
      font-size: 20px;
      line-height: 24px;
      color: #051f24;
    }
    .t-transaction-key-field {
      font-size: 15px;
      line-height: 18px;
      color: rgba(5, 31, 36, 0.6);
    }
    .t-transaction-value {
      font-size: 16px;
      line-height: 19px;
      font-weight: 500;
      color: #051f24;
      margin: 10px;
      &.blue {
        color: #4da1ff;
      }
    }
    .key-value-wrapper {
      row-gap: 10px;
    }
    .edit-col {
      display: flex;
      align-items: center;
      column-gap: 20px;
    }
  }

  .ant-btn.add-new-button {
    border: 1px solid rgba(7, 105, 125, 0.25);
    color: rgba(7, 105, 125, 1);
    box-sizing: border-box;
    border-radius: 5px;
    height: fit-content;
    padding: 15px;
    display: flex;
    align-items: center;
    column-gap: 10px;
  }

  .sr-no-wrapper {
    position: relative;
    .srno-default-pop {
      position: absolute;
      background: #4aaf05;
      border: 2px solid #4aaf05;
      font-size: 12px;
      padding: 0 6px;
      top: -10px;
      left: 12px;
      color: #fff;
      border-radius: 100px;
    }
  }

  .sub-routes-wrapper {
    column-gap: 8px;
    margin: 60px auto;
    text-align: center;
    .sub-route {
      padding: 8px;
      width: 100%;
      background: rgba(198, 198, 198, 0.6);
      color: rgba(5, 31, 36, 0.6);
      border-radius: 4px;
      cursor: pointer;
      font-weight: 600;
      &.active {
        background: #fff;
        border: 1px solid #eee;
        color: #4da1ff;
      }
    }
  }
  a {
    text-decoration: none;
    color: inherit;
  }
  .cta-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;

    &.save {
      background: #369afe;
      color: #ffffff;
    }
    &.discard {
      border: 1px solid #369afe;
      color: #369afe;
      margin-left: 10px;
    }
  }
`;

interface EditTeamDetailsProps { }

type TeamLeagueOptions = {
  __typename?: "TeamLeague" | undefined;
  nodeId: string;
  leagueName: string;
}[];

const EditTeamDetails = ({ }: EditTeamDetailsProps): JSX.Element => {
  const { teamId } = useParams<{ teamId: string }>();
  const history = useHistory();
  const [form] = Form.useForm();

  const [createTeamNote] = useCreateTeamNoteMutation();
  const { profile } = useSnapshot(state);

  const user_id = profile?.user_id;

  const [isNoteModalVisible, setIsNoteModalVisible] = React.useState<boolean>(false);
  const [confirmLoading, setConfirmLoading] = React.useState(false);

  const { data: teamById } = useGetTeamByIdQuery({
    variables: { teamId: +teamId },
    fetchPolicy: 'no-cache'
  });

  const { data: teamStatuses, loading: teamStatusLoading } = useGetTeamStatusesQuery();
  const { data: countries } = useGetAllCountriesQuery();
  const { data: teamCategories } = useGetTeamCategoriesQuery();
  const { data: teamCurrencyTypes } = useGetTeamCurrencyQuery();
  const { data: allTeamLeagues } = useGetAllTeamLeaguesQuery();

  React.useEffect(() => {
    let contructTeamDetails = {
      ...teamById?.team_by_pk,
      teamActivation: [
        moment(teamById?.team_by_pk?.activeStartAt?.concat('z')),
        moment(teamById?.team_by_pk?.activeEndAt?.concat('z')),
      ],
      teamDisplay: [
        moment(teamById?.team_by_pk?.displayStartAt?.concat('z')),
        moment(teamById?.team_by_pk?.displayEndAt?.concat('z')),
      ],
      league: teamById?.selectedLeague?.length ? teamById?.selectedLeague?.map((_: any) => _?.league) : [],
      existingLeague: teamById?.selectedLeague?.length ? teamById?.selectedLeague : [],
      coinSymbol: teamById?.team_by_pk?.coinSymbol,
      tokenAddress: teamById?.team_by_pk?.tokenAddress
    }
    form.setFieldsValue({...contructTeamDetails})
  }, [teamById?.team_by_pk]);


  const onBeforeSave = async () => {
    form.validateFields()
      .then(async () => {
        if (['SUSPENDED', 'DISCONTINUED', 'SUBMITTED', 'ACTIVE'].includes(form.getFieldValue('statusId'))) {
          setIsNoteModalVisible(true)
        }
        else {
          onSave()
        }
      })
      .catch(e => {
        console.log(e, 'e')
      })
  };

  const onSave = async () => {
    setConfirmLoading(true);
    try {
      let formValues = form.getFieldsValue();
      await teamUpdateApi({
        teamId,
        name: formValues?.name,
        statusId: formValues?.statusId,
        activeStartAt: (formValues.teamActivation?.[0]).toISOString(),
        activeEndAt: (formValues.teamActivation?.[1]).toISOString(),
        displayStartAt: (formValues.teamDisplay?.[0]).toISOString(),
        displayEndAt: (formValues.teamDisplay?.[1]).toISOString(),
        coinSymbol: formValues?.coinSymbol,
        currencyTypeId: formValues?.currencyTypeId,
        countryTitle: formValues?.countryTitle,
        categoryId: formValues?.categoryId,
        league: formValues?.league,
        tokenAddress: formValues?.tokenAddress,
        isInternalTeam: formValues?.isInternalTeam,
      })
        .then(res => {
          if (res.data.statusCode === "500") {
            toast.error(res.data?.message);
            return false
          }
          history.push(`/team/${teamId}`);
          toast.success(`Updated Team: ${formValues?.name}`);
        })
        .catch(e => {
          console.log(e)
          toast.error('Something went wrong!');
          return false
        })
    }
    catch (error: any) {
      console.log(error);
      toast.error(error?.message || JSON.stringify(error));
    }
    finally {
      setConfirmLoading(false);
    }
  }

  const createNote = async (val: any) => {
    createTeamNote({
      variables: {
        creatorId: user_id,
        teamId: +teamId,
        name: val.name,
        content: val.note,
      },
    })
      .then(() => {
        toast.success('Note Created successfully');
        setIsNoteModalVisible(false);
        onSave();
      })
      .catch(e => {
        toast.error('Something went wrong');
        console.log(e);

      })
  }
  
  //disable start date
  const isDisableStartDate = useCallback((date) => {
    return date && moment(date).isBefore(moment(), 'day')
  }, [form])

  //team status validation
  const statusValidations = useMemo(() => {
    return teamFilterStatus(teamStatuses?.teamStatus, teamById?.team_by_pk?.statusId)
  }, [teamById, teamStatuses])

  //find disable components
  const disabledComponents = useCallback((key: string) => {
    let validate = statusValidations?.disableComponents?.includes(key)
    return statusValidations?.except ? validate : !validate
  }, [statusValidations])

  //status validation for Display Start End Date Time
  const displayStartEndDateTimeValidation = (getFieldValue: any, value: any) => {
    if (!value?.[0] || !value?.[1]) {
      return Promise.reject(new Error(`Display ${!value[0] ? "Start" : "End"} Date Time is required!`));
    }
    if (getFieldValue('teamActivation')) {
      if (!value?.[0].isBefore(getFieldValue('teamActivation')[0])) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('teamActivation')[0].format('DD MM yyyy hh:mm:ss')})`));
      }
      if (!value?.[1].isAfter(getFieldValue('teamActivation')[1])) {
        return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('teamActivation')[1].format('DD MM yyyy hh:mm:ss')})`));
      }
    }
    return Promise.resolve();
  }

  const onValuesChange = (field:any) =>{
    if(field?.teamActivation && field?.teamDisplay){
      form.validateFields(['teamDisplay']);
    }
  }

  return (
    <EditTeamDetailsWrapper>
      <Card>
        <TitleCta
          title="Edit Team"
          cta={
            <>
              <Button
                className="save cta-button"
                onClick={onBeforeSave}

                disabled={confirmLoading}
              >
                {confirmLoading ? 'Updating...' : 'Update'}
              </Button>
              <Button
                className="cta-button discard"
                onClick={() => history.goBack()}
                disabled={confirmLoading}
              >
                Discard
              </Button>
            </>
          }
        />

        <Form onFinish={onSave} form={form} preserve={false} onValuesChange={onValuesChange}>
          <Row gutter={24}>
            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="Team Name"
                name="name"
                rules={[
                  {
                    required: true,
                    message: 'Team Name is required!',
                  },
                ]}
                inputElement={
                  <Input
                    type="text"
                    disabled={disabledComponents('teamName')}
                  />
                }
              />
            </Col>

            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="Status"
                name="statusId"
                rules={[
                  {
                    required: true,
                    message: 'Status is required!',
                  },
                ]}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                    loading={teamStatusLoading}
                    options={teamStatusLoading ? [] : statusValidations.options ?? []}
                    toFilter="status"
                    placeholder="Status"
                    disabled={disabledComponents('status')}
                  />
                }
              />
            </Col>

            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="Activation Start and End time"
                name="teamActivation"
                rules={[
                  {
                    required: true,
                    message: 'Activation Start and End time is required!',
                  },
                  () => ({
                    validator(_, value) {
                      if (!value?.[0] || !value?.[1]) {
                        return Promise.reject(new Error(`Activation ${!value[0] ? "Start" : "End"} Date Time is required!`));
                      }
                      return Promise.resolve();
                    },
                  }),
                ]}
                inputElement={
                  <DatePicker.RangePicker
                    style={{
                      width: '100%'
                    }}
                    showTime
                    disabled={[disabledComponents('activationStartDate'),disabledComponents('activationEndDate')]}
                    disabledDate={(value) => isDisableStartDate(value)}
                  />
                }
              />
            </Col>
          </Row>
          <Divider />
          <Row gutter={24}>
            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="Display Start and End time"
                name="teamDisplay"
                rules={[
                  {
                    required: true,
                    message: 'Display Start and End time is required!',
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      return displayStartEndDateTimeValidation(getFieldValue,value)
                    },
                  }),
                ]}
                inputElement={
                  <DatePicker.RangePicker
                    showTime
                    disabled={[disabledComponents('displayStartAt'),disabledComponents('displayEndAt')]}
                    disabledDate={(value) => isDisableStartDate(value)}
                    style={{
                      width: '100%'
                    }}
                  />
                }
              />
            </Col>
            <Col span={8}>
              <LabelInput
                label="Currency Symbol"
                name="coinSymbol"
                inputElement={
                  <Input
                    type="text"
                    disabled={disabledComponents('coinSymbol')}
                    placeholder={'Currency Symbol'}
                  />
                }
              />
            </Col>

            <Col span={8}>
              <LabelInput
                label="Currency type"
                name="currencyTypeId"
                rules={[
                  {
                    required: true,
                    message: 'Currency type is required!',
                  },
                ]}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                    options={teamCurrencyTypes?.teamCurrencyType ?? []}
                    toFilter="currency"
                    placeholder="Currency type"
                    disabled={disabledComponents('currencyTypeId')}
                  />
                }
              />
            </Col>
          </Row>
          <Divider />
          <Row gutter={24}>
            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                name="countryTitle"
                label="Country"
                rules={[
                  {
                    required: true,
                    message: 'Country is required!',
                  },
                ]}
                // rules=[[required: "true"]]
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                    options={countries?.country ?? []}
                    toFilter="title"
                    placeholder="Country"
                    disabled={disabledComponents('countryTitle')}
                  />
                }
              />
            </Col>
            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="Sports Category"
                name="categoryId"
                rules={[
                  {
                    required: true,
                    message: 'Sports Category is required!',
                  },
                ]}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                    options={teamCategories?.teamCategory ?? []}
                    toFilter="description"
                    toStore="category"
                    placeholder="Sport Category"
                    disabled={disabledComponents('categoryId')}
                  />
                }
              />
            </Col>

            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="League"
                name="league"
                rules={[
                  {
                    required: true,
                    message: 'League is required!',
                  },
                ]}
                inputElement={
                  <SelectElement
                    toFilter="description"
                    toStore="leagueName"
                    options={allTeamLeagues?.allTeamLeagues?.nodes as unknown as TeamLeagueOptions ?? []}
                    allowClear
                    placeholder="Select League"
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {}}
                    mode="multiple"
                    searchable={true}
                    disabled={disabledComponents('league')}
                  />
                }
              />
            </Col>

            {/* <Col span={6}></Col> */}
          </Row>
          <Divider />
          <Row gutter={24}>
            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="Token Address"
                name="tokenAddress"
                inputElement={
                  <Input
                    placeholder={"Token Address"}
                    type="text"
                    disabled={disabledComponents('tokenAddress')}
                  />
                }
              />
            </Col>
            <Col lg={8} md={8} sm={12} xs={24}>
              <LabelInput
                label="Is Internal Team"
                name="isInternalTeam"
                valuePropName="checked"
                inputElement={
                  <Checkbox
                    disabled={disabledComponents('isInternalTeam')}
                  />
                }
              />
            </Col>
          </Row>
        </Form>
      </Card>
      <NoteModal
        handleDelete={(val: any) => { return val }}
        EditData={{}}
        ModalVisible={isNoteModalVisible}
        onclickModel={() => setIsNoteModalVisible(true)}
        handleOk={(val: any) => createNote(val)}
        handleCancel={() => setIsNoteModalVisible(false)}
      />
    </EditTeamDetailsWrapper>
  );
};

export default EditTeamDetails;
