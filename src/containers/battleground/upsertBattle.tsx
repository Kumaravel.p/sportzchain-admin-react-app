import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Row, Col, Typography, Input, Card, Upload as AntUpload, Button, Form, Spin, Radio, Space, TablePaginationConfig, DatePicker, InputNumber, Checkbox } from 'antd';
import SelectElement from 'components/common/SelectElement';
import styled from 'styled-components/macro';
import LabelInput from 'components/common/LabelInput';
import BreadcrumbComp from 'components/common/breadcrumb';
import { toast } from 'react-toastify';
import { useHistory, useParams } from 'react-router-dom';
import TextEditorInput from 'components/common/TextEditorInput';
import { parseJson } from 'utils/functions';
import { useGetGroupByIdQuery, useGetBattleGroundByIdQuery, useGetBattleCountQuery } from 'generated/pgraphql';
import { useGetTeamCategoriesQuery, useGetTeamStatusesQuery } from 'generated/graphql';
import { battleUpsert, matchDataInsert } from 'apis/battleground';
import config from 'config';
import { useDebounce } from 'hooks/useDebounce';
import { pollFilterStatus, ternaryOperator } from 'utils';
import moment, { MomentInput } from 'moment';
import { UpsertBattleGroundPayload, InsertMatchPayload } from 'ts/interfaces/battleground';
import { PlusOutlined } from '@ant-design/icons';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import currentSession from 'utils/getAuthToken';
import { MatchCard } from 'components/contests';
import axios from 'axios';
import { Upload } from 'components/common/Upload';

const Wrapper = styled('div')`
width:100%;
padding:40px;
background-color: #F5F5F7;
& .card-wrap{
    margin-top: 16px;
    & .ant-card-bordered{
        border-radius: 10px;
        margin-bottom: 16px;
    }
}
& .header{
    color:#051F24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
}
& .label{
    font-weight: 400;
    font-size: 14px;
    color:#051F2499;
    margin-bottom: 16px;
    & span{
        color:red;
        margin-left: 8px;
    }
}
& .ant-select-selector,.datepicker{
    border:1px solid #d9d9d9 !important;
    border-radius: 2px !important;
}
& .datepicker{
    padding:4px 11px !important;
}
& .tabs{
    width:100%;
    margin-block:24px;
    display:flex;
    gap:12px;
    align-items:center;
    transition:0.5s;
    & .tab-item{
        flex:1;
        cursor:pointer;
        padding:8px;
        text-align: center;
        background: rgba(198, 198, 198, 0.6);
        border-radius: 4px;
        font-weight: 600;
        font-size: 16px;
        color:rgba(5, 31, 36, 0.6);
    }
    & .active{
        color:#4DA1FF;
        background: #FFFFFF;
    }
}
& .remove-btn{
    color:#1890ff;
}
& .table-list{
        width: 100%;
        margin-bottom: 16px;
        & .ant-space-item:first-of-type{
            flex:1;
            & .table-title{
                margin-bottom: 0;
                color:#000000;
                font-size: 16px;
                font-weight: 600;
            }
        }
}
& .search-input{
    border: 1px solid rgba(5, 31, 36, 0.1);
    border-radius: 5px;
    & .ant-input-prefix{
        color:#97a0c3;
    }
}
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

interface battleProps { }


const UpsertBattle = (props: battleProps): JSX.Element => {
    //debounce hook
    const debounce = useDebounce();
    const [form] = Form.useForm();
    const params: { id?: string } = useParams();
    const history = useHistory();

    //states
    const [formData, setFormData] = React.useState<object>({});
    const [loading, setLoading] = useState<'save' | 'fan' | null>(null);
    const [activeTab, setActiveTab] = useState<number>(0);
    const [groupDesc, setGroupDesc] = useState<string>("");
    const [isVisible, setIsVisible] = React.useState<boolean>(false);
    const [editIndex, setEditIndex] = React.useState<number>(-1);
    const [checkStatus, setCheckStatus] = React.useState<boolean>(false);
    const [userData, setUserData] = React.useState<any>({});
    const [leagueOptions, setLeagueOptions] = React.useState<any>([]);
    const [selectedLeague, setSelectedLeague] = React.useState<any>([]);
    const [matchs, setMatch] = React.useState<any>([]);
    const [matchSeedData, setMatchSeedData] = React.useState<any>([]);
    const [uploads, setUploads] = useState<{
        team1Logo?: any[],
        team2Logo?: any[],
    }>({
        team1Logo: [],
        team2Logo: [],
    });

    //get BattleGround Detail By id
    const { data: battleById, loading: battleByIdLoading } = useGetBattleGroundByIdQuery({
        ...(params?.id && { variables: { id: params?.id }, }),
        skip: !(!!params.id),
        fetchPolicy: 'network-only',
    });
    const { data: battleCount, loading: battleCountLoading } = useGetBattleCountQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    })


    const { data: teamStatuses, loading: teamStatusLoading } = useGetTeamStatusesQuery();




    const { data: teamCategories } = useGetTeamCategoriesQuery();

    const changeTab = (tabNumber: number) => {
        setActiveTab(tabNumber)
    }


    const beforeUpload = (file: any) => {
        const allowedTypes =
            file.type === 'image/png' || file.type === 'image/jpeg';
        if (!allowedTypes) {
            toast.error(`${file.name} is not a png or jpeg file`);
        }
        return allowedTypes || AntUpload.LIST_IGNORE;
    };




    const onBeforeSave = () => {
        form.submit();
        form.validateFields()
            .then(async () => {
                try {
                    onSave(form.getFieldsValue());

                } catch (error: any) {
                    toast.error(error?.message || JSON.stringify(error));
                }
            })
            .catch((e) => {
                console.log(e, 'e');
                toast.info('Please fill all the mandatory fields!')
            });
    }

    //on save or update
    const onSave = async (res: any) => {
        setLoading('save');

        let callSeedMatchDetail;
        if (!params?.id) {
            let insertMatchPayload: InsertMatchPayload = {
                "sportName": res?.sportCategory,
                "Events": matchSeedData?.matchDetail,
                "team1Logo": res?.team1Logo?.[0]?.fileId,
                "team2Logo": res?.team2Logo?.[0]?.fileId,
            }
            callSeedMatchDetail = await matchDataInsert(insertMatchPayload)
        }


        let payload: UpsertBattleGroundPayload = {
            ...(params?.id && { "id": params?.id, }),
            "title": res?.title,
            "sportCategory": res?.sportCategory,
            "winningPrizeInSpn": res?.winningPrizeInSpn,
            "statusId": res?.statusId,
            "displayPriority": res?.displayPriority,
            "startAt": res?.startAt?.seconds('0o0').toISOString(),
            "endAt": res?.endAt?.seconds('0o0').toISOString(),
            "displayStartAt": res?.displayStartAt?.seconds('0o0').toISOString(),
            "displayEndAt": res?.displayEndAt?.seconds('0o0').toISOString(),
            ...(!params?.id && { "matchId": callSeedMatchDetail?.data?.data?.matchId }),
            "leagueId": res?.leagueId,
            "team1Color": res?.team1Color,
            "team2Color": res?.team2Color,
            "isInstantRedemption": res?.isInstantRedemption,
        }
        battleUpsert(payload)
            .then(data => {
                if (data?.data?.statusCode === "500") {
                    catchError(data?.data?.message)
                }
                setLoading(null);
                toast.success(data?.data?.message);
                history.push(`${params?.id ? `/BattleGround` : '/BattleGround'}`)
            })
            .catch(e => catchError(e?.message))
    }

    const catchError = (msg: string) => {
        console.log("error--------", msg);
        setLoading(null);
        toast.error(msg ? msg : 'Something went wrong!');
        return false
    }


    //disable start date
    const isDisableStartDate = useCallback((date) => {
        return date && moment(date).isBefore(moment(), 'day')
    }, [form])

    //disable end date
    const isDisableEndDate = useCallback((date, key) => {
        if (!form.getFieldValue(key)) {
            return true
        }
        let checkDate: MomentInput = moment(form.getFieldValue(key)).isBefore(moment(), 'day') ? moment() : moment(form.getFieldValue(key))
        return date && moment(date).isBefore(checkDate, 'day')
    }, [form])

    //status validation for Start Date Time
    const startDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`Start Date Time is required!`));
        }
        if (getFieldValue('endAt')) {
            if (!value.isBefore(getFieldValue('endAt'))) {
                return Promise.reject(new Error(`Start Date Time should be lesser than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        if (getFieldValue('displayStartAt')) {
            if (!value.isAfter(getFieldValue('displayStartAt'))) {
                return Promise.reject(new Error(`Start Date Time should be greater than Display Start Date Time(${getFieldValue('displayStartAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for End Date Time
    const endDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`End Date Time is required!`));
        }
        if (getFieldValue('startAt')) {
            if (!value.isAfter(getFieldValue('startAt'))) {
                return Promise.reject(new Error(`End Date Time should be greater than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        if (getFieldValue('displayEndAt')) {
            if (!value.isBefore(getFieldValue('displayEndAt'))) {
                return Promise.reject(new Error(`End Date Time should be lesser than Display End Date Time(${getFieldValue('displayEndAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for Display Start Date Time
    const displayStartDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`Display Start Date Time is required!`));
        }
        if (getFieldValue('startAt')) {
            if (!value.isBefore(getFieldValue('startAt'))) {
                return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('startAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }

    //status validation for Display End Date Time
    const displayEndDateTimeValidation = (getFieldValue: any, value: any) => {
        if (!value) {
            return Promise.reject(new Error(`Display End Date Time is required!`));
        }
        if (getFieldValue('endAt')) {
            if (!value.isAfter(getFieldValue('endAt'))) {
                return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('endAt').format('DD MM yyyy hh:mm:ss')})`));
            }
        }
        return Promise.resolve();
    }
    const contructFileList = (fileList: string[]) => {
        if (!fileList?.length) return [];
        return fileList?.map(_ => ({
            fileId: _,
            uid: _,
            url: `${config.apiBaseUrl}files/${_}`
        }))
    }
    useEffect(() => {
        if (!battleByIdLoading) {
            let battle = battleById?.allBattlegrounds?.nodes?.[0];

            setUploads({

                team1Logo: contructFileList(battle?.matchByMatchId?.tournamentGroupMemberByParticipant1?.teamLogo ? [battle?.matchByMatchId?.tournamentGroupMemberByParticipant1?.teamLogo] : []),
                team2Logo: contructFileList(battle?.matchByMatchId?.tournamentGroupMemberByParticipant2?.teamLogo ? [battle?.matchByMatchId?.tournamentGroupMemberByParticipant2?.teamLogo] : []),

            })
        }
    }, [battleById, battleByIdLoading]);
    const returnBattleById = useMemo(() => {
        if (battleByIdLoading) return {}
        let battle = battleById?.allBattlegrounds?.nodes?.[0];
        return {
            title: battle?.title,
            sportCategory: battle?.sportCategory,
            winningPrizeInSpn: parseFloat(battle?.winningPrizeInSpn).toFixed(2),
            statusId: battle?.statusId,
            startAt: moment(battle?.startAt + "z"),
            endAt: moment(battle?.endAt + "z"),
            displayPriority: Number(battle?.displayPriority),
            displayStartAt: moment(battle?.displayStartAt + "z"),
            displayEndAt: moment(battle?.displayEndAt + "z"),
            team1Color: battle?.team1Color,
            team2Color: battle?.team2Color,
            team1Logo: contructFileList(battle?.matchByMatchId?.tournamentGroupMemberByParticipant1?.teamLogo ? [battle?.matchByMatchId?.tournamentGroupMemberByParticipant1?.teamLogo] : []),
            team2Logo: contructFileList(battle?.matchByMatchId?.tournamentGroupMemberByParticipant2?.teamLogo ? [battle?.matchByMatchId?.tournamentGroupMemberByParticipant2?.teamLogo] : []),
            isInstantRedemption: battle?.isInstantRedemption,
            leagueId: battle?.leagueId,
            Events: `${battle?.matchByMatchId?.tournamentGroupMemberByParticipant1?.teamName}` + ` vs ` + `${battle?.matchByMatchId?.tournamentGroupMemberByParticipant2?.teamName}`
        }
    }, [battleById, battleByIdLoading]);

    React.useEffect(() => {

        form.setFieldsValue({
            displayPriority: params?.id ? battleById?.allBattlegrounds?.nodes?.[0]?.displayPriority : (battleCount?.allBattlegrounds?.totalCount ?? 0) + 1
        })

    }, [battleCount]);

    //post status validation
    const statusValidations = useMemo(() => {
        return pollFilterStatus(teamStatuses?.teamStatus, returnBattleById?.statusId, returnBattleById?.displayStartAt)
    }, [returnBattleById, teamStatuses])

    //find disable components
    const disabledComponents = useCallback((key: string) => {
        if (!(!!params?.id)) return false
        let validate = statusValidations?.disableComponents?.includes(key)
        return statusValidations?.except ? validate : !validate
    }, [statusValidations])



    useEffect(() => {
        currentSession().then(res => {
            setUserData(res.payload)
        })
    }, []);
    const onCategorySelect = (event: any) => {
        const selectedCategory = teamCategories?.teamCategory?.find(it => it?.category == event)
        // setCategory(selectedCategory?.description)
        getLeagueList(selectedCategory?.description)
    }
    const getLeagueList = (category: any) => {


        const URL = 'https://livescore6.p.rapidapi.com/leagues/v2/list';
        axios.get(URL, {
            'headers': {
                'X-RapidAPI-Key': 'dd653a602cmshb204a38aef98e53p1fda23jsnfd556f0e537d',
                'X-RapidAPI-Host': 'livescore6.p.rapidapi.com'
            },

            params: { Category: category },
        })
            .then((response) => {
                let optionArray = response.data.Ccg.map((league: any) => {
                    return {
                        description: league?.Ccd,
                        leagueName: league?.Csnm,
                        leagueGroups: league?.Stages?.map((group: any) => ({ groupName: group?.Scd }))
                    }
                })
                setLeagueOptions(optionArray)
            })
            .catch((error) => {
                console.log(error);
            });
    }
    const onLeagueSelect = (league: any) => {


        const filterSelectedLeague = leagueOptions?.find((it: any) => it?.leagueName === league);
        setSelectedLeague(filterSelectedLeague)
    }

    const onLeagueStageSelect = (stage: any) => {


        const URL = 'https://livescore6.p.rapidapi.com/matches/v2/list-by-league';
        const spCategory = form.getFieldValue('sportCategory')
        const selectedCategory = teamCategories?.teamCategory?.find(it => it?.category == spCategory)
        const league = form.getFieldValue('leagueId')
        const selectedLeague = leagueOptions?.find((opt: any) => opt?.leagueName === league)


        axios.get(URL, {
            'headers': {
                'X-RapidAPI-Key': 'dd653a602cmshb204a38aef98e53p1fda23jsnfd556f0e537d',
                'X-RapidAPI-Host': 'livescore6.p.rapidapi.com'
            },

            params: { Category: selectedCategory?.description, Ccd: selectedLeague?.description, Scd: stage, Timezone: '-7' },
        }).then(res => {


            let matchData = res?.data?.Stages?.map((stage: any) => {

                return stage?.Events.map((event: any) => {

                    return {
                        matchDetail: event,
                        teamName: `${event?.T1?.[0]?.Nm}` + ` vs ` + `${event?.T2?.[0]?.Nm}`
                    }

                })
            })


            setMatch(matchData.flat())

        }).catch(err => {
            console.log(err)
        })

    }

    const onEventSelected = (event: any) => {
        const filterEvent = matchs.find((it: any) => it?.teamName === event)

        setMatchSeedData(filterEvent)

    }

    const onValuesChange = (field: any) => {
        if (['team1Logo', 'team2Logo']?.some(_ => field.hasOwnProperty(_))) {
            let key = Object.keys(field)[0];
            setUploads({
                ...uploads,
                [key]: field?.[key]
            })
        }
        setFormData({ ...form.getFieldsValue(true) })
    }
    return (
        <Wrapper style={{ width: '100%' }}>
            <BreadcrumbComp
                backPath='/BattleGround'
                cta={<>
                    <Button
                        type="primary"
                        style={{ marginRight: "16px" }}
                        onClick={() => onBeforeSave()}
                        disabled={loading === "save"}
                    >
                        {params?.id ? 'Update' : 'Add'} Battle Ground
                    </Button>
                    <Button href={`${params?.id ? `/BattleGround` : '/BattleGround'}`}>Discard</Button>
                </>}
                breadCrumbs={[
                    { name: "BattleGround", url: "/BattleGround" },
                    { name: `${params?.id ? 'Update' : 'Add New'} BattleGround`, url: params.id ? `/edit-battle/${params.id}` : "/create-battle" },
                ]}
            />
            {(loading === "save" || battleByIdLoading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <div className='card-wrap'>
                        <Form
                            // onFinish={onSave}
                            form={form}
                            preserve={false}
                            initialValues={
                                params?.id ? returnBattleById : {

                                }
                            }
                            onValuesChange={onValuesChange}

                        >
                            <Card>
                                <Typography className='header'>Basic Info</Typography>
                                <Row gutter={[24, 24]}>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Sports Category"
                                            name="sportCategory"
                                            rules={[
                                                { required: true, message: 'Sports Category is required!' },
                                            ]}
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e) => onCategorySelect(e)}
                                                    disabled={disabledComponents('sportCategory')}

                                                    options={teamCategories?.teamCategory ?? []}
                                                    toFilter="description"
                                                    toStore="category"
                                                    placeholder="Sport Category"
                                                    searchable={true}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Start Date Time"
                                            name={'startAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return startDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabledDate={(value) => isDisableStartDate(value)}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                    disabled={disabledComponents('startAt')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="End Date Time"
                                            name={'endAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return endDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime
                                                    // showTime={Boolean(form.getFieldValue('startAt'))}
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabledDate={(value) => isDisableEndDate(value, 'startAt')}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                    disabled={disabledComponents('endAt')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Display Start Date Time"
                                            name={'displayStartAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return displayStartDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabledDate={(value) => isDisableStartDate(value)}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                    disabled={disabledComponents('displayStartAt')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Display End Date Time"
                                            name={'displayEndAt'}
                                            rules={[
                                                ({ getFieldValue }) => ({
                                                    validator(_, value) {
                                                        return displayEndDateTimeValidation(getFieldValue, value)
                                                    },
                                                }),
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    showTime
                                                    //showTime={Boolean(form.getFieldValue('displayStartAt'))}
                                                    className='datepicker'
                                                    style={{ width: "100%" }}
                                                    disabled={disabledComponents('displayEndAt')}
                                                    disabledDate={(value) => isDisableEndDate(value, 'displayStartAt')}
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showNow={false}
                                                    showSecond={false}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="BattleGround Priority"
                                            name="displayPriority"
                                            className="label-input"
                                            inputElement={
                                                <InputNumber
                                                    type={'number'}
                                                    className='fields'

                                                    disabled={battleCountLoading || disabledComponents('displayPriority')}
                                                    min={1}
                                                    max={(battleCount?.allBattlegrounds?.totalCount ?? 0) + 1}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Winning Prize"
                                            name="winningPrizeInSpn"
                                            className="label-input"

                                            inputElement={
                                                <Input
                                                    placeholder="Type Title"
                                                    disabled={disabledComponents('winningPrizeInSpn')}
                                                    suffix={
                                                        <span
                                                            style={{
                                                                fontSize: 14,
                                                                color: '#4DA1FF',
                                                                fontWeight: '500',
                                                            }}
                                                        >
                                                            {'SPN'}
                                                        </span>
                                                    }
                                                />
                                            }
                                        />
                                    </Col>

                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Title"
                                            name="title"
                                            className="label-input"

                                            inputElement={
                                                <Input
                                                    placeholder="Type Title"
                                                    disabled={disabledComponents('title')}

                                                />
                                            }
                                        />
                                    </Col>
                                    {params?.id && (
                                        <Col xs={24} sm={12} md={7}>
                                            <LabelInput
                                                label="Status"
                                                name="statusId"
                                                inputElement={
                                                    <SelectElement
                                                        onChange={(e: any) => {
                                                            form.setFieldsValue({ statusId: e });
                                                        }}
                                                        options={teamStatusLoading ? [] : statusValidations.options}
                                                        toFilter="status"
                                                        placeholder="Status"
                                                    />
                                                }
                                            />
                                        </Col>
                                    )}

                                    <Col xs={24} sm={12} md={8} style={{ display: 'flex', justifyContent: 'start' }}>
                                        <Form.Item
                                            name="isInstantRedemption"
                                            labelAlign="left"
                                            colon={false}
                                            valuePropName="checked"
                                        >
                                            <Checkbox defaultChecked={false} disabled={disabledComponents('isInstantRedemption')}>
                                                is Instant Redemption
                                            </Checkbox>
                                        </Form.Item>
                                    </Col>

                                </Row>
                            </Card>

                            <Card>
                                <Typography className="header">Match Details</Typography>
                                <Row gutter={[24, 24]} >

                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="League Names"
                                            name="leagueId"

                                            inputElement={
                                                <SelectElement
                                                    onChange={(e) => onLeagueSelect(e)}
                                                    disabled={Boolean(params?.id)}
                                                    options={leagueOptions ?? []}
                                                    toFilter="description"
                                                    toStore='leagueName'
                                                    placeholder="League Name"
                                                    searchable={true}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Stages"
                                            name="leagueStage"

                                            inputElement={
                                                <SelectElement
                                                    onChange={(e) => onLeagueStageSelect(e)}
                                                    disabled={Boolean(params?.id)}
                                                    options={selectedLeague?.leagueGroups ?? []}
                                                    toFilter="groupName"
                                                    toStore='groupName'
                                                    placeholder="Stages"
                                                    searchable={true}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={8}>
                                        <LabelInput
                                            label="Matchs"
                                            name="Events"
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e) => onEventSelected(e)}
                                                    options={matchs ?? []}
                                                    toFilter="teamName"
                                                    disabled={Boolean(params?.id)}
                                                    // toStore='matchDetail'
                                                    placeholder="Matchs"
                                                    searchable={true}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={8} md={4}>
                                        <LabelInput
                                            label="Team 1 Logo"
                                            name="team1Logo"
                                            className="label-input"


                                            inputElement={
                                                <Upload
                                                    fileList={uploads?.team1Logo}
                                                    maxCount={1}
                                                    disabled={Boolean(params?.id) || disabledComponents('team1Logo')}
                                                    accept="image/png, image/jpeg"
                                                    beforeUpload={beforeUpload}
                                                />
                                            }
                                        />
                                    </Col>

                                    <Col xs={24} sm={8} md={4}>
                                        <LabelInput
                                            label="Team 2 Logo"
                                            name="team2Logo"
                                            className="label-input"

                                            inputElement={
                                                <Upload
                                                    fileList={uploads?.team2Logo}
                                                    maxCount={1}
                                                    disabled={Boolean(params?.id) || disabledComponents('team2Logo')}
                                                    accept="image/png, image/jpeg"
                                                    beforeUpload={beforeUpload}
                                                />
                                            }
                                        />
                                    </Col>

                                    <Col xs={24} sm={8} md={8}>
                                        <LabelInput
                                            label="Team 1 Color"
                                            name="team1Color"
                                            inputElement={
                                                <Input
                                                    type="color"
                                                    disabled={Boolean(params?.id)}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={8} md={8}>
                                        <LabelInput
                                            label="Team 2 Color"
                                            name="team2Color"
                                            inputElement={
                                                <Input
                                                    type="color"
                                                    disabled={Boolean(params?.id)}
                                                />
                                            }
                                        />
                                    </Col>

                                </Row>

                            </Card>

                        </Form>


                    </div>

                </>)}
        </Wrapper>
    )
}

export default UpsertBattle;