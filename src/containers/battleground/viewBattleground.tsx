import React, { useMemo, useState } from 'react';
import { Avatar, Button, Card, Col, Input, Radio, Row, Space, Spin, TablePaginationConfig, Typography, Form } from 'antd';
import BreadcrumbComp from 'components/common/breadcrumb';
import LabelValue from 'components/common/LabelValue';
import { useParams } from 'react-router-dom';
import styled from 'styled-components/macro';
import { MatchCard } from 'components/contests';
import { useGetBattleGroundByIdQuery } from 'generated/pgraphql';
import { ternaryOperator } from 'utils';

import { useDebounce } from 'hooks/useDebounce';
import moment from 'moment';


interface ViewBattleProps { }

const Wrapper = styled('div')`
    width:100%;
    padding:40px;
    background-color: #F5F5F7;
    .ant-card{
        margin-top: 24px;
    }
    & .table-title{
        color:#000000;
        font-size: 16px;
        font-weight: 600;
        margin-bottom:16px;
    }
    .row-label-value{
        .ant-col > div{
            gap:16px;
        }
    }
    & .tabs{
        width:100%;
        margin-bottom:16px;
        display:flex;
        gap:12px;
        margin-top:24px;
        align-items:center;
        transition:0.5s;
        & .tab-item{
            flex:1;
            cursor:pointer;
            padding:8px;
            text-align: center;
            background: rgba(198, 198, 198, 0.6);
            border-radius: 4px;
            font-weight: 600;
            font-size: 16px;
            color:rgba(5, 31, 36, 0.6);
        }
        & .active{
            color:#4DA1FF;
            background: #FFFFFF;
        }
    }   
    & .radio-readonly{
        pointer-events: none;
    }
    & .card-chips{  
        & .ant-card-bordered{
            border-radius:8px;
            & .ant-card-body{
                display:flex;
                flex-direction:column;
                gap:8px;
                & .ant-typography{
                    &:first-of-type{
                        color:#A3A3A3;
                        font-weight: 500;
                        font-size: 16px;
                    }
                    &:last-of-type{
                        color:#4DA1FF;
                        line-height:1;
                        font-weight: 700;
                        font-size: 24px;
                    }
                }
            }
        }
    }
    & .image-col{
        display:flex;
        flex-direction:column;
        gap:16px;
        & .ant-typography{
            &:first-of-type{
                font-size: 15px;
                line-height: 18px;
                color: rgba(5,31,36,0.6);
                margin-bottom: 6px;
            }     
        }
    }
    & .search-input{
        border: 1px solid rgba(5, 31, 36, 0.1);
        border-radius: 5px;
        & .ant-input-prefix{
            color:#97a0c3;
        }
    }
    & .table-list{
        width: 100%;
        margin-bottom: 16px;
        & .ant-space-item:first-of-type{
            flex:1;
            & .table-title{
                margin-bottom: 0;
            }
        }
    }
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

const formatDate = (date: string) => date ? moment(date + 'z').format('MMMM D YYYY, HH:mm') : '-'

const ViewBattleground = (props: ViewBattleProps): JSX.Element => {

    //debounce hook
    const debounce = useDebounce();
    const params: { id?: string } = useParams();

    //get battlegroundBy id
    const { data, loading } = useGetBattleGroundByIdQuery({
        ...(params?.id && { variables: { id: params?.id }, }),
        skip: !(!!params.id),
        fetchPolicy: 'network-only',
    });

    const battle = useMemo(() => data?.allBattlegrounds?.nodes?.[0], [data]);
    const [form] = Form.useForm();



    return (
        <Wrapper>
            <BreadcrumbComp
                backPath='/BattleGround'
                cta={<>
                    {
                        battle?.statusId && !['INACTIVE', 'DISCONTINUED', 'CLOSED', 'PENDING'].includes(battle?.statusId) &&
                        <Button type="primary" href={`/edit-battle/${params?.id}`}>Edit</Button>
                    }
                </>}
                breadCrumbs={[
                    { name: "Battleground", url: "/BattleGround" },
                    { name: battle?.title ?? '-', url: `/view-battle/${params?.id}` },
                ]}
            />
            {(loading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <Card>
                        <Typography className='table-title'>Basic Info</Typography>
                        <Row gutter={[24, 40]} className="row-label-value">
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Select Sport"
                                    value={ternaryOperator(battle?.sportCategory, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="ID"
                                    value={ternaryOperator(battle?.id, '-')}
                                />
                            </Col>

                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Group Status"
                                    value={ternaryOperator(battle?.teamStatusByStatusId?.description, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Start Date & Time"
                                    value={formatDate(battle?.startAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="End Date & Time"
                                    value={formatDate(battle?.endAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Display Start Date & Time"
                                    value={formatDate(battle?.displayStartAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Display End Date & Time"
                                    value={formatDate(battle?.displayEndAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Battleground Priority"
                                    value={(battle?.displayPriority)}
                                />
                            </Col>

                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Winning Prize"
                                    value={parseFloat(battle?.winningPrizeInSpn).toFixed(2)}
                                />
                            </Col>


                        </Row>
                    </Card>
                    <Card>
                        <Typography className="header">Match Details</Typography>
                        <MatchCard
                            form={form}
                            data={{
                                ...battle
                                    ?.matchByMatchId,
                                team1Color:
                                    battle
                                        ?.team1Color,
                                team2Color:
                                    battle
                                        ?.team2Color,
                                battleLeague: battle?.leagueId
                            }}
                            isView={true}
                        />
                    </Card>

                </>
            )}
        </Wrapper>
    )
}

export default ViewBattleground