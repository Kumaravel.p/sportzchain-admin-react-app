import { Button, Card } from 'antd';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import LeaderboardInfo from 'components/leaderboard/LeaderboardInfo';
import { useGetLeaderboardInfoQuery } from 'generated/graphql';
import { Link } from 'react-router-dom';
import StatCard from 'components/common/cards/StatCard';

const StoWrapper = styled.div`
  width: 100%;
  padding: 40px;
  .ant-card {
    width: 303px;
    .ant-card-head {
      border-bottom: none;
    }
  }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-sto {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
    .ant-btn.create-sto-button {
      /* 
      background: #07697d;
      color: #fff;
      box-sizing: border-box;
      border-radius: 5px;
      display: flex;
      align-items: center;
      column-gap: 10px; */
      padding: 15px;
      font-weight: bold;
      height: fit-content;

      border: 1px solid rgba(7, 105, 125, 0.25);
      box-sizing: border-box;
      border-radius: 5px;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */
    white-space: nowrap;
    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }
  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
`;

interface StoProps {}

const Sto = ({}: StoProps): JSX.Element => {
  const { data: leaderboardInfo } = useGetLeaderboardInfoQuery();
  return (
    <StoWrapper>
      <FlexRowWrapper className="user-text-create-wrapper">
        <Text className="t-sto">Sto</Text>
        <Link to="/rewards">
          <Button className="create-sto-button"> Create STO</Button>
        </Link>
      </FlexRowWrapper>

      <FlexRowWrapper className="cards-wrapper">
        <StatCard
          title="New users"
          cta={<MoreIcon />}
          value={<div className="green">400</div>}
          supportingValueText="This Month"
        />
        <StatCard
          title="New users"
          cta={<MoreIcon />}
          value={<div className="green">400</div>}
          supportingValueText="This Month"
        />{' '}
        <StatCard
          title="New users"
          cta={<MoreIcon />}
          value={<div className="green">400</div>}
          supportingValueText="This Month"
        />
      </FlexRowWrapper>

      <LeaderboardInfo
        leaderboardInfo={leaderboardInfo?.platformLeaderboard ?? []}
      />
    </StoWrapper>
  );
};

export default Sto;
