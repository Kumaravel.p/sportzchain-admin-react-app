import React, { useEffect, useMemo, useState } from 'react';
import { Row, Col, Typography, DatePicker, Input, Card, Space, Button, TablePaginationConfig, Modal, Form, Spin, FormInstance } from 'antd';
import SelectElement from 'components/common/SelectElement';
import styled from 'styled-components/macro';
import { DeleteOutlined } from '@ant-design/icons';
import CustomTable from 'components/customTable';
import { useGetAllActivitiesQuery, useGetRwrdActivitiesLazyQuery, useGetRwrdActivitiesQuery, useGetRwrdCategoriesQuery, useGetRwrdLineItemQuery } from 'generated/pgraphql';
import LabelInput from 'components/common/LabelInput';
import BreadcrumbComp from 'components/common/breadcrumb';
import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import moment from 'moment';
import { format } from 'date-fns';
import { toast } from 'react-toastify';
import { useHistory, useParams } from 'react-router-dom';
import { range } from 'utils';
import currentSession from 'utils/getAuthToken';
import { activityPointCreation, activityPointUpdation } from 'apis/activityPoints';
import { ActivityModal } from 'components/modals/activityPoint';
import { ActivityPointCreationProps } from 'ts/interfaces/activityPoints';
import { activityColumns, ActivityType, contructActivityDetails, contructActivityList, contructNoteList, notesColumns, NotesType } from './utils';

const Wrapper = styled('div')`
width:100%;
padding:40px;
background-color: #F5F5F7;
& .card-wrap{
    margin-top: 16px;
    & .ant-card-bordered{
        border-radius: 10px;
        margin-bottom: 16px;
        & .edit-icon{
            & path{
                fill: #4DA1FF;
            }
        }
        & .delete-icon{
            & path{
                fill: red;
            }
        }
    }
}
& .header{
    color:#051F24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
}
& .label{
    font-weight: 400;
    font-size: 14px;
    color:#051F2499;
    margin-bottom: 16px;
    & span{
        color:red;
        margin-left: 8px;
    }
}
& .ant-select-selector{
    border:1px solid #d9d9d9 !important;
    border-radius: 2px !important;
}
& .space{
    width:100%;
    padding-inline: 16px;
    margin-bottom: 16px;
    & .ant-space-item:first-of-type{
        flex:1;
    }
    & .table-title{
        color:#000000;
        font-size: 16px;
        font-weight: 600;
    }
    & .ant-btn-text{
        font-weight: 600;
        font-size: 16px;
        color: #4DA1FF;
    }
}
`;

const ModalWrapper = styled.div`
& .label{
    font-weight: 400;
    font-size: 14px;
    color:#051F2499;
    margin-bottom: 12px;
}
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

interface ActivityPointProps { }

type RwrdCategoriesOptionType = {
    category: string;
    id: number;
    __typename: number;
}[];

type RwrdActivitiesOptionType = {
    description: string;
    id: number;
    __typename: number;
}[];


const CreateActivityPoint = (props: ActivityPointProps): JSX.Element => {

    const [form] = Form.useForm();
    const [activityForm] = Form.useForm();
    const [noteForm] = Form.useForm();
    const params: { id?: string } = useParams();
    const history = useHistory();

    const [getActivites, {
        data: RwrdActivitiesOption,
        loading: RwrdActivitiesOptionLoading
    }] = useGetRwrdActivitiesLazyQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const {
        data: RwrdCategoriesOption,
        loading: RwrdCategoriesOptionLoading
    } = useGetRwrdCategoriesQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const {
        data: activityPointDetailsData,
        loading: activityPointDetailsLoading,
    } = useGetAllActivitiesQuery({
        ...(params?.id && { variables: { id: params?.id }, }),
        skip: !(!!params.id),
        fetchPolicy: "no-cache",
    });

    const [loading, setLoading] = useState<boolean>(false);
    const [visibleModal, setVisibleModal] = useState<'notes' | 'activities' | null>(null);
    const [editIndex, setEditIndex] = useState<number>(-1);
    const [activityList, setActivityList] = useState<ActivityType[] | []>([]);
    const [notesList, setNotesList] = useState<NotesType[] | []>([]);
    const [userData, setUserData] = React.useState<any>({});


    //Fetching activity details
    React.useEffect(() => {
        let data = activityPointDetailsData?.allActivities?.nodes?.[0];
        if (data) {
            setLoading(true);
            let { activities = [], notes } = contructActivityDetails(data);
            let categoryId = activityPointDetailsData?.allActivities?.nodes?.[0]?.masterRwrdCategoryByCategoryId?.id;
            if(categoryId){
                getActivites({
                    variables: {
                        categoryId
                    }
                })
            }
            setActivityList(activities);
            setNotesList(notes);
            setLoading(false);
        }
    }, [activityPointDetailsData])

    useEffect(() => {
        currentSession().then(res => {
            setUserData(res.payload)
        })
    }, []);

    const formatDate = (date: Date | number, dateFormat: string = "dd MMM, yy", delimiter: string = "",) => date ? format(new Date(date), dateFormat) : delimiter;

    //contruct rules table
    const activityDataSource: ActivityType[] = useMemo(() => {
        if (!activityList?.length) return [];
        return activityList?.map((rule: any) => ({
            status: rule?.status?.label ?? '-',
            ruleName: rule?.ruleName,
            activityPointsPerUser: rule?.activityPointsPerUser,
            timePeriod: `${formatDate(rule?.startDateTime)} - ${formatDate(rule?.endDateTime)}`,
            actions: (rowRecord: any, rowIndex: number) => (
                <Space size={12}>
                <EditIcon className='edit-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "activities", "edit")} />
                <DeleteOutlined className='delete-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "activities", "delete")} />
               </Space>
            )
        }))
        // eslint-disable-next-line
    }, [activityList])

    //contruct notes table
    const notesDataSource: NotesType[] = useMemo(() => {
        if (!notesList?.length) return []
        return contructNoteList(notesList, (rowRecord: any, rowIndex: number) => (
            <Space size={12}>
                <EditIcon className='edit-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "notes", "edit")} />
                <DeleteOutlined className='delete-icon' onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "notes", "delete")} />
            </Space>
        ))
        // eslint-disable-next-line
    }, [notesList])

    //on click table icons
    const onClickIcons = (e: any, rowRecord: any, rowIndex: number, key: 'notes' | 'activities' | null = null, from: string,data?:any) => {
        e.stopPropagation()
        let form: FormInstance, list: any[] = [], setState: Function;
        if (key === "activities") {
            form = activityForm;
            list = activityList;
            setState = setActivityList;
        }
        else {
            form = noteForm;
            list = notesList;
            setState = setNotesList;
        }
        if (from === "edit") {
            form.setFieldsValue(list[rowIndex]);
            setVisibleModal(key);
            setEditIndex(rowIndex)
        }
        else if (from === "delete") {
            setState(list?.filter((_: any, index: number) => index !== rowIndex))
        }
    }

    const onChangeTable = () => { }

    const onCancelModal = () => {
        setVisibleModal(null)
        setEditIndex(-1)
    }

    //on save or update check validations
    const onSubmitModal = () => {

        let list: any[] = [], setState: Function, form: FormInstance, keys: string[] = [];

        const checkError = (array: string[] = [], form: FormInstance) => (array.some((key: string) => !form.getFieldValue(key)))

        if (visibleModal === "activities") {
            keys = ['ruleName', 'status', 'activityPointsPerUser', 'startDateTime', 'endDateTime']
            list = activityList;
            form = activityForm;
            setState = setActivityList;
        }
        else {
            keys = ['title', 'description'];
            list = notesList;
            form = noteForm;
            setState = setNotesList;
        }

        form.submit();

        if (!checkError(keys, form)) {
            if (editIndex > -1) {
                let copyList = [...list];
                copyList[editIndex] = { ...copyList[editIndex], ...form.getFieldsValue() }
                setState(copyList)
                setEditIndex(-1);
            }
            else {
                if (visibleModal === "activities") {
                    setState([...list, form.getFieldsValue()])
                }
                else {
                    setState([
                        ...list, {
                            ...form.getFieldsValue(),
                            addedBy: userData?.name,
                            addedDate: moment()
                        }])
                }
            }
            setVisibleModal(null);
        }
    }

    //on save or update
    const onSave = () => {

        if (activityList?.length) {
            let activityDetails = form.getFieldsValue();

            const dateToISOString = (date: Date): string => moment(date).seconds('0o0' as unknown as number).toISOString();
            setLoading(true);
            let payload: ActivityPointCreationProps = {
                ...(params?.id && { id: params.id }),
                startAt: dateToISOString(activityDetails?.startDateTime),
                endAt: dateToISOString(activityDetails?.endDateTime),
                description: activityDetails?.activityDescription,
                categoryId: activityDetails?.category,
                subCategoryId: activityDetails?.subCategory,
                rules: activityList?.map((rule: any) => ({
                    ruleName: rule?.ruleName,
                    activityPointPerUser: rule?.activityPointsPerUser,
                    startAt: dateToISOString(rule.startDateTime),
                    endAt: dateToISOString(rule.endDateTime),
                    maxLimit: (rule?.maxLimit && typeof Number(rule?.maxLimit)) ? Number(rule.maxLimit) : null,
                    duration: (rule?.duration && typeof Number(rule?.duration)) ? Number(rule.duration) : null,
                    statusId: rule.status.value,
                    id:rule.id
                })),
                notes: notesList?.map((note: any) => ({
                    title: note?.title,
                    description: note?.description,
                    addedDate: dateToISOString(note?.addedDate),
                    addedby: note?.addedBy,
                }))
            }

            let api = params?.id ? activityPointUpdation : activityPointCreation;
            api(payload)
                .then(res => {
                    if (res.data.statusCode === "500") {
                        toast.error(res.data.message);
                        return false
                    }
                    setLoading(false);
                    toast.success(res?.data?.message)
                    history.push('/activity-points');
                })
                .catch(e => {
                    console.log(e)
                    setLoading(false);
                    toast.error('Something went wrong')
                })
        }
        else {
            toast.error('Atleast one Activity should be added. !!')
        }
    }

    const onValuesChange = (field: any) => {
        if (field?.category) {
            getActivites({
                variables: {
                    categoryId: field?.category
                }
            })
            form.setFieldsValue({ subCategory: null })
        }
    }

    const activityDetails = activityPointDetailsData?.allActivities?.nodes?.[0];
    //setting activity details to form
    const returnInitialValues = useMemo(() => {
        return {
            category: activityDetails?.masterRwrdCategoryByCategoryId?.id,
            subCategory: activityDetails?.masterRwrdActivityBySubCategoryId?.id,
            startDateTime: moment(activityDetails?.startAt?.concat('z')),
            endDateTime: moment(activityDetails?.endAt?.concat('z')),
            activityDescription: activityDetails?.description
        }
    }, [activityDetails])

    return (
        <Wrapper style={{ width: '100%' }}>
            <BreadcrumbComp
                backPath='/activity-points'
                cta={<>
                    <Button
                        type="primary"
                        style={{ marginRight: "16px" }}
                        onClick={form.submit}
                        disabled={loading}
                    >
                        {params?.id ? 'Update' : 'Add'} Activity
                    </Button>
                    <Button href='/activity-points'>Discard</Button>
                </>}
                breadCrumbs={[
                    { name: "Activity Points", url: "/activity-points" },
                    { name: `${params?.id ? 'Update' : 'Add New'} Activity`, url: params.id ? `/edit-activity-point/${params.id}` : "/create-activity-point" },
                ]}
            />
            {(loading || activityPointDetailsLoading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <div className='card-wrap'>
                        <Card>
                            <Typography className='header'>Activity Details</Typography>
                            <Form
                                onFinish={onSave}
                                form={form}
                                preserve={false}
                                initialValues={
                                    params?.id ? returnInitialValues : {}
                                }
                                onValuesChange={onValuesChange}
                            >
                                <Row gutter={[24, 24]}>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="Category"
                                            name="category"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Category is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                    options={
                                                        RwrdCategoriesOption?.allMasterRwrdCategories?.nodes as unknown as RwrdCategoriesOptionType ?? []
                                                    }
                                                    placeholder="Select Category"
                                                    searchable={true}
                                                    toFilter="category"
                                                    toStore='id'
                                                    loading={RwrdCategoriesOptionLoading}
                                                    disabled={RwrdCategoriesOptionLoading}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="Sub-category"
                                            name="subCategory"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Sub-category is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <SelectElement
                                                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => null}
                                                    options={RwrdActivitiesOption?.allMasterRwrdActivities?.nodes as unknown as RwrdActivitiesOptionType ?? []}
                                                    placeholder="Select Sub-category"
                                                    searchable={true}
                                                    toFilter="description"
                                                    toStore='id'
                                                    // mode="multiple"
                                                    loading={RwrdActivitiesOptionLoading}
                                                    disabled={RwrdActivitiesOptionLoading}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="Start Date & Time"
                                            name="startDateTime"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Start Date & Time is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showTime
                                                    showNow={false}
                                                    showSecond={false}
                                                    style={{ width: '100%' }}
                                                    disabledDate={d => d && d < moment().subtract(1, 'days')}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24} sm={12} md={6}>
                                        <LabelInput
                                            label="End Date & Time"
                                            name="endDateTime"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'End Date & Time is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <DatePicker
                                                    format={"DD-MM-YYYY hh:mm"}
                                                    showTime
                                                    showNow={false}
                                                    showSecond={false}
                                                    style={{ width: '100%' }}
                                                    disabledDate={d => (form.getFieldValue('startDateTime') ? d && d < moment(form.getFieldValue('startDateTime')) : true)}
                                                    disabledHours={() => form.getFieldValue('startDateTime') ? [] : range(0, 24)}
                                                    disabledMinutes={() => form.getFieldValue('startDateTime') ? [] : range(0, 59)}
                                                    disabledSeconds={() => form.getFieldValue('startDateTime') ? [] : range(0, 59)}
                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Activity Description"
                                            name="activityDescription"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Activity Description is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Input
                                                    placeholder="Description"
                                                />
                                            }
                                        />
                                    </Col>
                                </Row>
                            </Form>
                        </Card>
                        <Card>
                            <Space className='space'>
                                <Typography className='table-title'>Activities ({activityList?.length})</Typography>
                                <Button type="text" onClick={() => setVisibleModal("activities")}>+ Add New Activity</Button>
                            </Space>
                            <CustomTable
                                columns={activityColumns}
                                dataSource={activityDataSource}
                                hidePagination={true}
                                onChangeTable={onChangeTable}
                            />
                        </Card>
                        <Card>
                            <Space className='space'>
                                <Typography className='table-title'>Notes ({notesList?.length})</Typography>
                                <Button type="text" onClick={() => setVisibleModal('notes')}>+ Add New Note</Button>
                            </Space>
                            <CustomTable
                                columns={notesColumns}
                                dataSource={notesDataSource}
                                hidePagination={true}
                                onChangeTable={onChangeTable}
                            />
                        </Card>
                    </div>
                </>)}
            <Modal
                title={visibleModal === "activities" ? 'Activity' : 'Notes'}
                centered
                visible={visibleModal ? ['activities', 'notes']?.includes(visibleModal) : false}
                onOk={() => onSubmitModal()}
                onCancel={() => onCancelModal()}
                width={visibleModal === "activities" ? 1000 : 500}
                destroyOnClose={true}
            >
                <ModalWrapper>
                    {visibleModal === "activities" ? (
                        <ActivityModal
                            form={activityForm}
                        />
                    ) : (
                        <div>
                            <Form
                                // onFinish={onSaveNoteForm}
                                form={noteForm}
                                preserve={false}
                                initialValues={{}}
                            >
                                <Row gutter={[1 / 2, 1 / 2]}>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Note Title"
                                            name="title"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Title is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Input
                                                    placeholder="Enter Title"

                                                />
                                            }
                                        />
                                    </Col>
                                    <Col xs={24}>
                                        <LabelInput
                                            label="Enter Description"
                                            name="description"
                                            className="label-input"
                                            required
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Description is required!',
                                                },
                                            ]}
                                            inputElement={
                                                <Input
                                                    placeholder="Enter Description"
                                                />
                                            }
                                        />
                                    </Col>
                                </Row>
                            </Form>
                        </div>
                    )}
                </ModalWrapper>
            </Modal>
        </Wrapper>
    )
}

export default CreateActivityPoint;