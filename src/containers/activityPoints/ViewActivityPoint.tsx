import React, { useCallback, useMemo, useState } from 'react';
import { Button, Card, Col, Modal, Row, Spin, Typography } from 'antd';
import BreadcrumbComp from 'components/common/breadcrumb';
import LabelValue from 'components/common/LabelValue';
import { useGetAllActivitiesQuery } from 'generated/pgraphql';
import { useParams } from 'react-router-dom';
import styled from 'styled-components/macro';
import moment from 'moment';
import { activityColumns, ActivityType, contructActivityDetails, contructActivityList, contructNoteList, notesColumns, NotesType } from './utils';
import { EyeOutlined } from '@ant-design/icons';
import CustomTable from 'components/customTable';

interface ViewActivityPointProps { }

const Wrapper = styled('div')`
    width:100%;
    padding:40px;
    background-color: #F5F5F7;
    .ant-card{
        margin-top: 24px;
    }
    & .table-title{
        color:#000000;
        font-size: 16px;
        font-weight: 600;
        margin-bottom:16px;
    }
    .row-label-value{
        .ant-col > div{
            gap:16px;
        }
    }
`;


const ModalWrapper = styled.div`
& .label{
        font-weight: 400;
        font-size: 14px;
        color:#051F2499;
        margin-bottom: 12px;
    }
    .row-label-value{
        .ant-col > div{
            gap:16px;
        }
    }
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

const ViewActivityPoint = (props: ViewActivityPointProps) => {

    const params: { id?: string } = useParams();

    const { data, loading: activityLoading } = useGetAllActivitiesQuery({
        ...(params?.id && { variables: { id: params?.id }, }),
        skip: !(!!params.id),
        fetchPolicy: 'network-only',
    });

    const [activityList, setActivityList] = useState<ActivityType[] | []>([]);
    const [notesList, setNotesList] = useState<NotesType[] | []>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [visibleModal, setVisibleModal] = useState<'notes' | 'activities' | null>(null);
    const [viewData, setViewData] = useState<any>({});


    //Fetching activity details
    React.useEffect(() => {
        let activityDetails = data?.allActivities?.nodes?.[0];
        if (activityDetails) {
            setLoading(true);
            let { activities = [], notes } = contructActivityDetails(activityDetails)
            setActivityList(activities);
            setNotesList(notes);
            setLoading(false);
        }
    }, [data])

    //contruct rules table
    const activityDataSource: ActivityType[] = useMemo(() => {
        if (!activityList?.length) return [];
        return contructActivityList(activityList, (rowRecord: any, rowIndex: number) => (
            <EyeOutlined style={{ color: "#1890ff" }} onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "activities",)} />
        ))
        // eslint-disable-next-line
    }, [activityList])

    //contruct notes table
    const notesDataSource: NotesType[] = useMemo(() => {
        if (!notesList?.length) return []
        return contructNoteList(notesList, (rowRecord: any, rowIndex: number) => (
            <EyeOutlined style={{ color: "#1890ff" }} onClick={(e) => onClickIcons(e, rowRecord, rowIndex, "notes",)} />
        ))
        // eslint-disable-next-line
    }, [notesList])

    const onClickIcons = (e: any, rowRecord: any, rowIndex: number, key: 'notes' | 'activities' | null = null) => {
        e.stopPropagation();
        if (key) {
            let data = key === "activities" ? activityList : notesList
            setViewData({
                [key]: data[rowIndex]
            })
            setVisibleModal(key)
        }
    }

    const onChangeTable = () => { }

    const onCancelModal = () => {
        setVisibleModal(null);
        setViewData({});
    }

    let activityDetails = data?.allActivities?.nodes?.[0];

    const formatDate = useCallback((date: string) => {
        if (date && moment(date).isValid()) {
            return moment(date)?.format('MMM DD, yyyy, hh:mm')
        }
        return '-'
    }, [activityDetails,viewData]);

    return (
        <Wrapper>
            <BreadcrumbComp
                backPath='/activity-points'
                cta={<>
                    <Button type="primary" href={`/edit-activity-point/${params?.id}`}>Edit</Button>
                </>}
                breadCrumbs={[
                    { name: "Activity Points", url: "/activity-points" },
                    { name: activityDetails?.masterRwrdCategoryByCategoryId?.category ?? '-', url: `/view-activity-point/${params?.id}` },
                ]}
            />
            {(loading || activityLoading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <Card>
                        <Typography className='table-title'>Activity Details</Typography>
                        <Row gutter={[24, 24]} className="row-label-value">
                            <Col xs={24} sm={12} md={6}>
                                <LabelValue
                                    field="Category"
                                    value={activityDetails?.masterRwrdCategoryByCategoryId?.category ?? '-'}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={6}>
                                <LabelValue
                                    field="Sub-category"
                                    value={activityDetails?.masterRwrdActivityBySubCategoryId?.description ?? '-'}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={6}>
                                <LabelValue
                                    field="Start Date & Time"
                                    value={formatDate(activityDetails?.startAt+'z')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={6}>
                                <LabelValue
                                    field="End Date & Time"
                                    value={formatDate(activityDetails?.endAt+'z')}
                                />
                            </Col>
                            <Col xs={24}>
                                <LabelValue
                                    field="Activity Description"
                                    value={activityDetails?.description ?? '-'}
                                />
                            </Col>
                        </Row>
                    </Card>
                    <Card>
                        <Typography className='table-title'>Activities ({activityList?.length})</Typography>
                        <CustomTable
                            columns={activityColumns}
                            dataSource={activityDataSource}
                            hidePagination={true}
                            onChangeTable={onChangeTable}
                        />
                    </Card>
                    <Card>
                        <Typography className='table-title'>Notes ({notesList?.length})</Typography>
                        <CustomTable
                            columns={notesColumns}
                            dataSource={notesDataSource}
                            hidePagination={true}
                            onChangeTable={onChangeTable}
                        />
                    </Card>
                </>
            )}
            <Modal
                title={visibleModal === "activities" ? 'Activity' : 'Notes'}
                centered
                visible={visibleModal ? ['activities', 'notes']?.includes(visibleModal) : false}
                onOk={() => onCancelModal()}
                onCancel={() => onCancelModal()}
                width={visibleModal === "activities" ? 1000 : 500}
                destroyOnClose={true}
            >
                <ModalWrapper>
                    {visibleModal === "activities" ? (
                        <div>
                            <Row gutter={[32, 32]} className="row-label-value">
                                <Col xs={24} sm={12} md={8}>
                                    <LabelValue
                                        field="Rule Name"
                                        value={viewData?.activities?.ruleName ?? '-'}
                                    />
                                </Col>
                                <Col xs={24} sm={12} md={8}>
                                    <LabelValue
                                        field="Rule Status"
                                        value={viewData?.activities?.status?.label ?? '-'}
                                    />
                                </Col>
                                <Col xs={24} sm={12} md={8}>
                                    <LabelValue
                                        field="Points Per User"
                                        value={viewData?.activities?.activityPointsPerUser ?? '-'}
                                    />
                                </Col>
                                <Col xs={24} sm={12} md={8}>
                                    <LabelValue
                                        field="Start Date & Time"
                                        value={formatDate(viewData?.activities?.startDateTime)}
                                    />
                                </Col>
                                <Col xs={24} sm={12} md={8}>
                                    <LabelValue
                                        field="End Date & Time"
                                        value={formatDate(viewData?.activities?.endDateTime)}
                                    />
                                </Col>
                            </Row>
                            <Typography.Title level={4} style={{ marginBlock: '32px' }}>Restriction Limit</Typography.Title>
                            <Row gutter={[32, 32]} className="row-label-value">
                                <Col xs={24} sm={12} md={8}>
                                    <LabelValue
                                        field="Max Limit (No. of times)"
                                        value={viewData?.activities?.maxLimit ?? '-'}
                                    />
                                </Col>
                                <Col xs={24} sm={12} md={8}>
                                    <LabelValue
                                        field="Duration"
                                        value={`${viewData?.activities?.duration ?? 0} ${Number(viewData?.activities?.duration) > 1 ? 'Days' : 'Day'}`}
                                    />
                                </Col>
                            </Row>
                        </div>
                    ) : (
                        <div>
                            <Row gutter={[32, 32]} className="row-label-value">
                                <Col xs={24}>
                                    <LabelValue
                                        field="Note Title"
                                        value={viewData?.notes?.title ?? '-'}
                                    />
                                </Col>
                                <Col xs={24}>
                                    <LabelValue
                                        field="Description"
                                        value={viewData?.notes?.description ?? '-'}
                                    />
                                </Col>
                                <Col xs={24}>
                                    <LabelValue
                                        field="Added By"
                                        value={viewData?.notes?.addedBy ?? '-'}
                                    />
                                </Col>
                                <Col xs={24}>
                                    <LabelValue
                                        field="Added At"
                                        value={formatDate(viewData?.notes?.addedDate)}
                                    />
                                </Col>
                            </Row>
                        </div>
                    )}
                </ModalWrapper>
            </Modal>
        </Wrapper>
    )
}

export default ViewActivityPoint