import moment from "moment";

export interface ActivityType {
    status: string;
    ruleName: string;
    activityPointsPerUser: string;
    timePeriod: string;
    actions?: any;
}

export interface NotesType {
    index: number;
    note: string;
    addedBy: string;
    addedDate: string;
    actions: any;
}

export const activityColumns = [
    { title: 'Status', dataIndex: "status", component: 'status', },
    { title: 'Rule Name', dataIndex: "ruleName", sorter: (a: any, b: any) => a.ruleName.localeCompare(b.ruleName) },
    { title: 'Activity Points Per User', dataIndex: "activityPointsPerUser" },
    { title: 'Time Period', dataIndex: "timePeriod" },
    { title: 'Actions', dataIndex: "actions", component: 'icons', align: 'center' },
]

export const notesColumns = [
    { title: '#', dataIndex: "index", },
    { title: 'Note', dataIndex: "note", sorter: (a: any, b: any) => a.note.localeCompare(b.note) },
    { title: 'Added By', dataIndex: "addedBy" },
    { title: 'Added Date', dataIndex: "addedDate" },
    { title: 'Actions', dataIndex: "actions", component: 'icons', align: 'center' },
]

export const formatDate = (date: Date, dateFormat: string = "DD MMM, yy", delimiter: string = "",) => {
    if (date && moment(date).isValid()) {
        return moment(date).format(dateFormat)
    }
    return delimiter
}


export const contructActivityList = (data: any[], actions: any) => {
    if (!data?.length) return []
    return data?.map((rule: any) => ({
        status: rule?.status?.label ?? '-',
        ruleName: rule?.ruleName,
        activityPointsPerUser: rule?.activityPointsPerUser,
        timePeriod: `${formatDate(rule?.startDateTime)} - ${formatDate(rule?.endDateTime)}`,
        actions
    }))
}

export const contructNoteList = (data: any[], actions: any) => {
    if (!data?.length) return []
    return data?.map((note: any, index: number) => ({
        index: index + 1,
        note: note?.title ?? '-',
        addedBy: note?.addedBy,
        addedDate: `${formatDate(note?.addedDate, 'DD MMM, yy hh:mm')}`,
        actions
    }))
}

export const contructActivityDetails = (data: any) => {
    let activities: any[] = data?.activityRulesByActivityId?.nodes?.map((rule: any) => ({
        status: {
            label: rule?.masterRwrdRuleStatusByStatusId?.status,
            value: rule?.masterRwrdRuleStatusByStatusId?.id,
        },
        ruleName: rule?.ruleName,
        activityPointsPerUser: Number(rule?.activityPointPerUser),
        startDateTime: moment(rule?.startAt?.concat('z')),
        endDateTime: moment(rule?.endAt?.concat('z')),
        maxLimit: rule?.maxLimit,
        duration: rule?.duration,
        id:rule?.id
    }));

    let notes: any[] = data?.activityNotesByActivityId?.nodes?.map((note: any) => ({
        title: note?.name,
        description: note?.notes,
        addedDate: moment(note?.createdDate?.concat('z')),
        addedBy: note?.addedBy
    }))

    return { activities, notes }
}