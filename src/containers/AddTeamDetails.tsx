import React, { useCallback, useState } from 'react';
import { Card, Checkbox, Space, Typography } from 'antd';
import { Form, Input, Row, Col, Divider, Button, DatePicker } from 'antd';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import Teams from "../components/NewTeams/teams";
import Note from "../components/NewTeams/note";
import Document from "../components/NewTeams/document";
import "../index.css";
import AddressModal from '../components/NewTeams/addModal';
import TeamsModal from "../components/NewTeams/teamsModel";
import NoteModal from "../components/NewTeams/addNoteModal";
import DocModal from 'components/NewTeams/docModal';
import { useHistory } from 'react-router-dom';
import LabelInput from 'components/common/LabelInput';
import SelectElement from 'components/common/SelectElement';
import Profile from '../components/NewTeams/profile';
import { teamInsertApi } from "../apis/teamsApi";
import { toast } from 'react-toastify';
import { ReactComponent as EditIcon } from 'assets/icons/edit.svg';
import { useGetAllCountriesQuery, useGetTeamCategoriesQuery, useGetTeamCurrencyQuery, useGetTeamAddressTypesQuery } from 'generated/graphql';
import { useGetAllTeamLeaguesQuery } from 'generated/pgraphql';
import moment from 'moment';
import VideoModal from 'components/NewTeams/videoUrl';
import config from 'config';
import currentSession from 'utils/getAuthToken';
import styled from 'styled-components/macro';
import { useSnapshot } from 'valtio';
import { state } from 'state';


type TeamLeagueOptions = {
  __typename?: "TeamLeague" | undefined;
  nodeId: string;
  leagueName: string;
}[];

const Header = styled.div`
    color: #051f24;
    font-weight: 600;
    font-size: 20px;
    margin-bottom: 24px;
`;

const { Paragraph } = Typography;
const AddTeamDetails = (): JSX.Element => {
  const [form] = Form.useForm();

  // get api's
  const { data: countries } = useGetAllCountriesQuery();
  const { data: teamCategories } = useGetTeamCategoriesQuery();
  const { data: teamCurrencyTypes } = useGetTeamCurrencyQuery();
  const { data: teamAddressTypes } = useGetTeamAddressTypesQuery();
  const { data: allTeamLeagues } = useGetAllTeamLeaguesQuery();

  const [editAddress, setEditAddress] = useState<number>(-1);
  const [AddressisModalVisible, setAddressIsModalVisible] = useState(false);
  const [isModalEdit, setIsModalEdit] = useState({});

  const [TeamsisModalVisible, setTeamsIsModalVisible] = useState(false);
  const [NoteisModalVisible, setNoteIsModalVisible] = useState(false);
  const [DocisModalVisible, setDocIsModalVisible] = useState(false);
  const [VideoisModalVisible, setVideoIsModalVisible] = useState(false);
  const [teamDetails, setTeamDetails] = React.useState<any>({});

  // loading state
  const [confirmLoading, setConfirmLoading] = React.useState(false);
  const [editId, setEditId] = React.useState<any>(null);
  const [userData, setUserData] = React.useState<any>({});

  // history
  const history = useHistory();

  // address
  const onclickModel = () => {
    setAddressIsModalVisible(!AddressisModalVisible)
  }
  const handleOk = (addressSend: any) => {
    let saveJson = [];
    if (teamDetails.addresses) {
      saveJson = teamDetails.addresses;
    }
    let findByType = saveJson?.findIndex((_: any) => _?.addressType === addressSend?.addressType);
    if (findByType > -1) {
      saveJson[findByType] = addressSend;
    }
    else {
      saveJson.push(addressSend);
    }
    setTeamDetails({
      ...teamDetails,
      addresses: saveJson,
    });
    setAddressIsModalVisible(false);
    editAddress > -1 && setEditAddress(-1)
  };
  const handleCancel = () => {
    setAddressIsModalVisible(false);
    editAddress > -1 && setEditAddress(-1)
    setEditId(null);
    setIsModalEdit({});
  };

  React.useEffect(() => {
    currentSession().then(res => {
      setUserData(res.payload)
    })
  }, []);

  // Teams
  const onclickModelTeams = () => {
    setTeamsIsModalVisible(!TeamsisModalVisible)
  }
  const handleOkTeams = (TeamsSend: any, isEdidata: any) => {
    if (Object.keys(isEdidata).length > 0) {
      commonEditFunction(TeamsSend, "teamMembers");
    }
    else {
      let saveJson = [];
      if (teamDetails.teamMembers) {
        saveJson = teamDetails.teamMembers;
      }
      saveJson.push(TeamsSend);
      setTeamDetails({
        ...teamDetails,
        teamMembers: saveJson,
      });

      setTeamsIsModalVisible(false);
    }
  };
  const handleCancelTeams = () => {
    setTeamsIsModalVisible(false);
    setEditId(null);
    setIsModalEdit({});
  };

  // Note
  const onclickModelNote = () => {
    setNoteIsModalVisible(!NoteisModalVisible)
  }
  const handleOkNote = (NoteSend: any, isEdidata: any) => {
    if (Object.keys(isEdidata).length > 0) {
      commonEditFunction(NoteSend, "notes");
    }
    else {
      let saveJson = [];
      NoteSend.addedDate = moment().format('DD/MM/YYYY, hh:mm');
      NoteSend.addedby = userData?.name;
      if (teamDetails.notes) {
        saveJson = teamDetails.notes;
      }
      saveJson.push(NoteSend);
      setTeamDetails({
        ...teamDetails,
        notes: saveJson,
      });

      setNoteIsModalVisible(false);
    }
  };
  const handleCancelNote = () => {
    setNoteIsModalVisible(false);
    setEditId(null);
    setIsModalEdit({});
  };

  // Document
  const onclickModelDoc = () => {
    setDocIsModalVisible(!DocisModalVisible)
  }
  const handleOkDoc = (Document: any, isEdidata: any) => {
    if (Object.keys(isEdidata).length > 0) {
      commonEditFunction(Document, "documents");
    } else {
      let saveJson = [];
      Document.addedDate = moment().format('DD/MM/YYYY, hh:mm');
      Document.addedby = userData?.name;
      if (teamDetails.documents) {
        saveJson = teamDetails.documents;
      }
      saveJson.push(Document);
      setTeamDetails({
        ...teamDetails,
        documents: saveJson,
      });

      setDocIsModalVisible(false);
    }
  };
  const handleCancelDoc = () => {
    setDocIsModalVisible(false);
    setEditId(null);
    setIsModalEdit({});
  };

  // Video 
  const onclickModelVideo = () => {
    setVideoIsModalVisible(!VideoisModalVisible)
  }
  const handleOkVideo = (video: any, isEdidata: any) => {
    if (Object.keys(isEdidata).length > 0) {
      commonEditFunction(Document, "video");
    } else {
      setTeamDetails({
        ...teamDetails,
        video: video,
      });
      setVideoIsModalVisible(false);
    }
  };
  const handleCancelVideo = () => {
    setVideoIsModalVisible(false);
    setEditId(null);
    setIsModalEdit({});
  };

  const profileData = (val: any, type: any) => {
    setTeamDetails({
      ...teamDetails,
      [type]: val,
    });
  }
  const commonEditFunction = (data: any, type: string) => {
    teamDetails[type][editId] = data;
    setTeamDetails({
      ...teamDetails,
    });
    setEditId(null);
    setIsModalEdit({});
    setTeamsIsModalVisible(false);
    setNoteIsModalVisible(false);
    setDocIsModalVisible(false);
  }
  const openEdit = (obj: any, key: any, name: any) => {
    setEditId(key - 1);
    if (name === "contracts") {
      setIsModalEdit(teamDetails.contracts[key - 1]);
    }
    else if (name === "teamMembers") {
      setIsModalEdit(teamDetails.teamMembers[key - 1]);
      setTeamsIsModalVisible(true);
    }
    else if (name === "notes") {
      setIsModalEdit(teamDetails.notes[key - 1]);
      setNoteIsModalVisible(true);
    } else if (name === "documents") {
      setIsModalEdit(teamDetails.documents[key - 1]);
      setDocIsModalVisible(true);
    }
  }
  const commonDeleteFunction = (val: any) => {
    let data = teamDetails[val].filter((x: any, i: any) => i !== editId);
    setTeamDetails({
      ...teamDetails,
      [val]: data,
    });
    setEditId(null);
    setIsModalEdit({});
    setTeamsIsModalVisible(false);
    setNoteIsModalVisible(false);
    setDocIsModalVisible(false);
  }


  const onBeforeSave = async () => {
    form.validateFields()
      .then(async () => {
        SubmitFuntion()
      })
      .catch(e => {
        console.log(e, 'e')
      })
  };

  const { profile } = useSnapshot(state);

  const SubmitFuntion = async () => {

 
    setTeamDetails({
      ...teamDetails,
      userId: profile?.user_id,
    });
    setEditId(null);
    setIsModalEdit({});
    
    if (
      teamDetails?.teamName &&
      teamDetails?.coinSymbol &&
      teamDetails.currencyTypeId &&
      teamDetails.countryTitle &&
      teamDetails.categoryId &&
      teamDetails.league &&
      teamDetails.activeStartAt &&
      teamDetails.activeEndAt &&
      teamDetails.displayStartAt &&
      teamDetails.displayEndAt
    ) {

    
      let payload = {
        
        ...teamDetails,
        activeStartAt:moment( teamDetails?.activeStartAt).seconds('0o0' as unknown as number).toISOString(),
        activeEndAt:  moment(teamDetails?.activeEndAt).seconds('0o0'as unknown as number).toISOString() ,
        displayEndAt: moment(teamDetails?.displayEndAt).seconds('0o0' as unknown as number).toISOString(),
        displayStartAt: moment(teamDetails?.displayStartAt).seconds('0o0' as unknown as number).toISOString(),
        addresses: teamDetails?.addresses?.map((address: any) => ({
          ...address,
          addressState: JSON.stringify({ label: address?.addressState?.label, value: address?.addressState?.value }),
          country: JSON.stringify({ label: address?.country?.label, value: address?.country?.value }),
        })),
        teamMembers: teamDetails?.teamMembers?.map((team: any) => ({
          ...team,
          primaryCountryCode: team?.teamsPrimaryContact?.countryCode,
          teamsPrimaryContact: team?.teamsPrimaryContact?.mobileNumber,
          secondaryCountryCode: team?.teamsSecondaryContact?.countryCode,
          teamsSecondaryContact: team?.teamsSecondaryContact?.mobileNumber
        })),
        video: teamDetails?.video?.url ? teamDetails?.video?.url : teamDetails?.video?.uploadFile
      }
      let team: any = await teamInsertApi(payload);

      if (team?.data?.statusCode === "500") {
        toast.error("Something Went Wrong, Please Try Again");
      } else {
        let teamId = team?.data?.data?.createdTeam?.createTeam?.team?.id 
        toast.success("Team Created Successfully");
        if (teamId) {
          history.replace(`/team/${teamId}`)
        } else {
          history.push('/team-home')
        }
      }
    } else {
      toast.error("Please fill all required fields");
    }
  }

  const onEditAddress = (index: number) => {
    setEditAddress(index);
    onclickModel()
  }

  const onDeleteAddress = (index: number) => {
    let deleteAddress = teamDetails.addresses.filter((_: any, i: number) => i !== index);
    setTeamDetails({
      ...teamDetails,
      addresses: deleteAddress,
    });
  }

   //disable start date
   const isDisableStartDate = useCallback((date) => {
     return date && moment(date).isBefore(moment(), 'day')
  }, [form])

  //status validation for Display Start End Date Time
  const displayStartEndDateTimeValidation = (getFieldValue: any, value: any) => {
    if(!value){
      return Promise.reject(new Error(`Display Start and End time is required!`));
    }
    if (!value?.[0] || !value?.[1]) {
      return Promise.reject(new Error(`Display ${!value[0] ? "Start" : "End"} Date Time is required!`));
    }
    if (getFieldValue('startDatePicker')) {
      if (!value?.[0]?.isBefore(getFieldValue('startDatePicker')?.[0])) {
        return Promise.reject(new Error(`Display Start Date Time should be lesser than Start Date Time(${getFieldValue('startDatePicker')[0].format('DD MM yyyy hh:mm')})`));
      }
      if (!value?.[1]?.isAfter(getFieldValue('startDatePicker')?.[1])) {
        return Promise.reject(new Error(`Display End Date Time should be greater than End Date Time(${getFieldValue('startDatePicker')[1].format('DD MM yyyy hh:mm')})`));
      }
    }
    return Promise.resolve();
  }

  const onValuesChange = (field:any) =>{
    if(field?.startDatePicker){
      form.validateFields(['displayDatePicker']);
    }
  }

  return (
    <Row>
      <div style={{ display: "flex", width: "100%", justifyContent: "space-between", marginRight: "3%", marginTop: "2%" }}>
        <div style={{ marginLeft: "4%" }}>
          <h2 style={{ fontWeight: "bold" }}>Create New Team</h2>
        </div>
        <div style={{ width: "18%" }}>
          <Button className="save cta-button" onClick={()=>onBeforeSave()} type={"primary"} size={"middle"} style={{ padding: "0px 24px" }}>
            {confirmLoading ? 'Creating...' : 'Save'}
          </Button>
          <Button
            size={"middle"}
            className="cta-button discard"
            onClick={() => history.push('/team-home')}
            style={{ marginLeft: "2%" }}
          >
            Discard
          </Button>
        </div>
      </div>
      <Card style={{ width: "100%", margin: 30 }}>
        <Header>Basic info</Header>
        <Form
          form={form}
          initialValues={{
            layout: "horizontal",
          }}
          onFinish={SubmitFuntion}
          layout={"vertical"}
          onValuesChange={onValuesChange}
        >
          <Row gutter={24}>

            <Col lg={8} md={8} sm={24}>
              <LabelInput
                label="Team Name"
                rules={[
                  {
                    required: true,
                    message: 'Team Name is required!',
                  },
                ]}
                name="team name"
                inputElement={
                  <Input
                    type="text"
                    placeholder='Team Name'
                    onChange={(e) => {
                      setTeamDetails({
                        ...teamDetails,
                        teamName: e.target.value,
                      });
                      form.setFieldsValue({
                        teamName: e.target.value,
                      });
                    }}
                    autoComplete={"off"}
                  />
                }
              />
            </Col>

            <Col lg={8}>
              <LabelInput
                label="Activation Start and End time"
                name="startDatePicker"
                rules={[
                  () => ({
                    validator(_, value) {
                      if (!value) {
                        return Promise.reject(new Error(`Activation Start and End time is required!`));
                      }
                      if (!value?.[0] || !value?.[1]) {
                        return Promise.reject(new Error(`Activation ${!value?.[0] ? "Start" : "End"} Date Time is required!`));
                      }
                      return Promise.resolve();
                    },
                  }),
                ]}
                inputElement={
                  <DatePicker.RangePicker
                    defaultValue={null}
                    format={"DD-MM-YYYY hh:mm"}
                    showTime
                    showNow={false}
                    showSecond={false}
                   
                    disabledDate={(value) => isDisableStartDate(value)}
                    onChange={(e: any) =>
                     
                      setTeamDetails({
                   
                        ...teamDetails,
                        activeStartAt: e[0]._d,
                        activeEndAt: e[1]._d,
                      })
                    }
                    style={{
                      width: '100%'
                    }}
                  />
                }
              />
            </Col>

            <Col lg={8}>
              <LabelInput
                label="Display Start and End time"
                name="displayDatePicker"
                 rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      return displayStartEndDateTimeValidation(getFieldValue,value)
                    },
                  }),
                ]}
                inputElement={
                  <DatePicker.RangePicker
                    defaultValue={null}
                    format={"DD-MM-YYYY hh:mm"}
                    showTime
                    showNow={false}
                    showSecond={false}
                    disabledDate={(value) => isDisableStartDate(value)}
                    onChange={(e: any) =>
                      setTeamDetails({
                        ...teamDetails,
                        displayStartAt: e[0]._d,
                        displayEndAt: e[1]._d,
                      })
                    }
                    style={{
                      width: '100%'
                    }}
                  />
                }
              />
            </Col>
          </Row>
          <Divider />
          <Row gutter={24}>
            <Col lg={8}>
              <LabelInput
                label="Country"
                name="countryTitle"
                rules={[
                  {
                    required: true,
                    message: 'Country is required!',
                  },
                ]}
                initialValue={teamDetails.countryTitle}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      setTeamDetails({ ...teamDetails, countryTitle: e });
                      form.setFieldsValue({ countryTitle: e });
                    }}
                    defaultValue={teamDetails.countryTitle}
                    options={countries?.country ?? []}
                    toFilter="title"
                    placeholder="Country"
                    searchable={true}
                  />
                }
              />
            </Col>
            <Col lg={8} md={8} sm={24}>
              <LabelInput
                label="Currency Symbol"
                rules={[
                  {
                    required: true,
                    message: 'Currency Symbol is required!',
                  },
                ]}
                name="coinSymbol"
                inputElement={
                  <Input
                    type="text"
                    placeholder='Currency Symbol'
                    onChange={(e) => {
                      setTeamDetails({
                        ...teamDetails,
                        coinSymbol: e.target.value,
                      });
                      form.setFieldsValue({
                        coinSymbol: e.target.value,
                      });
                    }}
                    autoComplete={"off"}
                  />
                }
              />
            </Col>
            <Col lg={8}>
              <LabelInput
                label="Currency type"
                name="currencyTypeId"
                initialValue={teamDetails.currencyTypeId}
                rules={[
                  {
                    required: true,
                    message: 'Currency type is required!',
                  },
                ]}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      setTeamDetails({ ...teamDetails, currencyTypeId: e });
                      form.setFieldsValue({ currencyTypeId: e });
                    }}
                    defaultValue={teamDetails.currencyTypeId}
                    options={teamCurrencyTypes?.teamCurrencyType ?? []}
                    toFilter="currency"
                    placeholder="Currency Type"
                    searchable={true}
                  />
                }
              />
            </Col>
          </Row>
          <Divider />
          <Row gutter={24}>

            <Col lg={8}>
              <LabelInput
                label="Sports Category"
                name="categoryId"
                rules={[
                  { required: true, message: 'Sports Category is required!' },
                ]}
                initialValue={teamDetails?.categoryId}
                inputElement={
                  <SelectElement
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      setTeamDetails({ ...teamDetails, categoryId: e });
                      form.setFieldsValue({ categoryId: e });
                    }}
                    defaultValue={teamDetails?.categoryId}
                    options={teamCategories?.teamCategory ?? []}
                    toFilter="description"
                    toStore="category"
                    placeholder="Sport Category"
                    searchable={true}
                  />
                }
              />
            </Col>
            <Col lg={8}>
              <LabelInput
                label="Enter League"
                rules={[
                  {
                    required: true,
                    message: 'League is required!',
                  },
                ]}
                name="league"
                inputElement={
                  <SelectElement
                    toFilter="description"
                    toStore='leagueName'
                    options={allTeamLeagues?.allTeamLeagues?.nodes as unknown as TeamLeagueOptions ?? []}
                    allowClear
                    defaultValue={teamDetails?.league ?? []}
                    placeholder="Select League"
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                      setTeamDetails({ ...teamDetails, league: e });
                      form.setFieldsValue({ league: e });
                    }}
                    mode="multiple"
                    searchable={true}
                  />
                }
              />
            </Col>
            <Col lg={8} md={8} sm={24}>
              <LabelInput
                label="Token address"
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (getFieldValue('currencyTypeId') === "TOKEN") {
                        if (value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(new Error('Token address is required!'));
                      }
                    },
                  }),
                ]}
                name="tokenAddress"
                inputElement={
                  <Input
                    type="text"
                    placeholder='Token address'
                    onChange={(e) => {
                      setTeamDetails({
                        ...teamDetails,
                        tokenAddress: e.target.value,
                      });
                      form.setFieldsValue({
                        tokenAddress: e.target.value,
                      });
                    }}
                    autoComplete={"off"}
                  />
                }
              />
            </Col>
            {/* <Col lg={7}>
              </Col> */}
          </Row>
          <Divider />
          <Row gutter={24}>
            <Col lg={8}>
              <LabelInput
                label="Is Internal Team"
                name="isInternalTeam"
                initialValue={teamDetails?.isInternalTeam}
                inputElement={
                  <Checkbox
                    defaultChecked={false}
                    checked={teamDetails?.isInternalTeam}
                    onChange={(e: any) => {
                      setTeamDetails({ ...teamDetails, isInternalTeam: e.target.checked });
                      form.setFieldsValue({ isInternalTeam: e.target.checked });
                    }}
                  />
                }
              />
            </Col>
          </Row>
        </Form>
      </Card>
      <Profile teamDetails={teamDetails} setTeamDetails={setTeamDetails} teamDetailsState={teamDetails} id={"0"} profileDataFun={(e: any, val: any) => profileData(e, val)} />
      <Card style={{ width: "100%", margin: 30 }}>
        <Row justify="space-between">
          <Col lg={6} md={8} xl={5} sm={24}>
            <Header>Video URL</Header>
            {/* <h3 className="bold">Video URL</h3> */}
          </Col>
          {
            teamDetails?.video?.url ?
              null
              : (
                <Col lg={6} style={{ textAlign: "end" }}>
                  <Button type="link" icon={<PlusOutlined />} onClick={() => onclickModelVideo()}>
                    Add New Video
                  </Button>
                </Col>
              )
          }
          <Col lg={24} md={24} xl={24} sm={24}>
            {
              teamDetails?.video?.url || teamDetails?.video?.uploadFile ?
                (
                  <span>
                    <a target="_blank" href={
                      teamDetails?.video?.url ? teamDetails?.video?.url : `${config.apiBaseUrl}files/${teamDetails?.video?.uploadFile}`
                    }
                    >
                      {teamDetails?.video?.url ? teamDetails?.video?.url : teamDetails?.video?.uploadFile}
                    </a>
                    <DeleteOutlined
                      onClick={() =>
                        setTeamDetails({
                          ...teamDetails,
                          video: ""
                        })}
                      style={{ color: "red", paddingLeft: "1%" }}
                    />
                  </span>
                ) : null
            }
          </Col>
          <VideoModal ModalVisible={VideoisModalVisible} onclickModel={() => onclickModelVideo()} handleOk={(val: any, editdata: any) => handleOkVideo(val, editdata)} handleCancel={handleCancelVideo} EditData={isModalEdit} handleDelete={(val: any) => commonDeleteFunction(val)} />
        </Row>
      </Card>
      <Card style={{ width: "100%", margin: 30 }}>
        <Row justify="space-between">
          <Col lg={6} md={8} xl={5} sm={24}>
            <Header>Address</Header>
            {/* <h3 className="bold">Address</h3> */}
          </Col>
          {
            (teamDetails?.addresses?.length ?? 0) < 2 &&
            <Col lg={6} style={{ textAlign: "end" }}>
              <Button type="link" icon={<PlusOutlined />} onClick={() => onclickModel()}>
                Add New Address
              </Button>
            </Col>
          }
        </Row>
        <Row justify="start">
          {teamDetails?.addresses?.map((val: any, index: number) => {
            return (
              <Col lg={7} style={{ backgroundColor: "#F5F5F7", padding: 20, marginRight: 12, textOverflow: "ellipsis", whiteSpace: "nowrap", overflow: "hidden", borderRadius: 8 }}>
                <Row style={{
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginBottom: '1em'
                }}>
                  <Paragraph ellipsis={true} style={{ margin: 0, }}>{val?.addressType}</Paragraph>
                  <Space size={16}>
                    <EditIcon onClick={() => onEditAddress(index)} style={{ cursor: 'pointer' }} />
                    <DeleteOutlined onClick={() => onDeleteAddress(index)} style={{ cursor: 'pointer', fontSize: '18px', color: "red" }} />
                  </Space>
                </Row>
                {val?.line1 && <Row><Paragraph ellipsis={true}>{val?.line1}</Paragraph></Row>}
                {val?.line2 && <Row><Paragraph ellipsis={true}>{val?.line2}</Paragraph></Row>}
                {val?.city && <Row><Paragraph ellipsis={true}>{val?.city}</Paragraph></Row>}
                {val?.addressState?.label && <Row><Paragraph ellipsis={true}>{val?.addressState?.label}</Paragraph></Row>}
                {val?.country?.label && <Row><Paragraph ellipsis={true}>{val?.country?.label}</Paragraph></Row>}
                {val?.pincode && <Row><Paragraph ellipsis={true}>{val?.pincode}</Paragraph></Row>}
              </Col>
            )
          })}
        </Row>
        {AddressisModalVisible &&
          <AddressModal
            data={editAddress > -1 ? teamDetails?.addresses[editAddress] : {}}
            teamAddressTypes={teamAddressTypes?.teamAddressType}
            ModalVisible={AddressisModalVisible}
            onclickModel={() => onclickModel()}
            handleOk={(val: any) => handleOk(val)}
            handleCancel={handleCancel}
          />
        }
      </Card>
      {/* <Card style={{ width: "100%", margin: 30 }}>
        <Row justify="space-between">
          <Col lg={6} md={8} xl={5} sm={24}>
            <h3 className="bold">Contracts</h3>
          </Col>
          <Col lg={6} style={{ textAlign: "end" }}>
            <Button type="link" icon={<PlusOutlined />} onClick={() => onclickModelContact()}>
              Add New Contracts
            </Button>
          </Col>
        </Row>
        <NewContactModal handleDelete={(val: any) => commonDeleteFunction(val)} EditData={isModalEdit} teamStatuses={teamStatuses} ModalVisible={ContactisModalVisible} onclickModel={() => onclickModelContact()} handleOk={(val: any, editdata: any) => handleOkContact(val, editdata)} handleCancel={handleCancelContact} />
        <Contact JsonData={teamDetails?.contracts} openEdit={(obj: any, key: any, name: any) => openEdit(obj, key, name)} />
      </Card> */}
      <Card style={{ width: "100%", margin: 30 }}>
        <Row justify="space-between">
          <Col lg={6} md={8} xl={5} sm={24}>
            <Header>Team</Header>
            {/* <h3 className="bold">Team</h3> */}
          </Col>
          <Col lg={6} style={{ textAlign: "end" }}>
            <Button type="link" icon={<PlusOutlined />} onClick={() => onclickModelTeams()}>
              Add New Team
            </Button>
          </Col>
        </Row>
        <TeamsModal handleDelete={(val: any) => commonDeleteFunction(val)} EditData={isModalEdit} ModalVisible={TeamsisModalVisible} onclickModel={() => onclickModelTeams()} handleOk={(val: any, editdata: any) => handleOkTeams(val, editdata)} handleCancel={handleCancelTeams} />
        <Teams JsonData={teamDetails?.teamMembers} openEdit={(obj: any, key: any, name: any) => openEdit(obj, key, name)} />
      </Card>
      <Card style={{ width: "100%", margin: 30 }}>
        <Row justify="space-between">
          <Col lg={6} md={8} xl={5} sm={24}>
            <Header>Notes</Header>
            {/* <h3 className="bold">Notes</h3> */}
          </Col>
          <Col lg={6} style={{ textAlign: "end" }}>
            <Button type="link" icon={<PlusOutlined />} onClick={() => onclickModelNote()}>
              Add New Notes
            </Button>
          </Col>
        </Row>
        <NoteModal handleDelete={(val: any) => commonDeleteFunction(val)} EditData={isModalEdit} ModalVisible={NoteisModalVisible} onclickModel={() => onclickModelNote()} handleOk={(val: any, editdata: any) => handleOkNote(val, editdata)} handleCancel={handleCancelNote} />
        <Note JsonData={teamDetails?.notes} openEdit={(obj: any, key: any, name: any) => openEdit(obj, key, name)} />
      </Card>
      <Card style={{ width: "100%", margin: 30 }}>
        <Row justify="space-between">
          <Col lg={6} md={8} xl={5} sm={24}>
            <Header>Documents</Header>
            {/* <h3 className="bold">Documents</h3> */}
          </Col>
          <Col lg={6} style={{ textAlign: "end" }}>
            <Button type="link" icon={<PlusOutlined />} onClick={() => onclickModelDoc()}>
              Add New Document
            </Button>
          </Col>
        </Row>
        <DocModal handleDelete={(val: any) => commonDeleteFunction(val)} EditData={isModalEdit} ModalVisible={DocisModalVisible} onclickModel={() => onclickModelDoc()} handleOk={(val: any, editdata: any) => handleOkDoc(val, editdata)} handleCancel={handleCancelDoc} />
        <Document JsonData={teamDetails?.documents} openEdit={(obj: any, key: any, name: any) => openEdit(obj, key, name)} />
      </Card>
    </Row>
  )
}

export default AddTeamDetails;