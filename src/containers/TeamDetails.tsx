import styled from 'styled-components/macro';
import { Breadcrumb, Button, Card, Col, Divider, Row } from 'antd';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import Text from 'antd/lib/typography/Text';
import * as React from "react";
import TitleCtaTable from 'components/common/TitleCtaTable';
import { contractsColumns, contractsData } from 'data/contracts';
import { teamColumns, teamData } from 'data/team';
import { actionHistoryColumns, actionHistoryData } from 'data/actionHistory';
import { notesColumns, notesData } from 'data/notes';

import { ReactComponent as MoreIcon } from 'assets/icons/more.svg';
import AdoptionGraphs from 'components/reward-details/AdoptionGraphs';
import TeamStats from 'components/team-details/TeamStats';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import BasicInfo from 'components/team-details/BasicInfo';
import Engagement from 'components/team-details/Engagement';
import Community from 'components/team-details/Community';
import { Link, useHistory, useParams } from 'react-router-dom';
import useSearchQuery from 'hooks/useSearchQuery';
import { useEffect, useState } from 'react';
import { useGetTeamByIdQuery } from 'generated/graphql';
import Profile from 'components/team-details/Profile';
import Token from 'components/team-details/Token_bk';
import TitleCta from 'components/common/TitleCta';
import { format } from 'date-fns';
import { TeamSelectionContext } from 'App';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const TeamDetailsWrapper = styled.div`
  padding: 40px;
  width: 100%;

  .t-transactions {
    font-size: 25px;
    line-height: 30px;
    color: rgba(5, 31, 36, 0.3);
  }
  .t-transaction-id {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .ant-card {
    margin-top: 40px;
    width: 100%;
    .t-transaction-key-field {
      font-size: 15px;
      line-height: 18px;
      color: rgba(5, 31, 36, 0.6);
    }
    .t-transaction-value {
      font-size: 16px;
      line-height: 25px;
      font-weight: 500;
      color: #051f24;
      &.blue {
        color: #4da1ff;
      }
    }
    .key-value-wrapper {
      row-gap: 10px;
    }
    .edit-col {
      display: flex;
      align-items: center;
      column-gap: 20px;
    }
  }
  .ant-btn.add-new-button {
    border: 1px solid rgba(7, 105, 125, 0.25);
    color: rgba(7, 105, 125, 1);
    box-sizing: border-box;
    border-radius: 5px;
    height: fit-content;
    padding: 15px;
    display: flex;
    align-items: center;
    column-gap: 10px;
  }

  .sr-no-wrapper {
    position: relative;
    .srno-default-pop {
      position: absolute;
      background: #4aaf05;
      border: 2px solid #4aaf05;
      font-size: 12px;
      padding: 0 6px;
      top: -10px;
      left: 12px;
      color: #fff;
      border-radius: 100px;
    }
  }

  .sub-routes-wrapper {
    column-gap: 8px;
    margin: 60px auto;
    text-align: center;
    .sub-route {
      padding: 8px;
      width: 100%;
      background: rgba(198, 198, 198, 0.6);
      color: rgba(5, 31, 36, 0.6);
      border-radius: 4px;
      cursor: pointer;
      font-weight: 600;
      &.active {
        background: #fff;
        border: 1px solid #eee;
        color: #4da1ff;
      }
    }
  }
  a {
    text-decoration: none;
    color: inherit;
  }
`;

interface TeamDetailsProps { }

interface SelectedLeagueProps {
  league: string
}

const TeamDetails = ({ }: TeamDetailsProps): JSX.Element => {
  const { id } = useParams<{ id: string }>();
  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }
  const isTeamUser = isIamTeamUser();
  const teamSelectionContext = React.useContext(TeamSelectionContext);

  // const query = useSearchQuery();
  const history = useHistory();
  const { data: teamDetails } = useGetTeamByIdQuery({
    variables: {
      teamId: +id,
    },
  });
  const [tab, setTab] = useState('basic-info')

  // useEffect(() => {
  //   history.replace(`/team/${id}?tab=basic-info`);
  // }, []);

  const formatDate = (date: string, delimiter: string = "", dateFormat: string = "dd MMM, yyyy hh:mm") => date ? format(new Date(date.concat('z')), dateFormat) : delimiter;

  if (isTeamUser) {
    if (parseInt(id) !== teamSelectionContext.team.id) {
      history.push("/");
    }
  }

  return (
    <TeamDetailsWrapper>
      <Breadcrumb separator=">">
        <Breadcrumb.Item className="t-transactions">Team</Breadcrumb.Item>
        <Breadcrumb.Item className="t-transaction-id" href="">
          {teamDetails?.team_by_pk?.name ?? '-'}
        </Breadcrumb.Item>
      </Breadcrumb>

      <Card>
        <TitleCta
          title="Basic Info"
          cta={
            <>
              {
                !['INACTIVE', 'DISCONTINUED'].includes(teamDetails?.team_by_pk?.statusId ?? '') &&
                <Col className="edit-col">
                  <Link to={`/team/${id}/edit`}>Edit</Link>
                  <MoreIcon />
                </Col>
              }
            </>
          }
        />

        <Row gutter={[24, 24]}>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Name</Text>
              <Text className="t-transaction-value">
                {teamDetails?.team_by_pk?.name ?? '-'}
              </Text>
            </FlexColumnWrapper>
          </Col>

          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">ID</Text>
              <Text className="t-transaction-value">{id}</Text>
            </FlexColumnWrapper>
          </Col>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Status</Text>
              <Text className="t-transaction-value blue">
                {teamDetails?.team_by_pk?.statusId}
              </Text>
            </FlexColumnWrapper>
          </Col>
        </Row>
        <Divider />
        <Row gutter={[24, 24]}>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Activation Start Date - End Date</Text>
              <Text className="t-transaction-value">{`${formatDate(teamDetails?.team_by_pk?.activeStartAt)} - ${formatDate(teamDetails?.team_by_pk?.activeEndAt)}`}</Text>
            </FlexColumnWrapper>
          </Col>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Display Start Date - End Date</Text>
              <Text className="t-transaction-value">
                <Text className="t-transaction-value">{`${formatDate(teamDetails?.team_by_pk?.displayStartAt)} - ${formatDate(teamDetails?.team_by_pk?.displayEndAt)}`}</Text>
              </Text>
            </FlexColumnWrapper>
          </Col>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Currency Symbol</Text>
              <Text className="t-transaction-value">
                {teamDetails?.team_by_pk?.coinSymbol ?? '-'}
              </Text>
            </FlexColumnWrapper>
          </Col>
        </Row>
        <Divider />
        <Row gutter={[24, 24]}>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Currency type</Text>
              <Text className="t-transaction-value">
                {teamDetails?.team_by_pk?.currencyTypeId}
              </Text>
            </FlexColumnWrapper>
          </Col>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Country</Text>
              <Text className="t-transaction-value">
                {teamDetails?.team_by_pk?.country?.title ?? '-'}
              </Text>
            </FlexColumnWrapper>
          </Col>

          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Category</Text>
              <Text className="t-transaction-value">
                {' '}
                {teamDetails?.team_by_pk?.teamCategory?.description ?? '-'}
              </Text>
            </FlexColumnWrapper>
          </Col>
        </Row>
        <Divider />
        <Row gutter={[24, 24]}>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">League </Text>
              <Text className="t-transaction-value">{teamDetails?.selectedLeague?.length ? teamDetails?.selectedLeague?.map(({ teamLeague }: any) => teamLeague?.description)?.join(',') : '-'}</Text>
            </FlexColumnWrapper>
          </Col>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Token Address </Text>
              <Text className="t-transaction-value">{teamDetails?.team_by_pk?.tokenAddress ?? '-'}</Text>
            </FlexColumnWrapper>
          </Col>
          <Col span={8}>
            <FlexColumnWrapper className="key-value-wrapper">
              <Text className="t-transaction-key-field">Is Internal Team</Text>
              <Text className="t-transaction-value">{teamDetails?.team_by_pk?.isInternalTeam ? 'Yes' : 'No'}</Text>
            </FlexColumnWrapper>
          </Col>
        </Row>
      </Card>
      {/* <TeamStats /> */}
      <FlexRowWrapper className="sub-routes-wrapper">
        <Text
          className={`sub-route ${tab === 'profile' && 'active'}`}
          onClick={() => setTab('profile')}
        >
          <a>Profile</a>
        </Text>
        <Text
          className={`sub-route ${tab === 'token' && 'active'}`}
          onClick={() => setTab('token')}
        >
          <a>Token</a>
        </Text>
        <Text
          className={`sub-route ${tab === 'engagement' && 'active'
            }`}
          onClick={() => setTab('engagement')}
        >
          <a>Engagement</a>
        </Text>
        <Text
          className={`sub-route ${tab === 'community' && 'active'
            }`}
          onClick={() => setTab('community')}
        >
          <a>Community</a>
        </Text>
        <Text
          className={`sub-route ${tab === 'basic-info' && 'active'
            }`}
          onClick={() => setTab('basic-info')}
        >
          <a>Basic Info</a>
        </Text>
      </FlexRowWrapper>

      {(() => {
        switch (tab) {
          case 'engagement':
            return <Engagement id={id} />;
          case 'profile':
            return <Profile id={id} />;
          case 'token':
            return <Token id={id} />;

          case 'basic-info':
            return (
              <BasicInfo
                permanentAddress={teamDetails?.team_by_pk?.teamAddresses[0]}
                presentAddress={teamDetails?.team_by_pk?.teamAddresses[1]}
                id={id}
              />
            );

          case 'community':
            return (
              <Community
                id={id}
                coinSymbol={
                  teamDetails?.team_by_pk?.wallets[0]?.coinSymbol ?? '-'
                }
              />
            );

          default:
            return null;
        }
      })()}
    </TeamDetailsWrapper>
  );
};

export default TeamDetails;
