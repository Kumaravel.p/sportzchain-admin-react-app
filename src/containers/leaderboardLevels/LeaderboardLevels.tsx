import { Button, Modal, Space, Typography, Spin, FormInstance } from 'antd';
import React, { useCallback, useMemo, useState } from 'react';
import styled from 'styled-components/macro';
import { LeaderboardCards, StatusCard } from './components';
import { ReactComponent as EyeIcon } from 'assets/icons/EyePlus.svg';
import CustomTable from 'components/customTable';
import moment, { MomentInput } from 'moment';
import { CheckIcon } from 'components/svg';
import { useDeleteLeaderboardPointByIdMutation, useGetAllLeaderboardsQuery } from 'generated/pgraphql';
import { LeaderboardLevelCreation, LeaderboardLevelUpdation } from 'apis/leaderboardLevels';
import { toast } from 'react-toastify';
import { LeaderboardLevelPayload } from 'ts/interfaces/leaderboardLevel';
import { leaderboardId } from './utils';

interface ModalProps {
    type: 'status' | 'table' | null
}

const Wrapper = styled.div`
    width:100%;
    padding:40px;
    background-color: #F5F5F7;
    .leaderboards{
        .title-head{
            margin-top: 32px;
            margin-bottom: 24px;
        }
        .leaderboard-card{
            margin-bottom: 16px;
            .status-icon{
                cursor: pointer;
                & div:first-of-type{
                    display:flex;
                }
            }
        }
    }
   
`;

const ModalWrapper = styled(Modal) <ModalProps>`
    width: ${props => props.type === "table" ? "85%" : "500px"} !important;
    @media (max-width: 600px) {
        width:95%;
    }
    .table-container{
        border: 1px solid #E6E6E6;
        width: 100%;
        border-radius: 4px;
        padding: 1px;
        margin-bottom: 24px;
        .status{
            .ant-space-item:first-of-type{
                display:flex;
            }
        }
    }
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

interface LeaderboardLevelsProps { };

interface LevelType {
    level: "Platform" | "Team" | null;
    type: "Socials" | "Front Runners" | "Zuperfan" | "Vested" | "Leader" | "Focused" | null;
}

interface TableDataType {
    sno?: number;
    minActivityPoint: string;
    maxActivityPoint: string;
    effectiveFrom: string;
    effectiveTill: string;
    status: any;
    id?: number;
}

const tableInitialState = {
    // sno: 1,
    minActivityPoint: "0",
    maxActivityPoint: "0",
    effectiveFrom: moment().toISOString(),
    effectiveTill: moment().toISOString(),
    status: "Upcoming"
}

const labels = [
    "Minimum Activity Points", "Maximum Activity Points", "Effective from", "Effective till", "Status"
];

const appendTimeZone = (date: string) => date ? date.includes('Z' || 'z') ? date : date.concat('z') : "";
const formatDate = (value: Date | string) => moment(value).format('DD-MM-yyyy, hh:mm a');
const fixedNumber = (value: number, digit: number = 2) => isNaN(value) ? 0 : Number(value).toFixed(digit)

const LeaderboardLevels = (props: LeaderboardLevelsProps): JSX.Element => {

    const [openModal, setOpenModal] = useState<'status' | 'table' | null>(null);
    const [loading, setLoading] = useState<string | null>(null);
    const [tableDataSource, setTableDataSource] = useState<TableDataType[] | []>([]);
    const [statusData, setStatusData] = useState<any>({});
    const [levelType, setLevelType] = useState<LevelType>({
        level: null,
        type: null
    });
    const [editIndex, setEditIndex] = useState<number>(-1);

    const {
        data: leaderboardData,
        loading: leaderboardLoding,
        refetch: refetchLeaderboardData
    } = useGetAllLeaderboardsQuery({
        notifyOnNetworkStatusChange: true,
        fetchPolicy: "network-only",
        variables:{
            leaderboardTypeByLevelId:["Platform", "Team"]
        }
    });

    const [
        deleteLeaderboard,
        { loading: deletedLeaderboardLoading }
    ] = useDeleteLeaderboardPointByIdMutation({
        notifyOnNetworkStatusChange: true,
    });

    const constructLeaderboadData: any = useMemo(() => {
        if (!leaderboardData?.allLeaderboards?.nodes?.length) return [];
        let leaderboards = leaderboardData?.allLeaderboards?.nodes;

        return leaderboards.reduce((total: any, item: any) => ({
            ...total,
            [item?.masterLeaderboardTypeByLevelId?.name]: [...total?.[item?.masterLeaderboardTypeByLevelId?.name] ?? [], item]
        }), {})
        // eslint-disable-next-line
    }, [leaderboardData])

    //construct leaderboard card's data
    const constructLeaderboardData = (labels: string[] = [], values: any[] = []) => {
        return values?.map((value, index) => ({
            label: labels?.[index] ? labels?.[index] : "",
            value: value
        }))
    }

    React.useEffect(() => {
        if (levelType?.level && levelType?.type) {
            let updateTableData = constructLeaderboadData[levelType.level].find((_: any) => _.masterLeaderboardByTypeId.name === levelType.type).leaderboardPointsByLeaderboardId.nodes;
            setTableDataSource(updateTableData)
            setLoading(null)
        }
        // eslint-disable-next-line
    }, [constructLeaderboadData])

    const renderStatus = useCallback((val: 'Completed' | 'Upcoming' | 'Active') => {
        let color = "#10B981";
        if (val === "Upcoming") {
            color = "#0075FF"
        }
        else if (val === "Completed") {
            color = "#A3A3A3"
        }
        return (
            <Space className='status' align="center" size={8}>
                <CheckIcon color={color} />
                <Typography.Text style={{ color }}>{val}</Typography.Text>
            </Space>
        )        
         // eslint-disable-next-line
    }, [tableDataSource, constructLeaderboadData])

    const openStatusModal = (value: any = {}, leaderboard: any) => {
        setLevelTypeState(leaderboard);
        setStatusData({
            minActivityPoint: fixedNumber(value?.minActivityPoint),
            maxActivityPoint: fixedNumber(value?.maxActivityPoint),
            effectiveFrom: moment(appendTimeZone(value?.effectiveFrom)),
            effectiveTill: moment(appendTimeZone(value?.effectiveTill)),
            id: value?.id,
            status: value?.masterLeaderboardStatusByStatusId?.description
        })
        setOpenModal("status")
    }

    const openTableModal = (leaderboard: any) => {
        setLevelTypeState(leaderboard);
        setTableDataSource(
            leaderboard?.leaderboardPointsByLeaderboardId?.nodes?.length
                ? leaderboard?.leaderboardPointsByLeaderboardId?.nodes
                : []
        )
        setOpenModal('table')
    }

    const setLevelTypeState = (leaderboard: any = null) => {
        setLevelType({
            level: leaderboard?.masterLeaderboardTypeByLevelId?.name,
            type: leaderboard?.masterLeaderboardByTypeId?.name
        })
    }

    //construct leaderboard card
    const contructValues = (value: any = {}, leaderboard: any) => {
        return constructLeaderboardData(labels, [
            fixedNumber(value?.minActivityPoint),
            fixedNumber(value?.maxActivityPoint),
            formatDate(appendTimeZone(value?.effectiveFrom)),
            formatDate(appendTimeZone(value?.effectiveTill)),
            <div className='status-icon' onClick={() => openStatusModal(value, leaderboard)}>
                {renderStatus("Active")}
            </div>,
            <EyeIcon
                style={{ cursor: "pointer" }}
                onClick={() => openTableModal(leaderboard)}
            />
        ])
    }


    const tableColumns = useMemo(() => [
        { title: 'S.No', dataIndex: "sno", width: 100 },
        {
            title: 'Minimum Activity Points',
            dataIndex: "minActivityPoint",
            component: 'EDITABLE',
        },
        {
            title: 'Maximum Activity Points',
            dataIndex: "maxActivityPoint",
            component: 'EDITABLE',
        },
        {
            title: 'Effective from',
            dataIndex: "effectiveFrom",
            component: 'EDITABLE',
            component_name: "date-picker",
            valueFormat: (value: Date) => formatDate(value),
            disabledDate: (value: MomentInput) => value && moment(value).isBefore(moment(), 'day')
        },
        {
            title: 'Effective till',
            dataIndex: "effectiveTill",
            component: 'EDITABLE',
            component_name: "date-picker",
            valueFormat: (value: Date) => formatDate(value),
            disabledDate: (value: MomentInput, rowRecord: any, newRecord: any) => {
                let val = newRecord?.effectiveFrom ? newRecord?.effectiveFrom : rowRecord?.effectiveFrom;
                let checkDate = moment(val).isBefore(moment(), 'day') ? moment() : moment(val)
                return value && moment(value).isBefore(checkDate, 'day')
            }
        },
        {
            title: 'Status',
            dataIndex: "status",
            valueFormat: (value: 'Completed' | 'Upcoming' | 'Active') => {
                return renderStatus(value)
            }
        },
        { title: '', dataIndex: '', component: 'INSERT', },
    ], [tableDataSource])

    const returnEditComponents = (rowRecord: any) => {
        const checkStatus = (val: string) => (editIndex > -1) ? true : val === "Upcoming"
        return {
            minActivityPoint: checkStatus(rowRecord?.status),
            maxActivityPoint: checkStatus(rowRecord?.status),
            effectiveFrom: checkStatus(rowRecord?.status),
            effectiveTill: true,
        }
    }

    const isEditIcon = (rowRecord: any) => {
        return ['Active', 'Upcoming'].includes(rowRecord?.status);
    }

    const isDeleteIcon = (rowRecord: any) => {
        return rowRecord?.status === "Upcoming"
    }

    const onAddRow = () => {
        let data = [...tableDataSource, tableInitialState];
        setTableDataSource(data)
        setEditIndex(data.length - 1);
    }

    const onCancelRow = (index: number) => {
        if(editIndex > -1){
            setEditIndex(-1);
            let filteredData = tableDataSource?.filter((_, i) => i !== index);
            setTableDataSource(filteredData)
        }
    }

    const LeaderboardUpsertion = async (payload: LeaderboardLevelPayload, type: "add" | "edit", callback: Function = () => null, loading = "table") => {

        let api = type === "add" ? LeaderboardLevelCreation : LeaderboardLevelUpdation
        setLoading(loading)
        await api(payload)
            .then((res: any) => {
                if (res.data.statusCode === "500") {
                    catchError(res.data.message)
                }
                toast.success(res.data.message);
                callback && callback();
                refetchLeaderboardData()
            })
            .catch(e => catchError(e))
    }

    const returnTableData = useMemo(() => {
        if (!tableDataSource?.length) return []
        return tableDataSource?.map((data: any, index) => ({
            sno: index + 1,
            minActivityPoint: fixedNumber(Number(data?.minActivityPoint)),
            maxActivityPoint: fixedNumber(Number(data?.maxActivityPoint)),
            effectiveFrom: appendTimeZone(data?.effectiveFrom) ?? moment().toISOString(),
            effectiveTill: appendTimeZone(data?.effectiveTill) ?? moment().toISOString(),
            status: data?.masterLeaderboardStatusByStatusId?.description ?? "Upcoming",
            id: data?.id,
            index
        }))
    }, [tableDataSource])

    const onSubmitModal = (formValues: FormInstance) => {

        let values = formValues.getFieldsValue();

        if (levelType.type && !isNaN(leaderboardId?.[levelType.type])) {
            let payload: LeaderboardLevelPayload = {
                id: statusData?.id,
                leaderboardId: leaderboardId?.[levelType.type],
                minActivityPoint: values?.minActivityPoint ?? "0",
                maxActivityPoint: values?.maxActivityPoint ?? "0",
                effectiveFrom: values?.effectiveFrom?.toISOString(),
                effectiveTill: values?.effectiveTill?.toISOString(),
            }
            LeaderboardUpsertion(
                payload,
                "edit",
                () => onCancelModal(),
                "status"
            )
        }
    }

    const onCancelModal = () => {
        setOpenModal(null);
        setEditIndex(-1);
        // setLevelTypeState();
    }

    const handleTableUpdate = (newRecord: TableDataType, rowIndex: number, type: "edit" | "delete") => {

        let data = [...tableDataSource];

        if (type === "edit") {
            const {
                minActivityPoint,
                maxActivityPoint,
                effectiveFrom,
                effectiveTill
            } = newRecord;

            if (levelType.type && !isNaN(leaderboardId?.[levelType.type])) {

                let payload: LeaderboardLevelPayload = {
                    ...(data?.[rowIndex]?.id && { id: data?.[rowIndex].id, }),
                    leaderboardId: leaderboardId?.[levelType.type],
                    minActivityPoint: minActivityPoint ? minActivityPoint : data?.[rowIndex]?.minActivityPoint ?? "0",
                    maxActivityPoint: maxActivityPoint ? maxActivityPoint : data?.[rowIndex]?.maxActivityPoint ?? "0",
                    effectiveFrom: effectiveFrom ? effectiveFrom : data?.[rowIndex]?.effectiveFrom,
                    effectiveTill: effectiveTill ? effectiveTill : data?.[rowIndex]?.effectiveTill
                }
                LeaderboardUpsertion(
                    payload,
                    data?.[rowIndex]?.id ? "edit" : "add",
                    () => {
                        setEditIndex(-1)
                    })
            }
        }

        else if (type === "delete") {
            let id = data?.[rowIndex].id;
            if (id && !isNaN(id)) {
                deleteLeaderboard({
                    variables: {
                        id
                    }
                })
                    .then((res) => {
                        if (res?.data?.deleteLeaderboardPointById?.leaderboardPoint?.id) {
                            toast.success("Deleted Successfully!");
                            refetchLeaderboardData()
                        }
                        else{
                            toast.error('Something went wrong!')
                        }
                    })
                    .catch(e => catchError(e))
            }
        }
    }

    const catchError = (msg: string) => {
        console.log(msg);
        setLoading(null);
        toast.error('Something went wrong!')
        return false
    }

    const returnModalBody = () => {
        if (openModal === "status") {
            return (
                <StatusCard
                    onCancel={onCancelModal}
                    onSave={onSubmitModal}
                    data={statusData ?? {}}
                    loading={loading === "status" || leaderboardLoding}
                />
            )
        }
        else if (openModal === "table") {
            return (
                <div>
                    <div className="table-container">
                        <CustomTable
                            dataSource={returnTableData}
                            columns={tableColumns}
                            onRowSave={handleTableUpdate}
                            loading={(loading === "table" || leaderboardLoding || deletedLeaderboardLoading)}
                            editIndex={editIndex > -1 ? editIndex : null}
                            onCancel={(index: number) => onCancelRow(index)}
                            returnEditComponents={returnEditComponents}
                            isEditIcon={isEditIcon}
                            isDeleteIcon={isDeleteIcon}
                            editValue={"index"}
                            scrollY={250}
                            columnWidth={200}
                        />
                    </div>
                    <Button
                        type='primary'
                        // disabled={editIndex && editIndex}
                        onClick={onAddRow}
                    >
                        Add Row
                    </Button>
                </div>
            )
        }
    }

    //render leaderboard only if its active
    const returnActiveLeaderboards = (data = []): any[] => {
        return data?.map((leaderboard: any) => {
            return leaderboard?.leaderboardPointsByLeaderboardId?.nodes?.filter((_: any) => _?.statusId === 2) // 2 is Active
        })
    }

    const mapActiveLeaderboards = useCallback((data = []) => {
        return returnActiveLeaderboards(data).map((_, index) => {
            if (_?.length) {
                return _?.map((values: any) => {
                    return (
                        <div className='leaderboard-card' key={`platform-leaderboard-card-${index}`}>
                            <LeaderboardCards
                                label={`${data?.[index]?.masterLeaderboardByTypeId?.name}`}
                                jsonData={contructValues(values, data?.[index])}
                            />
                        </div>
                    )
                })
            }
            else {
                return (
                    <div className='leaderboard-card'>
                        <LeaderboardCards
                            label={`${data?.[index]?.masterLeaderboardByTypeId?.name}`}
                            jsonData={constructLeaderboardData(labels, [
                                '-', '-', '-', '-', '-',
                                <EyeIcon
                                    style={{ cursor: "pointer" }}
                                    onClick={() => openTableModal(data?.[index])}
                                />
                            ])}
                        />
                    </div>
                )
            }
        })
    }, [constructLeaderboadData]);

    const isVisible = useMemo(() => openModal ? ['status', 'table']?.includes(openModal) : false, [openModal]);

    const modalTitle = useMemo(() => {
        if (openModal === "table") {
            return `${levelType?.level} Leaderboard > ${levelType?.type}`
        }
        else if (openModal === "status") {
            return `${levelType?.type}`
        }
        return null
    }, [openModal]);

    return (
        <Wrapper>

            <Typography.Title level={4}>Leaderboard Levels</Typography.Title>
            {
                leaderboardLoding ? (
                    <SpinWrapper>
                        <Spin />
                    </SpinWrapper>
                ) : (
                    <div className="leaderboards">
                        {
                            constructLeaderboadData && Object.keys(constructLeaderboadData)?.map((leaderboard: string) => (
                                <div>
                                    <Typography.Title level={5} className="title-head">{leaderboard} Leaderboard</Typography.Title>
                                    {mapActiveLeaderboards(constructLeaderboadData?.[leaderboard])}
                                </div>
                            ))
                        }
                    </div>
                )
            }
            <ModalWrapper
                type={openModal}
                title={modalTitle}
                centered
                visible={isVisible}
                onCancel={() => onCancelModal()}
                destroyOnClose={true}
                footer={null}
            >
                {returnModalBody()}
            </ModalWrapper>
        </Wrapper>
    )
}

export default LeaderboardLevels