import React from 'react';
import { Card, Space, Typography } from 'antd';
import styled from 'styled-components/macro';

const Wrapper = styled.div`
.ant-card{
    border-radius: 8px;
    .card-space{
        justify-content: space-between;
        width: 100%;
    }
    .title{
        font-weight: 600;
        font-size: 16px;
        color: #171717;
    }
    .label{
        font-weight: 400;
        font-size: 14px;
        color: #737373;
    }
    .value{
        font-weight: 500;
        font-size: 16px;
        color: #171717;
    }
}
`;

interface LeaderboardCardsProps {
    label?: string;
    align?: 'start' | 'end' | 'center' | 'baseline';
    jsonData:any[]
};

interface JsonDataStruct {
    label?: any;
    value?: any;
}


export const LeaderboardCards = (props: LeaderboardCardsProps) => {

    const {
        label = "",
        align = "end",
        jsonData=[]
    } = props;
    
    const returnVal = (val: any, className: string) => {
        if (typeof val === "object") return val
        return <Typography.Text className={className}>{val}</Typography.Text>
    };

    return (
        <Wrapper>
            <Card>
                {label && <Typography.Title level={5} className='title'>{label}</Typography.Title>}
                {
                    jsonData?.length > 0 &&
                    <Space wrap align={align} className="card-space" size={24}>
                        {
                            jsonData?.map(({ label, value }: JsonDataStruct, index: number) => (
                                <Space size={8} direction="vertical" key={index}>
                                    {label && returnVal(label, 'label')}
                                    {value && returnVal(value, 'value')}
                                </Space>
                            ))
                        }
                    </Space>
                }

            </Card>
        </Wrapper>
    )
}
