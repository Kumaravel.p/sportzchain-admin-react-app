import { Button, Col, DatePicker, Form, FormInstance, Input, Row, Space, Spin } from 'antd';
import LabelInput from 'components/common/LabelInput';
import moment from 'moment';
import React, { useMemo } from 'react';
import styled from 'styled-components/macro';

const Wrapper = styled.div`
position:'relative';
.btn-actions{
    margin-top:16px;
    width:100%;
    justify-content: flex-end;
    & .ant-space-item:first-of-type{
        button{
            border-color: #1890ff;
            color:#1890ff;
        }
        }
    button{
        border-radius:5px;
    }
}
.ant-input{
    border-color: #d9d9d9 !important;
    border-radius: 2px !important;
}
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
    position: absolute;
    top:0;
    left:0;
    right:0;
    bottom:0;
    z-index: 1;
`;
interface StatusCardProps {
    onSave?: (form: FormInstance) => void;
    onCancel?: () => void;
    data?: any;
    loading?:boolean;
}

export const StatusCard = (props: StatusCardProps) => {

    const {
        onSave,
        onCancel,
        data = {},
        loading=false
    } = props;

    const [form] = Form.useForm();

    const onFinishForm = () => { }

    const onSaveForm = () => {
        form.submit()
        form.validateFields()
            .then(() => {
                onSave && onSave(form)
            })
            .catch(e => {
                console.log(e, 'e')
            })
    }

    const returnInitialValues = useMemo(() => data, [data]);

    const isDisabled = useMemo(() => !(data?.status === "Upcoming"), [data]);


    return (
        <Wrapper>
            <Form
                onFinish={onFinishForm}
                form={form}
                preserve={false}
                initialValues={returnInitialValues}
            >
                {loading && <SpinWrapper>
                    <Spin />
                </SpinWrapper>}
                <Row gutter={[16, 8]}>
                    <Col xs={24} md={12}>
                        <LabelInput
                            label="Minimum Activity Points"
                            name="minActivityPoint"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Minimum Activity Points!',
                                },
                            ]}
                            inputElement={
                                <Input
                                    placeholder="Minimum Activity Points"
                                    type="number"
                                    disabled={isDisabled}
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} md={12}>
                        <LabelInput
                            label="Maximum Activity Points"
                            name="maxActivityPoint"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Maximum Activity Points!',
                                },
                            ]}
                            inputElement={
                                <Input
                                    placeholder="Maximum Activity Points"
                                    type="number"
                                    disabled={isDisabled}
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} md={12}>
                        <LabelInput
                            label="Effective from"
                            name="effectiveFrom"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Effective from is required!',
                                },
                            ]}
                            inputElement={
                                <DatePicker
                                    showTime
                                    style={{
                                        width: '100%'
                                    }}
                                    disabled={isDisabled}
                                />
                            }
                        />
                    </Col>
                    <Col xs={24} md={12}>
                        <LabelInput
                            label="Effective till"
                            name="effectiveTill"
                            required
                            rules={[
                                {
                                    required: true,
                                    message: 'Effective till is required!',
                                },
                            ]}
                            inputElement={
                                <DatePicker
                                    showTime
                                    style={{
                                        width: '100%'
                                    }}
                                    disabledDate={value => value && moment(value).isBefore(moment(),'day')}
                                />
                            }
                        />
                    </Col>
                </Row>
            </Form>
            <Space align='center' size={16} className="btn-actions">
                <Button disabled={loading} onClick={onCancel}>Cancel</Button>
                <Button disabled={loading} type="primary" onClick={onSaveForm}>SAVE</Button>
            </Space>
        </Wrapper>
    )
}