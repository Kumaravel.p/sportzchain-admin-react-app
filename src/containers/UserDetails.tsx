import * as React from 'react';
import { Breadcrumb, Divider } from 'antd';
import { useParams } from 'react-router-dom';

import TitleCtaTable from 'components/common/TitleCtaTable';
import UserDetailsCard from 'components/user-details/UserDetailsCard';
import UserStats from 'components/user-details/UserStats';
import styled from 'styled-components/macro';
import {
  useGetRewardedUserByIdQuery,
  useGetUserByIdQuery,
} from 'generated/graphql';
import AddTokenModal from 'components/modals/users/AddTokenModal';

import { walletColumns } from 'data/cryptoWallet';
import { pointsColumn } from 'data/pointsWallet';
import { favouriteTeamsColumn } from 'data/favouriteTeams';
import { transactionHistoryColumn } from 'data/transactionHistory';

import defaultImage from 'assets/images/default_image.png';

const UserDetailsWrapper = styled.div`
  padding: 40px;

  .ant-breadcrumb {
    .ant-breadcrumb-separator {
      color: rgba(5, 31, 36, 0.3);
      height: 6px;
      width: 10px;
      margin: auto 10px;
    }
    .t-user {
      font-size: 25px;
      line-height: 30px;
      color: rgba(5, 31, 36, 0.3);
    }
    .t-user-name {
      font-size: 25px;
      font-weight: 600;
      line-height: 30px;
      color: #051f24;
    }
  }
`;

interface UserDetailsProps {}

enum coin {
  SPN = 'SPN',
  RWRD = 'RWRD',
}

const UserDetails = ({}: UserDetailsProps): JSX.Element => {
  const { id } = useParams<{ id: string }>();

  const [isAddTokenModalVisible, setIsAddTokenModalVisible] =
    React.useState(false);

  const { data: userData, refetch: refetchUserData } = useGetUserByIdQuery({
    variables: {
      userId: +id,
    },
  });

  const { data: rewardedUser } = useGetRewardedUserByIdQuery({
    variables: { userId: +id },
  });

  const {
    name,
    bio,
    dob,
    gender,
    email,
    mobile,
    imageURL,
    status,
    leaderboardRank,
    location,
    joinedAt,
    lifetimeSPNValue,
    lastActive,
    totalHoldingValue,
    wallets,
    createdAccounts,
    favourite_teams,
    transactions,
  } = userData?.user_by_pk ?? {};

  React.useEffect(() => {
    refetchUserData();
  }, []);

  // every coin is crypto coin except RWRD
  const cryptoWallets =
    wallets?.filter(
      ({ coinSymbol }: { coinSymbol: string }) => coinSymbol !== coin.RWRD
    ) ?? [];

  // only RWRD

  const pointsWallet =
    wallets?.filter(
      ({ coinSymbol }: { coinSymbol: string }) => coinSymbol === coin.RWRD
    ) ?? [];

  return (
    <UserDetailsWrapper>
      <Breadcrumb separator=">">
        <Breadcrumb.Item className="t-user">User</Breadcrumb.Item>
        <Breadcrumb.Item className="t-user-name" href="#">
          {name}
        </Breadcrumb.Item>
      </Breadcrumb>

      <UserDetailsCard
        name={name ?? '-'}
        id={id}
        bio={bio ?? '-'}
        dob={dob ?? ''}
        gender={gender ?? '-'}
        email={email ?? '-'}
        mobile={mobile ?? '-'}
        imageURL={imageURL ?? defaultImage}
        status={status ?? '-'}
        leaderboardRank={leaderboardRank ?? '-'}
        location={location ?? '-'}
        joinedAt={joinedAt ?? ''}
        lifetimeSPNValue={lifetimeSPNValue ?? 0}
        lastActive={lastActive ?? ''}
      />

      <UserStats
        totalHoldingValue={totalHoldingValue}
        totalLoyaltyPoints={
          rewardedUser?.rewardedUser[0]?.rewardAmount /
          rewardedUser?.rewardedUser[0]?.USDExchangeRate
        }
        totalSpnTokens={
          wallets?.find(
            ({ coinSymbol }: { coinSymbol: string }) => coinSymbol === coin.SPN
          )?.balance ?? 0
        }
      />

      <TitleCtaTable
        columns={walletColumns}
        data={cryptoWallets}
        title={`Crypto Wallet (${cryptoWallets?.length} team token${
          cryptoWallets?.length <= 1 ? '' : 's'
        })`}
        cta={
          <button onClick={() => setIsAddTokenModalVisible(true)}>
            + Add Tokens
          </button>
        }
      />

      <AddTokenModal
        isModalVisible={isAddTokenModalVisible}
        setIsModalVisible={setIsAddTokenModalVisible}
        userAddress={createdAccounts?.[0]?.accountAddress ?? ''}
      />

      <TitleCtaTable
        columns={pointsColumn}
        data={pointsWallet}
        title={`Points Wallet (${pointsWallet?.length} point token${
          pointsWallet?.length <= 1 ? '' : 's'
        })`}
        // cta="+ Add points"
      />

      <Divider />

      <TitleCtaTable
        columns={favouriteTeamsColumn}
        data={favourite_teams ?? []}
        title="Favourite Teams"
      />

      <TitleCtaTable
        columns={transactionHistoryColumn}
        data={transactions ?? []}
        title="Transaction History"
        cta="+ Sort and Filter"
      />
    </UserDetailsWrapper>
  );
};

export default UserDetails;
