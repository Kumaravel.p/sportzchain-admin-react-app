import React from 'react';
import { ReactComponent as SportzchainLogo } from 'assets/Sportzchain_logo_dark.svg';
import { Row, Col, Form, Input, Typography, Button, Spin, Space, Tooltip } from 'antd';
import styled from 'styled-components/macro';
import { useHistory, useLocation } from 'react-router-dom';
import { Auth } from 'aws-amplify';
import { toast } from 'react-toastify';
import { AccountContext } from 'context/AccountContext';
import { passwordStrength, Result } from 'check-password-strength';
import { InfoCircleOutlined } from '@ant-design/icons';

const Wrapper = styled.div`
height:100vh;
width:100%;
& .row-wrapper{
    height:100vh;
    & .background-column{
        background-color: #eee;
    }
    & .form-fields{
        padding: 40px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        & .sportzchain-logo{
            margin-bottom: 32px;
        }
        & .ant-form-item-label > label{
            font-weight: 500;
        }
        & .header{
            margin-bottom: 32px;
        }
        & .ant-typography{
            text-align:center;
        }
        & .help{
                font-size: 14px;
                font-weight: 600;
                & .ant-typography{
                    color:#fb923c;
                    cursor:pointer;
                }
            }
        & .verify-btn{
            width: 100%;
            padding: 10px;
            background: #fb923c;
            height: 100%;
            &:hover,&:focus{
                color: #000;
                border-color: #fb923c;
            }
        }
    }
}
.password-strength-wrapper {
    width:100%;
      .password-strength {
        width: 100%;
        /* display: flex; */
        column-gap: 2px;
        text-align: left;
        .ant-space-item{
            flex: 1;
            display: flex;
            justify-content: center;
        }
        .bottom-border{
            width: 60%;
            height: 2.5px;
            border-radius: 8px;
        }
        .level {
          background: #10b981;
        }
        .empty {
            background: #a3a3a3;
          }
      }
      .t-password-strength {
        display: flex;
        margin-left: 0;
        color: #10b981;
        margin-right: auto;
      }
}
`;


const ChangePassword = (): JSX.Element => {

    const [form] = Form.useForm();
    const [loading, setLoading] = React.useState<boolean>(false);
    const [passwordStrengthResult, setPasswordStrengthResult] =
        React.useState<Result<string> | null>(null);
    const history = useHistory();
    const location: { state: { email: string, password: string } } = useLocation();
    const { authenticate } = React.useContext(AccountContext);

    const onSave = () => {
        console.log(form.getFieldsValue())
    }

    const changePassword = () => {
        form.submit()
        form.validateFields()
            .then(async () => {
                setLoading(true);
                try {
                    const { password } = form.getFieldsValue();
                    authenticate(location?.state?.email, location?.state?.password).then(async (user: any) => {
                        const data = await Auth.completeNewPassword(
                            user,
                            password
                        );
                        if (data) {
                            toast.success('Password resets successfully!');
                            history.push('/signin/email');
                        }
                    }).catch((err: any) => {
                        console.error('Failed to Reset', err);
                        toast.error(err.message);
                    })
                }
                catch (error: any) {
                    console.log(error);
                    toast.error(error?.message || JSON.stringify(error));
                }
                finally {
                    setLoading(false);
                }
            })
            .catch(e => {
                console.log(e, 'e')
            })
    }

    const onValuesChange = (changedValues: any) =>{
        if(changedValues?.hasOwnProperty("password")){
            const result = passwordStrength(changedValues?.password);
            setPasswordStrengthResult(result);
        }
    }

    return (
        <Wrapper>
            <Row className="row-wrapper">
                <Col xs={24} md={14} className="background-column"></Col>
                <Col xs={24} md={10} className="form-fields">
                    <div className="sportzchain-logo">
                        <SportzchainLogo />
                    </div>
                    <Typography.Title level={2} className="header">Reset Password</Typography.Title>
                    <Typography.Title level={5} className="header">You need to reset your temporary password</Typography.Title>
                    <Form
                        onFinish={onSave}
                        form={form}
                        preserve={false}
                        // layout="vertical"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        // validateTrigger="onSubmit"
                        onValuesChange={onValuesChange}
                    >

                        <Row gutter={[12, 32]}>
                            <Col xs={24}>
                                <Form.Item
                                    name="password"
                                    label="Password"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Password is required!',
                                        },
                                        () => ({
                                            validator(_,value) {
                                                if (value && (!passwordStrengthResult?.id || (passwordStrengthResult?.id < 3))) {
                                                    return Promise.reject(new Error(`Password is ${passwordStrengthResult?.value}`));
                                                }
                                                return Promise.resolve();
                                            },
                                        }),
                                    ]}
                                >
                                    <Input.Password
                                        placeholder='Enter Password'
                                        prefix={(
                                            <Tooltip placement="right" title={(
                                                <>
                                                    Password must contain at least 8 characters
                                                    <ul>
                                                        <li>lower-case letters (a, b, c)</li>
                                                        <li>upper-case letters (A, B, C)</li>
                                                        <li>digits (1, 2 3)</li>
                                                        <li>
                                                            special characters,” which include punctuation <br /> (. ; !) and
                                                            other characters (# * &)
                                                        </li>
                                                    </ul>
                                                </>
                                            )}>
                                                <InfoCircleOutlined style={{color:"rgba(0, 0, 0, 0.45)"}} />
                                            </Tooltip>
                                        )}
                                    // type={"password"}
                                    
                                    />
                                </Form.Item>
                                <Row>
                                    <Col span={8}></Col>
                                    <Col span={16}>
                                        <div className="password-strength-wrapper">
                                            <Space align='center' className="password-strength">
                                                {
                                                    new Array(4)
                                                        .fill(0)
                                                        .map((_, i) => (
                                                            <div className={
                                                                `bottom-border ${(passwordStrengthResult && (passwordStrengthResult.id >= i)) ? "level" : "empty"}`
                                                            }
                                                                key={i}
                                                            />
                                                        ))}
                                            </Space>
                                        </div>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24}>
                                <Form.Item
                                    name="confirmPassword"
                                    label="Confirm Password"
                                    dependencies={['password']}
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Confirm Password is required!',
                                        },
                                        ({ getFieldValue }) => ({
                                            validator(_, value) {
                                                if (!value || getFieldValue('password') === value) {
                                                    return Promise.resolve();
                                                }
                                                return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                            },
                                        }),
                                    ]}
                                >
                                    <Input.Password
                                        placeholder='Confirm Password'
                                    // type={"password"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Button
                                    style={{ width: '100%' }}
                                    className="verify-btn"
                                    onClick={changePassword}
                                    loading={loading}
                                    disabled={loading}
                                >
                                    Reset Password
                                </Button>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
        </Wrapper>
    )
}

export default ChangePassword;