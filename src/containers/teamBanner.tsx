import { Card, Input, TablePaginationConfig } from 'antd';
import * as React from 'react';
import styled from 'styled-components/macro';
import { SearchOutlined } from '@ant-design/icons';
import TitleCta from 'components/common/TitleCta';
import CustomTable from 'components/customTable';
import { useHistory, useParams } from 'react-router-dom';
import { teamBannerList } from 'apis/cms/teambanner'
import { toast } from 'react-toastify';
import { useDebounce } from 'hooks/useDebounce';
import moment from 'moment';
import { teamBannerColumns } from 'data/teambanner';


const TeamBannerWrapper = styled.div`
    width: 100%;
    padding: 40px;
    background: rgba(245, 245, 247, 1);
  `;

interface teamBanner {
  type?: string;
}

const TeamBanner = ({}: teamBanner): JSX.Element => {

  
  const debounce = useDebounce();
  const [searchInput, setSearchInput] = React.useState("");
  const [teamBanner, setTeamBanner] = React.useState<any>([]);
  const { id,type } = useParams<{ id: string, type: string }>();
  const [cmsLoading, setCmsLoading] = React.useState(false);
  const history = useHistory();

  console.log(type,"??????")


  const ConstructRouteJson =
    [
      {
        name: "Tamil Thalaivas",
        route: "tamil_thalaivas",
      },
      {
        name: "Delhi Dabang",
        route: "delhi_dabang",
      },
      {
        name: "Bengal Warriors",
        route: "bengal_warriors",
      },
      {
        name: "Chennai Super Kings",
        route: "chennai_super_kings",
      },
      {
        name: "Mumbai Indians",
        route: "mumbai_indians",
      },
    ]
  let pageSize = 5;

  const [pagination, setPagination] = React.useState<TablePaginationConfig>({
    current: 1,
    pageSize,
  });

  let handleSearch = (value: string) => {
    debounce(() => setSearchInput(value), 800);
  }

  const getCmsData = async (offset: number = 1, limit: number = pageSize, from: string = "initial") => {
    await teamBannerList(
      {
        start: ((offset * limit) - limit),
        length: limit,
        searchText: searchInput
      }).then((data) => {
        setPagination({
          ...pagination,
          current: from === "initial" ? 1 : offset,
          pageSize: limit,
          total: data?.data?.data?.list?.allTeams?.totalCount
        });
        let stock: any[] = []
        if (data?.data?.data?.list?.allTeams?.nodes?.length) {
          data?.data.data.list?.allTeams?.nodes?.map((val: any) => {
            let obj: any = {}
            obj.id = val?.id;
            obj.teamId = val?.teamId;
            obj.updatedOn = moment(val?.contentItemsByTeamId?.nodes?.[0]?.updatedAt.concat('z')).format("Do MMM YY hh:mm A");
            obj.name = val?.name ?? '';
            stock.push(obj)
            return val
          })
          setCmsLoading(false);
        }
        setTeamBanner(stock);
      })
      .catch((error) => {
        console.log(error);
        toast.error(`Not able to load the Cms. Please try again!`);
      })
  }
  const onChangeTable = (newPagination: TablePaginationConfig) => {
    setCmsLoading(true);
    const { current = 1, pageSize } = newPagination;
    let offset = current;
    let from = "pagination"
    if (pagination?.pageSize !== newPagination?.pageSize) {
      offset = 0;
      from = ""
    }
    getCmsData(offset, pageSize, from)
  }

  React.useEffect(() => {
    setCmsLoading(true);
    getCmsData();
  }, [searchInput]);

  const onRowClick = (record: any) => {
    const Route = ConstructRouteJson.filter(v => v.name === record?.contentName)?.[0]?.route;
    history.push(`/teamBannerCms/${record.id}/${id}/${type}`)
  }

  return (
    <TeamBannerWrapper>
      <TitleCta
        title="CMS"
      />
      <Card>
        <TitleCta
          title={`Content`}
          cta={
            <Input
              size="middle"
              type="text"
              className="search"
              allowClear
              prefix={<SearchOutlined />}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleSearch(e.target?.value)
              }
              placeholder={'Search'}
            />
          }
        />
        <CustomTable
          columns={teamBannerColumns}
          dataSource={teamBanner}
          onRowClick={onRowClick}
          pagination={pagination}
          onChangeTable={onChangeTable}
        />
      </Card>

    </TeamBannerWrapper>
  );
};

export default TeamBanner;
