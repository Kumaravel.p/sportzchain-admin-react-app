import React from 'react';
import { Card, Row, Col,  Button, Spin } from 'antd';
import "../index.css";
import { useHistory } from 'react-router-dom';
import ViewCardGenerator from 'components/ViewCardGenerator/viewCardGenerator';
import CustomTable from 'components/customTable';
import {ArrowLeftOutlined, EyeOutlined} from "@ant-design/icons";
import moment from 'moment';
import ViewNoteModal from 'components/NewSTO/viewNotesModel';
import RewardsInfo from 'components/rewards/RewardsInfo';
import { useGetRwrdLineItemQuery } from 'generated/pgraphql';
import viewGenJSON from 'components/rewards/viewGenJSON';

const RewardDetailsPage = (): JSX.Element => {
  const history = useHistory<any>();
  const [viewModel, setviewModel] = React.useState<boolean>(false);
  const [viewModelData, setViewModelData] = React.useState<any | unknown | object>([]);

  const [viewRuleModel, setviewRuleModel] = React.useState<boolean>(false);
  const [viewRuleModelData, setViewRuleModelData] = React.useState<any | unknown | object>([]);

  const [ViewRwrd, setViewRwrd] = React.useState<any>([]);

  const {data, loading:loadingShow, error} = useGetRwrdLineItemQuery(
    {variables:{id: history?.location?.state?.id},
    fetchPolicy: 'network-only',
  }
    );

  const handleCloseViewModel = () => {
    setviewModel(false);
  }
  // Notes Column
  const NotetableColumns = [
    { title: '#', dataIndex: "no" },
    { title: 'Name', dataIndex: "name", sorter: (a: any, b: any) => a.name.localeCompare(b.name)},
    { title: 'Added by', dataIndex: "addedBy" },
    { title: 'Added date', dataIndex: "addedDate" },
    { title: '', dataIndex: "actions", component: 'icons', align: 'center' }
  ];
  // Notes Data Source
  
  let NotedataSource:any = [];
  if(data?.allRwrds?.nodes[0]?.rwrdNotesByRwrdId){
    NotedataSource= data.allRwrds.nodes[0].rwrdNotesByRwrdId.nodes?.map((val:any,i)=>({
      no: i+1,
      name: val?.name,
      addedBy: val?.addedBy,
      addedDate: moment(val?.createdDate?.concat('z')).format('MMMM D YYYY, HH:mm'),
      notes: val?.notes,
      actions: (rowRecord: any, rowIndex: number) => <EyeOutlined style={{color:"#4DA1FF"}}onClick={(e) => onClickViewModelEye(e, rowRecord, rowIndex)} />  
    }));
  }
  // Rules Column
  const RulestableColumns = [
    { title: 'STATUS', dataIndex: "status" , sorter: (a: any, b: any) => a.name.localeCompare(b.name)},
    { title: 'RULE NAME', dataIndex: "ruleName", sorter: (a: any, b: any) => a.name.localeCompare(b.name)},
    { title: 'REWARD PER USER', dataIndex: "rewardsPerUser", sorter: (a: any, b: any) => a.name.localeCompare(b.name) },
    { title: 'TOTAL QUANTITY', dataIndex: "totalQuantity", sorter: (a: any, b: any) => a.name.localeCompare(b.name) },
    { title: 'TIME PERIOF', dataIndex: "timePeriod", sorter: (a: any, b: any) => a.name.localeCompare(b.name) },
    { title: '', dataIndex: "actions", component: 'icons', align: 'center' }
  ];

  // Rules Data Source
  let RulesdataSource:any = [];
  if(data?.allRwrds?.nodes[0]?.rwrdRulesByRwrdId){
    RulesdataSource = data?.allRwrds?.nodes[0]?.rwrdRulesByRwrdId?.nodes?.map((val:any,i)=>({
      status: val?.masterRwrdRuleStatusByRuleStatusId?.status,
      ruleName: val?.ruleName,
      rewardsPerUser: parseInt(val?.rewardPerUser),
      totalQuantity: parseInt(val?.totalQuantity),
      timePeriod: moment(val?.startDate).format("DD MMM 'YY") +" - "+ moment(val?.endDate).format("DD MMM 'YY"),
      extra: {val:val},
      actions: (rowRecord: any, rowIndex: number) => <EyeOutlined style={{color:"#4DA1FF"}}onClick={(e) => onClickViewRuleEye(e, rowRecord, rowIndex)} />  
    }));
  }
  const handleCloseRuleViewModel = () => {
    setviewRuleModel(false);
  }
  const onClickViewModelEye = (e: any, rowRecord:any, rowIndex:any) => {
    setViewModelData(rowRecord);
    setviewModel(true);
  }
  const onClickViewRuleEye = (e: any, rowRecord:any, rowIndex:any) => {
    setViewRuleModelData(rowRecord);
    setviewRuleModel(true);
  }
 

  React.useEffect(()=>{
    
    (async () => {
      let getViewSto = viewGenJSON(data?.allRwrds?.nodes[0]);
      setViewRwrd(getViewSto);
    })()

  },[data?.allRwrds?.nodes[0]])
  
  return (
    <div style={{width:"100%", backgroundColor:"#F4F5F6", paddingRight:"5%"}}>
      {loadingShow  ? 
        <div style={{display: "flex",alignItems: "center",justifyContent: "center",width: "100%", height: "80%"}}>
          <Spin /> 
        </div>
        :
        <>
          <div style={{display: "flex", width: "100%", justifyContent:"space-between", marginRight: "3%", marginTop: "2%"}}>
            <div style={{display: "flex", width: "100%", justifyContent:"space-between", marginRight: "0%", marginTop: "2%"}}>
              <div style={{marginLeft:"2.8%"}}>
                <div style={{display:"flex", alignItems:"center"}}><h2 style={{fontWeight: "bold", color:"#051F244D"}}><Button shape="circle" icon={<ArrowLeftOutlined />} onClick={() => history.push('/rewards')}/>&nbsp;&nbsp;Rewards &gt;</h2>&nbsp;&nbsp;<h2 style={{fontWeight:"bold"}}>&nbsp; Category Name</h2></div>
              </div>
              <div style={{width:"5%"}}>
                <Button className="save cta-button" onClick={()=> {
                  history.push(`/edit-reward/${history?.location?.state?.id}`) 
                }} type={"primary"} size={"large"} style={{padding:"0px 24px"}}>
                  Edit
                </Button>
              </div>
            </div>
          </div>
          <ViewCardGenerator DataJSON={ViewRwrd?.RewardsDetail}/>
          <Row style={{width: "100%", margin: "30px",marginBottom:"0%"}}>
            <Card style={{width: "100%"}}>
              <Row justify='space-between' style={{width: "100%"}}>
                <Col lg={12}>
                  <h2 className="bold">Rules ({data?.allRwrds?.nodes[0]?.rwrdRulesByRwrdId?.nodes ? data?.allRwrds?.nodes[0]?.rwrdRulesByRwrdId?.nodes?.length : ""})</h2>
                </Col>
              </Row>
              <CustomTable
                  columns={RulestableColumns}
                  dataSource={RulesdataSource}
                  loading={false}
              />
              <RewardsInfo ModalVisible={viewRuleModel} handleCancel={handleCloseRuleViewModel} Data={viewRuleModelData}/>
            </Card>
          </Row>
          <Row style={{width: "100%", margin: "30px",marginBottom:"5%"}}>
            <Card style={{width: "100%"}}>
              <Row justify='space-between' style={{width: "100%"}}>
                <Col lg={12}>
                  <h2 className="bold">Notes</h2>
                </Col>
              </Row>
              <CustomTable
                  columns={NotetableColumns}
                  dataSource={NotedataSource}
                  loading={false}
              />
              <ViewNoteModal ModalVisible={viewModel} handleCancel={handleCloseViewModel} Data={viewModelData}/>
            </Card>
          </Row>
        </>
      }
      </div>
  )
}

export default RewardDetailsPage;