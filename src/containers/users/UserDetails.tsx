import React, { useMemo, useState, useEffect, useCallback } from 'react';
import { Avatar, Button, Card, Col, Modal, Row, Space, Spin, TablePaginationConfig, Typography } from 'antd';
import BreadcrumbComp from 'components/common/breadcrumb';
import LabelValue from 'components/common/LabelValue';
import { useParams } from 'react-router-dom';
import styled from 'styled-components/macro';
import CustomTable from 'components/customTable';
import { ternaryOperator } from 'utils';
import config from 'config';
// import { useDebounce } from 'hooks/useDebounce';
// import { SearchOutlined } from '@ant-design/icons';
import { toast } from 'react-toastify';
import { activityPointColumns, fixedNumber, followingTeamsColumns, formatDate, rewardsColumns, StatusId, teamTokenHoldingColumns, transactionHistoryColumns } from './utils';
import { activityPointListIF, followingTeamsIF, rewardsListIF, tableListPayload, teamTokenHoldingsIF, transactionHistoryIF, UpdateStatusInterface } from 'ts/interfaces/users';
import { activityPointListApi, followingTeamsListApi, getSpnValuesApi, getTeamTokensApi, rewardsListApi, transactionHistoryListApi, updateUserStatus, } from 'apis/users';
import { useGetUserByIdQuery } from 'generated/pgraphql';
import { parseJson } from 'utils/functions';
import { CardValue } from './CardValue';

interface UserDetailsProps { }

const Wrapper = styled('div')`
    width:100%;
    padding:40px;
    background-color: #F5F5F7;
    .ant-card{
        margin-top: 24px;
    }
    & .table-title{
        color:#000000;
        font-size: 16px;
        font-weight: 600;
        margin-bottom:16px;
    }
    .row-label-value{
        .ant-col > div{
            gap:16px;
        }
    }
    & .search-input{
        border: 1px solid rgba(5, 31, 36, 0.1);
        border-radius: 5px;
        & .ant-input-prefix{
            color:#97a0c3;
        }
    }
    & .table-list{
        width: 100%;
        margin-bottom: 16px;
        & .ant-space-item:first-of-type{
            flex:1;
            & .table-title{
                margin-bottom: 0;
            }
        }
    }
    & .btn-group{
        & .ant-btn-primary{
            min-width: 130px;
            min-height: 40px;
            border-radius: 5px;
        }
    }
`;

const FlexCenter = styled.div<{ gap: string }>`
    display:flex;
    align-items: center;
    gap:${props => `${props?.gap} !important`};
`;

const SpinWrapper = styled.div`
    display:flex;
    align-items: center;
    justify-content: center;
    height: 100%;
`;

const ModalWrapper = styled(Modal)`
    .ant-modal-body{
        padding:40px;
    }
    .modal-title{
        font-weight: 500;
        margin-block: 30px;
        text-align: center;
    }
    .flex-btn{
        & button{
            flex:1;
            min-height:40px;
            border-radius:5px;
        }
    }
`;

let pageSize = 5;
let initialPagination: TablePaginationConfig = {
    current: 1,
    pageSize,
    total: 0,
}

type ButtnGroupType = 'Suspend' | 'Block' | 'Reinstate' | null;

type loadingType = 'initial' | 'teamTokenHolding' | 'followingTeams' | 'rewards' | 'activityPoint' | 'transactionHistory' | 'statusModal' | null;

const UserDetails = (props: UserDetailsProps) => {

    //debounce hook
    // const debounce = useDebounce();
    const params: { id?: string } = useParams();

    // const [searchText, setSearchText] = useState<string>("");
    const [isVisible, setIsVisible] = useState<ButtnGroupType>(null);
    const [loading, setLoading] = useState<loadingType>(null);
    const [spnValues, setSpnValues] = useState<any>({});

    const [teamTokenHolding, setTeamTokenHolding] = useState<{
        data: teamTokenHoldingsIF[] | [],
        pagination: TablePaginationConfig
    }>({
        data: [],
        pagination: initialPagination
    });

    const [followingTeams, setFollowingTeams] = useState<{
        data: followingTeamsIF[] | [],
        pagination: TablePaginationConfig
    }>({
        data: [],
        pagination: initialPagination
    });

    const [rewardList, setRewardList] = useState<{
        data: rewardsListIF[] | [],
        pagination: TablePaginationConfig
    }>({
        data: [],
        pagination: initialPagination
    });

    const [activityPointList, setActivityPointList] = useState<{
        data: activityPointListIF[] | [],
        pagination: TablePaginationConfig
    }>({
        data: [],
        pagination: initialPagination
    });

    const [transactionHistory, setTransactionHistory] = useState<{
        data: transactionHistoryIF[] | [],
        pagination: TablePaginationConfig
    }>({
        data: [],
        pagination: initialPagination
    });

    //get User Details
    const { data: userData, loading: userDetailsLoading, refetch: refetchUserData } = useGetUserByIdQuery({
        ...(params?.id && { variables: { id: Number(params.id) }, }),
        skip: !(!!params.id),
        fetchPolicy: 'network-only',
        notifyOnNetworkStatusChange: true
    });

    useEffect(() => {
        getTableData();
    }, [])

    const getTableData = () => {

        setLoading('initial')

        let userId = Number(params.id);

        let payload: tableListPayload = {
            userId,
            start: 1,
            length: 5
        }

        getTeamTokens(payload)
        getFollowingTeams(payload)
        getRewardList(payload)
        getActivityPointList(payload)
        getTransactionHistoryList(payload)
        getSpnValues(userId)
        setLoading(null)
    }

    const setPagination = (val: tableListPayload, total: number = 0) => {
        let pagination: TablePaginationConfig = {
            current: val?.start,
            pageSize: val?.length,
            total
        }
        return pagination
    }

    const returnPayload = (val: tableListPayload): tableListPayload => {
        return {
            ...val,
            start: ((val?.start * val?.length) - val?.length),
        }
    }

    const getSpnValues = (userId: number) => {
        getSpnValuesApi(userId)
            .then((res: any) => {
                if (res?.data?.statusCode === "500") {
                    catchError(res?.data?.message)
                }
                setSpnValues(res?.data?.data)
            })
            .catch((e: any) => {
                let msg = e?.response?.data?.message || e?.message
                catchError(msg ? msg : (e?.response || JSON.stringify(e)))
            })

    }

    const getTeamTokens = async (payload: tableListPayload) => {

        if (loading !== "initial") {
            setLoading("teamTokenHolding")
        }

        await getTeamTokensApi(returnPayload(payload))
            .then((res: any) => {
                if (res?.data?.statusCode === "500") {
                    catchError(res?.data?.message)
                }
                let data = res?.data?.data;
                let constructTeamTokenHoldings: teamTokenHoldingsIF[] = data?.list?.map((_: any) => ({
                    team: _?.teamName,
                    quantity: _?.quantity,
                    quantityWithToken: `${ternaryOperator(fixedNumber(_?.quantity), 0)} ${_?.tokenSymbol}`,
                    usd: _?.usd,
                }))
                setTeamTokenHolding({
                    data: constructTeamTokenHoldings ?? [],
                    pagination: setPagination(payload, data?.totalCount)
                })

                if (loading !== "initial") {
                    setLoading(null)
                }
            })
            .catch((e: any) => {
                let msg = e?.response?.data?.message || e?.message
                catchError(msg ? msg : (e?.response || JSON.stringify(e)))
            })
    }

    const getFollowingTeams = async (payload: tableListPayload) => {

        if (loading !== "initial") {
            setLoading("followingTeams")
        }

        await followingTeamsListApi(returnPayload(payload))
            .then((res: any) => {
                if (res?.data?.statusCode === "500") {
                    catchError(res?.data?.message)
                }
                let data = res?.data?.data;
                let constructFollowingTeams: followingTeamsIF[] = data?.followingTeams?.map((_: any) => ({
                    team: _?.teamByTeamId?.name,
                    leaderboard: '-',
                    rank: 0,
                    favourite: _?.isFavourite ? "Yes" : "No",
                    followingDate: _?.likedAt
                }))
                setFollowingTeams({
                    data: constructFollowingTeams ?? [],
                    pagination: setPagination(payload, data?.totalCount)
                })

                if (loading !== "initial") {
                    setLoading(null)
                }
            })
            .catch((e: any) => {
                let msg = e?.response?.data?.message || e?.message
                catchError(msg ? msg : (e?.response || JSON.stringify(e)))
            })
    }

    const getRewardList = async (payload: tableListPayload) => {

        if (loading !== "initial") {
            setLoading("rewards")
        }

        await rewardsListApi(returnPayload(payload))
            .then((res: any) => {
                if (res?.data?.statusCode === "500") {
                    catchError(res?.data?.message)
                }
                let data = res?.data?.data;
                let constructRewardsData: rewardsListIF[] = data?.userReward?.map((_: any) => ({
                    dateTime: _?.date,
                    category: _?.category,
                    activity: _?.activity,
                    ruleName: _?.ruleName,
                    reward: _?.rewardPoint,
                    spn: _?.rewardValue,
                    status: _?.status,
                }))
                setRewardList({
                    data: constructRewardsData ?? [],
                    pagination: setPagination(payload, data?.totalCount)
                })

                if (loading !== "initial") {
                    setLoading(null)
                }

            })
            .catch((e: any) => {
                let msg = e?.response?.data?.message || e?.message
                catchError(msg ? msg : (e?.response || JSON.stringify(e)))
            })
    }

    const getActivityPointList = async (payload: tableListPayload) => {

        if (loading !== "initial") {
            setLoading("activityPoint")
        }

        await activityPointListApi(returnPayload(payload))
            .then((res: any) => {
                if (res?.data?.statusCode === "500") {
                    catchError(res?.data?.message)
                }
                let data = res?.data?.data;
                let constructActivityPointListData: activityPointListIF[] = data?.activityPoints?.map((_: any) => ({
                    dateTime: _?.date,
                    category: _?.category,
                    activity: _?.activity,
                    ruleName: _?.ruleName,
                    activityPoint: _?.activityPoint,
                }))
                setActivityPointList({
                    data: constructActivityPointListData ?? [],
                    pagination: setPagination(payload, data?.totalCount)
                })

                if (loading !== "initial") {
                    setLoading(null)
                }

            })
            .catch((e: any) => {
                let msg = e?.response?.data?.message || e?.message
                catchError(msg ? msg : (e?.response || JSON.stringify(e)))
            })
    }

    const getTransactionHistoryList = async (payload: tableListPayload) => {

        if (loading !== "transactionHistory") {
            setLoading("rewards")
        }

        await transactionHistoryListApi(returnPayload(payload))
            .then((res: any) => {
                if (res?.data?.statusCode === "500") {
                    catchError(res?.data?.message)
                }
                let data = res?.data?.data;
                let constructTransactionHistoryData: transactionHistoryIF[] = data?.transaction?.map((_: any) => ({
                    transactionHash: _?.transactionHash,
                    dateTime: _?.updatedAt,
                    transactionType: _?.type,
                    from: `${fixedNumber(_?.fromQuantity)} ${_?.fromToken}`,
                    fromQuantity: _?.fromQuantity,
                    to: `${fixedNumber(_?.toQuantity)} ${_?.toToken}`,
                    toQuantity: _?.toQuantity,
                    gasFees: 0,
                    status: _?.status,
                }))
                setTransactionHistory({
                    data: constructTransactionHistoryData ?? [],
                    pagination: setPagination(payload, data?.totalCount)
                })

                if (loading !== "transactionHistory") {
                    setLoading(null)
                }
            })
            .catch((e: any) => {
                let msg = e?.response?.data?.message || e?.message
                catchError(msg ? msg : (e?.response || JSON.stringify(e)))
            })
    }


    const catchError = (err: string) => {
        console.log(err);
        setLoading(null)
        toast.error(err ? err : 'Something went wrong!');
        return false
    }


    const onChangeTable = (newPagination: TablePaginationConfig, type: loadingType, pagination: TablePaginationConfig) => {

        const { current = 1, pageSize = 5 } = newPagination;
        let offset = current;
        if (pagination?.pageSize !== newPagination?.pageSize) {
            offset = 1;
        }

        let payload: tableListPayload = {
            userId: Number(params.id),
            length: pageSize,
            start: offset,
        }

        switch (type) {
            case "teamTokenHolding":
                return getTeamTokens(payload)
            case "followingTeams":
                return getFollowingTeams(payload)
            case "rewards":
                return getRewardList(payload)
            case "activityPoint":
                return getActivityPointList(payload)
            case "transactionHistory":
                return getTransactionHistoryList(payload)
            default:
                return null
        }
    }

    // const onChangeSearch = (val: string = "") => {
    //     setSearchText(val);
    //     // debounce(() => getRetchList(val, 1), 800);
    // }

    const isModalVisible = useMemo(() => ({
        visible: isVisible ? ['Suspend', 'Block', 'Reinstate']?.includes(isVisible) : false,
        danger: isVisible ? isVisible === "Block" : false,
        name: isVisible ? (isVisible === "Suspend") ? "SUSPEND" : (isVisible === "Block") ? "BLOCK" : "REINSTATE" : null
    }), [isVisible]);

    const onToggleModal = (type: ButtnGroupType = null) => {
        setIsVisible(type)
    }

    const userDetails = useMemo(() => userData?.userById, [userData]);

    const onSuspendOrBlock = () => {

        setLoading("statusModal")

        let userStatusId = isVisible === "Suspend" ? StatusId["suspend"] : isVisible === "Block" ? StatusId["blocked"] : (StatusId[userDetails?.status as keyof typeof StatusId]);
        let payload: UpdateStatusInterface = {
            userId: Number(params.id),
            userStatusId
        }
        updateUserStatus(payload)
            .then((res: any) => {
                if (res?.data?.statusCode === "500") {
                    // onToggleModal()
                    catchError(res?.data?.message)
                }
                onToggleModal();
                setLoading(null);
                toast.success(res?.data?.message)
                refetchUserData()
            })
            .catch((e: any) => {
                let msg = e?.response?.data?.message || e?.message;
                onToggleModal();
                catchError(msg ? msg : (e?.response || JSON.stringify(e)))
            })
    }

    const isLoadingType = (loadingType: loadingType) => {
        return ['initial', loadingType]?.includes(loading)
    }

    const constructPhoneNumber = useMemo(() => {
        let countryCode = userDetails?.countryCode ? parseJson(userDetails?.countryCode) : []
        let countryCodeValue = countryCode?.value ? countryCode?.value : userDetails?.countryCode
        let replaceCountryCode = userDetails?.mobile?.replace(countryCodeValue, "");
        let splitMobile = `${replaceCountryCode?.substring(0, 5)} ${replaceCountryCode?.substring(5, replaceCountryCode?.length)}`
        return `${countryCodeValue} ${splitMobile}`.trim() ?? '-'
    }, [userDetails]);

    const checkDate = useCallback((val) => {
        if (!val) return '-'
        return `${formatDate(val)} IST`
    }, [userDetails])

    const returnStatus = useMemo(() => {
        if (userDetails?.masterUserStatusByUserStatusId?.id) return userDetails?.masterUserStatusByUserStatusId?.description
        else if (userDetails?.status) return userDetails?.status
        return '-'
    }, [userDetails])

    const getButtonProps = useMemo(() => {
        if (userDetails?.masterUserStatusByUserStatusId?.id === 4) return "Reinstate"
        return "Suspend"
    }, [userDetails])

    return (
        <Wrapper>
            <BreadcrumbComp
                backPath='/users'
                cta={
                    <>
                        {
                            userDetails?.masterUserStatusByUserStatusId?.id !== 3 &&
                            <FlexCenter gap="16px" className='btn-group'>
                                <Button type="primary" onClick={() => onToggleModal(getButtonProps)}>{getButtonProps}</Button>
                                <Button danger type="primary" onClick={() => onToggleModal("Block")}>Block</Button>
                            </FlexCenter>
                        }
                    </>
                }
                breadCrumbs={[
                    { name: "Users", url: "/users" },
                    { name: ternaryOperator(userDetails?.name, '-'), url: `/user/${params?.id}` },
                ]}
            />
            {(loading === "initial" || userDetailsLoading) ? (
                <SpinWrapper>
                    <Spin />
                </SpinWrapper>) : (
                <>
                    <Card>
                        <Typography className='table-title'>Basic Info</Typography>
                        <Row gutter={[24, 32]} className="row-label-value">
                            <Col xs={24}>
                                <FlexCenter gap="32px">
                                    <div style={{ flex: 1 }}>
                                        <Avatar
                                            size={114}
                                            src={`${config.apiBaseUrl}files/${userDetails?.imageUrl}`}
                                        >
                                            {userDetails?.name?.charAt(0)}
                                        </Avatar>
                                    </div>
                                    <LabelValue
                                        field="Bio"
                                        value={ternaryOperator(userDetails?.bio, '-')}
                                    />
                                </FlexCenter>
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Full Name"
                                    value={ternaryOperator(userDetails?.name, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Username"
                                    value={ternaryOperator(userDetails?.username, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Status"
                                    value={returnStatus}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Gender"
                                    value={ternaryOperator(userDetails?.gender, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Date of Birth"
                                    value={checkDate(userDetails?.dob)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Location"
                                    value={ternaryOperator(userDetails?.location, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Email"
                                    value={ternaryOperator(userDetails?.email, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Phone Number"
                                    value={ternaryOperator(constructPhoneNumber, '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Platform Leaderboard"
                                    value={ternaryOperator('-', '-')}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Joining Date"
                                    value={checkDate(userDetails?.joinedAt)}
                                />
                            </Col>
                            <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Last Activity"
                                    value={checkDate(userDetails?.loginActivitiesByUserId?.nodes?.[0]?.lastLogin)}
                                />
                            </Col>
                            {/* <Col xs={24} sm={12} md={8}>
                                <LabelValue
                                    field="Lifetime Value"
                                    value={ternaryOperator('-', '-')}
                                />
                            </Col> */}
                        </Row>
                    </Card>
                    <Row gutter={[24, 24]} style={{ paddingBottom: 24 }}>
                        <Col xs={24} sm={12} md={8}>
                            <CardValue
                                title='Current Holding Value'
                                subTitle={`$${(fixedNumber(spnValues?.currentHoldingValue))}`}
                            />
                        </Col>
                        <Col xs={24} sm={12} md={8}>
                            <CardValue
                                title='Lifetime Claimed RWRD'
                                subTitle={`${(fixedNumber(spnValues?.claimedRwrd))} RWRD`}
                                spanText={`${(fixedNumber(spnValues?.claimedRwrdInSPN))} SPN • $${fixedNumber(spnValues?.claimedRwrdInUSD)}`}
                            />
                        </Col>
                        <Col xs={24} sm={12} md={8}>
                            <CardValue
                                title='Unclaimed RWRD'
                                subTitle={`${(fixedNumber(spnValues?.unClaimedRwrd))} RWRD`}
                                spanText={`${(fixedNumber(spnValues?.unclaimedRwrdInSpn))} SPN • $${fixedNumber(spnValues?.unclaimedRwrdInUSD)}`}
                            />
                        </Col>
                        <Col xs={24} sm={12} md={8}>
                            <CardValue
                                title='Current SPN Holding'
                                subTitle={`${(fixedNumber(spnValues?.currentSpn))} SPN`}
                                spanText={`$${(fixedNumber(spnValues?.currentSpnInUsd))}`}
                            />
                        </Col>
                        <Col xs={24} sm={12} md={8}>
                            <CardValue
                                title='Total Gas Spent - User'
                                subTitle={`${(fixedNumber(spnValues?.userTotalGasFee))} SPN`}
                                spanText={`$${(fixedNumber(spnValues?.userTotalGasFeeInUsd))}`}
                            />
                        </Col>

                        <Col xs={24} sm={12} md={8}>
                            <CardValue
                                title='Total Gas Spent - User'
                                subTitle={`${(fixedNumber(spnValues?.platformTotalGasFee))} SPN`}
                                spanText={`$${(fixedNumber(spnValues?.platformTotalGasFeeInUsd))}`}
                            />
                        </Col>
                    </Row>
                    <Card>
                        <Space align="center" size={24} wrap className='table-list'>
                            <Typography className='table-title'>{'Team Token Holdings'}</Typography>
                            {/* <Input
                                type="text"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeSearch(e.target.value)}
                                value={searchText}
                                autoComplete={"off"}
                                placeholder="Search by Team Name"
                                className="search-input"
                                prefix={<SearchOutlined />}
                            /> */}
                        </Space>
                        <CustomTable
                            columns={teamTokenHoldingColumns}
                            dataSource={teamTokenHolding?.data ?? []}
                            loading={isLoadingType('teamTokenHolding')}
                            pagination={teamTokenHolding?.pagination}
                            onChangeTable={(newPagination) => onChangeTable(newPagination, "teamTokenHolding", teamTokenHolding?.pagination)}
                        />
                    </Card>
                    <Card>
                        <Space align="center" size={24} wrap className='table-list'>
                            <Typography className='table-title'>{'Following Teams'}{` (${ternaryOperator(followingTeams?.pagination?.total, 0)})`}</Typography>
                            {/* <Input
                                type="text"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeSearch(e.target.value)}
                                value={searchText}
                                autoComplete={"off"}
                                placeholder="Search by Team Name"
                                className="search-input"
                                prefix={<SearchOutlined />}
                            /> */}
                        </Space>
                        <CustomTable
                            columns={followingTeamsColumns}
                            dataSource={followingTeams?.data ?? []}
                            loading={isLoadingType('followingTeams')}
                            pagination={followingTeams?.pagination}
                            onChangeTable={(newPagination) => onChangeTable(newPagination, "followingTeams", followingTeams?.pagination)}
                        />
                    </Card>
                    <Card>
                        <Space align="center" size={24} wrap className='table-list'>
                            <Typography className='table-title'>{'Rewards'}{` (${ternaryOperator(rewardList?.pagination?.total, 0)})`}</Typography>
                            {/* <Input
                                type="text"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeSearch(e.target.value)}
                                value={searchText}
                                autoComplete={"off"}
                                placeholder="Search by Team Name"
                                className="search-input"
                                prefix={<SearchOutlined />}
                            /> */}
                        </Space>
                        <CustomTable
                            columns={rewardsColumns}
                            dataSource={rewardList?.data ?? []}
                            loading={isLoadingType('rewards')}
                            pagination={rewardList?.pagination}
                            onChangeTable={(newPagination) => onChangeTable(newPagination, "rewards", rewardList?.pagination)}
                        />
                    </Card>
                    <Card>
                        <Space align="center" size={24} wrap className='table-list'>
                            <Typography className='table-title'>{'Activity Point'}{` (${ternaryOperator(activityPointList?.pagination?.total, 0)})`}</Typography>
                            {/* <Input
                                type="text"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeSearch(e.target.value)}
                                value={searchText}
                                autoComplete={"off"}
                                placeholder="Search by Team Name"
                                className="search-input"
                                prefix={<SearchOutlined />}
                            /> */}
                        </Space>
                        <CustomTable
                            columns={activityPointColumns}
                            dataSource={activityPointList?.data ?? []}
                            loading={isLoadingType('activityPoint')}
                            pagination={activityPointList?.pagination}
                            onChangeTable={(newPagination) => onChangeTable(newPagination, "activityPoint", activityPointList?.pagination)}
                        />
                    </Card>
                    <Card>
                        <Space align="center" size={24} wrap className='table-list'>
                            <Typography className='table-title'>{'Transaction History'}{` (${ternaryOperator(transactionHistory?.pagination?.total, 0)})`}</Typography>
                            {/* <Input
                                type="text"
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeSearch(e.target.value)}
                                value={searchText}
                                autoComplete={"off"}
                                placeholder="Search by Team Name"
                                className="search-input"
                                prefix={<SearchOutlined />}
                            /> */}
                        </Space>
                        <CustomTable
                            columns={transactionHistoryColumns}
                            dataSource={transactionHistory?.data ?? []}
                            loading={isLoadingType('transactionHistory')}
                            pagination={transactionHistory?.pagination}
                            onChangeTable={(newPagination) => onChangeTable(newPagination, "transactionHistory", transactionHistory?.pagination)}
                        />
                    </Card>
                </>
            )}
            <ModalWrapper
                centered
                visible={isModalVisible?.visible}
                onCancel={() => onToggleModal()}
                destroyOnClose={true}
                footer={null}
            >
                <Typography.Title level={5} className="modal-title" >
                    Are you sure you want to {isVisible} {ternaryOperator(userDetails?.name, '-')}?
                </Typography.Title>
                <FlexCenter gap={"16px"} className="flex-btn">
                    <Button onClick={() => onToggleModal()}>Cancel</Button>
                    <Button
                        danger={isModalVisible?.danger}
                        type="primary"
                        onClick={onSuspendOrBlock}
                        loading={loading === "statusModal"}
                    >
                        {isModalVisible?.name}
                    </Button>
                </FlexCenter>
            </ModalWrapper>
        </Wrapper>
    )
}

export default UserDetails