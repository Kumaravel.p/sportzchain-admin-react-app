import moment from "moment";
import { middleEllipsis } from "utils/functions";

const isValidMoment = (val: string) => moment(val + 'z').isValid() ? moment(val + 'z') : moment(val)

export const toLowerCase = (val: string) => val ? val?.toLowerCase()?.trim() : "";
export const formatDate = (date: string, format: string = "DD MMM YYYY, hh:mm a") => isValidMoment(date).format(format);
export const fixedNumber = (value: number | string, digit: number = 2) => isNaN(Number(value)) ? 0 : Number(Number(value).toFixed(digit));

const teamTokenHoldingColumns = [
    {
        title: 'Team',
        dataIndex: "team", sorter: (a: any, b: any) => toLowerCase(a?.team).localeCompare(toLowerCase(b?.team))
    },
    {
        title: 'Quantity',
        dataIndex: "quantityWithToken",
        sorter: (a: any, b: any) => toLowerCase(a?.quantity).localeCompare(toLowerCase(b?.quantity)),
    },
    {
        title: 'USD',
        dataIndex: "usd",
        sorter: (a: any, b: any) => toLowerCase(a?.usd).localeCompare(toLowerCase(b?.usd)),
        valueFormat: (value: string) => `$${fixedNumber(value)}`,
    },
]

const followingTeamsColumns = [
    { title: 'Team', dataIndex: "team", sorter: (a: any, b: any) => toLowerCase(a?.team).localeCompare(toLowerCase(b?.team)) },
    { title: 'Leaderboad', dataIndex: "leaderboad", sorter: (a: any, b: any) => toLowerCase(a?.leaderboad).localeCompare(toLowerCase(b?.leaderboad)) },
    { title: 'Rank', dataIndex: "rank", sorter: (a: any, b: any) => Number(a.rank) - Number(b.rank) },
    { title: 'Favourite', dataIndex: "favourite", },
    {
        title: 'Favourite Date',
        dataIndex: "followingDate",
        valueFormat: (value: string) => `${formatDate(value)} IST`,
        sorter: (a: any, b: any) => moment(a.followingDate).unix() - moment(b.followingDate).unix()
    },
]

const rewardsColumns = [
    {
        title: 'Date & Time',
        dataIndex: "dateTime",
        valueFormat: (value: string) => `${formatDate(value)} IST`,
        sorter: (a: any, b: any) => moment(a.dateTime).unix() - moment(b.dateTime).unix()
    },
    { title: 'Category', dataIndex: "category", sorter: (a: any, b: any) => toLowerCase(a?.category).localeCompare(toLowerCase(b?.category)) },
    { title: 'Activity', dataIndex: "activity", sorter: (a: any, b: any) => toLowerCase(a?.activity).localeCompare(toLowerCase(b?.activity)) },
    { title: 'Rule Name', dataIndex: "ruleName", sorter: (a: any, b: any) => toLowerCase(a?.ruleName).localeCompare(toLowerCase(b?.ruleName)) },
    {
        title: 'Reward',
        dataIndex: "reward",
        valueFormat: (value: number) => fixedNumber(value),
        sorter: (a: any, b: any) => Number(a.reward) - Number(b.reward)
    },
    {
        title: 'SPN',
        dataIndex: "spn",
        valueFormat: (value: number) => fixedNumber(value),
        sorter: (a: any, b: any) => Number(a.spn) - Number(b.spn)
    },
    { title: 'Status', dataIndex: "status", sorter: (a: any, b: any) => toLowerCase(a?.spn).localeCompare(toLowerCase(b?.spn)) },
    // {
    //     title: 'Activity Point',
    //     dataIndex: "activityPoint",
    //     valueFormat: (value: number) => fixedNumber(value),
    //     sorter: (a: any, b: any) => Number(a.activityPoint) - Number(b.activityPoint)
    // },
]

const activityPointColumns = [
    {
        title: 'Date & Time',
        dataIndex: "dateTime",
        valueFormat: (value: string) => `${formatDate(value)} IST`,
        sorter: (a: any, b: any) => moment(a.dateTime).unix() - moment(b.dateTime).unix()
    },
    { title: 'Category', dataIndex: "category", sorter: (a: any, b: any) => toLowerCase(a?.category).localeCompare(toLowerCase(b?.category)) },
    { title: 'Activity', dataIndex: "activity", sorter: (a: any, b: any) => toLowerCase(a?.activity).localeCompare(toLowerCase(b?.activity)) },
    { title: 'Rule Name', dataIndex: "ruleName", sorter: (a: any, b: any) => toLowerCase(a?.ruleName).localeCompare(toLowerCase(b?.ruleName)) },
    {
        title: 'Activity Point',
        dataIndex: "activityPoint",
        valueFormat: (value: number) => fixedNumber(value),
        sorter: (a: any, b: any) => Number(a.activityPoint) - Number(b.activityPoint)
    },
]

const transactionHistoryColumns = [
    {
        title: 'Transaction Hash',
        dataIndex: "transactionHash",
        valueFormat: (value: string) => middleEllipsis(value),
    },
    {
        title: 'Date & Time',
        dataIndex: "dateTime",
        valueFormat: (value: string) => `${formatDate(value)} IST`,
        sorter: (a: any, b: any) => moment(a.dateTime).unix() - moment(b.dateTime).unix()
    },
    { title: 'Transaction Type', dataIndex: "transactionType" },
    {
        title: 'From',
        dataIndex: "from",
        sorter: (a: any, b: any) => Number(a.fromQuantity) - Number(b.fromQuantity)
    },
    {
        title: 'To',
        dataIndex: "to",
        sorter: (a: any, b: any) => Number(a.toQuantity) - Number(b.toQuantity)
    },
    {
        title: 'Gas Fees',
        dataIndex: "gasFees",
        sorter: (a: any, b: any) => Number(a.gasFees) - Number(b.gasFees)
    },
    { title: 'Status', dataIndex: "status" },
]

export {
    teamTokenHoldingColumns,
    followingTeamsColumns,
    rewardsColumns,
    transactionHistoryColumns,
    activityPointColumns
}

export enum StatusId {
    "verified" = 1,
    "unverified",
    "blocked",
    "suspend",
}