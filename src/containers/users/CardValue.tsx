import React from 'react';
import { Card, Typography } from 'antd';
import styled from 'styled-components/macro';

interface CardValueProps {
    title?: string;
    subTitle?: string;
    spanText?: string;
}

const CardWrapper = styled(Card)`
    border-radius:8px;
    height:100%;
    & .ant-card-body{
        display:flex;
        flex-direction:column;
        gap:16px;
        height: 100%;
        justify-content: center;
        &::before,::after{
            display: none;
        }
        & .title{
            color:#A3A3A3;
            font-weight: 500;
            font-size: 14px;
        }
        & .centerBtn{
            display:flex;
            align-items: center;
            justify-content: space-between;
            flex-wrap: wrap;
            gap: 16px;
            & .subtitle{
                color:#4DA1FF;
                line-height:1;
                font-weight: 500;
                font-size: 20px;
            }
            & .spanText{
                font-weight: 400;
                font-size: 14px;
                color: #505050;
            }
        }
    }
`;

export const CardValue = (props: CardValueProps): JSX.Element => {

    const {
        title = "",
        subTitle = "",
        spanText = "",
    } = props;

    return (
        <CardWrapper>
            {title && <Typography.Text className="title">{title}</Typography.Text>}
            <div className="centerBtn">
                {subTitle && <Typography.Text className='subtitle'>{subTitle}</Typography.Text>}
                {spanText && <Typography.Text className='spanText'>{spanText}</Typography.Text>}
            </div>
        </CardWrapper>
    )
}