import React, { useEffect, useMemo, useState } from 'react';
import { Col, Input, Button, Space, Typography, Card, Row } from 'antd';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import CustomTable from 'components/customTable'
import type { TablePaginationConfig } from 'antd/lib/table';
import SelectElement from 'components/common/SelectElement';
import { toast } from 'react-toastify';
import { useDebounce } from 'hooks/useDebounce';
import { CloseOutlined, EyeOutlined, SearchOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import { getUserCountsApi, userListingApi } from 'apis/users';
import { userListingPayload } from 'ts/interfaces/users';
import config from 'config';
import { ternaryOperator } from 'utils';
import { fixedNumber } from './utils';
import { useGetAllCountryListQuery, useGetAllLeaderboardsByTypeQuery } from 'generated/pgraphql';
import currentSession from 'utils/getAuthToken';
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

const UsersWrapper = styled('div')`
    width:100%;
    padding:32px;
    background-color:#F5F5F7;
    ${FlexRowWrapper}{
        align-items:center;   
        margin-bottom:32px;
        gap:16px;
        .title{
            color: #051f24;
            font-weight: 600;
            font-size: 25px;
            flex:1;
            margin:0;
        }
        .create-rewards-btn{
            display:flex;
            align-items:center;
            color:#07697D;
            border-color:#07697D;
            border-radius: 5px;
            font-weight: 500;
            font-size: 16px;
            gap: 12px;
            height: 100%;
            padding: 10px;
            & span{
                line-height:1;
            }
        }
    }
    .ant-space{
        width:100%;
        margin-bottom:32px;
        & .ant-space-item:first-child{
            flex:1;
        }
        & .ant-typography{
            color:#051F2499;
            font-weight: 400;
            font-size: 14px;
            margin-bottom:8px
        }
        & .ant-col{
            & .ant-input,.ant-picker{
                border-radius: 5px;
                border: 1px solid #97a0c3;
            }
        }
        & .clear-filter{
            color: #FF4D4A;
            border-color: #FF4D4A;
            border-radius: 5px;
            & .anticon{
                font-size:12px;
            }
        }
    }
    .ant-card-body{
        padding:24px;
        padding-left:8px;
        ${FlexRowWrapper}{
            margin-bottom:16px;
            & .title{
                font-size:18px;
                padding-left:16px
            }
            & .search-input{
                border: 1px solid rgba(5, 31, 36, 0.1);
                border-radius: 5px;
                & .ant-input-prefix{
                color:#97a0c3;
            }
            }
        }
    }
    & .card-chips{ 
        margin-bottom:24px;
        & .ant-card-bordered{
            border-radius:5px;
            & .ant-card-body{
                &::before,::after{
                    display: none;
                }
                display:flex;
                flex-direction:column;
                gap:10px;
                padding-left: 24px;
                & .title_text{
                    color:#323C47;
                    font-weight: 400;
                    font-size: 14px;
                }
                & .subtitle_text{
                    line-height:1;
                    font-weight: 500;
                    font-size: 24px;
                }
                .centerBtn{
                    display:flex;
                    align-items: center;
                    justify-content: space-between;
                    gap: 16px;
                }
                .success_text{
                    color:#6BCE6F;
                }
                .danger_text{
                    color:#EF4444;
                }
                .active_text{
                    color:#4DA1FF;
                }
                .disabled_text{
                    color: #B4B4B4;
                    font-weight: 400;
                    font-size: 14px;
                }
            }
        }
    }
`;

interface UserListingProps { }

interface TableDataType {
    avatar: {
        src: string;
        firstChar: string;
    };
    name: string;
    location: string;
    currentHoldings: string;
    leaderboard: string;
    status: string;
    actions: any;
}

const toLowerCase = (val: string) => val?.toLowerCase()?.trim();

const tableColumns = [
    { title: 'Avatar', dataIndex: "avatar", component: 'avatar' },
    { title: 'Name', dataIndex: "name", sorter: (a: any, b: any) => toLowerCase(a?.name).localeCompare(toLowerCase(b?.name)) },
    { title: 'Location', dataIndex: "location", sorter: (a: any, b: any) => toLowerCase(a?.location).localeCompare(toLowerCase(b?.location)) },
    {
        title: 'Current Holdings',
        dataIndex: "currentHoldings",
        sorter: (a: any, b: any) => a?.currentHoldings - b?.currentHoldings,
        valueFormat: (value: string) => ternaryOperator(fixedNumber(value), '0')
    },
    { title: 'Leaderboard', dataIndex: "leaderboard" },
    { title: 'Status', dataIndex: "status", valueFormat: (value: string) => value },
    { title: 'Actions', dataIndex: "actions", component: 'icons', align: 'center' },
]

let initialState = {
    status: null,
    location: null,
    leaderboard: null,
}

type CountryOptionsType = {
    title: string
}[];

let pageSize = 5;

const UserListing = ({ }: UserListingProps): JSX.Element => {

    //debounce hook
    const debounce = useDebounce();
    const history = useHistory();

    const { data: countriesOption, loading: countriesOptionLoading } = useGetAllCountryListQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true
    });

    const { data: leaderboardsTypeList, loading: leaderboardsTypeListLoading } = useGetAllLeaderboardsByTypeQuery({
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        variables: {
            leaderboardTypeByLevelId: ["Platform", "Team"]
        }
    });

    const [pagination, setPagination] = React.useState<TablePaginationConfig>({
        current: 1,
        pageSize,
        total: 0,
    });
    const [searchText, setSearchText] = useState<string>("");
    const [state, setState] = React.useState<any>(initialState);
    const [loading, setLoading] = useState<boolean>(false);
    const [dataSource, setDataSource] = useState<TableDataType[]>([]);
    const [userCount, setUserCount] = useState<{
        totalUsers: number;
        newUsers: number;
        inactiveUsers: number;
    }>({
        totalUsers: 0,
        newUsers: 0,
        inactiveUsers: 0,
    })
    const { profile } = useSnapshot(ProxyState);

    useEffect(() => {
        getAllUsers(1, pageSize, searchText, state)
        getUserCounts()
    }, []);

    const getUserCounts = () => {
        currentSession().then((res: any) => {
            let userId = Number(profile?.user_id);
            getUserCountsApi(userId)
                .then((res: any) => {
                    if (res?.data?.statusCode === "500") {
                        catchError(res?.data?.message)
                    }
                    let { totalUsers = 0, newUsers = 0, inactiveUsers = 0 } = res?.data?.data;
                    setUserCount({ totalUsers, newUsers, inactiveUsers })
                })
                .catch((e: any) => {
                    catchError(e?.message)
                })
        })
    }

    //on search input in the table
    const onSearchTableList = (value: string) => {
        setSearchText(value)
        debounce(() => getAllUsers(1, pageSize, value, state), 800);
    }

    //to clear filter
    const clearFilters = () => {
        setSearchText("")
        setState(initialState);
        getAllUsers(1, pageSize, "", initialState)
    }

    const onClickIcons = (e: any, rowRecord: any, rowIndex: number) => {
        e.stopPropagation();
        onRow(rowRecord)
    }

    // get all Groups
    const getAllUsers = (offset: number = 1, limit: number = pageSize, searchText: string = "", variables: any, from: string = "initial") => {
        setLoading(true);
        let payload: userListingPayload = {
            "start": ((offset * limit) - limit),
            "length": limit,
            ...(searchText && { searchText }),
            ...(variables?.status && { status: variables?.status }),
            ...(variables?.location && { location: variables?.location }),
            ...(variables?.leaderboard && { leaderboard: variables?.leaderboard }),

        }

        userListingApi(payload).then((res: any) => {
            let users = res?.data?.data;
            let contructUsers = [];
            if (res?.data?.statusCode === "500") {
                catchError(res?.data?.message)
            }
            if (users?.list?.length) {
                contructUsers = users?.list?.map((user: any) => {
                    return (
                        {
                            id: user?.id,
                            avatar: {
                                src: `${config.apiBaseUrl}files/${user?.imageUrl}`,
                                firstChar: user?.name?.charAt(0),
                            },
                            name: ternaryOperator(user?.name, '-'),
                            location: ternaryOperator(user?.location, '-'),
                            currentHoldings: ternaryOperator(user?.currentHoldings, 0),
                            leaderboard: ternaryOperator('-', '-'),
                            status: ternaryOperator(user?.status, '-'),
                            actions: (rowRecord: any, rowIndex: number) => (
                                <EyeOutlined style={{ color: "#1890ff" }} className='remove-btn' onClick={(e: any) => onClickIcons(e, rowRecord, rowIndex)} />
                            )
                        }
                    )
                })
            }
            setDataSource(contructUsers);
            setPagination({
                ...pagination,
                current: from === "initial" ? 1 : offset,
                pageSize: limit,
                total: users?.totalCount
            })
            setLoading(false)
        }).catch(err => catchError(err?.message));
    }

    const catchError = (err: string) => {
        console.log(err);
        setLoading(false)
        toast.error(err ? err : 'Something went wrong!');
        return false
    }

    const onChangeState = (key: string, value: any) => {
        let updateState = {
            ...state, [key]: value

        }
        setState(updateState)
        getAllUsers(1, pageSize, searchText, updateState)
    }

    const onChangeTable = (newPagination: TablePaginationConfig) => {
        const { current = 1, pageSize } = newPagination;
        let offset = current;
        let from = "pagination"
        if (pagination?.pageSize !== newPagination?.pageSize) {
            offset = 1;
            from = ""
        }
        getAllUsers(offset, pageSize, searchText, state, from)
    }

    const onRow = (record: any) => {
        history.push(`/user/${record?.id}`);
    }

    const getLeaderboardTypeOptions = useMemo(() => {
        if (leaderboardsTypeListLoading || !leaderboardsTypeList?.allLeaderboards?.nodes?.length) return []
        return leaderboardsTypeList?.allLeaderboards?.nodes?.map(_ => ({ name: _?.masterLeaderboardByTypeId?.name }))
    }, [leaderboardsTypeList, leaderboardsTypeListLoading])

    return (
        <UsersWrapper>
            <FlexRowWrapper>
                <p className='title'>Users</p>
            </FlexRowWrapper>
            <Row gutter={[24, 8]} className="card-chips">
                <Col xs={24} sm={12} md={8}>
                    <Card>
                        <Typography.Text className='title_text'>Total Users</Typography.Text>
                        <Typography.Text className='subtitle_text active_text'>{ternaryOperator(userCount?.totalUsers, 0)}</Typography.Text>
                    </Card>
                </Col>
                <Col xs={24} sm={12} md={8}>
                    <Card>
                        <Typography.Text className='title_text'>New Users</Typography.Text>
                        <div className="centerBtn">
                            <Typography.Text className='subtitle_text success_text'>{ternaryOperator(userCount?.newUsers, 0)}</Typography.Text>
                            <Typography.Text className='disabled_text'>{'Last 7 Days'}</Typography.Text>
                        </div>
                    </Card>
                </Col>
                <Col xs={24} sm={12} md={8}>
                    <Card>
                        <Typography.Text className='title_text'>Inactive Users</Typography.Text>
                        <div className="centerBtn">
                            <Typography.Text className='subtitle_text danger_text'>{ternaryOperator(userCount?.inactiveUsers, 0)}</Typography.Text>
                            <Typography.Text className='disabled_text'>{'Last 30 Days'}</Typography.Text>
                        </div>
                    </Card>
                </Col>
            </Row>
            <Space size={24} align="end">
                <Row gutter={[24, 24]}>
                    <Col xs={24} sm={8} md={6}>
                        <Typography>Status</Typography>
                        <SelectElement
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeState('status', e)}
                            value={state?.status ?? null}
                            options={[
                                { status: "verified" },
                                { status: "unverified" },
                                { status: "blocked" },
                                { status: "suspend" },
                            ]}
                            placeholder="Status"
                            searchable={true}
                            toFilter="status"
                            loading={false}
                        />
                    </Col>
                    <Col xs={24} sm={8} md={6}>
                        <Typography>Leaderboard</Typography>
                        <SelectElement
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeState('leaderboard', e)}
                            value={state?.leaderboard ?? null}
                            options={getLeaderboardTypeOptions ?? []}
                            placeholder="Leaderboard"
                            searchable={true}
                            toFilter="name"
                            loading={leaderboardsTypeListLoading}
                        />
                    </Col>
                    <Col xs={24} sm={8} md={6}>
                        <Typography>Location</Typography>
                        <SelectElement
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChangeState('location', e)}
                            value={state?.location ?? null}
                            options={countriesOption?.allCountries?.nodes as unknown as CountryOptionsType ?? []}
                            placeholder="Location"
                            searchable={true}
                            toFilter="title"
                            loading={countriesOptionLoading}
                        />
                    </Col>
                </Row>
                <Button onClick={clearFilters} className='clear-filter'>
                    <CloseOutlined />
                    Clear Filter
                </Button>
            </Space>
            <Card>
                <FlexRowWrapper>
                    <p className='title'>List of Users ({pagination?.total ?? 0})</p>
                    <Col xs={24} sm={8} md={6}>
                        <Input
                            type="text"
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => onSearchTableList(e.target.value)}
                            value={searchText}
                            autoComplete={"off"}
                            placeholder="Search by Name"
                            className="search-input"
                            prefix={<SearchOutlined />}
                        />
                    </Col>
                </FlexRowWrapper>
                <CustomTable
                    columns={tableColumns}
                    dataSource={dataSource}
                    loading={loading}
                    pagination={pagination}
                    onChangeTable={onChangeTable}
                    onRowClick={onRow}
                />
            </Card>
        </UsersWrapper>
    );
};

export default UserListing;

