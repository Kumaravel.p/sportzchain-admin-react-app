import { Button, Card, Switch, Row, Col, Spin, Divider } from 'antd';
import React,{ useMemo } from 'react';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import UserInfo from 'components/user-management/UserInfo';
import styled from 'styled-components/macro';
import { LeftOutlined } from '@ant-design/icons';
import TeamInfo from 'components/team/TeamInfo';
import {
  GetPollQuery,
  useGetPollQuery,
  useGetPostsQuery,
  useGetTeamsQuery,
} from 'generated/graphql';
import { Link, useLocation } from 'react-router-dom';
import TitleCta from 'components/common/TitleCta';
import { useParams } from 'react-router-dom';
import PollTemplate from 'components/poll/PollTemplate';
import { useHistory } from 'react-router-dom';
import { Label, TextContent } from 'components/common/atoms';
import { format } from 'date-fns';
import OptionsView from 'components/poll/optionsview';
import BreadcrumbComp from 'components/common/breadcrumb';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import config from 'config';
import moment from 'moment';
import { TeamSelectionContext } from 'App';
import { GetNews } from 'graphql_v2';
import graphqlReq from 'apis/graphqlRequest';
import LabelInput from 'components/common/LabelInput';
import TextEditorInput from 'components/common/TextEditorInput';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ChartDataLabels
);

const PollsManagementWrapper = styled.div`
  width: 100%;
  padding: 40px;
  background: rgba(245, 245, 247, 1);
  display: grid;
  grid-gap: 24px;
  .ant-divider-horizontal {
    margin: 12px 0px;
  }
  .ant-card {
    border-radius: 10px;
    .ant-card-head {
      border-bottom: none;
    }
  }
  .add-new-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;
  }

  .banner-image {
    &.banner-urls {
      display: flex;
      column-gap: 20px;
      width: 70vw;
      overflow-x: scroll;
      img {
        height: 500px;
        width: 800px;
      }
    }
    img.logo {
      max-width: 300px;
      border-radius: 4px;
      max-height: 300px;
    }
  }

  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-user-management {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }

   

    .ant-btn.add-new-button {
      border: 1px solid rgba(7, 105, 125, 0.25);
      color: rgba(7, 105, 125, 1);
      box-sizing: border-box;
      border-radius: 5px;
      height: fit-content;
      /* padding: 15px; */
      display: flex;
      align-items: center;
      column-gap: 10px;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */

    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      /* Red / 1 */
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }

  .t-month {
    font-size: 14px;
    line-height: 19px;
    display: flex;
    align-items: flex-end;
    letter-spacing: -0.1px;
    color: rgba(50, 60, 71, 0.4);
  }

  .count-monthly-wrapper {
    justify-content: space-between;
    align-items: center;
  }

  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
  .cta-button {
    font-weight: 500;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;
    margin-left: 10px;
  }

  .cta-button_live {
    border-radius: 5px;
    font-weight: 500;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;
    border: 1px solid #369afe;
    color: white;
    background-color: #369afe;
    margin-left: 10px;
  }

  .t-allow-multiple {
    font-size: 16px;
    line-height: 150%;
    color: #0c2146;
  }
  & .stats-count {
    color: #4da1ff;
    font-size: 26px;
    margin: 0;
    font-weight:700
  }
  & .live-preview-poll {
    background: #1890ff;
    color: #fff;
    font-weight: 500;
    border-radius: 5px;
    padding: 8px;
  }
`;


const NewsView = () => {
  const { state } = useLocation<any>();

  const { id, teamId } = useParams<{ id: string, teamId: string }>();
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  const [poll,setPolls] = React.useState<any>();
  const [loading,setLoading] = React.useState(false)
  const [isVisible,setIsVisible] = React.useState<boolean>(false);
  const [editIndex,setEditIndex] = React.useState<number>(-1);
  const history = useHistory();

  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  if (isIamTeamUser()) {
    if (parseInt(teamId) !== teamSelectionContext.team.id) {
      history.push("/");
    }
  }

  React.useEffect(()=>{
    setLoading(true)
    graphqlReq({ document: GetNews(parseInt(id)) })
    .then((data: any) => {
       setPolls(data?.allPosts?.nodes[0])
       setLoading(false)
    })
    .catch(e => console.log(e))
  },[])

  const getValueByType = React.useCallback((team: string | undefined, group: string | undefined) => {
    return poll?.type === "GROUPS" ? group : team
  }, [poll])

  const contructNoteJson = useMemo(() => {
    let data = poll?.postNotesByPostId?.nodes;
    if (!data?.length) return []
    return data?.map((note: any) => ({
      name: note?.name,
      notes: note?.notes,
      addedDate: moment(note?.createdDate.concat('z')),
      addedby: note?.addedBy,
      id: note?.id,
    }))
  }, [poll])

  const viewNoteData = useMemo(() => {
    if (editIndex > -1) {
      return {
        name: contructNoteJson[editIndex]?.name,
        note: contructNoteJson[editIndex]?.notes
      }
    }
    return {}
  }, [editIndex])

  const handleNoteModal = (bool: boolean = false, index: number = -1) => {
    setIsVisible(bool)
    setEditIndex(!bool ? -1 : index)
  }

  
  
  return (
    <PollsManagementWrapper>
      {loading && !poll ? (
        <Row style={{ height: '70%' }} justify="center" align="middle">
          <Spin />
        </Row>
      ) : (
        <>
          <Row>
            <BreadcrumbComp
              backPath="/news"
              cta={
                <>
                  {!['INACTIVE', 'DISCONTINUED', 'CLOSED', 'PENDING'].includes(
                    poll?.statusId ?? ''
                  ) && (
                    <>
                     <Button
                        type="primary"
                        className="breadcumb-btn-mg"
                        onClick={() => history.push(`/newsfeed/edit/${id}`) }
                      >
                        Edit
                      </Button>
                      
                        <Button
                         className='cta-button'
                         onClick={()=>window.open(`${config.sportzchainFanPageURL}/post-preview?formValues=${btoa(JSON.stringify({...poll}))}`)}
                       >
                        Live Preview
                      </Button>
                      </>
                    )}
                </>
              }
              breadCrumbs={[
                { name: 'Newsfeed', url: '/news' },
                { name: poll?.title, url: `/news/${id}` },
              ]}
            />
          </Row>
          <Card>
            <TitleCta
              // title={`Poll > ${id}`}
              title={'Basic Info'}
            />
            <Row 
              justify="space-between"
              gutter={[24,0]}
              style={{ marginBottom: '36px' }}
            >
              <Col span={6}>
                <Label>Poll Title</Label>
                <TextContent>{poll?.title}</TextContent>
              </Col>
              <Col span={6}>
              <Label>ID</Label>
              <TextContent>{poll?.id}</TextContent>
              </Col>
              <Col span={6}>
                <Label>Poll Status</Label>
                <TextContent style={{color:"#1890ff"}}>{poll?.statusId}</TextContent>
              </Col>
              <Divider />
              <Col span={6}>
                  <Label>{`${getValueByType("Team", "Group")} Name`}</Label>
                  <TextContent>{getValueByType(poll?.teamByTeamId?.name, poll?.groupByGroupId?.groupName)}</TextContent>
              </Col> 
              <Col span={6}>
                <Label>Start Date Time </Label>
                <TextContent>
                  {` ${moment(poll?.startAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Col span={6}>
                <Label>End Date Time</Label>
                <TextContent>
                  {`${moment(poll?.endAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Divider />
              <Col span={6}>
                <Label>Display Start Date Duration</Label>
                <TextContent>
                  {`${moment(poll?.displayStartAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Col span={6}>
                <Label>Display End Date Duration</Label>
                <TextContent>
                  {`${moment(poll?.displayEndAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Col span={6}>
                <Label>Currency Symbol</Label>
                <TextContent>{poll?.coinSymbol}</TextContent>
              </Col>
              <Divider />
              <Col span={6}>
                <Label>Min Token needed</Label>
                {poll?.minJoinBalance !== null ?
                 <TextContent>{parseInt(poll?.minJoinBalance).toFixed(2) + ' ' + poll?.coinSymbol}</TextContent>
                 : '-'
                }
               
              </Col>
              <Col span={6}>
                <Label>Post Display priority</Label>
                <TextContent>{poll?.displayPriority}</TextContent>
              </Col>
              <Divider />
            </Row>
          
          </Card>

          <Card>

<TitleCta
    title="Post Details"
  />
  <LabelInput
    label="Body Text"
    name="description"
    inputElement={
        <TextEditorInput 
         editData={poll?.description} 
         setValue={()=>false} 
         view={true}
         />
     }
  />
  <Divider/>
  <Label>Post image</Label>
              <div className="banner-image">
                {poll?.postUrl ? <img
                  className="banner-image-element logo"
                  src={
                    `${config.apiBaseUrl}files/${poll?.postUrl}` ??
                    ''
                  }
                  alt="banner"
                /> : '-'}
              </div>
</Card>
         
          
            <Row gutter={[24, 24]}>
             <Col xs={18} md={8}>
                <Card>
                  <Label>Total Likes</Label>
                  <TextContent className="stats-count">
                    {poll?.totalVoteCount ?? 0}
                  </TextContent>
                </Card>
              </Col>
              <Col xs={18} md={8}>
                <Card>
                  <Label>Total Shares </Label>
                  <TextContent className="stats-count">
                    {poll?.numOfShares ?? 0}
                  </TextContent>
                </Card>
              </Col>
              <Col xs={18} md={8}>
                <Card>
                  <Label>Total Views</Label>
                  <TextContent className="stats-count">
                    {poll?.viewsCount ?? 0}
                  </TextContent>
                </Card>
              </Col>
             
            </Row>
            <Card>
              <TitleCta
                title="Notes"
              />
              <NoteModal
                EditData={viewNoteData}
                ModalVisible={isVisible}
                handleOk={handleNoteModal}
                handleCancel={handleNoteModal}
                readOnly
              />
              <Note
                JsonData={contructNoteJson}
                openEdit={(obj: any, key: number) => handleNoteModal(true, key - 1)}
                readOnly
              />
            </Card>
        </>
      )}
    </PollsManagementWrapper>
  );
};

export default NewsView;
