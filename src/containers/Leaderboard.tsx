import { Button, Card } from 'antd';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import styled from 'styled-components/macro';
import { ReactComponent as MoreIcon } from 'assets/icons/ellipse.svg';
import { ReactComponent as AddPersonIcon } from 'assets/icons/add-person.svg';
import LeaderboardInfo from 'components/leaderboard/LeaderboardInfo';
import { useGetLeaderboardInfoQuery } from 'generated/graphql';
import { Link } from 'react-router-dom';
import StatCard from 'components/common/cards/StatCard';

const LeaderboardWrapper = styled.div`
  width: 100%;
  padding: 40px;
  .ant-card {
    width: 303px;
    .ant-card-head {
      border-bottom: none;
    }
  }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-leaderboard-rules {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
    .ant-btn.view-leaderboard-button {
      border: 1px solid rgba(7, 105, 125, 0.25);
      color: #fff;
      box-sizing: border-box;
      border-radius: 5px;
      background: #07697d;
      height: fit-content;
      padding: 15px;
      font-weight: bold;
      display: flex;
      align-items: center;
      column-gap: 10px;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */
    white-space: nowrap;
    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }
  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
`;

interface LeaderboardProps {}

const Leaderboard = ({}: LeaderboardProps): JSX.Element => {
  const { data: leaderboardInfo } = useGetLeaderboardInfoQuery();
  return (
    <LeaderboardWrapper>
      <FlexRowWrapper className="user-text-create-wrapper">
        <Text className="t-leaderboard-rules">Leaderboard rules</Text>
        <Link to="/rewards">
          <Button className="view-leaderboard-button"> View Rewards</Button>
        </Link>
      </FlexRowWrapper>

      <FlexRowWrapper className="cards-wrapper">
        <StatCard
          title="New users"
          cta={<MoreIcon />}
          value={<div className="green">400</div>}
          supportingValueText="This Month"
        />

        <StatCard
          title="Leaderboard movements"
          cta={<MoreIcon />}
          value={<div className="green">400</div>}
          supportingValueText="This Month"
        />
      </FlexRowWrapper>
      <LeaderboardInfo
        leaderboardInfo={leaderboardInfo?.platformLeaderboard ?? []}
      />
    </LeaderboardWrapper>
  );
};

export default Leaderboard;
