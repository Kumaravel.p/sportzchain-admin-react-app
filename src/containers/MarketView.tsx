import { Card } from 'antd';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import MarketStats from 'components/market-view/MarketStats';
import styled from 'styled-components/macro';

const MarketViewWrapper = styled.div`
  padding: 40px;
  width: 100%;
  .t-market-view {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
    width: 100%;
  }
  .cards-wrapper {
    margin: 40px 0;
    column-gap: 40px;
    width: 100%;
    .count-monthly-wrapper {
      justify-content: space-between;
      align-items: center;
    }
    .t-users-count {
      font-size: 24px;
      line-height: 28px;
      /* identical to box height, or 117% */

      display: flex;
      align-items: center;

      color: #ff6d4a;
      span {
        background: #ff5756;
        /* Red / 1 */

        border: 2px solid #ff5756;
        box-sizing: border-box;
        border-radius: 100px;
        font-size: 12px;
        line-height: 16px;
        /* identical to box height, or 133% */

        display: flex;
        align-items: center;
        letter-spacing: 0.4px;

        color: #ffffff;

        padding: 1px 9px;
      }
    }
    .t-month {
      font-size: 14px;
      line-height: 19px;
      display: flex;
      align-items: flex-end;
      letter-spacing: -0.1px;
      color: rgba(50, 60, 71, 0.4);
    }
  }
`;

interface MarketViewProps {}

const MarketView = ({}: MarketViewProps): JSX.Element => {
  return (
    <MarketViewWrapper>
      <Text className="t-market-view">Market View</Text>
      <FlexRowWrapper className="cards-wrapper">
        <Card title="SPN" extra={<a href="#">More</a>} style={{ width: 300 }}>
          <FlexRowWrapper className="count-monthly-wrapper">
            {/* <FlexRowWrapper> */}
            <Text className="t-users-count">
              800 &nbsp;<span>7.00%</span>
            </Text>
            {/* </FlexRowWrapper> */}
            <Text className="t-month">This Month</Text>
          </FlexRowWrapper>
        </Card>
        <Card title="RWRD" extra={<a href="#">More</a>} style={{ width: 300 }}>
          <FlexRowWrapper className="count-monthly-wrapper">
            <Text className="t-users-count">
              800 &nbsp;<span>7.00%</span>
            </Text>
            <Text className="t-month">This Month</Text>
          </FlexRowWrapper>
        </Card>{' '}
      </FlexRowWrapper>
      <MarketStats />
    </MarketViewWrapper>
  );
};

export default MarketView;
