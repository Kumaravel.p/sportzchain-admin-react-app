import { Button, Card, Switch, Row, Col, Spin, Divider } from 'antd';
import React,{ useCallback, useMemo } from 'react';
import Text from 'antd/lib/typography/Text';
import FlexRowWrapper from 'components/common/wrappers/FlexRowWrapper';
import UserInfo from 'components/user-management/UserInfo';
import styled from 'styled-components/macro';
import { LeftOutlined } from '@ant-design/icons';
import TeamInfo from 'components/team/TeamInfo';
import {
  GetPollQuery,
  useGetPollQuery,
  useGetPostsQuery,
  useGetTeamsQuery,
} from 'generated/graphql';
import { Link, useLocation } from 'react-router-dom';
import TitleCta from 'components/common/TitleCta';
import TitleCtaTable from 'components/common/TitleCtaTable';
import { pollsColumns } from 'data/polls';
import { UserAddOutlined } from '@ant-design/icons';
import { useParams } from 'react-router-dom';
import PollTemplate from 'components/poll/PollTemplate';
import { useHistory } from 'react-router-dom';
import { Label, TextContent } from 'components/common/atoms';
import { format } from 'date-fns';
import OptionsView from 'components/poll/optionsview';
import BreadcrumbComp from 'components/common/breadcrumb';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import config from 'config';
import moment from 'moment';
import { TeamSelectionContext } from 'App';
import NoteModal from "components/NewTeams/addNoteModal";
import Note from "components/NewTeams/note";
import { useSnapshot } from 'valtio';
import { state as ProxyState } from 'state';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ChartDataLabels
);

const PollsManagementWrapper = styled.div`
  width: 100%;
  padding: 40px;
  background: rgba(245, 245, 247, 1);
  display: grid;
  grid-gap: 24px;
  .ant-divider-horizontal {
    margin: 12px 0px;
  }
  .ant-card {
    border-radius: 10px;
    .ant-card-head {
      border-bottom: none;
    }
  }
  .add-new-button {
    border-radius: 8px;
    font-weight: 500;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;
  }
  .user-text-create-wrapper {
    justify-content: space-between;
    margin-bottom: 40px;
    .t-user-management {
      font-weight: 600;
      font-size: 25px;
      line-height: 30px;
      color: #051f24;
    }
    .ant-btn.add-new-button {
      border: 1px solid rgba(7, 105, 125, 0.25);
      color: rgba(7, 105, 125, 1);
      box-sizing: border-box;
      border-radius: 5px;
      height: fit-content;
      /* padding: 15px; */
      display: flex;
      align-items: center;
      column-gap: 10px;
    }
  }
  .t-users-count {
    font-size: 24px;
    line-height: 28px;
    /* identical to box height, or 117% */

    display: flex;
    align-items: center;

    &.red {
      color: #ff6d4a;
    }
    &.green {
      color: #4aaf05;
    }

    span {
      /* Red / 1 */
      align-self: flex-end;
      box-sizing: border-box;
      border-radius: 100px;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.4px;
      color: #ffffff;
      padding: 1px 9px;
      &.red {
        border: 2px solid #ff5756;

        background: #ff5756;
      }
      &.green {
        border: 2px solid #4aaf05;
        background: #4aaf05;
      }
    }
  }

  .t-month {
    font-size: 14px;
    line-height: 19px;
    display: flex;
    align-items: flex-end;
    letter-spacing: -0.1px;
    color: rgba(50, 60, 71, 0.4);
  }

  .count-monthly-wrapper {
    justify-content: space-between;
    align-items: center;
  }

  .cards-wrapper {
    column-gap: 20px;
    font-family: 'Nunito', sans-serif;
  }
  .cta-button {
    border-radius: 5px;
    font-weight: 500;
    font-size: 14px;
    line-height: 19px;
    text-align: center;
    letter-spacing: 0.5px;
    border: 1px solid #369afe;
    color: #369afe;
    margin-left: 10px;
  }
  .t-allow-multiple {
    font-size: 16px;
    line-height: 150%;
    color: #0c2146;
  }
  & .stats-count {
    color: #4da1ff;
    font-size: 20px;
    margin: 0;
  }
  & .live-preview-poll {
    background: #1890ff;
    color: #fff;
    font-weight: 500;
    border-radius: 5px;
    padding: 8px;
  }
`;

interface PollsManagementProps { }
const eData = [
  {
    __typename: 'teamContest',
    id: 11,
    teamId: 1,
    title: 'test title',
    endAt: '2022-04-07T19:30:20.224',
    startAt: '2022-03-22T19:30:18.057',
    statusId: 'Active',
    team: {
      __typename: 'team',
      name: 'testin',
    },
  },
];
const PollView = () => {
  const { state } = useLocation<any>();

  const { id, teamId } = useParams<{ id: string, teamId: string }>();
  const teamSelectionContext = React.useContext(TeamSelectionContext);
  const [isVisible,setIsVisible] = React.useState<boolean>(false);
  const [editIndex,setEditIndex] = React.useState<number>(-1);
  const history = useHistory();

  const { profile } = useSnapshot(ProxyState);
  
  let team:any = profile?.teamDetail

  const isIamTeamUser = () => {
    return team?.teams?.length > 0;
  }

  if (isIamTeamUser()) {
    if (parseInt(teamId) !== teamSelectionContext.team.id) {
      history.push("/");
    }
  }
  const {
    data: pollData,
    loading,
    refetch: refetchPolls,
  } = useGetPollQuery({
    variables: { id: parseInt(id) },
  });

  const poll = pollData?.teamPoll_by_pk as NonNullable<
    GetPollQuery['teamPoll_by_pk']
  >;

  React.useEffect(() => {
    refetchPolls();
  }, []);

  const getPollResult = React.useMemo(() => {
    if (!poll?.teamPollOptions || !poll?.totalVoteCount) return [];
    return poll?.teamPollOptions?.map(
      ({ voteCount }) => (voteCount / poll?.totalVoteCount) * 100
    );
  }, [poll]);

  const dataBar = React.useMemo(
    () => ({
      labels: poll?.teamPollOptions
        ? [...poll?.teamPollOptions?.map(({ description }) => description)]
        : [],
      datasets: [
        {
          label: poll?.title,
          barPercentage: 0.2,
          data: [...getPollResult],
        },
      ],
    }),
    [getPollResult, poll]
  );

  const giveMeDisplayDate = (_poll: any) => {
    if (_poll.displayResult) {
      return 'Yes'
    } else {
      return 'No'
    }
  }

  const giveMeTitleForCTA = () => {
    let title = "";
    if (state?.teamName) {
      title = `Team > ${state?.teamName} > Poll`
    } else {
      title = 'Poll'
    }
    return `${title} ${id}`;
  }

  const contructNoteJson = useMemo(() => {
   if(!poll?.teamPollNotes?.length) return []
   return poll?.teamPollNotes?.map(note=>({
    name: note?.name,
    notes: note?.notes,
    addedDate: moment(note?.createdDate.concat('z')),
    addedby: note?.addedBy,
   }))
  }, [poll])

  const editNoteData = useMemo(() => {
    if(editIndex > -1){
      return {
        name:contructNoteJson[editIndex]?.name,
        note:contructNoteJson[editIndex]?.notes
      }
    }
    return {}
  }, [editIndex])

  const handleNoteModal = (bool:boolean=false,index:number=-1) => {
    setIsVisible(bool)
    setEditIndex(!bool ? -1 : index)
  }

  const getValueByType = useCallback((team: string = "-", group: string = "-") => {
    return poll.type === "GROUPS" ? group : team
  }, [poll])

  return (
    <PollsManagementWrapper>
      {loading && !poll ? (
        <Row style={{ height: '70%' }} justify="center" align="middle">
          <Spin />
        </Row>
      ) : (
        <>
          <Row>
            <BreadcrumbComp
              backPath="/polls"
              cta={
                <>
                  {!['INACTIVE', 'DISCONTINUED', 'CLOSED', 'PENDING'].includes(
                    poll?.status ?? ''
                  ) && (
                      <Button
                        className="cta-button discard"
                        onClick={() =>
                          history.push(`/poll/edit/${id}`, {
                            teamName: state?.teamName,
                            teamId: state?.teamId,
                          })
                        }
                      >
                        Edit
                      </Button>
                    )}
                </>
              }
              breadCrumbs={[
                { name: 'Polls', url: '/polls' },
                { name: poll.title, url: `/polls/${id}` },
              ]}
            />
          </Row>
          <Card>
            <TitleCta
              // title={`Poll > ${id}`}
              title={giveMeTitleForCTA()}
            />
            <Row 
              justify="space-between"
              gutter={[24,0]}
              style={{ marginBottom: '36px' }}
            >
              <Col span={6}>
                <Label>Poll Title</Label>
                <TextContent>{poll.title}</TextContent>
              </Col>
              <Col span={6}>
              <Label>ID</Label>
              <TextContent>{poll.id}</TextContent>
              </Col>
              <Col span={6}>
                <Label>Poll Status</Label>
                <TextContent>{poll.status}</TextContent>
              </Col>
              <Divider />
              <Col span={6}>
                <Label>{`${getValueByType("Team","Group")} Name`}</Label>
                <TextContent>{getValueByType(poll?.team?.name,poll?.group?.groupName ?? "-")}</TextContent>
              </Col>
              <Col span={6}>
                <Label>Poll Type</Label>
                <TextContent>{poll.typeId}</TextContent>
              </Col>
              <Col span={6}>
                <Label>Poll Platform</Label>
                <TextContent>{poll.pollPlatform}</TextContent>
              </Col>
              <Divider />  
              <Col span={6}>
                <Label>Start Date Time </Label>
                <TextContent>
                  {` ${moment(poll.startAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Col span={6}>
                <Label>End Date Time</Label>
                <TextContent>
                  {`${moment(poll.endAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Col span={6}>
                <Label>Display Start Date Duration</Label>
                <TextContent>
                  {`${moment(poll.displayStartAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Divider />  
              <Col span={6}>
                <Label>Display End Date Duration</Label>
                <TextContent>
                  {`${moment(poll.displayEndAt + 'z').format(
                    'Do MMM YY, hh:mm A'
                  )}`}
                </TextContent>
              </Col>
              <Col span={6}>
                <Label>Currency Symbol</Label>
                <TextContent>{poll.currency ? poll.currency : '-'}</TextContent>
              </Col>
              <Col span={6}>
                <Label>Min Join Balance</Label>
                <TextContent>{poll.minJoinBalance ?? 0}</TextContent>
              </Col>
              <Divider />
              <Col span={6}>
                <Label>Number Of Participants Allowed </Label>
                <TextContent>{poll.allowedParticipants ? poll.allowedParticipants : '-'}</TextContent>
              </Col>
              <Col span={6}>
                <Label>Display results at the end of poll</Label>
                <TextContent>{poll.displayResult ? 'Yes' : 'No'}</TextContent>
              </Col>
              <Col span={6}>
                <Label>Poll Position</Label>
                <TextContent>{poll.orderBy}</TextContent>
              </Col>
              <Divider />
            </Row>
          
          </Card>
          {poll.pollPlatform === 'ON_CHAIN' && (
            <Card>
              <TitleCta
                // title={`Poll > ${id}`}
                title={`Smart Contract Details`}
              />
              <Row gutter={[24, 0]} style={{ marginBottom: '36px' }}>
                <>
                  <Col>
                    <Label>Smart Contract Address</Label>
                    <TextContent>
                      {poll?.smartContractAddress ?? '-'}
                    </TextContent>
                  </Col>
                  {/* <Col>
                    <Label>Transaction ID</Label>
                    <TextContent>
                      {poll?.smartContractStatus ?? '-'}
                    </TextContent>
                  </Col>
                  <Col>
                    <Label>Block No.</Label>
                    <TextContent>
                      {poll?.smartContractStatus ?? '-'}
                    </TextContent>
                  </Col> */}
                  <Col>
                    <Label>Smart Contract Status</Label>
                    <TextContent>
                      {poll?.smartContractStatus ?? '-'}
                    </TextContent>
                  </Col>
                </>
              </Row>
            </Card>
          )}
          <Card>
            <PollTemplate value={poll.teamPollContentType ?? 'text'} />
          </Card>
          <Card>
            <TitleCta title="Poll Stats" />
            <Row gutter={[24, 24]}>
              <Col xs={24} md={12}>
                <Card>
                  <Label>Participants</Label>
                  <TextContent className="stats-count">
                    {poll?.totalVoteCount ?? 0}
                  </TextContent>
                </Card>
              </Col>
              {/* <Col xs={24} md={12}>
                <Card>
                  <Label>
                    Participants Won
                  </Label>
                  <TextContent className="stats-count">
                    3000
                  </TextContent>
                </Card>
              </Col> */}
            </Row>
          </Card>
          <Card>
            <TitleCta
              title="Poll Question"
              cta={
                <Link
                  to={{
                    pathname: `${config.sportzchainFanPageURL}/poll-preview?formValues=${JSON.stringify({ ...poll,teamDetails:poll?.team })}`,
                  }}
                  target="_blank"
                  className="live-preview-poll"
                >
                  Poll Preview
                </Link>
              }
            />
            <Row>
              <Col span={24}>
                <Label>Poll Question</Label>
                <TextContent>{poll.question}</TextContent>
              </Col>
              <Divider />
              <Col span={24}>
                <Label>Poll Description</Label>
                <TextContent>{poll.description}</TextContent>
              </Col>
              <Divider />
              {poll.teamPollOptions && poll.teamPollOptions.length > 0 && (
                <Col span={24} style={{ marginTop: '16px' }}>
                  <OptionsView options={poll.teamPollOptions ?? []} />
                </Col>
              )}
              <Col style={{ marginTop: '32px' }}>
                <Row gutter={[24, 0]}>
                  <Col>
                    <Switch
                      disabled={true}
                      className="toggle-if-multiple"
                      defaultChecked={poll.canMultiselect}
                    />
                  </Col>
                  <Text className="t-allow-multiple" strong>
                    Allow Multiple
                  </Text>
                </Row>
              </Col>
            </Row>
          </Card>
          <Card>
            <TitleCta title="Results" />
            <Bar
              data={dataBar}
              height={100}
              options={{
                indexAxis: 'y',
                layout: {},
                elements: {
                  bar: {
                    borderSkipped: false,
                    backgroundColor: '#FF3A29',
                    borderWidth: 1,
                    borderRadius: 50,
                  },
                },
                scales: {
                  y: {
                    grid: {
                      display: false,
                    },
                  },
                  x: {
                    grid: {
                      display: false,
                    },
                  },
                },
                plugins: {
                  datalabels: {
                    display: true,
                    anchor: 'center',
                    // align: "right",
                    labels: {
                      title: {
                        font: {
                          weight: 'bold',
                        },
                        color: '#fff',
                      },
                    },
                    formatter: (value: number) => {
                      return `${value.toFixed(3)}%`;
                    },
                  },
                },
              }}
            />
          </Card>
            <Card>
            <TitleCta title="Notes" />
              <NoteModal
                EditData={editNoteData}
                ModalVisible={isVisible}
                handleOk={handleNoteModal}
                handleCancel={handleNoteModal}
                readOnly
              />
              <Note
                JsonData={contructNoteJson}
                openEdit={(obj: any, key: number) => handleNoteModal(true,key-1)}
                readOnly
              />
            </Card>
        </>
      )}
    </PollsManagementWrapper>
  );
};

export default PollView;
