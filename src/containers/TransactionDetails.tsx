import styled from 'styled-components/macro';
import { Breadcrumb, Card, Col, Divider, Row } from 'antd';
import FlexColumnWrapper from 'components/common/wrappers/FlexColumnWrapper';
import Text from 'antd/lib/typography/Text';
import { ReactComponent as MoreIcon } from 'assets/icons/more.svg';
import { useParams } from 'react-router-dom';
import { useGetTransactionByIdQuery } from 'generated/graphql';
import { format } from 'date-fns/esm';
import LabelValue from 'components/common/LabelValue';

const TransactionDetailsWrapper = styled.div`
  padding: 40px;
  width: 100%;

  .t-transactions {
    font-size: 25px;
    line-height: 30px;
    color: rgba(5, 31, 36, 0.3);
  }
  .t-transaction-id {
    font-weight: 600;
    font-size: 25px;
    line-height: 30px;
    color: #051f24;
  }
  .ant-card {
    margin-top: 40px;
    width: 100%;
    .info-edit-wrapper {
      margin-bottom: 40px;
    }
    .t-info {
      font-weight: 600;
      font-size: 20px;
      line-height: 24px;
      color: #051f24;
    }
    .t-transaction-key-field {
      font-size: 15px;
      line-height: 18px;
      color: rgba(5, 31, 36, 0.6);
    }
    .t-transaction-value {
      font-size: 16px;
      line-height: 19px;
      font-weight: 500;

      color: #051f24;
    }
    .key-value-wrapper {
      row-gap: 10px;
    }
  }
`;

interface TransactionDetailsProps {}

const TransactionDetails = ({}: TransactionDetailsProps): JSX.Element => {
  const { id } = useParams<{ id: string }>();

  const { data: transactionData } = useGetTransactionByIdQuery({
    variables: { transactionId: +id },
  });

  return (
    <TransactionDetailsWrapper>
      <Breadcrumb separator=">">
        <Breadcrumb.Item className="t-transactions">
          Transactions
        </Breadcrumb.Item>
        <Breadcrumb.Item
          className="t-transaction-id"
          href="#"
        ></Breadcrumb.Item>
      </Breadcrumb>

      <Card>
        <Row className="info-edit-wrapper" justify="space-between">
          <Col className="t-info">Info</Col>
          <Col>
            <MoreIcon />
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <LabelValue
              field="Pay ID"
              value={transactionData?.transactions_by_pk?.id}
            />
          </Col>

          <Col span={8}>
            <LabelValue
              field="Name"
              value={transactionData?.transactions_by_pk?.user?.name}
            />
          </Col>
          <Col span={8}>
            <LabelValue
              field="Status"
              value={transactionData?.transactions_by_pk?.status}
            />
          </Col>
        </Row>
        <Divider />
        <Row>
          <Col span={8}>
            <LabelValue
              field="Transaction type"
              value={transactionData?.transactions_by_pk?.type}
            />
          </Col>
          <Col span={8}>
            <LabelValue
              field="Exchange Rate"
              value={transactionData?.transactions_by_pk?.exchangeRate}
            />
          </Col>
          <Col span={8}>
            <LabelValue
              field="Time"
              value={
                transactionData?.transactions_by_pk?.endAt &&
                format(
                  new Date(transactionData?.transactions_by_pk?.endAt),
                  'PPp'
                )
              }
            />
          </Col>
        </Row>
        <Divider />
        <Row>
          <Col span={8}>
            <LabelValue
              field="From"
              value={`${transactionData?.transactions_by_pk?.fromCoinAmount}
              ${transactionData?.transactions_by_pk?.fromCoin}`}
            />
          </Col>

          <Col span={8}>
            <LabelValue
              field="To"
              value={`${transactionData?.transactions_by_pk?.toCoinAmount}
              ${transactionData?.transactions_by_pk?.toCoin}`}
            />
          </Col>
        </Row>
      </Card>
    </TransactionDetailsWrapper>
  );
};

export default TransactionDetails;
