import {
  Button,
  Card,
  Row,
  Input,
  Col,
  Select,
  DatePicker,
  TablePaginationConfig,
  Typography,
} from 'antd';
import * as React from 'react';
import styled from 'styled-components/macro';
import { SearchOutlined, CloseOutlined } from '@ant-design/icons';
import TitleCta from 'components/common/TitleCta';
import { cmsColumns } from 'data/cms';
import CustomTable from 'components/customTable';
import { useHistory, useParams } from 'react-router-dom';
import { cmsList } from 'apis/cms/createList';
import { toast } from 'react-toastify';
import { useDebounce } from 'hooks/useDebounce';
import moment from 'moment';
import { format } from 'path';


const StaticContentManagementWrapper = styled.div`
    width: 100%;
    padding: 40px;
    background: rgba(245, 245, 247, 1);
  `;

interface StaticContentManagementProps {
}
//  interface StaticContentManagementFilter { 
//   offset: number;
//   limit: number;
//   searchText: string;
//  }

const StaticContentManagement = ({ }: StaticContentManagementProps): JSX.Element => {

  const debounce = useDebounce();
  const [searchInput, setSearchInput] = React.useState("");
  const [cms, setCms] = React.useState<any>([]);
  const [cmsLoading, setCmsLoading] = React.useState(false);
  const history = useHistory();

  const { type }: { type?: 'terms-conditions' | 'privacy-policy' | 'announcements' | 'explore' | 'ad_Banner_Home_Page' | 'con_terms&condition' | 'Team_ad_banner' } = useParams();

  const ConstructRouteJson =
    [
      {
        name: "Terms & Conditions",
        route: "terms-conditions",
      },
      {
        name: "Privacy Policy",
        route: "privacy-policy",
      },
      {
        name: "Announcements",
        route: "announcements",
      },
      {
        name: "Explore",
        route: "explore",
      },
      {
        name: "Ad banner - Home Page",
        route: "ad_Banner_Page",
      },
      {
        name: "Contest - Terms & Conditions",
        route: "con_terms&condition"
      },
      {
        name: "Team Ad Banner",
        route: "Team_ad_banner"
      }
    ]

  let pageSize = 5;

  const [pagination, setPagination] = React.useState<TablePaginationConfig>({
    current: 1,
    pageSize,
  });

  let handleSearch = (value: string) => {
    debounce(() => setSearchInput(value), 800);
  }

  const getCmsData = async () => {
    await cmsList(
      {
        searchText: searchInput
      }).then((data) => {
        // setPagination({
        //   ...pagination,
        //   current: from === "initial" ? 1 : offset,
        //   pageSize: limit,
        //   total: data?.data?.data?.totalCount
        // });
        let stock: any[] = []
        if (data?.data?.data?.result?.length) {
          data.data.data.result?.map((val: any) => {
            let obj: any = {}
            obj.id = val?.id;
            obj.updatedOn = moment(val?.contentItemsByContentId?.nodes?.[0]?.updatedAt.concat('z')).format("Do MMM YY hh:mm A");
            obj.contentName = val?.description ?? '';
            obj.contentType = val?.masterContentTypeByTypeId?.description ?? ''
            stock.push(obj)
            return val
          })
          setCms(stock);
          setCmsLoading(false);
        }
      })
      .catch((error) => {
        console.log(error);
        toast.error(`Not able to load the Cms. Please try again!`);
      })
  }
  const onChangeTable = (newPagination: TablePaginationConfig) => {
    // setCmsLoading(true);
    // const { current = 1, pageSize } = newPagination;
    // let offset = current;
    // let from = "pagination"
    // if (pagination?.pageSize !== newPagination?.pageSize) {
    //   offset = 1;
    //   from = ""
    // }
    // getCmsData(offset, pageSize, from)
  }

  React.useEffect(() => {
    setCmsLoading(true);
    getCmsData();
  }, [searchInput]);

  const onRowClick = (record: any) => {
    const Route = ConstructRouteJson.filter(v => v.name === record?.contentName)?.[0]?.route;
    history.push(`/cms/${Route}/${record.id}`)
  }


  return (
    <StaticContentManagementWrapper>
      <TitleCta
        title="CMS"
      />
      <Card>
        <TitleCta
          title={`Content`}
          cta={
            <Input
              size="middle"
              type="text"
              className="search"
              allowClear
              prefix={<SearchOutlined />}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                handleSearch(e.target?.value)
              }
              placeholder={'Search'}
            />
          }
        />
        <CustomTable
          columns={cmsColumns}
          dataSource={cms}
          onRowClick={onRowClick}
          pagination={{
            hideOnSinglePage:true
          }}
          onChangeTable={onChangeTable}
        />
      </Card>

    </StaticContentManagementWrapper>
  );
};

export default StaticContentManagement;
