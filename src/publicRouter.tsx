import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import currentSession from 'utils/getAuthToken';
import { useState, useEffect } from 'react';

const PublicRoute = ({ children, ...rest }: any): JSX.Element => {
  const [isAuthenticate, setIsAuthenticate] = useState(false);

  useEffect(() => {
    currentSession()
      .then((res) => {
        const token = res?.getJwtToken();
        if (token) {
          setIsAuthenticate(true);
        } else {
          setIsAuthenticate(false);
        }
      })
      .catch((ex) => {
        setIsAuthenticate(false);
      });
  }, []);

  const RouterRedirect = () => {
    return (
      <>
        <Route
          {...rest}
          render={(_) =>
            isAuthenticate ? (
              <Redirect
                to={{
                  pathname: '/users',
                  state: { from: _?.location },
                }}
              />
            ) : (
              children
            )
          }
        />
      </>
    );
  };

  return <RouterRedirect />;
};

export default PublicRoute;
