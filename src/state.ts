import { proxy } from 'valtio';


interface State {
  profile: any;
}

const state = proxy<State>({
  profile: null
});

export { state };